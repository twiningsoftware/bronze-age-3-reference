﻿namespace Bronze.Contracts.Help
{
    public static class HelpConstants
    {
        public static readonly string PAGE_BUILTIN_ITEMS = "builtin_items";
        public static readonly string PAGE_BUILTIN_BONUSES = "builtin_bonuses";
        public static readonly string PAGE_BUILTIN_INFRASTRUCTURE = "builtin_infrastructure";
        public static readonly string PAGE_BUILTIN_STRUCTURES = "builtin_structures";
        public static readonly string PAGE_BUILTIN_UNITS = "builtin_units";
        public static readonly string PAGE_BUILTIN_PLAYER_RACE = "builtin_player_race";
    }
}
