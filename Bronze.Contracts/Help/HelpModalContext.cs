﻿namespace Bronze.Contracts.Help
{
    public class HelpModalContext
    {
        public string PageId { get; set; }
        public string TopicId { get; set; }
    }
}
