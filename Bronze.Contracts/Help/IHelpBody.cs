﻿using Bronze.Contracts.UI;

namespace Bronze.Contracts.Help
{
    public interface IHelpBody
    {
        IUiElement BuildUiElement();
    }
}
