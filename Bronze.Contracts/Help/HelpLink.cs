﻿namespace Bronze.Contracts.Help
{
    public class HelpLink
    {
        public string PageId { get; }
        public string TopicId { get; }

        public HelpLink(string pageId, string topicId)
        {
            PageId = pageId;
            TopicId = topicId;
        }

        public HelpLink(string pageId)
        {
            PageId = pageId;
            TopicId = string.Empty;
        }
    }
}
