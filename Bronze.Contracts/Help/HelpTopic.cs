﻿namespace Bronze.Contracts.Help
{
    public class HelpTopic
    {
        public string Id { get; set; }

        public string Title { get; set; }

        public string ImageKey { get; set; }

        public IHelpBody[] Body { get; set; } 

        public HelpLink[] PageLinks { get; set; }

        public HelpTopic()
        {
            Body = new IHelpBody[0];
            PageLinks = new HelpLink[0];
        }
    }
}
