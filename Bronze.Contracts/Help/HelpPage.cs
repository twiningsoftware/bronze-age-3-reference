﻿namespace Bronze.Contracts.Help
{
    public class HelpPage
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public bool IsStartingPage { get; set; }

        public HelpTopic[] Topics { get; set; }
    }
}
