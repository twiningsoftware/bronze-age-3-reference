﻿using System.Collections.Generic;
using System.Numerics;

namespace Bronze.Contracts
{
    public static class Constants
    {
        public static readonly int CELL_SIZE_PIXELS = 128;
        public static readonly int CELL_SIZE_SUMMARY_PIXELS = 32;
        public static readonly int CELL_SIZE_MAP_PIXELS = 8;
        public static readonly int TILE_SIZE_PIXELS = 32;

        public static readonly int LARGE_HEX_HEIGHT_PIXELS = 54;
        public static readonly int LARGE_HEX_WIDTH_PIXELS = 64;
        public static readonly int LARGE_HEX_HORIZONTAL_STEP = 64;
        public static readonly int LARGE_HEX_VERTICAL_STEP = 41;
        
        public static readonly float ZOOM_MIN = 0.2f;
        public static readonly float ZOOM_DEFAULT = 1;
        public static readonly float ZOOM_MAX = 1;
        public static readonly float ZOOM_MAP_THRESHOLD = 0.3f;


        public static readonly double MONTHS_PER_SECOND = 0.05;
        
        public static readonly float HUNGER_PER_FOOD = 100;

        public static readonly float GARRISON_HEAL_PER_SECOND = 0.1f;
        public static readonly double COMBAT_ENGAGEMENT_MOD = 0.2;
        public static readonly float COMBAT_LIMIT_BASE_SECONDS = 1200;
        public static readonly double COMBAT_ROUT_TIME_SECONDS = 10;
        public static readonly double COMBAT_MOVE_MOD = 0.4;

        public static readonly float DEVELOPMENT_DEVASTATION_DAMAGE = 1.0f;
        public static readonly float DEVELOPMENT_DEVASTATION_HEAL = 0.1f;

        public static readonly float MULTI_UNIT_MOVE_PENALTY = 0.5f;

        public static readonly float UI_LAYER = 1f;

        public static readonly float START_MONTH = 14904;


        public static readonly float LAYER_DETAILED_BORDER = 0.2f;

        public static readonly float LAYER_SUMMARY_TERRAIN = 0;
        public static readonly float LAYER_SUMMARY_STRUCTURE = 1;
        public static readonly float LAYER_SUMMARY_UNIT = 2;
        public static readonly float LAYER_SUMMARY_BORDER = 0.2f;

        public static readonly float LAYER_MAP_TERRAIN = 0;
        public static readonly float LAYER_MAP_STRUCTURE = 1;
        public static readonly float LAYER_MAP_UNIT = 2;
        public static readonly float LAYER_MAP_BORDER = 1.2f;

        public static readonly int UNITS_PER_ARMY = 10;

        public static readonly string ANIMATION_IDLE = "idle";

        public static readonly double ATTRITION_RATE = 0.2;
        public static readonly double HEAL_RATE = 0.1;
        public static readonly double RESUPPLY_RATE = 6;

        public static readonly int ITEM_BUFFER_LOGI = 5;
        public static readonly int ITEM_BUFFER_CONSUME = 1;

        public static readonly float DIST_SKIRMISH_MAX = 1.5f;
        public static readonly float DIST_HARASSMENT_MIN = 0.9f;
        public static readonly float DIST_HARASSMENT_MAX = 1.3f;
        public static readonly float DIST_COMBAT = 0.3f;

        public static readonly float SKIRMISH_MOVEMENT_MOD = 0.5f;
        public static readonly float ROUTING_MOVEMENT_MOD = 1.1f;

        public static readonly int AUTHORITY_PER_STAT = 30;

        public static readonly float AUTOSAVE_MAX = 960;
        public static readonly float AUTOSAVE_MIN = 60;
        public static readonly float AUTOSAVE_DISABLE = 1080;
        public static readonly int TILE_VARIATION_COUNT = 20;

        public static readonly IEnumerable<string> IDLE_ANIMATIONS = new[] { "idle" };

        public static readonly IEnumerable<Vector2> SUBHEX_POSITIONS = new[]
        {
            new Vector2(-0.125f, -0.255f),
            new Vector2(0.125f, -0.255f),
            new Vector2(-0.25f, -0.0875f),
            new Vector2(0f, -0.0875f),
            new Vector2(0.25f, -0.0875f),
            new Vector2(-0.375f, 0f),
            new Vector2(-0.125f, 0f),
            new Vector2(0.125f, 0f),
            new Vector2(0.375f, 0f),
            new Vector2(-0.25f, 0.0875f),
            new Vector2(0f, 0.0875f),
            new Vector2(0.25f, 0.0875f),
            new Vector2(-0.125f, 0.255f),
            new Vector2(0.125f, 0.255f),
        };
    }
}
