﻿using Bronze.Contracts.Data.Dungeons;
using System.Collections.Generic;

namespace Bronze.Contracts.InjectableServices
{
    /// <summary>
    /// Defines a service that stores DungeonDefinitions.
    /// </summary>
    public interface IDungeonDataManager
    {
        List<DungeonDefinition> DungeonDefinitions { get; }
    }
}
