﻿using System.Collections.Generic;
using System.Numerics;
using Bronze.Common.Data.Game;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.UI;

namespace Bronze.Contracts.InjectableServices
{
    public interface IGameWorldDrawer
    {
        void DrawWorld(
            IDrawingService drawingService,
            IEnumerable<IMapInteractionComponent> mapInteractionComponents);
        
        void DrawCell(
            IDrawingService drawingService, 
            Cell cell, 
            IEnumerable<CellAnimationLayer> layers,
            ViewMode viewMode);

        void DrawCellAsSimple(
            IDrawingService drawingService,
            string imageKey,
            float drawLayer,
            CellPosition position,
            BronzeColor colorization,
            int cellSize,
            bool drawInRows,
            int frame);
        
        void DrawTile(IDrawingService drawingService, Tile tile, IEnumerable<TileAnimationLayer> layers);

        void DrawSprite(IDrawingService drawingService, Tile tile, IEnumerable<SpriteAnimation> layers);

        void DrawStructure(IDrawingService drawingService, Tile ulTile, IEnumerable<ITileDisplay> animationLayers, int tilesWide, int tilesHigh);

        Vector2 TransformFromScreen(Vector2 screenPos);

        Vector2 TransformToScreen(Vector2 worldPos);
        
        void DrawSelectionBox(IDrawingService drawingService, IStructure structure);

        void DrawRedX(IDrawingService drawingService, CellPosition position, ViewMode viewMode);

        void DrawPathdot(IDrawingService drawingService, CellPosition position, ViewMode viewMode);

        void DrawSelectionBox(IDrawingService drawingService, CellPosition position, ViewMode viewMode);

        void DrawCellIcon(
            IDrawingService drawingService,
            CellPosition position,
            ViewMode viewMode,
            string detailedImage,
            string summaryImage);
    }
}
