﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;

namespace Bronze.Contracts.InjectableServices
{
    public interface IPeopleHelper
    {
        double GetPregnancyChance(NotablePerson father, NotablePerson mother, double forDate);

        void Kill(NotablePerson person);

        NotablePersonSkillPlacement IncrementOrAdd(NotablePerson person, NotablePersonSkill skill);
    }
}
