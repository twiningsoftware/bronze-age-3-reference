﻿using System;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;

namespace Bronze.Contracts.InjectableServices
{
    public interface INotablePersonWorldgenAddon
    {
        void ApplyForWorldgen(
            Random random,
            Tribe owner,
            Race race,
            bool inDynasty,
            NotablePerson person,
            bool isRuler);
    }
}
