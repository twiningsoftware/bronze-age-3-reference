﻿using Bronze.Contracts.Data.World;

namespace Bronze.Contracts.InjectableServices
{
    public interface IDistrictGenerator
    {
        void QueueForGeneration(District district);
    }
}
