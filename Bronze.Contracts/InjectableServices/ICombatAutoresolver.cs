﻿using Bronze.Contracts.Data.Diplomacy;
using Bronze.Contracts.Data.World;
using System.Collections.Generic;

namespace Bronze.Contracts.InjectableServices
{
    public interface ICombatAutoresolver
    {
        PitchedBattleResult AutoResolveCombat(
            Cell location, 
            IEnumerable<Army> attackers, 
            IEnumerable<Army> defenders,
            int? fixedSeed);
    }
}
