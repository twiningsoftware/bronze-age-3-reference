﻿using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.Trade;
using Bronze.Contracts.Data.World;
using System.Collections.Generic;

namespace Bronze.Contracts.InjectableServices
{
    public interface ITradeManager
    {
        IEnumerable<TradeRoute> AllTradeRoutes { get; }
        IEnumerable<TradeCapInfo> GetTradeCapsFor(Settlement settlement);
        IEnumerable<ItemRate> GetImportsFor(Settlement settlement);
        IEnumerable<ItemRate> GetExportsFor(Settlement settlement);
        IEnumerable<TradeRoute> GetExportRoutes(Settlement settlement);
        IEnumerable<TradeRoute> GetImportRoutes(Settlement settlement);
        IEnumerable<TradePartnerInfo> GetTradePartnersFor(Settlement settlement);
        IEnumerable<TradeRoute> GetTradeRoutesFor(Tribe tribe);

        void RemoveTradeRoute(TradeRoute tradeRoute);

        TradeRoute CreateTradeRoute(ITradeActor from, ITradeReciever to);

        void ReparentTradeRoute(ITradeActor oldTrader, ITradeActor newTrader, TradeRoute tradeRoute);

        void ReparentTradeRoute(ITradeReciever oldReciever, ITradeReciever newReciever, TradeRoute tradeRoute);
    }
}
