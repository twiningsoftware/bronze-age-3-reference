﻿namespace Bronze.Contracts.InjectableServices
{
    public interface IConsoleCommand
    {
        string CommandText { get; }

        string QuickHelp { get; }
        string[] DetailedHelp { get; }

        void Execute(string[] parameters);

        string GetHint(string[] parameters);
    }
}
