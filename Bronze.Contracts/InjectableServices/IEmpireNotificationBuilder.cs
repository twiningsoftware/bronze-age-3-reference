﻿using Bronze.Contracts.Data;
using Bronze.Contracts.Data.World;

namespace Bronze.Contracts.InjectableServices
{
    public interface IEmpireNotificationBuilder
    {
        Notification BuildCapitalChanged(Tribe tribe, Settlement capital);
    }
}
