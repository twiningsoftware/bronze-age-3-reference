﻿using Bronze.Contracts.Data;
using System.Collections.Generic;

namespace Bronze.Contracts.InjectableServices
{
    public interface INotificationsSystem
    {
        IEnumerable<Notification> GetActiveNotifications();

        void AddNotification(Notification notification);

        void ShowNotification(Notification notification);
    }
}
