﻿using Bronze.Contracts.Data;
using Bronze.Contracts.Data.World;

namespace Bronze.Contracts.InjectableServices
{
    public interface IMarriageOptionNotificationBuilder
    {
        Notification BuildMarriageOptionNotification(
            string icon, 
            string summaryTitle, 
            string summaryBody, 
            string title, 
            string body, 
            NotablePerson person,
            NotablePerson spouse,
            Settlement fromSettlement,
            Buff settlementBuff);

        Notification BuildAdoptionOptionNotification(
            string icon,
            string summaryTitle,
            string summaryBody,
            string title,
            string body,
            NotablePerson adoptee,
            Settlement fromSettlement,
            Buff settlementBuff);
    }
}
