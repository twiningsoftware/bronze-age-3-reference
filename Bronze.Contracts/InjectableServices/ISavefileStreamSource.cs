﻿using Bronze.Contracts.Data.Serialization;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Bronze.Contracts.InjectableServices
{
    public interface ISavefileStreamSource
    {
        IEnumerable<SaveSummary> ListSaveFiles { get; }

        Task<Stream> OpenWriteStream(string filename);

        Task<Stream> OpenReadStream(string filename);

        void DeleteFile(string filename);
    }
}
