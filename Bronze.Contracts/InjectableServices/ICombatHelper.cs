﻿using System;

namespace Bronze.Contracts.InjectableServices
{
    public interface ICombatHelper
    {
        double ResolveMeleeAttack(Random random, int attackSkill, int defenseSkill, int blockSkill, double damage, int armor, int armorPenetration);

        double ResolveRangedAttack(Random random, int attackSkill, int blockSkill, double damage, int armor, int armorPenetration);
    }
}
