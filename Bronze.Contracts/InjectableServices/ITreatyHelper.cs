﻿using Bronze.Contracts.Data.Diplomacy;
using Bronze.Contracts.Data.World;
using System.Collections.Generic;

namespace Bronze.Contracts.InjectableServices
{
    public interface ITreatyHelper
    {
        IEnumerable<ITreaty> GetTreatiesFor(Tribe tribe);

        IEnumerable<ITreaty> GetTreatiesFor(Tribe tribe1, Tribe tribe2);

        void AddTreaty(ITreaty treaty);

        void RemoveTreaty(ITreaty treaty);
    }
}
