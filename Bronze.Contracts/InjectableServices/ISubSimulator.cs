﻿namespace Bronze.Contracts.InjectableServices
{
    public interface ISubSimulator
    {
        void StepSimulation(double elapsedMonths);
    }
}
