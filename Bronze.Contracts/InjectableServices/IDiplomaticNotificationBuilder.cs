﻿using Bronze.Contracts.Data;
using Bronze.Contracts.Data.World;

namespace Bronze.Contracts.InjectableServices
{
    public interface IDiplomaticNotificationBuilder
    {
        Notification BuildDiplomaticDiscoveryNotification(Tribe otherTribe, CellPosition discoveryLocation);

        Notification BuildNegotiationRequestNotification(Tribe otherTribe);
    }
}
