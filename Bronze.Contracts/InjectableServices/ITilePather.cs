﻿using Bronze.Contracts.Data.World;
using System;
using System.Collections.Generic;

namespace Bronze.Contracts.InjectableServices
{
    public interface ITilePather
    {
        IEnumerable<Tile> DeterminePath(
            Tile start, 
            Tile goal, 
            Settlement activeSettlement,
            Func<Tile, bool> isValid,
            bool penalizeTurning = false,
            float abortFactor = 2,
            int stepSize = 1);

        IEnumerable<Tile> DeterminePath(
            Tile[] starts,
            Tile[] goals,
            Settlement activeSettlement,
            Func<Tile, double> getHeuristic,
            Func<Tile, bool> isValid,
            bool penalizeTurning = false,
            float abortFactor = 2,
            int stepSize = 1);
    }
}
