﻿using System.Collections.Generic;
using System.Numerics;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;

namespace Bronze.Contracts.InjectableServices
{
    public interface IDrawingService
    {
        int DisplayWidth { get; }
        int DisplayHeight { get; }

        Vector2 GetImageSize(string imageKey);

        void DrawImage(
            string imageKey, 
            Rectangle bounds, 
            float layer,
            bool doVerticalLayerAdjust);

        void DrawImage(
            string imageKey,
            Rectangle bounds,
            float layer,
            BronzeColor colorization,
            bool doVerticalLayerAdjust);

        void DrawImage(
            string imageKey, 
            Vector2 center, 
            float layer,
            bool doVerticalLayerAdjust);

        void DrawImage(
            string imageKey, 
            BronzeColor colorization,
            Vector2 center, 
            float layer,
            bool doVerticalLayerAdjust);

        void DrawSlicedImage(
            string imageKey, 
            Rectangle bounds, 
            float layer,
            bool doVerticalLayerAdjust);

        void DrawScaledImage(
            string imageKey, 
            BronzeColor colorization, 
            Vector2 center, 
            float scale, 
            float spriteScale, 
            float layer,
            bool doVerticalLayerAdjust);

        void DrawAnimationFrame(
            string imageKey, 
            BronzeColor colorization,
            Rectangle sourceRect,
            Vector2 center, 
            float scale,
            bool horizontalMirror,
            float layer,
            bool doVerticalLayerAdjust);

        void DrawAnimationFrame(
            string imageKey,
            BronzeColor colorization,
            IEnumerable<Rectangle> sourceRects,
            Vector2 center,
            float scale,
            bool horizontalMirror, 
            float layer,
            bool doVerticalLayerAdjust);

        Vector2 MeasureText(string text);

        void OptimizedTileDraw(
            string imageKey, 
            BronzeColor colorization, 
            Vector2 center, 
            float drawLayer,
            bool[] adjacencies,
            bool doVerticalLayerAdjust,
            TileLayout layout);

        void DrawText(
            string text, 
            Point position, 
            float layer, 
            BronzeColor color = BronzeColor.White,
            float scale = 1);

        void DrawLine(
            Vector2 from, 
            Vector2 to, 
            float width, 
            float layer, 
            BronzeColor color,
            bool doVerticalLayerAdjust);

        int GetFrameForAnimation(int frameCount, float loopTime);
    }
}
