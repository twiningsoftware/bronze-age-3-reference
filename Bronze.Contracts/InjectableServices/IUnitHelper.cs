﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;

namespace Bronze.Contracts.InjectableServices
{
    public interface IUnitHelper
    {
        IUnit RecruitUnit(IUnitType unitType, UnitEquipmentSet equipmentSet, Settlement sourceSettlement);

        void ChangeEquipment(IUnit unit, UnitEquipmentSet equipmentSet, Settlement sourceSettlement);

        void RemoveUnit(Army army, IUnit unit, bool removedViolently);

        Army CreateArmy(Tribe owner, Cell position);
        void UnitAttritionedToDeath(Army army, IUnit unit);
        void RemoveArmy(Army army);
        void Disband(Army army, IUnit unit);
    }
}
