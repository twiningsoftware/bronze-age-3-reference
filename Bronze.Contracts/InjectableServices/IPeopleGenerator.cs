﻿using System;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;

namespace Bronze.Contracts.InjectableServices
{
    public interface IPeopleGenerator
    {
        NotablePerson CreatePerson(
            Random random, 
            Tribe owner,
            Race race, 
            bool isMale, 
            double birthDate, 
            NotablePerson father, 
            NotablePerson mother,
            bool inDynasty);
    }
}
