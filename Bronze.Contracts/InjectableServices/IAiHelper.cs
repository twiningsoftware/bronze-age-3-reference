﻿using Bronze.Contracts.Data.Diplomacy;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using System;
using System.Collections.Generic;

namespace Bronze.Contracts.InjectableServices
{
    public interface IAiHelper
    {
        void CheckForArmyStates(Tribe tribe, Dictionary<string, SerializedObject> armyStates);

        void SetSettlementIntruderStates(Tribe tribe, Dictionary<string, SerializedObject> settlementStates);

        void FormImperialArmies(
            Tribe tribe,
            Dictionary<string, SerializedObject> armyStates);

        void FormColonizerArmy(
            Tribe tribe,
            Dictionary<string, SerializedObject> armyStates);

        void FormLevyDefenseArmies(Tribe tribe, Settlement settlement, Dictionary<string, SerializedObject> armyStates);

        bool TryOrderArmyLevyPatrol(Tribe tribe, Army army, SerializedObject armyState, Random random);
        bool TryOrderArmyLevyDisband(Tribe tribe, Army army, SerializedObject armyState, Random random);
        
        bool TryOrderArmyPatrol(Tribe tribe, Army army);
        bool TryOrderArmyGatherUnits(Tribe tribe, Army army);
        bool TryOrderArmyRaiding(Tribe tribe, Army army, bool isAggressive, bool isCamping, double minSupplyPerc);
        bool TryOrderArmyCamping(Tribe tribe, Army army, bool isCamping);
        bool TryOrderArmySiege(Tribe tribe, Army army, Region targetRegion, bool takeByForce, bool isCamping, double minSupplyPerc);
        bool TryOrderArmyDisband(Tribe tribe, Army army);
        bool TryOrderArmyColonize(
            Tribe tribe, 
            Army army, 
            bool isColonizer, 
            StrategicTarget strategicTarget,
            Dictionary<Trait, int> settlementPlacementGoalScore,
            Dictionary<Trait, int> settlementPlacementNeighborScores);
        bool TryOrderCreepCleanup(Tribe tribe, Army army);

        void RecruitWarriors(Settlement settlement, Army army);
    }
}
