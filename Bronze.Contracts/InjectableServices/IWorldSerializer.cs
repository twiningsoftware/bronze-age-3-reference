﻿using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Delegates;

namespace Bronze.Contracts.InjectableServices
{
    public interface IWorldSerializer
    {
        void SaveWorld(ReportProgress report);
        void LoadWorld(ReportProgress report, SaveSummary saveSummary);
    }
}
