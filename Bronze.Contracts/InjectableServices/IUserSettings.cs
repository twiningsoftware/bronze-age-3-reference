﻿using Bronze.Contracts.Data;

namespace Bronze.Contracts.InjectableServices
{
    public delegate void SettingsChangedEventHandler(IUserSettings settings);

    public interface IUserSettings
    {
        float MusicVolume { get; set; }
        float EffectsVolume { get; set; }
        float AutosaveInterval { get; set; }
        bool IsFullScreen { get; set; }
        
        bool ShowRegionBorders { get; set; }

        bool PausedByPlayer { get; set; }
        bool SimulationPaused { get; }
        float SimulationSpeed { get; set; }
        bool SidebarMinimized { get; set; }
        bool ShowOverlaysMenu { get; set; }
        LogLevel LogLevel { get; set; }
        AlertLevel AlertLevel { get; set; }
        bool TutorialEnabled { get; set; }
        bool ActiveAdvice { get; set; }
        int AnalyticsEnabled { get; set; }
        bool OptimizeTileDrawing { get; set; }

        OutlinerMode OutlinerMode { get; set; }

        event SettingsChangedEventHandler OnSettingsChanged;
    }
}
