﻿using Bronze.Contracts.Delegates;
using System;

namespace Bronze.Contracts.InjectableServices
{
    public delegate void ExceptionThrownEventHandler(Exception ex);

    public interface IGameInterface
    {
        Version AssemblyVersion { get; }

        event EventHandler<EventArgs> Exiting;
        event ExceptionThrownEventHandler ExceptionThrown;
        event FpsHandler OnFpsUpdate;
        event PerformanceHandler OnRenderFinished;
        event PerformanceHandler OnUpdateFinished;
        event PerformanceHandler OnLayoutFinished;

        void OnException(Exception ex);

        void ExitGame();
        void MainMenu();
        void SaveAndMainMenu();
    }
}
