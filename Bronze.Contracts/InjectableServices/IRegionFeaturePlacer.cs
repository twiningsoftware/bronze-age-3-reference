﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Generation;
using Bronze.Contracts.Data.World;
using System;
using System.Collections.Generic;

namespace Bronze.Common.Generation
{
    public interface IRegionFeaturePlacer
    {
        void FillOutRegion(
            GenerationContext context,
            Plane plane,
            RegionFeature regionFeature,
            IEnumerable<Region> regions);

        void FillOutFeature(
            GenerationContext context,
            Plane plane,
            Biome biome,
            ICellFeature cellFeature,
            Plane mirrorsPlane,
            IEnumerable<RegionMirror> regionMirrors,
            IEnumerable<InclusionFeature> inclusions,
            IEnumerable<BorderFeature> borderFeatures,
            IEnumerable<SubFeature> subFeatures,
            IEnumerable<Region> regions);

        void PlaceInclusions(
            GenerationContext context,
            Plane plane,
            IEnumerable<InclusionFeature> inclusions,
            IEnumerable<Cell> cells);
    }
}
