﻿using Bronze.Contracts.Data.Mods;
using System;
using System.Collections.Generic;
using System.IO;

namespace Bronze.Contracts.InjectableServices
{
    public interface IModManager
    {
        bool AreModsSupported { get; }
        
        IEnumerable<ModInfo> GetActiveMods();
        IEnumerable<ModInfo> GetInactiveMods();

        void OpenModFolder();

        void RefreshModList();

        void SetActiveMods(IEnumerable<ModInfo> activeMods);

        void UnpackModCovers(Dictionary<string, MemoryStream> files);

        void UnpackMods(Dictionary<string, MemoryStream> files);
        
        void ResetModExceptions();

        IEnumerable<string> ModLoadFailures { get; }

        void ModLoadException(Exception loadException);

        void ModLoadException(string message);
    }
}
