﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bronze.Contracts.InjectableServices
{
    public interface ITextSanitizer
    {
        string SanitizeTextForFont(string input);
    }
}
