﻿using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Diplomacy;
using Bronze.Contracts.Data.World;

namespace Bronze.Contracts.InjectableServices
{
    public interface IArmyNotificationBuilder
    {
        Notification BuildAttritionDeathNotification(string locationName, IUnit unit, CellPosition? position);

        Notification BuildArmyLostNotification(Army army, CellPosition position);

        Notification BuildUnitWentHomeNotification(string name, IUnit unit, CellPosition? position);

        Notification BuildArmyDestroyedNotification(Army army, CellPosition position, bool generalDied, bool wasTrapped);
        
        Notification BuildBattleNotification(string battleName, Cell location, PitchedBattleResult battleResult);

        Notification BuildSiegeAssaultNotification(string battleName, Cell settlementLocation, PitchedBattleResult battleResult, bool settlementCaptured, bool playerAttacker, Settlement settlement);
    }
}
