﻿namespace Bronze.Contracts.InjectableServices
{
    public interface IInjectionLifecycleService
    {
        void OnInjectionBuild();

        void OnInjectionDestroy();
    }
}
