﻿using Bronze.Contracts.Data.Diplomacy;
using Bronze.Contracts.Data.World;
using System.Collections.Generic;

namespace Bronze.Contracts.InjectableServices
{
    public interface IWarHelper
    {
        IEnumerable<War> AllWars { get; }

        War StartWar(Tribe aggressor, Tribe defender);

        bool AreAtWar(Tribe playerTribe, Tribe forTribe);

        void EndWar(War war);

        double GetWarScore(Tribe forTribe, Tribe otherTribe);

        bool IsAtWar(Tribe tribe);

        void RecordRaid(Army army, Cell target, War war);
    }
}
