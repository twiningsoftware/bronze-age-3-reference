﻿using System;
using System.Collections.Generic;

namespace Bronze.Contracts.InjectableServices
{
    public interface IAudioService
    {
        void PlaySound(string soundKey);

        void ClearAmbience();

        void AddAmbience(string soundKey);
        
        IEnumerable<Tuple<string, int>> AmbienceDebug();
    }
}
