﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Generation;
using Bronze.Contracts.Data.World;
using System;
using System.Collections.Generic;
using System.Xml.Linq;

namespace Bronze.Contracts.InjectableServices
{
    public interface IXmlReaderUtil
    {
        XElement Element(XDocument document, string descendantName);

        XElement Element(XElement element, string descendantName);

        XElement ElementOrNull(XElement element, string descendantName);

        string AttributeValue(XElement element, string attributeName);

        string OptionalAttributeValue(XElement element, string attributeName, string defaultValue);

        double OptionalAttributeAsDouble(XElement element, string attributeName);

        int AttributeAsInt(XElement element, string attributeName);

        float AttributeAsFloat(XElement element, string attributeName);

        double AttributeAsDouble(XElement element, string attributeName);

        bool AttributeAsBool(XElement element, string attributeName);

        TEnum AttributeAsEnum<TEnum>(XElement element, string attributeName) where TEnum : struct;

        T ObjectReferenceLookup<T>(
            XElement element,
            string attributeName,
            IEnumerable<T> options,
            Func<T, string> matchDataForObject);

        CellAnimationLayer LoadCellSpriteLayer(XElement e);

        CellAnimationLayer LoadCellAnimationLayer(XElement e);
        
        T OptionalObjectReferenceLookup<T>(
            XElement element,
            string attributeName,
            IEnumerable<T> options,
            Func<T, string> matchDataForObject);

        IEnumerable<ITileDisplay> LoadTileDisplays(XElement e);

        ItemRate LoadItemRate(XElement e);

        ItemQuantity LoadItemQuantity(XElement e);
        
        TradeHaulerInfo LoadTradeHaulerInfo(XElement haulerElement);

        IndividualAnimation LoadIndividualAnimation(XElement element);

        WorkerNeed LoadWorkerNeed(XElement constructionElement, Race race);

        InclusionFeature LoadInclusionFeature(XElement element);

        RiverSpawner LoadRiverSpawner(XElement spawnerElement);

        BorderFeatureCondition LoadBorderFeatureCondition(XElement element);

        BorderFeature LoadBorderFeature(XElement element);

        ISeedPoint LoadSeedPoint(
            XElement element,
            RegionFeature[] regionFeatures);

        SeedPointGroup LoadSeedPointGroup(
            XElement element,
            RegionFeature[] regionFeatures);

        IPopNeed LoadPopNeed(XElement element);

        DistrictGenerationLayer LoadDistrictGenerationLayer(XElement element);

        TribeSpawner LoadTribeSpawner(XElement element);

        string SanitizeTextForFont(string input);
    }
}
