﻿using Bronze.Contracts.Data;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.UI;
using System.Numerics;

namespace Bronze.Contracts.InjectableServices
{
    public interface IPlayerDataTracker
    {
        int CurrentPlane { get; set; }
        Vector2 ViewCenter { get; set; }
        Vector2 ScaledViewCenter { get; }
        ViewMode ViewMode { get; set; }

        Tribe PlayerTribe { get; set; }

        Cell SelectedCell { get; set; }
        Cell HoveredCell { get; set; }

        Region ActiveRegion { get; set; }

        Tile HoveredTile { get; set; }
        IStructure SelectedStructure { get; set; }

        Settlement ActiveSettlement { get; set; }

        Army HoveredArmy { get; set; }
        Army SelectedArmy { get; set; }
        
        IMouseMode MouseMode { get; set; }
        
        void LookAt(CellPosition cellPosition);
        void LookAt(TilePosition tilePosition);
    }
}
