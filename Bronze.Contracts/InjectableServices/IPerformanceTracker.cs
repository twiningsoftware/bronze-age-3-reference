﻿using System;
using System.Collections.Generic;

namespace Bronze.Contracts.InjectableServices
{
    public interface IPerformanceTracker
    {
        int FPS { get; set; }
        long UpdateTime { get; set; }
        long LayoutTime { get; set; }
        long RenderTime { get; set; }

        IEnumerable<Tuple<string, long>> GetPoorlyPerformingScripts(int count);
        IEnumerable<Tuple<string, long>> GetPoorlyPerformingUpdates(int count);
        IEnumerable<Tuple<string, long>> GetPoorlyPerformingRendering(int count);
        IEnumerable<Tuple<string, long>> GetPoorlyPerformingCalculations(int count);

        void LogRenderTime(string stepName, long ticks);
        void LogUpdateTime(string stepName, long ticks);
        void LogScriptTime(string stepName, long ticks);
        void LogCalculationTime(string stepName, long ticks);
        void LogSettlementCalculationTime(string settlementName, long ticks);

        IEnumerable<Tuple<string, long>> GetSlowSettlementCalculations(int count);
    }
}
