﻿namespace Bronze.Contracts.InjectableServices
{
    public interface IInjectionProvider
    {
        TService Build<TService>();

        TService BuildNamed<TService>(string name);

        TService[] BuildAll<TService>();
    }
}
