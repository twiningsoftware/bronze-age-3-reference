﻿using Bronze.Contracts.Data.Simulation;

namespace Bronze.Contracts.InjectableServices
{
    public delegate void SimulationEventHandler(ISimulationEvent evt);

    public interface ISimulationEventBus
    {
        event SimulationEventHandler OnSimulationEvent;

        void SendEvent(ISimulationEvent evt);
    }
}
