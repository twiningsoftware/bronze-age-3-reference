﻿using Bronze.Contracts.Data.AI;
using Bronze.Contracts.Data.World;

namespace Bronze.Contracts.InjectableServices
{
    public interface ICityPrefabPlacer
    {
        void QueueForPlacement(
            Settlement settlement, 
            CityPrefab[] prefabs, 
            bool instantBuild,
            CityPrefab[] storageDistFallback);
        
        bool IsPendingPacement(Settlement settlement);
    }
}
