﻿using System;

namespace Bronze.Contracts.InjectableServices
{
    public interface ILogger
    {
        void Error(string message);
        void Error(string message, Exception ex);
        void Debug(string message);
        void Info(string message);
        void Console(string message);

        LogHistory[] GetHistory(int numEntries);
        void ClearHistory();
    }

    public enum LogLevel
    {
        CONSOLE = 0,
        ERROR = 1,
        DEBUG = 2,
        ALL = 3,
    }

    public struct LogHistory
    {
        public string Message { get; }
        public LogLevel LogLevel { get; }

        public LogHistory(string message, LogLevel logLevel)
            : this()
        {
            Message = message;
            LogLevel = logLevel;
        }
    }
}
