﻿using Bronze.Contracts.Data;
using Bronze.Contracts.Data.World;

namespace Bronze.Contracts.InjectableServices
{
    public interface IGenericNotificationBuilder
    {
        Notification BuildSimpleNotification(string icon, string summaryTitle, string summaryBody, string title, string body, CellPosition? lookAt, bool autoOpen = false);

        Notification BuildPersonNotification(string icon, string summaryTitle, string summaryBody, string title, string body, NotablePerson person, CellPosition? lookAt);

        Notification BuildPersonSkillNotification(
            string icon, 
            string summaryTitle, 
            string summaryBody, 
            string title,
            string body, 
            NotablePerson person,
            NotablePersonSkillPlacement skillPlacement,
            CellPosition? lookAt);

        Notification BuildTribeNotification(
            string icon,
            string summaryTitle,
            string summaryBody,
            string title,
            string body,
            Tribe tribe);

        Notification BuildTribeNotification(
            string icon,
            string summaryTitle,
            string summaryBody,
            string title,
            string body,
            Tribe tribeA,
            Tribe tribeB);

        Notification BuildArmyNotification(
            string icon,
            string summaryTitle,
            string summaryBody,
            string title,
            string body,
            Army army);

        Notification BuildBuffNotification(
            string icon, 
            string summaryTitle, 
            string summaryBody, 
            string title, 
            string body, 
            Settlement settlement,
            Buff buff,
            bool autoOpen = false);

        Notification BuildLookAtNotification(
            string icon, 
            string summaryTitle,
            CellPosition lookAt);
    }
}
