﻿using Bronze.Contracts.Help;

namespace Bronze.Contracts.InjectableServices
{
    public interface IHelpManager
    {
        HelpPage StartingPage { get; }

        HelpPage GetPage(string pageId);

        void Clear();

        void AddPage(HelpPage page);

        void CreateAutoPages();
        void OpenHelpDialog();
    }
}
