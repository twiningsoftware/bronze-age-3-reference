﻿using Bronze.Contracts.Data.World;
using System.Collections.Generic;
using System.Numerics;

namespace Bronze.Contracts.InjectableServices
{
    public interface IWorldEffectManager
    {
        IEnumerable<WorldEffect> GetActiveEffects();

        void AddDeathEffect(
            Army army,
            IUnit unit,
            Vector2 worldPosition);
    }
}
