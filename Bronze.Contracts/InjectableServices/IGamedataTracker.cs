﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Generation;
using System.Collections.Generic;

namespace Bronze.Contracts.InjectableServices
{
    public interface IGamedataTracker
    {
        List<Biome> Biomes { get; }
        List<RiverType> RiverTypes { get; }
        List<MidpathType> MidpathTypes { get; }
        List<ICellFeature> CellFeatures { get; }
        List<Trait> Traits { get; }
        List<WorldType> WorldTypes { get; }
        List<Race> Races { get; }
        List<ICellDevelopmentType> CellDevelopments { get; }
        List<IStructureType> StructureTypes { get; }
        List<IUnitType> UnitTypes { get; }
        List<Terrain> Terrain { get; }
        List<Decoration> Decorations { get; }
        List<Item> Items { get; }
        List<BonusType> BonusTypes { get; }
        List<ServiceType> ServiceTypes { get; }
        List<Recipie> Recipies { get; }
        List<MovementType> MovementTypes { get; }
        List<UnitCategory> UnitCategories { get; }
        List<PeoplePart> PeopleParts { get; }
        List<NotablePersonSkill> Skills { get; }
        List<ITransportType> TransportTypes { get; }
        List<RecruitmentSlotCategory> RecruitmentSlotCategories { get; }
        List<ITribeControllerType> ControllerTypes { get; }
        List<HaulerInfo> HaulerInfos { get; }
        List<Cult> Cults { get; }

        void ClearGameData();
    }
}
