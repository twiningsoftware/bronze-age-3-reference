﻿using System;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;

namespace Bronze.Contracts.InjectableServices
{
    public interface INotablePersonGenerationAddon
    {
        void Apply(Random random, Tribe owner, Race race, bool inDynasty, NotablePerson person);
    }
}
