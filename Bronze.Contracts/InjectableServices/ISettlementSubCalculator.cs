﻿using Bronze.Contracts.Data.Simulation;

namespace Bronze.Contracts.InjectableServices
{
    public interface ISettlementSubcalcuator
    {
        void DoCalculations(CalculationResults results);
    }
}
