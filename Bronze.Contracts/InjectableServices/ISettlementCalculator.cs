﻿using Bronze.Contracts.Data.World;
using Bronze.Contracts.Delegates;

namespace Bronze.Contracts.InjectableServices
{
    public interface ISettlementCalculator
    {
        void QueueCalculation(Settlement settlement);//, bool newEconomicActor, bool newStructure);
        void ApplyCalculationResults();
        void StartCalculations();
        void ForceCalculations(ReportStepProgress reportProgress);
    }
}
