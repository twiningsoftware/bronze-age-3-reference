﻿using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using System.Collections.Generic;

namespace Bronze.Contracts.InjectableServices
{
    public interface IWorldManager
    {
        string WorldName { get; }
        string Seed { get; }
        double Month { get; set; }
        string DateString { get; }
        double Hostility { get; }
        double PlaytimeSeconds { get; set; }
        int WorldSize { get; }
        bool IsPreview { get; }
        string[] EdgeAdjacencies { get; }
        List<Trait> WorldTraits { get; }

        List<Tribe> Tribes { get; }
        
        IEnumerable<Plane> Planes { get; }

        void Clear();

        IEnumerable<Cell> GetCells(int planeId, Rectangle viewport);

        Cell GetCell(CellPosition cellPosition);

        Plane GetPlane(int planeId);

        Region GetRegion(int regionId);
    }
}
