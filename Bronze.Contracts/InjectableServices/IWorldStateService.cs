﻿using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;

namespace Bronze.Contracts.InjectableServices
{
    /// <summary>
    /// An interface for any service that remembers state about the world.
    /// </summary>
    public interface IWorldStateService
    {
        /// <summary>
        /// Clear world state.
        /// </summary>
        void Clear();

        /// <summary>
        /// Initialize for a newly loaded or generated world.
        /// </summary>
        /// <param name="worldState">The state of the loaded or generated world.</param>
        void Initialize(WorldState worldState);

        /// <summary>
        /// Save any custom data for this service.
        /// </summary>
        /// <param name="root">The root object for the world.</param>
        void SaveCustomData(SerializedObject worldRoot);

        /// <summary>
        /// Load any custom data for this service.
        /// </summary>
        /// <param name="root">The root object for the world.</param>
        void LoadCustomData(SerializedObject worldRoot);
    }
}
