﻿using System.Xml.Linq;

namespace Bronze.Contracts.InjectableServices
{
    public interface IDataLoader
    {
        string ElementName { get; }

        void Load(XElement element);

        void PostLoad(XElement element);
    }
}
