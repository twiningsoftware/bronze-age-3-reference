﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Generation;
using Bronze.Contracts.Data.World;
using System.Collections.Generic;

namespace Bronze.Contracts.InjectableServices
{
    public interface INameSource
    {
        void InitializeForSeed(int seed);

        string MakeWorldName(WorldType worldType);

        string MakeTribeName(Race tribeRace);

        string MakeSettlementName(Race race, Cell cell);

        string MakeArmyName(Race race);

        string MakeRegionName(string regionNamePattern, Region region);

        string MakePersonName(Race race, bool isMale);

        string EvaluateDialog(
            string pattern,
            List<string> context,
            Dictionary<string, string> variables);
    }
}
