﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using System.Collections.Generic;

namespace Bronze.Contracts.InjectableServices
{
    public interface IDiplomacyHelper
    {
        string GenerateText(Tribe aiTribe, string pattern, IEnumerable<string> additionalContext = null, Dictionary<string,string> additionalVariables = null);

        double ScoreConvoCheck(NotablePersonStatType statType, Tribe aiTribe);

        int GetInfluenceFor(Tribe tribe);

        int GetTrustFor(Tribe tribe);

        double GetRelativePower(Tribe from, Tribe to);

        double GetTrustMod(Tribe aiTribe);

        int SumTreatyValues(Tribe aiTribe, Tribe playerTribe);
    }
}
