﻿using Bronze.Contracts.UI;

namespace Bronze.Contracts.InjectableServices
{
    public interface IDialogManager
    {
        IModal CurrentModal { get; }
        
        void PushModal<TModal>() where TModal : IModal;
        void PushModal<TModal, TInit>(TInit initValue) where TModal : IModal<TInit>;

        void PopModal();
        void PopAllModals();
    }
}
