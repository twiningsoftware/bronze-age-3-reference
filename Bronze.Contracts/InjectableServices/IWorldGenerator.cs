﻿using Bronze.Contracts.Data.Generation;
using Bronze.Contracts.Delegates;
using System;

namespace Bronze.Contracts.InjectableServices
{
    public delegate void GenerationFinishedHandler(
        WorldgenParameters parameters, 
        TimeSpan generationTime);

    public interface IWorldGenerator
    {
        event GenerationFinishedHandler OnGenerationFinished;

        void GenerateNewWorld(
            ReportProgress reportProgress,
            WorldgenParameters parameters);
    }
}
