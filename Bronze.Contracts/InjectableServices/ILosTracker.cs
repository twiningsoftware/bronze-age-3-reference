﻿using Bronze.Contracts.Data.World;
using System.Collections.Generic;

namespace Bronze.Contracts.InjectableServices
{
    public interface ILosTracker
    {
        bool GodVision { get; set; }
        
        void ExploreAround(Tribe tribe, Region region);

        IEnumerable<Cell> GetVisionFor(Army army);

        void SetLosCopy(Tribe from, Tribe to);

        void RemoveLosCopy(Tribe from, Tribe to);
    }
}
