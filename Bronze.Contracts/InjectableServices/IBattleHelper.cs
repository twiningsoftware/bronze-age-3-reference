﻿using Bronze.Contracts.Data.World;
using System.Collections.Generic;

namespace Bronze.Contracts.InjectableServices
{
    public interface IBattleHelper
    {
        void RunBattle(Army army);

        void DoSkirmishing(Army army, Army targetArmy);

        void DoSiegeAssault(Settlement settlement, Tribe initializer);

        void SetStunned(IEnumerable<Army> armies);

        void SetRouted(IEnumerable<Army> routingArmies, double durationFactor);
    }
}
