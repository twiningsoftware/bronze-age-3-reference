﻿namespace Bronze.Contracts.InjectableServices
{
    public interface IWorldMinimapDrawer
    {
        void PromptRedraw(int planeId);
    }
}
