﻿using System;
using System.Collections.Generic;
using Bronze.Contracts.Data.AI;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;

namespace Bronze.Contracts.InjectableServices
{
    public interface IAiSpy
    {
        void ToggleFor(Tribe tribe);

        ISettlementAiSpy StartSettlement(
            string settlementRole,
            Settlement settlement, 
            Dictionary<Item, double> idealNetProduction, 
            Dictionary<Caste, int> availablePopByCaste, 
            Dictionary<MovementType, int> tradeCapacity);
    }

    public interface ISettlementAiSpy : IDisposable
    {
        IChoiceAiSpy StartChoice(string choiceType);
        void Log(string v);
    }

    public interface IChoiceAiSpy : IDisposable
    {
        void Pick(ICellDevelopmentType cellDevelopment, Recipie recipie);
        void Pick(ICellDevelopmentType cellDevelopment);
        void PickForClearing(ICellDevelopmentType clearanceDevelopment, ICellDevelopmentType developmentGoal);
        void Options(string name, IEnumerable<ICellDevelopmentType> developmentOptions);
        void Options(string name, IEnumerable<CityPrefab> prefabOptions);
        void Log(string v);
        void Options(Tuple<IUnitType, UnitEquipmentSet>[] unitOptions);
        void Pick(IUnitType unitType, UnitEquipmentSet unitEquipment);
        void Pick(CityPrefab[] prefabOptions);
    }
}
