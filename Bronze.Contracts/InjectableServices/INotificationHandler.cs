﻿using Bronze.Contracts.Data;

namespace Bronze.Contracts.InjectableServices
{
    public interface INotificationHandler
    {
        string Name { get; }

        void Handle(Notification notification);

        void HandleIgnored(Notification notification);
    }
}
