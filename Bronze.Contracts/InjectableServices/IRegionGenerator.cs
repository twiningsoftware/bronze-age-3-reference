﻿using Bronze.Contracts.Data.World;
using System;
using System.Collections.Generic;

namespace Bronze.Contracts.InjectableServices
{
    public interface IRegionGenerator
    {
        string RegionStyle { get; }

        Dictionary<CellPosition, List<Cell>> FormRegions(Random random, Cell[,] cells);
    }
}
