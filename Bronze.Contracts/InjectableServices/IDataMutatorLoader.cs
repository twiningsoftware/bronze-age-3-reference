﻿using Bronze.Contracts.Data.Gamedata;
using System.Collections.Generic;
using System.Xml.Linq;

namespace Bronze.Contracts.InjectableServices
{
    public interface IDataMutatorLoader
    {
        void LoadMutators(XElement element, List<IDataMutator> dataMutators);
    }
}
