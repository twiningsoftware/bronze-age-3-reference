﻿namespace Bronze.Contracts.InjectableServices
{
    public interface IMusicPlayer
    {
        bool PlayMenuMusic { set; }

        void SetDangerLevel(double danger);
    }
}
