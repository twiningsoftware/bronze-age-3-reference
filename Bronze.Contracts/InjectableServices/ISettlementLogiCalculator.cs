﻿using System.Collections.Generic;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.World;

namespace Bronze.Contracts.InjectableServices
{
    public interface ISettlementLogiCalculator
    {
        IEnumerable<DeliveryRoute> CalculateDeliveryRoutes(CalculationResults current);

        IEnumerable<IEconomicActor> FloodFillToActors(Tile[] from, Trait[] reqiredTraits, double maximumDistance);

        Dictionary<Tile, Tile> FloodFillOut(Tile[] logiDoors, HaulerInfo hauler);
    }
}
