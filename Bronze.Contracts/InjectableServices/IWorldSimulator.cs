﻿namespace Bronze.Contracts.InjectableServices
{
    public delegate void OnUpdateHandler();

    public interface IWorldSimulator
    {
        event OnUpdateHandler OnUpdate;

        void StepSimulation(float elapsedSeconds);
    }
}
