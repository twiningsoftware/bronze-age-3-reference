﻿using Bronze.Contracts.Data.World;

namespace Bronze.Contracts.InjectableServices
{
    public interface IGrazingManager
    {
        bool CanGraze(Cell cell, IGrazingUnit grazingUnit);
        void DoGrazing(Army army, IGrazingUnit grazingUnit, double deltaMonths);
    }
}
