﻿namespace Bronze.Contracts
{
    public interface IInjectionModule
    {
        void RegisterInjections(IInjector injector);
    }
}
