﻿namespace Bronze.Contracts.Delegates
{
    public delegate void FpsHandler(int newFps);
    public delegate void PerformanceHandler(long updateTime);
    public delegate void NamedPerformanceHandler(string serviceName, long updateTime);
}
