﻿namespace Bronze.Contracts.UI
{
    public interface IUiWidget : IUiElement
    {
        bool IsActive { get; }
    }
}
