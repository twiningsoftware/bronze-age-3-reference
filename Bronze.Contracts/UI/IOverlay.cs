﻿namespace Bronze.Contracts.UI
{
    public interface IOverlay : IMapInteractionComponent
    {
        string Name { get; }

        bool IsAvailble { get; }

        bool IsActive { get; set; }
    }
}