﻿using Bronze.Contracts.Data;

namespace Bronze.Contracts.UI
{
    public interface IMouseMode
    {
        void OnCancel();
        
        bool IsValidFor(ViewMode viewMode);
    }
}
