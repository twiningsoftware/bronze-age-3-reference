﻿using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Contracts.UI
{
    public interface IModal
    {
        bool PausesGame { get; }

        void OnLoad();
        void OnUnload();

        void Update(
            float elapsedSeconds,
            InputState inputState,
            IAudioService audioService);

        void Layout(IDrawingService drawingService);

        void Draw(
            float elapsedSeconds,
            IDrawingService drawingService);
    }

    public interface IModal<TInit> : IModal
    {
        void Initialize(TInit initValue);
    }
}
