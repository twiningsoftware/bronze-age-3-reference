﻿using Bronze.Contracts.Data;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System.Collections.Generic;

namespace Bronze.Contracts.UI
{
    public interface IMapInteractionComponent
    {
        void Update(
            InputState inputState, 
            float elapsedSeconds);

        void DrawWorld(
            IDrawingService drawingService,
            IEnumerable<Cell> cellsInView,
            ViewMode viewMode);

        void DrawTiles(
            IDrawingService drawingService,
            IEnumerable<Tile> tilesInView);
    }
}
