﻿using System.Collections.Generic;

namespace Bronze.Contracts.UI
{
    public interface IStackUiElement : IUiElement
    {
        List<IUiElement> Children { get; }
    }
}
