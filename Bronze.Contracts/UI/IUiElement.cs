﻿using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Contracts.UI
{
    public interface IUiElement
    {
        bool NeedsRelayout { get; }

        Rectangle Bounds { get; }
        
        void DoLayout(
            IDrawingService drawingService,
            Rectangle parentBounds);

        void Draw(IDrawingService drawingService, float layer);

        void Update(
            InputState inputState,
            IAudioService audioService,
            float elapsedSeconds);
    }
}
