﻿using Bronze.Contracts.InjectableServices;
using System;
using System.Threading.Tasks;

namespace Bronze.Contracts
{
    public abstract class AbstractBackgroundCalculatorSubSimulator : ISubSimulator
    {
        private readonly IGameInterface _gameInterface;
        private Task _calculationTask;

        protected AbstractBackgroundCalculatorSubSimulator(IGameInterface gameInterface)
        {
            _gameInterface = gameInterface;
        }

        public virtual void StepSimulation(double elapsedMonths)
        {
            ApplyResults();

            if (_calculationTask == null
                || _calculationTask.IsCompleted
                || _calculationTask.IsFaulted
                || _calculationTask.IsCanceled)
            {
                _calculationTask = Task.Run(() =>
                {
                    try
                    {
                        StartCalculations();
                    }
                    catch (Exception ex)
                    {
                        OnException(ex);
                        throw new Exception(ex.Message, ex);
                    }
                });
            }
        }

        protected abstract void ApplyResults();

        protected abstract void StartCalculations();
        
        protected virtual void OnException(Exception ex)
        {
            _gameInterface.OnException(ex);
        }
    }
}
