﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bronze.Contracts.Data;

namespace Bronze.Contracts
{
    public static class Extensions
    {
        public static T Choose<T>(this Random rnd, IEnumerable<T> set)
        {
            var array = (set as T[]) ?? set.ToArray();

            if (array.Length < 1)
            {
                throw new ArgumentException("Cannot pick item from empty set.");
            }

            var r = rnd.Next(array.Length);

            return array[r];
        }

        public static T ChooseByLikelyhood<T>(this Random rnd, IEnumerable<LikelyhoodFor<T>> set)
        {
            var array = (set as LikelyhoodFor<T>[]) ?? set.ToArray();

            if (array.Length < 1)
            {
                throw new ArgumentException("Cannot pick item from empty set.");
            }

            var total = array.Sum(a => a.Likelyhood);
            var r = rnd.Next(total);

            foreach(var item in array)
            {
                if(r < item.Likelyhood)
                {
                    return item.Value;
                }
                else
                {
                    r -= item.Likelyhood;
                }
            }
            
            return array.Last().Value;
        }

        public static IEnumerable<T> ChooseCount<T>(this Random rnd, IEnumerable<T> set, int count)
        {
            var array = (set as T[]) ?? set.ToArray();

            if (array.Length < 1)
            {
                throw new ArgumentException("Cannot pick item from empty set.");
            }

            for (var i = 0; i < count; i++)
            {

                var r = rnd.Next(array.Length);

                yield return array[r];
            }
        }

        public static float Variance(this Random rnd, float baseValue, float maxVariance)
        {
            return (float)(baseValue + (rnd.NextDouble() * 2 - 1) * maxVariance);
        }

        public static IEnumerable<T> Yield<T>(this T self)
        {
            yield return self;
        }

        public static Queue<T> ToQueue<T>(this IEnumerable<T> source)
        {
            return new Queue<T>(source);
        }

        public static T RandomOrDefault<T>(this IEnumerable<T> source, Random random)
        {
            var a = source.ToArray();

            if(a.Length < 1)
            {
                return default(T);
            }

            return a[random.Next(a.Length)];
        }

        public static SafeDictionary<TKey, TValue> ToSafeDictionary<T, TKey, TValue>(
            this IEnumerable<T> source, 
            Func<T, TKey> keyFunc, 
            Func<T, TValue> valueFunc)
        {
            var dictionary = new SafeDictionary<TKey, TValue>();

            foreach(var val in source)
            {
                dictionary[keyFunc(val)] = valueFunc(val);
            }

            return dictionary;
        }

        public static SafeDictionary<TKey, TValue> ToSafeDictionary<TKey, TValue>(
            this IDictionary<TKey, TValue> source)
        {
            var dictionary = new SafeDictionary<TKey, TValue>();

            foreach (var key in source.Keys)
            {
                dictionary[key] = source[key];
            }

            return dictionary;
        }

        public static T GetLowest<T>(this IEnumerable<T> source, Func<T, double> getValue)
        {
            var lowest = default(T);
            var lowestVal = double.MaxValue;

            foreach(var item in source)
            {
                var val = getValue(item);

                if(val < lowestVal)
                {
                    lowest = item;
                    lowestVal = val;
                }
            }

            return lowest;
        }

        // Adapted from http://stackoverflow.com/a/1262619
        public static void Shuffle<T>(this Random rnd, List<T> list)
        {
            var n = list.Count;
            while (n > 1)
            {
                n--;
                var k = rnd.Next(n + 1);
                var value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        // Adapted from http://stackoverflow.com/a/1262619
        public static void Shuffle<T>(this Random rnd, T[] list)
        {
            var n = list.Length;
            while (n > 1)
            {
                n--;
                var k = rnd.Next(n + 1);
                var value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        // From https://msdn.microsoft.com/en-us/library/kdcak6ye%28v=vs.110%29.aspx?f=255&MSPPError=-2147217396
        public static string ToAsciiString(this string value)
        {
            if(string.IsNullOrEmpty(value))
            {
                return string.Empty;
            }

            Encoding ascii = Encoding.ASCII;
            Encoding unicode = Encoding.Unicode;

            // Convert the string into a byte array.
            byte[] unicodeBytes = unicode.GetBytes(value);

            // Perform the conversion from one encoding to the other.
            byte[] asciiBytes = Encoding.Convert(unicode, ascii, unicodeBytes);

            // Convert the new byte[] into a char[] and then into a string.
            char[] asciiChars = new char[ascii.GetCharCount(asciiBytes, 0, asciiBytes.Length)];
            ascii.GetChars(asciiBytes, 0, asciiBytes.Length, asciiChars, 0);
            string asciiString = new string(asciiChars);

            return asciiString;
        }

        // Adapted from https://stackoverflow.com/a/1183153
        public static IEnumerable<T> ToEnumerable<T>(this Array target)
        {
            foreach (var item in target)
            {
                yield return (T)item;
            }
        }
        
        public static bool ContainsAll<T>(this IEnumerable<T> target, IEnumerable<T> toCheck)
        {
            return target.Intersect(toCheck).Count() == toCheck.Count();
        }

        public static bool ContainsAny<T>(this IEnumerable<T> target, IEnumerable<T> toCheck)
        {
            return target.Intersect(toCheck).Any();
        }
    }
}
