﻿using System.Numerics;

namespace Bronze.Contracts.Data
{
    public struct Point
    {
        public readonly int X;
        public readonly int Y;
        
        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }

        public Vector2 ToVector2()
        {
            return new Vector2(X, Y);
        }

        public static Vector2 operator -(Point a, Point b)
        {
            return new Vector2(
                a.X - b.X,
                a.Y - b.Y);
        }

        public Point Relative(int dx, int dy)
        {
            return new Point(X - dx, Y - dy);
        }
    }
}
