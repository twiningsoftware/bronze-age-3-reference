﻿namespace Bronze.Contracts.Data
{
    public enum MinimapColor
    {
        None,
        Black,
        Dark,
        Brown,
        Red,
        Orange,
        Tan,
        Yellow,
        White,
        DarkBlue,
        MediumBlue,
        LightBlue,
        DarkGrey,
        MediumGrey,
        LightGrey,
        LightGreen,
        DarkGreen
    }
}
