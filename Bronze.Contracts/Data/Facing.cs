﻿namespace Bronze.Contracts.Data
{
    public enum Facing
    {
        East,
        NorthEast,
        North,
        NorthWest,
        West,
        SouthWest,
        South,
        SouthEast
    }
}
