﻿using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using Bronze.Contracts.UI;

namespace Bronze.Contracts.Data
{
    public class InputState
    {
        private const int MAX_KEY = 255;
        
        private bool[] _previousKeyStates;
        private bool[] _currentKeyStates;
        private int _lastMouseScrollWheelValue;

        public bool MouseReleased { get; private set; }
        public bool MousePressed { get; private set; }
        public bool MouseDown { get; private set; }

        public bool MouseRightReleased { get; private set; }
        public bool MouseRightPressed { get; private set; }
        public bool MouseRightDown { get; private set; }

        public bool MouseInWindow { get; set; }
        public bool MouseOverWidget { get; set; }

        public int MouseScrollDelta { get; private set; }

        public Vector2 MouseDelta { get; private set; }
        public Point MousePosition { get; private set; }
        public bool AnyKeyPressed { get; private set; }

        public IUiElement FocusedElement { get; set; }

        public InputState()
        {
            _previousKeyStates = new bool[MAX_KEY];
            _currentKeyStates = new bool[MAX_KEY];
        }
        
        public void Update(
            IEnumerable<KeyCode> pressedKeys, 
            bool leftMouseDown, 
            bool rightMouseDown,
            Point mousePosition,
            int mouseScrollWheelValue)
        {
           _previousKeyStates = _currentKeyStates;
            _currentKeyStates = new bool[MAX_KEY];
            
            foreach (var key in pressedKeys)
            {
                _currentKeyStates[(int)key] = true;
            }
            
            MouseReleased = MouseDown && !leftMouseDown;
            MousePressed = !MouseDown && leftMouseDown;
            MouseDown = leftMouseDown;

            MouseRightReleased = MouseRightDown && !rightMouseDown;
            MouseRightPressed = !MouseRightDown && rightMouseDown;
            MouseRightDown = rightMouseDown;


            MouseDelta = MousePosition - mousePosition;

            MouseScrollDelta = mouseScrollWheelValue - _lastMouseScrollWheelValue;
            _lastMouseScrollWheelValue = mouseScrollWheelValue;

            MousePosition = mousePosition;

            AnyKeyPressed = pressedKeys.Any();
        }

        public bool JustPressed(KeyCode key)
        {
            return _currentKeyStates[(int)key] && !_previousKeyStates[(int)key];
        }

        public bool KeyDown(KeyCode key)
        {
            return _currentKeyStates[(int)key];
        }
    }
}
