﻿using System.Collections.Generic;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.UI;

namespace Bronze.Contracts.Data.Simulation
{
    public interface IEconomicActor
    {
        string Id { get; }
        string TypeId { get;  }
        string Name { get; }
        WorkerNeed WorkerNeed { get; }
        int WorkerSupply { get; set; }
        double WorkerPercent { get; }
        double UpkeepPercent { get; set; }
        bool IsRemoved { get; }
        IEnumerable<Tile> DeliveryArea { get; set; }

        Settlement Settlement { get; }
        IEnumerable<Trait> NetTraits { get; }
        IEnumerable<ItemQuantity> ConstructionCost { get; }

        bool UnderConstruction { get; set; }
        double ConstructionProgress { get; set; }
        double ConstructionMonths { get; }

        bool IsUpgrading { get; }
        double UpgradeProgress { get; set; }
        double UpgradeMonths { get; }
        
        ItemInventory Inventory { get; }
        int InventoryCapacity { get; }
        IntIndexableLookup<BonusType, double> CurrentBonuses { get; }

        bool IsCellLevel { get; }

        IntIndexableLookup<Item, bool> ItemsProduced { get; }
        IntIndexableLookup<Item, bool> ItemsConsumed { get; }
        IntIndexableLookup<Item, double> IncomingItems { get; }

        List<LogiHauler> IncomingHaulers { get; }
        List<LogiHauler> OutgoingHaulers { get; }

        IEnumerable<Recipie> AvailableRecipies { get; }
        Recipie ActiveRecipie { get; set; }
        FactoryState FactoryState { get; set; }
        List<Item> MissingItems { get; }
        IntIndexableLookup<Item, double> PerItemSatisfaction { get; }
        IEnumerable<HaulerInfo> LogiSourceHaulers { get; }
        List<DeliveryRoute> DeliveryRoutes { get; }

        List<IServiceProviderActor> ServiceProviders { get; }

        IEnumerable<ItemRate> NeededInputs { get; }
        IEnumerable<ItemRate> ExpectedOutputs { get; }
        IEnumerable<ItemRate> UpkeepInputs { get; }
        IEnumerable<ItemQuantity> UpgradeCost { get; }
        double ProductionCheckWait { get; set; }
        double ItemTransmissionWait { get; set; }
        double ProductionProgress { get; set; }

        Tile[] CalculateLogiDoors(IEnumerable<MovementType> movementTypes);

        void DoUpdate(double deltaMonths);

        void OnConstructionFinished();
        void OnProductionStarted();
        void FinishUpgrading();

        IStackUiElement BuildHoverInfoColumn(bool playerOwns, bool playerSees);

        double NeedOf(Item item);
    }
}
