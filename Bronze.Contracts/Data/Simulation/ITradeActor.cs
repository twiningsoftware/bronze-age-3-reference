﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Trade;
using System.Collections.Generic;

namespace Bronze.Contracts.Data.Simulation
{
    public interface ITradeActor : IEconomicActor
    {
        MovementType TradeType { get; }
        double TradeCapacity { get; }
        TradeRoute TradeRoute { get; set; }
        IEnumerable<TradeCellExit> CellExits { get; }
        TradeHaulerInfo TradeHauler { get; }
        
        void OnTradePathChanged();
    }
}
