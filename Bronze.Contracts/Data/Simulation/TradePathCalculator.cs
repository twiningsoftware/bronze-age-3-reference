﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Priority_Queue;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Contracts.Data.Simulation
{
    public class TradePathCalculator
    {
        private Tribe _tribe;
        private MovementType _movementType;
        private ITradeActor _fromActor;
        private ITradeReciever _toActor;
        
        public bool PathSuccessful { get; private set; }
        public bool PathFinished { get; private set; }
        public List<Cell> Path { get; private set; }
        public bool RecalculateOnFailure { get; set; }
        
        private readonly SimplePriorityQueue<Cell, double> _open;
        private readonly HashSet<Cell> _closed;
        private readonly Dictionary<Cell, Cell> _cameFrom;
        private readonly Dictionary<Cell, double> _distTo;
        
        public TradePathCalculator(Tribe tribe, MovementType movementType, ITradeActor fromActor, ITradeReciever toActor)
        {
            _tribe = tribe;
            _movementType = movementType;
            _fromActor = fromActor;
            _toActor = toActor;
            
            PathSuccessful = false;
            PathFinished = false;
            Path = new List<Cell>();
            RecalculateOnFailure = true;

            _open = new SimplePriorityQueue<Cell, double>();
            _closed = new HashSet<Cell>();
            _cameFrom = new Dictionary<Cell, Cell>();
            _distTo = new Dictionary<Cell, double>();

            ResetPathing();
        }

        private void ResetPathing()
        {
            PathSuccessful = false;
            PathFinished = false;
            Path.Clear();

            _open.Clear();
            _closed.Clear();
            _cameFrom.Clear();
            _distTo.Clear();

            _fromActor.OnTradePathChanged();
            _toActor.OnTradePathChanged();

            foreach (var cellExit in _fromActor.CellExits)
            {
                _open.Enqueue(cellExit.Cell, 0);
                _cameFrom.Add(cellExit.Cell, cellExit.FromCell);
                _distTo.Add(cellExit.Cell, 0);

                if (cellExit.FromCell != null && !_cameFrom.ContainsKey(cellExit.FromCell))
                {
                    _cameFrom.Add(cellExit.FromCell, null);
                    _distTo.Add(cellExit.FromCell, 0);
                    _closed.Add(cellExit.FromCell);
                }
                
            }
        }

        public void Update()
        {
            if (!PathIsValid() && RecalculateOnFailure)
            {
                ResetPathing();
            }

            if (PathFinished)
            {
                return;
            }
            
            const int iterationLimit = 100;

            IteratePath(iterationLimit);
        }

        private void IteratePath(int iterationLimit)
        {

            for (var i = 0; i < iterationLimit && !PathFinished && _open.Any(); i++)
            {
                var current = _open.Dequeue();
                _closed.Add(current);

                if (_toActor.CellExits.Any(ce => ce.Cell == current))
                {
                    var extraCell = _toActor.CellExits
                        .Where(ce => ce.Cell == current)
                        .Select(ce => ce.FromCell)
                        .FirstOrDefault();

                    ConstructPath(current, extraCell);
                    PathSuccessful = true;
                    PathFinished = true;

                    _fromActor.Settlement.SetCalculationFlag("trade pathing finished");
                    return;
                }

                var neighbors = current.OrthoNeighbors
                    .Where(n => _tribe.IsExplored(n)
                        && n.Traits.ContainsAll(_movementType.RequiredTraits)
                        && !n.Traits.ContainsAny(_movementType.PreventedBy)
                        && (n.Owner == null || n.Owner.AllowsAccess(_fromActor.Settlement.Owner) || n.Owner.AllowsAccess(_toActor.Settlement.Owner)))
                    .Where(n => !_closed.Contains(n))
                    .ToArray();

                foreach (var neighbor in neighbors)
                {
                    var dist = _distTo[current] + 1;

                    if (_open.Contains(neighbor))
                    {
                        if (dist < _distTo[neighbor])
                        {
                            _distTo[neighbor] = dist;
                            _cameFrom[neighbor] = current;
                            _open.UpdatePriority(neighbor, dist);
                        }
                    }
                    else
                    {
                        _distTo.Add(neighbor, dist);
                        _cameFrom.Add(neighbor, current);
                        _open.Enqueue(neighbor, dist);
                    }
                }
            }

            if (!_open.Any())
            {
                // Was successful, now isn't
                if (PathSuccessful)
                {
                    _fromActor.Settlement.SetCalculationFlag("trade pathing finished");
                }

                PathFinished = true;
                PathSuccessful = false;
            }
        }

        private bool PathIsValid()
        {
            if(!PathFinished)
            {
                return true;
            }

            if (Path.Any())
            {

                if (_fromActor is ITradeActor fromTrader
                    && !fromTrader.CellExits.Any(ce => (ce.FromCell ?? ce.Cell) == Path.First()))
                {
                    return false;
                }

                if (_toActor is ITradeActor toTrader
                    && !toTrader.CellExits.Any(ce => (ce.FromCell ?? ce.Cell) == Path.Last()))
                {
                    return false;
                }
            }
            
            return PathSuccessful 
                // Exclude first and last cells, as coastal cities might not be boat passable
                && Path.Except(Path.First().Yield())
                    .Except(Path.Last().Yield())
                    .All(c => c != null 
                        && c.Traits.ContainsAll(_movementType.RequiredTraits)
                        && !c.Traits.ContainsAny(_movementType.PreventedBy));
        }

        private void ConstructPath(Cell current, Cell extraCell)
        {
            var path = new List<Cell>();
            while (current != null)
            {
                path.Add(current);
                current = _cameFrom[current];
            }
            path.Reverse();

            foreach(var cell in path)
            {
                Path.Add(cell);
            }

            if(extraCell != null)
            {
                Path.Add(extraCell);
            }

            _fromActor.OnTradePathChanged();
            _toActor.OnTradePathChanged();
        }

        public void CalculateToEnd()
        {
            ResetPathing();
            IteratePath(int.MaxValue);
        }
    }
}
