﻿using Bronze.Contracts.Data.Trade;
using System.Collections.Generic;

namespace Bronze.Contracts.Data.Simulation
{
    public interface ITradeReciever : IEconomicActor
    {
        List<TradeRoute> TradeRoutes { get; }

        IEnumerable<TradeCellExit> CellExits { get; }

        void OnTradePathChanged();

        void OnTradeRoutesChanged();
    }
}
