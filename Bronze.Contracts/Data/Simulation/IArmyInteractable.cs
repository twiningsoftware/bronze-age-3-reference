﻿using Bronze.Contracts.Data.World;

namespace Bronze.Contracts.Data.Simulation
{
    public interface IArmyInteractable
    {
        string InteractVerb { get; }
        bool InteractAdjacent { get; } 
        bool InteractWithForeign { get; }
        Cell Cell { get; }

        void DoInteraction(Army interactingArmy);
    }
}
