﻿using Bronze.Contracts.Data.World;

namespace Bronze.Contracts.Data.Simulation.Events
{
    public class NotablePersonMarriageSimulationEvent : ISimulationEvent
    {
        public NotablePerson LocalSpouse { get; }
        public NotablePerson IncomingSpouse { get; }

        public NotablePersonMarriageSimulationEvent(
            NotablePerson localSpouse,
            NotablePerson incomingSpouse)
        {
            LocalSpouse = localSpouse;
            IncomingSpouse = incomingSpouse;
        }
    }
}
