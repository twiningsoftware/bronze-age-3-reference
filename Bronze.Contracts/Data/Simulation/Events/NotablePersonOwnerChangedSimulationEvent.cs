﻿using Bronze.Contracts.Data.World;

namespace Bronze.Contracts.Data.Simulation.Events
{
    public class NotablePersonOwnerChangedSimulationEvent : ISimulationEvent
    {
        public NotablePerson Person { get; set; }
        public Tribe OldOwner { get; set; }
        public Tribe NewOwner { get; set; }
    }
}
