﻿using Bronze.Contracts.Data.World;

namespace Bronze.Contracts.Data.Simulation.Events
{
    public class NotablePersonDiedSimulationEvent : ISimulationEvent
    {
        public NotablePerson Person { get; }

        public NotablePersonDiedSimulationEvent(NotablePerson person)
        {
            Person = person;
        }
    }
}
