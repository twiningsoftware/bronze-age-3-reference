﻿using Bronze.Contracts.Data.World;

namespace Bronze.Contracts.Data.Simulation.Events
{
    public class SettlementOwnerChangedSimulationEvent : ISimulationEvent
    {
        public Settlement Settlement { get; set; }
        public Tribe OldOwner { get; set; }
        public Tribe NewOwner { get; set; }
    }
}
