﻿using Bronze.Contracts.Data.World;

namespace Bronze.Contracts.Data.Simulation.Events
{
    public class CellExploredSimulationEvent : ISimulationEvent
    {
        public Cell Cell { get; }

        public CellExploredSimulationEvent(Cell cell)
        {
            Cell = cell;
        }
    }
}
