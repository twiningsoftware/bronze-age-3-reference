﻿using System.Numerics;
using Bronze.Contracts.Data.World;

namespace Bronze.Contracts.Data.Simulation.Events
{
    public class UnitDiedSimulationEvent : ISimulationEvent
    {
        public Army Army { get; }
        public IUnit Unit { get; }
        public Vector2? LastDrawPosition { get; }

        public UnitDiedSimulationEvent(Army army, IUnit unit, Vector2? lastDrawPosition)
        {
            Army = army;
            Unit = unit;
            LastDrawPosition = lastDrawPosition;
        }
    }
}
