﻿using Bronze.Contracts.Data.World;

namespace Bronze.Contracts.Data.Simulation.Events
{
    public class PopKilledOnWorldSimulationEvent : ISimulationEvent
    {
        public Pop Pop { get; }
        public Cell Cell { get; }
        
        public PopKilledOnWorldSimulationEvent(Pop pop, Cell cell)
        {
            Pop = pop;
            Cell = cell;
        }
    }
}
