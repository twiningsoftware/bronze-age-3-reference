﻿using Bronze.Contracts.Data.Diplomacy;

namespace Bronze.Contracts.Data.Simulation.Events
{
    public class BattleOccurredSimulationEvent : ISimulationEvent
    {
        public PitchedBattleResult BattleResult { get; private set; }

        public BattleOccurredSimulationEvent(PitchedBattleResult battleResult)
        {
            BattleResult = battleResult;
        }
    }
}
