﻿using Bronze.Contracts.Data.World;
using System.Collections.Generic;

namespace Bronze.Contracts.Data.Simulation
{
    public interface IEconomicActorType
    {
        IEnumerable<ItemQuantity> ConstructionCost { get; }
    }
}
