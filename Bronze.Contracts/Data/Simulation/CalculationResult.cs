﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Contracts.Data.Simulation
{
    public class CalculationResults
    {
        public Settlement Settlement { get; }
        public IStructure[] Structures { get; }
        public ICellDevelopment[] CellDevelopments { get; }
        public Pop[] Population { get; }

        public IEconomicActor[] EconomicActors { get; set; }
        public IEconomicActor[] ActiveEconomicActors { get; set; }
        public List<DeliveryRoute> DeliveryRoutes { get; }
        public List<Action> ApplyResultsFunctions { get; }
        public Dictionary<Pop, IHousingProvider> NewHousingAssignments { get; }
        public Dictionary<Caste, int> HousingByCaste { get; }
        public Dictionary<Caste, int> PopulationByCaste { get; }
        public Dictionary<RecruitmentSlotCategory, int> RecruitmentSlotsMax { get; }
        public Dictionary<Caste, int> JobsByCaste { get; }
        public IntIndexableLookup<Item, bool> ItemAvailability { get; }
        public Dictionary<IEconomicActor, IEnumerable<IEconomicActor>> ServiceProviders { get; }
        public Dictionary<IEconomicActor, Tile[]> DeliveryAreas { get; }
        public Dictionary<IEconomicActor, IntIndexableLookup<BonusType, double>> ActorBonuses { get; }
        public IntIndexableLookup<Cult, List<Pop>> CultMembership { get; }

        public CalculationResults(Settlement settlement)
        {
            Settlement = settlement;
            Structures = settlement.Structures.ToArray();
            CellDevelopments = settlement.Region.Cells
                .Where(c => c.Development != null)
                .Select(c => c.Development)
                .ToArray();
            Population = settlement.Population.ToArray();
            DeliveryRoutes = new List<DeliveryRoute>();
            EconomicActors = new IEconomicActor[0];
            ActiveEconomicActors = new IEconomicActor[0];
            ApplyResultsFunctions = new List<Action>();
            NewHousingAssignments = new Dictionary<Pop, IHousingProvider>();
            HousingByCaste = new Dictionary<Caste, int>();
            PopulationByCaste = new Dictionary<Caste, int>();
            RecruitmentSlotsMax = new Dictionary<RecruitmentSlotCategory, int>();
            JobsByCaste = new Dictionary<Caste, int>();
            ItemAvailability = new IntIndexableLookup<Item, bool>();
            ServiceProviders = new Dictionary<IEconomicActor, IEnumerable<IEconomicActor>>();
            DeliveryAreas = new Dictionary<IEconomicActor, Tile[]>();
            ActorBonuses = new Dictionary<IEconomicActor, IntIndexableLookup<BonusType, double>>();
            CultMembership = new IntIndexableLookup<Cult, List<Pop>>();
        }
    }
}
