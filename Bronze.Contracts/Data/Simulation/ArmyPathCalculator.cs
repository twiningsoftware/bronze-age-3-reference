﻿using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Priority_Queue;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Contracts.Data.Simulation
{
    public class ArmyPathCalculator
    {
        private Army _army;

        private Cell _start;
        private bool _pathAdjacent;
        public Cell Destination { get; private set; }
        public bool PathSuccessful { get; private set; }
        public bool PathFinished { get; private set; }
        public Queue<Cell> Path { get; private set; }
        
        private readonly SimplePriorityQueue<Cell, double> _open;
        private readonly IWorldManager _worldManager;
        
        private readonly HashSet<Cell> _closed;
        private readonly Dictionary<Cell, Cell> _cameFrom;
        private readonly Dictionary<Cell, double> _distTo;

        public ArmyPathCalculator(Army army, IWorldManager worldManager)
        {
            _army = army;
            _worldManager = worldManager;
            Destination = null;
            PathSuccessful = false;
            PathFinished = true;
            Path = new Queue<Cell>();

            _open = new SimplePriorityQueue<Cell, double>();
            _closed = new HashSet<Cell>();
            _cameFrom = new Dictionary<Cell, Cell>();
            _distTo = new Dictionary<Cell, double>();

            _start = army.Cell;
            Destination = army.Cell;
        }

        public void SetDestination(Cell cell, bool pathAdjacent)
        {
            _pathAdjacent = pathAdjacent;
            if (cell != Destination)
            {
                Destination = cell;
                ResetPathing();
            }
        }

        public void SetDestination(Army army)
        {
            SetDestination(army.Cell, pathAdjacent: true);
        }

        public void ClearPathing()
        {
            SetDestination(null, false);
        }

        public void ResetPathing()
        {
            _start = _army.Cell;
            PathSuccessful = false;
            PathFinished = false;
            Path.Clear();

            _open.Clear();
            _closed.Clear();
            _cameFrom.Clear();
            _distTo.Clear();

            _open.Enqueue(_army.Cell, 0);
            _cameFrom.Add(_army.Cell, null);
            _distTo.Add(_army.Cell, 0);
        }

        public void Update()
        {
            if(Destination == null)
            {
                return;
            }

            if (!PathIsValid())
            {
                ResetPathing();
            }

            if (PathFinished)
            {
                return;
            }

            var planeChangeOptions = _worldManager.Planes
                .Where(p => p.Id != _army.Cell.Position.Plane && Destination.Position.Plane == p.Id)
                .Where(p => _army.Units.All(u => u.MovementModes.Any(mm => p.NativePlaneChanging.Contains(mm.MovementType))))
                .ToArray();

            const int iterationLimit = 10;

            for (var i = 0; i < iterationLimit && !PathFinished && _open.Any(); i++)
            {
                var current = _open.Dequeue();
                _closed.Add(current);
                
                if ((!_pathAdjacent && current == Destination)
                    || (_pathAdjacent && current.OrthoNeighbors.Any(n => n == Destination)))
                {
                    ConstructPath(current);
                    PathSuccessful = true;
                    PathFinished = true;
                    return;
                }
                
                var neighbors = current.OrthoNeighbors
                    .Concat(planeChangeOptions.Select(p => _worldManager.GetCell(new CellPosition(p.Id, current.Position.X, current.Position.Y))))
                    .Where(n => _army.CanPath(n))
                    .Where(n => !_closed.Contains(n))
                    .ToArray();

                foreach (var neighbor in neighbors)
                {
                    var dist = _distTo[current] + 1 / _army.GetMovementSpeed(neighbor);

                    if (_open.Contains(neighbor))
                    {
                        if (dist < _distTo[neighbor])
                        {
                            _distTo[neighbor] = dist;
                            _cameFrom[neighbor] = current;
                            _open.UpdatePriority(neighbor, dist);
                        }
                    }
                    else
                    {
                        _distTo.Add(neighbor, dist);
                        _cameFrom.Add(neighbor, current);
                        _open.Enqueue(neighbor, dist);
                    }
                }
            }


            if (!_open.Any())
            {
                PathFinished = true;
            }
        }

        private bool PathIsValid()
        {
            if (!PathFinished)
            {
                return _start == _army.Cell;
            }

            if (Path.Any())
            {
                if (Path.First() != _army.Cell
                    && !_army.Cell.OrthoNeighbors.Contains(Path.First()))
                {
                    return false;
                }
            }
            else
            {
                return _army.Cell == Destination;
            }

            foreach(var cell in Path)
            {
                if(cell == null
                    || !_army.CanPath(cell))
                {
                    return false;
                }

                if(_army.Cell.Position.Plane != cell.Position.Plane)
                {
                    var dstPlane = _worldManager.GetPlane(cell.Position.Plane);

                    if(!_army.Units.All(u => u.MovementModes.Any(mm => dstPlane.NativePlaneChanging.Contains(mm.MovementType))))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        private void ConstructPath(Cell current)
        {
            var path = new List<Cell>();
            while (current != null)
            {
                path.Add(current);
                current = _cameFrom[current];
            }
            path.Reverse();

            foreach (var cell in path)
            {
                Path.Enqueue(cell);
            }
        }

        public void SerializeTo(SerializedObject root)
        {
            _start.Position.SerializeTo(root.CreateChild("start_pos"));
            if (Destination != null)
            {
                Destination.Position.SerializeTo(root.CreateChild("destination_pos"));
            }
            root.Set("path_successful", PathSuccessful);
            root.Set("path_finished", PathFinished);

            foreach (var cell in Path)
            {
                cell.Position.SerializeTo(root.CreateChild("path_pos"));
            }
        }

        public static ArmyPathCalculator DeserializeFrom(
            IWorldManager worldManager,
            IGamedataTracker gamedataTracker,
            IEnumerable<Region> regions,
            Army army,
            SerializedObject root)
        {
            var calculator = new ArmyPathCalculator(army, worldManager);

            var startPos = CellPosition.DeserializeFrom(root.GetChild("start_pos"));

            calculator._start = regions
                .SelectMany(r => r.Cells)
                .Where(c => c.Position == startPos)
                .FirstOrDefault();

            if (root.GetOptionalChild("destination_pos") != null)
            {
                var dstPos = CellPosition.DeserializeFrom(root.GetChild("destination_pos"));

                calculator.Destination = regions
                    .SelectMany(r => r.Cells)
                    .Where(c => c.Position == dstPos)
                    .FirstOrDefault();
            }

            calculator.PathSuccessful = root.GetBool("path_successful");
            calculator.PathFinished = root.GetBool("path_finished");

            var pathPositions = root.GetChildren("path_pos")
                .Select(x => CellPosition.DeserializeFrom(x))
                .ToArray();

            foreach (var pos in pathPositions)
            {
                var cell = regions
                    .SelectMany(r => r.Cells)
                    .Where(c => c.Position == pos)
                    .FirstOrDefault();

                calculator.Path.Enqueue(cell);
            }

            return calculator;
        }
    }
}
