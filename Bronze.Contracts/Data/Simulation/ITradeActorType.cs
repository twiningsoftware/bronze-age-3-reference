﻿using Bronze.Contracts.Data.Gamedata;

namespace Bronze.Contracts.Data.Simulation
{
    public interface ITradeActorType : IEconomicActorType
    {
        MovementType TradeType { get; }
        double TradeCapacity { get; }
        TradeHaulerInfo TradeHauler { get; }
    }
}
