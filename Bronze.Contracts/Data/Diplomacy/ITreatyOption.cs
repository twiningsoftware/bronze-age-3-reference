﻿using System;
using System.Collections.Generic;

namespace Bronze.Contracts.Data.Diplomacy
{
    public interface ITreatyOption
    {
        string Name { get; }
        string Description { get; }
        bool HasParameters { get; }

        ITreaty BuildTreaty();

        void ShowEditor(Action<ITreaty> callback, IEnumerable<ITreaty> treatiesInNegotiation);
    }
}
