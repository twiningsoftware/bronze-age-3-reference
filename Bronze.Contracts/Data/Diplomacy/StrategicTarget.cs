﻿using Bronze.Contracts.Data.World;

namespace Bronze.Contracts.Data.Diplomacy
{
    public class StrategicTarget
    {
        public Region TargetRegion { get; set; }
        public bool TakeByForce { get; set; }
        public string Goal { get; set; }
    }
}
