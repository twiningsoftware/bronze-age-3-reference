﻿using System;
using Bronze.Contracts.Data.Serialization;

namespace Bronze.Contracts.Data.Diplomacy
{
    public class DiplomaticMemory
    {
        public string Category { get; private set; }
        public string Description { get; private set; }
        public double Date { get; private set; }
        public double StartingStrength { get; private set; }
        public double StrengthPerMonth { get; private set; }
        public double Influence { get; private set; }
        public double Trust { get; private set; }

        public DiplomaticMemory(
            string category,
            string description,
            double date,
            double startingStrength,
            double strengthPerMonth,
            double influence,
            double trust)
        {
            Category = category;
            Description = description;
            Date = date;
            StartingStrength = startingStrength;
            StrengthPerMonth = strengthPerMonth;
            Influence = influence;
            Trust = trust;
        }

        public void SerializeTo(SerializedObject memoryRoot)
        {
            memoryRoot.Set("category", Category);
            memoryRoot.Set("description", Description);
            memoryRoot.Set("date", Date);
            memoryRoot.Set("starting_strength", StartingStrength);
            memoryRoot.Set("strength_per_month", StrengthPerMonth);
            memoryRoot.Set("influence", Influence);
            memoryRoot.Set("trust", Trust);
        }

        public static DiplomaticMemory DeserializeFrom(
            SerializedObject memoryRoot)
        {
            return new DiplomaticMemory(
                memoryRoot.GetString("category"),
                memoryRoot.GetString("description"),
                memoryRoot.GetDouble("date"),
                memoryRoot.GetDouble("starting_strength"),
                memoryRoot.GetDouble("strength_per_month"),
                memoryRoot.GetDouble("influence"),
                memoryRoot.GetDouble("trust"));
        }

        public double GetStrengthAt(double date)
        {
            return Util.Clamp(
                    StartingStrength + StrengthPerMonth * (date - Date),
                    0, 1);
        }

        public double GetInfluenceAt(double date)
        {
            return Influence * GetStrengthAt(date);
        }

        public double GetTrustAt(double date)
        {
            return Trust * GetStrengthAt(date);
        }
    }
}
