﻿using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;

namespace Bronze.Contracts.Data.Diplomacy
{
    public interface ITreaty
    {
        Tribe From { get; }
        Tribe To { get; }
        string Name { get; }
        string Description { get; }
        bool CanBeCancelled { get; }
        string SummaryIconKey { get; }
        bool BlocksWar { get; }

        void LoadFrom(IWorldManager worldManager, SerializedObject treatyRoot);

        void SerializeTo(SerializedObject treatyRoot);

        void OnRatification();
        void ApplyChanges();
        void RevertChanges();
        void StepSimulation(double elapsedMonths);
        void OnRemoval();
        IEnumerable<TreatyValueDetail> GetValueDetailFor(Tribe aiTribe);

        bool IsCoercing(Tribe tribe);

        void ShowEditor(Action finishedCallback, IEnumerable<ITreaty> treatiesInNegotiation);
    }
}
