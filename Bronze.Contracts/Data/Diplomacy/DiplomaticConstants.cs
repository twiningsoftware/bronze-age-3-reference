﻿namespace Bronze.Contracts.Data.Diplomacy
{
    public static class DiploConstants
    {
        public static readonly string ACTION_TYPE_FIRSTMEET = "firstmeet";
        public static readonly string ACTION_TYPE_GREETING = "greeting";

        public static readonly string ACTION_TYPE_NEGOTIATE = "negotiate";
        public static readonly string RESPONSE_NEGOTIATE = "<^_diplo_negotiate>";

        public static readonly string ACTION_TYPE_CHARM = "charm";
        public static readonly string ACTION_TEXT_CHARM = "<^_diplo_charm_button>";
        public static readonly string RESPONSE_CHARM = "<^_diplo_charm>";

        public static readonly string ACTION_TYPE_INTIMIDATE = "intimidate";
        public static readonly string ACTION_TEXT_INTIMIDATE = "<^_diplo_intimidate_button>";
        public static readonly string RESPONSE_INTIMIDATE = "<^_diplo_intimidate>";

        public static readonly string ACTION_TYPE_RATIONALIZE = "rationalize";
        public static readonly string ACTION_TEXT_RATIONALIZE = "<^_diplo_rationalize_button>";
        public static readonly string RESPONSE_RATIONALIZE = "<^_diplo_rationalize>";

        public static readonly string ACTION_TYPE_STARTWAR = "startwar";
        public static readonly string RESPONSE_STARTWAR = "<^_diplo_startwar>";

        public static readonly string RESPONSE_FIRSTMEET = "<^_diplo_firstmeet>";
        public static readonly string RESPONSE_GREETING = "<^_diplo_greeting>";

        public static readonly string MESSAGE_WANTREGION = "<^_message_wantregion>";
        public static readonly string MESSAGE_STARTWAR = "<^_message_startwar>";
    }
}
