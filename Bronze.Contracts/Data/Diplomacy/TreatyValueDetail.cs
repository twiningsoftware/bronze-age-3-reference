﻿namespace Bronze.Contracts.Data.Diplomacy
{
    public class TreatyValueDetail
    {
        public string Reason { get; }
        public int Value { get; }

        public TreatyValueDetail(string reason, int value)
        {
            Reason = reason;
            Value = value;
        }
    }
}