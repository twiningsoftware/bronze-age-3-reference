﻿using Bronze.Contracts.Data.World;
using System.Collections.Generic;

namespace Bronze.Contracts.Data.Diplomacy
{
    public class PitchedBattleResult
    {
        public IEnumerable<Army> Attackers { get; set; }
        public IEnumerable<Army> Defenders { get; set; }

        public bool TimedOut { get; set; }
        
        public double TotalStrength { get; set; }

        public double AttackerScore { get; set; }
        public double DefenderScore { get; set; }
        public bool AttackerWon { get; set; }
        public bool DefenderWon { get; set; }
        public double AttackerBeforeStrength { get; set; }
        public double AttackerAfterStrength { get; set; }
        public double DefenderBeforeStrength { get; set; }
        public double DefenderAfterStrength { get; set; }

        public IEnumerable<Army> RemainingArmies { get; set; }
        public IEnumerable<Army> RoutingArmies { get; set; }
        public IEnumerable<Army> DestroyedArmies { get; set; }

        public IEnumerable<IUnit> SurvivingAttackers { get; set; }
        public IEnumerable<IUnit> LostAttackers { get; set; }
        public IEnumerable<IUnit> SurvivingDefenders { get; set; }
        public IEnumerable<IUnit> LostDefenders { get; set; }
        public IEnumerable<NotablePerson> AttackerLeaders { get; set; }
        public IEnumerable<NotablePerson> DefenderLeaders { get; set; }
        
        public Cell Location { get; set; }

        public List<UnitStateSnapshot> DetailedResults { get; set; }
        public string Name { get; set; }
        public string BackgroundImage { get; set; }
    }
}
