﻿using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using System.Linq;

namespace Bronze.Contracts.Data.Diplomacy
{
    public class WarEvent
    {
        public string Name { get; private set; }
        public double WarScore { get; private set; }
        public double Date { get; private set; }
        public string Type { get; private set; }
        public string TargetId { get; private set; }
        public CellPosition? Location { get; private set; }

        public WarEvent(
            string name,
            double warScore,
            double date,
            string type,
            string targetId,
            CellPosition? location)
        {
            Name = name;
            WarScore = warScore;
            Date = date;
            Type = type;
            TargetId = targetId;
            Location = location;
        }
        
        public void SerializeTo(SerializedObject warEventRoot)
        {
            warEventRoot.Set("name", Name);
            warEventRoot.Set("war_score", WarScore);
            warEventRoot.Set("date", Date);
            warEventRoot.Set("type", Type);
            warEventRoot.Set("target_id", TargetId);
            if (Location != null)
            {
                Location.Value.SerializeTo(warEventRoot.CreateChild("location"));
            }
        }

        public static WarEvent DeserializeFrom(SerializedObject warEventRoot)
        {
            var position = warEventRoot
                .GetChildren("position")
                .Select(p => CellPosition.DeserializeFrom(p))
                .FirstOrDefault();

            return new WarEvent(
                warEventRoot.GetString("name"),
                warEventRoot.GetDouble("war_score"),
                warEventRoot.GetDouble("date"),
                warEventRoot.GetString("type"),
                warEventRoot.GetOptionalString("target_id"),
                position);
        }
    }
}