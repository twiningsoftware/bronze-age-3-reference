﻿using System.Collections.Generic;

namespace Bronze.Contracts.Data.Diplomacy
{
    public class DiplomaticResponse
    {
        public string ResponseText { get; set; }

        public IEnumerable<IEnumerable<DiplomaticAction>> Actions { get; set; }

        public DiplomaticMemory FormedMemory { get; set; }
    }
}
