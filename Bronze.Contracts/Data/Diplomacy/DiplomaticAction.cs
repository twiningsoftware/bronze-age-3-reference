﻿namespace Bronze.Contracts.Data.Diplomacy
{
    public class DiplomaticAction
    {
        public string ButtonText { get; set; }
        public string Tooltip { get; set; }
        public bool IsEnabled { get; set; }
        public string ActionType { get; set; }
        public object ActionData { get; set; }
        public bool EndConvo { get; set; }
        public bool StartNegotition { get; set; }
    }
}