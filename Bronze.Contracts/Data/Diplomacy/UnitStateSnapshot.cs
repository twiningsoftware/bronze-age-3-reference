﻿using Bronze.Contracts.Data.World;

namespace Bronze.Contracts.Data.Diplomacy
{
    public class UnitStateSnapshot
    {
        public IUnit Unit { get; set; }
        public double HealthPercent { get; set; }
        public double MoralePercent { get; set; }
        public double Position { get; set; }
        public double Time { get; set; }
        public bool MadeMeleeAttack { get; set; }
        public bool MadeRangedAttack { get; set; }
        public bool WasAttacked { get; set; }
        public bool IsAttacker { get; set; }
    }
}