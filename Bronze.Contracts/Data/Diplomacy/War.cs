﻿using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;

namespace Bronze.Contracts.Data.Diplomacy
{
    public class War
    {
        public string Id { get; private set; }
        public double StartDate { get; private set; }
        public double EndDate { get; set; }
        public bool IsOngoing => EndDate < StartDate;
        public Tribe Aggressor { get; private set; }
        public Tribe Defender { get; private set; }
        public double WarScore { get; private set; }
        public IEnumerable<WarEvent> Events => _events;
        private List<WarEvent> _events;

        public War(double startDate, Tribe aggressor, Tribe defender)
        {
            Id = Guid.NewGuid().ToString();
            StartDate = startDate;
            EndDate = -1;
            Aggressor = aggressor;
            Defender = defender;
            WarScore = 0;
            _events = new List<WarEvent>();
        }

        public void AddEvent(WarEvent warEvent)
        {
            _events.Add(warEvent);

            WarScore += warEvent.WarScore;
        }

        public void SerializeTo(SerializedObject warRoot)
        {
            warRoot.Set("id", Id);
            warRoot.Set("start_date", StartDate);
            warRoot.Set("end_date", EndDate);
            warRoot.Set("aggressor_id", Aggressor.Id);
            warRoot.Set("defender_id", Defender.Id);
            
            foreach(var warEvent in _events)
            {
                warEvent.SerializeTo(warRoot.CreateChild("war_event"));
            }
        }

        public static War DeserializeFrom(IWorldManager worldManager, SerializedObject warRoot)
        {
            var war = new War(
                warRoot.GetDouble("start_date"),
                warRoot.GetObjectReference(
                    "aggressor_id",
                    worldManager.Tribes,
                    t => t.Id),
                warRoot.GetObjectReference(
                    "defender_id",
                    worldManager.Tribes,
                    t => t.Id))
            {
                Id = warRoot.GetString("id"),
                EndDate = warRoot.GetDouble("end_date")
            };

            foreach(var warEventRoot in warRoot.GetChildren("war_event"))
            {
                war.AddEvent(WarEvent.DeserializeFrom(warEventRoot));
            }

            return war;
        }
    }
}
