﻿namespace Bronze.Contracts.Data.Generation
{
    public class InclusionRadius
    {
        public int Radius { get; set; }
        public InclusionFeature[] InclusionFeatures { get; set; }
    }
}
