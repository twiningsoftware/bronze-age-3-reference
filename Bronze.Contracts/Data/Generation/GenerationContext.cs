﻿using Bronze.Common.Generation;
using Bronze.Contracts.Data.World;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Contracts.Data.Generation
{
    public class GenerationContext
    {
        public Random Random { get; }
        public WorldgenParameters Parameters { get; }
        public Dictionary<Plane, List<PlacedRegionFeature>> PlacedRegionFeatures { get; }
        public Dictionary<PlaneFeature, Plane> Planes { get; }
        public Dictionary<Cell, RiverSpawner> RiverSpawners { get; }
        public Dictionary<Cell, MidpathSpawner> MidpathSpawners { get; }
        public List<BorderFeaturePlacement> BorderFeaturesToPlace { get; }
        public Dictionary<string, List<Tuple<ISeedPoint, System.Numerics.Vector2>>> PlannedSeedPoints { get; }
        public Tribe PlayerTribe { get; }
        public Cell PlayerStartPos { get; set; }
        public List<TribePlacement> TribePlacements { get; }
        public List<Region> MirroredRegions { get; }
        public EmpirePlacerSeedPoint PlayerStart { get; }
        public Dictionary<string, int> PlaneToId { get; }

        public GenerationContext(Random random, WorldgenParameters parameters)
        {
            Random = random;
            Parameters = parameters;
            PlacedRegionFeatures = new Dictionary<Plane, List<PlacedRegionFeature>>();
            Planes = new Dictionary<PlaneFeature, Plane>();
            RiverSpawners = new Dictionary<Cell, RiverSpawner>();
            MidpathSpawners = new Dictionary<Cell, MidpathSpawner>();
            BorderFeaturesToPlace = new List<BorderFeaturePlacement>();
            PlayerTribe = parameters.PlayerTribe;
            PlannedSeedPoints = parameters.WorldType.PlaneFeatures
                .ToDictionary(pf => pf.Id, pf => new List<Tuple<ISeedPoint, System.Numerics.Vector2>>());
            TribePlacements = new List<TribePlacement>();
            MirroredRegions = new List<Region>();
            PlayerStart = parameters.PlayerStart;
            PlaneToId = new Dictionary<string, int>();
        }

        public void ClearForNewPlane()
        {
            RiverSpawners.Clear();
            MidpathSpawners.Clear();
            BorderFeaturesToPlace.Clear();
        }
    }
}
