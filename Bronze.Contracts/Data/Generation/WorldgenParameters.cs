﻿using Bronze.Contracts.Data.World;

namespace Bronze.Contracts.Data.Generation
{
    public class WorldgenParameters
    {
        public string Seed { get; set; }
        public float Hostility { get; set; }
        public bool Preview { get; set; }
        public WorldType WorldType { get; set; }
        public string WorldName { get; set; }
        public Tribe PlayerTribe { get; set; }
        public EmpirePlacerSeedPoint PlayerStart { get; set; }
    }
}
