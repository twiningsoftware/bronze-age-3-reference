﻿using Bronze.Contracts.Data.Gamedata;

namespace Bronze.Contracts.Data.Generation
{
    public class TribeSpawner
    {
        public string Id { get; set; }
        public int RegionsPerOccurance { get; set; }
        public int MinimumOccurances { get; set; }
        public int MinTerritory { get; set; }
        public int MaxTerritory { get; set; }
        public Race Race { get; set; }
        public ICellDevelopmentType StartingDevelopmentType { get; set; }
        public IStartingPeopleScheme StartingPeopleScheme { get; set; }
        public ITribeControllerType TribeControllerType { get; set; }
        public Trait[] RequiredTraits { get; set; }
        public Trait[] PreferredTraits { get; set; }
        public Trait[] NotPreferredTraits { get; set; }
        public Trait[] PreferredNeighborTraits { get; set; }
        public Trait[] ForbiddenTraits { get; set; }
        public Caste StartingCaste { get; set; }
        public int StartingPops { get; set; }
    }
}
