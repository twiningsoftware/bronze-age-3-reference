﻿namespace Bronze.Contracts.Data.Generation
{
    public class BorderFeature
    {
        public string Id { get; set; }
        public int CellsPerOccurance { get; set; }
        public int MinimumOccurances { get; set; }
        public int MinLength { get; set; }
        public int MaxLength { get; set; }
        public BorderFeatureCondition[] StartConditions { get; set; }
        public BorderFeatureCondition[] PathConditions { get; set; }
        public BorderFeatureCondition[] EndConditions { get; set; }
        public InclusionRadius[] InclusionRadii { get; set; }
        public BorderFeatureInterruption[] Interruptions { get; set; }
    }
}
