﻿using Bronze.Contracts.Data.Gamedata;

namespace Bronze.Contracts.Data.Generation
{
    public class RegionMirror
    {
        public string Id { get; set; }
        public string MirrorsRegion { get; set; }
        public Biome Biome { get; set; }
        public ICellFeature CellFeature { get; set; }
        public RegionMirror[] RegionMirrors { get; set; }
        public SubFeature[] SubFeatures { get; set; }
        public BorderFeature[] BorderFeatures { get; set; }
        public InclusionFeature[] InclusionFeatures { get; set; }
    }
}
