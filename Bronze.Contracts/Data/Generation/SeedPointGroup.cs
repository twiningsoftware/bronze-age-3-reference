﻿namespace Bronze.Contracts.Data.Generation
{
    public class SeedPointGroup
    {
        public string Id { get; set; }
        public int PickCount { get; set; }
        public ISeedPoint[] Options { get; set; }
    }
}