﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using System.Collections.Generic;

namespace Bronze.Contracts.Data.Generation
{
    public class TribePlacement
    {
        public Tribe Tribe { get; }
        public IEnumerable<Region> Regions { get; }
        public Trait[] RequiredTraits { get; }
        public Trait[] PreferredTraits { get; }
        public Trait[] NotPreferredTraits { get; }
        public Trait[] PreferredNeighborTraits { get; }
        public Trait[] NotPreferredNeighborTraits { get; }
        public Trait[] ForbiddenTraits { get; }
        public Trait[] RequiredNeighborTraits { get; }
        public ICellDevelopmentType StartingDevelopmentType { get; }
        public Caste StartingCaste { get; }
        public int StartingPops { get; }
        public int SettlementCount { get; }
        public bool HighPriority { get; }
        public ITribeControllerType TribeControllerType { get; }
        public EmpirePlacementSeedDevelopment[] SeedDevelopments { get; }


        public TribePlacement(
            Tribe tribe, 
            IEnumerable<Region> regions, 
            Trait[] requiredTraits, 
            Trait[] preferredTraits,
            Trait[] notPreferredTraits,
            Trait[] preferredNeighborTraits,
            Trait[] notPreferredNeighborTraits,
            Trait[] forbiddenTraits,
            Trait[] requiredNeighborTraits,
            ICellDevelopmentType startingDevelopmentType,
            Caste startingCaste,
            int startingPops,
            int settlementCount,
            bool highPriority,
            ITribeControllerType tribeControllerType,
            EmpirePlacementSeedDevelopment[] seedDevelopments)
        {
            Tribe = tribe;
            Regions = regions;
            RequiredTraits = requiredTraits;
            PreferredTraits = preferredTraits;
            NotPreferredTraits = notPreferredTraits;
            PreferredNeighborTraits = preferredNeighborTraits;
            NotPreferredNeighborTraits = notPreferredNeighborTraits;
            RequiredNeighborTraits = requiredNeighborTraits;
            ForbiddenTraits = forbiddenTraits;
            StartingDevelopmentType = startingDevelopmentType;
            StartingCaste = startingCaste;
            StartingPops = startingPops;
            SettlementCount = settlementCount;
            HighPriority = highPriority;
            TribeControllerType = tribeControllerType;
            SeedDevelopments = seedDevelopments;
        }
    }
}
