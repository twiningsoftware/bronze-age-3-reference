﻿namespace Bronze.Contracts.Data.Generation
{
    public class MidpathSpawner
    {
        public string Id { get; set; }
        public double SpawnChance { get; set; }
        public MidpathType MidpathType { get; set; }
    }
}
