﻿using Bronze.Contracts.Data.Gamedata;

namespace Bronze.Contracts.Data.Generation
{
    public class BorderFeatureInterruption
    {
        public string Id { get; set; }
        public int CellsPerOccurance { get; set; }
        public int MinimumOccurances { get; set; }
        public Biome Biome { get; set; }
        public ICellFeature CellFeature { get; set; }
    }
}
