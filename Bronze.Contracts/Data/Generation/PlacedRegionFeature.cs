﻿using Bronze.Contracts.Data.Generation;
using Bronze.Contracts.Data.World;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.Generation
{
    public class PlacedRegionFeature
    {
        public RegionFeature RegionFeature { get;  private set; }
        public Region[] ContainedRegions { get; set; }

        public PlacedRegionFeature(RegionFeature regionFeature, IEnumerable<Region> containedRegions)
        {
            RegionFeature = regionFeature;
            ContainedRegions = containedRegions.ToArray();
        }
    }
}
