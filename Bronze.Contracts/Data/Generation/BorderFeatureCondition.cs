﻿using Bronze.Contracts.Data.World;
using System;

namespace Bronze.Contracts.Data.Generation
{
    public class BorderFeatureCondition
    {
        private Func<Cell, bool> _checkFunc;

        public BorderFeatureCondition(Func<Cell, bool> checkFunc)
        {
            _checkFunc = checkFunc;
        }

        public bool IsValid(Cell cell)
        {
            return cell != null && _checkFunc(cell);
        }
    }
}
