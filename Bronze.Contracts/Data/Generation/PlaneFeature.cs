﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Contracts.Data.Generation
{
    public class PlaneFeature
    {
        public string Id { get; set; }
        public string MirrorsPlane { get; set; }
        public string Name { get; set; }
        public int DisplayOrder { get; set; }
        public string RegionNamePattern { get; set; }
        public Biome Biome { get; set; }
        public ICellFeature CellFeature { get; set; }
        public IRegionGenerator RegionGenerator { get; set; }
        public RegionMirror[] RegionMirrors { get; set; }
        public ISeedPoint[] SeedPoints { get; set; }
        public SeedPointGroup[] SeedPointGroups { get; set; }
        public MovementType[] NativePlaneChanging { get; set; }
    }
}
