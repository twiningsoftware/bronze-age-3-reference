﻿using Bronze.Contracts.Data.Gamedata;

namespace Bronze.Contracts.Data.Generation
{
    public class EmpirePlacementSeedDevelopment
    {
        public ICellDevelopmentType Development { get; set; }
        public Recipie Recipie { get; set; }
        public Trait[] ClearTraits { get; set; }
        public Trait[] PreserveTraits { get; set; }
    }
}
