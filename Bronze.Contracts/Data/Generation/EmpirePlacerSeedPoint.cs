﻿using Bronze.Common.Generation;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.Exceptions;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Xml.Linq;

namespace Bronze.Contracts.Data.Generation
{
    public class EmpirePlacerSeedPoint : ISeedPoint
    {
        public string Id { get; private set; }
        public int Priority { get; private set; }
        public int X { get; private set; }
        public int Y { get; private set; }
        public int PositionVariance { get; private set; }
        public double InfluenceWeight { get; private set; }
        public double InfluenceRange { get; private set; }
        public ISeedPoint[] SeedPoints { get; set; }
        public SeedPointGroup[] SeedPointGroups { get; set; }

        public Trait[] RequiredTraits { get; private set; }
        public Trait[] PreferredTraits { get; private set; }
        public Trait[] NotPreferredTraits { get; private set; }
        public Trait[] PreferredNeighborTraits { get; private set; }
        public Trait[] NotPreferredNeighborTraits { get; private set; }
        public Trait[] ForbiddenTraits { get; private set; }
        public Trait[] RequiredNeighborTraits { get; private set; }

        public EmpirePlacement[] EmpirePlacements { get; private set; }
        
        public bool ClaimsRegions => true;

        private readonly IRegionFeaturePlacer _regionFeaturePlacer;
        private readonly IGamedataTracker _gamedataTracker;
        private readonly INameSource _nameSource;
        private readonly BronzeColor[] _tribeColors;

        public EmpirePlacerSeedPoint(
            IRegionFeaturePlacer regionFeaturePlacer,
            IGamedataTracker gamedataTracker,
            INameSource nameSource)
        {
            _regionFeaturePlacer = regionFeaturePlacer;
            _gamedataTracker = gamedataTracker;
            _nameSource = nameSource;

            _tribeColors = Enum.GetValues(typeof(BronzeColor))
                .Cast<BronzeColor>()
                .Where(c => c != BronzeColor.None)
                .ToArray();
        }
        
        public void LoadFrom(
            IXmlReaderUtil xmlReaderUtil,
            XElement element,
            RegionFeature[] regionFeatures,
            IEnumerable<ISeedPoint> childPoints,
            IEnumerable<SeedPointGroup> childGroups)
        {
            Id = xmlReaderUtil.AttributeValue(element, "id");
            Priority = xmlReaderUtil.AttributeAsInt(element, "priority");
            X = xmlReaderUtil.AttributeAsInt(element, "x");
            Y = xmlReaderUtil.AttributeAsInt(element, "y");
            PositionVariance = xmlReaderUtil.AttributeAsInt(element, "pos_variance");
            InfluenceWeight = xmlReaderUtil.AttributeAsDouble(element, "infl_weight");
            InfluenceRange = xmlReaderUtil.AttributeAsDouble(element, "infl_range");
            SeedPoints = childPoints.ToArray();
            SeedPointGroups = childGroups.ToArray();

            RequiredTraits = element
                .Elements("required")
                .Select(e => xmlReaderUtil.ObjectReferenceLookup(
                    e,
                    "trait",
                    _gamedataTracker.Traits,
                    t => t.Id))
                .ToArray();

            PreferredTraits = element
                .Elements("preferred")
                .Select(e => xmlReaderUtil.ObjectReferenceLookup(
                    e,
                    "trait",
                    _gamedataTracker.Traits,
                    t => t.Id))
                .ToArray();

            NotPreferredTraits = element
                .Elements("not_preferred")
                .Select(e => xmlReaderUtil.ObjectReferenceLookup(
                    e,
                    "trait",
                    _gamedataTracker.Traits,
                    t => t.Id))
                .ToArray();

            PreferredNeighborTraits = element
                .Elements("preferred_neighbor")
                .Select(e => xmlReaderUtil.ObjectReferenceLookup(
                    e,
                    "trait",
                    _gamedataTracker.Traits,
                    t => t.Id))
                .ToArray();

            NotPreferredNeighborTraits = element
                .Elements("not_preferred_neighbor")
                .Select(e => xmlReaderUtil.ObjectReferenceLookup(
                    e,
                    "trait",
                    _gamedataTracker.Traits,
                    t => t.Id))
                .ToArray();

            RequiredNeighborTraits = element
                .Elements("required_neighbor")
                .Select(e => xmlReaderUtil.ObjectReferenceLookup(
                    e,
                    "trait",
                    _gamedataTracker.Traits,
                    t => t.Id))
                .ToArray();

            ForbiddenTraits = element
                .Elements("forbidden")
                .Select(e => xmlReaderUtil.ObjectReferenceLookup(
                    e,
                    "trait",
                    _gamedataTracker.Traits,
                    t => t.Id))
                .ToArray();

            EmpirePlacements = element
                .Elements("empire")
                .Select(e => LoadEmpirePlacement(xmlReaderUtil, e))
                .ToArray();
        }

        private EmpirePlacement LoadEmpirePlacement(
            IXmlReaderUtil xmlReaderUtil,
            XElement empireElement)
        {
            var race = xmlReaderUtil.ObjectReferenceLookup(
                    empireElement,
                    "race",
                    _gamedataTracker.Races,
                     r => r.Id);

            return new EmpirePlacement
            {
                Id = xmlReaderUtil.AttributeValue(empireElement, "id"),
                IsPlayerAvailable = xmlReaderUtil.AttributeAsBool(empireElement, "player_available"),
                Race = race,
                TribeControllerType = xmlReaderUtil.ObjectReferenceLookup(
                    empireElement,
                    "controller_type",
                    _gamedataTracker.ControllerTypes,
                    x => x.Id),
                StartingDevelopment = xmlReaderUtil.ObjectReferenceLookup(
                    empireElement,
                    "starting_development",
                    race.AvailableCellDevelopments,
                    d => d.Id),
                StartingCaste = xmlReaderUtil.ObjectReferenceLookup(
                    empireElement,
                    "starting_caste",
                    race.Castes,
                    c => c.Id),
                StartingPops = xmlReaderUtil.AttributeAsInt(empireElement, "starting_pops"),
                SeedDevelopments = empireElement.Elements("seed_development")
                    .Select(e => new EmpirePlacementSeedDevelopment
                    {
                        Development = xmlReaderUtil.ObjectReferenceLookup(
                            e,
                            "development_id",
                            _gamedataTracker.CellDevelopments,
                            cd => cd.Id),
                        Recipie = xmlReaderUtil.OptionalObjectReferenceLookup(
                            e,
                            "recipie_id",
                            _gamedataTracker.Recipies,
                            r => r.Id),
                        ClearTraits = e.Elements("clear_feature")
                            .Select(e2 => xmlReaderUtil.ObjectReferenceLookup(
                                e2,
                                "trait",
                                _gamedataTracker.Traits,
                                t => t.Id))
                            .ToArray(),
                        PreserveTraits = e.Elements("preserve_feature")
                            .Select(e2 => xmlReaderUtil.ObjectReferenceLookup(
                                e2,
                                "trait",
                                _gamedataTracker.Traits,
                                t => t.Id))
                            .ToArray()
                    })
                    .ToArray()
            };
        }

        public void Generate(
            GenerationContext context,
            World.Plane plane,
            Vector2 position,
            List<Cell> cells,
            List<Region> regions)
        {
            Tribe tribe = null;
            ICellDevelopmentType startingDevelopmentType = null;
            Caste startingCaste = null;
            int startingPops = 0;
            ITribeControllerType tribeControllerType = null;
            EmpirePlacementSeedDevelopment[] seedDevelopments = null;

            if(context.PlayerStart == this)
            {
                tribe = context.PlayerTribe;

                var empirePlacement = context.Random.Choose(EmpirePlacements.Where(ep => ep.Race == tribe.Race));

                startingDevelopmentType = empirePlacement.StartingDevelopment;
                startingCaste = empirePlacement.StartingCaste;
                startingPops = empirePlacement.StartingPops;
                seedDevelopments = empirePlacement.SeedDevelopments;
            }
            else if (EmpirePlacements.Any())
            {
                var empirePlacement = context.Random.Choose(EmpirePlacements);
                
                tribe = new Tribe
                {
                    Race = empirePlacement.Race,
                    PrimaryColor = context.Random.Choose(_tribeColors),
                    SecondaryColor = context.Random.Choose(_tribeColors),
                    Name = _nameSource.MakeTribeName(empirePlacement.Race)
                };

                tribeControllerType = empirePlacement.TribeControllerType;
                startingDevelopmentType = empirePlacement.StartingDevelopment;
                startingCaste = empirePlacement.StartingCaste;
                startingPops = empirePlacement.StartingPops;
                seedDevelopments = empirePlacement.SeedDevelopments;
            }
            
            if(tribe != null)
            {
                tribe.NotablePeople.Clear();
                tribe.Race.StartingPeopleScheme
                    .CreateInitialRulers(context.Random, tribe);
                
                context.TribePlacements.Add(new TribePlacement(
                    tribe,
                    regions,
                    RequiredTraits,
                    PreferredTraits,
                    NotPreferredTraits,
                    PreferredNeighborTraits,
                    NotPreferredNeighborTraits,
                    ForbiddenTraits,
                    RequiredNeighborTraits,
                    startingDevelopmentType,
                    startingCaste,
                    startingPops,
                    1,
                    true,
                    tribeControllerType,
                    seedDevelopments));
            }   
        }
    }
}