﻿using Bronze.Contracts.Data.Gamedata;

namespace Bronze.Contracts.Data.Generation
{
    public class WorldType
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string WorldNamePattern { get; set; }
        public int Size { get; set; }
        public PlaneFeature[] PlaneFeatures { get; set; }
        public RegionFeature[] RegionFeatures { get; set; }
        public string[] EdgeAdjacencies { get; set; }
        public Trait[] WorldTraits { get; set; }
    }
}
