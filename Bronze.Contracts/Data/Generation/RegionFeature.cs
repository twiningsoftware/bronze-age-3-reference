﻿using Bronze.Contracts.Data.Gamedata;

namespace Bronze.Contracts.Data.Generation
{
    public class RegionFeature
    {
        public string Id { get; set; }
        public int Priority { get; set; }
        public int RegionsPerOccurance { get; set; }
        public int MinimumOccurances { get; set; }
        public WorldFeatureShape Shape { get; set; }
        public int MinSize { get; set; }
        public int MaxSize { get; set; }
        public int Margin { get; set; }
        public Biome Biome { get; set; }
        public ICellFeature CellFeature { get; set; }
        public Biome FringeBiome { get; set; }
        public SubFeature[] SubFeatures { get; set; }
        public BorderFeature[] BorderFeatures { get; set; }
        public InclusionFeature[] InclusionFeatures { get; set; }
        public TribeSpawner[] TribeSpawners { get; set; }
    }
}
