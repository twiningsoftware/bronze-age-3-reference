﻿using Bronze.Common.Generation;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Xml.Linq;

namespace Bronze.Contracts.Data.Generation
{
    public class RegionBlobSeedPoint : ISeedPoint
    {
        public string Id { get; private set; }
        public int Priority { get; private set; }
        public int X { get; private set; }
        public int Y { get; private set; }
        public int PositionVariance { get; private set; }
        public double InfluenceWeight { get; private set; }
        public double InfluenceRange { get; private set; }
        public RegionFeature RegionFeature { get; private set; }
        public ISeedPoint[] SeedPoints { get; set; }
        public SeedPointGroup[] SeedPointGroups { get; set; }

        public bool ClaimsRegions => true;

        private readonly IRegionFeaturePlacer _regionFeaturePlacer;

        public RegionBlobSeedPoint(IRegionFeaturePlacer regionFeaturePlacer)
        {
            _regionFeaturePlacer = regionFeaturePlacer;
        }
        
        public void LoadFrom(
            IXmlReaderUtil xmlReaderUtil,
            XElement element,
            RegionFeature[] regionFeatures,
            IEnumerable<ISeedPoint> childPoints,
            IEnumerable<SeedPointGroup> childGroups)
        {
            Id = xmlReaderUtil.AttributeValue(element, "id");
            Priority = xmlReaderUtil.AttributeAsInt(element, "priority");
            X = xmlReaderUtil.AttributeAsInt(element, "x");
            Y = xmlReaderUtil.AttributeAsInt(element, "y");
            PositionVariance = xmlReaderUtil.AttributeAsInt(element, "pos_variance");
            InfluenceWeight = xmlReaderUtil.AttributeAsDouble(element, "infl_weight");
            InfluenceRange = xmlReaderUtil.AttributeAsDouble(element, "infl_range");
            RegionFeature = xmlReaderUtil.ObjectReferenceLookup(
                element,
                "region_feature",
                regionFeatures,
                x => x.Id);
            SeedPoints = childPoints.ToArray();
            SeedPointGroups = childGroups.ToArray();
        }

        public void Generate(
            GenerationContext context,
            World.Plane plane,
            Vector2 position,
            List<Cell> cells,
            List<Region> regions)
        {
            _regionFeaturePlacer.FillOutRegion(
                context,
                plane,
                RegionFeature,
                regions);
        }
    }
}
