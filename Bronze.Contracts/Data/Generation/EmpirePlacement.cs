﻿using Bronze.Contracts.Data.Gamedata;

namespace Bronze.Contracts.Data.Generation
{
    public class EmpirePlacement
    {
        public string Id { get; set; }
        public bool IsPlayerAvailable { get; set; }
        public Race Race { get; set; }
        public Caste StartingCaste { get; set; }
        public int StartingPops { get; set; }
        public ITribeControllerType TribeControllerType { get; set; }
        public ICellDevelopmentType StartingDevelopment { get; set; }
        public EmpirePlacementSeedDevelopment[] SeedDevelopments { get; set; }
    }
}