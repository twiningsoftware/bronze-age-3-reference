﻿namespace Bronze.Contracts.Data.Generation
{
    public enum WorldFeatureShape
    {
        Linear,
        Blob
    }
}
