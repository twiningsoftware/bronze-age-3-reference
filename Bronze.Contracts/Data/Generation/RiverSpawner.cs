﻿namespace Bronze.Contracts.Data.Generation
{
    public class RiverSpawner
    {
        public string Id { get; set; }
        public double SpawnChance { get; set; }
        public RiverType RiverType { get; set; }
    }
}
