﻿using Bronze.Contracts.Data.Gamedata;

namespace Bronze.Contracts.Data.Generation
{
    public class InclusionFeature
    {
        public string Id { get; set; }
        public int CellsPerOccurance { get; set; }
        public int MinimumOccurances { get; set; }
        public Biome Biome { get; set; }
        public ICellFeature CellFeature { get; set; }
        public ICellDevelopmentType CellDevelopment { get; set; }
        public RiverSpawner[] RiverSpawners { get; set; }
        public MidpathSpawner[] MidpathSpawners { get; set; }
    }
}
