﻿using Bronze.Contracts.Data.World;

namespace Bronze.Contracts.Data.Generation
{
    public class BorderFeaturePlacement
    {
        public Cell[] OutsideBorder { get; }
        public BorderFeature[] ChosenFeatures { get; }

        public BorderFeaturePlacement(
            Cell[] cell, 
            BorderFeature[] borderFeatures)
        {
            OutsideBorder = cell;
            ChosenFeatures = borderFeatures;
        }
    }
}