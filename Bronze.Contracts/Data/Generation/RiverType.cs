﻿using Bronze.Contracts.Data.Gamedata;

namespace Bronze.Contracts.Data.Generation
{
    public class RiverType
    {
        public string Id { get; set; }
        public string MergeGroup { get; set; }
        public int MergeSize { get; set; }
        public int MinLength { get; set; }
        public int MaxLength { get; set; }
        public Biome Biome { get; set; }
        public ICellFeature CellFeature { get; set; }
        public InclusionFeature[] Fords { get; set; }
        public BorderFeatureCondition[] InclusionConditions { get; set; }
        public BorderFeatureCondition[] ShortcutConditions { get; set; }
        public BorderFeatureCondition[] PathConditions { get; set; }
        public BorderFeatureCondition[] EndConditions { get; set; }
        public InclusionRadius[] InclusionRadii { get; set; }

        public BorderFeatureCondition[] DeltaConditions { get; set; }
        public InclusionRadius[] DeltaInclusionRadii { get; set; }

        public BorderFeatureCondition[] BiomeChangeConditions { get; set; }

        public RiverType MergeTo { get; set; }
        public int MergeToThreshold { get; set; }
        public int MergeToAdds { get; set; }
    }
}
