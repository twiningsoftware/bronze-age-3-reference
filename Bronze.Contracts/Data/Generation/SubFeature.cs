﻿namespace Bronze.Contracts.Data.Generation
{
    public class SubFeature
    {
        public string Id { get; set; }
        public int Priority { get; set; }
        public int RegionsPerOccurance { get; set; }
        public int MinimumOccurances { get; set; }
        public int MinSize { get; set; }
        public int MaxSize { get; set; }
        public int Margin { get; set; }
        public RegionFeature RegionFeature { get; set; }
    }
}
