﻿using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System.Collections.Generic;
using System.Numerics;
using System.Xml.Linq;

namespace Bronze.Contracts.Data.Generation
{
    public interface ISeedPoint
    {
        string Id { get; }
        int Priority { get; }
        int X { get;  }
        int Y { get; }
        int PositionVariance { get; }
        double InfluenceWeight { get; }
        double InfluenceRange { get; }
        ISeedPoint[] SeedPoints { get; }
        SeedPointGroup[] SeedPointGroups { get; }

        bool ClaimsRegions { get; }
        
        void LoadFrom(
            IXmlReaderUtil xmlReaderUtil, 
            XElement element, 
            RegionFeature[] regionFeatures,
            IEnumerable<ISeedPoint> childPoints,
            IEnumerable<SeedPointGroup> childGroups);

        void Generate(
            GenerationContext context,
            World.Plane plane,
            Vector2 position,
            List<Cell> cells,
            List<Region> regions);
    }
}