﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Contracts.Data.Generation
{
    public class StandardFamilyPeopleScheme : IStartingPeopleScheme
    {
        private readonly IPeopleGenerator _peopleGenerator;
        private readonly IPeopleHelper _peopleHelper;
        private readonly INotablePersonWorldgenAddon[] _notablePersonWorldgenAddons;

        public StandardFamilyPeopleScheme(
            IPeopleGenerator peopleGenerator,
            IPeopleHelper peopleHelper,
            IEnumerable<INotablePersonWorldgenAddon> notablePersonWorldgenAddons)
        {
            _peopleGenerator = peopleGenerator;
            _peopleHelper = peopleHelper;
            _notablePersonWorldgenAddons = notablePersonWorldgenAddons.ToArray();
        }

        public IEnumerable<NotablePerson> CreateInitialRulers(Random random, Tribe tribe)
        {
            var fatherAge = (tribe.Race.AgeOld - tribe.Race.AgeMaturity) * (random.NextDouble() * 0.4 + 0.3) + tribe.Race.AgeMaturity;
            var motherAge = (tribe.Race.AgeOld - tribe.Race.AgeMaturity) * (random.NextDouble() * 0.4 + 0.3) + tribe.Race.AgeMaturity;
            motherAge = Math.Min(motherAge, fatherAge);
            
            var father = _peopleGenerator.CreatePerson(
                random,
                tribe,
                tribe.Race,
                true,
                Constants.START_MONTH - fatherAge,
                null,
                null,
                true);

            var mother = _peopleGenerator.CreatePerson(
                random,
                tribe,
                tribe.Race,
                false,
                Constants.START_MONTH - motherAge,
                null,
                null,
                true);

            father.Spouse = mother;
            mother.Spouse = father;

            var family = new List<NotablePerson>
            {
                father,
                mother
            };
            
            var pregnancyCooldown = 0.0;
            for(var month = mother.BirthDate + mother.Race.AgeMaturity; month < Constants.START_MONTH - mother.Race.PregnancyTime; month++)
            {
                if(pregnancyCooldown <= 0)
                {
                    if(random.NextDouble() < _peopleHelper.GetPregnancyChance(father, mother, month))
                    {
                        pregnancyCooldown = tribe.Race.PregnancyTime * 4;

                        family.Add(_peopleGenerator.CreatePerson(
                            random,
                            tribe,
                            mother.Race,
                            random.NextDouble () < 0.5,
                            month + mother.Race.PregnancyTime,
                            father,
                            mother,
                            true));
                    }
                }
                else
                {
                    pregnancyCooldown -= 1;
                }
            }
            
            foreach(var person in family)
            {
                foreach(var addon in _notablePersonWorldgenAddons)
                {
                    addon.ApplyForWorldgen(
                        random,
                        tribe,
                        tribe.Race,
                        person.InDynasty,
                        person,
                        person == father);
                }
            }

            tribe.NotablePeople.AddRange(family);

            return family;
        }
    }
}
