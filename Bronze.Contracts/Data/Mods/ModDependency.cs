﻿using System;

namespace Bronze.Contracts.Data.Mods
{
    public class ModDependency
    {
        public string Name { get; }
        public Version Version { get; }

        public ModDependency(string name, Version version)
        {
            Name = name;
            Version = version;
        }
    }
}
