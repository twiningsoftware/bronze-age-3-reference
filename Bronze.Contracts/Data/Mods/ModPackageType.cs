﻿namespace Bronze.Contracts.Data.Mods
{
    public enum ModPackageType
    {
        Folder,
        RootPackagedZip,
        FolderPackagedZip
    }
}
