﻿using System;
using System.Collections.Generic;

namespace Bronze.Contracts.Data.Mods
{
    public class ModInfo
    {
        public string Filename { get; }
        public Version Version { get; }
        public string Name { get; }
        public string Author { get; }
        public string Description { get; }
        public string CoverImageKey { get; }
        public ModPackageType PackageType { get; }
        public bool IsLocked { get; }

        public List<ModDependency> Dependencies { get; }

        public ModInfo(
            string filename,
            string name,
            Version version,
            string author,
            string description,
            string coverImageKey,
            ModPackageType packageType,
            bool isLocked,
            List<ModDependency> dependencies)
        {
            Filename = filename;
            Name = name;
            Version = version;
            Author = author;
            Description = description;
            CoverImageKey = coverImageKey;
            PackageType = packageType;
            IsLocked = isLocked;
            Dependencies = dependencies;
        }

        public bool Matches(ModDependency dependency)
        {
            return Name == dependency.Name && Version >= dependency.Version;
        }
    }
}
