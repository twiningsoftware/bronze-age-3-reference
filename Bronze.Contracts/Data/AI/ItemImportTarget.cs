﻿using Bronze.Contracts.Data.Gamedata;

namespace Bronze.Contracts.Data.AI
{
    public class ItemImportTarget
    {
        public Item Item { get; set; }
        public int Value { get; set; }
        public int TargetSurplus { get; set; }
    }
}
