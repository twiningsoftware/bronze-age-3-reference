﻿namespace Bronze.Contracts.Data.AI
{
    public interface IBuildingDecision
    {
        string Id { get; }
    }
}
