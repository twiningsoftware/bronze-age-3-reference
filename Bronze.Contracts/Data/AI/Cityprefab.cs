﻿using Bronze.Contracts.Data.Gamedata;
using System;

namespace Bronze.Contracts.Data.AI
{
    public class CityPrefab
    {
        public string Id { get; set; }
        public bool IsCityStarter { get; set; }
        public CityprefabStructurePlacement[] StructurePlacements { get; set; }
        public Item[] RequiredItemAccess { get; set; }
        public IStructureType ToBorderRoads { get; set; }
        public Trait[] ConnectivityTraits { get; set; }
        public Trait[] OverlapAllowances { get; set; }
        public double? DistanceToStorage { get; set; }
    }

    public class CityprefabStructurePlacement
    {
        public int RelativeX { get; set; }
        public int RelativeY { get; set; }
        public IStructureType Structure { get; set; }
        public Recipie Recipie { get; set; }
    }

    public class CityPrefabPlacementPlan
    {
        public CityPrefab CityPrefab { get; set; }
        public Action PlaceAction { get; set; }
    }
}
