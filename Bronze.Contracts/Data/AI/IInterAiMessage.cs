﻿using Bronze.Contracts.Data.World;

namespace Bronze.Contracts.Data.AI
{
    public interface IInterAiMessage
    {
        Tribe From { get; }
        Tribe To { get; }
    }
}
