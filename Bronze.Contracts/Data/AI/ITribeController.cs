﻿using Bronze.Contracts.Data.Diplomacy;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using System;
using System.Collections.Generic;

namespace Bronze.Contracts.Data.AI
{
    public interface ITribeController
    {
        bool IsAggressive { get; }

        void DoFastAI();

        IEnumerable<Action> DoDeepAI();

        void HandleMessage(IInterAiMessage message);

        IEnumerable<string> BuildConversationContext();

        DiplomaticResponse GetDiplomaticResponse(string actionType, object actionData);

        IEnumerable<ITreatyOption> GetNegotiationAiOptions(List<ITreaty> proposedTreaties);

        IEnumerable<ITreatyOption> GetNegotiationPlayerOptions(List<ITreaty> proposedTreaties);

        IEnumerable<ITreaty> GetNegotiationAiWants(List<ITreaty> proposedTreaties);

        void SetStrategicTarget(StrategicTarget strategicTarget);

        void SerializeTo(SerializedObject controllerRoot);

        int TradeValueOf(Item item, Settlement settlement);

        int ImportNeedOf(Item item, Settlement settlement);

        int ExportAvailabilityOf(Item item, Settlement settlement);

        int ScoreRegion(Region region);
    }
}
