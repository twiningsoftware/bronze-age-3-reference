﻿using Bronze.Contracts.Data.Diplomacy;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Contracts.Data.AI
{
    public class NullTribeController : ITribeController
    {
        public bool IsAggressive => false;

        public IEnumerable<string> BuildConversationContext()
        {
            return Enumerable.Empty<string>();
        }

        public IEnumerable<Action> DoDeepAI()
        {
            return Enumerable.Empty<Action>();
        }

        public void DoFastAI()
        {
        }

        public int ExportAvailabilityOf(Item item, Settlement settlement)
        {
            return 0;
        }

        public DiplomaticResponse GetDiplomaticResponse(string actionType, object actionData)
        {
            return new DiplomaticResponse
            {
                Actions = Enumerable.Empty<IEnumerable<DiplomaticAction>>(),
                FormedMemory = null,
                ResponseText = string.Empty
            };
        }

        public IEnumerable<ITreatyOption> GetNegotiationAiOptions(List<ITreaty> proposedTreaties)
        {
            return Enumerable.Empty<ITreatyOption>();
        }

        public IEnumerable<ITreaty> GetNegotiationAiWants(List<ITreaty> proposedTreaties)
        {
            return Enumerable.Empty<ITreaty>();
        }

        public IEnumerable<ITreatyOption> GetNegotiationPlayerOptions(List<ITreaty> proposedTreaties)
        {
            return Enumerable.Empty<ITreatyOption>();
        }

        public void HandleMessage(IInterAiMessage message)
        {
        }

        public int ImportNeedOf(Item item, Settlement settlement)
        {
            return 0;
        }

        public int ScoreRegion(Region region)
        {
            return 0;
        }

        public void SerializeTo(SerializedObject controllerRoot)
        {
        }

        public void SetStrategicTarget(StrategicTarget strategicTarget)
        {
        }

        public int TradeValueOf(Item item, Settlement settlement)
        {
            return 0;
        }
    }
}
