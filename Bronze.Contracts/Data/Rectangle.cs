﻿using System.Numerics;

namespace Bronze.Contracts.Data
{
    public struct Rectangle
    {
        public int X { get; }
        public int Y { get; }
        public int Width { get; }
        public int Height { get; }

        public int Left => X;
        public int Right => X + Width;
        public int Top => Y;
        public int Bottom => Y + Height;

        public Point Center => new Point(X + Width / 2, Y + Height / 2);
        
        public Rectangle(Point point, int width, int height)
        {
            X = point.X;
            Y = point.Y;
            Width = width;
            Height = height;
        }

        public Rectangle(Vector2 point, int width, int height)
        {
            X = (int)point.X;
            Y = (int)point.Y;
            Width = width;
            Height = height;
        }

        public Rectangle(int x, int y, int width, int height)
        {
            X = x;
            Y = y;
            Width = width;
            Height = height;
        }
        
        public bool Intersects(Rectangle other)
        {
            return other.Left < Right &&
                   Left < other.Right &&
                   other.Top < Bottom &&
                   Top < other.Bottom;
        }

        public bool Contains(Point point)
        {
            return Contains(point.X, point.Y);
        }

        public bool Contains(Vector2 point)
        {
            return Contains((int)point.X, (int)point.Y);
        }

        public bool Contains(int x, int y)
        {
            return Left <= x && x < Right
                && Top <= y && y < Bottom;
        }

        public static Rectangle AroundCenter(Point center, int width, int height)
        {
            return new Rectangle(
                center.X - width / 2,
                center.Y - height / 2,
                width,
                height);
        }

        public static Rectangle AroundCenter(Vector2 center, int width, int height)
        {
            return new Rectangle(
                (int)(center.X - width / 2),
                (int)(center.Y - height / 2),
                width,
                height);
        }
    }
}
