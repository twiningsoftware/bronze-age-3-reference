﻿using Bronze.Contracts.Data.Serialization;
using System.Collections.Generic;

namespace Bronze.Contracts.Data
{
    public class Notification
    {
        public double ExpireDate { get; set; }
        public string Icon { get; set; }
        public string SoundEffect { get; set; }
        public bool AutoOpen { get; set; }
        public bool QuickDismissable { get; set; }
        public string SummaryTitle { get; set; }
        public string SummaryBody { get; set; }
        public string Handler { get; set; }
        public bool IsDismissed { get; set; }
        public bool WasOpened { get; set; }
        public bool DismissOnOpen { get; set; }
        public Dictionary<string, string> Data { get; set; }
        public Dictionary<string, SerializedObject> SerializedData { get; set; }

        public Notification()
        {
            Icon = string.Empty;
            SoundEffect = string.Empty;
            SummaryTitle = string.Empty;
            SummaryBody = string.Empty;
            Handler = string.Empty;
            Data = new Dictionary<string, string>();
            SerializedData = new Dictionary<string, SerializedObject>();
        }
    }
}
