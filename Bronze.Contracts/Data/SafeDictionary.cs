﻿using System.Collections.Generic;

namespace Bronze.Contracts.Data
{
    public class SafeDictionary<TKey, TValue>
    {
        private Dictionary<TKey, TValue> _dictionary;

        public IEnumerable<TKey> Keys => _dictionary.Keys;

        public SafeDictionary()
        {
            _dictionary = new Dictionary<TKey, TValue>();
        }

        public TValue this[TKey key]
        {
            get
            {
                if(_dictionary.ContainsKey(key))
                {
                    return _dictionary[key];
                }

                return default(TValue);
            }

            set
            {
                if (_dictionary.ContainsKey(key))
                {
                    _dictionary[key] = value;
                }
                else
                {
                    _dictionary.Add(key, value);
                }
            }
        }
    }
}
