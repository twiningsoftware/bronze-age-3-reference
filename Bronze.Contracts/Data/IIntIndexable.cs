﻿namespace Bronze.Contracts.Data
{
    public interface IIntIndexable
    {
        int LookupIndex { get; }
    }
}
