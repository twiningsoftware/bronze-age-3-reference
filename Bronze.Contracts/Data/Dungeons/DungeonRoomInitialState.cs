﻿namespace Bronze.Contracts.Data.Dungeons
{
    public class DungeonRoomInitialState
    {
        public string State { get; set; }
        public string Value { get; set; }
    }
}
