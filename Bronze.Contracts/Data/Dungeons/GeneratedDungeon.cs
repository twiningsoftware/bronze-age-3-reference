﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.InjectableServices;
using System;
using System.Linq;

namespace Bronze.Contracts.Data.Dungeons
{
    /// <summary>
    /// The current state of a generated dungeon.
    /// </summary>
    public class GeneratedDungeon
    {
        public DungeonRoom[] Rooms { get; }
        public DungeonRoom CurrentRoom { get; set; }
        private DungeonDefinition _dungeonDefinition;
        public string Name => _dungeonDefinition.Name;
        public string OverviewImage => _dungeonDefinition.OverviewImage;
        public string OverviewDescription => _dungeonDefinition.OverviewDescription;
        public UnitCategory[] AcceptableBodyguardCategories => _dungeonDefinition.AcceptableBodyguardCategories;

        public GeneratedDungeon(int seed, DungeonDefinition dungeonDefinition)
        {
            _dungeonDefinition = dungeonDefinition;
            var random = new Random(seed);

            Rooms = dungeonDefinition.RoomDefinitions
                .Select(roomDefinition => new DungeonRoom(
                        roomDefinition.Id,
                        random.Choose(roomDefinition.RoomTypeOptions)))
                .ToArray();

            SetToFirstRoom();
        }

        private GeneratedDungeon(DungeonDefinition dungeonDefinition, DungeonRoom[] dungeonRooms)
        {
            _dungeonDefinition = dungeonDefinition;
            Rooms = dungeonRooms;
            SetToFirstRoom();
        }
        
        public void SetToFirstRoom()
        {
            CurrentRoom = Rooms
                .Where(r => r.Id == _dungeonDefinition.StartingRoomId)
                .First();
        }

        public void SerializeTo(SerializedObject dungeonRoot)
        {
            dungeonRoot.Set("dungeon_definition", _dungeonDefinition.Id);

            foreach(var room in Rooms)
            {
                room.SerializeTo(dungeonRoot.CreateChild("room"));
            }
        }

        public static GeneratedDungeon DeserializeFrom(
            IDungeonDataManager dungeonDataManager, 
            SerializedObject dungeonRoot)
        {
            var dungeonDefinition = dungeonRoot
                .GetObjectReference(
                    "dungeon_definition",
                    dungeonDataManager.DungeonDefinitions,
                    dd => dd.Id);

            var rooms = dungeonRoot.GetChildren("room")
                .Select(roomRoot => DungeonRoom.DeserializeFrom(dungeonDefinition, roomRoot))
                .ToArray();

            return new GeneratedDungeon(dungeonDefinition, rooms);
        }
    }
}
