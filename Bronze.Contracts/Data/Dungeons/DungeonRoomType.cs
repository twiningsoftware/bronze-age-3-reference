﻿namespace Bronze.Contracts.Data.Dungeons
{
    /// <summary>
    /// Stores data about a type of room in a dungeon.
    /// </summary>
    public class DungeonRoomType
    {
        public string Id { get; set; }
        
        public DungeonRoomDescription[] Descriptions { get; set; }
        public IRoomResolution[] Resolutions { get; set; }
        public DungeonRoomInitialState[] InitialStates { get; set; }

        public IfState[] EscapeConditions { get; set; }
    }
}
