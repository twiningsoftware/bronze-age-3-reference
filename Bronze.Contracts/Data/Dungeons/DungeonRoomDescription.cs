﻿namespace Bronze.Contracts.Data.Dungeons
{
    public class DungeonRoomDescription
    {
        public string Title { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
        public IfState IfState { get; set; }
    }
}
