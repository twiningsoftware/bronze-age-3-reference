﻿using System.Collections.Generic;

namespace Bronze.Contracts.Data.Dungeons
{
    public class IfState
    {
        public string State { get; set; }
        public string Value { get; set; }

        public bool Matches(Dictionary<string, string> roomState)
        {
            if(string.IsNullOrWhiteSpace(State))
            {
                return true;
            }

            if(roomState.ContainsKey(State))
            {
                return roomState[State] == Value;
            }

            return false;
        }
    }
}
