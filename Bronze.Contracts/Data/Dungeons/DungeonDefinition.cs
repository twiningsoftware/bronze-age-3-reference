﻿using Bronze.Contracts.Data.Gamedata;

namespace Bronze.Contracts.Data.Dungeons
{
    /// <summary>
    /// This defines a basic dungeon that can be explored.
    /// </summary>
    public class DungeonDefinition
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string StartingRoomId { get; set; }
        public string OverviewImage { get; set; }
        public string OverviewDescription { get; set; }
        public DungeonRoomType[] RoomTypes { get; set; }
        public DungeonRoomDefinition[] RoomDefinitions { get; set; }
        public UnitCategory[] AcceptableBodyguardCategories { get; set; }
    }
}
