﻿using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using System;
using System.Collections.Generic;
using System.Xml.Linq;

namespace Bronze.Contracts.Data.Dungeons
{
    /// <summary>
    /// An interface for any action that takes place as part of a resolution.
    /// </summary>
    public interface IResolutionAction
    {
        IEnumerable<IUiElement> Apply(
            Random random,
            Cell dungeonLocation,
            GeneratedDungeon dungeon, 
            NotablePerson character, 
            IUnit bodyguard);

        IResolutionAction LoadFrom(
            IXmlReaderUtil xmlreaderUtil,
            XElement element,
            DungeonDefinition dungeonDefinition);
    }
}
