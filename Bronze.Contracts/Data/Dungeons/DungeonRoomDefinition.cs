﻿namespace Bronze.Contracts.Data.Dungeons
{
    /// <summary>
    /// Defines the initial state of a room in a dungeon.
    /// </summary>
    public class DungeonRoomDefinition
    {
        public string Id { get; set; }
        public DungeonRoomType[] RoomTypeOptions { get; set; }
    }
}