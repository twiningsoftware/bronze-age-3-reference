﻿using System.Collections.Generic;
using System.Linq;
using Bronze.Contracts.Data.Serialization;

namespace Bronze.Contracts.Data.Dungeons
{
    public class DungeonRoom
    {
        public string Id { get; }

        public DungeonRoomDescription Description => RoomType.Descriptions.Where(d => d.IfState.Matches(RoomState)).FirstOrDefault();
        public IEnumerable<IRoomResolution> Resolutions => RoomType.Resolutions.Where(r => (!r.IfStates.Any()) ||  r.IfStates.All(i => i.Matches(RoomState))).ToArray();
        public bool CanBeEscaped => !RoomType.EscapeConditions.Any() || RoomType.EscapeConditions.All(c => c.Matches(RoomState));

        public Dictionary<string, string> RoomState { get; }
        private DungeonRoomType _roomType;

        public DungeonRoom(string id, DungeonRoomType roomType)
        {
            Id = id;
            RoomState = new Dictionary<string, string>();

            RoomType = roomType;
        }

        public DungeonRoomType RoomType
        {
            get => _roomType;
            set
            {
                RoomState.Clear();
                _roomType = value;

                foreach (var initialState in _roomType.InitialStates)
                {
                    if(!RoomState.ContainsKey(initialState.State))
                    {
                        RoomState.Add(initialState.State, initialState.Value);
                    }
                    else
                    {
                        RoomState[initialState.State] = initialState.Value;
                    }
                }
            }
        }

        public void SerializeTo(SerializedObject roomRoot)
        {
            roomRoot.Set("id", Id);
            roomRoot.Set("room_type", _roomType.Id);

            foreach (var key in RoomState.Keys)
            {
                var kvp = roomRoot.CreateChild("kvp");
                kvp.Set("key", key);
                kvp.Set("value", RoomState[key]);
            }
        }

        public static DungeonRoom DeserializeFrom(
            DungeonDefinition dungeonDefinition, 
            SerializedObject roomRoot)
        {
            var room = new DungeonRoom(
                roomRoot.GetString("id"),
                roomRoot.GetObjectReference(
                    "room_type",
                    dungeonDefinition.RoomTypes,
                    rt => rt.Id));

            foreach (var kvp in roomRoot.GetChildren("kvp"))
            {
                var key = kvp.GetString("key");
                var value = kvp.GetString("value");

                if (!room.RoomState.ContainsKey(key))
                {
                    room.RoomState.Add(key, value);
                }
                else
                {
                    room.RoomState[key] = value;
                }
            }

            return room;
        }
    }
}