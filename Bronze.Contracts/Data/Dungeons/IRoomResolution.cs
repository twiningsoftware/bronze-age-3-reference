﻿using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;
using System.Xml.Linq;

namespace Bronze.Contracts.Data.Dungeons
{
    /// <summary>
    /// An option for resolving a dungeon room encounter, this is presented to the player as buttons on the UI.
    /// </summary>
    public interface IRoomResolution
    {
        string Text { get; }

        IfState[] IfStates { get; }

        IRoomResolution LoadFrom(
            IInjectionProvider injectionProvider, 
            IXmlReaderUtil xmlreaderUtil, 
            XElement element, 
            DungeonDefinition dungeonDefinition);

        ResolutionData GetResolution(
            Random random,
            Cell dungeonLocation,
            GeneratedDungeon dungeon,
            NotablePerson character,
            IUnit bodyguard);
    }
}