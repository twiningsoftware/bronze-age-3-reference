﻿using System.Linq;
using System.Xml.Linq;
using Bronze.Contracts.Data.Diplomacy;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Contracts.Data.Dungeons
{
    /// <summary>
    /// A container for data on how to apply a room resolution.
    /// </summary>
    public class ResolutionData
    {
        public string Description { get; private set; }

        public IResolutionAction[] ResolutionActions { get; private set; }

        public PitchedBattleResult BattleResult { get; set; }

        public static ResolutionData LoadFrom(
            IInjectionProvider injectionProvider,
            IXmlReaderUtil xmlReaderUtil, 
            XElement element, 
            DungeonDefinition dungeonDefinition)
        {
            var descriptionElement = xmlReaderUtil.Element(element, "description");

            var data = new ResolutionData
            {
                Description = descriptionElement.Value,
                ResolutionActions =
                    element.Elements("action")
                        .Select(e => injectionProvider
                            .BuildNamed<IResolutionAction>(xmlReaderUtil.AttributeValue(e, "type"))
                            .LoadFrom(xmlReaderUtil, e, dungeonDefinition))
                    .ToArray()
            };
            return data;
        }
    }
}
