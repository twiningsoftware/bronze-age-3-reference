﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Contracts.Data
{
    public class IntIndexableLookup<TKey, TValue> where TKey : IIntIndexable
    {
        private TValue[] _lookup;
        private TKey[] _keys;

        public IntIndexableLookup()
        {
            _lookup = new TValue[0];
            _keys = new TKey[0];
        }

        public TValue this[TKey key]
        {
            get
            {
                var index = key.LookupIndex;

                if(_lookup.Length <= index)
                {
                    return default(TValue);
                }

                return _lookup[index];
            }

            set
            {
                var index = key.LookupIndex;

                if (_lookup.Length <= index)
                {
                    var newLookup = new TValue[index + 1];
                    Array.Copy(_lookup, newLookup, _lookup.Length);
                    _lookup = newLookup;

                    var newKeys = new TKey[index + 1];
                    Array.Copy(_keys, newKeys, _keys.Length);
                    _keys = newKeys;
                }

                _lookup[index] = value;
                _keys[index] = key;
            }
        }

        public IEnumerable<TKey> Keys => _keys.Where(x => x != null);

        public void Clear()
        {
            for(var i = 0; i < _lookup.Length; i++)
            {
                _lookup[i] = default(TValue);
            }
        }
    }
}
