﻿namespace Bronze.Contracts.Data
{
    public enum ViewMode
    {
        Settlement,
        Detailed,
        Summary,
    }
}
