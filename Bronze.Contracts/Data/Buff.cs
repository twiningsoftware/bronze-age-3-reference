﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Contracts.Data
{
    public class Buff
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string IconKey { get; set; }
        public string Description { get; set; }
        public IntIndexableLookup<BonusType, double> AppliedBonuses { get; set; }
        public IntIndexableLookup<Cult, double> AppliedInfluence { get; set; }
        public double ExpireMonth { get; set; }

        public Buff()
        {
            AppliedBonuses = new IntIndexableLookup<BonusType, double>();
            AppliedInfluence = new IntIndexableLookup<Cult, double>();
        }
        
        public void SerializeTo(SerializedObject buffRoot)
        {
            buffRoot.Set("id", Id);
            buffRoot.Set("name", Name);
            buffRoot.Set("description", Description);
            buffRoot.Set("icon_key", IconKey);
            buffRoot.Set("expire_month", ExpireMonth);

            foreach (var key in AppliedBonuses.Keys)
            {
                var kvpRoot = buffRoot.CreateChild("kvp");

                kvpRoot.Set("key", key.Id);
                kvpRoot.Set("val", AppliedBonuses[key]);
            }

            foreach (var key in AppliedInfluence.Keys)
            {
                var kvpRoot = buffRoot.CreateChild("kvp_inf");

                kvpRoot.Set("key", key.Id);
                kvpRoot.Set("val", AppliedInfluence[key]);
            }
        }

        public static Buff DeserializeFrom(IGamedataTracker gameDataTracker, SerializedObject buffRoot)
        {
            var buff = new Buff
            {
                Id = buffRoot.GetString("id"),
                Name = buffRoot.GetString("name"),
                Description = buffRoot.GetOptionalString("description") ?? string.Empty,
                IconKey = buffRoot.GetString("icon_key"),
                AppliedBonuses = new IntIndexableLookup<BonusType, double>(),
                ExpireMonth = buffRoot.GetDouble("expire_month")
            };

            foreach (var kvpRoot in buffRoot.GetChildren("kvp"))
            {
                var key = kvpRoot.GetOptionalObjectReferenceOrDefault(
                    "key",
                    gameDataTracker.BonusTypes,
                    bt => bt.Id);

                if (key != null)
                {
                    var val = kvpRoot.GetDouble("val");
                    buff.AppliedBonuses[key] = val;
                }
            }

            foreach (var kvpRoot in buffRoot.GetChildren("kvp_inf"))
            {
                var key = kvpRoot.GetOptionalObjectReferenceOrDefault(
                    "key",
                    gameDataTracker.Cults,
                    c => c.Id);

                if (key != null)
                {
                    var val = kvpRoot.GetDouble("val");
                    buff.AppliedInfluence[key] = val;
                }
            }

            return buff;
        }
    }
}
