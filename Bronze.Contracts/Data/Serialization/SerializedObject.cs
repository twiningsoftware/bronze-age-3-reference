﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace Bronze.Contracts.Data.Serialization
{
    public class SerializedObject
    {
        public string Name { get; }

        public IEnumerable<SerializedObject> Children { get; private set; }
        public Dictionary<string, string> Attributes { get; set; }

        public SerializedObject(string name)
        {
            Name = name;
            Children = new List<SerializedObject>();
            Attributes = new Dictionary<string, string>();
        }
        
        public SerializedObject GetChild(string childName)
        {
            var child = Children
                .Where(c => c.Name == childName)
                .FirstOrDefault();

            if(child == null)
            {
                throw new FileLoadException($"Expected object {Name} to have child {childName}, but it does not.");
            }

            return child;
        }

        public SerializedObject GetOptionalChild(string childName)
        {
            return Children
                .Where(c => c.Name == childName)
                .FirstOrDefault();
        }

        public IEnumerable<SerializedObject> GetChildren(string childName)
        {
            return Children
                .Where(c => c.Name == childName)
                .ToArray();
        }
        
        public SerializedObject CreateChild(string name)
        {
            var newChild = new SerializedObject(name);

            Children = Children.Concat(newChild.Yield()).ToList();
            
            return newChild;
        }

        public SerializedObject AddChild(SerializedObject newChild)
        {
            Children = Children.Concat(newChild.Yield()).ToList();

            return newChild;
        }

        public void Set<T>(string name, T value)
        {
            if (value != null)
            {
                if (Attributes.ContainsKey(name))
                {
                    Attributes[name] = value.ToString();
                }
                else
                {
                    Attributes.Add(name, value.ToString());
                }
            }
            else
            {
                if (Attributes.ContainsKey(name))
                {
                    Attributes.Remove(name);
                }
            }
        }

        public string GetString(string attrName)
        {
            if(Attributes.ContainsKey(attrName))
            {
                return Attributes[attrName];
            }

            throw new FileLoadException($"Expected object {Name} to have attribute {attrName} but it does not.");
        }

        public Dictionary<string, double> GetStringDoubleDictionary()
        {
            return GetChildren("kvp")
                .ToDictionary(x => x.GetString("key"), x => x.GetDouble("val"));
        }

        public Dictionary<string, string> GetStringStringDictionary()
        {
            return GetChildren("kvp")
                .ToDictionary(x => x.GetString("key"), x => x.GetString("val"));
        }

        public string GetOptionalString(string attrName)
        {
            if (Attributes.ContainsKey(attrName))
            {
                return Attributes[attrName];
            }

            return null;
        }

        public void SetStringDoubleDictionary(Dictionary<string, double> dict)
        {
            foreach(var key in dict.Keys)
            {
                var kvp = CreateChild("kvp");
                kvp.Set("key", key);
                kvp.Set("val", dict[key]);
            }
        }

        public void SetStringStringDictionary(Dictionary<string, string> dict)
        {
            foreach (var key in dict.Keys)
            {
                var kvp = CreateChild("kvp");
                kvp.Set("key", key);
                kvp.Set("val", dict[key]);
            }
        }

        public int GetInt(string attrName)
        {
            var rawVal = GetString(attrName);

            if(int.TryParse(rawVal, out var result))
            {
                return result;
            }

            throw new FileLoadException($"Attribute {attrName} on object {Name} is expected to be an integer, but cannot be parsed as such: '{rawVal}'");
        }

        public int GetOptionalInt(string attrName)
        {
            var rawVal = GetOptionalString(attrName);

            if (rawVal == null)
            {
                return 0;
            }

            if (int.TryParse(rawVal, out var result))
            {
                return result;
            }

            throw new FileLoadException($"Attribute {attrName} on object {Name} is expected to be an integer, but cannot be parsed as such: '{rawVal}'");
        }

        public double GetDouble(string attrName)
        {
            var rawVal = GetString(attrName).Replace(',', '.');

            if (double.TryParse(rawVal, NumberStyles.Any, CultureInfo.InvariantCulture, out var result))
            {
                return result;
            }

            throw new FileLoadException($"Attribute {attrName} on object {Name} is expected to be a number, but cannot be parsed as such: '{rawVal}'");
        }

        public double GetOptionalDouble(string attrName)
        {
            var rawVal = GetOptionalString(attrName);

            if(rawVal == null)
            {
                return 0;
            }

            if (double.TryParse(rawVal.Replace(',', '.'), NumberStyles.Any, CultureInfo.InvariantCulture, out var result))
            {
                return result;
            }

            throw new FileLoadException($"Attribute {attrName} on object {Name} is expected to be a number, but cannot be parsed as such: '{rawVal}'");
        }

        public bool GetBool(string attrName)
        {
            var rawVal = GetString(attrName);

            if (bool.TryParse(rawVal, out var result))
            {
                return result;
            }

            throw new FileLoadException($"Attribute {attrName} on object {Name} is expected to be a boolean (true/false), but cannot be parsed as such: '{rawVal}'");
        }

        public bool GetOptionalBool(string attrName)
        {
            var rawVal = GetOptionalString(attrName);

            if (rawVal == null)
            {
                return false;
            }

            if (bool.TryParse(rawVal, out var result))
            {
                return result;
            }

            throw new FileLoadException($"Attribute {attrName} on object {Name} is expected to be a boolean, but cannot be parsed as such: '{rawVal}'");
        }

        public T GetObjectReference<T>(
            string attrName,
            IEnumerable<T> options, 
            Func<T, string> optionToKey)
        {
            var rawValue = GetString(attrName);

            var option = options
                .Where(o => optionToKey(o) == rawValue)
                .FirstOrDefault();

            if(option != null)
            {
                return option;
            }

            throw new FileLoadException($"Attribute {attrName} on object {Name} is expected to reference a {typeof(T).Name} but no match can be found for value '{rawValue}'.");
        }

        public T GetOptionalObjectReference<T>(
            string attrName,
            IEnumerable<T> options,
            Func<T, string> optionToKey)
        {
            if(!Attributes.ContainsKey(attrName))
            {
                return default(T);
            }

            var rawValue = GetString(attrName);

            var option = options
                .Where(o => optionToKey(o) == rawValue)
                .FirstOrDefault();

            if (option != null)
            {
                return option;
            }

            throw new FileLoadException($"Attribute {attrName} on object {Name} is expected to reference a {typeof(T).Name} but no match can be found for value '{rawValue}'.");
        }

        public T GetOptionalObjectReferenceOrDefault<T>(
            string attrName,
            IEnumerable<T> options,
            Func<T, string> optionToKey)
        {
            if (!Attributes.ContainsKey(attrName))
            {
                return default(T);
            }

            var rawValue = GetString(attrName);

            var option = options
                .Where(o => optionToKey(o) == rawValue)
                .FirstOrDefault();

            if (option != null)
            {
                return option;
            }

            return default(T);
        }

        public TEnum GetEnum<TEnum>(string attrName) where TEnum : struct
        {
            var rawValue = GetString(attrName);

            if(Enum.TryParse<TEnum>(rawValue, out var result))
            {
                return result;
            }

            throw new FileLoadException($"Attribute {attrName} on object {Name} is expected to be a {typeof(TEnum).Name}, but cannot be parsed as such.");
        }

        public override string ToString()
        {
            return $"[{Name} Children: {Children.Count()}]";
        }

        public void RemoveChild(SerializedObject childToRemove)
        {
            Children = Children.Where( c => c != childToRemove).ToList();
        }
    }
}