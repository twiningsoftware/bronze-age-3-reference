﻿using System;

namespace Bronze.Contracts.Data.Serialization
{
    public class SaveSummary
    {
        public string WorldName { get; set; }
        public double PlayedTimeSeconds { get; set; }
        public DateTime LastPlayed { get; set; }
        public string Seed { get; set; }
        public string[] ActiveMods { get; set; }
        public string PlayerRace { get; set; }
        public int SchemaVersion { get; set; }
        public string FileName { get; set; }
        public string GameVersion { get; set; }

        public SaveSummary()
        {
            ActiveMods = new string[0];
        }
    }
}
