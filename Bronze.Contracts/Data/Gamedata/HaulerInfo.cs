﻿using System.Collections.Generic;

namespace Bronze.Contracts.Data.Gamedata
{
    public class HaulerInfo
    {
        public string Id { get; set; }
        public double HaulDistance { get; set; }
        public int Capacity { get; set; }
        public IEnumerable<MovementType> MovementTypes { get; set; }
        public IEnumerable<IndividualAnimation> Animations { get; set; }
    }
}
