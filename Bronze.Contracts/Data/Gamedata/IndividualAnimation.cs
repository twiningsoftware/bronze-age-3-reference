﻿using Bronze.Contracts.InjectableServices;
using System.Numerics;

namespace Bronze.Contracts.Data.Gamedata
{
    public class IndividualAnimation
    {
        public string Name { get; set; }
        public string ImageKey { get; set; }
        public AnimationFacingType FacingType { get; set; }
        public int FrameCount { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public float LoopTime { get; set; }
        public float Layer { get; set; }
        public SoundEffectType[] SoundEffects { get; set; }

        public void Draw(
            IDrawingService drawingService, 
            Vector2 position, 
            double time,
            BronzeColor colorization, 
            Facing facing,
            float layer)
        {
            var frame = (int)((time % LoopTime) / LoopTime * FrameCount);

            var startY = 0;
            switch (facing)
            {
                case Facing.East:
                case Facing.NorthEast:
                    startY = 0 * Height;
                    break;
                case Facing.North:
                case Facing.NorthWest:
                    startY = 1 * Height;
                    break;
                case Facing.West:
                case Facing.SouthWest:
                    startY = 2 * Height;
                    break;
                case Facing.South:
                case Facing.SouthEast:
                    startY = 3 * Height;
                    break;
            }

            var startX = frame * Width;

            drawingService.DrawAnimationFrame(
                ImageKey,
                colorization,
                new Rectangle(startX, startY, Width, Height),
                position,
                1,
                false,
                Layer + layer,
                true);
        }
    }
}
