﻿using System.Collections.Generic;

namespace Bronze.Contracts.Data.Gamedata
{
    public class PeoplePartGroup
    {
        public string Id { get; set; }
        public bool Inherited { get; set; }
        public IEnumerable<PeoplePart> Options { get; set; }
    }
}