﻿using System.Collections.Generic;
using System.Xml.Linq;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;

namespace Bronze.Contracts.Data.Gamedata
{
    public interface IArmyAction
    {
        string Icon { get; }
        string Name { get; }
        string Description { get; }
        bool PathAdjacent { get; }

        void LoadFrom(IXmlReaderUtil xmlReaderUtil, Race race, XElement actionElement);

        IUiElement BuildDescriptionStack();

        bool IsValidFor(Army army, Cell target);

        bool CouldBeValidFor(Tribe tribe, Cell target);

        bool IsValidForQuickAction(Army army, Cell target);

        IEnumerable<string> GetValidationMessagesFor(Army army, Cell target);

        IArmyOrder BuildOrderFor(Army army, Cell target);

        IArmyOrder DeserializeOrderFrom(
            IGamedataTracker gamedataTracker,
            IEnumerable<Region> regions,
            Tribe tribe,
            Army army,
            SerializedObject orderRoot);
    }
}
