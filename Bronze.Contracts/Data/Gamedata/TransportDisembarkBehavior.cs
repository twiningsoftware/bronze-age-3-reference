﻿namespace Bronze.Contracts.Data.Gamedata
{
    public class TransportDisembarkBehavior
    {
        public TransportDisembarkType DisembarkType { get; set; }
        public ICellDevelopmentType ReplacementDevelopment { get; set; }
    }

    public enum TransportDisembarkType
    {
        Nothing,
        ToDevelopment
    }
}