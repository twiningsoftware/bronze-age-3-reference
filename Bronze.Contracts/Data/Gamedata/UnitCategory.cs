﻿namespace Bronze.Contracts.Data.Gamedata
{
    public class UnitCategory
    {
        public string Id { get; set; }
        public string IconKey { get; set; }
        public string Name { get; set; }
    }
}
