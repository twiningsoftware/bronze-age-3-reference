﻿using Bronze.Contracts.Data.AI;
using System.Collections.Generic;

namespace Bronze.Contracts.Data.Gamedata
{
    public class Race
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string TribeNamePattern { get; set; }
        public string SettlementNamePattern { get; set; }
        public string ArmyNamePattern { get; set; }
        public string PersonNamePattern { get; set; }
        public string Description { get; set; }
        public string ArmyFlag { get; set; }
        public string ArmyFlagIcon { get; set; }
        public bool IsPlayerAvailable { get; set; }
        public string MarriageCompatabilityGroup { get; set; }
        public List<ICellDevelopmentType> AvailableCellDevelopments { get;}
        public List<IStructureType> AvailableStructures { get; }
        public List<IUnitType> AvailableUnits { get; }
        public IEnumerable<Caste> Castes { get; set; }
        public IEnumerable<Trait> Traits { get; set; }
        
        public IStartingPeopleScheme StartingPeopleScheme { get; set; }
        public double AgeOld { get; set; }
        public double AgeMaturity { get; set; }
        public double PregnancyTime { get; set; }
        public double BasePregnancyChance { get; set; }
        public IEnumerable<PeoplePartGroup> PeoplePartGroups { get; set; }
        public IEnumerable<StartingSkillGroup> StartingSkills { get; set; }
        public IEnumerable<LikelyhoodFor<Cult>> StartingPeopleCultLikelyhoods { get; set; }
        
        public List<CityPrefab> CityPrefabs { get; private set; }

        public PopRecruitmentSource[] PopRecruitmentSources { get; set; }

        public Race()
        {
            AvailableCellDevelopments = new List<ICellDevelopmentType>();
            AvailableStructures = new List<IStructureType>();
            AvailableUnits = new List<IUnitType>();
            CityPrefabs = new List<CityPrefab>();
        }
    }
}
