﻿using System.Xml.Linq;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;

namespace Bronze.Contracts.Data.Gamedata
{
    public interface ICultRite
    {
        string Id { get; }
        string Name { get; }
        string Description { get; }
        double Cooldown { get; }
        string IconKey { get; }

        void Enact(Settlement settlement);
        void LoadFrom(
            IGamedataTracker gamedataTracker, 
            IXmlReaderUtil xmlReaderUtil, 
            XElement riteElement,
            Cult forCult);

        IStackUiElement BuildTooltip(Settlement settlement);

        bool RequirementsMet(Settlement activeSettlement);
    }
}
