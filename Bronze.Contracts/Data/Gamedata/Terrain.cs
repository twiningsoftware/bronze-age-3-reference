﻿using System.Collections.Generic;

namespace Bronze.Contracts.Data.Gamedata
{
    public class Terrain
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string[] AdjacencyGroups { get; set; }
        public IEnumerable<Trait> Traits { get; set; }
        public IEnumerable<Trait> NotTraits { get; set; }
        public IEnumerable<ITileDisplay> DisplayLayers { get; set; }
        public double LogiDistance { get; set; }
        public MinimapColor MinimapColor { get; set; }
    }
}
