﻿namespace Bronze.Contracts.Data.Gamedata
{
    public enum SpriteType
    {
        Tiles,
        Adjacency,
        Simple,
        AutoVariantTiles
    }
}
