﻿namespace Bronze.Contracts.Data.Gamedata
{
    public class DistrictGenerationTransition
    {
        public string ToAdjacency { get; set; }
        public Terrain Terrain { get; set; }
        public float Threshold { get; set; }
    }
}