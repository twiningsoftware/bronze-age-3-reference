﻿using System.Collections.Generic;

namespace Bronze.Contracts.Data.Gamedata
{
    public class Cult : IIntIndexable
    {
        public string Id { get; }
        public string Name { get; }
        public string Adjective { get; }
        public string IconKey { get; }
        public int LookupIndex { get; }
        public double OwnPopInfluence { get; }

        public IntIndexableLookup<BonusType, double> SettlementBonuses { get; set; }
        public Dictionary<RecruitmentSlotCategory, int> RecruitmentSlots { get; set; }
        public IntIndexableLookup<Cult, double> CultRelations { get; set; }
        public List<ICultRite> Rites { get; }

        public Cult(
            string id,
            string name,
            string adjective,
            string iconKey,
            int lookupIndex,
            double ownPopInfluence)
        {
            Id = id;
            Name = name;
            Adjective = adjective;
            IconKey = iconKey;
            LookupIndex = lookupIndex;
            OwnPopInfluence = ownPopInfluence;

            SettlementBonuses = new IntIndexableLookup<BonusType, double>();
            RecruitmentSlots = new Dictionary<RecruitmentSlotCategory, int>();
            CultRelations = new IntIndexableLookup<Cult, double>();
            Rites = new List<ICultRite>();
        }
    }
}
