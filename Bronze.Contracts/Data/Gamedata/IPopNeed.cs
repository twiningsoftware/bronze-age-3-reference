﻿namespace Bronze.Contracts.Data.Gamedata
{
    public interface IPopNeed
    {
        double ProsperityPerPop { get; set; }
    }
}
