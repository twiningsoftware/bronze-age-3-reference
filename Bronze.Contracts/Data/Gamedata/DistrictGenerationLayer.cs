﻿namespace Bronze.Contracts.Data.Gamedata
{
    public class DistrictGenerationLayer
    {
        public float Layer { get; set; }
        public float Weight { get; set; }
        public bool UsesNoise { get; set; }
        public int NoiseOctaves { get; set; }
        public int NoiseMultiplier { get; set; }
        public float NoiseThresholdMax { get; set; }
        public float NoiseThresholdMin { get; set; }
        public float NoDecorationWeight { get; set; }
        public Terrain[] TerrainOptions { get; set; }
        public DecorationOption[] DecorationOptions { get; set; }
        public DistrictGenerationTransition[] Transitions { get; set; }
        public DistrictGenerationBorder Border { get; set; }
    }
}