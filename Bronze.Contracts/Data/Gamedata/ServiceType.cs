﻿namespace Bronze.Contracts.Data.Gamedata
{
    public class ServiceType : IIntIndexable
    {
        public string Name { get; }
        public string IconKey { get; }
        public string AreaIconKey { get; }
        public string Description { get; }
        public int LookupIndex { get; }

        public ServiceType(
            string name, 
            string iconKey,
            string areaIconKey,
            string description, 
            int lookupIndex)
        {
            Name = name;
            IconKey = iconKey;
            AreaIconKey = areaIconKey;
            Description = description;
            LookupIndex = lookupIndex;
        }
    }
}
