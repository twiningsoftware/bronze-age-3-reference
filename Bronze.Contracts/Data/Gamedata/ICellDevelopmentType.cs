﻿using System.Collections.Generic;
using System.Xml.Linq;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;

namespace Bronze.Contracts.Data.Gamedata
{
    public interface ICellDevelopmentType : IEconomicActorType
    {
        string Id { get; }

        string IconKey { get; }

        string Name { get; }

        string Description { get; }

        string[] AdjacencyGroups { get; }

        bool UpgradeOnly { get; }

        IEnumerable<Trait> Traits { get; }

        IEnumerable<Trait> NotTraits { get; }

        IEnumerable<CellAnimationLayer> DetailLayers { get; }
        IEnumerable<CellAnimationLayer> SummaryLayers { get; }
        IEnumerable<CellAnimationLayer> ConstructionDetailLayers { get; }
        IEnumerable<CellAnimationLayer> ConstructionSummaryLayers { get; }
        
        WorkerNeed ConstructionWorkers { get; set; }

        WorkerNeed OperationWorkers { get; set; }

        double ConstructionMonths { get; }
        
        int InventoryCapacity { get; }

        IEnumerable<Recipie> Recipies { get; }

        HaulerInfo[] LogiSourceHaulers { get; }

        bool IsActivity { get; }

        int VisionRange { get; }

        IEnumerable<ICellDevelopmentType> ManuallyUpgradesTo { get; }

        ICellDevelopmentType ManuallyDowngradesTo { get; }

        IEnumerable<ItemRate> UpkeepInputs { get; }

        Trait[] TraitsToClear { get; }

        MinimapColor MinimapColor { get; }

        DevelopmentAbandonmentBehavior AbandonmentBehavior { get; }
        DevelopmentAbandonmentBehavior RaidedBehavior { get; }
        IntIndexableLookup<Item, double> ConstructionCostLookup { get; }

        bool CanBePlaced(Tribe tribe, Cell cell);
        
        bool CouldBePlacedWithoutFeature(Tribe tribe, Cell cell);

        string[] GetPlacementMessages(Tribe tribe, Cell cell);

        bool CouldBePlaced(Settlement settlement);
        
        void Place(Tribe tribe, Cell cell, bool instantBuild);

        IStackUiElement BuildTypeInfoPanel(int width, bool withName, Cell cell);
        
        void LoadFrom(IGamedataTracker gamedataTracker, IXmlReaderUtil xmlReaderUtil, XElement element);

        void PostLink(IGamedataTracker gamedataTracker, IXmlReaderUtil xmlReaderUtil, XElement element);

        void PostLoad(IGamedataTracker gamedataTracker, IXmlReaderUtil xmlReaderUtil, XElement element);

        ICellDevelopment DeserializeFrom(IGamedataTracker gamedataTracker, Cell cell, SerializedObject developmentRoot);
    }
}
