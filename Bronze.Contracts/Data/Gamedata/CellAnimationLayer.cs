﻿namespace Bronze.Contracts.Data.Gamedata
{
    public class CellAnimationLayer
    {
        public string AnimationName { get; set; }

        public float DrawLayer { get; set; }

        public string ImageKey { get; set; }

        public string Adjacency { get; set; }

        public SpriteType Type { get; set; }

        public int FrameCount { get; set; }

        public float LoopTime { get; set; }
        
        public SoundEffectType[] SoundEffects { get; set; }
    }
}
