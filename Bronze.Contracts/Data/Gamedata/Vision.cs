﻿namespace Bronze.Contracts.Data.Gamedata
{
    public class Vision
    {
        public int Range { get; set; }
        public Trait[] HinderedBy { get; set; }
        public Trait[] BlockedBy { get; set; }
    }
}
