﻿using System.Collections.Generic;

namespace Bronze.Contracts.Data.Gamedata
{
    public class TradeHaulerInfo
    {
        public IEnumerable<IndividualAnimation> Animations { get; set; }
    }
}
