﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;
using Bronze.Contracts.Data.AI;
using Bronze.Contracts.Data.Diplomacy;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;

namespace Bronze.Contracts.Data.Gamedata
{
    public interface ISettlementRole
    {
        bool CanApplyTo(Settlement settlement);
        bool IsFor(SerializedObject serializedObject);
        void Assign(Settlement settlement, SerializedObject state);
        void Seed(Tribe tribe, SerializedObject tribeState, Settlement settlement, SerializedObject settlementState);
        IEnumerable<Action> RunFor(Tribe tribe, Settlement settlement, SerializedObject settlementState, StrategicTarget tribeStrategicTarget);
        void LoadFrom(XElement element);

        IEnumerable<ItemImportTarget> GetItemImportTargets();
    }
}