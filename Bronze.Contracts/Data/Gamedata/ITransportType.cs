﻿using System.Collections.Generic;
using System.Xml.Linq;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Contracts.Data.Gamedata
{
    public interface ITransportType
    {
        string Id { get; }
        string Icon { get; }
        string Name { get; }

        IndividualAnimation[] IndividualAnimations { get; }

        MovementMode[] MovementModes { get; }
        IEnumerable<IArmyAction> AvailableActions { get; }
        bool ShowSingleTransport { get; }

        bool CanTransport(IUnitType unitType);
        bool CanTransport(IUnit unit);

        void OnDisembark(Army army);

        void OnEmbark(Army army);

        void LoadFrom(IGamedataTracker gamedataTracker, IXmlReaderUtil xmlReaderUtil, IInjectionProvider injectionProvider, XElement element);
        void PostLink(IGamedataTracker gamedataTracker, IXmlReaderUtil xmlReaderUtil, XElement element);
        bool CanPath(Cell cell);
        double GetMovementSpeed(Cell cell);       
    }
}
