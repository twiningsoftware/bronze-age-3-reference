﻿using Bronze.Contracts.Data.Simulation;
using System.Collections.Generic;

namespace Bronze.Contracts.Data.Gamedata
{
    public interface IServiceProviderActorType : IEconomicActorType
    {
        ServiceType ServiceType { get; }
        double ServiceRange { get; }
        IEnumerable<Trait> ServiceTransmissionTraits { get; }

        bool CanService(IEconomicActor economicActor);
        bool CanService(IEconomicActorType economicActorType);
    }
}
