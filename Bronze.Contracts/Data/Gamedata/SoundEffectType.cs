﻿namespace Bronze.Contracts.Data.Gamedata
{
    public class SoundEffectType
    {
        public SoundEffectMode Mode { get; set; }

        public string SoundKey { get; set; }
    }

    public enum SoundEffectMode
    {
        Instant,
        Ambient
    }
}
