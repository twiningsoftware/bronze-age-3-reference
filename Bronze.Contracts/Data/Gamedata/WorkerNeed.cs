﻿namespace Bronze.Contracts.Data.Gamedata
{
    public class WorkerNeed
    {
        public Caste Caste { get; set; }
        public int Need { get; set; }
    }
}
