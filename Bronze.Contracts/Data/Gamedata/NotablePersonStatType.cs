﻿namespace Bronze.Contracts.Data.Gamedata
{
    public enum NotablePersonStatType
    {
        Valor,
        Wit,
        Charisma
    }
}