﻿namespace Bronze.Contracts.Data.Gamedata
{
    public class RecruitmentSlotCategory
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public double RegenTimeMonths { get; set; }
        public double StrengthEstimate { get; set; }
    }
}