﻿using System.Collections.Generic;
using System.Xml.Linq;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;

namespace Bronze.Contracts.Data.Gamedata
{
    public interface ICellFeature
    {
        string Id { get; set; }

        string Name { get; set; }

        string[] AdjacencyGroups { get; set; }

        IEnumerable<Trait> Traits { get; set; }

        IEnumerable<Trait> NotTraits { get; set; }

        IEnumerable<CellAnimationLayer> DetailLayers { get; }
        IEnumerable<CellAnimationLayer> SummaryLayers { get; }

        DistrictGenerationLayer[] DistrictGenerationLayers { get; }

        MinimapColor MinimapColor { get; }

        void ApplyGeneration(IWorldManager worldManager, District district, Tile[,] tiles);

        void LoadFrom(IGamedataTracker gamedataTracker, IXmlReaderUtil xmlReaderUtil, XElement element);

        IUiElement BuildDetailColumn();
    }
}
