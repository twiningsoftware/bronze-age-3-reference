﻿namespace Bronze.Contracts.Data.Gamedata
{
    public class BonusType : IIntIndexable
    {
        public string Id { get; }
        public string Name { get; }
        public string IconKey { get; }
        public int LookupIndex { get; }

        public BonusType(
            string id,
            string name, 
            string iconKey,
            int lookupIndex)
        {
            Id = id;
            Name = name;
            IconKey = iconKey;
            LookupIndex = lookupIndex;
        }
    }
}
