﻿using Bronze.Contracts.Data.World;
using System.Collections.Generic;

namespace Bronze.Contracts.Data.Gamedata
{
    public class MovementMode
    {
        public MovementType MovementType { get; set; }

        public double Speed { get; set; }

        public bool IsValidFor(Cell nextCell)
        {
            return nextCell.Traits.ContainsAll(MovementType.RequiredTraits)
                && !nextCell.Traits.ContainsAny(MovementType.PreventedBy);
        }

        public double GetMovementSpeedFor(IEnumerable<Trait> traits)
        {
            return Speed * MovementType.GetSpeedFactor(traits);
        }
    }
}