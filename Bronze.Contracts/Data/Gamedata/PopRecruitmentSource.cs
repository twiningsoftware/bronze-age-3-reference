﻿namespace Bronze.Contracts.Data.Gamedata
{
    public class PopRecruitmentSource
    {
        public RecruitmentSlotCategory Category { get; set; }
        public Caste Caste { get; set; }
        public int PopPerSlot { get; set; }
    }
}