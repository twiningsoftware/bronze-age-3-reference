﻿using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Contracts.Data.Gamedata
{
    public class SpriteAnimation : ITileDisplay
    {
        public float DrawLayer { get; set; }
        public string ImageKey { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public string AnimationName { get; set; }
        public float LoopTime { get; set; }
        public int FrameCount { get; set; }
        public SoundEffectType[] SoundEffects { get; set; }
        
        public void DrawStructure(
            IDrawingService drawingService, 
            BronzeColor colorization,
            Tile ulTile, 
            int tilesWide, 
            int tilesHigh,
            bool[] adjacencies)
        {
            var center = (ulTile.Position.ToVector() + new System.Numerics.Vector2(tilesWide / 2f, tilesHigh / 2f)) * Constants.TILE_SIZE_PIXELS - new System.Numerics.Vector2(16,16);

            var frame = drawingService.GetFrameForAnimation(FrameCount, LoopTime);

            drawingService.DrawAnimationFrame(
                ImageKey,
                colorization,
                new Rectangle(frame * Width, 0, Width, Height),
                center,
                1,
                false,
                DrawLayer,
                true);
        }

        public void DrawTile(
            IDrawingService drawingService,
            BronzeColor colorization,
            Tile tile,
            bool[] adjacenciess)
        {
            var frame = drawingService.GetFrameForAnimation(FrameCount, LoopTime);

            drawingService.DrawAnimationFrame(
                ImageKey,
                colorization,
                new Rectangle(Width * frame, 0, Width, Height),
                tile.Position.ToVector() * Constants.TILE_SIZE_PIXELS,
                1,
                false,
                DrawLayer,
                true);
        }
    }
}