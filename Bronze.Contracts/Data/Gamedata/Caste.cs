﻿using System.Collections.Generic;

namespace Bronze.Contracts.Data.Gamedata
{
    public class Caste
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string NamePlural { get; set; }
        public bool IsDefault { get; set; }
        public string IconKey { get; set; }
        public string BigIconKey { get; set; }
        public string WorkerIcon { get; set; }
        public double GrowthRate { get; set; }
        public Race Race { get; set; }
        public IEnumerable<BonusType> SatisfactionBonusedBy { get; set; }
        public IEnumerable<BonusType> GrowthBonusedBy { get; set; }
        public IEnumerable<LikelyhoodFor<Cult>> CultLikelyhoods { get; set; }

        public IEnumerable<Caste> AscendsTo { get; set; }
        public IndividualAnimation[] IndividualAnimations { get; set; }
    }
}