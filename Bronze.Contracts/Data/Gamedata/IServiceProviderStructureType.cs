﻿using Bronze.Contracts.Data.World;
using System.Collections.Generic;

namespace Bronze.Contracts.Data.Gamedata
{
    public interface IServiceProviderStructureType : IServiceProviderActorType, IStructureType
    {
        IEnumerable<Tile> CalculateServiceArea(Tile ulTile);
    }
}
