﻿namespace Bronze.Contracts.Data.Gamedata
{
    public class DistrictGenerationBorder
    {
        public Terrain Terrain { get; set; }
        public float InterruptionsPerTile { get; set; }
        public int InterruptionMinLength { get; set; }
        public int InterruptionMaxLength { get; set; }
        public int MinimumLength { get; set; }
    }
}
