﻿using System.Collections.Generic;

namespace Bronze.Contracts.Data.Gamedata
{
    public class NotablePersonSkill
    {
        public string Id { get; set; }
        public double InheritChance { get; set; }
        public IEnumerable<NotablePersonSkillLevel> Levels { get; set; }
    }

    public class NotablePersonSkillLevel
    {
        public int Level { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public IEnumerable<NotablePersonSkillStatBonus> StatBonuses { get; set; }
        public Dictionary<Cult, double> CultInfluence { get; set; }
        public Dictionary<string, double> MinorStats { get; set; }

        public int GetStat(NotablePersonStatType statType)
        {
            var stat = 0; 

            foreach(var statBonus in StatBonuses)
            {
                if(statBonus.Stat == statType)
                {
                    stat += statBonus.Amount;
                }
            }

            return stat;
        }

        public double GetMinorStat(string statName)
        {
            if(MinorStats.ContainsKey(statName))
            {
                return MinorStats[statName];
            }

            return 0;
        }

        public double GetCultInfluence(Cult cult)
        {
            if (CultInfluence.ContainsKey(cult))
            {
                return CultInfluence[cult];
            }

            return 0;
        }
    }

    public class NotablePersonSkillStatBonus
    {
        public NotablePersonStatType Stat { get; set; }
        public int Amount { get; set; }
    }
}