﻿namespace Bronze.Contracts.Data.Gamedata
{
    public class DevelopmentAbandonmentBehavior
    {
        public AbandonmentType AbandonmentType { get; set; }
        public ICellFeature ReplacementFeature { get; set; }
    }

    public enum AbandonmentType
    {
        NoChange,
        Clear,
        ToFeature
    }
}