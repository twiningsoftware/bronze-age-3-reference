﻿using Bronze.Contracts.Data.Gamedata;

namespace Bronze.Common.Data.Game
{
    public class ItemPopNeed : IPopNeed
    {
        public Item Item { get; set; }
        public double PerMonthPerPop { get; set; }
        public double ProsperityPerPop { get; set; }
    }
}
