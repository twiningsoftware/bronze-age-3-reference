﻿using System.Collections.Generic;

namespace Bronze.Contracts.Data.Gamedata
{
    public class Biome
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string[] AdjacencyGroups { get; set; }
        public IEnumerable<Trait> Traits { get; set; }
        public IEnumerable<Trait> NotTraits { get; set; }
        public IEnumerable<CellAnimationLayer> DetailLayers { get; set; }
        public IEnumerable<CellAnimationLayer> SummaryLayers { get; set; }
        public MinimapColor MinimapColor { get; set; }
        public DistrictGenerationLayer[] DistrictGenerationLayers { get; set; }
    }
}
