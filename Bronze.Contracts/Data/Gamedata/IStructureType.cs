﻿using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using System.Collections.Generic;
using System.Xml.Linq;

namespace Bronze.Contracts.Data.Gamedata
{
    public interface IStructureType : IEconomicActorType
    {
        string Id { get; }

        string IconKey { get; }

        string Name { get; }

        string Description { get; }

        bool UpgradeOnly { get; }

        string[] AdjacencyGroups { get; }

        int TilesWide { get; }

        int TilesHigh { get; }

        IEnumerable<Trait> Traits { get; }

        IEnumerable<Trait> NotTraits { get; }

        StructurePlacementType PlacementType { get; }

        IEnumerable<ITileDisplay> AnimationLayers { get; }

        IEnumerable<ITileDisplay> ConstructionAnimationLayers { get; }

        WorkerNeed ConstructionWorkers { get; }

        WorkerNeed OperationWorkers { get; }

        double ConstructionMonths { get; }

        
        IntIndexableLookup<Item, double> ConstructionCostLookup { get; }

        int InventoryCapacity { get; }

        IEnumerable<Recipie> Recipies { get; }

        double LogiDistance { get; }

        HaulerInfo[] LogiSourceHaulers { get; }

        IEnumerable<ItemRate> UpkeepInputs { get; }

        MinimapColor MinimapColor { get; }
        
        IEnumerable<IStructureType> ManuallyUpgradesTo { get; }

        IStructureType ManuallyDowngradesTo { get; }
        
        bool CanBePlaced(Tile ulTile);

        bool CouldBePlaced(Settlement settlement);

        string[] GetPlacementMessages(Tile ulTile);

        IStructure Place(Tile ulTile);

        IStackUiElement BuildTypeInfoPanel(int width, bool withName);

        void LoadFrom(IGamedataTracker gamedataTracker, IXmlReaderUtil xmlReaderUtil, XElement element);

        void PostLink(IGamedataTracker gamedataTracker, IXmlReaderUtil xmlReaderUtil, XElement element);

        IStructure DeserializeFrom(
            IGamedataTracker gamedataTracker,
            Settlement settlement,
            Cell cell,
            Tile[,] tiles, 
            SerializedObject structureRoot);
    }
}
