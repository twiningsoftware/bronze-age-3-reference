﻿using System.Xml.Linq;

namespace Bronze.Contracts.Data.Gamedata
{
    public interface IDataMutator
    {
        void Mutate(XDocument dataDoc);
    }
}
