﻿using Bronze.Contracts.Data.World;
using System;
using System.Collections.Generic;

namespace Bronze.Contracts.Data.Gamedata
{
    public interface IStartingPeopleScheme
    {
        IEnumerable<NotablePerson> CreateInitialRulers(Random random, Tribe tribe);
    }
}
