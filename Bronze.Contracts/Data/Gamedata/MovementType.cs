﻿using System.Collections.Generic;

namespace Bronze.Contracts.Data.Gamedata
{
    public class MovementType
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string IconKey { get; set; }
        public Trait[] RequiredTraits { get; set; }
        public Trait[] SlowedBy { get; set; }
        public Trait[] SpedBy { get; set; }
        public Trait[] PreventedBy { get; set; }
        public IEnumerable<string> AnimationNames { get; set; }

        public double GetSpeedFactor(IEnumerable<Trait> traits)
        {
            var factor = 1.0;

            if(traits.ContainsAny(SpedBy))
            {
                factor += 0.5;
            }
            
            if(traits.ContainsAny(SlowedBy))
            {
                factor -= 0.5;
            }

            return factor;
        }

        public bool IsSubsetOf(MovementType other)
        {
            return other.RequiredTraits.ContainsAll(RequiredTraits);
        }
    }
}
