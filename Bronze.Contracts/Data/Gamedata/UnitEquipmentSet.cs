﻿using Bronze.Contracts.Data.World;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Contracts.Data.Gamedata
{
    public class UnitEquipmentSet
    {
        public int Level { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string IconKey { get; set; }
        public string PortraitKey { get; set; }
        public int SupplyCapacity { get; set; }
        public MovementMode[] MovementModes { get; set; }
        public int HealthPerIndividual { get; set; }
        public int ResolveSkill { get; set; }
        public int Armor { get; set; }
        public int IndividualCount { get; set; }
        public IndividualAnimation[] IndividualAnimations { get; set; }
        public ItemRate[] UpkeepNeeds { get; set; }
        public int VisionRange { get; set; }
        public IEnumerable<IArmyAction> AvailableActions { get; set; }
        public double StrengthEstimate { get; set; }
        public int SkirmishSkill { get; set; }
        public int BlockSkill { get; set; }
        public int DefenseSkill { get; set; }
        public AttackInfo[] MeleeAttacks { get; set; }
        public AttackInfo[] RangedAttacks { get; set; }
        public ItemQuantity[] RecruitmentCost { get; set; }

        public virtual bool CanPath(Cell cell)
        {
            return MovementModes
                .Select(mm => mm.MovementType)
                .Where(mm => cell.Traits.ContainsAll(mm.RequiredTraits))
                .Where(mm => !cell.Traits.ContainsAny(mm.PreventedBy))
                .Any();
        }

        public virtual bool CanPath(Tile tile)
        {
            return MovementModes
                .Select(mm => mm.MovementType)
                .Where(mm => tile.Traits.ContainsAll(mm.RequiredTraits))
                .Where(mm => !tile.Traits.ContainsAny(mm.PreventedBy))
                .Any();
        }

        public virtual double GetMovementSpeed(Cell cell)
        {
            return MovementModes
                .Where(mm => cell.Traits.ContainsAll(mm.MovementType.RequiredTraits))
                .Select(mm => mm.GetMovementSpeedFor(cell.Traits))
                .OrderByDescending(ms => ms)
                .FirstOrDefault();
        }
    }
}
