﻿using System.Collections.Generic;

namespace Bronze.Contracts.Data.Gamedata
{
    public class StartingSkillGroup
    {
        public int PickCount { get; set; }
        public int LikelyhoodNone { get; set; }
        public IEnumerable<LikelyhoodFor<NotablePersonSkill>> Options { get; set; }
    }
}