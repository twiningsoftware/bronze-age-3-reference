﻿namespace Bronze.Contracts.Data.Gamedata
{
    public class DecorationOption
    {
        public float Weight { get; set; }
        public Decoration Decoration { get; set; }
        public Terrain[] AcceptableTerrain { get; set; }
    }
}
