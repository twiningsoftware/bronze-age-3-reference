﻿namespace Bronze.Contracts.Data.Gamedata
{
    public enum StructurePlacementType
    {
        Single,
        LineDrag,
        BoxDrag
    }
}
