﻿using Bronze.Contracts.Data.World;

namespace Bronze.Contracts.Data.Gamedata
{
    public class PeoplePartKeys
    {
        public string BaseKey { get; set; }
        public string MaleKey { get; set; }
        public string FemaleKey { get; set; }
    }
}
