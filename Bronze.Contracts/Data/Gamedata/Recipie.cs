﻿using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.World;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Contracts.Data.Gamedata
{
    public class Recipie
    {
        private IEnumerable<ItemRate> _inputs;
        private IEnumerable<ItemRate> _outputs;

        public string Id { get; set; }
        public string Verb { get; set; }
        public string IconKey { get; set; }
        public string Name { get; set; }
        
        public IEnumerable<Trait> RequiredTraits { get; set; }
        public IEnumerable<Trait> BoostingTraits { get; set; }
        public IEnumerable<Trait> SlowingTraits { get; set; }
        public IEnumerable<BonusType> BonusedBy { get; set; }
        public string[] AnimationNames { get; set; }

        public IntIndexableLookup<Item, double> InputLookup { get; }
        public IntIndexableLookup<Item, double> OutputLookup { get; }

        public Recipie()
        {
            InputLookup = new IntIndexableLookup<Item, double>();
            OutputLookup = new IntIndexableLookup<Item, double>();
        }

        public IEnumerable<ItemRate> Inputs
        {
            get => _inputs;
            set
            {
                _inputs = value;

                InputLookup.Clear();
                foreach(var input in _inputs)
                {
                    InputLookup[input.Item] = input.PerMonth;
                }
            }

        }

        public IEnumerable<ItemRate> Outputs
        {
            get => _outputs;
            set
            {
                _outputs = value;

                OutputLookup.Clear();
                foreach (var output in _outputs)
                {
                    OutputLookup[output.Item] = output.PerMonth;
                }
            }
        }

        public bool IsValid(IEconomicActor actor)
        {
            var owner = actor?.Settlement?.Owner;
            
            var traits = actor.Settlement?.Owner?.Traits?.AsEnumerable() ?? Enumerable.Empty<Trait>();

            if(actor is ICellDevelopment cellDevelopment)
            {
                traits = traits.Concat(cellDevelopment.Cell.Traits);
            }
            if(actor is IStructure structure)
            {
                traits = traits.Concat(structure.Footprint.SelectMany(t => t.Traits).Distinct());
            }

            return traits.ToArray()
                .ContainsAll(RequiredTraits);
        }

        public bool ShowToPlayer(IEconomicActor actor)
        {
            var owner = actor?.Settlement?.Owner;

            if (owner != null && !Inputs.All(ir => owner.KnownItems[ir.Item]))
            {
                return false;
            }

            return IsValid(actor);
        }

        public IEnumerable<ItemRate> BoostedOutputs(IEconomicActor economicActor, IEnumerable<Trait> traits)
        {
            var boostFactor = 1 + BoostingTraits.Intersect(traits).Count() * 0.5
                - SlowingTraits.Intersect(traits).Count() * 0.5;

            var governor = economicActor?.Settlement?.Governor;

            if (governor != null)
            {
                boostFactor += governor.GetBonusProductivity();
            }

            foreach(var bonusType in BonusedBy)
            {
                boostFactor += economicActor.CurrentBonuses[bonusType];
            }

            return Outputs
                .Select(ir => new ItemRate(ir.Item, ir.PerMonth * Math.Max(0.1, boostFactor)))
                .ToArray();
        }
    }
}
