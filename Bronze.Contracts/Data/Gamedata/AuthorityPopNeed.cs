﻿namespace Bronze.Contracts.Data.Gamedata
{
    public class AuthorityPopNeed : IPopNeed
    {
        public double ProsperityPerPop { get; set; }
    }
}
