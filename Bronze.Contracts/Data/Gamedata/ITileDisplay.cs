﻿namespace Bronze.Contracts.Data.Gamedata
{
    public interface ITileDisplay
    {
        string AnimationName { get; set; }

        SoundEffectType[] SoundEffects { get; set; }
    }
}
