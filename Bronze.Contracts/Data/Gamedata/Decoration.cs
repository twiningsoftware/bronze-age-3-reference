﻿using System.Collections.Generic;

namespace Bronze.Contracts.Data.Gamedata
{
    public class Decoration
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public MinimapColor MinimapColor { get; set; }
        public IEnumerable<ITileDisplay> DisplayLayers { get; set; }
    }
}