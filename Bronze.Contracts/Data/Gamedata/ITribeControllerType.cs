﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;
using Bronze.Contracts.Data.AI;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Contracts.Data.Gamedata
{
    public interface ITribeControllerType
    {
        string Id { get; }
        ITribeController BuildController(Tribe tribe, Random random, double worldHostility, double month);
        void LoadFrom(IGamedataTracker gamedataTracker, IXmlReaderUtil xmlReaderUtil, XElement element);
        ITribeController DeserializeFrom(IEnumerable<Region> regions, SerializedObject controllerRoot, Tribe tribe);
    }
}
