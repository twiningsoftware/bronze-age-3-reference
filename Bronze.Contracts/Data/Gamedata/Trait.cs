﻿namespace Bronze.Contracts.Data.Gamedata
{
    public class Trait
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string IconKey { get; set; }
        public string Description { get; set; }
        public bool Hidden { get; set; }
    }
}
