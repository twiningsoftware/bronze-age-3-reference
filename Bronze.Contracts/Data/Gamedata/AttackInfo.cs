﻿namespace Bronze.Contracts.Data.Gamedata
{
    public class AttackInfo
    {
        public string AnimationName { get; set; }
        public int Skill { get; set; }
        public int Damage { get; set; }
        public double Cooldown { get; set; }
        public int Range { get; set; }
        public int ArmorPenetration { get; set; }
        public int Ammo { get; set; }
    }
}