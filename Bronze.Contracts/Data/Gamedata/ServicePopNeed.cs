﻿namespace Bronze.Contracts.Data.Gamedata
{
    public class ServicePopNeed : IPopNeed
    {
        public ServiceType Service { get; set; }
        public double ProsperityPerPop { get; set; }
    }
}
