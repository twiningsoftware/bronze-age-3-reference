﻿using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Contracts.Data.Gamedata
{
    public class TileAnimationLayer : ITileDisplay
    {
        public string AnimationName { get; set; }
        public float DrawLayer { get; set; }
        public string ImageKey { get; set; }
        public string Adjacency { get; set; }
        public SoundEffectType[] SoundEffects { get; set; }
        public TileLayout TileLayout { get; set; }
        
        public void DrawStructure(
            IDrawingService drawingService, 
            BronzeColor colorization,
            Tile ulTile, 
            int tilesWide, 
            int tilesHigh,
            bool[] adjacencies)
        {
            for(var i = 0; i < tilesWide; i++)
            {
                for(var j = 0; j < tilesHigh; j++)
                {
                    var tile = ulTile.District.Settlement.GetTile(ulTile.Position.Relative(i, j));

                    if(tile != null)
                    {
                        DrawTile(drawingService, colorization, tile, adjacencies);
                    }
                }
            }
        }

        public void DrawTile(IDrawingService drawingService, BronzeColor colorization, Tile tile, bool[] adjacencies)
        {
            drawingService.OptimizedTileDraw(
                ImageKey,
                colorization,
                tile.Position.ToVector() * Constants.TILE_SIZE_PIXELS,
                DrawLayer,
                adjacencies,
                true,
                TileLayout);
        }
    }
}