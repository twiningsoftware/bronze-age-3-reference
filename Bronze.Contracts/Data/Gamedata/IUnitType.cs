﻿using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System.Xml.Linq;

namespace Bronze.Contracts.Data.Gamedata
{
    public interface IUnitType
    {
        string Id { get; }
        UnitCategory Category { get; }
        string Name { get; }
        string Description { get; }
        UnitEquipmentSet[] EquipmentSets { get; }
        bool CanBeTrained { get; }

        RecruitmentSlotCategory RecruitmentCategory { get; }
        
        IUnit CreateUnit(UnitEquipmentSet equipmentSet);
        
        void LoadFrom(
            IGamedataTracker gamedataTracker,
            IXmlReaderUtil xmlReaderUtil,
            IInjectionProvider injectionProvider,
            XElement element);

        void PostLink(
            IGamedataTracker gamedataTracker, 
            IXmlReaderUtil xmlReaderUtil, 
            XElement element);

        IUnit DeserializeFrom(
            IGamedataTracker gamedataTracker,
            SerializedObject unitRoot);
    }
}
