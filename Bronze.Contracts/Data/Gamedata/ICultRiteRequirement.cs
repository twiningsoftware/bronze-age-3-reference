﻿using Bronze.Contracts.Data.World;

namespace Bronze.Contracts.Data.Gamedata
{
    public interface ICultRiteRequirement
    {
        bool IsValidFor(Settlement settlement);
        string GetErrorMessageFor(Settlement settlement);
    }
}
