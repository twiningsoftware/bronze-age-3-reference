﻿using Bronze.Contracts.Data.Simulation;

namespace Bronze.Contracts.Data.Gamedata
{
    public interface IRecruitmentSlotProviderType : IEconomicActorType
    {
        RecruitmentSlotCategory RecruitmentCategory { get; }
        int ProvidedRecruitmentSlots { get; }
    }
}
