﻿using Bronze.Contracts.Data.World;

namespace Bronze.Contracts.Data.Gamedata
{
    public interface ITargetedCultRite : ICultRite
    {
        string DetailedTargetingIcon { get; }
        string SummaryTargetingIcon { get; }

        bool IsValidFor(Settlement settlement, Cell target);

        string GetErrorMessageFor(Settlement settlement, Cell target);

        void Enact(Settlement settlement, Cell target);
    }
}
