﻿using Bronze.Contracts.Data.World;

namespace Bronze.Contracts.Data.Gamedata
{
    public class PeoplePart
    {
        public string Id { get; set; }
        public double Layer { get; set; }
        public PeoplePartKeys BaseKeys { get; set; }
        public PeoplePartKeys GeneralKeys { get; set; }
        public PeoplePartKeys RulerKeys { get; set; }
        public PeoplePartKeys DeadKeys { get; set; }
        
        public string GetKeyFor(NotablePerson person)
        {
            string key = null;
            if(person.IsDead && DeadKeys != null)
            {
                key = (person.IsMale ? DeadKeys.MaleKey : DeadKeys.FemaleKey) ?? DeadKeys.BaseKey;
            }
            else if (person.Owner.Ruler == person && RulerKeys != null)
            {
                key = (person.IsMale ? RulerKeys.MaleKey : RulerKeys.FemaleKey) ?? RulerKeys.BaseKey;
            }
            else if (person.GeneralOf != null && GeneralKeys != null)
            {
                key = (person.IsMale ? GeneralKeys.MaleKey : GeneralKeys.FemaleKey) ?? GeneralKeys.BaseKey;
            }
            else if (key == null)
            {
                key = (person.IsMale ? BaseKeys.MaleKey : BaseKeys.FemaleKey) ?? BaseKeys.BaseKey;
            }

            return key;
        }
    }
}
