﻿namespace Bronze.Contracts.Data.Gamedata
{
    public class Item : IIntIndexable
    {
        public string Name { get; }
        public string IconKey { get; }
        public string IconKeyBig { get; }
        public string Description { get; }
        public int LookupIndex { get; }

        public Item(
            string name, 
            string iconKey, 
            string iconKeyBig,
            string description, 
            int lookupIndex)
        {
            Name = name;
            IconKey = iconKey;
            IconKeyBig = iconKeyBig;
            Description = description;
            LookupIndex = lookupIndex;
        }
    }
}
