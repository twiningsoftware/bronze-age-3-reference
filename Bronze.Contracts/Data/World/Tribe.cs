﻿using Bronze.Contracts.Data.AI;
using Bronze.Contracts.Data.Diplomacy;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Contracts.Data.World
{
    public delegate void ItemDiscoveredEventHandler(Item item);

    public class Tribe
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public BronzeColor PrimaryColor { get; set; }
        public BronzeColor SecondaryColor { get; set; }
        public Race Race { get; set; }
        public List<Trait> Traits { get; }
        public List<Settlement> Settlements { get; }
        public List<Army> Armies { get; }
        public List<NotablePerson> NotablePeople { get; }
        public NotablePerson Ruler { get; set; }
        public NotablePerson Heir { get; set; }
        public bool IsExtinct { get; set; }
        public bool IsBandits { get; set; }
        public ITribeController Controller { get; set; }
        public IReadOnlyList<Buff> Buffs { get; }

        public Settlement Capital { get; set; }
        
        public bool[,,] WorldVisibility { get; set; }
        public bool[,,] WorldExplored { get; set; }
        public IntIndexableLookup<Item, bool> KnownItems { get; }
        public double LastCapitalChangeMonth { get; set; }

        private HashSet<string> _tribeKnowledge;
        public IReadOnlyList<DiplomaticMemory> DiplomaticMemories { get; private set; }
        private List<DiplomaticMemory> _diplomaticMemories;
        private Dictionary<Tribe, bool> _hostility;
        
        public event ItemDiscoveredEventHandler OnItemDiscovered;

        public List<War> CurrentWars { get; }
        public List<Tribe> AllowedAccess { get; set; }

        public int Authority { get; set; }
        public int TotalPopulation { get; set; }
        public int AuthorityPerSettlement { get; set; }
        private List<Buff> _buffs;

        public Tribe()
        {
            Id = Guid.NewGuid().ToString();
            Traits = new List<Trait>();
            Settlements = new List<Settlement>();
            Armies = new List<Army>();
            KnownItems = new IntIndexableLookup<Item, bool>();
            NotablePeople = new List<NotablePerson>();
            WorldVisibility = new bool[0, 0, 0];
            WorldExplored = new bool[0, 0, 0];
            _tribeKnowledge = new HashSet<string>();
            CurrentWars = new List<War>();
            AllowedAccess = new List<Tribe>();
            _hostility = new Dictionary<Tribe, bool>();
            Controller = new NullTribeController();
            _diplomaticMemories = new List<DiplomaticMemory>();
            DiplomaticMemories = _diplomaticMemories.AsReadOnly();
            _buffs = new List<Buff>();
            Buffs = _buffs.AsReadOnly();
        }
        
        public void AddKnowledge(Tribe otherTribe)
        {
            _tribeKnowledge.Add(otherTribe.Id);
        }

        public bool HasKnowledgeOf(Tribe otherTribe)
        {
            return otherTribe == this || _tribeKnowledge.Contains(otherTribe.Id);
        }

        public bool IsHostileTo(Tribe otherTribe)
        {
            if(otherTribe == null || otherTribe == this)
            {
                return false;
            }

            if(IsBandits || otherTribe.IsBandits)
            {
                return true;
            }

            return _hostility.ContainsKey(otherTribe) 
                && _hostility[otherTribe];
        }

        public void SetHostilityTo(Tribe otherTribe, bool hostility)
        {
            if(_hostility.ContainsKey(otherTribe))
            {
                _hostility[otherTribe] = hostility;
            }
            else
            {
                _hostility.Add(otherTribe, hostility);
            }
        }

        public bool IsVisible(Cell cell)
        {
            try
            {
                if(WorldVisibility.GetLength(0) == 0 || cell == null)
                {
                    return false;
                }

                return WorldVisibility[cell.Position.Plane, cell.Position.X, cell.Position.Y];
            }
            catch (IndexOutOfRangeException ex)
            {
                throw new Exception($"Cell position {cell.Position} out of bounds", ex);
            }
        }

        public bool IsExplored(Cell cell)
        {
            try
            {
                if (WorldExplored.GetLength(0) == 0 || cell == null)
                {
                    return false;
                }

                return WorldExplored[cell.Position.Plane, cell.Position.X, cell.Position.Y];
            }
            catch (IndexOutOfRangeException ex)
            {
                throw new Exception($"Cell position {cell.Position} out of bounds", ex);
            }
        }

        public void RemoveDiplomaticMemory(string category)
        {
            _diplomaticMemories.RemoveAll(m => m.Category == category);
        }

        public void DiscoverItem(Item item)
        {
            KnownItems[item] = true;
            OnItemDiscovered?.Invoke(item);
        }

        public void SerializeTo(SerializedObject tribeRoot)
        {
            tribeRoot.Set("id", Id);
            tribeRoot.Set("name", Name);
            tribeRoot.Set("primary_color", PrimaryColor.ToString());
            tribeRoot.Set("secondary_color", SecondaryColor.ToString());
            tribeRoot.Set("race_id", Race.Id);
            tribeRoot.Set("last_capital_change_month", LastCapitalChangeMonth);
            tribeRoot.Set("is_extinct", IsExtinct);
            tribeRoot.Set("is_bandits", IsBandits);

            if (Capital != null)
            {
                tribeRoot.Set("capital_id", Capital.Id);
            }

            Controller.SerializeTo(tribeRoot.CreateChild("controller"));
            
            foreach(var trait in Traits)
            {
                var traitRoot = tribeRoot.CreateChild("trait");
                traitRoot.Set("id", trait.Id);
            }

            foreach(var settlement in Settlements)
            {
                settlement.SerializeTo(tribeRoot.CreateChild("settlement"));
            }

            foreach(var army in Armies)
            {
                army.SerializeTo(tribeRoot.CreateChild("army"));
            }
            
            var planeCount = WorldExplored.GetLength(0);
            var worldSize = WorldExplored.GetLength(1);
            var data = new char[planeCount * worldSize * worldSize];
            var index = 0;
            for (var planeId = 0; planeId < planeCount; planeId++)
            {
                for (var x = 0; x < worldSize; x++)
                {
                    for (var y = 0; y < worldSize; y++)
                    {
                        if (WorldExplored[planeId, x, y])
                        {
                            data[index] = '1';
                        }
                        else
                        {
                            data[index] = '0';
                        }

                        index += 1;
                    }
                }
            }

            
            var exploredRoot = tribeRoot.CreateChild("explored");
            exploredRoot.Set("plane_count", planeCount);
            exploredRoot.Set("world_size", worldSize);
            exploredRoot.Set("data", new string(data));

            foreach (var key in KnownItems.Keys)
            {
                if (KnownItems[key])
                {
                    var itemKnowledgeRoot = tribeRoot.CreateChild("item_knowledge");
                    itemKnowledgeRoot.Set("item", key.Name);
                }
            }

            foreach(var tribeId in _tribeKnowledge)
            {
                var knowledgeRoot = tribeRoot.CreateChild("known_tribe");
                knowledgeRoot.Set("tribe_id", tribeId);
            }

            foreach (var memory in DiplomaticMemories)
            {
                memory.SerializeTo(tribeRoot.CreateChild("diplo_memory"));
            }

            foreach (var buff in _buffs)
            {
                buff.SerializeTo(tribeRoot.CreateChild("buff"));
            }
        }

        public bool AllowsAccess(Tribe otherTribe)
        {
            if(otherTribe == this)
            {
                return true;
            }

            return AllowedAccess.Contains(otherTribe)
                || IsHostileTo(otherTribe)
                || CurrentWars.Any(w => w.Aggressor == otherTribe || w.Defender == otherTribe);
        }

        public static Tribe DeserializeFrom(
            IWorldManager worldManager,
            IGamedataTracker gamedataTracker, 
            IEnumerable<Region> regions,
            SerializedObject tribeRoot)
        {
            var tribe = new Tribe()
            {
                Id = tribeRoot.GetString("id"),
                Name = tribeRoot.GetString("name"),
                PrimaryColor = tribeRoot.GetEnum<BronzeColor>("primary_color"),
                SecondaryColor = tribeRoot.GetEnum<BronzeColor>("secondary_color"),
                LastCapitalChangeMonth = tribeRoot.GetOptionalDouble("last_capital_change_month"),
                Race = tribeRoot.GetObjectReference(
                    "race_id",
                    gamedataTracker.Races,
                    r => r.Id),
                IsExtinct = tribeRoot.GetOptionalBool("is_extinct"),
                IsBandits = tribeRoot.GetOptionalBool("is_bandits")
            };
            
            foreach(var traitRoot in tribeRoot.GetChildren("trait"))
            {
                tribe.Traits.Add(traitRoot.GetObjectReference(
                    "id",
                    gamedataTracker.Traits,
                    t => t.Id));
            }

            foreach(var settlementRoot in tribeRoot.GetChildren("settlement"))
            {
                tribe.Settlements.Add(Settlement.DeserializeFrom(gamedataTracker, regions, tribe, settlementRoot));
            }

            var capitalId = tribeRoot.GetOptionalString("capital_id");

            if(capitalId != null)
            {
                tribe.Capital = tribe.Settlements
                    .Where(s => s.Id == capitalId)
                    .FirstOrDefault();
            }
            
            foreach (var armyRoot in tribeRoot.GetChildren("army"))
            {
                tribe.Armies.Add(Army.DeserializeFrom(worldManager, gamedataTracker, regions, tribe, armyRoot));
            }

            var exploredRoot = tribeRoot.GetChild("explored");
            var planeCount = exploredRoot.GetInt("plane_count");
            var worldSize = exploredRoot.GetInt("world_size");
            var data = exploredRoot.GetString("data");
            tribe.WorldVisibility = new bool[planeCount, worldSize, worldSize];
            tribe.WorldExplored = new bool[planeCount, worldSize, worldSize];
            var index = 0;
            for (var planeId = 0; planeId < planeCount; planeId++)
            {
                for (var x = 0; x < worldSize; x++)
                {
                    for (var y = 0; y < worldSize; y++)
                    {
                        tribe.WorldExplored[planeId, x, y] = data[index] == '1';
                        
                        index += 1;
                    }
                }
            }

            foreach (var itemKnowledgeRoot in tribeRoot.GetChildren("item_knowledge"))
            {
                var item = itemKnowledgeRoot.GetObjectReference(
                    "item",
                    gamedataTracker.Items,
                    i => i.Name);

                tribe.KnownItems[item] = true;
            }

            foreach (var knowledgeRoot in tribeRoot.GetChildren("known_tribe"))
            {
                tribe._tribeKnowledge.Add(knowledgeRoot.GetString("tribe_id"));
            }

            foreach (var memoryRoot in tribeRoot.GetChildren("diplo_memory"))
            {
                tribe.AddDiplomaticMemory(DiplomaticMemory.DeserializeFrom(memoryRoot));
            }

            var controllerRoot = tribeRoot.GetChild("controller");

            var controllerType = controllerRoot.GetOptionalObjectReference(
                    "type_id",
                    gamedataTracker.ControllerTypes,
                    x => x.Id);

            if(controllerType != null)
            {
                tribe.Controller = controllerType.DeserializeFrom(regions, controllerRoot, tribe);
            }

            foreach (var buffRoot in tribeRoot.GetChildren("buff"))
            {
                tribe._buffs.Add(Buff.DeserializeFrom(gamedataTracker, buffRoot));
            }

            return tribe;
        }

        public void Abandon(Settlement settlement)
        {
            foreach (var cell in settlement.Region.Cells)
            {
                if(cell.Development != null)
                {
                    cell.Development.Abandon();
                }
            }

            Settlements.Remove(settlement);

            settlement.Region.Settlement = null;
            settlement.IsRemoved = true;

            if(Capital == settlement)
            {
                Capital = null;
            }
        }

        public void AddDiplomaticMemory(DiplomaticMemory diplomaticMemory)
        {
            RemoveDiplomaticMemory(diplomaticMemory.Category);
            _diplomaticMemories.Add(diplomaticMemory);
        }

        public void AddBuff(Buff buff)
        {
            _buffs.Add(buff);

            foreach(var settlement in Settlements)
            {
                settlement.SetCalculationFlag("tribe buff added");
            }
        }

        public void RemoveBuff(Buff buff)
        {
            _buffs.Remove(buff);

            foreach (var settlement in Settlements)
            {
                settlement.SetCalculationFlag("tribe buff added");
            }
        }
    }
}
