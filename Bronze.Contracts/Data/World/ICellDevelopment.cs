﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.UI;
using System.Collections.Generic;

namespace Bronze.Contracts.Data.World
{
    public interface ICellDevelopment: IEconomicActor
    {
        bool CanBeRaided { get; }
        double Devastation { get; set; }
        string[] AdjacencyGroups { get; }
        bool CanBeSieged { get; }
        bool CanBeDestroyed { get; }

        IEnumerable<Trait> Traits { get; }

        IEnumerable<Trait> NotTraits { get; }

        IEnumerable<CellAnimationLayer> DetailLayers { get; }
        IEnumerable<CellAnimationLayer> SummaryLayers { get; }

        int VisionRange { get; }

        IEnumerable<ICellDevelopmentType> ManuallyUpgradesTo { get; }
        bool CanManuallyUpgrade { get; }
        bool CanManuallyDowngrade { get; }
        ICellDevelopmentType ManuallyDowngradesTo { get; }
        ICellDevelopmentType UpgradingTo { get; }

        Cell Cell { get; }

        MinimapColor MinimapColor { get; }

        IEnumerable<string> GetAnimationsNames();
        
        IStackUiElement BuildDetailColumn(bool playerOwned, bool playerSees);

        void Abandon();

        void WasRaided();

        void SerializeTo(SerializedObject developmentRoot);
        
        void OnRemoval();

        void UpgradeTo(ICellDevelopmentType cellDevelopmentType);

        void CancelUpgrading();

        bool AllowsAccess(Army army);
    }
}
