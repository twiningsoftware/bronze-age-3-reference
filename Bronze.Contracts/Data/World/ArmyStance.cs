﻿namespace Bronze.Contracts.Data.World
{
    public enum ArmyStance
    {
        Agressive,
        Defensive,
        Evasive,
        Harassing
    }
}