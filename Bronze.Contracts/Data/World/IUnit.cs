﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using System.Collections.Generic;
using System.Numerics;

namespace Bronze.Contracts.Data.World
{
    public interface IUnit
    {
        string IconKey { get; }
        string PortraitKey { get; }
        string Name { get; }
        string Description { get; }
        UnitCategory Category { get; }
        double Supplies { get; set; }
        double MaxSupplies { get; }
        double SuppliesPercent { get; }
        int VisionRange { get; }
        MovementMode[] MovementModes { get; }
        double Health { get; set; }
        int MaxHealth { get; }
        double HealthPercent { get; }
        double Morale { get; set; }
        int Armor { get; }
        int IndividualCount { get; }
        IndividualAnimation[] IndividualAnimations { get; }
        double AnimationTime { get; set; }
        ItemRate[] UpkeepNeeds { get; }
        bool SufferingAttrition { get; }
        UnitEquipmentSet Equipment { get; set; }
        RecruitmentSlotCategory RecruitmentCategory { get; }
        Vector2? LastDrawPosition { get; set; }

        IEnumerable<IArmyAction> AvailableActions { get; }
        
        IndividualAnimation CurrentAnimation { get; set; }
        IUnitType UnitType { get; }
        bool CanResupply { get; }
        double StrengthEstimate { get; }
        int SkirmishSkill { get; }
        int BlockSkill { get; }
        int DefenseSkill { get; }
        AttackInfo[] MeleeAttacks { get; }
        AttackInfo[] RangedAttacks { get; }
        int ResolveSkill { get; }
        double MoraleMod { get; }

        void SetAnimationForMovement(Army army, IEnumerable<Trait> terrainTraits);
        void SetAnimation(Army army, IEnumerable<string> animationName);
        void SetInstantAnimation(Army army, IEnumerable<string> animationName);

        bool CanPath(Cell cell);
        bool CanPath(Tile tile);
        double GetMovementSpeed(Cell cell);

        void SerializeTo(SerializedObject unitRoot);

        void Update(double elapsedMonths, Army army);
        void OnDisbanded();
    }
}
