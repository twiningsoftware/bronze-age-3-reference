﻿using Bronze.Contracts.Data.Gamedata;

namespace Bronze.Contracts.Data.World
{
    public interface ICultInfluenceServiceProvider : IServiceProviderActor
    {
        Cult Cult { get; }
        double InfluencePerPop { get; }
    }
}
