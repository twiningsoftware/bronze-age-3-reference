﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace Bronze.Contracts.Data.World
{
    public class WorldCorpse : IWorldActor
    {
        public string Id { get; private set; }
        public Cell Cell { get; set; }
        public Vector2 Position { get; set; }
        public Facing Facing { get; set; }
        public BronzeColor Colorization { get; set; }
        public IUnitType UnitType { get; }
        public UnitEquipmentSet UnitEquipmentSet { get; }
        public Race PopRace { get; }
        public Caste PopCaste { get; }
        public bool IsRemoved { get; set; }
        public double DecayTime { get; set; }
        public bool IsExpired => DecayTime >= (_corpseAnimation?.LoopTime ?? 0);

        private IndividualAnimation _corpseAnimation;
        private IndividualAnimation _deathAnimation;

        public WorldCorpse(
            Cell cell, 
            Vector2 worldPosition, 
            Facing facing, 
            BronzeColor colorization, 
            IUnitType unitType,
            UnitEquipmentSet equipment)
        {
            Id = Guid.NewGuid().ToString();
            Cell = cell;
            Position = worldPosition;
            Facing = Facing;
            Colorization = colorization;
            UnitType = unitType;
            UnitEquipmentSet = equipment;

            _deathAnimation = UnitEquipmentSet.IndividualAnimations.Where(a => a.Name == "death").FirstOrDefault();
            _corpseAnimation = UnitEquipmentSet.IndividualAnimations.Where(a => a.Name == "corpse").FirstOrDefault();
        }

        public WorldCorpse(
            Cell cell,
            Vector2 worldPosition,
            Facing facing,
            BronzeColor colorization,
            Race popRace,
            Caste popCaste)
        {
            Id = Guid.NewGuid().ToString();
            Cell = cell;
            Position = worldPosition;
            Facing = Facing;
            Colorization = colorization;
            PopRace = popRace;
            PopCaste = popCaste;

            _deathAnimation = PopCaste.IndividualAnimations.Where(a => a.Name == "death").FirstOrDefault();
            _corpseAnimation = PopCaste.IndividualAnimations.Where(a => a.Name == "corpse").FirstOrDefault();
        }

        public void Draw(IDrawingService drawingService, ViewMode viewMode, bool isSelected)
        {
            if(viewMode == ViewMode.Detailed && _corpseAnimation != null)
            {
                IndividualAnimation currentAnimation;

                if (_deathAnimation != null && DecayTime < _deathAnimation.LoopTime)
                {
                    currentAnimation = _deathAnimation;
                }
                else
                {
                    currentAnimation = _corpseAnimation;
                }

                currentAnimation.Draw(
                    drawingService,
                    Position * Constants.CELL_SIZE_PIXELS,
                    DecayTime,
                    Colorization,
                    Facing,
                    0);
            }
        }

        public IEnumerable<SoundEffectType> GetSoundEffects()
        {
            return Enumerable.Empty<SoundEffectType>();
        }

        public void SerializeTo(SerializedObject corpseRoot)
        {
            corpseRoot.Set("id", Id);
            Cell.Position.SerializeTo(corpseRoot.CreateChild("cell_position"));
            corpseRoot.Set("x", Position.X);
            corpseRoot.Set("y", Position.Y);
            corpseRoot.Set("facing", Facing);
            corpseRoot.Set("colorization", Colorization);
            corpseRoot.Set("unit_type_id", UnitType?.Id);
            corpseRoot.Set("equipment_set_level", UnitEquipmentSet?.Level);
            corpseRoot.Set("pop_race", PopRace?.Id);
            corpseRoot.Set("pop_caste", PopCaste?.Id);
            corpseRoot.Set("decay_time", DecayTime);
        }

        public static WorldCorpse DeserializeFrom(
            IGamedataTracker gamedataTracker, 
            IWorldManager worldManager,
            SerializedObject corpseRoot)
        {
            var position = CellPosition.DeserializeFrom(corpseRoot.GetChild("cell_position"));
            
            var unitType = corpseRoot.GetOptionalObjectReference(
                "unit_type_id",
                gamedataTracker.UnitTypes,
                u => u.Id);

            var popRace = corpseRoot.GetOptionalObjectReference(
                "pop_race",
                gamedataTracker.Races,
                u => u.Id);

            if(unitType != null)
            {
                var equipment = corpseRoot.GetObjectReference(
                    "equipment_set_level",
                    unitType.EquipmentSets,
                    es => es.Level.ToString());

                return new WorldCorpse(
                    worldManager.GetCell(position),
                    new Vector2(
                        (float)corpseRoot.GetDouble("x"),
                        (float)corpseRoot.GetDouble("y")),
                    corpseRoot.GetEnum<Facing>("facing"),
                    corpseRoot.GetEnum<BronzeColor>("colorization"),
                    unitType,
                    equipment)
                {
                    Id = corpseRoot.GetString("id"),
                    DecayTime = corpseRoot.GetDouble("decay_time"),
                };
            }
            else
            {
                var popCaste = corpseRoot.GetObjectReference(
                    "pop_caste",
                    popRace.Castes,
                    c => c.Id);

                return new WorldCorpse(
                    worldManager.GetCell(position),
                    new Vector2(
                        (float)corpseRoot.GetDouble("x"),
                        (float)corpseRoot.GetDouble("y")),
                    corpseRoot.GetEnum<Facing>("facing"),
                    corpseRoot.GetEnum<BronzeColor>("colorization"),
                    popRace,
                    popCaste)
                {
                    Id = corpseRoot.GetString("id"),
                    DecayTime = corpseRoot.GetDouble("decay_time"),
                };
            }

            
        }
    }
}
