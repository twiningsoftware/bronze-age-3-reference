﻿using Bronze.Contracts.Data.Gamedata;

namespace Bronze.Contracts.Data.World
{
    public struct ItemRate
    {
        public Item Item { get; }
        public double PerMonth { get; }

        public ItemRate(Item item, double perMonth)
        {
            Item = item;
            PerMonth = perMonth;
        }

        public ItemQuantity ToQuantity(double deltaMonths)
        {
            return new ItemQuantity(Item, PerMonth * deltaMonths);
        }
    }
}