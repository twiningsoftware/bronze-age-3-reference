﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Contracts.Data.World
{
    public class ItemInventory
    {
        private int _maxCapacity;
        private Dictionary<Item, double> _inventory;

        public int RemainingCapacity => _maxCapacity - UsedCapacity;

        public int UsedCapacity => (int)_inventory.Sum(kvp => kvp.Value);

        public IEnumerable<Item> ItemsPresent => _inventory.Where(kvp => kvp.Value >= 1).Select(kvp => kvp.Key);

        public ItemInventory()
            : this(int.MaxValue)
        {

        }

        public ItemInventory(int maxCapacity)
        {
            _inventory = new Dictionary<Item, double>();
            _maxCapacity = maxCapacity;
        }

        public void Add(ItemQuantity itemQuantity)
        {
            Add(itemQuantity.Item, itemQuantity.Quantity);
        }

        public void Remove(ItemQuantity itemQuantity)
        {
            Remove(itemQuantity.Item, itemQuantity.Quantity);
        }

        public void Add(Item item, double qty)
        {
            // Don't exceed max capacity
            qty = Math.Min(qty, RemainingCapacity);

            if (!_inventory.ContainsKey(item))
            {
                _inventory.Add(item, qty);
            }
            else
            {
                _inventory[item] += qty;
            }
        }

        public void Remove(Item item, double qty)
        {
            if (!_inventory.ContainsKey(item))
            {
                _inventory.Add(item, 0);
            }
            else
            {
                _inventory[item] = Math.Max(0, _inventory[item] - qty);
            }
        }

        public double QuantityOf(Item item)
        {
            if (!_inventory.ContainsKey(item))
            {
                return 0;
            }
            else
            {
                return _inventory[item];
            }
        }

        public void TransferTo(ItemInventory reciever)
        {
            foreach (var item in _inventory.Keys.ToArray())
            {
                reciever.Add(item, QuantityOf(item));
            }

            _inventory.Clear();
        }

        public bool HasAll(IEnumerable<ItemQuantity> itemQuantities)
        {
            foreach (var iq in itemQuantities)
            {
                if (iq.Quantity > QuantityOf(iq.Item))
                {
                    return false;
                }
            }

            return true;
        }

        public void RemoveAll(IEnumerable<ItemQuantity> itemQuantities)
        {
            foreach (var itemQuantity in itemQuantities)
            {
                Remove(itemQuantity);
            }
        }

        public void AddAll(IEnumerable<ItemQuantity> itemQuantities)
        {
            foreach (var itemQuantity in itemQuantities)
            {
                Add(itemQuantity);
            }
        }

        public void SerializeTo(SerializedObject inventoryRoot)
        {
            foreach(var kvp in _inventory)
            {
                inventoryRoot.Set(kvp.Key.Name, kvp.Value);
            }
        }

        public void DeserializeFrom(
            IGamedataTracker gamedataTracker, 
            SerializedObject inventoryRoot)
        {
            foreach(var item in gamedataTracker.Items)
            {
                var amount = inventoryRoot.GetOptionalDouble(item.Name);

                if(amount > 0)
                {
                    Add(item, amount);
                }
            }
        }
    }
}