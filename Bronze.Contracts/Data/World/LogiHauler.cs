﻿using System;
using System.Linq;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Simulation;

namespace Bronze.Contracts.Data.World
{
    public class LogiHauler
    {
        public IEconomicActor From { get; set; }
        public IEconomicActor To { get; set; }
        public Tile[] Path { get; set; }
        public ItemQuantity Cargo { get; }
        public int PositionOnPath { get; set; } 
        public double ProgressToNext { get; set; }
        public HaulerInfo HaulerInfo { get; set; }
        public double AnimationTime { get; set; }
        public MovementType ActiveMovementType { get; set; }

        public LogiHauler(
            IEconomicActor from,
            IEconomicActor to,
            Tile[] path,
            ItemQuantity cargo,
            HaulerInfo haulerInfo)
        {
            From = from;
            To = to;
            Path = path;
            Cargo = cargo;
            PositionOnPath = 0;
            ProgressToNext = 0.5;
            HaulerInfo = haulerInfo;

            DetermineActiveMovementType();
        }

        public void DetermineActiveMovementType()
        {
            ActiveMovementType = null;

            if(PositionOnPath < Path.Length)
            {
                var fromTile = Path[PositionOnPath];
                var toTile = fromTile;

                if(PositionOnPath + 1 < Path.Length)
                {
                    toTile = Path[PositionOnPath + 1];
                }

                ActiveMovementType = HaulerInfo.MovementTypes
                    .Where(mt => (toTile.Traits.ContainsAll(mt.RequiredTraits)
                            && !toTile.Traits.ContainsAny(mt.PreventedBy))
                        || (fromTile.Traits.ContainsAll(mt.RequiredTraits)
                            && !fromTile.Traits.ContainsAny(mt.PreventedBy)))
                    .FirstOrDefault();
            }
        }
    }
}
