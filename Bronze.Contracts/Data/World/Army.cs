﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace Bronze.Contracts.Data.World
{
    public class Army : IWorldActor
    {
        public string Id { get; private set; }
        public string Name { get; set; }
        public Tribe Owner { get; set; }
        public List<IUnit> Units { get; }
        public Facing Facing { get; set; }
        public NotablePerson General { get; set; }
        public ArmyStance Stance { get; set; }
        public bool IsLocked { get; set; }
        public bool IsRouting { get; set; }
        public Vector2 Position { get; set; }
        public List<Army> SkirmishingWith { get; }

        public bool IsFull => Units.Count >= Constants.UNITS_PER_ARMY;

        public ArmyPathCalculator CellPathCalculator { get; private set; }

        public int VisionRange => Units.Any() ? Units.Max(u => u.VisionRange) : 0;

        public IEnumerable<IArmyAction> AvailableActions => TransportType?.AvailableActions
            ?? Units
                .SelectMany(u => u.AvailableActions)
                .GroupBy(a => a.GetType().Name)
                .Select(g => g.First())
                .ToArray();

        public double StrengthEstimate => Units.Sum(u => u.StrengthEstimate);

        private Cell _cell;
        private IArmyOrder _currentOrder;
        private ITransportType _transportType;

        public Army(Tribe owner, Cell cell, IWorldManager worldManager)
        {
            Id = Guid.NewGuid().ToString();
            Name = "Name this army";
            Units = new List<IUnit>(Constants.UNITS_PER_ARMY);
            Cell = cell;
            Position = cell.Position.ToVector();
            Owner = owner;
            cell.WorldActors.Add(this);

            Stance = ArmyStance.Agressive;
            CellPathCalculator = new ArmyPathCalculator(this, worldManager);
            CurrentOrder = new NullOrder();
            SkirmishingWith = new List<Army>();
        }

        public Cell Cell
        {
            get => _cell;
            set
            {
                if (value != _cell)
                {
                    if (_cell != null)
                    {
                        _cell.WorldActors.RemoveAll(a => a == this);

                        if (_cell.Region != value.Region)
                        {
                            _cell.Region.WorldActors.RemoveAll(a => a == this);
                        }
                    }

                    if(_cell?.Region != value.Region && !value.Region.WorldActors.Contains(this))
                    {
                        value.Region.WorldActors.Add(this);
                    }

                    _cell = value;
                    
                    if (!_cell.WorldActors.Contains(this))
                    {
                        _cell.WorldActors.Add(this);
                    }
                }
            }
        }

        public void MoveTo(Cell cell)
        {
            Cell = cell;
            Position = cell.Position.ToVector();
            CellPathCalculator.ClearPathing();
            CurrentOrder = null;
        }

        public void MoveToward(double deltaMonths, Vector2 destination)
        {
            var moveProgress = deltaMonths * GetMovementSpeed(Cell);

            if(SkirmishingWith.Any() && !IsRouting)
            {
                moveProgress *= Constants.SKIRMISH_MOVEMENT_MOD;
            }
            if(IsRouting)
            {
                moveProgress *= Constants.ROUTING_MOVEMENT_MOD;
            }

            var delta = destination - Position;
            var deltaLen = delta.Length();

            if (deltaLen > 0)
            {
                delta = delta * (1 / deltaLen) * (float)moveProgress;
            }

            Position += delta;

            Facing = Util.GetFacing(Position, destination);

            if (!Cell.ContainsWorldPoint(Position))
            {
                Cell = Cell.Neighbors
                    .Where(c => c.ContainsWorldPoint(Position))
                    .FirstOrDefault() ?? Cell;
            }
        }

        public IEnumerable<ItemRate> UpkeepInputs
        {
            get
            {
                return Units
                    .SelectMany(u => u.UpkeepNeeds)
                    .GroupBy(i => i.Item)
                    .Select(g => new ItemRate(g.Key, g.Sum(i => i.PerMonth)))
                    .OrderBy(i => i.Item.Name)
                    .ToArray();
            }
        }

        public IArmyOrder CurrentOrder
        {
            get => _currentOrder;
            set
            {
                var newOrder = value ?? new NullOrder();

                if (_currentOrder != null)
                {
                    _currentOrder.OnOrderChanged(this, newOrder);
                }

                _currentOrder = newOrder;
            }
        }

        public ITransportType TransportType
        {
            get => _transportType;
            set
            {
                if(_transportType != null)
                {
                    _transportType.OnDisembark(this);
                }
                _transportType = value;
                if(_transportType != null)
                {
                    _transportType.OnEmbark(this);
                }

                foreach (var unit in Units)
                {
                    unit.CurrentAnimation = null;
                }
            }
        }
        
        public bool CanPath(Cell cell)
        {
            if (Owner.IsExplored(cell))
            {
                return (cell.Development == null || cell.Development.AllowsAccess(this))
                    && (cell.Owner == null || cell.Owner.AllowsAccess(Owner))
                    && ((TransportType != null && TransportType.CanPath(cell))
                        || (TransportType == null && Units.All(u => u.CanPath(cell))));
            }

            return true;
        }

        public bool CouldPathWithoutTransport(Cell cell)
        {
            if (Owner.IsExplored(cell))
            {
                return (cell.Owner == null || cell.Owner.AllowsAccess(Owner))
                    && Units.All(u => u.CanPath(cell));
            }

            return true;
        }

        public double GetMovementSpeed(Cell cell)
        {
            var speed = 0.0;

            if(Owner.IsExplored(cell))
            {
                if (TransportType != null)
                {
                    return TransportType.GetMovementSpeed(cell);
                }
                else if (Units.Any())
                {
                    speed = Units.Min(u => u.GetMovementSpeed(cell));
                }
            }
            
            if(speed == 0)
            {
                speed = 1;
            }

            speed += General?.GetBonusArmySpeed() ?? 0.0;

            return speed;
        }
        
        public void CheckAnimations()
        {
            foreach (var unit in Units)
            {
                CurrentOrder.SetAnimationFor(this, unit);
            }
        }

        public void SerializeTo(SerializedObject armyRoot)
        {
            armyRoot.Set("id", Id);
            armyRoot.Set("name", Name);
            armyRoot.Set("pos_x", Position.X);
            armyRoot.Set("pos_y", Position.Y);
            armyRoot.Set("facing", (int)Facing);
            armyRoot.Set("transport_type_id", TransportType?.Id);
            armyRoot.Set("stance", Stance.ToString());
            
            CellPathCalculator.SerializeTo(armyRoot.CreateChild("cell_path_calculator"));

            _cell.Position.SerializeTo(armyRoot.CreateChild("cell_position"));
            
            foreach(var unit in Units)
            {
                unit.SerializeTo(armyRoot.CreateChild("unit"));
            }

            CurrentOrder.SerializeTo(armyRoot.CreateChild("order"));
        }

        public static Army DeserializeFrom(
            IWorldManager worldManager,
            IGamedataTracker gamedataTracker,
            IEnumerable<Region> regions, 
            Tribe tribe, 
            SerializedObject armyRoot)
        {
            var position = CellPosition.DeserializeFrom(armyRoot.GetChild("cell_position"));

            var cell = regions
                .SelectMany(r => r.Cells)
                .Where(c => c.Position == position)
                .FirstOrDefault();
            
            var army = new Army(tribe, cell, worldManager);

            army.Id = armyRoot.GetString("id");
            army.Name = armyRoot.GetString("name");
            army.Position = new Vector2(
                (float)armyRoot.GetDouble("pos_x"),
                (float)armyRoot.GetDouble("pos_y"));
            army.Facing = (Facing)armyRoot.GetInt("facing");
            army.TransportType = armyRoot.GetOptionalObjectReference(
                "transport_type_id",
                gamedataTracker.TransportTypes,
                tt => tt.Id);
            army.Stance = armyRoot.GetEnum<ArmyStance>("stance");
            
            foreach (var unitRoot in armyRoot.GetChildren("unit"))
            {
                var unitType = unitRoot.GetObjectReference(
                        "type_id",
                        gamedataTracker.UnitTypes,
                        x => x.Id);

                var unit = unitType
                    .DeserializeFrom(
                        gamedataTracker,
                        unitRoot);

                army.Units.Add(unit);

                // Force the proper animation if on a transport
                var animationName = unitRoot.GetOptionalString("animation_name");
                if (animationName != null)
                {
                    unit.CurrentAnimation = null;
                    unit.SetAnimation(army, animationName.Yield());
                }
            }
            
            army.CellPathCalculator = ArmyPathCalculator.DeserializeFrom(
                worldManager,
                gamedataTracker,
                regions, 
                army,
                armyRoot.GetChild("cell_path_calculator"));

            // Set the default order, and override if needed
            army.CurrentOrder = new NullOrder();

            var orderRoot = armyRoot.GetOptionalChild("order");
            if (orderRoot != null)
            {
                var actionTypeName = orderRoot.GetOptionalString("action_type_name");

                var action = army.AvailableActions
                    .Where(a => a.GetType().Name == actionTypeName)
                    .FirstOrDefault();

                if(action != null)
                {
                    army.CurrentOrder = action.DeserializeOrderFrom(gamedataTracker, regions, tribe, army, orderRoot);
                }
            }

            return army;
        }

        public string GetStatusDescription()
        {
            return CurrentOrder.GetDescription();
        }

        public void ChangeOwner(Tribe newOwner)
        {
            Owner.Armies.Remove(this);
            Owner = newOwner;
            Owner.Armies.Add(this);

            if (General != null)
            {
                General.ClearAssignments();
            }
        }

        public void Draw(IDrawingService drawingService, ViewMode viewMode, bool isSelected)
        {
            CheckAnimations();

            var drawCenter = Position
                * Constants.CELL_SIZE_PIXELS
                * Util.ViewModeToScale(viewMode);

            var flagScale = 1f;
            if (isSelected && Units.Any())
            {
                // The flag subtly pulses on the selected army
                flagScale += 0.2f + (float)Math.Cos(DateTime.Now.Ticks / 2000000.0) * 0.1f;
            }

            if (viewMode == ViewMode.Detailed
                && (TransportType == null || !TransportType.ShowSingleTransport))
            {
                const int perUnitStep = 16;

                var factor = (float)Math.Sqrt(Units.Count) / 2 * perUnitStep;

                var maxX = drawCenter.X + factor;
                var minX = drawCenter.X - factor;

                var drawPos = new Vector2(minX, drawCenter.Y - factor);

                var targetPositions = new List<Vector2>();
                var x = 0f;
                var y = 0f;
                var formationFrontage = Constants.CELL_SIZE_PIXELS / 2;
                var maxDepth = 16f;
                var rotation = Matrix3x2.CreateRotation((float)Util.FacingToRadian(Facing));
                foreach (var unit in Units)
                {
                    targetPositions.Add(new Vector2(x, y));

                    y += (unit.CurrentAnimation?.Width ?? 32) * 0.5f;
                    maxDepth = Math.Max(maxDepth, unit.CurrentAnimation?.Width ?? 32);
                    if (y >= formationFrontage)
                    {
                        y = 0;
                        x -= maxDepth * 0.5f;
                        maxDepth = 16;
                    }
                }

                var average = Vector2.Zero;
                if (targetPositions.Any())
                {
                    average = new Vector2(
                        targetPositions.Average(v => v.X),
                        targetPositions.Average(v => v.Y));
                }

                for (var i = 0; i < Units.Count; i++)
                {
                    var unit = Units[i];
                    if (unit.CurrentAnimation != null)
                    {
                        unit.LastDrawPosition = drawCenter + Vector2.Transform(targetPositions[i] - average, rotation);

                        unit.CurrentAnimation.Draw(
                            drawingService,
                            unit.LastDrawPosition.Value,
                            unit.AnimationTime,
                            Owner.PrimaryColor,
                            Facing,
                            0);
                    }
                }

                drawingService.DrawScaledImage(
                    Owner.Race.ArmyFlag,
                    Owner.PrimaryColor,
                    drawCenter + new Vector2(0, -32),
                    flagScale,
                    flagScale,
                    10,
                    true);
                drawingService.DrawScaledImage(
                    Owner.Race.ArmyFlagIcon,
                    Owner.SecondaryColor,
                    drawCenter + new Vector2(0, -32),
                    flagScale,
                    flagScale,
                    11,
                    true);

                if (Units.Any(u => u.SufferingAttrition))
                {
                    drawingService.DrawScaledImage(
                        "army_flag_attrition",
                        BronzeColor.None,
                        drawCenter + new Vector2(0, -32),
                        flagScale,
                        flagScale,
                        11,
                        true);
                }
            }
            else
            {
                for (var i = 0; i < Units.Count; i++)
                {
                    Units[i].LastDrawPosition = null;
                }

                var unit = Units
                    .Where(u => u.CurrentAnimation != null)
                    .FirstOrDefault();

                if (unit != null)
                {
                    unit.LastDrawPosition = drawCenter + new Vector2(-4, 0);

                    unit.CurrentAnimation.Draw(
                        drawingService,
                        unit.LastDrawPosition.Value,
                        unit.AnimationTime,
                        Owner.PrimaryColor,
                        Facing,
                        0);
                }

                drawingService.DrawScaledImage(
                    Owner.Race.ArmyFlag,
                    Owner.PrimaryColor,
                    drawCenter + new Vector2(8, -10),
                    flagScale,
                    flagScale,
                    10,
                    true);
                drawingService.DrawScaledImage(
                    Owner.Race.ArmyFlagIcon,
                    Owner.SecondaryColor,
                    drawCenter + new Vector2(8, -10),
                    flagScale,
                    flagScale,
                    11,
                    true);

                if (Units.Any(u => u.SufferingAttrition))
                {
                    drawingService.DrawScaledImage(
                        "army_flag_attrition",
                        BronzeColor.None,
                        drawCenter + new Vector2(8, -10),
                        flagScale,
                        flagScale,
                        11,
                        true);
                }
            }
        }

        public IEnumerable<SoundEffectType> GetSoundEffects()
        {
            return Units.SelectMany(u => u.CurrentAnimation.SoundEffects.Where(e => e.Mode == SoundEffectMode.Ambient));
        }
    }
}
