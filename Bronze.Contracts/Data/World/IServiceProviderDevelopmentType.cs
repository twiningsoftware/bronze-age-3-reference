﻿using Bronze.Contracts.Data.Gamedata;
using System.Collections.Generic;

namespace Bronze.Contracts.Data.World
{
    public interface IServiceProviderDevelopmentType : ICellDevelopmentType, IServiceProviderActorType
    {
        IEnumerable<Cell> CalculateServiceArea(Cell cell);
    }
}
