﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Simulation;
using System.Collections.Generic;

namespace Bronze.Contracts.Data.World
{
    public interface IRecruitableUnitTypeSource : IEconomicActor
    {
        IEnumerable<IUnitType> ProvidedUnitTypes { get; }
    }
}
