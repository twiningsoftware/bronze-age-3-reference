﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Simulation;
using System.Collections.Generic;

namespace Bronze.Contracts.Data.World
{
    public interface IServiceProviderActor : IEconomicActor
    {
        ServiceType ServiceType { get; }
        List<IEconomicActor> ActorsServed { get; }
        double ServiceRange { get; }
        double ServicePercent { get; }
        IEnumerable<Trait> ServiceTransmissionTraits { get; }

        bool CanService(IEconomicActor economicActor);
        bool CanService(IEconomicActorType economicActorType);
        void ClearServicedActors();
        void AddServicedActor(IEconomicActor actorToService);
    }
}
