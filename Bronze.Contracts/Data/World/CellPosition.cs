﻿using Bronze.Contracts.Data.Serialization;
using System.Numerics;

namespace Bronze.Contracts.Data.World
{
    public struct CellPosition
    {
        public int Plane { get; }
        public int X { get; }
        public int Y { get; }

        private Vector2 _vector;

        public CellPosition(int plane, int x, int y)
        {
            Plane = plane;
            X = x;
            Y = y;
            _vector = new Vector2(X, Y);
        }

        public override bool Equals(object obj)
        {
            if(obj is CellPosition pos)
            {
                return Plane == pos.Plane && X == pos.X && Y == pos.Y;
            }

            return false;
        }

        public override int GetHashCode()
        {
            var hashCode = -1066053480;
            hashCode = hashCode * -1521134295 + Plane.GetHashCode();
            hashCode = hashCode * -1521134295 + X.GetHashCode();
            hashCode = hashCode * -1521134295 + Y.GetHashCode();
            return hashCode;
        }

        public static bool operator ==(CellPosition lhs, CellPosition rhs)
        {
            return lhs.Plane == rhs.Plane && lhs.X == rhs.X && lhs.Y == rhs.Y;
        }
        public static bool operator !=(CellPosition lhs, CellPosition rhs)
        {
            return lhs.Plane != rhs.Plane || lhs.X != rhs.X || lhs.Y == rhs.Y;
        }

        public double DistanceSq(CellPosition position)
        {
            var dx = X - position.X;
            var dy = Y - position.Y;

            return dx * dx + dy * dy;
        }

        public Vector2 ToVector()
        {
            return _vector;
        }

        public override string ToString()
        {
            return $"{Plane}: {X}, {Y}";
        }

        public void SerializeTo(SerializedObject positionRoot)
        {
            positionRoot.Set("plane", Plane);
            positionRoot.Set("x", X);
            positionRoot.Set("y", Y);
        }

        public static CellPosition DeserializeFrom(
            SerializedObject positionRoot)
        {
            return new CellPosition(
                positionRoot.GetInt("plane"),
                positionRoot.GetInt("x"),
                positionRoot.GetInt("y"));
        }
    }
}
