﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.UI;
using System.Collections.Generic;

namespace Bronze.Contracts.Data.World
{
    public interface IStructure : IEconomicActor
    {
        Tile UlTile { get; }

        TilePosition Center { get; }

        string[] AdjacencyGroups { get; }

        int TilesWide { get; }

        int TilesHigh { get; }

        IEnumerable<Trait> Traits { get; }

        IEnumerable<Trait> NotTraits { get; }

        IEnumerable<ITileDisplay> AnimationLayers { get; }

        IEnumerable<Tile> Footprint { get; }

        MinimapColor MinimapColor { get; }

        double LogiDistance { get; }

        IEnumerable<IStructureType> ManuallyUpgradesTo { get; }
        bool CanManuallyUpgrade { get; }
        bool CanManuallyDowngrade { get; }
        IStructureType ManuallyDowngradesTo { get; }
        IStructureType UpgradingTo { get; }

        IStackUiElement BuildDetailColumn();

        double Prosperity { get; }

        void OnRemoval();

        void UpgradeTo(IStructureType structureType);
        void CancelUpgrading();

        void SerializeTo(SerializedObject serializedObject);
    }
}
