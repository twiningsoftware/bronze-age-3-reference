﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.InjectableServices;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Contracts.Data.World
{
    public class Plane
    {
        public int Id { get; }

        public string Name { get; }

        public IEnumerable<Region> Regions { get; }

        public IEnumerable<MovementType> NativePlaneChanging { get; }

        public Plane(int id, string name, IEnumerable<Region> regions, IEnumerable<MovementType> nativePlaneChanging)
        {
            Id = id;
            Name = name;
            Regions = regions.ToArray();
            NativePlaneChanging = nativePlaneChanging.ToArray();
        }

        public void SerializeTo(SerializedObject planeRoot)
        {
            planeRoot.Set("id", Id);
            planeRoot.Set("name", Name);

            foreach (var region in Regions)
            {
                var regionRoot = planeRoot.CreateChild("region");

                region.SerializeTo(regionRoot);
            }

            foreach(var movementType in NativePlaneChanging)
            {
                planeRoot.CreateChild("native_plane_changing").Set("movement_type_id", movementType.Id);
            }
        }

        public static Plane DeserializeFrom(
            IGamedataTracker gamedataTracker, 
            SerializedObject planeRoot)
        {
            var id = planeRoot.GetInt("id");
            var name = planeRoot.GetString("name");
            var regions = new List<Region>();

            foreach(var regionRoot in planeRoot.GetChildren("region"))
            {
                regions.Add(Region.DeserializeFrom(gamedataTracker, regionRoot));
            }

            var nativePlaneChanging = planeRoot.GetChildren("native_plane_changing")
                .Select(o => o.GetObjectReference("movement_type_id", gamedataTracker.MovementTypes, m => m.Id))
                .ToArray();

            return new Plane(id, name, regions, nativePlaneChanging);
        }
    }
}
