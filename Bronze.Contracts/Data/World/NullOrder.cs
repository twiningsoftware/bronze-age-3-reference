﻿using Bronze.Contracts.Data.Serialization;

namespace Bronze.Contracts.Data.World
{
    public class NullOrder : IArmyOrder
    {
        public string ActionTypeName => "ArmyIdleAction";

        public bool IsIdle => true;

        public string GetDescription()
        {
            return "Waiting";
        }

        public void OnOrderChanged(Army army, IArmyOrder newOrder)
        {
        }

        public void SerializeTo(SerializedObject orderRoot)
        {
        }

        public void SetAnimationFor(Army army, IUnit unit)
        {
            unit.SetAnimation(army, Constants.ANIMATION_IDLE.Yield());
        }

        public void Simulate(Army army, double deltaMonths, AggregatePerformanceTracker aggregatePerformanceTracker)
        {
            army.CellPathCalculator.ClearPathing();
            
            foreach (var unit in army.Units)
            {
                unit.AnimationTime += deltaMonths / Constants.MONTHS_PER_SECOND;
            }
        }
    }
}
