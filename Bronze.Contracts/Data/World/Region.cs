﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;

namespace Bronze.Contracts.Data.World
{
    public class Region
    {
        public int Id { get; }

        public string Name { get; set; }
        public IEnumerable<Cell> Cells => _cells;
        public List<IWorldActor> WorldActors { get; private set; }
        public Biome Biome { get; set; }
        public List<Region> Neighbors { get; }
        public Rectangle BoundingBox { get; private set; }

        public Tribe Owner => Settlement?.Owner;
        public Settlement Settlement { get; set; }
        public CellPosition Center => new CellPosition(_cells[0].Position.Plane, BoundingBox.Center.X, BoundingBox.Center.Y);

        private List<Cell> _cells;

        public Region(int id)
        {
            Id = id;
            _cells = new List<Cell>();
            Neighbors = new List<Region>();
            BoundingBox = new Rectangle();
            WorldActors = new List<IWorldActor>();
        }
        
        public void AddCell(Cell newCell)
        {
            // If the region is empty, recreate the bounding box
            if(_cells.Count < 1)
            {
                BoundingBox = new Rectangle(
                    newCell.Position.X,
                    newCell.Position.Y,
                    0,
                    0);
            }
            // Otherwise, expand the box to fit the new cell, if needed
            else if(newCell.Position.X < BoundingBox.Left
                || BoundingBox.Right < newCell.Position.X
                || newCell.Position.Y < BoundingBox.Top
                || BoundingBox.Bottom < newCell.Position.Y)
            {
                var minX = Math.Min(newCell.Position.X, BoundingBox.Left);
                var maxX = Math.Max(newCell.Position.X, BoundingBox.Right);
                var minY = Math.Min(newCell.Position.Y, BoundingBox.Top);
                var maxY = Math.Max(newCell.Position.Y, BoundingBox.Bottom);

                BoundingBox = new Rectangle(
                    minX,
                    minY,
                    maxX - minX,
                    maxY - minY);
            }

            if(newCell.Region != null)
            {
                newCell.Region._cells.Remove(newCell);
            }

            _cells.Add(newCell);
            newCell.Region = this;
        }

        public void SerializeTo(SerializedObject regionRoot)
        {
            regionRoot.Set("id", Id);
            regionRoot.Set("name", Name);
            regionRoot.Set("biome_id", Biome?.Id);
            
            foreach (var cell in Cells)
            {
                var cellRoot = regionRoot.CreateChild("cell");

                cell.SerializeTo(cellRoot);
            }
        }

        public static Region DeserializeFrom(
            IGamedataTracker gamedataTracker, 
            SerializedObject regionRoot)
        {
            var region = new Region(regionRoot.GetInt("id"))
            {
                Name = regionRoot.GetString("name"),
                Biome = regionRoot.GetObjectReference(
                    "biome_id",
                    gamedataTracker.Biomes,
                    b => b.Id)
            };

            foreach(var cellRoot in regionRoot.GetChildren("cell"))
            {
                region.AddCell(Cell.DeserializeFrom(gamedataTracker, cellRoot));
            }

            return region;
        }
    }
}
