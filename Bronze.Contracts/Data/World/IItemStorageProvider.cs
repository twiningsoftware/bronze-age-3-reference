﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Simulation;

namespace Bronze.Contracts.Data.World
{
    public interface IItemStorageProvider : IEconomicActor
    {
        bool CanStore(Item item);
    }
}
