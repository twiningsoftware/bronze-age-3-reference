﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace Bronze.Contracts.Data.World
{
    /// <summary>
    /// The basic building block of the world.
    /// </summary>
    public class Cell
    {
        public CellPosition Position { get; }        
        public Region Region { get; set; }
        public List<IWorldActor> WorldActors { get; private set; }
        public IEnumerable<Trait> Traits { get; private set; }
        public IEnumerable<Trait> NotTraits { get; private set; }
        public IEnumerable<Trait> TraitsAfterConstruction { get; private set; }
        public IEnumerable<Trait> NotTraitsAfterConstruction { get; private set; }
        public IEnumerable<Cell> Neighbors => _neighbors.Where(c => c != null);
        public IEnumerable<Cell> OrthoNeighbors => new[] { GetNeighbor(Facing.North), GetNeighbor(Facing.West), GetNeighbor(Facing.East), GetNeighbor(Facing.South) }.Where(c => c != null);
        public IEnumerable<string> AdjacencyGroups => Biome.AdjacencyGroups.Concat(Feature?.AdjacencyGroups ?? Enumerable.Empty<string>()).Concat(Development?.AdjacencyGroups ?? Enumerable.Empty<string>());
        public int TileVariation { get; private set; }
        public IEnumerable<CellAnimationLayer> DetailLayers { get; private set; }
        public IEnumerable<CellAnimationLayer> SummaryLayers { get; private set; }

        public IEnumerable<DistrictGenerationLayer> DistrictGenerationLayers => Biome.DistrictGenerationLayers.Concat(Feature?.DistrictGenerationLayers ?? Enumerable.Empty<DistrictGenerationLayer>());

        public bool ContainsWorldPoint(Vector2 position)
        {
            return position.X < Position.X + 0.5
                && position.X >= Position.X - 0.5
                && position.Y < Position.Y + 0.5
                && position.Y >= Position.Y - 0.5;
        }

        public Tribe Owner => Region?.Settlement?.Owner;

        private Cell[] _neighbors;

        private Biome _biome;
        private ICellFeature _cellFeature;
        private ICellDevelopment _cellDevelopment;
        

        public Cell(CellPosition position)
        {
            Position = position;
            _neighbors = new Cell[8];
            CalculateTraits();
            
            DetailLayers = Enumerable.Empty<CellAnimationLayer>();
            SummaryLayers = Enumerable.Empty<CellAnimationLayer>();
            WorldActors = new List<IWorldActor>();
            TileVariation = Math.Abs(position.GetHashCode()) % Constants.TILE_VARIATION_COUNT;
        }

        public Biome Biome
        {
            get => _biome;
            set
            {
                _biome = value;
                CalculateTraits();
                CalculateLayers();
            }
        }
        public ICellFeature Feature
        {
            get => _cellFeature;
            set
            {
                _cellFeature = value;
                CalculateTraits();
                CalculateLayers();
            }
        }

        public ICellDevelopment Development
        {
            get => _cellDevelopment;
            set
            {
                if(_cellDevelopment != null)
                {
                    _cellDevelopment.OnRemoval();
                }

                _cellDevelopment = value;

                CalculateTraits();

                if (Region?.Settlement != null)
                {
                    Region.Settlement.SetCalculationFlag("development changed");
                }
                CalculateLayers();
            }
        }

        public bool IsOnBorder { get; set; }

        public Cell GetNeighbor(Facing facing)
        {
            return _neighbors[(int)facing];
        }

        public void SetNeighbor(Facing facing, Cell cell)
        {
            _neighbors[(int)facing] = cell;
        }

        public void CalculateTraits()
        {
            if(Biome == null)
            {
                return;
            }

            if (Development != null)
            {
                if (!Development.UnderConstruction)
                {
                    NotTraits = Biome.NotTraits
                        .Concat(Development.NotTraits)
                        .Concat(Feature?.NotTraits ?? Enumerable.Empty<Trait>())
                        .ToArray();

                    Traits = Biome.Traits
                        .Concat(Region?.Settlement?.Traits ?? Enumerable.Empty<Trait>())
                        .Concat(Development.Traits)
                        .Concat(Feature?.Traits ?? Enumerable.Empty<Trait>())
                        .Except(NotTraits)
                        .ToArray();

                    NotTraitsAfterConstruction = NotTraits;
                    TraitsAfterConstruction = Traits;
                }
                else
                {
                    NotTraitsAfterConstruction = Biome.NotTraits
                        .Concat(Development.NotTraits)
                        .Concat(Feature?.NotTraits ?? Enumerable.Empty<Trait>())
                        .ToArray();

                    TraitsAfterConstruction = Biome.Traits
                        .Concat(Region?.Settlement?.Traits ?? Enumerable.Empty<Trait>())
                        .Concat(Development.Traits)
                        .Concat(Feature?.Traits ?? Enumerable.Empty<Trait>())
                        .Except(NotTraitsAfterConstruction)
                        .ToArray();

                    NotTraits = Biome.NotTraits
                        .Concat(Feature?.NotTraits ?? Enumerable.Empty<Trait>())
                        .ToArray();

                    Traits = Biome.Traits
                        .Concat(Region?.Settlement?.Traits ?? Enumerable.Empty<Trait>())
                        .Concat(Feature?.Traits ?? Enumerable.Empty<Trait>())
                        .Except(NotTraits)
                        .ToArray();
                }
            }
            else
            {
                NotTraits = Biome.NotTraits
                    .Concat(Feature?.NotTraits ?? Enumerable.Empty<Trait>())
                    .ToArray();

                Traits = Biome.Traits
                    .Concat(Region?.Settlement?.Traits ?? Enumerable.Empty<Trait>())
                    .Concat(Feature?.Traits ?? Enumerable.Empty<Trait>())
                    .Except(NotTraits)
                    .ToArray();

                NotTraitsAfterConstruction = NotTraits;
                TraitsAfterConstruction = Traits;
            }
        }

        public void CalculateLayers()
        {
            var animations = Development?.GetAnimationsNames() ?? Enumerable.Empty<string>();

            SummaryLayers = Enumerable.Empty<CellAnimationLayer>()
                .Concat(Biome?.SummaryLayers ?? Enumerable.Empty<CellAnimationLayer>())
                .Concat(Feature?.SummaryLayers ?? Enumerable.Empty<CellAnimationLayer>())
                .Concat(Development?.SummaryLayers ?? Enumerable.Empty<CellAnimationLayer>())
                .Where(l => string.IsNullOrWhiteSpace(l.AnimationName) || animations.Contains(l.AnimationName))
                .ToArray();

            DetailLayers = Enumerable.Empty<CellAnimationLayer>()
                .Concat(Biome?.DetailLayers ?? Enumerable.Empty<CellAnimationLayer>())
                .Concat(Feature?.DetailLayers ?? Enumerable.Empty<CellAnimationLayer>())
                .Concat(Development?.DetailLayers ?? Enumerable.Empty<CellAnimationLayer>())
                .Where(l => string.IsNullOrWhiteSpace(l.AnimationName) || animations.Contains(l.AnimationName))
                .ToArray();
        }

        public void SerializeTo(SerializedObject cellRoot)
        {
            cellRoot.Set("biome_id", Biome?.Id);
            cellRoot.Set("feature_id", Feature?.Id);

            Position.SerializeTo(cellRoot.CreateChild("position"));
            
            if(Development != null)
            {
                Development.SerializeTo(cellRoot.CreateChild("development"));
            }
        }

        public static Cell DeserializeFrom(
            IGamedataTracker gamedataTracker,
            SerializedObject cellRoot)
        {
            var cell = new Cell(CellPosition.DeserializeFrom(cellRoot.GetChild("position")));

            cell.Biome = cellRoot.GetOptionalObjectReference(
                "biome_id",
                gamedataTracker.Biomes,
                b => b.Id);

            cell.Feature = cellRoot.GetOptionalObjectReference(
                "feature_id",
                gamedataTracker.CellFeatures,
                f => f.Id);
            
            var developmentRoot = cellRoot.GetOptionalChild("development");

            if(developmentRoot != null)
            {
                var developmentType = developmentRoot.GetObjectReference(
                    "type_id",
                    gamedataTracker.CellDevelopments,
                    x => x.Id);

                cell.Development = developmentType
                    .DeserializeFrom(
                        gamedataTracker, 
                        cell, 
                        developmentRoot);
            }

            return cell;
        }
    }
}
