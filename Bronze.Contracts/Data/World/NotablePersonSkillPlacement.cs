﻿using Bronze.Contracts.Data.Gamedata;
using System.Linq;

namespace Bronze.Contracts.Data.World
{
    public class NotablePersonSkillPlacement
    {
        public NotablePersonSkill Skill { get; set; }
        public NotablePersonSkillLevel Level { get; set; }

        public bool CanIncrement()
        {
            return Level.Level < Skill.Levels.Max(l => l.Level);
        }

        public void Increment()
        {
            Level = Skill.Levels
                .Where(l => l.Level == Level.Level + 1)
                .First();
        }

        public bool CanDecrement()
        {
            return Level.Level > 0;
        }

        public void Decrement()
        {
            Level = Skill.Levels
                .Where(l => l.Level == Level.Level - 1)
                .First();
        }
    }
}