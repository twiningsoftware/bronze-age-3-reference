﻿using Bronze.Contracts.Data.Gamedata;
using System.Collections.Generic;

namespace Bronze.Contracts.Data.World
{
    public interface IGrazingUnit : IUnit
    {
        ItemRate[] GrazingProduction { get; }
        double GrazingRecovery { get; }
        double GrazingExhaustion { get; }
        Trait[] GrazingRequirements { get; }
        Trait[] GrazingForbidden { get; }
        IEnumerable<BonusType> BonusedBy { get; }
        bool GrazeInRegion { get; }
        HaulerInfo[] LogiSourceHaulers { get; }

        IEnumerable<ItemRate> GetBonusedGrazingProduction(IntIndexableLookup<BonusType, double> currentBonuses);
    }
}
