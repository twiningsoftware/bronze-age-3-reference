﻿using System.Collections.Generic;

namespace Bronze.Contracts.Data.World
{
    public interface IServiceProviderStructure : IStructure, IServiceProviderActor
    {
        List<Tile> TilesServed { get; }

        IEnumerable<Tile> CalculateServiceArea();
    }
}
