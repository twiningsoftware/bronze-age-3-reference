﻿using Bronze.Contracts.Data.Serialization;
using System;
using System.Numerics;

namespace Bronze.Contracts.Data.World
{
    public struct TilePosition
    {
        public const int TILES_PER_CELL = 96;

        public Cell Cell { get; }

        public int X { get; }
        public int Y { get; }

        private Vector2 _vector;

        public TilePosition(Cell cell, int x, int y)
        {
            Cell = cell;
            X = x;
            Y = y;

            if (Cell != null)
            {
                _vector = new Vector2(X + Cell.Position.X * TILES_PER_CELL, Y + Cell.Position.Y * TILES_PER_CELL);
            }
            else
            {
                _vector = new Vector2(X, Y);
            }
        }

        public TilePosition Relative(int dx, int dy)
        {
            var newCell = Cell;
            var newX = X + dx;
            var newY = Y + dy;

            while(newX > TILES_PER_CELL && newCell != null)
            {
                newX -= TILES_PER_CELL;
                newCell = newCell.GetNeighbor(Facing.East);
            }
            while (newX < 0 && newCell != null)
            {
                newX += TILES_PER_CELL;
                newCell = newCell.GetNeighbor(Facing.West);
            }
            while (newY > TILES_PER_CELL && newCell != null)
            {
                newY -= TILES_PER_CELL;
                newCell = newCell.GetNeighbor(Facing.South);
            }
            while (newY < 0 && newCell != null)
            {
                newY += TILES_PER_CELL;
                newCell = newCell.GetNeighbor(Facing.North);
            }

            return new TilePosition(Cell, X + dx, Y + dy);
        }

        public override bool Equals(object obj)
        {
            if(obj is TilePosition pos)
            {
                return Cell.Position == pos.Cell.Position && X == pos.X && Y == pos.Y;
            }

            return false;
        }

        public override int GetHashCode()
        {
            var hashCode = -1066053480;
            hashCode = hashCode * -1521134295 + Cell.Position.GetHashCode();
            hashCode = hashCode * -1521134295 + X.GetHashCode();
            hashCode = hashCode * -1521134295 + Y.GetHashCode();
            return hashCode;
        }

        public void SerializeTo(SerializedObject positionRoot)
        {
            positionRoot.Set("x", X);
            positionRoot.Set("y", Y);
        }

        public static TilePosition DeserializeFrom(Cell cell, SerializedObject positionRoot)
        {
            return new TilePosition(
                cell,
                positionRoot.GetInt("x"),
                positionRoot.GetInt("y"));
        }

        public static bool operator ==(TilePosition lhs, TilePosition rhs)
        {
            return lhs.Cell.Position == rhs.Cell.Position && lhs.X == rhs.X && lhs.Y == rhs.Y;
        }
        public static bool operator !=(TilePosition lhs, TilePosition rhs)
        {
            return lhs.Cell.Position != rhs.Cell.Position || lhs.X != rhs.X || lhs.Y == rhs.Y;
        }

        public double Distance(TilePosition other)
        {
            return Math.Sqrt(DistanceSq(other));
        }

        public double DistanceSq(TilePosition other)
        {
            var delta = ToVector() - other.ToVector();

            return delta.X * delta.X + delta.Y * delta.Y;
        }

        public Vector2 ToVector()
        {
            return _vector;
        }

        public override string ToString()
        {
            return $"Cell({Cell.Position}): {X}, {Y}";
        }

        public Facing DirectionTo(TilePosition other)
        {
            var delta = other.ToVector() - ToVector();
            var angle = Math.Atan2(delta.Y, delta.X);

            if(angle < 0)
            {
                angle += Math.PI * 2;
            }

            if(angle < Math.PI / 8)
            {
                return Facing.East;
            }
            else if (angle < 3 * Math.PI / 8)
            {
                return Facing.SouthEast;
            }
            else if (angle < 5 * Math.PI / 8)
            {
                return Facing.South;
            }
            else if (angle < 7 * Math.PI / 8)
            {
                return Facing.SouthWest;
            }
            else if (angle < 9 * Math.PI / 8)
            {
                return Facing.West;
            }
            else if (angle < 11 * Math.PI / 8)
            {
                return Facing.NorthWest;
            }
            else if (angle < 13 * Math.PI / 8)
            {
                return Facing.North;
            }
            else if (angle < 15 * Math.PI / 8)
            {
                return Facing.NorthEast;
            }
            else
            {
                return Facing.East;
            }
        }
    }
}
