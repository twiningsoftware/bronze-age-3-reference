﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Contracts.Data.World
{
    public class Tile
    {
        public TilePosition Position { get; }
        
        public Decoration Decoration { get; set; }
        public IStructure Structure { get; set; }

        public District District { get; set; }
        
        public IEnumerable<Tile> Neighbors => _neighbors.Where(t => t != null);
        public IEnumerable<Tile> OrthoNeighbors => new[] { GetNeighbor(Facing.North), GetNeighbor(Facing.West), GetNeighbor(Facing.East), GetNeighbor(Facing.South) }.Where(t => t != null);
        
        public IEnumerable<Trait> Traits { get; private set; }
        public IEnumerable<Trait> NotTraits { get; private set; }

        public bool IsBorderTile { get; private set; }

        private Tile[] _neighbors;
        private Terrain _terrain;

        public Tile(TilePosition position)
        {
            Position = position;
            _neighbors = new Tile[8];
            CalculateTraits();
            IsBorderTile = false;
        }

        public double LogiDistance
        {
            get
            {
                if(Structure != null && !Structure.UnderConstruction)
                {
                    return Structure.LogiDistance;
                }

                return Terrain.LogiDistance;
            }
        }

        public Terrain Terrain
        {
            get => _terrain;
            set
            {
                _terrain = value;
                CalculateTraits();
            }
        }

        

        public Tile GetNeighbor(Facing facing)
        {
            return _neighbors[(int)facing];
        }

        public void SetNeighbor(Facing facing, Tile tile)
        {
            _neighbors[(int)facing] = tile;

            IsBorderTile = GetNeighbor(Facing.North) == null
                || GetNeighbor(Facing.South) == null
                || GetNeighbor(Facing.East) == null
                || GetNeighbor(Facing.West) == null;
        }

        public void CalculateTraits()
        {
            try
            {
                if (Terrain == null)
                {
                    return;
                }

                if (Structure != null && !Structure.UnderConstruction)
                {
                    NotTraits = Terrain.NotTraits
                        .Concat(Structure.NotTraits)
                        .ToArray();

                    Traits = Terrain.Traits
                        .Concat(Structure.Traits)
                        .Concat(District.Settlement.Traits)
                        .Except(NotTraits).ToArray();
                }
                else
                {
                    NotTraits = Terrain.NotTraits
                        .ToArray();

                    Traits = Terrain.Traits
                        .Concat(District.Settlement.Traits)
                        .Except(NotTraits)
                        .ToArray();
                }
            }
            catch (Exception)
            { }
        }

        public void SerializeTo(SerializedObject tileRoot)
        {
            Position.SerializeTo(tileRoot.CreateChild("position"));
            tileRoot.Set("terrain_id", Terrain?.Id);
            tileRoot.Set("decoration_id", Decoration?.Id);
        }

        public static Tile DeserializeFrom(
            IGamedataTracker gamedataTracker, 
            District district,
            Cell cell,
            SerializedObject tileRoot)
        {
            return new Tile(TilePosition.DeserializeFrom(cell, tileRoot.GetChild("position")))
            {
                District = district,
                Terrain = tileRoot.GetOptionalObjectReference(
                    "terrain_id",
                    gamedataTracker.Terrain,
                    t => t.Id),
                Decoration = tileRoot.GetOptionalObjectReference(
                    "decoration_id",
                    gamedataTracker.Decorations,
                    d => d.Id),
            };
        }
    }
}