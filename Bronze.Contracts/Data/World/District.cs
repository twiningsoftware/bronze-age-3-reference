﻿using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.InjectableServices;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Bronze.Contracts.Data.World
{
    public class District
    {
        public Cell Cell { get; set; }
        public bool IsGenerated { get; set; }
        public Tile[,] Tiles { get; set; }
        public IEnumerable<IStructure> Structures { get; private set; }
        public Settlement Settlement { get; set; }

        private List<IStructure> _structures;

        public District(Cell cell)
        {
            Cell = cell;
            Tiles = new Tile[TilePosition.TILES_PER_CELL, TilePosition.TILES_PER_CELL];
            _structures = new List<IStructure>();
            Structures = new IStructure[0];
        }

        public IEnumerable<Tile> AllTiles
        {
            get
            {
                for(var x = 0; x < TilePosition.TILES_PER_CELL; x++)
                {
                    for(var y = 0; y < TilePosition.TILES_PER_CELL; y++)
                    {
                        if (Tiles[x, y] != null)
                        {
                            yield return Tiles[x, y];
                        }
                    }
                }
            }
        }

        public void AddStructure(IStructure structure)
        {
            _structures.Add(structure);
            Structures = _structures.ToArray();
            Settlement.SetCalculationFlag("structure added");
        }

        public void RemoveStructure(IStructure structure)
        {
            _structures.Remove(structure);
            Structures = _structures.ToArray();
            structure.OnRemoval();
            foreach (var tile in structure.Footprint)
            {
                tile.Structure = null;
            }
            Settlement.SetCalculationFlag("structure removed");
        }

        public bool Contains(TilePosition tilePosition)
        {
            return tilePosition.X >= 0 && tilePosition.X < TilePosition.TILES_PER_CELL
               && tilePosition.Y >= 0 && tilePosition.Y < TilePosition.TILES_PER_CELL;
        }

        public void SerializeTo(SerializedObject districtRoot)
        {
            Cell.Position.SerializeTo(districtRoot.CreateChild("cell_position"));
            districtRoot.Set("is_generated", IsGenerated);
            if (IsGenerated)
            {
                foreach (var structure in _structures)
                {
                    structure.SerializeTo(districtRoot.CreateChild("structure"));
                }
                foreach (var tile in AllTiles)
                {
                    tile.SerializeTo(districtRoot.CreateChild("tile"));
                }
            }
        }

        public static District DeserializeFrom(
            IGamedataTracker gamedataTracker, 
            Settlement settlement, 
            SerializedObject districtRoot)
        {
            var cellPosition = CellPosition.DeserializeFrom(districtRoot.GetChild("cell_position"));
            var cell = settlement.Region.Cells
                .Where(c => c.Position == cellPosition)
                .FirstOrDefault();

            if(cell == null)
            {
                throw new FileLoadException($"District in settlement {settlement.Name} references cell that cannot be found.");
            }

            var district = new District(cell);
            district.IsGenerated = districtRoot.GetBool("is_generated");
            district.Settlement = settlement;
            settlement.AddDistrict(district);

            if(district.IsGenerated)
            {
                var tiles = new Tile[TilePosition.TILES_PER_CELL, TilePosition.TILES_PER_CELL];
                foreach(var tileRoot in districtRoot.GetChildren("tile"))
                {
                    var tile = Tile.DeserializeFrom(gamedataTracker, district, cell, tileRoot);
                    tiles[tile.Position.X, tile.Position.Y] = tile;
                }
                district.Tiles = tiles;

                GenUtils.LinkTilesToNeighbors(settlement);

                foreach (var structureRoot in districtRoot.GetChildren("structure"))
                {
                    var structureType = structureRoot.GetObjectReference(
                        "type_id",
                        gamedataTracker.StructureTypes,
                        x => x.Id);

                    district.AddStructure(structureType
                        .DeserializeFrom(
                            gamedataTracker,
                            settlement,
                            cell,
                            tiles,
                            structureRoot));
                }
            }

            return district;
        }
    }
}
