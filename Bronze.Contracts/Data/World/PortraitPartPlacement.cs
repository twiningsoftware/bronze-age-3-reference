﻿using Bronze.Contracts.Data.Gamedata;

namespace Bronze.Contracts.Data.World
{
    public class PortraitPartPlacement
    {
        public string GroupId { get; set; }
        public PeoplePart Part { get; set; }
    }
}