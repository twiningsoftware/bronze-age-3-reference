﻿using Bronze.Contracts.InjectableServices;
using System;
using System.Numerics;

namespace Bronze.Contracts.Data.World
{
    public class WorldEffect
    {
        public Facing Facing { get; set; }
        public Vector2 WorldPosition { get; set; }
        public BronzeColor Colorization { get; set; }
        public int PlaneId { get; set; }
        public string ImageKey { get; set; }
        public bool IsFacingByRow { get; set; }
        public int FrameCount { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public float AnimationTime { get; set; }
        public float Layer { get; set; }
        public float TimeToLive { get; set; }

        public void Draw(
            IDrawingService drawingService)
        {
            var frame = Math.Max(0, (int)(AnimationTime / TimeToLive * FrameCount));
            
            if (frame < FrameCount)
            {
                var startY = 0;
                if (IsFacingByRow)
                {
                    switch (Facing)
                    {
                        case Facing.East:
                        case Facing.NorthEast:
                            startY = 0 * Height;
                            break;
                        case Facing.North:
                        case Facing.NorthWest:
                            startY = 1 * Height;
                            break;
                        case Facing.West:
                        case Facing.SouthWest:
                            startY = 2 * Height;
                            break;
                        case Facing.South:
                        case Facing.SouthEast:
                            startY = 3 * Height;
                            break;
                    }
                }

                var startX = frame * Width;

                drawingService.DrawAnimationFrame(
                    ImageKey,
                    Colorization,
                    new Rectangle(startX, startY, Width, Height),
                    WorldPosition,
                    1,
                    false,
                    Layer,
                    true);
            }
        }
    }
}
