﻿namespace Bronze.Contracts.Data.World
{
    public enum NotablePersonAgeBracket
    {
        NotSet,
        Baby,
        Child,
        Young,
        MiddleAged,
        Old
    }
}