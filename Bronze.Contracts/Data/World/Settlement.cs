﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Bronze.Contracts.Data.World
{
    public class Settlement
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public Region Region { get; set; }
        public Tribe Owner { get; private set; }
        public Race Race { get; }
        public IEnumerable<District> Districts => _districts;
        public List<Pop> Population { get; }
        public bool NeedsCalculation { get; private set; }
        public IEnumerable<string> CalculationReasons => _calculationReasons;
        public double LastSimMonth { get; set; }
        public double NextSimMonth { get; set; }
        public double NextSummaryMonth { get; set; }
        public bool IsRemoved { get; set; }
        public double PopCultChangeTimer { get; set; }
        private NotablePerson _governor;

        public bool[] RoadConnections { get; }

        public Dictionary<RecruitmentSlotCategory, double> RecruitmentSlots { get; }
        public Dictionary<RecruitmentSlotCategory, int> RecruitmentSlotsMax { get; }
        public IReadOnlyList<Buff> Buffs { get; }
        
        public IEconomicActor[] EconomicActors { get; set; }
        public IEconomicActor[] ActiveEconomicActors { get; set; }
        public IEnumerable<IStructure> Structures => Districts.SelectMany(d => d.Structures);

        private List<District> _districts;
        public Dictionary<Caste, int> HousingByCaste { get; }
        public Dictionary<Caste, double> GrowthByCaste { get; }
        public Dictionary<Caste, int> PopulationByCaste { get; }
        public Dictionary<Caste, double> GrowthCounterByCaste { get; }
        public Dictionary<Caste, int> JobsByCaste { get; }
        public Dictionary<Caste, double> SatisfactionByCaste { get; }
        public IntIndexableLookup<Cult, List<Pop>> CultMembership { get; }
        public IntIndexableLookup<Cult, double> InfluenceByCult { get; }
        public IntIndexableLookup<Cult, List<Tuple<string, double>>> InfluenceSourcesByCult { get; }
        public IntIndexableLookup<Item, double> NetItemProduction { get; }
        public IntIndexableLookup<Item, double> LastMonthProduction { get; }
        public IntIndexableLookup<Item, bool> ItemAvailability { get; }

        public List<Trait> Traits { get; }

        public double TaxRate { get; set; }
        public double AverageSatisfaction { get; set; }

        private List<string> _calculationReasons;
        public List<Army> SiegedBy { get; }
        public bool IsUnderSiege => SiegedBy.Any();

        public int GovernorAuthority { get; set; }
        public int StructureAuthority { get; set; }
        public int TotalAuthority { get; set; }
        public double AuthorityFraction { get; set; }
        public double RiteCooldown { get; set; }
        public ReadOnlyCollection<LogiHauler> LogiHaulers { get; }
        private readonly List<LogiHauler> _logiHaulers;
        private List<Buff> _buffs;
        
        public Settlement(Race race, Tribe owner)
        {
            Id = Guid.NewGuid().ToString();
            Race = race;
            Owner = owner;
            _districts = new List<District>();
            Population = new List<Pop>();
            EconomicActors = new IEconomicActor[0];
            ActiveEconomicActors = new IEconomicActor[0];
            HousingByCaste = race.Castes
                .ToDictionary(c => c, c => 0);
            GrowthByCaste = race.Castes
                .ToDictionary(c => c, c => 0.0);
            PopulationByCaste = race.Castes
                .ToDictionary(c => c, c => 0);
            GrowthCounterByCaste = race.Castes
                .ToDictionary(c => c, c => 0.0);
            JobsByCaste = race.Castes
                .ToDictionary(c => c, c => 0);
            SatisfactionByCaste = race.Castes
                .ToDictionary(c => c, c => 0.0);
            NetItemProduction = new IntIndexableLookup<Item, double>();
            TaxRate = 0.10;
            ItemAvailability = new IntIndexableLookup<Item, bool>();
            LastMonthProduction = new IntIndexableLookup<Item, double>();
            Traits = new List<Trait>();
            _calculationReasons = new List<string>();
            RecruitmentSlots = new Dictionary<RecruitmentSlotCategory, double>();
            RecruitmentSlotsMax = new Dictionary<RecruitmentSlotCategory, int>();
            SiegedBy = new List<Army>();
            _logiHaulers = new List<LogiHauler>();
            LogiHaulers = new ReadOnlyCollection<LogiHauler>(_logiHaulers);
            RoadConnections = new bool[8];
            _buffs = new List<Buff>();
            Buffs = _buffs.AsReadOnly();
            CultMembership = new IntIndexableLookup<Cult, List<Pop>>();
            InfluenceByCult = new IntIndexableLookup<Cult, double>();
            InfluenceSourcesByCult = new IntIndexableLookup<Cult, List<Tuple<string, double>>>();
            
            SetCalculationFlag("initialization");
        }


        public NotablePerson Governor
        {
            get => _governor;
            set
            {
                _governor = value;
                SetCalculationFlag("governor changed");
            }
        }
        
        public Tile GetTile(TilePosition tilePosition)
        {
            return Districts
                .Where(d => d.Cell == tilePosition.Cell)
                .Where(d => d.Contains(tilePosition))
                .Select(d => d.Tiles[tilePosition.X, tilePosition.Y])
                .FirstOrDefault();
        }

        public void AddDistrict(District district)
        {
            _districts.Add(district);
            SetCalculationFlag("new district");
        }

        public void SerializeTo(SerializedObject settlementRoot)
        {
            settlementRoot.Set("id", Id);
            settlementRoot.Set("name", Name);
            settlementRoot.Set("owner_id", Owner.Id);
            settlementRoot.Set("race_id", Race.Id);
            settlementRoot.Set("region_id", Region.Id);
            settlementRoot.Set("tax_rate", TaxRate);
            settlementRoot.Set("pop_cult_change_timer", PopCultChangeTimer);
            settlementRoot.Set("rite_cooldown", RiteCooldown);
            
            foreach (var pop in Population)
            {
                var popRoot = settlementRoot.CreateChild("pop");
                popRoot.Set("race_id", pop.Race.Id);
                popRoot.Set("caste_id", pop.Caste.Id);
                popRoot.Set("cult_id", pop.CultMembership.Id);
            }

            foreach (var district in Districts)
            {
                district.SerializeTo(settlementRoot.CreateChild("district"));
            }

            foreach (var kvp in GrowthCounterByCaste)
            {
                var counterRoot = settlementRoot.CreateChild("growth_counter");
                counterRoot.Set("caste_id", kvp.Key.Id);
                counterRoot.Set("value", kvp.Value);
            }

            foreach (var key in ItemAvailability.Keys)
            {
                if (ItemAvailability[key])
                {
                    var availablilityRoot = settlementRoot.CreateChild("item_availability");
                    availablilityRoot.Set("item", key.Name);
                }
            }

            foreach (var key in LastMonthProduction.Keys)
            {
                var productionRoot = settlementRoot.CreateChild("item_production");
                productionRoot.Set("item", key.Name);
                productionRoot.Set("rate", LastMonthProduction[key]);
            }

            foreach (var trait in Traits)
            {
                var traitRoot = settlementRoot.CreateChild("trait");
                traitRoot.Set("id", trait.Id);
            }

            foreach(var recruitmentSlotCategory in RecruitmentSlots.Keys)
            {
                var recruitmentSlotRoot = settlementRoot.CreateChild("recruitment_slots");
                recruitmentSlotRoot.Set("id", recruitmentSlotCategory.Id);
                recruitmentSlotRoot.Set("current", RecruitmentSlots[recruitmentSlotCategory]);
            }

            foreach(var hauler in LogiHaulers)
            {
                var haulerRoot = settlementRoot.CreateChild("logi_hauler");
                haulerRoot.Set("hauler_info", hauler.HaulerInfo.Id);
                haulerRoot.Set("from", hauler.From.Id);
                haulerRoot.Set("to", hauler.To.Id);
                haulerRoot.Set("cargo_item", hauler.Cargo.Item.Name);
                haulerRoot.Set("cargo_qty", hauler.Cargo.Quantity);
                haulerRoot.Set("path_pos", hauler.PositionOnPath);
                haulerRoot.Set("to_next", hauler.ProgressToNext);

                foreach(var tile in hauler.Path)
                {
                    var pathRoot = haulerRoot.CreateChild("path");
                    pathRoot.Set("cell_x", tile.Position.Cell.Position.X);
                    pathRoot.Set("cell_y", tile.Position.Cell.Position.Y);
                    pathRoot.Set("cell_plain", tile.Position.Cell.Position.Plane);
                    pathRoot.Set("tile_x", tile.Position.X);
                    pathRoot.Set("tile_y", tile.Position.Y);
                }
            }

            foreach(var buff in _buffs)
            {
                buff.SerializeTo(settlementRoot.CreateChild("buff"));
            }
        }

        public static Settlement DeserializeFrom(
            IGamedataTracker gamedataTracker,
            IEnumerable<Region> regions,
            Tribe owner,
            SerializedObject settlementRoot)
        {
            var race = settlementRoot.GetObjectReference(
                "race_id",
                gamedataTracker.Races,
                r => r.Id);

            var settlement = new Settlement(race, owner)
            {
                Id = settlementRoot.GetString("id"),
                Name = settlementRoot.GetString("name"),
                Region = settlementRoot.GetObjectReference(
                    "region_id",
                    regions,
                    r => r.Id.ToString()),
                TaxRate = settlementRoot.GetDouble("tax_rate"),
                PopCultChangeTimer = settlementRoot.GetOptionalDouble("pop_cult_change_timer"),
                RiteCooldown = settlementRoot.GetOptionalDouble("rite_cooldown")
            };

            settlement.Region.Settlement = settlement;

            foreach (var popRoot in settlementRoot.GetChildren("pop"))
            {
                var popRace = popRoot.GetObjectReference(
                    "race_id",
                    gamedataTracker.Races,
                    r => r.Id);

                var caste = popRoot.GetObjectReference(
                    "caste_id",
                    race.Castes,
                    c => c.Id);

                var cult = popRoot.GetObjectReference(
                    "cult_id",
                    gamedataTracker.Cults,
                    c => c.Id);

                settlement.Population.Add(new Pop(
                    popRace,
                    caste,
                    cult));
            }

            foreach (var districtRoot in settlementRoot.GetChildren("district"))
            {
                District.DeserializeFrom(gamedataTracker, settlement, districtRoot);
            }

            foreach (var counterRoot in settlementRoot.GetChildren("growth_counter"))
            {
                var caste = counterRoot.GetObjectReference(
                    "caste_id",
                    settlement.Race.Castes,
                    c => c.Id);

                var value = counterRoot.GetDouble("value");

                settlement.GrowthCounterByCaste[caste] = value;
            }

            foreach (var availablilityRoot in settlementRoot.GetChildren("item_availability"))
            {
                var item = availablilityRoot.GetObjectReference(
                    "item",
                    gamedataTracker.Items,
                    i => i.Name);

                settlement.ItemAvailability[item] = true;
            }

            foreach (var productionRoot in settlementRoot.GetChildren("item_production"))
            {
                var item = productionRoot.GetObjectReference(
                    "item",
                    gamedataTracker.Items,
                    i => i.Name);

                var production = productionRoot.GetDouble("rate");

                settlement.LastMonthProduction[item] = production;
            }

            var recruitmentSlotRoots = settlementRoot.GetChildren("recruitment_slots");
            foreach(var recruitmentSlotRoot in recruitmentSlotRoots)
            {
                var category = recruitmentSlotRoot.GetObjectReference(
                    "id",
                    gamedataTracker.RecruitmentSlotCategories,
                    c => c.Id);

                var slots = recruitmentSlotRoot.GetDouble("current");

                if(!settlement.RecruitmentSlots.ContainsKey(category))
                {
                    settlement.RecruitmentSlots.Add(category, slots);
                }
                else
                {
                    settlement.RecruitmentSlots[category] = slots;
                }
            }
            
            GenUtils.LinkTilesToNeighbors(settlement);

            settlement.Traits.AddRange(settlementRoot
                .GetChildren("trait")
                .Select(x => x.GetObjectReference(
                    "id",
                    gamedataTracker.Traits,
                    t => t.Id)));

            settlement.SetCalculationFlag("save load");

            var allEconomicActors = settlement.Region.Cells
                .Select(c => c.Development)
                .OfType<IEconomicActor>()
                .Concat(settlement.Districts.SelectMany(d => d.Structures))
                .Where(s => s != null)
                .ToArray();

            foreach(var haulerRoot in settlementRoot.GetChildren("logi_hauler"))
            {
                var haulerInfo = haulerRoot.GetObjectReference(
                    "hauler_info",
                    gamedataTracker.HaulerInfos,
                    h => h.Id);

                var cargo = new ItemQuantity(
                    haulerRoot.GetObjectReference(
                        "cargo_item",
                        gamedataTracker.Items,
                        i => i.Name),
                    haulerRoot.GetDouble("cargo_qty"));

                var fromActor = haulerRoot.GetOptionalObjectReferenceOrDefault(
                    "from",
                    allEconomicActors,
                    ea => ea.Id);

                var toActor = haulerRoot.GetOptionalObjectReferenceOrDefault(
                    "to",
                    allEconomicActors,
                    ea => ea.Id);

                var pathChildren = haulerRoot.GetChildren("path").ToArray();

                var path = new List<Tile>();

                foreach(var pathRoot in haulerRoot.GetChildren("path"))
                {
                    var cellPos = new CellPosition(
                        pathRoot.GetInt("cell_plain"),
                        pathRoot.GetInt("cell_x"),
                        pathRoot.GetInt("cell_y"));

                    var cell = settlement.Districts.Where(d => d.Cell.Position == cellPos).FirstOrDefault()?.Cell;

                    if(cell != null)
                    {
                        var tile = settlement.GetTile(new TilePosition(
                            cell,
                            pathRoot.GetInt("tile_x"),
                            pathRoot.GetInt("tile_y")));

                        path.Add(tile);
                    }
                }
                if (fromActor != null && toActor != null)
                {
                    var hauler = new LogiHauler(
                        fromActor,
                        toActor,
                        path.ToArray(),
                        cargo,
                        haulerInfo)
                    {
                        PositionOnPath = haulerRoot.GetInt("path_pos"),
                        ProgressToNext = haulerRoot.GetDouble("to_next")
                    };

                    settlement.AddLogiHauler(hauler);
                }
            }

            foreach(var buffRoot in settlementRoot.GetChildren("buff"))
            {
                settlement._buffs.Add(Buff.DeserializeFrom(gamedataTracker, buffRoot));
            }
            
            return settlement;
        }

        public void SetCalculationFlag(string reason)
        {
            _calculationReasons.Add(reason);
            NeedsCalculation = true;
        }

        public void ClearCalculationFlag()
        {
            _calculationReasons.Clear();
            NeedsCalculation = false;
        }

        public void ChangeOwner(Tribe newOwner)
        {
            SetCalculationFlag("new owner");
            Owner.Settlements.Remove(this);
            Owner = newOwner;
            Owner.Settlements.Add(this);
            
            if(Governor != null)
            {
                Governor.ClearAssignments();
            }
        }

        public void AddLogiHauler(DeliveryRoute deliveryRoute, ItemQuantity cargo)
        {
            var hauler = new LogiHauler(
                deliveryRoute.Source,
                deliveryRoute.Destination,
                deliveryRoute.Path,
                cargo,
                deliveryRoute.Hauler);

            AddLogiHauler(hauler);
        }

        public void AddLogiHauler(LogiHauler logiHauler)
        {
            _logiHaulers.Add(logiHauler);
            logiHauler.From.OutgoingHaulers.Add(logiHauler);
            logiHauler.To.IncomingHaulers.Add(logiHauler);
            
            logiHauler.To.IncomingItems[logiHauler.Cargo.Item] += logiHauler.Cargo.Quantity;
        }

        public void RemoveLogiHauler(LogiHauler logiHauler)
        {
            logiHauler.To.IncomingItems[logiHauler.Cargo.Item] -= logiHauler.Cargo.Quantity;
            _logiHaulers.Remove(logiHauler);
            logiHauler.To.IncomingHaulers.Remove(logiHauler);
            logiHauler.From.OutgoingHaulers.Remove(logiHauler);
        }

        public void AddBuff(Buff buff)
        {
            _buffs.Add(buff);
            SetCalculationFlag("buff added");
        }

        public void RemoveBuff(Buff buff)
        {
            _buffs.Remove(buff);
            SetCalculationFlag("buff removed");
        }
    }
}
