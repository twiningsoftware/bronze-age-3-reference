﻿using Bronze.Contracts.Data.Simulation;

namespace Bronze.Contracts.Data.World
{
    public interface IAuthorityProvider : IEconomicActor
    {
        int AuthorityProvided { get; }
    }
}
