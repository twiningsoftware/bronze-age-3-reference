﻿using Bronze.Contracts.Data.Gamedata;

namespace Bronze.Contracts.Data.World
{
    public struct ItemQuantity
    {
        public Item Item { get; }
        public double Quantity { get; }

        public ItemQuantity(Item item, double quantity)
        {
            Item = item;
            Quantity = quantity;
        }
    }
}