﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Contracts.Data.World
{
    public class NotablePerson
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public bool IsMale { get; set; }
        public double BirthDate { get; set; }
        public Race Race { get; set; }
        public Tribe BirthTribe { get; set; }
        public Tribe Owner { get; set; }
        public bool IsDead { get; set; }
        public bool IsMature { get; set; }
        public bool InDynasty { get; set; }
        public Cult CultMembership { get; set; }

        public bool IsIdle => IsMature && !IsDead && GeneralOf == null && GovernorOf == null;

        public string HimHer => IsMale ? "him" : "her";
        public string HeShe => IsMale ? "he" : "she";
        public string HeSheCap => IsMale ? "He" : "She";

        public NotablePerson Father { get; set; }
        public NotablePerson Mother { get; set; }
        public List<NotablePerson> Children { get; set; }
        public NotablePerson Spouse { get; set; }

        public List<PortraitPartPlacement> PortraitParts { get; set; }
        public List<NotablePersonSkillPlacement> Skills { get; set; }

        public Settlement GovernorOf { get; private set; }
        public Army GeneralOf { get; private set; }
        public NotablePersonAgeBracket AgeBracket { get; set; }

        public NotablePerson()
        {
            Id = Guid.NewGuid().ToString();
            Children = new List<NotablePerson>();
            PortraitParts = new List<PortraitPartPlacement>();
            Skills = new List<NotablePersonSkillPlacement>();
        }
        
        public string CurrentAssignment
        {
            get
            {
                if(GovernorOf != null)
                {
                    return "Governor of " + GovernorOf.Name;
                }

                if(GeneralOf != null)
                {
                    return "General of " + GeneralOf.Name;
                }

                return string.Empty;
            }
        }
        
        public void SerializeTo(SerializedObject personRoot)
        {
            personRoot.Set("id", Id);
            personRoot.Set("name", Name);
            personRoot.Set("is_male", IsMale);
            personRoot.Set("birthdate", BirthDate);
            personRoot.Set("race_id", Race.Id);
            personRoot.Set("owner_id", Owner.Id);
            personRoot.Set("birth_tribe_id", BirthTribe.Id);
            personRoot.Set("is_dead", IsDead);
            personRoot.Set("father_id", Father?.Id);
            personRoot.Set("mother_id", Mother?.Id);
            personRoot.Set("spouse_id", Spouse?.Id);
            personRoot.Set("in_dynasty", InDynasty);
            personRoot.Set("is_king", Owner.Ruler == this);
            personRoot.Set("is_heir", Owner.Heir == this);
            personRoot.Set("is_mature", IsMature);
            personRoot.Set("governor_of", GovernorOf?.Id);
            personRoot.Set("general_of", GeneralOf?.Id);
            personRoot.Set("age_bracket", AgeBracket);
            personRoot.Set("cult_membership", CultMembership?.Id);

            foreach (var part in PortraitParts)
            {
                var partRoot = personRoot.CreateChild("portrait_part");
                partRoot.Set("group_id", part.GroupId);
                partRoot.Set("part_id", part.Part.Id);
            }

            foreach (var skill in Skills)
            {
                var skillRoot = personRoot.CreateChild("skill");
                skillRoot.Set("skill_id", skill.Skill.Id);
                skillRoot.Set("level", skill.Level.Level);
            }
        }

        public static NotablePerson DeserializeFrom(
            IGamedataTracker gamedataTracker,
            IEnumerable<Tribe> tribes,
            SerializedObject personRoot)
        {
            var person = new NotablePerson
            {
                Id = personRoot.GetString("id"),
                Name = personRoot.GetString("name"),
                IsMale = personRoot.GetBool("is_male"),
                BirthDate = personRoot.GetDouble("birthdate"),
                Race = personRoot.GetObjectReference(
                    "race_id",
                    gamedataTracker.Races,
                    r => r.Id),
                Owner = personRoot.GetObjectReference(
                    "owner_id",
                    tribes,
                    t => t.Id),
                BirthTribe = personRoot.GetObjectReference(
                    "birth_tribe_id",
                    tribes,
                    t => t.Id),
                IsDead = personRoot.GetBool("is_dead"),
                IsMature = personRoot.GetBool("is_mature"),
                InDynasty = personRoot.GetBool("in_dynasty"),
                AgeBracket = personRoot.GetEnum<NotablePersonAgeBracket>("age_bracket"),
                PortraitParts = personRoot.GetChildren("portrait_part")
                    .Select(c => new PortraitPartPlacement
                    {
                        GroupId = c.GetString("group_id"),
                        Part = c.GetObjectReference(
                            "part_id",
                            gamedataTracker.PeopleParts,
                            p => p.Id)
                    })
                    .ToList(),
                Skills = personRoot.GetChildren("skill")
                    .Select(e => new NotablePersonSkillPlacement
                    {
                        Skill = e.GetObjectReference(
                            "skill_id",
                            gamedataTracker.Skills,
                            s => s.Id),
                        Level = e.GetObjectReference(
                            "level",
                            e.GetObjectReference(
                                "skill_id",
                                gamedataTracker.Skills,
                                s => s.Id).Levels,
                            l => l.Level.ToString())
                    })
                    .ToList(),
                CultMembership = personRoot.GetOptionalObjectReference(
                    "cult_membership",
                    gamedataTracker.Cults,
                    c => c.Id)
            };

            
            if(personRoot.GetBool("is_king"))
            {
                person.Owner.Ruler = person;
            }
            if (personRoot.GetBool("is_heir"))
            {
                person.Owner.Heir = person;
            }

            var governed = personRoot.GetOptionalObjectReferenceOrDefault(
                "governor_of",
                person.Owner.Settlements,
                s => s.Id);

            if(governed != null)
            {
                governed.Governor = person;
                person.GovernorOf = governed;
            }

            var generaled = personRoot.GetOptionalObjectReferenceOrDefault(
                "general_of",
                person.Owner.Armies,
                a => a.Id);
            if (generaled != null)
            {
                generaled.General = person;
                person.GeneralOf = generaled;
            }

            return person;
        }
        
        public void ClearAssignments()
        {
            if (GeneralOf != null)
            {
                GeneralOf.General = null;
                GeneralOf = null;
            }
            if (GovernorOf != null)
            {
                GovernorOf.Governor = null;
                GovernorOf = null;
            }
        }
        
        public void AssignTo(Army army)
        {
            ClearAssignments();

            if (army.General != null)
            {
                army.General.ClearAssignments();
            }

            army.General = this;
            GeneralOf = army;
        }

        public void AssignTo(Settlement settlement)
        {
            ClearAssignments();

            if(settlement.Governor != null)
            {
                settlement.Governor.ClearAssignments();
            }

            settlement.Governor = this;
            GovernorOf = settlement;
        }
        
        public static void DeserializePostLinking(
            IGamedataTracker gamedataTracker,
            List<NotablePerson> people,
            SerializedObject personRoot)
        {
            var person = personRoot.GetObjectReference(
                "id",
                people,
                p => p.Id);

            person.Father = personRoot.GetOptionalObjectReference(
                "father_id",
                people,
                p => p.Id);

            person.Mother = personRoot.GetOptionalObjectReference(
                "mother_id",
                people,
                p => p.Id);

            person.Spouse = personRoot.GetOptionalObjectReference(
                "spouse_id",
                people,
                p => p.Id);

            if (person.Father != null)
            {
                person.Father.Children.Add(person);
            }
            if (person.Mother != null)
            {
                person.Mother.Children.Add(person);
            }
        }

        public int GetStat(NotablePersonStatType statType)
        {
            var stat = 0;

            for(var i = 0; i < Skills.Count; i++)
            {
                stat += Skills[i].Level.GetStat(statType);
            }

            return Util.Clamp(stat, 0, 10);
        }

        public double GetMinorStat(string statName)
        {
            var stat = 0.0;

            for (var i = 0; i < Skills.Count; i++)
            {
                stat += Skills[i].Level.GetMinorStat(statName);
            }

            return stat;
        }

        public int GetAuthority()
        {
            return (GetStat(NotablePersonStatType.Charisma)
                    + GetStat(NotablePersonStatType.Wit)
                    + GetStat(NotablePersonStatType.Valor))
                    * Constants.AUTHORITY_PER_STAT;
        }

        public double GetBonusBattleStartMorale()
        {
            return GetStat(NotablePersonStatType.Charisma) / 10.0;
        }

        public double GetBonusMoraleRegen()
        {
            return (GetStat(NotablePersonStatType.Charisma) + 5.0) / 10.0;
        }

        public double GetBonusMoraleShockReduction()
        {
            return GetStat(NotablePersonStatType.Valor) / 20.0;
        }

        public double GetBonusArmySpeed()
        {
            return 0.3 * GetStat(NotablePersonStatType.Wit) / 10.0;
        }

        public double GetBonusProductivity()
        {
            return GetStat(NotablePersonStatType.Wit) / 50.0;
        }

        public double GetBonusSatisfaction()
        {
            return GetStat(NotablePersonStatType.Charisma) / 25.0;
        }

        public double GetCultInfluence(Cult cult)
        {
            var influence = 0.0;

            if (cult == CultMembership)
            {
                influence += GetStat(NotablePersonStatType.Charisma) / 20.0;
            }

            foreach(var skill in Skills)
            {
                influence += skill.Level.GetCultInfluence(cult);
            }

            return influence;
        }
    }
}
