﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Simulation;

namespace Bronze.Contracts.Data.World
{
    public class DeliveryRoute
    {
        public IEconomicActor Source { get; }
        public IEconomicActor Destination { get; }
        public HaulerInfo Hauler { get; }
        public Tile[] Path { get; }
        public bool IsExternalPath { get; private set; }

        public DeliveryRoute(
            IEconomicActor source,
            IEconomicActor destination,
            HaulerInfo hauler,
            Tile[] path,
            bool isExternalPath)
        {
            Source = source;
            Destination = destination;
            Hauler = hauler;
            Path = path;
            IsExternalPath = isExternalPath;
        }

        public override string ToString()
        {
            return $"From {Source.ToString()} To {Destination.ToString()}";
        }
    }
}
