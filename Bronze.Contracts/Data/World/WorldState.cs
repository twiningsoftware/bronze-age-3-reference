﻿using Bronze.Contracts.Data.Gamedata;
using System.Collections.Generic;

namespace Bronze.Contracts.Data.World
{
    public class WorldState
    {
        public string Name { get; }
        public string Seed { get; }
        public double Month { get; }
        public double Hostility { get; }
        public int Size { get; }
        public IEnumerable<Plane> Planes { get; }
        public Tribe PlayerTribe { get; }
        public IEnumerable<Tribe> AllTribes { get; }
        public IEnumerable<NotablePerson> NotablePeople { get; }
        public bool IsPreview { get; set; }
        public string[] EdgeAdjacencies { get; set; }
        public IEnumerable<Trait> WorldTraits { get; }

        public WorldState(
            string name,
            string seed,
            double month,
            double hostility,
            int size,
            IEnumerable<Plane> planes,
            Tribe playerTribe,
            IEnumerable<Tribe> allTribes, 
            IEnumerable<NotablePerson> notablePeople,
            bool isPreview,
            string[] edgeAdjacencies,
            IEnumerable<Trait> worldTraits)
        {
            Name = name;
            Seed = seed;
            Month = month;
            Hostility = hostility;
            Size = size;
            Planes = planes;
            PlayerTribe = playerTribe;
            AllTribes = allTribes;
            NotablePeople = notablePeople;
            IsPreview = isPreview;
            EdgeAdjacencies = edgeAdjacencies;
            WorldTraits = worldTraits;
        }
    }
}
