﻿namespace Bronze.Contracts.Data.World
{
    public enum Hostility
    {
        Ally,
        Neutral,
        Hostile,
        War
    }
}
