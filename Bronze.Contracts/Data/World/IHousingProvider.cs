﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Simulation;
using System.Collections.Generic;

namespace Bronze.Contracts.Data.World
{
    public interface IHousingProvider : IEconomicActor
    {
        IEconomicActor Distributor { get; set; }
        double EffectiveTaxRate { get; set; }
        List<Pop> Residents { get; }
        Caste SupportedCaste { get; }
        int ProvidedHousing { get; }
        double AverageSatisfaction { get; }
        IntIndexableLookup<Cult, double> InfluencePerPop { get; }

        double NeedPerMonthOf(Item item);
    }
}
