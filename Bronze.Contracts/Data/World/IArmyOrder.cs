﻿using Bronze.Contracts.Data.Serialization;

namespace Bronze.Contracts.Data.World
{
    public interface IArmyOrder
    {
        string ActionTypeName { get; }

        bool IsIdle { get; }

        void Simulate(Army army, double deltaMonths, AggregatePerformanceTracker aggregatePerformanceTracker);

        void SerializeTo(SerializedObject orderRoot);

        void SetAnimationFor(Army army, IUnit unit);

        string GetDescription();

        void OnOrderChanged(Army army, IArmyOrder newOrder);
    }
}
