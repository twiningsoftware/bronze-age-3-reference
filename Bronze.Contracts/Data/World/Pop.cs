﻿using Bronze.Contracts.Data.Gamedata;
using System;

namespace Bronze.Contracts.Data.World
{
    public class Pop
    {
        public const double CHANGE_RATE = 0.1;

        public Caste Caste { get; set; }
        public Race Race { get; }
        public IHousingProvider Home { get; set; }
        public Cult CultMembership { get; set; }

        public double Satisfaction { get; private set; }
        public double GrowthBonus { get; set; }

        public Pop(Race race, Caste caste, Cult cultMembership)
        {
            Race = race;
            Caste = caste;
            CultMembership = cultMembership;
            Satisfaction = 1;
            GrowthBonus = 1;
        }

        public void AddSatisfaction(double deltaMonths, double satisfaction)
        {
            var delta = satisfaction - Satisfaction;

            if(delta > 0)
            {
                delta = Math.Min(delta, CHANGE_RATE * deltaMonths);
            }
            else if(delta < 0)
            {
                delta = Math.Max(delta, -1 * CHANGE_RATE * deltaMonths);
            }

            Satisfaction += delta;
        }
    }
}