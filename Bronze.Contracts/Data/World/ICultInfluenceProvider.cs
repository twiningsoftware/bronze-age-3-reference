﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Simulation;

namespace Bronze.Contracts.Data.World
{
    public interface ICultInfluenceProvider : IEconomicActor
    {
        Cult Cult { get; }
        double InfluencePerPop { get; }
    }
}
