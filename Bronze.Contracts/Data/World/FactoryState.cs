﻿namespace Bronze.Contracts.Data.World
{
    public enum FactoryState
    {
        Operating,
        NoRecipie,
        NeedsInputs,
        OutputsFull
    }
}
