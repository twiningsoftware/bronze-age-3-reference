﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.InjectableServices;
using System.Collections.Generic;
using System.Numerics;

namespace Bronze.Contracts.Data.World
{
    public interface IWorldActor
    {
        string Id { get; }
        Facing Facing { get; set; }
        Vector2 Position { get; set; }
        Cell Cell { get; set; }

        void Draw(IDrawingService drawingService, ViewMode viewMode, bool isSelected);
        IEnumerable<SoundEffectType> GetSoundEffects();
    }
}
