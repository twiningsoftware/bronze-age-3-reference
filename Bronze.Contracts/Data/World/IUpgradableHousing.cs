﻿namespace Bronze.Contracts.Data.World
{
    public interface IUpgradableHousing : IHousingProvider
    {
        int UpgradePercent { get; }
        int DowngradePercent { get; }
    }
}
