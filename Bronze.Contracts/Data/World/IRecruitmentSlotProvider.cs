﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Simulation;

namespace Bronze.Contracts.Data.World
{
    public interface IRecruitmentSlotProvider : IEconomicActor
    {
        RecruitmentSlotCategory RecruitmentCategory { get; }
        int ProvidedRecruitmentSlots { get; }
        int MaximumProvidedRecruitmentSlots { get; }
    }
}
