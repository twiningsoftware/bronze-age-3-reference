﻿namespace Bronze.Contracts.Data
{
    public enum AlertLevel
    {
        EnemySighted = 2,
        Invader = 1,
        SiegeInProgress = 0,
    }
}
