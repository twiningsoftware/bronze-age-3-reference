﻿namespace Bronze.Contracts.Data
{
    public enum BronzeColor
    {
        None,
        Blue,
        Cyan,
        Red,
        Yellow,
        Orange,
        Green,
        Purple,
        White,
        Grey,
        Black,
        Maroon,
        ForestGreen,
        Pink,
        Navy,
        Brown,
        LightPurple
    }
}
