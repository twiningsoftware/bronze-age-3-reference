﻿using Bronze.Contracts.Data.Gamedata;

namespace Bronze.Contracts.Data.Trade
{
    public class TradeCapInfo
    {
        public MovementType MovementType { get; }
        public double Capacity { get; }

        public TradeCapInfo(MovementType movementType, double capacity)
        {
            MovementType = movementType;
            Capacity = capacity;
        }
    }
}
