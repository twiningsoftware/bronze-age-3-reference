﻿using Bronze.Contracts.Data.World;

namespace Bronze.Contracts.Data.Trade
{
    public class TradeCellExit
    {
        public Cell Cell { get; }
        public Cell FromCell { get; }

        public TradeCellExit(Cell cell, Cell fromCell)
        {
            Cell = cell;
            FromCell = fromCell;
        }
    }
}
