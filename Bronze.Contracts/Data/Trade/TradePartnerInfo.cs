﻿using Bronze.Contracts.Data.World;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Contracts.Data.Trade
{
    public class TradePartnerInfo
    {
        public Settlement Partner { get; }
        public IEnumerable<ItemRate> Exports { get; set; }
        public IEnumerable<ItemRate> Imports { get; set; }

        public TradePartnerInfo(Settlement partner)
        {
            Partner = partner;
            Exports = Enumerable.Empty<ItemRate>();
            Imports = Enumerable.Empty<ItemRate>();
        }
    }
}
