﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.World;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Contracts.Data.Trade
{
    public class TradeRoute
    {
        public string Id { get; }
        public MovementType MovementType { get; }
        public ITradeActor FromActor { get; }
        public Settlement FromSettlement { get; }
        public ITradeReciever ToActor { get; }
        public Settlement ToSettlement { get; }
        public TradePathCalculator PathCalculator { get; }
        public bool IsTreatyRoute { get; set; }
        public TradeHaulerInfo TradeHauler => FromActor.TradeHauler;

        public double TotalCapacity => FromActor.TradeCapacity;
        public double UsedCapacity => _perMonth.Values.Sum();
        public bool RouteValid => !PathCalculator.PathFinished || PathCalculator.PathSuccessful;
        
        private Dictionary<Item, double> _perMonth;

        public TradeRoute(
            MovementType movementType,
            ITradeActor fromActor,
            Settlement fromSettlement,
            ITradeReciever toActor,
            Settlement toSettlement)
        {
            MovementType = movementType;
            FromActor = fromActor;
            FromSettlement = fromSettlement;
            ToActor = toActor;
            ToSettlement = toSettlement;
            PathCalculator = new TradePathCalculator(FromSettlement.Owner, movementType, fromActor, toActor);
            _perMonth = new Dictionary<Item, double>();
        }

        public IEnumerable<ItemRate> Exports => _perMonth
            .Where(kvp => kvp.Value > 0)
            .Select(kvp => new ItemRate(kvp.Key, kvp.Value))
            .ToArray();

        public void SetExportPerMonth(Item item, double value)
        {
            if (!_perMonth.ContainsKey(item))
            {
                _perMonth.Add(item, value);
            }
            else
            {
                _perMonth[item] = value;
            }

            FromSettlement.SetCalculationFlag("trade qty changed");
            ToSettlement.SetCalculationFlag("trade qty changed");
        }

        public double GetExportPerMonth(Item item)
        {
            if (!_perMonth.ContainsKey(item))
            {
                return 0;
            }
            else
            {
                return _perMonth[item];
            }
        }
    }
}
