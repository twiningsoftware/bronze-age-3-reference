﻿using Bronze.Contracts.Data.World;
using System.Collections.Generic;

namespace Bronze.Contracts.Data.Trade
{
    public class TraderToDistrictBorderPath
    {
        public TradeCellExit CellExit { get;}
        public IEnumerable<Tile> Path { get; }

        public TraderToDistrictBorderPath(Cell cell, Cell fromCell, IEnumerable<Tile> path)
        {
            CellExit = new TradeCellExit(cell, fromCell);
            Path = path;
        }
    }
}
