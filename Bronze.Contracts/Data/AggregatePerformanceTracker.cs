﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Contracts.Data
{
    public class AggregatePerformanceTracker
    {
        public Dictionary<string, long> _values;

        public AggregatePerformanceTracker()
        {
            _values = new Dictionary<string, long>();
        }

        public void AddValue(string key, long elapsedTicks)
        {
            if(!_values.ContainsKey(key))
            {
                _values.Add(key, 0);
            }

            _values[key] += elapsedTicks;
        }

        public IEnumerable<Tuple<string, long>> GetValues()
        {
            return _values
                .Select(kvp => Tuple.Create(kvp.Key, kvp.Value))
                .ToArray();
        }
     }
}
