﻿using System;
using System.Xml.Linq;

namespace Bronze.Contracts.Exceptions
{
    public class DataLoadException : BronzeException
    {
        public override bool IsFatal => false;

        private DataLoadException(string message)
            :base(message)
        {
        }

        private DataLoadException(string message, Exception ex)
            : base(message, ex)
        {
        }

        public static DataLoadException FailureInFile(
            string filename,
            Exception ex)
        {
            return new DataLoadException(
                $"Error when reading {filename}: " + ex.Message,
                ex);
        }

        public static DataLoadException FailureInFile(
            string filename, 
            string elementName, 
            Exception ex)
        {
            return new DataLoadException(
                $"Error when loading {elementName} data from {filename}: " + ex.Message,
                ex);
        }

        public static DataLoadException NamelistLoadFailure(
            string filename,
            Exception ex)
        {
            return new DataLoadException(
                $"Error when loading namelists from {filename}: " + ex.Message,
                ex);
        }

        public static DataLoadException ScriptLoadFailure(
            string filename,
            Exception ex)
        {
            return new DataLoadException(
                $"Error when reading scripts from {filename}: " + ex.Message,
                ex);
        }
        
        public static DataLoadException MissingDataReference(
            string objectType,
            string referenceObjectType,
            string refrenceId)
        {
            return new DataLoadException(
                $"A {objectType} is attempting to reference a {referenceObjectType} with the id {refrenceId}, but the {referenceObjectType} cannot be found.");
        }

        public static DataLoadException XmlMissingDescendant(
            XElement element,
            string descendantName)
        {
            return new DataLoadException(
                $"Element {element.Name} is expected to have descendant {descendantName}, but it does not.\n{element.ToString()}");
        }

        public static DataLoadException XmlMissingAttribute(
            XElement element,
            string attributeName)
        {
            if (element.Parent != null)
            {
                return new DataLoadException(
                    $"Element {element.Name} under {element.Parent.Name} is expected to have attribute {attributeName}, but it does not.\n{element.ToString()}");
            }

            return new DataLoadException(
                $"Element {element.Name} is expected to have attribute {attributeName}, but it does not.\n{element.ToString()}");
        }

        public static DataLoadException MiscError(
            XElement element,
            string message)
        {
            if (element.Parent != null)
            {
                return new DataLoadException(
                    $"Error processing element {element.Name} under {element.Parent.Name}: {message}\n{element.ToString()}");
            }

            return new DataLoadException(
                $"Error processing element {element.Name}: {message}\n{element.ToString()}");
        }

        public static DataLoadException XmlAttributeParse(
            XElement element,
            string attributeName,
            string typeName,
            string value)
        {
            if (element.Parent != null)
            {
                return new DataLoadException(
                    $"Attribute {attributeName} on element {element.Name} under {element.Parent.Name} is expected to be of type {typeName} but cannot be parsed as such, value is '{value}'.\n{element.ToString()}");
            }

            return new DataLoadException(
                $"Attribute {attributeName} on element {element.Name} is expected to be of type {typeName} but cannot be parsed as such, value is '{value}'.\n{element.ToString()}");
        }

        public static Exception InvalidValue(XElement element, string attributeName, string message)
        {
            if (element.Parent != null)
            {
                return new DataLoadException(
                    $"Attribute {attributeName} on element {element.Name} under {element.Parent.Name}: {message}\n{element.ToString()}");
            }

            return new DataLoadException(
                $"Attribute {attributeName} on element {element.Name}: {message}\n{element.ToString()}");
        }
    }
}
