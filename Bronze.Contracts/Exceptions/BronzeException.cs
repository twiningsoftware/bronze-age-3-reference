﻿using System;

namespace Bronze.Contracts.Exceptions
{
    public abstract class BronzeException : Exception
    {
        public abstract bool IsFatal { get; }

        protected BronzeException(string message)
            : base(message)
        {
        }

        protected BronzeException(string message, Exception ex)
            : base(message, ex)
        {
        }
    }
}
