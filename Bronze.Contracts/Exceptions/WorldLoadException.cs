﻿using Bronze.Contracts.Data.Serialization;
using System;

namespace Bronze.Contracts.Exceptions
{
    public class WorldLoadException : BronzeException
    {
        public override bool IsFatal => false;

        public SaveSummary SaveFile { get; }
        
        public WorldLoadException(SaveSummary saveFile, string message, Exception innerException)
            : base(message, innerException)
        {
            SaveFile = saveFile;
        }

        public WorldLoadException(SaveSummary saveFile, string message)
            : base(message)
        {
            SaveFile = saveFile;
        }
    }
}
