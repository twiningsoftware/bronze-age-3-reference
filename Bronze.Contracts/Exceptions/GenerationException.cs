﻿namespace Bronze.Contracts.Exceptions
{
    public class GenerationException : BronzeException
    {
        public override bool IsFatal => true;

        public GenerationException(string message)
            :base(message)
        {
        }
    }
}
