﻿using System;

namespace Bronze.Contracts.Exceptions
{
    public class ModUnpackException : BronzeException
    {
        public override bool IsFatal { get; }

        private ModUnpackException(string message, Exception ex, bool isFatal) 
            : base($"Error loading the base mod, Bronze Age must exit.", ex)
        {
        }

        public static ModUnpackException BaseModFailed(Exception ex)
        {
            return new ModUnpackException(
                $"Error unpacking the base mod, Bronze Age must exit: " + ex.Message,
                ex,
                isFatal: true);
        }

        public static ModUnpackException ModFailed(string modFile, Exception ex)
        {
            return new ModUnpackException(
                $"Error unpacking mod {modFile}: " + ex.Message,
                ex,
                isFatal: false);
        }
    }
}
