﻿using Bronze.Contracts.Data.Serialization;

namespace Bronze.Contracts.Exceptions
{
    public class WorldLoadSchemaException : WorldLoadException
    {
        public WorldLoadSchemaException(SaveSummary saveFile, string message)
            :base(saveFile, message)
        {
        }
    }
}
