﻿using System;

namespace Bronze.Contracts.Exceptions
{
    public class FatalBronzeException : BronzeException
    {
        public override bool IsFatal => true;

        public FatalBronzeException(string message)
            : base(message)
        {
        }

        public FatalBronzeException(string message, Exception ex)
            : base(message, ex)
        {
        }
    }
}
