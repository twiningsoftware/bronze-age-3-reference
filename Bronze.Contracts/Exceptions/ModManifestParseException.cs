﻿using System;

namespace Bronze.Contracts.Exceptions
{
    public class ModManifestParseException : BronzeException
    {
        public override bool IsFatal => false;

        public ModManifestParseException(string filename, Exception ex) 
            : base($"Error reading manifest for mod {filename}.", ex)
        {
        }
    }
}
