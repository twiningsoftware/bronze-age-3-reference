﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Generation;
using Bronze.Contracts.Data.World;

namespace Bronze.Contracts
{
    public static class GenUtils
    {
        public static void ScaleMap(float[,] map, float targetMin, float targetMax)
        {
            var minOnMap = float.MaxValue;
            var maxOnMap = float.MinValue;

            for (var x = 0; x < map.GetLength(0); x++)
            {
                for (var y = 0; y < map.GetLength(1); y++)
                {
                    minOnMap = Math.Min(minOnMap, map[x, y]);
                    maxOnMap = Math.Max(maxOnMap, map[x, y]);
                }
            }

            var rangeScale = (targetMax - targetMin) / (maxOnMap - minOnMap);

            for (var x = 0; x < map.GetLength(0); x++)
            {
                for (var y = 0; y < map.GetLength(1); y++)
                {
                    // Set Minimum
                    map[x, y] = map[x, y] - minOnMap + targetMin;
                    // Scale range
                    map[x, y] = map[x, y] * rangeScale;
                }
            }
        }

        public static float Bilerp(float ul, float ur, float ll, float lr, float a, float b)
        {
            var u = ul * (1 - a) + ur * a;
            var l = ll * (1 - a) + lr * a;

            return u * (1 - b) + l * b;
        }

        public static void LinkTilesToNeighbors(Settlement settlement)
        {
            var directionsAndDeltas = new Dictionary<Facing, Tuple<int, int>>()
            {
                {Facing.East, new Tuple<int, int>(1,0) },
                {Facing.NorthEast, new Tuple<int, int>(1,-1) },
                {Facing.North, new Tuple<int, int>(0,-1) },
                {Facing.NorthWest, new Tuple<int, int>(-1,-1) },
                {Facing.West, new Tuple<int, int>(-1,0) },
                {Facing.SouthWest, new Tuple<int, int>(-1,1) },
                {Facing.South, new Tuple<int, int>(0,1) },
                {Facing.SouthEast, new Tuple<int, int>(1,1) },
            };

            var districtLookup = settlement
                .Districts
                .Where(d => d.IsGenerated)
                .ToDictionary(d => d.Cell.Position, d => d.Tiles);

            foreach (var district in settlement.Districts.Where(d => d.IsGenerated))
            {
                for (var x = 0; x < TilePosition.TILES_PER_CELL; x++)
                {
                    for (var y = 0; y < TilePosition.TILES_PER_CELL; y++)
                    {
                        var tile = district.Tiles[x, y];

                        foreach (var kvp in directionsAndDeltas)
                        {
                            var otherX = x + kvp.Value.Item1;
                            var otherY = y + kvp.Value.Item2;
                            var otherCellX = district.Cell.Position.X;
                            var otherCellY = district.Cell.Position.Y;

                            if (otherX < 0)
                            {
                                otherX += TilePosition.TILES_PER_CELL;
                                otherCellX -= 1;
                            }
                            if (otherX >= TilePosition.TILES_PER_CELL)
                            {
                                otherX -= TilePosition.TILES_PER_CELL;
                                otherCellX += 1;
                            }
                            if (otherY < 0)
                            {
                                otherY += TilePosition.TILES_PER_CELL;
                                otherCellY -= 1;
                            }
                            if (otherY >= TilePosition.TILES_PER_CELL)
                            {
                                otherY -= TilePosition.TILES_PER_CELL;
                                otherCellY += 1;
                            }

                            var otherCellPos = new CellPosition(district.Cell.Position.Plane, otherCellX, otherCellY);

                            if (districtLookup.ContainsKey(otherCellPos))
                            {
                                tile.SetNeighbor(kvp.Key, districtLookup[otherCellPos][otherX, otherY]);
                            }
                            else
                            {
                                tile.SetNeighbor(kvp.Key, null);
                            }
                        }
                    }
                }
            }
        }

        public static void LinkTilesToNeighbors(Tile[,] tiles)
        {
            var directionsAndDeltas = new Dictionary<Facing, Tuple<int, int>>()
            {
                {Facing.East, new Tuple<int, int>(1,0) },
                {Facing.NorthEast, new Tuple<int, int>(1,-1) },
                {Facing.North, new Tuple<int, int>(0,-1) },
                {Facing.NorthWest, new Tuple<int, int>(-1,-1) },
                {Facing.West, new Tuple<int, int>(-1,0) },
                {Facing.SouthWest, new Tuple<int, int>(-1,1) },
                {Facing.South, new Tuple<int, int>(0,1) },
                {Facing.SouthEast, new Tuple<int, int>(1,1) },
            };
            
            for (var x = 0; x < TilePosition.TILES_PER_CELL; x++)
            {
                for (var y = 0; y < TilePosition.TILES_PER_CELL; y++)
                {
                    var tile = tiles[x, y];

                    foreach (var kvp in directionsAndDeltas)
                    {
                        var otherX = x + kvp.Value.Item1;
                        var otherY = y + kvp.Value.Item2;

                        if(otherX >= 0 && otherX < TilePosition.TILES_PER_CELL
                            && otherY >= 0 && otherY < TilePosition.TILES_PER_CELL)
                        {
                            tile.SetNeighbor(kvp.Key, tiles[otherX, otherY]);
                        }
                    }
                }
            }
        }

        public static void EvaluateSeedPointsGroups(
            GenerationContext context,
            string planeId,
            int worldSize,
            int basisX,
            int basisY,
            IEnumerable<ISeedPoint> seedPoints,
            IEnumerable<SeedPointGroup> seedPointGroups)
        {
            var seedPointPlacements = context.PlannedSeedPoints[planeId];

            foreach (var seedPoint in seedPoints)
            {
                var x = basisX + seedPoint.X + context.Random.Next(-seedPoint.PositionVariance / 2, seedPoint.PositionVariance / 2);
                var y = basisY + seedPoint.Y + context.Random.Next(-seedPoint.PositionVariance / 2, seedPoint.PositionVariance / 2);

                x = Math.Max(0, Math.Min(x, worldSize));
                y = Math.Max(0, Math.Min(y, worldSize));

                seedPointPlacements.Add(Tuple.Create(seedPoint, new System.Numerics.Vector2(x, y)));

                EvaluateSeedPointsGroups(
                    context,
                    planeId,
                    worldSize,
                    x,
                    y,
                    seedPoint.SeedPoints,
                    seedPoint.SeedPointGroups);
            }

            foreach (var group in seedPointGroups)
            {
                var options = group.Options.ToList();
                var picks = new List<ISeedPoint>();

                for (var i = 0; i < group.PickCount && options.Any(); i++)
                {
                    var r = context.Random.Next(options.Count);

                    picks.Add(options[r]);
                    options.RemoveAt(r);
                }

                if (picks.Any())
                {
                    EvaluateSeedPointsGroups(
                        context,
                        planeId,
                        worldSize,
                        basisX,
                        basisY,
                        picks,
                        Enumerable.Empty<SeedPointGroup>());
                }
            }
        }

        public static float ScoreRegion(
            Region region,
            Trait[] requiredTraits,
            Trait[] preferredTraits,
            Trait[] notPreferredTraits,
            Trait[] preferredNeighborTraits)
        {
            var acceptableCells = region.Cells
                .Where(c => c.Traits.ContainsAll(requiredTraits))
                .ToArray();

            if (acceptableCells.Any())
            {
                return acceptableCells
                    .Average(c => ScoreCell(
                        c,
                        requiredTraits,
                        preferredTraits,
                        notPreferredTraits,
                        preferredNeighborTraits,
                        new Trait[0],
                        new Trait[0]));
            }

            return -10000;
        }

        public static float ScoreCell(
            Cell cell,
            Trait[] requiredTraits,
            Trait[] preferredTraits,
            Trait[] notPreferredTraits,
            Trait[] preferredNeighborTraits,
            Trait[] notPreferredNeighborTraits,
            Trait[] requiredNeighborTraits)
        {
            var neighborTraits = cell.OrthoNeighbors
                .SelectMany(n => n.Traits)
                .Distinct()
                .ToArray();

            var score = requiredTraits
                .Select(t => cell.Traits.Contains(t) ? 1 : -10000)
                .Sum();

            score += preferredTraits
                .Select(t => cell.Traits.Contains(t) ? 1 : 0)
                .Sum();

            score += notPreferredTraits
                .Select(t => cell.Traits.Contains(t) ? -1 : 0)
                .Sum();

            score += preferredNeighborTraits
                .Select(t => neighborTraits.Contains(t) ? 1 : 0)
                .Sum();

            score += notPreferredNeighborTraits
                .Select(t => neighborTraits.Contains(t) ? -1 : 0)
                .Sum();

            score += requiredNeighborTraits
                .Select(t => neighborTraits.Contains(t) ? 1 : -10000)
                .Sum();

            return score;
        }

        public static TFeature[] DetermineFeaturesToPlace<TFeature>(
            Random random,
            IEnumerable<TFeature> featuresToChooseFrom,
            int basisCount,
            Func<TFeature, int> getCountPerBasis,
            Func<TFeature, int> getMinimumCount)
        {
            var featuresToPlace = new List<TFeature>();

            foreach (var feature in featuresToChooseFrom)
            {
                var rawPlacement = basisCount / (double)getCountPerBasis(feature);
                var fractionalPlacement = rawPlacement - (int)rawPlacement;

                var count = (int)rawPlacement;

                if (random.NextDouble() < fractionalPlacement)
                {
                    count += 1;
                }

                count = Math.Max(count, getMinimumCount(feature));

                for (var i = 0; i < count; i++)
                {
                    featuresToPlace.Add(feature);
                }
            }

            random.Shuffle(featuresToPlace);

            return featuresToPlace.ToArray();
        }
    }
}
