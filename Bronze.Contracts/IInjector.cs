﻿using Bronze.Contracts.Data.AI;
using Bronze.Contracts.Data.Diplomacy;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.UI;

namespace Bronze.Contracts
{
    public interface IInjector
    {
        void RegisterDialog<TModal>() where TModal : IModal;
        void RegisterService<TService>(TService instance);
        void RegisterService<TService>();
        void RegisterDevelopmentType<TDevelopmentType, TDevelopment>() where TDevelopmentType : ICellDevelopmentType where TDevelopment : ICellDevelopment;
        void RegisterStructureType<TStructureType, TStructure>() where TStructureType : IStructureType where TStructure : IStructure;
        void RegisterControllerType<TControllerType, TController>() where TControllerType : ITribeControllerType where TController : ITribeController;
        void RegisterUnitType<TUnitType, TUnit>() where TUnitType : IUnitType where TUnit : IUnit;
        void RegisterNamedType<TType, TAsType>();
        void RegisterWidget<TWidget>() where TWidget : IUiWidget;
        void RegisterTreaty<TTreaty>() where TTreaty : ITreaty;
        void RegisterTransportType<TTransportType>() where TTransportType : ITransportType;
    }
}
