﻿using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.World;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace Bronze.Contracts
{
    public static class Util
    {
        public static float Clamp(float value, float min, float max)
        {
            return Math.Max(
                    min,
                    Math.Min(
                        max,
                        value));
        }

        public static int Clamp(int value, int min, int max)
        {
            return Math.Max(
                    min,
                    Math.Min(
                        max,
                        value));
        }

        public static double Clamp(double value, double min, double max)
        {
            return Math.Max(
                    min,
                    Math.Min(
                        max,
                        value));
        }

        public static float Lerp(float a, float b, float factor)
        {
            return a * (1 - factor) + b * factor;
        }

        public static string FriendlyDateDisplay(double date)
        {
            var year = (int)Math.Floor(date / 12);
            var month = (int)Math.Floor(date % 12);
            var season = (int)Math.Floor(month / 3.0);
            var fractionalSeason = month / 3.0 - season;

            var fractionalSeasonText = "";
            if (fractionalSeason < 0.33)
            {
                fractionalSeasonText = "Early";
            }
            else if (fractionalSeason < 0.66)
            {
                fractionalSeasonText = "Mid";
            }
            else
            {
                fractionalSeasonText = "Late";
            }

            var seasonText = "";
            switch (season)
            {
                case 0:
                    seasonText = "Winter";
                    break;
                case 1:
                    seasonText = "Spring";
                    break;
                case 2:
                    seasonText = "Summer";
                    break;
                case 3:
                    seasonText = "Fall";
                    break;
            }

            return $"{fractionalSeasonText} {seasonText} {year}";
        }
        
        public static Vector2 Lerp(Vector2 a, Vector2 b, float factor)
        {
            return a * (1 - factor) + b * factor;
        }

        public static string FriendlyTimeDisplay(double months)
        {
            if(months >= 2)
            {
                return $"{(int)months} months";
            }
            else if (months >= 1)
            {
                return $"{(int)months} month";
            }

            return $"{(int)(months * 30)} days";
        }

        public static Facing GetFacing(Vector2 from, Vector2 to)
        {
            var delta = (to - from);

            var angle = Math.Atan2(delta.Y, delta.X);

            const double quarterPi = Math.PI / 4;

            if(angle > Math.PI)
            {
                angle -= 2 * Math.PI;
            }
            if (angle < -Math.PI)
            {
                angle += 2 * Math.PI;
            }

            if (angle >= 3 * quarterPi)
            {
                return Facing.West;
            }
            else if (angle >= 1 * quarterPi)
            {
                return Facing.South;
            }
            else if (angle >= -1 * quarterPi)
            {
                return Facing.East;
            }
            else if (angle >= -3 * quarterPi)
            {
                return Facing.North;
            }
            
            return Facing.West;
        }

        public static string PrependPositive(double value)
        {
            return (value >= 0 ? "+" : string.Empty) + value.ToString("F2");
        }

        public static string FriendlyDisplay(AlertLevel alertLevel)
        {
            switch(alertLevel)
            {
                case AlertLevel.EnemySighted:
                    return "Enemy Sighted";
                case AlertLevel.Invader:
                    return "Enemy Invading";
                case AlertLevel.SiegeInProgress:
                    return "Under Siege";
                default:
                    return alertLevel.ToString();
            }
        }

        public static BronzeColor PercentToColor(double healthPercent)
        {
            if(healthPercent <= 0.33)
            {
                return BronzeColor.Red;
            }
            else if(healthPercent <= 0.66)
            {
                return BronzeColor.Orange;
            }
            else
            {
                return BronzeColor.Green;
            }
        }

        //public static double FacingToAngle(Facing facing)
        //{
        //    switch(facing)
        //    {
        //        case Facing.East:
        //            return 0;
        //        case Facing.NorthEast:
        //            return Math.PI * 1 / 3f;
        //        case Facing.NorthWest:
        //            return Math.PI * 2 / 3f;
        //        case Facing.West:
        //            return Math.PI;
        //        case Facing.SouthWest:
        //            return Math.PI * 4 / 3f;
        //        case Facing.SouthEast:
        //        default:
        //            return Math.PI * 5 / 3f;
        //    }
        //}

        public static IEnumerable<T> FloodFill<T>(
            T start,
            Func<T, IEnumerable<T>> getNeighbors)
        {
            var closed = new HashSet<T>();
            var open = new Queue<T>();
            open.Enqueue(start);
            closed.Add(start);

            while (open.Any())
            {
                var current = open.Dequeue();

                var neighbors = getNeighbors(current)
                    .Where(n => !closed.Contains(n))
                    .ToArray();

                foreach(var neighbor in neighbors)
                {
                    closed.Add(neighbor);
                    open.Enqueue(neighbor);
                }
            }

            return closed;
        }

        public static void LinkAdjacentCells(Cell[,] cells)
        {
            var directionsAndDeltas = new Dictionary<Facing, Tuple<int, int>>()
            {
                {Facing.East, new Tuple<int, int>(1,0) },
                {Facing.NorthEast, new Tuple<int, int>(1,-1) },
                {Facing.North, new Tuple<int, int>(0,-1) },
                {Facing.NorthWest, new Tuple<int, int>(-1,-1) },
                {Facing.West, new Tuple<int, int>(-1,0) },
                {Facing.SouthWest, new Tuple<int, int>(-1,1) },
                {Facing.South, new Tuple<int, int>(0,1) },
                {Facing.SouthEast, new Tuple<int, int>(1,1) },
            };

            for (var x = 0; x < cells.GetLength(0); x++)
            {
                for (var y = 0; y < cells.GetLength(1); y++)
                {
                    var cell = cells[x, y];

                    foreach (var kvp in directionsAndDeltas)
                    {
                        var otherX = x + kvp.Value.Item1;
                        var otherY = y + kvp.Value.Item2;

                        if (otherX >= 0 && otherY >= 0
                            && otherX < cells.GetLength(0) && otherY < cells.GetLength(1))
                        {
                            cell.SetNeighbor(kvp.Key, cells[otherX, otherY]);
                        }
                        else
                        {
                            cell.SetNeighbor(kvp.Key, null);
                        }
                    }
                }
            }
        }

        public static void LinkAdjacentRegions(IEnumerable<Region> regions)
        {
            foreach(var region in regions)
            {
                var neighbors = region.Cells
                    .SelectMany(c => c.Neighbors)
                    .Where(c => c != null)
                    .Select(c => c.Region)
                    .Distinct()
                    .Where(r => r != region)
                    .ToArray();

                region.Neighbors.Clear();
                region.Neighbors.AddRange(neighbors);
            }
        }

        public static IEnumerable<Tile> CalculateFootprint(Tile ulTile, IStructureType structureType)
        {
            return TileBox(ulTile, structureType.TilesWide, structureType.TilesHigh);
        }

        public static IEnumerable<Tile> TileBox(Tile ulTile, int width, int height)
        {
            var footprint = new List<Tile>();

            if (ulTile != null)
            {
                for (var i = 0; i < width; i++)
                {
                    for (var j = 0; j < height; j++)
                    {
                        var tile = ulTile.District.Settlement
                            .GetTile(ulTile.Position.Relative(i, j));

                        if (tile != null)
                        {
                            footprint.Add(tile);
                        }
                    }
                }
            }

            return footprint;
        }

        public static float ViewModeToScale(ViewMode viewMode)
        {
            switch(viewMode)
            {
                case ViewMode.Settlement:
                case ViewMode.Detailed:
                    return 1;
                case ViewMode.Summary:
                    return 0.25f;
                default:
                    throw new ArgumentException("Unknown view mode: " + viewMode);
            }
        }

        public static double PullInputs(
            IEconomicActor actor,
            IEnumerable<ItemRate> inputs,
            double workRate)
        {
            if (!inputs.Any())
            {
                return workRate;
            }

            if (workRate == 0)
            {
                return 0;
            }

            var inputSatisfaction = 1.0;

            if (actor.Settlement != null)
            {
                foreach (var input in inputs)
                {
                    if (input.PerMonth > 0)
                    {
                        actor.Settlement.LastMonthProduction[input.Item] -= input.PerMonth * workRate;

                        var totalNeed = input.PerMonth * workRate;

                        if (totalNeed > 0)
                        {
                            var satisfaction = 0.0;
                            var qty = actor.Inventory.QuantityOf(input.Item);

                            if (qty > 0)
                            {
                                var toTake = Math.Min(totalNeed, qty);
                                actor.Inventory.Remove(input.Item, toTake);

                                satisfaction = toTake / totalNeed;
                            }

                            inputSatisfaction = Math.Min(inputSatisfaction, satisfaction);

                            actor.PerItemSatisfaction[input.Item] = satisfaction;
                        }
                    }
                }
            }

            return inputSatisfaction * workRate;
        }

        public static double FacingToRadian(Facing facing)
        {
            var quarterPi = Math.PI / 4;

            switch(facing)
            {
                case Facing.East:
                    return 0 * quarterPi;
                case Facing.NorthEast:
                    return -1 * quarterPi;
                case Facing.North:
                    return -2 * quarterPi;
                case Facing.NorthWest:
                    return -3 * quarterPi;
                case Facing.West:
                    return -4 * quarterPi;
                case Facing.SouthEast:
                    return 1 * quarterPi;
                case Facing.South:
                    return 2 * quarterPi;
                case Facing.SouthWest:
                    return 3 * quarterPi;
                default:
                    return 0;
            }
        }
    }
}
