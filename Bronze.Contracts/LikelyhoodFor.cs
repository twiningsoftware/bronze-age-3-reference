﻿namespace Bronze.Contracts
{
    public class LikelyhoodFor<T>
    {
        public int Likelyhood { get; set; }

        public T Value { get; set; }

        public override string ToString()
        {
            return $"({Value}: {Value})";
        }
    }
}
