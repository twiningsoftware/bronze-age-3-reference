﻿using Bronze.Common.Data.World.CellDevelopments;

namespace Bronze.Common.InjectableServices
{
    public interface IIrrigationCalculator
    {
        void Register(IrrigationDevelopment development);

        void Deregister(IrrigationDevelopment development);
    }
}
