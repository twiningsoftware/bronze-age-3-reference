﻿using Bronze.Common.Data.World.CellDevelopments;
using Bronze.Contracts.Data.World;

namespace Bronze.Common.InjectableServices
{
    public interface ICreepBanditManager
    {
        void Register(CreepCampDevelopment development);

        void Deregister(CreepCampDevelopment development);

        void HandleArmyAttack(CreepCampDevelopment development, Army attackingArmy);

        void HandleArmyAttack(CreepAttackableDevelopment development, Army attackingArmy);
    }
}
