﻿using System.Xml.Linq;

namespace Bronze.Common.SaveFileMigration
{
    public interface ISaveFileMigrator
    {
        string InputSchema { get; }
        string OutputSchema { get; }

        void Migrate(XDocument doc);
    }
}
