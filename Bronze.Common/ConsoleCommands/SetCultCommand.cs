﻿using Bronze.Common.Simulation.SettlementEvents;
using Bronze.Contracts;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.ConsoleCommands
{
    public class SetCultCommand : IConsoleCommand
    {
        public string CommandText => "setcult";

        public string QuickHelp => "setcult - change cult memberships for a settlement";

        private readonly IPlayerDataTracker _playerData;
        private readonly ILogger _logger;
        private readonly IGamedataTracker _gamedataTracker;

        public SetCultCommand(
            IPlayerDataTracker playerData,
            ILogger logger,
            IGamedataTracker gamedataTracker)
        {
            _playerData = playerData;
            _logger = logger;
            _gamedataTracker = gamedataTracker;
        }

        public string[] DetailedHelp => new[]
        {
            "----------",
            "setcult",
            "Change the cult membership of a the currently under" +
            "the mouse cursor. A number of pops up to the specified ",
            "limit will be chosen.",
            "\"setcult pop_count cult_id\"",
            "----------"
        };
        
        public void Execute(string[] parameters)
        {
            if (parameters.Length < 2)
            {
                _logger.Console("Specify pop_count and cult_id");
            }

            var targetSettlement = _playerData.HoveredCell?.Region?.Settlement;
            if (targetSettlement != null)
            {
                if(int.TryParse(parameters[0], out var count))
                {
                    var cultId = parameters[1];

                    var cult = _gamedataTracker.Cults
                        .Where(c => c.Id == cultId)
                        .FirstOrDefault();

                    if(cult != null)
                    {
                        var options = targetSettlement.Population.ToList();
                        var random = new Random();
                        var picked = 0;

                        for(var i = 0; i < count && options.Any(); i++)
                        {

                            var choice = random.Choose(options);
                            options.Remove(choice);
                            picked += 1;
                            choice.CultMembership = cult;
                        }

                        targetSettlement.SetCalculationFlag("cult memberships changed");

                        _logger.Console($"Converted {picked} pops to {cult.Name}");
                    }
                    else
                    {
                        _logger.Console("Unknown cult_id");
                    }
                }
                else
                {
                    _logger.Console("Specify pop_count as an integer.");
                }
            }
            else
            {
                _logger.Console("No settlement under cursor.");
            }
        }

        public string GetHint(string[] parameters)
        {
            return string.Empty;
        }
    }
}
