﻿using Bronze.Contracts.InjectableServices;
using System.Linq;

namespace Bronze.Common.ConsoleCommands
{
    public class RegenRecruitsCommand : IConsoleCommand
    {
        public string CommandText => "regenrecruits";

        public string QuickHelp => "regenrecruits - regenerate recruits";

        private readonly IPlayerDataTracker _playerData;
        private readonly ILogger _logger;

        public RegenRecruitsCommand(
            IPlayerDataTracker playerData,
            ILogger logger)
        {
            _playerData = playerData;
            _logger = logger;
        }

        public string[] DetailedHelp => new[]
        {
            "----------",
            "regenrecruits",
            "Regenerate all recruits for the settlement currently",
            "under the mouse cursor.",
            "\"regenrecruits\"",
            "----------"
        };
        
        public void Execute(string[] parameters)
        {
            var targetSettlement = _playerData.HoveredCell?.Region?.Settlement;
            if (targetSettlement != null)
            {
                foreach(var category in targetSettlement.RecruitmentSlots.Keys.ToArray())
                {
                    targetSettlement.RecruitmentSlots[category] = targetSettlement.RecruitmentSlotsMax[category];
                }
            }
            else
            {
                _logger.Console("No settlement under cursor.");
            }
        }

        public string GetHint(string[] parameters)
        {
            return string.Empty;
        }
    }
}
