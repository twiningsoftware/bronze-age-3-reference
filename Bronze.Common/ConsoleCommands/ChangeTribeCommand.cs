﻿using Bronze.Contracts;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System.Linq;

namespace Bronze.Common.ConsoleCommands
{
    public class ChangeTribeCommand : IConsoleCommand
    {
        public string CommandText => "changetribe";

        public string QuickHelp => "changetribe - change the player's tribe";

        private readonly IPlayerDataTracker _playerData;
        private readonly ILogger _logger;
        private readonly INameSource _nameSource;

        public ChangeTribeCommand(
            IPlayerDataTracker playerData,
            ILogger logger,
            INameSource nameSource)
        {
            _playerData = playerData;
            _logger = logger;
            _nameSource = nameSource;
        }

        public string[] DetailedHelp => new[]
        {
            "----------",
            "changetribe",
            "Change the player's tribe to that of the hovered army or",
            "settlement.",
            "\"changetribe\"",
            "----------"
        };
        
        public void Execute(string[] parameters)
        {
            if(_playerData.HoveredArmy != null)
            {
                _playerData.PlayerTribe = _playerData.HoveredArmy.Owner;
            }
            else if (_playerData.HoveredCell?.Region?.Settlement != null)
            {
                _playerData.PlayerTribe = _playerData.HoveredCell.Region.Settlement.Owner;
            }
        }

        public string GetHint(string[] parameters)
        {
            return string.Empty;
        }
    }
}
