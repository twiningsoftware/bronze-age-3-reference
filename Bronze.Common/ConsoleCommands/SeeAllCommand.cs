﻿using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.ConsoleCommands
{
    public class SeeAllCommand : IConsoleCommand
    {
        public string CommandText => "seeall";

        public string QuickHelp => "seeall - toggle unlimited vision";

        public string[] DetailedHelp => new[]
        {
            "----------",
            "seeall",
            "Toggle unlimited vision.",
            "----------"
        };

        private readonly ILosTracker _losTracker; 

        public SeeAllCommand(ILosTracker losTracker)
        {
            _losTracker = losTracker;
        }

        public void Execute(string[] parameters)
        {
            _losTracker.GodVision = !_losTracker.GodVision;
        }

        public string GetHint(string[] parameters)
        {
            return string.Empty;
        }
    }
}
