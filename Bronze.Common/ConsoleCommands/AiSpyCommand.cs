﻿using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.ConsoleCommands
{
    public class AiSpyCommand : IConsoleCommand
    {
        public string CommandText => "aispy";

        public string QuickHelp => "aispy - analize AI behavior";

        private readonly IPlayerDataTracker _playerData;
        private readonly IAiSpy _aiSpy;

        public AiSpyCommand(
            IPlayerDataTracker playerData,
            IAiSpy aiSpy)
        {
            _playerData = playerData;
            _aiSpy = aiSpy;
        }

        public string[] DetailedHelp => new[]
        {
            "----------",
            "aispy",
            "Toggle AI behavior monitoring for the tribe currently",
            "under the mouse cursor.",
            "\"aispy\"",
            "----------"
        };
        
        public void Execute(string[] parameters)
        {
            if(_playerData.HoveredArmy != null)
            {
                _aiSpy.ToggleFor(_playerData.HoveredArmy.Owner);
            }
            else if (_playerData.HoveredCell?.Region?.Settlement != null)
            {
                _aiSpy.ToggleFor(_playerData.HoveredCell.Region.Settlement.Owner);
            }
        }

        public string GetHint(string[] parameters)
        {
            return string.Empty;
        }
    }
}
