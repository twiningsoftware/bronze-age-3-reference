﻿using Bronze.Common.Simulation.SettlementEvents;
using Bronze.Contracts.InjectableServices;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.ConsoleCommands
{
    public class RebellionSpyCommand : IConsoleCommand
    {
        public string CommandText => "rebellionspy";

        public string QuickHelp => "rebellionspy - show rebellions info";

        private readonly IPlayerDataTracker _playerData;
        private readonly ILogger _logger;
        private readonly IRebellionEventTrigger[] _peasantRebellionEventTrigger;

        public RebellionSpyCommand(
            IPlayerDataTracker playerData,
            ILogger logger,
            IEnumerable<IRebellionEventTrigger> peasantRebellionEventTrigger)
        {
            _playerData = playerData;
            _logger = logger;
            _peasantRebellionEventTrigger = peasantRebellionEventTrigger.ToArray();
        }

        public string[] DetailedHelp => new[]
        {
            "----------",
            "rebellionspy",
            "Show rebellion information for the settlement",
            "currently under the mouse cursor",
            "\"rebellionspy\"",
            "----------"
        };
        
        public void Execute(string[] parameters)
        {
            var targetSettlement = _playerData.HoveredCell?.Region?.Settlement;
            if (targetSettlement != null)
            {
                _logger.Console("Rebellion info for " + targetSettlement.Name);

                foreach (var line in _peasantRebellionEventTrigger.SelectMany(et => et.GetInfoFor(targetSettlement)))
                {
                    _logger.Console(line);
                }
            }
            else
            {
                _logger.Console("No settlement under cursor.");
            }
        }

        public string GetHint(string[] parameters)
        {
            return string.Empty;
        }
    }
}
