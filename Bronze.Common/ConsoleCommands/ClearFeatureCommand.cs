﻿using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.ConsoleCommands
{
    public class ClearFeatureCommand : IConsoleCommand
    {
        public string CommandText => "clearfeature";

        public string QuickHelp => "clearfeature - clear a cell feature";

        public string[] DetailedHelp => new[]
        {
            "----------",
            "clearfeature",
            "Clear a cell feature from the cell under the mouse cursor.",
            "----------"
        };

        private readonly IPlayerDataTracker _playerData;

        public ClearFeatureCommand(IPlayerDataTracker playerData)
        {
            _playerData = playerData;
        }

        public void Execute(string[] parameters)
        {
            if(_playerData.HoveredCell != null
                && _playerData.HoveredCell.Feature != null)
            {
                _playerData.HoveredCell.Feature = null;
            }
        }

        public string GetHint(string[] parameters)
        {
            return string.Empty;
        }
    }
}
