﻿using Bronze.Common.Simulation.SettlementEvents;
using Bronze.Contracts.InjectableServices;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.ConsoleCommands
{
    public class RebellionToggleCommand : IConsoleCommand
    {
        public string CommandText => "rebelliontoggle";

        public string QuickHelp => "rebelliontoggle - toggle rebellions on or off";

        private readonly IPlayerDataTracker _playerData;
        private readonly ILogger _logger;
        private readonly IRebellionEventTrigger[] _rebellionEventTrigger;

        public RebellionToggleCommand(
            IPlayerDataTracker playerData,
            ILogger logger,
            IEnumerable<IRebellionEventTrigger> peasantRebellionEventTrigger)
        {
            _playerData = playerData;
            _logger = logger;
            _rebellionEventTrigger = peasantRebellionEventTrigger.ToArray();
        }

        public string[] DetailedHelp => new[]
        {
            "----------",
            "rebelliontoggle",
            "Toggle rebellions on or off for the settlement",
            "currently under the mouse cursor. This setting",
            "is not saved, and will have to be reset after",
            "each world load.",
            "\"rebelliontoggle\"",
            "----------"
        };
        
        public void Execute(string[] parameters)
        {
            var targetSettlement = _playerData.HoveredCell?.Region?.Settlement;
            if (targetSettlement != null)
            {
                foreach (var trigger in _rebellionEventTrigger)
                {
                    trigger.ToggleFor(targetSettlement);
                }
            }
            else
            {
                _logger.Console("No settlement under cursor.");
            }
        }

        public string GetHint(string[] parameters)
        {
            return string.Empty;
        }
    }
}
