﻿using Bronze.Contracts;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System.Linq;

namespace Bronze.Common.ConsoleCommands
{
    public class SpawnUnitCommand : IConsoleCommand
    {
        public string CommandText => "spawnunit";

        public string QuickHelp => "spawnunit - spawn a unit in a new army";

        private readonly IPlayerDataTracker _playerData;
        private readonly ILogger _logger;
        private readonly INameSource _nameSource;
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IWorldManager _worldManager;

        public SpawnUnitCommand(
            IPlayerDataTracker playerData,
            ILogger logger,
            INameSource nameSource,
            IGamedataTracker gamedataTracker,
            IWorldManager worldManager)
        {
            _playerData = playerData;
            _logger = logger;
            _nameSource = nameSource;
            _gamedataTracker = gamedataTracker;
            _worldManager = worldManager;
        }

        public string[] DetailedHelp => new[]
        {
            "----------",
            "spawnunit",
            "Spawns a unit for the player's tribe underneath the mouse",
            "cursor. If there is an army in that location, the unit ",
            "will be added to the army, Otherwise a new army will be",
            "created.",
            "\"spawnunit unit_id equipment_level\"",
            "----------"
        };
        
        public void Execute(string[] parameters)
        {
            if (parameters.Length > 1)
            {
                var unitType = _gamedataTracker.Races
                    .SelectMany(r => r.AvailableUnits)
                    .Where(u => u.Id == parameters[0])
                    .FirstOrDefault();
                
                if (unitType != null)
                {
                    var equipmentSet = unitType.EquipmentSets
                        .Where(es => es.Level.ToString() == parameters[1])
                        .FirstOrDefault();

                    if (equipmentSet != null)
                    {
                        if (_playerData.HoveredCell != null
                            && equipmentSet.CanPath(_playerData.HoveredCell))
                        {
                            var targetArmy = _playerData.HoveredCell.WorldActors
                                .OfType<Army>()
                                .Where(a => a.Owner == _playerData.PlayerTribe)
                                .Where(a => !a.IsFull)
                                .FirstOrDefault();

                            if (targetArmy == null)
                            {
                                targetArmy = new Army(_playerData.PlayerTribe, _playerData.HoveredCell, _worldManager)
                                {
                                    Name = _nameSource.MakeArmyName(_playerData.PlayerTribe.Race)
                                };
                                _playerData.PlayerTribe.Armies.Add(targetArmy);

                                _playerData.SelectedArmy = targetArmy;
                            }

                            var unit = unitType.CreateUnit(equipmentSet);
                            targetArmy.Units.Add(unit);
                        }
                        else
                        {
                            _logger.Console("Could not place unit at location.");
                        }
                    }
                    else
                    {
                        _logger.Console($"Unknown equipment level for {parameters[0]}: {parameters[1]}");
                    }
                }
                else
                {
                    _logger.Console("Unknown unit: " + parameters[0]);
                }
            }
            else
            {
                _logger.Console("Specify unit type.");
            }
        }

        public string GetHint(string[] parameters)
        {
            if(parameters.Length > 0)
            {
                var unit = _gamedataTracker.Races
                    .SelectMany(r => r.AvailableUnits)
                    .Where(u => u.Id.StartsWith(parameters[0]))
                    .FirstOrDefault();

                if(unit != null)
                {
                    if (unit.Id != parameters[0])
                    {
                        return unit.Id.Substring(parameters[0].Length);
                    }
                    else if(parameters.Length < 2)
                    {
                        return " 0";
                    }
                }
            }

            return string.Empty;
        }
    }
}
