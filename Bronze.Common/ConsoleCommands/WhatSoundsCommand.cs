﻿using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.ConsoleCommands
{
    public class WhatSoundsCommand : IConsoleCommand
    {
        public string CommandText => "whatsounds";

        public string QuickHelp => "whatsounds - show current ambience sounds";

        public string[] DetailedHelp => new[]
        {
            "----------",
            "whatsounds",
            "List current ambience sounds.",
            "----------"
        };

        private readonly IAudioService _audioService;
        private readonly ILogger _logger;

        public WhatSoundsCommand(
            IAudioService audioService,
            ILogger logger)
        {
            _audioService = audioService;
            _logger = logger;
        }

        public void Execute(string[] parameters)
        {
            _logger.Console("Current Ambience:");

            foreach (var tuple in _audioService.AmbienceDebug())
            {
                _logger.Console($"{tuple.Item1}: {tuple.Item2}");
            }
        }

        public string GetHint(string[] parameters)
        {
            return string.Empty;
        }
    }
}
