﻿using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.ConsoleCommands
{
    public class PerformanceSnapshotCommand : IConsoleCommand
    {
        public string CommandText => "perf";

        public string QuickHelp => "perf - print a performance snapshot";

        private readonly IPerformanceTracker _performanceTracker;
        private readonly ILogger _logger;

        public PerformanceSnapshotCommand(
            IPerformanceTracker performanceTracker,
            ILogger logger)
        {
            _performanceTracker = performanceTracker;
            _logger = logger;
        }

        public string[] DetailedHelp => new[]
        {
            "----------",
            "perf",
            "Prtint a performance snapshot to the console and log",
            "file",
            "\"perf\"",
            "----------"
        };
        
        public void Execute(string[] parameters)
        {
            _logger.Console($"FPS: {_performanceTracker.FPS}");
            _logger.Console($"Update: {_performanceTracker.UpdateTime}ms");
            _logger.Console($"Layout: {_performanceTracker.LayoutTime}ms");
            _logger.Console($"Render: {_performanceTracker.RenderTime}ms");
            _logger.Console("");
            _logger.Console($"Render");
            foreach(var tuple in _performanceTracker.GetPoorlyPerformingRendering(int.MaxValue))
            {
                _logger.Console($"{tuple.Item1}: {tuple.Item2}t");
            }
            _logger.Console("");
            _logger.Console($"Update");
            foreach (var tuple in _performanceTracker.GetPoorlyPerformingUpdates(int.MaxValue))
            {
                _logger.Console($"{tuple.Item1}: {tuple.Item2}t");
            }
            _logger.Console("");
            _logger.Console($"Settlement Calculations");
            foreach (var tuple in _performanceTracker.GetSlowSettlementCalculations(int.MaxValue))
            {
                _logger.Console($"{tuple.Item1}: {(int)(tuple.Item2 / 10000.0)}ms");
            }
        }

        public string GetHint(string[] parameters)
        {
            return string.Empty;
        }
    }
}
