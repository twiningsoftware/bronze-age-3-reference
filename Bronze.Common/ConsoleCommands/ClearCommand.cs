﻿using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.ConsoleCommands
{
    public class ClearCommand : IConsoleCommand
    {
        public string CommandText => "clear";

        public string QuickHelp => "clear - clear the console";

        public string[] DetailedHelp => new[]
        {
            "----------",
            "clear",
            "Clear all history from the console",
            "----------"
        };

        private readonly ILogger _logger; 

        public ClearCommand(ILogger logger)
        {
            _logger = logger;
        }

        public void Execute(string[] parameters)
        {
            _logger.ClearHistory();
        }

        public string GetHint(string[] parameters)
        {
            return string.Empty;
        }
    }
}
