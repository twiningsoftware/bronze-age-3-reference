﻿using Bronze.Contracts.InjectableServices;
using System;
using System.Linq;

namespace Bronze.Common.ConsoleCommands
{
    public class LogLevelCommand : IConsoleCommand
    {
        public string CommandText => "loglevel";

        public string QuickHelp => "loglevel - set the logging level";

        public string[] DetailedHelp => new[]
        {
            "----------",
            "loglevel",
            "Set the logging level. Options are:",
            string.Join(", ", _options),
            "CONSOLE, ERROR, DEBUG, ALL",
            "Default is ERROR",
            "\"loglevel ERROR\"",
            "----------"
        };

        private readonly IUserSettings _userSettings;
        private readonly ILogger _logger;
        private readonly LogLevel[] _options;

        public LogLevelCommand(
            IUserSettings userSettings,
            ILogger logger)
        {
            _userSettings = userSettings;
            _logger = logger;

            _options = Enum.GetValues(typeof(LogLevel))
                .Cast<LogLevel>()
                .ToArray();
        }

        public void Execute(string[] parameters)
        {
            if (parameters.Length > 0)
            {
                var level = _options
                    .OfType<LogLevel?>()
                    .Where(o => o.ToString().ToUpper() == parameters[0].ToUpper())
                    .FirstOrDefault();
                
                if (level != null)
                {
                    _userSettings.LogLevel = level.Value;

                    _logger.Console("Logging level set to " + level.Value);
                }
                else
                {
                    _logger.Console("Unknown logging level: " + parameters[0]);
                }
            }
            else
            {
                _logger.Console("Specify logging level.");
            }
        }

        public string GetHint(string[] parameters)
        {
            if (parameters.Length == 1)
            {
                var level = _options
                    .OfType<LogLevel?>()
                    .Select(o => o.ToString().ToUpper())
                    .Where(u => u.StartsWith(parameters[0].ToUpper()))
                    .FirstOrDefault();

                if (level != null)
                {
                    if (level != parameters[0])
                    {
                        return level.Substring(parameters[0].Length);
                    }
                    else if (parameters.Length < 2)
                    {
                        return " " + _options.First();
                    }
                }
            }

            return string.Empty;
        }
    }
}
