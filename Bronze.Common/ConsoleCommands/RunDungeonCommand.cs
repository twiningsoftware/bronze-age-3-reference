﻿using Bronze.Common.UI.Modals;
using Bronze.Contracts.Data.Dungeons;
using Bronze.Contracts.InjectableServices;
using System;
using System.Linq;

namespace Bronze.Common.ConsoleCommands
{
    /// <summary>
    /// A useful command to run dungeons at will, with the currently selected army.
    /// </summary>
    public class RunDungeonCommand : IConsoleCommand
    {
        public string CommandText => "rundungeon";

        public string QuickHelp => "rundungeon - explore a dungeon with the current army";

        private readonly IPlayerDataTracker _playerData;
        private readonly ILogger _logger;
        private readonly IDungeonDataManager _dungeonDataManager;
        private readonly IDialogManager _dialogManager;

        public RunDungeonCommand(
            IPlayerDataTracker playerData,
            ILogger logger,
            IDungeonDataManager dungeonDataManager,
            IDialogManager dialogManager)
        {
            _playerData = playerData;
            _logger = logger;
            _dungeonDataManager = dungeonDataManager;
            _dialogManager = dialogManager;
        }

        public string[] DetailedHelp => new[]
        {
            "----------",
            "rundungeon",
            "Explore a dungeon with the currently selected army and",
            "its general.",
            "\"rundungeon dungeon_id\"",
            "----------"
        };

        public void Execute(string[] parameters)
        {
            if (parameters.Length > 0)
            {
                var dungeonDefinition = _dungeonDataManager
                    .DungeonDefinitions
                    .Where(dd => dd.Id == parameters[0])
                    .FirstOrDefault();

                if (dungeonDefinition != null)
                {
                    if (_playerData.SelectedArmy != null)
                    {
                        _dialogManager.PushModal<RunDungeonDialog, RunDungeonDialogContext>(
                            new RunDungeonDialogContext
                            {
                                GeneratedDungeon = new GeneratedDungeon(new Random().Next(), dungeonDefinition),
                                Character = _playerData.SelectedArmy.General,
                                Army = _playerData.SelectedArmy,
                                DungeonLocation = _playerData.SelectedArmy.Cell
                            });
                    }
                    else
                    {
                        _logger.Console($"Must have an army selected.");
                    }
                }
                else
                {
                    _logger.Console("Unknown dungeon: " + parameters[0]);
                }
            }
            else
            {
                if (parameters.Length > 1)
                {
                    _logger.Console("Specify a bodyguard.");
                }
                else
                {
                    _logger.Console("Specify a dungeon id");
                }
            }
        }

        public string GetHint(string[] parameters)
        {
            if (parameters.Length > 0)
            {
                var dungeonDefinition = _dungeonDataManager
                    .DungeonDefinitions
                    .Where(dd => dd.Id.StartsWith(parameters[0]))
                    .FirstOrDefault();

                if (dungeonDefinition != null)
                {
                    if (dungeonDefinition.Id != parameters[0])
                    {
                        return dungeonDefinition.Id.Substring(parameters[0].Length);
                    }
                }
            }

            return string.Empty;
        }
    }
}
