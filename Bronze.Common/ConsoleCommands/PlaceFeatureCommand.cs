﻿using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System.Linq;

namespace Bronze.Common.ConsoleCommands
{
    public class PlaceFeatureCommand : IConsoleCommand
    {
        public string CommandText => "placefeature";

        public string QuickHelp => "placefeature - place a cell feature";

        private readonly IPlayerDataTracker _playerData;
        private readonly ILogger _logger;
        private readonly IGamedataTracker _gamedataTracker;

        public PlaceFeatureCommand(
            IPlayerDataTracker playerData,
            ILogger logger,
            IGamedataTracker gamedataTracker)
        {
            _playerData = playerData;
            _logger = logger;
            _gamedataTracker = gamedataTracker;
        }

        public string[] DetailedHelp => new[]
        {
            "----------",
            "placefeature",
            "Place a cell feature in the cell under the mouse cursor.",
            "\"placefeature feature_id\"",
            "----------"
        };
        
        public void Execute(string[] parameters)
        {
            if (parameters.Length > 0)
            {
                var cellFeature = _gamedataTracker.CellFeatures
                    .Where(f => f.Id.StartsWith(parameters[0]))
                    .FirstOrDefault();
                
                if (cellFeature != null)
                {
                    _playerData.HoveredCell.Feature = cellFeature;
                }
                else
                {
                    _logger.Console("Unknown cell feature: " + parameters[0]);
                }
            }
        }

        public string GetHint(string[] parameters)
        {
            if(parameters.Length > 0)
            {
                var cellFeature = _gamedataTracker.CellFeatures
                    .Where(f => f.Id.StartsWith(parameters[0]))
                    .FirstOrDefault();

                if(cellFeature != null)
                {
                    return cellFeature.Id.Substring(parameters[0].Length);
                }
            }

            return string.Empty;
        }
    }
}
