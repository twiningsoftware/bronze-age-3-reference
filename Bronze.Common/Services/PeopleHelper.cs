﻿using System.Linq;
using System.Xml.Linq;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Simulation.Events;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.Services
{
    public class PeopleHelper : IPeopleHelper, IDataLoader
    {
        private readonly INameSource _nameSource;
        private readonly IWorldManager _worldManager;
        private readonly IXmlReaderUtil _xmlReaderUtil;
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IPlayerDataTracker _playerDataTracker;
        private readonly INotificationsSystem _notificationsSystem;
        private readonly IGenericNotificationBuilder _genericNotificationBuilder;
        private readonly ISimulationEventBus _simulationEventBus;
        
        public string ElementName => "people_helper";

        private NotablePersonSkill[] _injurySkills;
        private NotablePersonSkill[] _infectionSkills;
        private NotablePersonSkill[] _diseaseSkills;

        public PeopleHelper(
            INameSource nameSource,
            IWorldManager worldManager,
            IXmlReaderUtil xmlReaderUtil,
            IGamedataTracker gamedataTracker,
            IPlayerDataTracker playerDataTracker,
            INotificationsSystem notificationsSystem,
            IGenericNotificationBuilder genericNotificationBuilder,
            ISimulationEventBus simulationEventBus)
        {
            _nameSource = nameSource;
            _worldManager = worldManager;
            _xmlReaderUtil = xmlReaderUtil;
            _gamedataTracker = gamedataTracker;
            _playerDataTracker = playerDataTracker;
            _notificationsSystem = notificationsSystem;
            _genericNotificationBuilder = genericNotificationBuilder;
            _simulationEventBus = simulationEventBus;
        }

        public void Load(XElement element)
        {
            _injurySkills = element.Elements("injury_skill")
                .Select(e => _xmlReaderUtil.ObjectReferenceLookup(
                    e,
                    "skill",
                    _gamedataTracker.Skills,
                    s => s.Id))
                .ToArray();

            _infectionSkills = element.Elements("infection_skill")
                .Select(e => _xmlReaderUtil.ObjectReferenceLookup(
                    e,
                    "skill",
                    _gamedataTracker.Skills,
                    s => s.Id))
                .ToArray();

            _diseaseSkills = element.Elements("disease_skill")
                .Select(e => _xmlReaderUtil.ObjectReferenceLookup(
                    e,
                    "skill",
                    _gamedataTracker.Skills,
                    s => s.Id))
                .ToArray();
        }

        public void PostLoad(XElement element)
        {
        }

        public double GetPregnancyChance(
            NotablePerson father, 
            NotablePerson mother,
            double forMonth)
        {
            var fatherFertility = (1 + father.GetMinorStat("Fertility")) * father.Race.BasePregnancyChance;

            var motherFertility = (1 + mother.GetMinorStat("Fertility")) * mother.Race.BasePregnancyChance;
            
            return (fatherFertility + motherFertility) / 2;
        }

        public void Kill(NotablePerson person)
        {
            if (_playerDataTracker.PlayerTribe == person.Owner)
            {
                var wasOld = person.AgeBracket == NotablePersonAgeBracket.Old;
                var wasInjured = person.Skills.Any(sp => _injurySkills.Contains(sp.Skill));
                var wasInfected = person.Skills.Any(sp => _infectionSkills.Contains(sp.Skill));
                var wasDiseased = person.Skills.Any(sp => _diseaseSkills.Contains(sp.Skill));

                var age = _worldManager.Month - person.BirthDate;

                var titleDescription = string.Empty;
                if (person.Owner.Ruler == person)
                {
                    titleDescription = $"\nThe people and nobility are devastated by the loss of their ruler.";
                }
                else if (person.Owner.Heir == person)
                {
                    titleDescription = $"\nThe heir of {person.Owner.Name} is dead.";
                }

                var assignmentDescription = string.Empty;
                if (person.GovernorOf != null)
                {
                    assignmentDescription = $"\n{person.GovernorOf.Name} is now without a governor.";
                }
                if (person.GeneralOf != null)
                {
                    assignmentDescription = $"\n{person.GeneralOf.Name} is now without a general.";
                }

                var summary = $"{person.Name} has died";
                var reasonDescription = string.Empty;
                if(wasInfected)
                {
                    summary += " from infection.";
                    reasonDescription += " from infection";
                }
                else if(wasInjured)
                {
                    summary += " from their injuries.";
                    reasonDescription += " from their injuries";
                }
                else if(wasDiseased)
                {
                    summary += " from disease.";
                    reasonDescription += " from disease";
                }
                else if(wasOld)
                {
                    summary += " of old age.";
                    reasonDescription += " of old age";
                }
                else
                {
                    summary += ".";
                }

                _notificationsSystem.AddNotification(_genericNotificationBuilder
                    .BuildPersonNotification(
                    "ui_notification_death",
                    $"{person.Name} is Dead",
                    summary,
                    $"{person.Name} is Dead",
                    $"The people of {person.Owner.Name} are in mourning, as {person.Name} has died{reasonDescription} at the age of {(int)(age / 12)}.{titleDescription}{assignmentDescription}",
                    person,
                    null));
            }

            person.IsDead = true;

            _simulationEventBus.SendEvent(new NotablePersonDiedSimulationEvent(person));

            person.ClearAssignments();
        }

        public NotablePersonSkillPlacement IncrementOrAdd(
            NotablePerson person, 
            NotablePersonSkill skill)
        {
            var skillPlacement = person.Skills
                .Where(sp => sp.Skill == skill)
                .FirstOrDefault();

            if (skillPlacement == null)
            {
                skillPlacement = new NotablePersonSkillPlacement
                {
                    Skill = skill,
                    Level = skill.Levels.First()
                };

                person.Skills.Add(skillPlacement);

                return skillPlacement;
            }
            else if (skillPlacement.CanIncrement())
            {
                skillPlacement.Increment();

                return skillPlacement;
            }

            return null;
        }
    }
}
