﻿using Autofac;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.Services
{
    public class NotificationsSystem : INotificationsSystem, IWorldStateService, ISubSimulator
    {
        private List<Notification> _notifications;

        private readonly IWorldManager _worldManager;
        private readonly INotificationHandler[] _notificationHandlers;
        private readonly IAudioService _audioService;

        public NotificationsSystem(
            IWorldManager worldManager,
            IEnumerable<INotificationHandler> notificationHandlers,
            IAudioService audioService)
        {
            _notifications = new List<Notification>();
            _worldManager = worldManager;
            _notificationHandlers = notificationHandlers.ToArray();
            _audioService = audioService;
        }

        public void Clear()
        {
            _notifications.Clear();
        }

        public void Initialize(WorldState worldState)
        {
        }
        
        public void LoadCustomData(SerializedObject worldRoot)
        {
            var systemRoot = worldRoot.GetOptionalChild("notification_system");

            if (systemRoot != null)
            {
                foreach (var notificationRoot in systemRoot.GetChildren("notification"))
                {
                    var notification = new Notification
                    {
                        ExpireDate = notificationRoot.GetDouble("expire_date"),
                        SoundEffect = notificationRoot.GetString("sound_effect"),
                        AutoOpen = notificationRoot.GetBool("auto_open"),
                        QuickDismissable = notificationRoot.GetBool("quick_dismiss"),
                        DismissOnOpen = notificationRoot.GetBool("dismiss_on_open"),
                        SummaryTitle = notificationRoot.GetString("summary_title"),
                        SummaryBody = notificationRoot.GetString("summary_body"),
                        Handler = notificationRoot.GetString("handler"),
                        Icon = notificationRoot.GetString("icon")
                    };

                    var dataRoot = notificationRoot.GetChild("data");
                    foreach (var child in dataRoot.Children)
                    {
                        notification.Data.Add(child.GetString("key"), child.GetOptionalString("value"));
                    }

                    var serializedDataRoot = notificationRoot.GetOptionalChild("serialized_data");

                    if (serializedDataRoot == null)
                    {
                        foreach (var child in serializedDataRoot.Children)
                        {
                            notification.SerializedData.Add(child.GetString("key"), child.Children.First());
                        }
                    }
                    
                    _notifications.Add(notification);
                }
            }
        }

        public void SaveCustomData(SerializedObject worldRoot)
        {
            var systemRoot = worldRoot.CreateChild("notification_system");

            foreach(var notification in _notifications)
            {
                var notificationRoot = systemRoot.CreateChild("notification");

                notificationRoot.Set("auto_open", notification.AutoOpen);
                notificationRoot.Set("expire_date", notification.ExpireDate);
                notificationRoot.Set("handler", notification.Handler);
                notificationRoot.Set("icon", notification.Icon);
                notificationRoot.Set("quick_dismiss", notification.QuickDismissable);
                notificationRoot.Set("sound_effect", notification.SoundEffect);
                notificationRoot.Set("summary_title", notification.SummaryTitle);
                notificationRoot.Set("summary_body", notification.SummaryBody);
                notificationRoot.Set("dismiss_on_open", notification.DismissOnOpen);
                
                var dataRoot = notificationRoot.CreateChild("data");
                foreach(var kvp in notification.Data)
                {
                    var child = dataRoot.CreateChild("kvp");
                    child.Set("key", kvp.Key);
                    child.Set("value", kvp.Value);
                }

                var serializedDataRoot = notificationRoot.CreateChild("serialized_data");
                foreach (var kvp in notification.SerializedData)
                {
                    var child = dataRoot.CreateChild("kvp");
                    child.Set("key", kvp.Key);
                    child.AddChild(kvp.Value);
                }
            }
        }

        public void StepSimulation(double elapsedMonths)
        {
            foreach(var notification in _notifications)
            {
                if(notification.ExpireDate > 0 && notification.ExpireDate <= _worldManager.Month)
                {
                    notification.IsDismissed = true;
                }

                if(notification.IsDismissed && !notification.WasOpened)
                {
                    HandleIgnoredNotification(notification);
                }
            }
            
            _notifications.RemoveAll(n => n.IsDismissed);
        }

        public IEnumerable<Notification> GetActiveNotifications()
        {
            return _notifications;
        }

        public void AddNotification(Notification notification)
        {
            _notifications.Add(notification);

            if (!string.IsNullOrWhiteSpace(notification.SoundEffect))
            {
                _audioService.PlaySound(notification.SoundEffect);
            }

            if(notification.AutoOpen)
            {
                ShowNotification(notification);
            }
        }

        public void ShowNotification(Notification notification)
        {
            var handler = _notificationHandlers
                .Where(h => h.Name == notification.Handler)
                .FirstOrDefault();

            if(handler == null)
            {
                throw new Exception($"No notification handler named {notification.Handler} has been registered.");
            }

            notification.WasOpened = true;

            if (notification.DismissOnOpen)
            {
                notification.IsDismissed = true;
            }

            handler.Handle(notification);
        }

        private void HandleIgnoredNotification(Notification notification)
        {
            var handler = _notificationHandlers
                .Where(h => h.Name == notification.Handler)
                .FirstOrDefault();

            if (handler == null)
            {
                throw new Exception($"No notification handler named {notification.Handler} has been registered.");
            }

            handler.HandleIgnored(notification);
        }
    }
}
