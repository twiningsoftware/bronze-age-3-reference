﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bronze.Contracts.Data.AI;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.Services
{
    public class AiSpy : IAiSpy
    {
        private readonly ILogger _logger;
        private readonly List<Tribe> _activeTribes;

        public AiSpy(ILogger logger)
        {
            _logger = logger;
            _activeTribes = new List<Tribe>();
        }
        
        public ISettlementAiSpy StartSettlement(
            string settlementRole,
            Settlement settlement, 
            Dictionary<Item, double> idealNetProduction, 
            Dictionary<Caste, int> availablePopByCaste, 
            Dictionary<MovementType, int> tradeCapacity)
        {
            if(_activeTribes.Contains(settlement.Owner))
            {
                Log($"Starting {settlementRole} {settlement.Name} ({settlement.Owner.Name}, {settlement.Race.Name}");
                Log($"    Net Production: " + string.Join(", ", idealNetProduction.Select(kvp => $"{kvp.Key.Name}: {kvp.Value}")));
                Log($"    Available Pops: " + string.Join(", ", availablePopByCaste.Select(kvp => $"{kvp.Key.Id}: {kvp.Value}")));
                Log($"    Idle Traders: " + string.Join(", ", tradeCapacity.Select(kvp => $"{kvp.Key.Id}: {kvp.Value}")));

                return new SettlementAiSpy(this, settlement);
            }

            return new DummySettlementAiSpy();
        }

        public void Log(string message)
        {
            _logger.Console(message);
        }

        public void ToggleFor(Tribe tribe)
        {
            if(_activeTribes.Contains(tribe))
            {
                _activeTribes.Remove(tribe);
                Log("AiSpy off for " + tribe.Name);
            }
            else
            {
                _activeTribes.Add(tribe);
                Log("AiSpy on for " + tribe.Name);
            }
        }

        private class SettlementAiSpy : ISettlementAiSpy
        {
            private AiSpy _aiSpy;
            private Settlement _settlement;

            public SettlementAiSpy(AiSpy aiSpy, Settlement settlement)
            {
                _aiSpy = aiSpy;
                _settlement = settlement;
            }

            public void Log(string text)
            {
                _aiSpy.Log($"    {text}");
            }

            public void Dispose()
            {
                _aiSpy.Log($"End for {_settlement.Name}");
            }

            public IChoiceAiSpy StartChoice(string choiceType)
            {
                _aiSpy.Log($"    Start Choice {choiceType}");
                return new ChoiceAiSpy(_aiSpy, choiceType);
            }
        }

        private class ChoiceAiSpy : IChoiceAiSpy
        {
            private AiSpy _aiSpy;
            private string _choiceType;

            public ChoiceAiSpy(AiSpy aiSpy, string choiceType)
            {
                _aiSpy = aiSpy;
                _choiceType = choiceType;
            }

            public void Dispose()
            {
                _aiSpy.Log($"    End Choice {_choiceType}");
            }

            public void Log(string message)
            {
                _aiSpy.Log($"        {message}");
            }

            public void Options(string name, IEnumerable<ICellDevelopmentType> developmentOptions)
            {
                if(!string.IsNullOrWhiteSpace(name))
                {
                    _aiSpy.Log($"        Options for {name}: {string.Join(", ", developmentOptions.Select(d => d.Id))}");
                }
                else
                {
                    _aiSpy.Log($"        Options: {string.Join(", ", developmentOptions.Select(d => d.Id))}");
                }
            }

            public void Options(Tuple<IUnitType, UnitEquipmentSet>[] unitOptions)
            {
                _aiSpy.Log($"        Options:  {string.Join(", ", unitOptions.Select(d => d.Item1.Id + ": " + d.Item2.Level))}");
            }

            public void Options(string name, IEnumerable<CityPrefab> prefabOptions)
            {
                if (!string.IsNullOrWhiteSpace(name))
                {
                    _aiSpy.Log($"        Options for {name}: {string.Join(", ", prefabOptions.Select(d => d.Id))}");
                }
                else
                {
                    _aiSpy.Log($"        Options: {string.Join(", ", prefabOptions.Select(d => d.Id))}");
                }
            }

            public void Pick(ICellDevelopmentType cellDevelopment, Recipie recipie)
            {
                _aiSpy.Log($"        Picked {cellDevelopment.Id} with recipie {recipie.Id}");
            }

            public void Pick(ICellDevelopmentType cellDevelopment)
            {
                _aiSpy.Log($"        Picked {cellDevelopment.Id}");
            }

            public void Pick(IUnitType unitType, UnitEquipmentSet unitEquipment)
            {
                _aiSpy.Log($"        Picked {unitType.Id}: {unitEquipment.Level}");
            }

            public void Pick(CityPrefab[] prefabOptions)
            {
                _aiSpy.Log($"        Queued {string.Join(", ", prefabOptions.Select(d => d.Id))}");
            }

            public void PickForClearing(ICellDevelopmentType clearanceDevelopment, ICellDevelopmentType developmentGoal)
            {
                _aiSpy.Log($"        Picked {clearanceDevelopment.Id} to clear cell to place {developmentGoal.Id}");
            }
        }

        private class DummySettlementAiSpy : ISettlementAiSpy
        {
            public void Dispose()
            {
            }

            public void Log(string text)
            {
            }

            public IChoiceAiSpy StartChoice(string choiceType)
            {
                return new DummyChoiceAiSpy();
            }
        }

        private class DummyChoiceAiSpy : IChoiceAiSpy
        {
            public void Dispose()
            {
            }

            public void Log(string message)
            {
            }

            public void Options(string name, IEnumerable<ICellDevelopmentType> developmentOptions)
            {
            }

            public void Options(Tuple<IUnitType, UnitEquipmentSet>[] unitOptions)
            {
            }

            public void Options(string name, IEnumerable<CityPrefab> prefabOptions)
            {
            }

            public void Pick(ICellDevelopmentType cellDevelopment, Recipie recipie)
            {
            }

            public void Pick(ICellDevelopmentType cellDevelopment)
            {
            }

            public void Pick(IUnitType unitType, UnitEquipmentSet unitEquipment)
            {
            }

            public void Pick(CityPrefab[] prefabOptions)
            {
            }

            public void PickForClearing(ICellDevelopmentType clearanceDevelopment, ICellDevelopmentType developmentGoal)
            {
            }
        }
    }
}
