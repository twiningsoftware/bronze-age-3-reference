﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.Simulation.Events;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.Services
{
    public class UnitHelper : IUnitHelper
    {
        private readonly IPlayerDataTracker _playerData;
        private readonly INameSource _nameSource;
        private readonly IDialogManager _dialogManager;
        private readonly INotificationsSystem _notificationsSystem;
        private readonly IArmyNotificationBuilder _armyNotificationBuilder;
        private readonly IWorldManager _worldManager;
        private readonly ISimulationEventBus _simulationEventBus;

        public UnitHelper(
            IPlayerDataTracker playerData,
            INameSource nameSource,
            IDialogManager dialogManager,
            INotificationsSystem notificationsSystem,
            IArmyNotificationBuilder armyNotificationBuilder,
            IWorldManager worldManager,
            ISimulationEventBus simulationEventBus)
        {
            _playerData = playerData;
            _nameSource = nameSource;
            _dialogManager = dialogManager;
            _notificationsSystem = notificationsSystem;
            _armyNotificationBuilder = armyNotificationBuilder;
            _worldManager = worldManager;
            _simulationEventBus = simulationEventBus;
        }
        
        public IUnit RecruitUnit(
            IUnitType unitType, 
            UnitEquipmentSet equipmentSet, 
            Settlement sourceSettlement)
        {
            if (sourceSettlement != null)
            {
                sourceSettlement.RecruitmentSlots[unitType.RecruitmentCategory] -= 1;

                var stockpiles = sourceSettlement.EconomicActors
                    .OfType<IArmoryStorageProvider>()
                    .ToArray();

                foreach (var iq in equipmentSet.RecruitmentCost)
                {
                    var remaining = iq.Quantity;

                    foreach (var stockpile in stockpiles)
                    {
                        if (remaining > 0)
                        {
                            var toTake = Math.Min(remaining, stockpile.Inventory.QuantityOf(iq.Item));

                            stockpile.Inventory.Remove(iq.Item, toTake);
                            remaining -= toTake;
                        }
                    }
                }
            }

            return unitType.CreateUnit(equipmentSet);
        }

        public void ChangeEquipment(IUnit unit, UnitEquipmentSet equipmentSet, Settlement sourceSettlement)
        {
            var stockpiles = sourceSettlement.EconomicActors
                .OfType<IArmoryStorageProvider>()
                .ToArray();

            foreach (var iq in equipmentSet.RecruitmentCost)
            {
                var remaining = iq.Quantity;

                foreach (var stockpile in stockpiles)
                {
                    if (remaining > 0)
                    {
                        var toTake = Math.Min(remaining, stockpile.Inventory.QuantityOf(iq.Item));

                        stockpile.Inventory.Remove(iq.Item, toTake);
                        remaining -= toTake;
                    }
                }
            }

            unit.Equipment = equipmentSet;
        }

        public Army CreateArmy(Tribe owner, Cell position)
        {
            var army = new Army(owner, position, _worldManager)
            {
                Name = _nameSource.MakeArmyName(owner.Race)
            };
            owner.Armies.Add(army);

            return army;
        }

        public void RemoveUnit(Army army, IUnit unit, bool removedViolently)
        {
            if(removedViolently)
            {
                _simulationEventBus.SendEvent(new UnitDiedSimulationEvent(army, unit, unit.LastDrawPosition));
            }

            army.Units.Remove(unit);

            if(!army.Units.Any())
            {
                // Notification first, before data is changed.
                if (army.Owner == _playerData.PlayerTribe)
                {
                    _notificationsSystem.AddNotification(_armyNotificationBuilder.BuildArmyLostNotification(
                        army,
                        army.Cell.Position));
                }

                RemoveArmy(army);
            }
        }

        public void RemoveArmy(Army army)
        {
            army.Cell.WorldActors.RemoveAll(a => a == army);
            army.Cell.Region.WorldActors.RemoveAll(a => a == army);
            army.Owner.Armies.Remove(army);
            if (army.General != null)
            {
                army.General.ClearAssignments();
            }

            if (_playerData.SelectedArmy == army)
            {
                _playerData.SelectedArmy = null;
            }
        }
        
        public void UnitAttritionedToDeath(Army army, IUnit unit)
        {
            if (army.Owner == _playerData.PlayerTribe)
            {
                _notificationsSystem.AddNotification(_armyNotificationBuilder.BuildAttritionDeathNotification(
                    army.Name,
                    unit,
                    army.Cell.Position));
            }

            RemoveUnit(army, unit, removedViolently: true);
        }
        
        public void Disband(Army army, IUnit unit)
        {
            var settlement = army.Cell.Region.Settlement;

            if(settlement != null && settlement.Owner == army.Owner)
            {
                settlement.RecruitmentSlots[unit.RecruitmentCategory] = Math.Min(
                    settlement.RecruitmentSlots[unit.RecruitmentCategory] + 1,
                    settlement.RecruitmentSlotsMax[unit.RecruitmentCategory]);
            }
            
            RemoveUnit(army, unit, removedViolently: false);
            unit.OnDisbanded();
        }
    }
}
