﻿using Bronze.Common.Data.World.Units;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.UI;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.Services
{
    public class ArmySkirmishInterrupter : ISubSimulator
    {
        private readonly IWorldManager _worldManager;
        private readonly IBattleHelper _battleHelper;
        private readonly IPlayerDataTracker _playerData;
        private readonly INotificationsSystem _notificationsSystem;

        private bool _playerWasSkirmishing;
        private bool _playerWasSieging;
        private bool _playerWasSieged;

        public ArmySkirmishInterrupter(
            IWorldManager worldManager,
            IBattleHelper battleHelper,
            IPlayerDataTracker playerData,
            INotificationsSystem notificationsSystem)
        {
            _worldManager = worldManager;
            _battleHelper = battleHelper;
            _playerData = playerData;
            _notificationsSystem = notificationsSystem;
        }
        
        public void StepSimulation(double elapsedMonths)
        {
            if(elapsedMonths <= 0)
            {
                return;
            }

            var playerIsSkirmishing = false;
            var playerIsSieging = false;
            var playerIsSieged = false;

            var allArmies = _worldManager.Tribes
                .SelectMany(t => t.Armies)
                .ToArray();
            foreach(var tribe in _worldManager.Tribes)
            {
                if (tribe == _playerData.PlayerTribe)
                {
                    playerIsSieged = tribe.Settlements.Any(s => s.IsUnderSiege);
                }

                foreach (var army in tribe.Armies.ToArray())
                {
                    var skirmishingWith = army.Cell.Yield()
                        .Concat(army.Cell.Neighbors)
                        .SelectMany(c => c.WorldActors.OfType<Army>())
                        .Where(a => a.Units.Any())
                        .Where(a => army.Owner.IsHostileTo(a.Owner))
                        .Where(a => army.CanPath(a.Cell))
                        .Where(a => !((a.CurrentOrder is ArmyCampOrder aco && aco.IsGarrisoned) || (army.CurrentOrder is ArmyCampOrder aco2 && aco2.IsGarrisoned)))
                        .Where(a => (a.Position - army.Position).Length() <= Constants.DIST_SKIRMISH_MAX)
                        .OrderBy(a => (a.Position - army.Position).Length())
                        .ToArray();

                    army.SkirmishingWith.Clear();
                    army.SkirmishingWith.AddRange(skirmishingWith);

                    if (tribe == _playerData.PlayerTribe)
                    {
                        playerIsSkirmishing = playerIsSkirmishing
                            || army.SkirmishingWith.Any();

                        playerIsSieging = playerIsSieging
                            || (army.CurrentOrder is ArmyInteractOrder interactOrder && interactOrder.SiegedSettlement != null);
                    }

                    if (skirmishingWith.Any() && !army.IsLocked)
                    {
                        var nearest = skirmishingWith.First();

                        if (!(army.CurrentOrder is ArmySkirmishOrder) && army.Stance != ArmyStance.Defensive)
                        {
                            army.CurrentOrder = new ArmySkirmishOrder(army, nearest, _worldManager, army.CurrentOrder, _battleHelper);
                        }
                        else if (army.CurrentOrder is ArmySkirmishOrder skirmishOrder && skirmishOrder.TargetArmy != nearest)
                        {
                            army.CurrentOrder = new ArmySkirmishOrder(army, nearest, _worldManager, skirmishOrder.FallbackOrder, _battleHelper);
                        }

                        if((nearest.Position - army.Position).Length() <= Constants.DIST_COMBAT)
                        {
                            _battleHelper.RunBattle(army);
                        }
                    }
                }
            }

            if(_playerWasSkirmishing != playerIsSkirmishing)
            {
                _playerWasSkirmishing = playerIsSkirmishing;

                var engagedNotification = _notificationsSystem.GetActiveNotifications()
                    .Where(n => n.Handler == typeof(ArmyEngagedNotificationHandler).Name)
                    .FirstOrDefault();
                
                if (playerIsSkirmishing && engagedNotification == null)
                {
                    _notificationsSystem.AddNotification(
                        new Notification
                        {
                            AutoOpen = false,
                            ExpireDate = -1,
                            Handler = typeof(ArmyEngagedNotificationHandler).Name,
                            Icon = "ui_notification_battle",
                            SoundEffect = UiSounds.NotificationMajor,
                            QuickDismissable = false,
                            DismissOnOpen = false,
                            SummaryTitle = "Battle!",
                            SummaryBody = "Our armies are engaged in battle.",
                            Data = new Dictionary<string, string>()
                        });
                }
                else if (!playerIsSkirmishing && engagedNotification != null)
                {
                    engagedNotification.IsDismissed = true;
                }
            }

            if (_playerWasSieging != playerIsSieging)
            {
                _playerWasSieging = playerIsSieging;

                var siegingNotification = _notificationsSystem.GetActiveNotifications()
                    .Where(n => n.Handler == typeof(SiegingNotificationHandler).Name)
                    .FirstOrDefault();

                if (playerIsSieging && siegingNotification == null)
                {
                    _notificationsSystem.AddNotification(
                        new Notification
                        {
                            AutoOpen = false,
                            ExpireDate = -1,
                            Handler = typeof(SiegingNotificationHandler).Name,
                            Icon = "ui_notification_sieging",
                            SoundEffect = UiSounds.NotificationMinor,
                            QuickDismissable = false,
                            DismissOnOpen = false,
                            SummaryTitle = "Sieging",
                            SummaryBody = "Our armies have settlements under siege.",
                            Data = new Dictionary<string, string>()
                        });
                }
                else if (!playerIsSieging && siegingNotification != null)
                {
                    siegingNotification.IsDismissed = true;
                }
            }

            if (_playerWasSieged != playerIsSieged)
            {
                _playerWasSieged = playerIsSieged;

                var siegedNotification = _notificationsSystem.GetActiveNotifications()
                    .Where(n => n.Handler == typeof(SiegedNotificationHandler).Name)
                    .FirstOrDefault();

                if (playerIsSieged && siegedNotification == null)
                {
                    _notificationsSystem.AddNotification(
                        new Notification
                        {
                            AutoOpen = false,
                            ExpireDate = -1,
                            Handler = typeof(SiegedNotificationHandler).Name,
                            Icon = "ui_notification_sieged",
                            SoundEffect = UiSounds.NotificationMajor,
                            QuickDismissable = false,
                            DismissOnOpen = false,
                            SummaryTitle = "Sieged",
                            SummaryBody = "Our settlements are under siege!",
                            Data = new Dictionary<string, string>()
                        });
                }
                else if (!playerIsSieged && siegedNotification != null)
                {
                    siegedNotification.IsDismissed = true;
                }
            }
        }
    }
}
