﻿using Bronze.Common.Data.World.CellDevelopments;
using Bronze.Common.InjectableServices;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Bronze.Common.Services
{
    public class IrrigationCalculator : IIrrigationCalculator, ISubSimulator, IWorldStateService
    {
        private readonly IPerformanceTracker _performanceTracker;
        private readonly List<IrrigationDevelopment> _developments;
        private readonly Dictionary<IrrigationDevelopment, List<Trait>> _carriedTraits;
        private readonly List<IrrigationDevelopment> _queue;
        private readonly List<Trait> _traitsToTransmit;
        private Trait _currentTrait;

        public IrrigationCalculator(IPerformanceTracker performanceTracker)
        {
            _performanceTracker = performanceTracker;
            _developments = new List<IrrigationDevelopment>();
            _carriedTraits = new Dictionary<IrrigationDevelopment, List<Trait>>();
            _queue = new List<IrrigationDevelopment>();
            _traitsToTransmit = new List<Trait>();
        }

        public void Clear()
        {
            _developments.Clear();
            _carriedTraits.Clear();
            _queue.Clear();
            _traitsToTransmit.Clear();
            _currentTrait = null;
        }

        public void Deregister(IrrigationDevelopment development)
        {
            var index = _developments.IndexOf(development);

            if(index >= 0)
            {
                _developments.RemoveAt(index);
                _carriedTraits.Remove(development);
            }
        }

        public void Initialize(WorldState worldState)
        {
        }

        public void LoadCustomData(SerializedObject worldRoot)
        {
        }

        public void Register(IrrigationDevelopment development)
        {
            _developments.Add(development);
            _carriedTraits.Add(development, new List<Trait>());
        }

        public void SaveCustomData(SerializedObject worldRoot)
        {
        }

        public void StepSimulation(double elapsedMonths)
        {
            var stopwatch = Stopwatch.StartNew();

            if (!_queue.Any() && !_traitsToTransmit.Any())
            {
                _traitsToTransmit.Clear();
                foreach (var development in _developments)
                {
                    _carriedTraits[development].Clear();

                    foreach (var trait in development.CarriedTraits)
                    {
                        if (!_traitsToTransmit.Contains(trait))
                        {
                            _traitsToTransmit.Add(trait);
                        }
                    }
                }
            }

            while ((_traitsToTransmit.Any() || _queue.Any()) && stopwatch.ElapsedTicks < 1000)
            {
                if (!_queue.Any())
                {
                    _queue.AddRange(_developments);

                    _currentTrait = _traitsToTransmit.First();
                    _traitsToTransmit.RemoveAt(0);
                }

                while (_queue.Any() && stopwatch.ElapsedTicks < 1000)
                {
                    var withSources = _queue
                        .Where(d => !d.UnderConstruction
                                && d.Cell.OrthoNeighbors
                                    .Where(n => (!(n.Development is IrrigationDevelopment) && n.Traits.Contains(_currentTrait))
                                        || (n.Development != null && n.Development is IrrigationDevelopment && _carriedTraits[n.Development as IrrigationDevelopment].Contains(_currentTrait)))
                                    .Any())
                        .ToArray();

                    foreach (var source in withSources)
                    {
                        if (source.CarriedTraits.Contains(_currentTrait)
                            && _carriedTraits.ContainsKey(source))
                        {
                            _carriedTraits[source].Add(_currentTrait);
                        }
                        _queue.Remove(source);
                    }

                    if (!withSources.Any())
                    {
                        _queue.Clear();
                    }
                }
            }

            if (!_queue.Any() && !_traitsToTransmit.Any())
            {
                foreach (var development in _developments)
                {
                    development.IsFull = _carriedTraits[development].Contains(development.SourceTrait);
                }
            }

            stopwatch.Stop();
        }
    }
}
