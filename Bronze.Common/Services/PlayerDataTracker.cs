﻿using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using System.Linq;
using System.Numerics;

namespace Bronze.Common.Services
{
    public class PlayerDataTracker : IPlayerDataTracker, IWorldStateService
    {
        public int CurrentPlane { get; set; }
        public Vector2 ScaledViewCenter { get; private set; }
        public Cell HoveredCell { get; set; }
        public Cell SelectedCell { get; set; }
        public Tribe PlayerTribe { get; set; }
        public Army SelectedArmy { get; set; }
        public Army HoveredArmy { get; set; }
        public Tile HoveredTile { get; set; }
        public IStructure SelectedStructure { get; set; }
        public Region ActiveRegion { get; set; }
        public Settlement ActiveSettlement { get; set; }
        
        private IMouseMode _mouseMode;
        private Vector2 _viewCenter;
        private ViewMode _viewMode;
        
        public IMouseMode MouseMode
        {
            get => _mouseMode;
            set
            {
                var oldMode = _mouseMode;
                _mouseMode = value;

                if (value == null)
                {
                    oldMode?.OnCancel();
                }
            }
        }

        public Vector2 ViewCenter
        {
            get => _viewCenter;
            set
            {
                _viewCenter = value;
                ScaledViewCenter = value * Util.ViewModeToScale(ViewMode);
            }
        }

        public ViewMode ViewMode
        {
            get => _viewMode;
            set
            {
                _viewMode = value;
                ScaledViewCenter = ViewCenter * Util.ViewModeToScale(value);
            }
        }

        public void Clear()
        {
            ViewCenter = Vector2.Zero;
            CurrentPlane = 0;
            PlayerTribe = null;
            SelectedCell = null;
            HoveredCell = null;
            ActiveRegion = null;
            ActiveSettlement = null;
            SelectedArmy = null;
            ViewMode = ViewMode.Detailed;
        }

        public void Initialize(WorldState worldState)
        {
            PlayerTribe = worldState.PlayerTribe;
        }

        public void LookAt(CellPosition cellPosition)
        {
            if(ViewMode == ViewMode.Settlement)
            {
                ViewMode = ViewMode.Detailed;
            }

            ActiveSettlement = null;
            CurrentPlane = cellPosition.Plane;
            ViewCenter = cellPosition.ToVector() * Constants.CELL_SIZE_PIXELS;
        }

        public void LookAt(TilePosition tilePosition)
        {
            ViewMode = ViewMode.Settlement;
            ActiveSettlement = tilePosition.Cell.Region.Settlement;
            ViewCenter = tilePosition.ToVector() * Constants.TILE_SIZE_PIXELS;
        }

        public void ValidateSelectedUnits()
        {
        }

        public void SaveCustomData(SerializedObject root)
        {
            var playerDataRoot = root.CreateChild("player_data");
            playerDataRoot.Set("current_plane", CurrentPlane);
            playerDataRoot.Set("view_x", ViewCenter.X);
            playerDataRoot.Set("view_y", ViewCenter.Y);
            playerDataRoot.Set("active_settlement_id", ActiveSettlement?.Id);
        }

        public void LoadCustomData(SerializedObject root)
        {
            var playerDataRoot = root.GetChild("player_data");
            CurrentPlane = playerDataRoot.GetInt("current_plane");
            ViewCenter = new Vector2(
                (float)playerDataRoot.GetDouble("view_x"),
                (float)playerDataRoot.GetDouble("view_y"));

            var activeSettlementId = playerDataRoot.GetOptionalString("active_settlement_id");

            ActiveSettlement = PlayerTribe.Settlements
                .Where(s => s.Id == activeSettlementId)
                .FirstOrDefault();

            if(ActiveSettlement != null)
            {
                ViewMode = ViewMode.Settlement;
            }
        }
    }
}
