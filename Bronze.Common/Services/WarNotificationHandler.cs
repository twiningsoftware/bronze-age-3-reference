﻿using Bronze.Common.UI.Modals;
using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.Services
{
    public class WarNotificationHandler : INotificationHandler
    {
        private readonly IDialogManager _dialogManager;

        public WarNotificationHandler(IDialogManager dialogManager)
        {
            _dialogManager = dialogManager;
        }

        public string Name => typeof(WarNotificationHandler).Name;

        public void Handle(Notification notification)
        {
            _dialogManager.PushModal<WarStatusModal>();
        }

        public void HandleIgnored(Notification notification)
        {
        }
    }
}
