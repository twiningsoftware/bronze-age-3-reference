﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using Bronze.Common.Data.World;
using Bronze.Common.Data.World.Units;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.Delegates;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.Services
{
    public class SettlementCalculator : ISettlementCalculator
    {
        private static readonly object LOCK = new object();

        private readonly IGameInterface _gameInterface;
        private readonly IGamedataTracker _gamedataTracker;
        private readonly ISettlementSubcalcuator[] _subCalculators;
        private readonly IPerformanceTracker _performanceTracker;
        private readonly ISettlementLogiCalculator _settlementLogiCalculator;
        private readonly ILogger _logger;
        private Thread _calculationThread = null;

        private List<CalculationResults> _calculationQueue;
        private Queue<CalculationResults> _calculationResults;

        public SettlementCalculator(
            IGameInterface gameInterface, 
            IGamedataTracker gamedataTracker,
            IEnumerable<ISettlementSubcalcuator> subCalculators,
            IPerformanceTracker performanceTracker,
            ISettlementLogiCalculator settlementLogiCalculator,
            ILogger logger)
        {
            _gameInterface = gameInterface;
            _gamedataTracker = gamedataTracker;
            _subCalculators = subCalculators.ToArray();
            _performanceTracker = performanceTracker;
            _settlementLogiCalculator = settlementLogiCalculator;
            _logger = logger;

            _calculationQueue = new List<CalculationResults>();
            _calculationResults = new Queue<CalculationResults>();
        }
        
        public void QueueCalculation(Settlement settlement)
        {
            lock(LOCK)
            {
                if (!_calculationQueue.Any(cr => cr.Settlement == settlement))
                {
                    _calculationQueue.Add(new CalculationResults(settlement));
                }
            }
        }

        public void StartCalculations()
        {
            if(_calculationThread == null
                || _calculationThread.ThreadState != System.Threading.ThreadState.Running)
            {
                Queue<CalculationResults> queue = null;
                lock (LOCK)
                {
                    if (_calculationQueue.Any())
                    {
                        queue = new Queue<CalculationResults>();
                        queue.Enqueue(_calculationQueue.First());
                        _calculationQueue.RemoveAt(0);

                        foreach (var village in _calculationQueue.Where(x => !x.Settlement.Districts.Any()).ToArray())
                        {
                            queue.Enqueue(village);
                            _calculationQueue.Remove(village);
                        }
                        
                        _logger.Info($"Starting calculations on {queue.Count} settlements, {_calculationQueue.Count} remaining in queue.");
                    }
                }

                if (queue != null)
                {
                    _calculationThread = new Thread(Start);
                    _calculationThread.Priority = ThreadPriority.BelowNormal;
                    _calculationThread.Start(queue);
                }
            }
        }

        private void Start(object obj)
        {
            var queue = obj as Queue<CalculationResults>;

            try
            {
                DoCalculations(queue);
            }
            catch (Exception ex)
            {
                _gameInterface.OnException(ex);
            }
        }

        public void ApplyCalculationResults()
        {
            CalculationResults result = null;
            lock (LOCK)
            {
                if(_calculationResults.Any())
                {
                    result = _calculationResults.Dequeue();
                }
            }

            if(result != null)
            {
                _logger.Info("Applying settlement calc result: " + result.Settlement.Name);
                _logger.Info($"{_calculationResults.Count} in queue");

                try
                {
                    result.Settlement.EconomicActors = result.EconomicActors;
                    result.Settlement.ActiveEconomicActors = result.ActiveEconomicActors;

                    foreach (var caste in result.HousingByCaste.Keys)
                    {
                        result.Settlement.HousingByCaste[caste] = result.HousingByCaste[caste];
                    }

                    foreach (var caste in result.PopulationByCaste.Keys)
                    {
                        result.Settlement.PopulationByCaste[caste] = result.PopulationByCaste[caste];
                    }

                    foreach (var actor in result.EconomicActors)
                    {
                        if (result.DeliveryAreas.ContainsKey(actor))
                        {
                            actor.DeliveryArea = result.DeliveryAreas[actor];
                        }
                        else
                        {
                            actor.DeliveryArea = new Tile[0];
                        }

                        actor.DeliveryRoutes.Clear();
                        
                        actor.ItemsConsumed.Clear();
                        foreach(var itemRate in actor.NeededInputs)
                        {
                            actor.ItemsConsumed[itemRate.Item] = true;
                        }

                        if (actor.UnderConstruction)
                        {
                            foreach (var iq in actor.ConstructionCost)
                            {
                                actor.ItemsConsumed[iq.Item] = true;
                            }
                        }

                        if (actor.IsUpgrading)
                        {
                            foreach (var iq in actor.UpgradeCost)
                            {
                                actor.ItemsConsumed[iq.Item] = true;
                            }
                        }

                        actor.ItemsProduced.Clear();
                        foreach (var itemRate in actor.ExpectedOutputs)
                        {
                            actor.ItemsProduced[itemRate.Item] = true;
                        }
                    }

                    foreach(var route in result.DeliveryRoutes)
                    {
                        route.Source.DeliveryRoutes.Add(route);
                    }
                    
                    foreach(var house in result.EconomicActors.OfType<IHousingProvider>())
                    {
                        house.Residents.Clear();
                    }

                    foreach(var pop in result.Population)
                    {
                        pop.Home = null;
                    }

                    foreach(var pop in result.NewHousingAssignments.Keys)
                    {
                        pop.Home = result.NewHousingAssignments[pop];
                        result.NewHousingAssignments[pop].Residents.Add(pop);
                    }

                    foreach(var cult in _gamedataTracker.Cults)
                    {
                        result.Settlement.CultMembership[cult] = result.CultMembership[cult];
                    }

                    var cultMembershipBuff = result.Settlement.Buffs
                        .Where(b => b.Id == "buff_cult_membership")
                        .FirstOrDefault();
                    if(cultMembershipBuff == null)
                    {
                        cultMembershipBuff = new Buff
                        {
                            Id = "buff_cult_membership",
                            IconKey = "buff_cult_membership",
                            Name = "Cult Memberships",
                            Description = string.Empty,
                            ExpireMonth = -1,
                            AppliedBonuses = new IntIndexableLookup<BonusType, double>()
                        };
                        result.Settlement.AddBuff(cultMembershipBuff);
                    }

                    cultMembershipBuff.AppliedBonuses.Clear();
                    foreach(var cult in _gamedataTracker.Cults)
                    {
                        var membership = result.CultMembership[cult].Count;
                        if(membership > 0)
                        {
                            var percent = membership / (double)result.Population.Length;

                            foreach(var bonus in cult.SettlementBonuses.Keys)
                            {
                                cultMembershipBuff.AppliedBonuses[bonus] += cult.SettlementBonuses[bonus] * percent;
                            }
                        }
                    }
                    
                    foreach (var fn in result.ApplyResultsFunctions)
                    {
                        fn();
                    }
                    
                    foreach(var cell in result.Settlement.Region.Cells)
                    {
                        cell.CalculateTraits();
                    }
                    
                    foreach(var caste in result.JobsByCaste.Keys)
                    {
                        result.Settlement.JobsByCaste[caste] = result.JobsByCaste[caste];
                    }

                    foreach(var item in _gamedataTracker.Items)
                    {
                        result.Settlement.ItemAvailability[item] = result.ItemAvailability[item];

                        foreach(var actor in result.EconomicActors)
                        {
                            actor.PerItemSatisfaction[item] = 1;
                        }
                    }

                    result.Settlement.RecruitmentSlotsMax.Clear();
                    foreach (var category in _gamedataTracker.RecruitmentSlotCategories)
                    {
                        result.Settlement.RecruitmentSlotsMax.Add(category, result.RecruitmentSlotsMax[category]);
                    }

                    foreach(var actor in result.Settlement.EconomicActors)
                    {
                        actor.CurrentBonuses.Clear();
                        if(result.ActorBonuses.ContainsKey(actor))
                        {
                            var newBonuses = result.ActorBonuses[actor];

                            foreach (var bonusType in newBonuses.Keys)
                            {
                                actor.CurrentBonuses[bonusType] = newBonuses[bonusType];
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    _gameInterface.OnException(ex);
                }
            }
        }

        private void DoCalculations(Queue<CalculationResults> queue)
        {
            while (queue.Any())
            {
                var current = queue.Dequeue();

                try
                {
                    var overallStopwatch = Stopwatch.StartNew();
                    var stopwatch = Stopwatch.StartNew();
                    current.DeliveryRoutes.Clear();

                    foreach (var district in current.Settlement.Districts)
                    {
                        foreach (var tile in district.AllTiles)
                        {
                            tile.CalculateTraits();
                        }
                    }
                    _performanceTracker.LogCalculationTime("CalculateTraits", stopwatch.ElapsedTicks);
                    stopwatch.Restart();

                    GatherEconomicActors(current);
                    _performanceTracker.LogCalculationTime("GatherEconomicActors", stopwatch.ElapsedTicks);
                    stopwatch.Restart();

                    CalculateBonuses(current);
                    _performanceTracker.LogCalculationTime("CalculateBonuses", stopwatch.ElapsedTicks);
                    stopwatch.Restart();

                    CalculateItemAvailability(current);
                    _performanceTracker.LogCalculationTime("CalculateItemAvailability", stopwatch.ElapsedTicks);
                    stopwatch.Restart();

                    DoWorkerAllocation(current);
                    _performanceTracker.LogCalculationTime("DoWorkerAllocation", stopwatch.ElapsedTicks);
                    stopwatch.Restart();

                    AssignHousing(current);
                    _performanceTracker.LogCalculationTime("AssignHousing", stopwatch.ElapsedTicks);
                    stopwatch.Restart();

                    TabulatePopulation(current);
                    _performanceTracker.LogCalculationTime("TabulatePopulation", stopwatch.ElapsedTicks);
                    stopwatch.Restart();

                    TabulateRecruitmentSlots(current);
                    _performanceTracker.LogCalculationTime("GatherGarrisonSlots", stopwatch.ElapsedTicks);
                    stopwatch.Restart();

                    foreach (var subCalculator in _subCalculators)
                    {
                        subCalculator.DoCalculations(current);
                        _performanceTracker.LogCalculationTime(subCalculator.GetType().Name, stopwatch.ElapsedTicks);
                        stopwatch.Restart();
                    }

                    current.DeliveryRoutes.AddRange(_settlementLogiCalculator.CalculateDeliveryRoutes(current));
                    _performanceTracker.LogCalculationTime("BuildDeliveryRoutes", stopwatch.ElapsedTicks);
                    stopwatch.Restart();
                    

                    stopwatch.Stop();
                    overallStopwatch.Stop();

                    _performanceTracker.LogSettlementCalculationTime(
                        current.Settlement.Name, 
                        overallStopwatch.Elapsed.Ticks);
                }
                catch (Exception ex)
                {
                    _gameInterface.OnException(ex);
                }

                lock (LOCK)
                {
                    _calculationResults.Enqueue(current);
                }
            }
        }
        
        private void TabulatePopulation(CalculationResults current)
        {
            foreach(var cult in _gamedataTracker.Cults)
            {
                current.CultMembership[cult] = new List<Pop>();
            }

            foreach (var pop in current.Population)
            {
                if (!current.PopulationByCaste.ContainsKey(pop.Caste))
                {
                    current.PopulationByCaste.Add(pop.Caste, 0);
                }

                current.PopulationByCaste[pop.Caste] += 1;

                if(pop.CultMembership != null)
                {
                    current.CultMembership[pop.CultMembership].Add(pop);
                }
            }
        }

        private void CalculateItemAvailability(CalculationResults current)
        {
            foreach(var item in _gamedataTracker.Items)
            {
                var isAvailable = current.EconomicActors
                    .Any(ea => ea.Inventory.QuantityOf(item) > 0 || ea.ExpectedOutputs.Any(o => o.Item == item));

                current.ItemAvailability[item] = isAvailable;

            }
        }

        private void TabulateRecruitmentSlots(CalculationResults calculationResults)
        {
            calculationResults.RecruitmentSlotsMax.Clear();
            foreach (var category in _gamedataTracker.RecruitmentSlotCategories)
            {
                calculationResults.RecruitmentSlotsMax.Add(category, 0);
            }

            foreach (var slotSource in calculationResults.Settlement.Race.PopRecruitmentSources)
            {
                if (calculationResults.PopulationByCaste.ContainsKey(slotSource.Caste))
                {
                    calculationResults.RecruitmentSlotsMax[slotSource.Category] +=
                        calculationResults.PopulationByCaste[slotSource.Caste] / slotSource.PopPerSlot;
                }
            }

            foreach (var slotSource in calculationResults.EconomicActors.OfType<IRecruitmentSlotProvider>())
            {
                calculationResults.RecruitmentSlotsMax[slotSource.RecruitmentCategory] += slotSource.ProvidedRecruitmentSlots;
            }

            if (calculationResults.Population.Count() > 0)
            {
                foreach (var cult in _gamedataTracker.Cults)
                {
                    var percent = calculationResults.CultMembership[cult].Count / (double)calculationResults.Population.Length;

                    foreach (var recruitmentCategory in cult.RecruitmentSlots.Keys)
                    {
                        var count = (int)Math.Floor(percent * cult.RecruitmentSlots[recruitmentCategory]);

                        calculationResults.RecruitmentSlotsMax[recruitmentCategory] += count;
                    }
                }
            }
        }
        
        private void GatherEconomicActors(CalculationResults calculationResults)
        {
            var developments = calculationResults.CellDevelopments;
            var structures = calculationResults.Structures;

            var campedArmies = calculationResults.Settlement.Owner.Armies
                .Where(a => a.CurrentOrder is ArmyCampOrder campOrder && campOrder.IsCamped)
                .Where(a => a.Cell?.Region?.Settlement == calculationResults.Settlement)
                .Select(a => (a.CurrentOrder as ArmyCampOrder).CampedArmy ?? new CampedArmy(a))
                .ToArray();

            var nextIndex = 0;
            var economicActors = new IEconomicActor[developments.Length + structures.Length + campedArmies.Length];

            // Interleave structures and developments in the actor list
            for (var i = 0; i < developments.Length || i < structures.Length || i < campedArmies.Length; i++)
            {
                if (i < developments.Length)
                {
                    economicActors[nextIndex] = developments[i];
                    nextIndex += 1;
                }
                if (i < structures.Length)
                {
                    economicActors[nextIndex] = structures[i];
                    nextIndex += 1;
                }
                if(i < campedArmies.Length)
                {
                    economicActors[nextIndex] = campedArmies[i];
                    nextIndex += 1;
                }
            }

            calculationResults.EconomicActors = economicActors;
            calculationResults.ActiveEconomicActors = economicActors
                .Where(e => e.NeededInputs.Any() || e.ExpectedOutputs.Any() || e.WorkerNeed.Need > 0 || e.UnderConstruction)
                .ToArray();

        }

        private void CalculateBonuses(CalculationResults calculationResults)
        {
            foreach (var actor in calculationResults.EconomicActors)
            {
                calculationResults.ActorBonuses.Add(actor, new IntIndexableLookup<BonusType, double>());
            }

            for (var i = 0; i < calculationResults.Settlement.Buffs.Count; i++)
            {
                var buff = calculationResults.Settlement.Buffs[i];
                if(buff != null)
                {
                    foreach (var actor in calculationResults.EconomicActors)
                    {
                        foreach (var bonusType in _gamedataTracker.BonusTypes)
                        {
                            calculationResults.ActorBonuses[actor][bonusType] += buff.AppliedBonuses[bonusType];
                        }
                    }
                }
            }

            for (var i = 0; i < calculationResults.Settlement.Owner.Buffs.Count; i++)
            {
                var buff = calculationResults.Settlement.Owner.Buffs[i];
                if (buff != null)
                {
                    foreach (var actor in calculationResults.EconomicActors)
                    {
                        foreach (var bonusType in _gamedataTracker.BonusTypes)
                        {
                            calculationResults.ActorBonuses[actor][bonusType] += buff.AppliedBonuses[bonusType];
                        }
                    }
                }
            }
        }

        private void DoWorkerAllocation(CalculationResults current)
        {
            foreach (var caste in current.Settlement.Race.Castes)
            {
                var workerNeed = 0;
                var constructionWorkerNeed = 0;
                var totalConstructionNeed = 0;

                foreach(var economicActor in current.EconomicActors)
                {
                    if (economicActor.WorkerNeed.Caste == caste)
                    {
                        if (!economicActor.UnderConstruction)
                        {
                            workerNeed += economicActor.WorkerNeed.Need;
                        }
                        else if (economicActor.UnderConstruction)
                        {
                            if (constructionWorkerNeed == 0)
                            {
                                constructionWorkerNeed = economicActor.WorkerNeed.Need;
                            }

                            totalConstructionNeed += economicActor.WorkerNeed.Need;
                        }
                    }
                }

                current.JobsByCaste.Add(caste, workerNeed + totalConstructionNeed);
                
                
                var workersAvailable = current.Population
                    .Where(p => p.Caste == caste)
                    .Count();

                var workerSupplyFactor = Math.Min(1, workersAvailable / Math.Max(1, (float)(workerNeed + constructionWorkerNeed)));

                // Allocate workers evenly to non-construction actors
                foreach (var actor in current.EconomicActors)
                {
                    if (!actor.UnderConstruction && actor.WorkerNeed.Caste == caste)
                    {
                        if (actor.WorkerNeed.Need > 0 && workersAvailable > 0)
                        {
                            var workersToAllocate = (int)Math.Max(1, Math.Round((double)(actor.WorkerNeed.Need * workerSupplyFactor)));
                            workersAvailable -= workersToAllocate;
                            actor.WorkerSupply = workersToAllocate;
                        }
                        else
                        {
                            actor.WorkerSupply = 0;
                        }
                    }
                }

                // Allocate workers first-come, first-served, to actors under construction
                foreach (var actor in current.EconomicActors)
                {
                    var canBeBuilt = actor.NeededInputs
                        .Select(ir => ir.Item)
                        .All(item => current.ItemAvailability[item]);

                    if (actor.UnderConstruction
                        && actor.WorkerNeed.Caste == caste
                        && canBeBuilt)
                    {
                        if (actor.WorkerNeed.Need > 0 && workersAvailable > 0)
                        {
                            var workersToAllocate = Math.Min(actor.WorkerNeed.Need, workersAvailable);
                            workersAvailable -= workersToAllocate;
                            actor.WorkerSupply = workersToAllocate;
                        }
                        else
                        {
                            actor.WorkerSupply = 0;
                        }
                    }
                }
            }
        }
        
        private void AssignHousing(CalculationResults current)
        {
            var houses = current.EconomicActors
                .Where(h => !h.UnderConstruction)
                .OfType<IHousingProvider>()
                .ToArray();

            foreach (var house in houses)
            {
                if(!current.HousingByCaste.ContainsKey(house.SupportedCaste))
                {
                    current.HousingByCaste.Add(house.SupportedCaste, 0);
                }

                current.HousingByCaste[house.SupportedCaste] += house.ProvidedHousing;
            }
            
            var availableHousing = houses
                .ToDictionary(
                    h => h,
                    h => h.ProvidedHousing);
            
            foreach(var pop in current.Population)
            {
                var house = houses
                    .Where(h => h.SupportedCaste == pop.Caste)
                    .Where(h => availableHousing[h] > 0)
                    .FirstOrDefault();

                if(house != null)
                {
                    current.NewHousingAssignments.Add(pop, house);
                    availableHousing[house] -= 1;
                }
            }
        }

        public void ForceCalculations(ReportStepProgress reportProgress)
        {
            var count = 0.0;
            foreach(var settlement in _calculationQueue)
            {
                DoCalculations(new Queue<CalculationResults>(settlement.Yield()));
                ApplyCalculationResults();
                count += 1;
                reportProgress(count / _calculationQueue.Count);
            }
            
            _calculationQueue.Clear();
        }
    }
}
