﻿using Bronze.Common.UI.Modals;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.UI;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.Services
{
    public class MarriageOptionNotificationBuilder : IMarriageOptionNotificationBuilder, INotificationHandler
    {
        private readonly IWorldManager _worldManager;
        private readonly IDialogManager _dialogManager;
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IPlayerDataTracker _playerData;

        public string Name => typeof(MarriageOptionNotificationBuilder).Name;

        public MarriageOptionNotificationBuilder(
            IWorldManager worldManager,
            IDialogManager dialogManager,
            IGamedataTracker gamedataTracker,
            IPlayerDataTracker playerData)
        {
            _worldManager = worldManager;
            _dialogManager = dialogManager;
            _gamedataTracker = gamedataTracker;
            _playerData = playerData;
        }

        public Notification BuildMarriageOptionNotification(
            string icon, 
            string summaryTitle, 
            string summaryBody, 
            string title, 
            string body, 
            NotablePerson person, 
            NotablePerson spouse, 
            Settlement fromSettlement,
            Buff settlementBuff)
        {
            var data = new Dictionary<string, string>
            {
                { "title", title },
                { "body", body },
                { "person_id", person.Id }
            };

            var serializedSpouse = new SerializedObject("spouse");
            spouse.SerializeTo(serializedSpouse);

            var serializedData = new Dictionary<string, SerializedObject>
            {
                { "spouse", serializedSpouse }
            };

            if (fromSettlement != null)
            {
                var serializedBuff = new SerializedObject("buff");
                settlementBuff.SerializeTo(serializedBuff);

                data.Add("from_settlement_id", fromSettlement.Id);
                serializedData.Add("from_settlement_buff", serializedBuff);
            }



            return new Notification
            {
                AutoOpen = false,
                ExpireDate = _worldManager.Month + 2,
                Handler = typeof(MarriageOptionNotificationBuilder).Name,
                Icon = icon,
                SoundEffect = UiSounds.NotificationMinor,
                QuickDismissable = true,
                DismissOnOpen = true,
                SummaryTitle = summaryTitle,
                SummaryBody = summaryBody,
                Data = data,
                SerializedData = serializedData
            };
        }

        public Notification BuildAdoptionOptionNotification(
            string icon,
            string summaryTitle,
            string summaryBody,
            string title,
            string body,
            NotablePerson adoptee,
            Settlement fromSettlement,
            Buff settlementBuff)
        {
            var data = new Dictionary<string, string>
            {
                { "title", title },
                { "body", body },
            };

            var serializedAdoptee = new SerializedObject("adoptee");
            adoptee.SerializeTo(serializedAdoptee);

            var serializedData = new Dictionary<string, SerializedObject>
            {
                { "adoptee", serializedAdoptee }
            };

            if (fromSettlement != null)
            {
                var serializedBuff = new SerializedObject("buff");
                settlementBuff.SerializeTo(serializedBuff);

                data.Add("from_settlement_id", fromSettlement.Id);
                serializedData.Add("from_settlement_buff", serializedBuff);
            }
            
            return new Notification
            {
                AutoOpen = false,
                ExpireDate = _worldManager.Month + 2,
                Handler = typeof(MarriageOptionNotificationBuilder).Name,
                Icon = icon,
                SoundEffect = UiSounds.NotificationMinor,
                QuickDismissable = true,
                DismissOnOpen = true,
                SummaryTitle = summaryTitle,
                SummaryBody = summaryBody,
                Data = data,
                SerializedData = serializedData
            };
        }

        public void Handle(Notification notification)
        {
            Settlement fromSettlement = null;
            NotablePerson person = null;
            NotablePerson spouse = null;
            Buff buff = null;

            if (notification.SerializedData.ContainsKey("spouse"))
            {
                if (notification.Data.ContainsKey("from_settlement_id"))
                {
                    var fromSettlementId = notification.Data["from_settlement_id"];

                    fromSettlement = _playerData.PlayerTribe
                        .Settlements
                        .Where(s => s.Id == fromSettlementId)
                        .FirstOrDefault();

                    buff = Buff.DeserializeFrom(
                        _gamedataTracker,
                        notification.SerializedData["from_settlement_buff"]);
                }

                spouse = NotablePerson.DeserializeFrom(
                    _gamedataTracker,
                    _worldManager.Tribes,
                    notification.SerializedData["spouse"]);

                var personId = notification.Data["person_id"];
                person = _playerData.PlayerTribe
                    .NotablePeople
                    .Where(p => p.Id == personId)
                    .FirstOrDefault();

                if (person != null && person.Spouse == null)
                {
                    _dialogManager.PushModal<MarriageOptionModal, MarriageOptionModalContext>(
                    new MarriageOptionModalContext
                    {
                        Title = notification.Data["title"],
                        Body = new[]
                        {
                            notification.Data["body"]
                        },
                        Person = person,
                        Spouse = spouse,
                        FromSettlement = fromSettlement,
                        FromSettlementBuff = buff
                    });
                }
            }
            else if (notification.SerializedData.ContainsKey("adoptee"))
            {
                if (notification.Data.ContainsKey("from_settlement_id"))
                {
                    var fromSettlementId = notification.Data["from_settlement_id"];

                    fromSettlement = _playerData.PlayerTribe
                        .Settlements
                        .Where(s => s.Id == fromSettlementId)
                        .FirstOrDefault();

                    buff = Buff.DeserializeFrom(
                        _gamedataTracker,
                        notification.SerializedData["from_settlement_buff"]);
                }

                var adoptee = NotablePerson.DeserializeFrom(
                    _gamedataTracker,
                    _worldManager.Tribes,
                    notification.SerializedData["adoptee"]);

                var personId = notification.Data["person_id"];
                person = _playerData.PlayerTribe
                    .NotablePeople
                    .Where(p => p.Id == personId)
                    .FirstOrDefault();
                
                _dialogManager.PushModal<AdoptionOptionModal, AdoptionOptionModalContext>(
                    new AdoptionOptionModalContext
                    {
                        Title = notification.Data["title"],
                        Body = new[]
                        {
                            notification.Data["body"]
                        },
                        Adoptee = adoptee,
                        FromSettlement = fromSettlement,
                        FromSettlementBuff = buff
                    });
            }
        }

        public void HandleIgnored(Notification notification)
        {
        }
    }
}
