﻿using Bronze.Common.Data.World.Units;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Bronze.Common.Services
{
    internal class WorldSimulator : IWorldSimulator
    {
        public event OnUpdateHandler OnUpdate;

        private readonly IGameInterface _gameInterface;
        private readonly IWorldManager _worldManager;
        private readonly IPerformanceTracker _performanceTracker;
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IUserSettings _userSettings;
        private readonly IPlayerDataTracker _playerData;
        private readonly ISubSimulator[] _subSimulators;
        private readonly SettlementSimulator _settlementSimulator;
        private readonly ISettlementCalculator _settlementCalculator;
        private readonly IEmpireNotificationBuilder _empireNotificationHandler;
        private readonly ILogger _logger;
        private readonly INotificationsSystem _notificationsSystem;
        private readonly IUnitHelper _unitHelper;
        private readonly IGenericNotificationBuilder _genericNotificationBuilder;
        private readonly IMusicPlayer _musicPlayer;
        
        public WorldSimulator(
            IGameInterface gameInterface,
            IWorldManager worldManager,
            IPerformanceTracker performanceTracker,
            IGamedataTracker gamedataTracker,
            IUserSettings userSettings,
            IPlayerDataTracker playerData,
            IEnumerable<ISubSimulator> subSimulators,
            IEnumerable<ISettlementSubcalcuator> subcalculators,
            IEmpireNotificationBuilder empireNotificationHandler,
            ILogger logger,
            ISettlementLogiCalculator settlementLogiCalculator,
            INotificationsSystem notificationsSystem,
            IUnitHelper unitHelper,
            IGenericNotificationBuilder genericNotificationBuilder,
            ISettlementCalculator settlementCalculator,
            IMusicPlayer musicPlayer,
            ITradeManager tradeManager)
        {
            _gameInterface = gameInterface;
            _worldManager = worldManager;
            _performanceTracker = performanceTracker;
            _gamedataTracker = gamedataTracker;
            _userSettings = userSettings;
            _playerData = playerData;
            _subSimulators = subSimulators.ToArray();
            _empireNotificationHandler = empireNotificationHandler;
            _logger = logger;
            _unitHelper = unitHelper;
            _settlementCalculator = settlementCalculator;
            _notificationsSystem = notificationsSystem;
            _genericNotificationBuilder = genericNotificationBuilder;
            _musicPlayer = musicPlayer;
            _settlementSimulator = new SettlementSimulator(worldManager, _gamedataTracker, _unitHelper, tradeManager);
        }

        public void StepSimulation(float elapsedSeconds)
        {
            _worldManager.PlaytimeSeconds += elapsedSeconds;

            var scaledElapsedSeconds = elapsedSeconds * _userSettings.SimulationSpeed;

            if(_userSettings.SimulationPaused)
            {
                scaledElapsedSeconds = 0;
            }

            var elapsedMonths = scaledElapsedSeconds * Constants.MONTHS_PER_SECOND;

            _worldManager.Month += elapsedMonths;

            if(_playerData.MouseMode != null && !_playerData.MouseMode.IsValidFor(_playerData.ViewMode))
            {
                _playerData.MouseMode.OnCancel();
                _playerData.MouseMode = null;
            }

            var stopwatch = Stopwatch.StartNew();
            
            DoTribeSimulation(elapsedMonths);
            _performanceTracker.LogUpdateTime("TribeSimulation", stopwatch.ElapsedTicks);
            stopwatch.Restart();

            var aggregatePerformanceTracker = new AggregatePerformanceTracker();

            DoSettlementSimulation(elapsedMonths, aggregatePerformanceTracker);
            _performanceTracker.LogUpdateTime("SettlementSimulation", stopwatch.ElapsedTicks);
            stopwatch.Restart();

            DoArmySimulation(elapsedMonths, aggregatePerformanceTracker);
            _performanceTracker.LogUpdateTime("ArmySimulation", stopwatch.ElapsedTicks);
            stopwatch.Restart();
            
            // Handle async settlement calculations
            foreach (var settlement in _worldManager.Tribes.SelectMany(t => t.Settlements))
            {
                if(settlement.NeedsCalculation)
                {
                    _logger.Info($"Queueing Calculation: {settlement.Name}({settlement.Owner.Name}): " + string.Join(", ", settlement.CalculationReasons.Distinct()));
                    settlement.ClearCalculationFlag();
                    _settlementCalculator.QueueCalculation(settlement);
                }
            }
            _performanceTracker.LogUpdateTime("Queueing Calculation", stopwatch.ElapsedTicks);
            stopwatch.Restart();

            foreach (var subSimulator in _subSimulators)
            {
                subSimulator.StepSimulation(elapsedMonths);

                _performanceTracker.LogUpdateTime(subSimulator.GetType().Name, stopwatch.ElapsedTicks);
                stopwatch.Restart();
            }

            _performanceTracker.LogUpdateTime("District Generation", stopwatch.ElapsedTicks);
            stopwatch.Restart();
            
            _settlementCalculator.ApplyCalculationResults();
            _performanceTracker.LogUpdateTime("Settlement Calculation Results", stopwatch.ElapsedTicks);
            stopwatch.Restart();
            _settlementCalculator.StartCalculations();

            _performanceTracker.LogUpdateTime("Settlement Calculation Start", stopwatch.ElapsedTicks);
            stopwatch.Restart();


            var dangerLevel = 0.0;
            if(_playerData.PlayerTribe != null)
            {
                if(_playerData.PlayerTribe.CurrentWars.Any())
                {
                    dangerLevel += 0.4;
                }
                if (_playerData.PlayerTribe.Armies.Any(a => a.SkirmishingWith.Any()))
                {
                    dangerLevel += 0.3;
                }
                if (_playerData.PlayerTribe.Settlements.Any(s => s.IsUnderSiege))
                {
                    dangerLevel += 0.3;
                }
            }
            _musicPlayer.SetDangerLevel(dangerLevel);

            OnUpdate?.Invoke();

            foreach(var tuple in aggregatePerformanceTracker.GetValues())
            {
                _performanceTracker.LogUpdateTime(tuple.Item1, tuple.Item2);
            }
        }

        private void DoTribeSimulation(double elapsedMonths)
        {
            try
            {
                foreach (var tribe in _worldManager.Tribes)
                {
                    if (tribe.Ruler != null)
                    {
                        tribe.Authority = tribe.Ruler.GetAuthority();
                    }
                    else
                    {
                        tribe.Authority = 0;
                    }

                    var buffsToRemove = tribe.Buffs
                        .Where(b => b.ExpireMonth > 0 && b.ExpireMonth < _worldManager.Month)
                        .ToArray();
                    foreach (var buff in buffsToRemove)
                    {
                        tribe.RemoveBuff(buff);
                    }

                    tribe.TotalPopulation = tribe.Settlements.Sum(s => s.Population.Count);
                    tribe.AuthorityPerSettlement = tribe.Authority / Math.Max(1, tribe.Settlements.Where(s => s.Governor == null).Count());

                    if ((tribe.Capital == null || tribe.Capital.Owner != tribe) && tribe.Settlements.Any())
                    {
                        tribe.Capital = tribe.Settlements
                            .OrderByDescending(t => t.Population.Count)
                            .FirstOrDefault();
                        
                        if(_playerData.PlayerTribe == tribe
                            && tribe.Capital != null)
                        {
                            _notificationsSystem.AddNotification(_empireNotificationHandler.BuildCapitalChanged(_playerData.PlayerTribe, _playerData.PlayerTribe.Capital));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _gameInterface.OnException(ex);
            }
        }

        private void DoSettlementSimulation(double elapsedMonths, AggregatePerformanceTracker aggregatePerformanceTracker)
        {
            try
            {
                foreach (var tribe in _worldManager.Tribes)
                {
                    for(var i = 0; i < tribe.Settlements.Count; i++)
                    {
                        var settlement = tribe.Settlements[i];

                        var buffsToRemove = settlement.Buffs
                            .Where(b => b.ExpireMonth > 0 && b.ExpireMonth < _worldManager.Month)
                            .ToArray();
                        foreach (var buff in buffsToRemove)
                        {
                            settlement.RemoveBuff(buff);
                        }

                        if (!settlement.Population.Any())
                        {
                            tribe.Abandon(settlement);
                            i -= 1;
                        }
                        else
                        {
                            settlement.StructureAuthority = settlement.EconomicActors
                                        .OfType<IAuthorityProvider>()
                                        .Sum(ap => ap.AuthorityProvided);

                            if (settlement.Governor != null)
                            {
                                settlement.GovernorAuthority = settlement.Governor.GetAuthority();

                                settlement.TotalAuthority = settlement.GovernorAuthority + settlement.StructureAuthority;
                            }
                            else
                            {
                                settlement.GovernorAuthority = 0;
                                settlement.TotalAuthority = tribe.AuthorityPerSettlement + settlement.StructureAuthority;
                            }

                            settlement.AuthorityFraction = Util.Clamp((double)settlement.TotalAuthority / Math.Max(settlement.Population.Count, 1), 0, 1);

                            _settlementSimulator.Simulate(settlement, elapsedMonths, aggregatePerformanceTracker);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _gameInterface.OnException(ex);
            }
        }

        private void DoArmySimulation(double elapsedMonths, AggregatePerformanceTracker aggregatePerformanceTracker)
        {
            try
            {
                foreach (var tribe in _worldManager.Tribes)
                {
                    foreach (var army in tribe.Armies.ToArray())
                    {
                        if (!army.Units.Any())
                        {
                            _unitHelper.RemoveArmy(army);
                        }
                        else
                        {
                            var generalMoraleRecoveryBonus = army.General?.GetBonusMoraleRegen() ?? 0.5;

                            foreach (var unit in army.Units)
                            {
                                if (!unit.SufferingAttrition)
                                {
                                    unit.Morale = Math.Min(1, unit.Morale + generalMoraleRecoveryBonus * elapsedMonths);
                                }
                                else if (unit.Morale > 0.1)
                                {
                                    unit.Morale = Math.Max(0.1, unit.Morale - elapsedMonths);
                                }
                            }

                            if (army.CurrentOrder is NullOrder)
                            {
                                army.CurrentOrder = new IdleOrder();
                            }

                            var isTrapped = army.Cell.Owner != null
                                && !army.Cell.Owner.AllowsAccess(army.Owner)
                                && !army.Cell.OrthoNeighbors
                                    .Any(n => army.CanPath(n));

                            if (isTrapped && army.Owner.Settlements.Any())
                            {
                                var teleportLocation = army.Owner.Settlements
                                    .SelectMany(s => s.Region.Cells)
                                    .Where(c => army.CanPath(c))
                                    .OrderBy(c => c.Position.DistanceSq(army.Cell.Position))
                                    .FirstOrDefault();

                                if (teleportLocation != null)
                                {
                                    army.MoveTo(teleportLocation);

                                    if (army.Owner == _playerData.PlayerTribe)
                                    {
                                        _notificationsSystem.AddNotification(_genericNotificationBuilder
                                            .BuildArmyNotification(
                                                "ui_notification_army",
                                                "Army Relocated",
                                                "An army has been forcibly relocated.",
                                                $"{army.Name} Relocated",
                                                "One of your armies has been forcibly relocated, becuase it was trapped in another tribe's territory that does not grant you access.",
                                                army));
                                    }
                                }
                            }

                            var stopwatch = Stopwatch.StartNew();

                            army.CurrentOrder.Simulate(army, elapsedMonths, aggregatePerformanceTracker);

                            aggregatePerformanceTracker.AddValue("army_order", stopwatch.ElapsedTicks);
                            stopwatch.Restart();

                            foreach (var unit in army.Units.ToArray())
                            {
                                unit.Update(elapsedMonths, army);

                                if (unit.HealthPercent <= 0)
                                {
                                    _unitHelper.UnitAttritionedToDeath(army, unit);
                                }
                            }
                            aggregatePerformanceTracker.AddValue("army_units", stopwatch.ElapsedTicks);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _gameInterface.OnException(ex);
            }
        }
    }
}
