﻿using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Bronze.Common.Services
{
    public class SettlementSimulator
    {
        private readonly IWorldManager _worldManager;
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IUnitHelper _unitHelper;
        private readonly ITradeManager _tradeManager;
        private readonly Random _random;
        
        public SettlementSimulator(
            IWorldManager worldManager,
            IGamedataTracker gamedataTracker,
            IUnitHelper unitHelper,
            ITradeManager tradeManager)
        {
            _worldManager = worldManager;
            _gamedataTracker = gamedataTracker;
            _unitHelper = unitHelper;
            _tradeManager = tradeManager;
            _random = new Random();
        }

        public void Simulate(Settlement settlement, double deltaMonths, AggregatePerformanceTracker performanceTracker)
        {
            var stopwatch = Stopwatch.StartNew();
            settlement.SiegedBy.RemoveAll(a => a.Units.Count < 1 || !a.Owner.IsHostileTo(settlement.Owner));

            if (_worldManager.Month >= settlement.NextSummaryMonth)
            {
                DoMonthlySummaries(settlement);
            }
            performanceTracker.AddValue("SS.MonthlySummaries", stopwatch.ElapsedTicks);
            stopwatch.Restart();

            DoHaulers(settlement, deltaMonths);
            performanceTracker.AddValue("SS.DoHaulers", stopwatch.ElapsedTicks);
            stopwatch.Restart();

            if (_worldManager.Month >= settlement.NextSimMonth || settlement.NextSimMonth == 0)
            {
                var monthsSinceLastSim = _worldManager.Month - settlement.LastSimMonth;

                if (settlement.LastSimMonth == 0)
                {
                    monthsSinceLastSim = 0;
                }

                // Homeless satisfaction updates
                foreach (var pop in settlement.Population.Where(p => p.Home == null))
                {
                    pop.AddSatisfaction(deltaMonths, 0);
                }

                performanceTracker.AddValue("SS.Preamble", stopwatch.ElapsedTicks);
                stopwatch.Restart();

                DoItemMovement(settlement, monthsSinceLastSim);
                performanceTracker.AddValue("SS.DoItemMovement", stopwatch.ElapsedTicks);
                stopwatch.Restart();
                
                DoProduction(settlement, monthsSinceLastSim);
                performanceTracker.AddValue("SS.DoProduction", stopwatch.ElapsedTicks);
                stopwatch.Restart();

                DoConstruction(settlement, monthsSinceLastSim);
                performanceTracker.AddValue("SS.DoConstruction", stopwatch.ElapsedTicks);
                stopwatch.Restart();

                DoGrowth(settlement, monthsSinceLastSim);
                performanceTracker.AddValue("SS.DoGrowth", stopwatch.ElapsedTicks);
                stopwatch.Restart();

                DoRecruitmentSlots(settlement, monthsSinceLastSim);
                performanceTracker.AddValue("SS.DoRecruitmentSlots", stopwatch.ElapsedTicks);
                stopwatch.Restart();
                
                foreach (var actor in settlement.EconomicActors)
                {
                    actor.DoUpdate(monthsSinceLastSim);
                }
                performanceTracker.AddValue("SS.EconomicActors", stopwatch.ElapsedTicks);
                stopwatch.Stop();

                settlement.LastSimMonth = _worldManager.Month;
                // Stagger simulations slightly to reduce load per frame
                // Anywhere between 0.1 and 0.2 months between simulations
                settlement.NextSimMonth = _random.Next(1, 2) / 10.0;
            }
        }
        
        private void DoMonthlySummaries(Settlement settlement)
        {
            settlement.NextSummaryMonth = _worldManager.Month + 1;
            
            foreach (var actor in settlement.EconomicActors)
            {
                if (actor.ActiveRecipie != null
                    && !actor.ActiveRecipie.IsValid(actor))
                {
                    actor.ActiveRecipie = null;
                }

                if(actor.ActiveRecipie != null)
                {
                    foreach(var input in actor.ActiveRecipie.Inputs)
                    {
                        settlement.LastMonthProduction[input.Item] -= input.PerMonth;
                    }

                    foreach (var output in actor.ActiveRecipie.BoostedOutputs(actor, actor.NetTraits))
                    {
                        settlement.LastMonthProduction[output.Item] += output.PerMonth;
                    }
                }
            }

            foreach (var item in _gamedataTracker.Items)
            {
                settlement.NetItemProduction[item] = Math.Round(settlement.LastMonthProduction[item]);
                settlement.LastMonthProduction[item] = 0;
            }
        }

        private void DoItemMovement(Settlement settlement, double deltaMonths)
        {
            for (var i = 0; i < settlement.ActiveEconomicActors.Length; i++)
            {
                var actor = settlement.ActiveEconomicActors[i];
                actor.ItemTransmissionWait -= deltaMonths * Math.Max(0.5, actor.WorkerPercent);

                if (actor.ItemTransmissionWait <= 0)
                {
                    actor.ItemTransmissionWait = _random.NextDouble() * 0.1 + 0.4;

                    var itemsToSend = actor.ExpectedOutputs.Select(ir => ir.Item);

                    var actorAsStorage = actor as IItemStorageProvider;

                    if(actorAsStorage != null)
                    {
                        itemsToSend = _gamedataTracker.Items.Where(item => actorAsStorage.CanStore(item));
                    }
                    
                    foreach (var item in itemsToSend)
                    {
                        if(actor.Inventory.QuantityOf(item) >= 1)
                        {
                            var maxToSend = double.MaxValue;
                            
                            var routeToUse = actor.DeliveryRoutes
                                .Where(dr => dr.Destination.ItemsConsumed[item] || (!(actor is IItemStorageProvider) && dr.Destination is IItemStorageProvider destStorage && destStorage.CanStore(item)))
                                .Where(dr => dr.Destination.NeedOf(item) > 0)
                                .OrderBy(dr => dr.Destination.Inventory.QuantityOf(item) + dr.Destination.IncomingItems[item])
                                .FirstOrDefault();

                            if (routeToUse != null)
                            {
                                var toSend = Math.Min(
                                    Math.Min(
                                        maxToSend,
                                        routeToUse.Hauler.Capacity),
                                    Math.Min(
                                        routeToUse.Destination.NeedOf(item),
                                        actor.Inventory.QuantityOf(item)));

                                if (toSend > 0)
                                {
                                    var cargo = new ItemQuantity(item, toSend);
                                    actor.Inventory.Remove(cargo);

                                    settlement.AddLogiHauler(routeToUse, cargo);
                                }
                            }

                            // If storage, try sending to other storage providers
                            if (actorAsStorage != null)
                            {
                                routeToUse = actor.DeliveryRoutes
                                    .Where(dr => dr.Destination is IItemStorageProvider destStorage && destStorage.CanStore(item))
                                    .Where(dr => dr.Destination.Inventory.QuantityOf(item) + dr.Destination.IncomingItems[item] < dr.Destination.InventoryCapacity)
                                    .Where(dr => dr.Destination.Inventory.QuantityOf(item) + dr.Destination.IncomingItems[item] < actorAsStorage.Inventory.QuantityOf(item) - 1)
                                    .OrderBy(dr => dr.Destination.Inventory.QuantityOf(item) + dr.Destination.IncomingItems[item])
                                    .FirstOrDefault();

                                if (routeToUse != null)
                                {
                                    maxToSend = actorAsStorage.Inventory.QuantityOf(item) - (routeToUse.Destination.Inventory.QuantityOf(item) + routeToUse.Destination.IncomingItems[item]) - 1;
                                }

                                if (routeToUse != null)
                                {
                                    var toSend = Math.Min(
                                        Math.Min(
                                            maxToSend,
                                            routeToUse.Hauler.Capacity),
                                        Math.Min(
                                            routeToUse.Destination.InventoryCapacity - (routeToUse.Destination.Inventory.QuantityOf(item) + routeToUse.Destination.IncomingItems[item]),
                                            actor.Inventory.QuantityOf(item)));

                                    if (toSend > 0)
                                    {
                                        var cargo = new ItemQuantity(item, toSend);
                                        actor.Inventory.Remove(cargo);

                                        settlement.AddLogiHauler(routeToUse, cargo);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void DoHaulers(Settlement settlement, double deltaMonths)
        {
            for(var i = 0; i < settlement.LogiHaulers.Count; i++)
            {
                var hauler = settlement.LogiHaulers[i];

                if(hauler.PositionOnPath >= hauler.Path.Length)
                {
                    hauler.To.Inventory.Add(hauler.Cargo);
                    settlement.RemoveLogiHauler(hauler);
                    i -= 1;
                }
                else
                {
                    hauler.AnimationTime += deltaMonths / Constants.MONTHS_PER_SECOND;
                    hauler.ProgressToNext += deltaMonths * 25 / Math.Max(0.01, hauler.Path[hauler.PositionOnPath].LogiDistance);

                    if(hauler.ProgressToNext >= 1)
                    {
                        hauler.ProgressToNext -= 1;
                        hauler.PositionOnPath += 1;

                        if (hauler.PositionOnPath + 1 >= hauler.Path.Length)
                        {
                            hauler.To.Inventory.Add(hauler.Cargo);
                            settlement.RemoveLogiHauler(hauler);
                            i -= 1;
                        }
                        else
                        {
                            hauler.DetermineActiveMovementType();

                            if (hauler.ActiveMovementType == null)
                            {
                                settlement.RemoveLogiHauler(hauler);
                                i -= 1;
                            }
                        }
                    }
                }
            }
        }

        private void DoConstruction(Settlement settlement, double deltaMonths)
        {
            for (var i = 0; i < settlement.EconomicActors.Length; i++)
            {
                var actor = settlement.EconomicActors[i];

                if (actor.UnderConstruction)
                {
                    if(actor.ConstructionProgress <= 0)
                    {
                        if(actor.Inventory.HasAll(actor.ConstructionCost))
                        {
                            actor.Inventory.RemoveAll(actor.ConstructionCost);
                            actor.ConstructionProgress = 0.001;
                            actor.FactoryState = FactoryState.Operating;
                            foreach(var iq in actor.ConstructionCost)
                            {
                                settlement.LastMonthProduction[iq.Item] -= iq.Quantity; 
                            }
                        }
                        else
                        {
                            actor.FactoryState = FactoryState.NeedsInputs;
                        }
                    }
                    else
                    {
                        actor.ConstructionProgress += deltaMonths * actor.WorkerPercent;
                        actor.FactoryState = FactoryState.Operating;
                    }

                    if (actor.ConstructionProgress >= actor.ConstructionMonths)
                    {
                        actor.ConstructionProgress = actor.ConstructionMonths;
                        actor.UnderConstruction = false;
                        actor.OnConstructionFinished();
                        settlement.SetCalculationFlag("construction finished");
                    }
                }

                if (actor.IsUpgrading)
                {
                    if (actor.UpgradeProgress <= 0)
                    {
                        if (actor.Inventory.HasAll(actor.UpgradeCost))
                        {
                            actor.Inventory.RemoveAll(actor.UpgradeCost);
                            actor.UpgradeProgress = 0.001;
                            foreach (var iq in actor.UpgradeCost)
                            {
                                settlement.LastMonthProduction[iq.Item] -= iq.Quantity;
                            }
                        }
                    }
                    else
                    {
                        actor.UpgradeProgress += deltaMonths * actor.WorkerPercent;
                    }

                    if (actor.UpgradeProgress>= actor.UpgradeMonths)
                    {
                        actor.FinishUpgrading();
                        settlement.SetCalculationFlag("upgrade finished");
                    }
                }
            }
        }

        private void DoProduction(Settlement settlement, double deltaMonths)
        {
            for (var i = 0; i < settlement.EconomicActors.Length; i++)
            {
                var actor = settlement.EconomicActors[i];
                
                if (!actor.UnderConstruction && actor.UpkeepInputs.Any() && deltaMonths > 0)
                {
                    actor.UpkeepPercent = Util.PullInputs(actor, actor.UpkeepInputs, deltaMonths) / deltaMonths;
                }
                else
                {
                    actor.UpkeepPercent = 1;
                }

                actor.ProductionCheckWait -= deltaMonths;

                if (!actor.UnderConstruction && actor.ActiveRecipie != null)
                {
                    if(actor.ProductionCheckWait <= 0 && actor.ProductionProgress <= 0)
                    {
                        actor.ProductionCheckWait = _random.NextDouble() * 0.05;

                        var inputsToPull = actor.ActiveRecipie.Inputs
                            .Select(ir => ir.ToQuantity(1))
                            .ToArray();

                        if (actor.Inventory.HasAll(inputsToPull))
                        {
                            var hasOutputRoom = false;
                            foreach(var output in actor.ActiveRecipie.Outputs)
                            {
                                hasOutputRoom = hasOutputRoom || actor.Inventory.QuantityOf(output.Item) < actor.InventoryCapacity;
                            }

                            if(hasOutputRoom)
                            {
                                actor.Inventory.RemoveAll(inputsToPull);
                                actor.ProductionProgress = deltaMonths;
                                actor.FactoryState = FactoryState.Operating;
                                actor.OnProductionStarted();
                            }
                            else
                            {
                                actor.FactoryState = FactoryState.OutputsFull;
                            }
                        }
                        else
                        {
                            actor.FactoryState = FactoryState.NeedsInputs;
                        }
                    }
                    else if (actor.ProductionProgress > 0)
                    {
                        actor.ProductionProgress += deltaMonths * actor.WorkerPercent;
                        
                        if(actor.ProductionProgress >= 1)
                        {
                            actor.ProductionProgress = 0;
                            actor.ProductionCheckWait = 0;

                            var outputs = actor.ActiveRecipie.BoostedOutputs(actor, actor.NetTraits)
                                .Select(ir => ir.ToQuantity(1))
                                .ToArray();
                            
                            var outputsToProduce = outputs
                                .Select(iq => new ItemQuantity(iq.Item, Math.Min(iq.Quantity, actor.InventoryCapacity - actor.Inventory.QuantityOf(iq.Item))))
                                .Where(iq => iq.Quantity > 0)
                                .ToArray();

                            actor.Inventory.AddAll(outputsToProduce);
                        }
                    }
                }
                
                if (actor.ActiveRecipie == null && !actor.UnderConstruction && actor.AvailableRecipies.Any())
                {
                    actor.FactoryState = FactoryState.NoRecipie;
                }
            }
        }

        private void DoGrowth(Settlement settlement, double deltaMonths)
        {
            if (settlement.Population.Any())
            {
                settlement.AverageSatisfaction = settlement.Population.Average(p => p.Satisfaction);
            }

            foreach (var caste in settlement.Race.Castes)
            {
                var pops = settlement.Population
                    .Where(p => p.Caste == caste)
                    .ToArray();

                var growthRate = 0.0;
                var growthBonus = 1.0;
                
                if (pops.Any())
                {
                    var averageSatisfaction = pops
                        .Select(p => p.Satisfaction)
                        .Average();

                    growthBonus = pops
                        .Select(p => p.GrowthBonus)
                        .Average();

                    settlement.SatisfactionByCaste[caste] = averageSatisfaction;

                    growthRate = Math.Max(0, averageSatisfaction * caste.GrowthRate * growthBonus);
                }
                else
                {
                    var ascendingCastes = settlement.Race.Castes
                        .Where(c => c.AscendsTo.Any(ac => ac == caste))
                        .ToArray();

                    if(ascendingCastes.Any())
                    {
                        growthRate = Math.Max(0, ascendingCastes.Average(ac => settlement.SatisfactionByCaste[ac]) * caste.GrowthRate * growthBonus);
                    }

                    settlement.SatisfactionByCaste[caste] = 0;
                }

                if(settlement.HousingByCaste[caste] > settlement.PopulationByCaste[caste])
                {
                    settlement.GrowthByCaste[caste] = growthRate;

                    var growthCounter = settlement.GrowthCounterByCaste[caste] + growthRate * deltaMonths;

                    if (growthCounter > 1)
                    {
                        IEnumerable<LikelyhoodFor<Cult>> cultLikelyhoods = settlement.Population
                            .Where(p => p.Caste == caste)
                            .GroupBy(p => p.CultMembership)
                            .Select(g => new LikelyhoodFor<Cult>
                            {
                                Value = g.Key,
                                Likelyhood = g.Count()
                            })
                            .ToArray();

                        if(!cultLikelyhoods.Any())
                        {
                            cultLikelyhoods = caste.CultLikelyhoods;
                        }

                        Cult cultMembership = null;
                        if(cultLikelyhoods.Any())
                        {
                            cultMembership = _random.ChooseByLikelyhood(cultLikelyhoods);
                        }



                        growthCounter -= 1;
                        settlement.Population.Add(new Pop(settlement.Race, caste, cultMembership));
                        settlement.SetCalculationFlag("population added");
                    }

                    settlement.GrowthCounterByCaste[caste] = growthCounter;
                }
                else
                {
                    settlement.GrowthByCaste[caste] = 0;
                    settlement.GrowthCounterByCaste[caste] = 0;
                }
            }
        }

        private void DoRecruitmentSlots(Settlement settlement, double deltaMonths)
        {
            foreach(var category in _gamedataTracker.RecruitmentSlotCategories)
            {
                if(!settlement.RecruitmentSlots.ContainsKey(category))
                {
                    settlement.RecruitmentSlots.Add(category, 0);
                }
                if (!settlement.RecruitmentSlotsMax.ContainsKey(category))
                {
                    settlement.RecruitmentSlotsMax.Add(category, 0);
                }

                settlement.RecruitmentSlots[category] = Math.Min(
                    settlement.RecruitmentSlotsMax[category],
                    settlement.RecruitmentSlots[category] + deltaMonths / category.RegenTimeMonths);
            }
        }
    }
}
