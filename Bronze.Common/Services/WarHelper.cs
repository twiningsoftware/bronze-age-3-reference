﻿using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Diplomacy;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.UI;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.Services
{
    public class WarHelper : IWarHelper, IWorldStateService
    {
        private readonly IWorldManager _worldManager;
        private readonly IPlayerDataTracker _playerData;
        private readonly INotificationsSystem _notificationsSystem;
        private readonly IGenericNotificationBuilder _genericNotificationBuilder;
        private readonly ITreatyHelper _treatyHelper;

        private readonly List<War> _wars;

        public IEnumerable<War> AllWars => _wars;

        public WarHelper(
            IWorldManager worldManager,
            IPlayerDataTracker playerData,
            INotificationsSystem notificationsSystem,
            IGenericNotificationBuilder genericNotificationBuilder,
            ITreatyHelper treatyHelper)
        {
            _worldManager = worldManager;
            _playerData = playerData;
            _notificationsSystem = notificationsSystem;
            _genericNotificationBuilder = genericNotificationBuilder;
            _treatyHelper = treatyHelper;
            _wars = new List<War>();
        }

        public bool AreAtWar(Tribe tribeA, Tribe tribeB)
        {
            if(tribeA == tribeB)
            {
                return false;
            }

            return tribeA.CurrentWars.Any(w => w.Aggressor == tribeB || w.Defender == tribeB)
                || tribeB.CurrentWars.Any(w => w.Aggressor == tribeA || w.Defender == tribeA);
        }

        public bool IsAtWar(Tribe tribe)
        {
            return tribe.CurrentWars.Any();
        }

        public void Clear()
        {
            _wars.Clear();
        }

        public void EndWar(War war)
        {
            war.EndDate = _worldManager.Month;

            war.Aggressor.CurrentWars.Remove(war);
            war.Defender.CurrentWars.Remove(war);

            if((war.Aggressor == _playerData.PlayerTribe || war.Defender == _playerData.PlayerTribe)
                && !_playerData.PlayerTribe.CurrentWars.Any())
            {
                var warNotifications = _notificationsSystem.GetActiveNotifications()
                    .Where(n => n.Handler == typeof(WarNotificationHandler).Name)
                    .ToArray();

                foreach(var notification in warNotifications)
                {
                    notification.IsDismissed = true;
                }
            }

            UpdateHostility(war.Aggressor, war.Defender, false);
        }

        public double GetWarScore(Tribe forTribe, Tribe otherTribe)
        {
            var sum = 0.0;
            foreach(var war in forTribe.CurrentWars.Where(w => w.Aggressor == otherTribe || w.Defender == otherTribe))
            {
                if(war.Aggressor == forTribe)
                {
                    sum += war.WarScore;
                }
                else
                {
                    sum -= war.WarScore;
                }
            }

            return sum;
        }

        public void Initialize(WorldState worldState)
        {
        }
        
        public void LoadCustomData(SerializedObject worldRoot)
        {
            var serviceRoot = worldRoot.GetChild("war_helper");
            foreach(var warRoot in serviceRoot.GetChildren("war"))
            {
                var war = War.DeserializeFrom(_worldManager, warRoot);
                _wars.Add(war);

                if(war.IsOngoing)
                {
                    war.Aggressor.CurrentWars.Add(war);
                    war.Defender.CurrentWars.Add(war);

                    CheckForWarNotification(war);

                    UpdateHostility(war.Aggressor, war.Defender, true);
                }
            }
        }

        public void SaveCustomData(SerializedObject worldRoot)
        {
            var serviceRoot = worldRoot.CreateChild("war_helper");

            foreach(var war in _wars)
            {
                war.SerializeTo(serviceRoot.CreateChild("war"));
            }
        }

        public War StartWar(Tribe aggressor, Tribe defender)
        {
            foreach (var treaty in _treatyHelper.GetTreatiesFor(aggressor, defender).ToArray())
            {
                _treatyHelper.RemoveTreaty(treaty);
            }

            var newWar = new War(
                _worldManager.Month,
                aggressor,
                defender);

            aggressor.CurrentWars.Add(newWar);
            defender.CurrentWars.Add(newWar);

            _wars.Add(newWar);

            CheckForWarNotification(newWar);

            newWar.AddEvent(new WarEvent(
                "War Started",
                0,
                _worldManager.Month,
                "warstart",
                null,
                null));

            UpdateHostility(aggressor, defender, true);

            if(aggressor != _playerData.PlayerTribe && defender != _playerData.PlayerTribe
                && (_playerData.PlayerTribe.HasKnowledgeOf(aggressor) || _playerData.PlayerTribe.HasKnowledgeOf(defender)))
            {
                _notificationsSystem.AddNotification(_genericNotificationBuilder
                    .BuildTribeNotification(
                        "ui_notification_diplomacy",
                        "News of War!",
                        $"War between {aggressor.Name} and {defender.Name}",
                        "News of War!",
                        $"There are reports of war between {aggressor.Name} and {defender.Name}",
                        aggressor,
                        defender));
            }

            return newWar;
        }
        
        private void CheckForWarNotification(War war)
        {
            if ((war.Aggressor == _playerData.PlayerTribe || war.Defender == _playerData.PlayerTribe)
                && !_notificationsSystem.GetActiveNotifications().Any(n => n.Handler == typeof(WarNotificationHandler).Name))
            {
                _notificationsSystem.AddNotification(
                    new Notification
                    {
                        AutoOpen = false,
                        ExpireDate = -1,
                        Handler = typeof(WarNotificationHandler).Name,
                        Icon = "ui_notification_war",
                        SoundEffect = UiSounds.NotificationMinor,
                        QuickDismissable = false,
                        DismissOnOpen = false,
                        SummaryTitle = "War!",
                        SummaryBody = "We are at war.",
                        Data = new Dictionary<string, string>()
                    });
            }
        }

        public void RecordRaid(Army army, Cell target, War war)
        {
            var score = war.Aggressor == army.Owner ? 5 : -5;

            war.AddEvent(new WarEvent(
                "Raiding in " + target.Region.Name,
                score,
                _worldManager.Month,
                "raiding",
                null,
                target.Position));

            if(target.Region.Settlement?.Owner == _playerData.PlayerTribe)
            {
                _notificationsSystem
                    .AddNotification(_genericNotificationBuilder.BuildSimpleNotification(
                        "ui_notification_raiding",
                        "Raid!",
                        "Raid in " + target.Region.Name,
                        "Raid in " + target.Region.Name,
                        $"An {army.Owner.Name} army has destroyed our {target.Development?.Name} in {target.Region.Name}.",
                        target.Position));
            }
        }

        private void UpdateHostility(Tribe aggressor, Tribe defender, bool hostile)
        {
            aggressor.SetHostilityTo(defender, hostile);
            defender.SetHostilityTo(aggressor, hostile);
        }
    }
}
