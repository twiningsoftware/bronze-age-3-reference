﻿using Bronze.Common.Data.World.CellDevelopments;
using Bronze.Common.Data.World.Units;
using Bronze.Contracts;
using Bronze.Contracts.Data.Diplomacy;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.Simulation.Events;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.Services
{
    public class BattleHelper : IBattleHelper, IWorldStateService, ISubSimulator
    {
        private readonly IWorldManager _worldManager;
        private readonly IPlayerDataTracker _playerData;
        private readonly INotificationsSystem _notificationsSystem;
        private readonly IWarHelper _warHelper;
        private readonly IUnitHelper _unitHelper;
        private readonly IArmyNotificationBuilder _armyNotificationBuilder;
        private readonly ICombatHelper _combatHelper;
        private readonly ICombatAutoresolver _combatAutoresolver;
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IAiHelper _aiHelper;
        private readonly ISimulationEventBus _simulationEventBus;
        
        private readonly Dictionary<Army, double> _routedTimers;
        private readonly Dictionary<Army, double> _stunnedTimers;
        private Random _random;

        public BattleHelper(
            IWorldManager worldManager,
            IPlayerDataTracker playerData,
            INotificationsSystem notificationsSystem,
            IWarHelper warHelper,
            IUnitHelper unitHelper,
            IArmyNotificationBuilder armyNotificationBuilder,
            ICombatHelper combatHelper,
            ICombatAutoresolver combatAutoresolver,
            IGamedataTracker gamedataTracker,
            IAiHelper aiHelper,
            ISimulationEventBus simulationEventBus)
        {
            _worldManager = worldManager;
            _playerData = playerData;
            _notificationsSystem = notificationsSystem;
            _warHelper = warHelper;
            _unitHelper = unitHelper;
            _armyNotificationBuilder = armyNotificationBuilder;
            _combatHelper = combatHelper;
            _combatAutoresolver = combatAutoresolver;
            _gamedataTracker = gamedataTracker;
            _aiHelper = aiHelper;
            _simulationEventBus = simulationEventBus;
            _routedTimers = new Dictionary<Army, double>();
            _stunnedTimers = new Dictionary<Army, double>();
            _random = new Random();
        }
        
        public void Clear()
        {
            _routedTimers.Clear();
            _stunnedTimers.Clear();
        }
        
        public void Initialize(WorldState worldState)
        {
            _random = new Random(worldState.Seed.GetHashCode());
        }
        
        public void LoadCustomData(SerializedObject worldRoot)
        {
            var serviceRoot = worldRoot.GetChild("battle_helper");

            var allSettlements = _worldManager
                .Tribes
                .SelectMany(t => t.Settlements)
                .ToArray();
            
            var allArmies = _worldManager.Tribes
                .SelectMany(t => t.Armies)
                .ToDictionary(t => t.Id);
            
            foreach (var routedRoot in serviceRoot.GetChildren("routed"))
            {
                var armyId = routedRoot.GetString("army_id");
                
                if (allArmies.ContainsKey(armyId))
                {
                    _routedTimers.Add(allArmies[armyId], routedRoot.GetDouble("timer"));
                }
            }

            foreach (var stunnedRoot in serviceRoot.GetChildren("stunned"))
            {
                var armyId = stunnedRoot.GetString("army_id");

                if (allArmies.ContainsKey(armyId))
                {
                    _stunnedTimers.Add(allArmies[armyId], stunnedRoot.GetDouble("timer"));
                }
            }
        }

        public void SaveCustomData(SerializedObject worldRoot)
        {
            var serviceRoot = worldRoot.CreateChild("battle_helper");
            
            foreach (var kvp in _routedTimers)
            {
                var routedRoot = serviceRoot.CreateChild("routed");
                routedRoot.Set("army_id", kvp.Key.Id);
                routedRoot.Set("timer", kvp.Value);
            }

            foreach (var kvp in _stunnedTimers)
            {
                var stunnedRoot = serviceRoot.CreateChild("stunned");
                stunnedRoot.Set("army_id", kvp.Key.Id);
                stunnedRoot.Set("timer", kvp.Value);
            }
        }

        public void StepSimulation(double elapsedMonths)
        {
            var allArmies = _worldManager.Tribes
                .SelectMany(t => t.Armies)
                .ToArray();
            
            foreach (var army in _routedTimers.Keys.ToArray())
            {
                _routedTimers[army] -= elapsedMonths;

                army.IsLocked = true;

                if (_routedTimers[army] <= 0 || !army.Units.Any())
                {
                    army.IsLocked = false;
                    _routedTimers.Remove(army);
                }
                else
                {
                    if (!(army.CurrentOrder is ArmyRecoverOrder recoverOrder) || recoverOrder.Target == army.Cell)
                    {
                        var nearestEnemy = army.Cell.Yield()
                            .Concat(army.Cell.Neighbors)
                            .SelectMany(c => c.WorldActors.OfType<Army>())
                            .Where(a => a.Units.Any() && a.Owner.IsHostileTo(army.Owner))
                            .OrderBy(a => (army.Position - a.Position).Length())
                            .FirstOrDefault();

                        var runCell = army.Cell.GetNeighbor(army.Facing);

                        if (nearestEnemy != null)
                        {
                            runCell = army.Cell.Yield()
                                .Concat(army.Cell.OrthoNeighbors)
                                .Where(c => army.CanPath(c))
                                .OrderByDescending(c => (c.Position.ToVector() - nearestEnemy.Position).Length())
                                .FirstOrDefault();
                        }
                        
                        if (runCell == null || !army.CanPath(runCell))
                        {
                            var nextCells = army.Cell.OrthoNeighbors.Where(c => army.CanPath(c)).ToArray();

                            if (nextCells.Any())
                            {
                                runCell = _random.Choose(nextCells);
                            }
                        }

                        if (runCell != null)
                        {
                            if(army.CurrentOrder is ArmyRecoverOrder armyRecoverOrder)
                            {
                                armyRecoverOrder.Target = runCell;
                                armyRecoverOrder.IsRouting = true;

                            }
                            else if (army.CurrentOrder is ArmySkirmishOrder skirmishOrder)
                            {
                                army.CurrentOrder = new ArmyRecoverOrder(army, true, skirmishOrder.FallbackOrder, _routedTimers[army])
                                {
                                    Target = runCell
                                };
                            }
                            else
                            {
                                army.CurrentOrder = new ArmyRecoverOrder(army, true, army.CurrentOrder, _routedTimers[army])
                                {
                                    Target = runCell
                                };
                            }
                        }
                    }
                }
            }

            foreach (var army in _stunnedTimers.Keys.ToArray())
            {
                _stunnedTimers[army] -= elapsedMonths;
                
                if (_stunnedTimers[army] <= 0 || !army.Units.Any())
                {
                    army.IsLocked = false;
                    _stunnedTimers.Remove(army);
                }
                else
                {
                    army.IsLocked = true;
                    
                    if (army.CurrentOrder is ArmyRecoverOrder armyRecoverOrder)
                    {
                        armyRecoverOrder.IsRouting = false;
                        armyRecoverOrder.Target = army.Cell;

                    }
                    else if (army.CurrentOrder is ArmySkirmishOrder skirmishOrder)
                    {
                        army.CurrentOrder = new ArmyRecoverOrder(army, false, skirmishOrder.FallbackOrder, _stunnedTimers[army]);
                    }
                    else
                    {
                        army.CurrentOrder = new ArmyRecoverOrder(army, false, army.CurrentOrder, _stunnedTimers[army]);
                    }
                }
            }
        }

        public void RunBattle(Army sourceArmy)
        {
            var nearbyArmies = sourceArmy.Cell.Yield()
                .Concat(sourceArmy.Cell.Neighbors)
                .SelectMany(c => c.WorldActors.OfType<Army>())
                .Where(a => (sourceArmy.Position - a.Position).Length() <= Constants.DIST_SKIRMISH_MAX)
                .Distinct()
                .ToArray();

            var enemyArmies = nearbyArmies
                .Where(a => sourceArmy.Owner.IsHostileTo(a.Owner))
                .ToArray();

            var allyArmies = nearbyArmies
                .Where(a => !sourceArmy.Owner.IsHostileTo(a.Owner) && enemyArmies.Any(ea => a.Owner.IsHostileTo(ea.Owner)))
                .ToArray();

            var engagedArmies = allyArmies.Concat(enemyArmies).ToArray();

            var battleResult = _combatAutoresolver.AutoResolveCombat(sourceArmy.Cell, allyArmies, enemyArmies, fixedSeed: null);

            foreach (var army in engagedArmies)
            {
                foreach (var unit in army.Units.ToArray())
                {
                    if (unit.Health <= 0)
                    {
                        _unitHelper.RemoveUnit(army, unit, removedViolently: true);
                    }
                }
            }

            var remainingAttackers = battleResult.RemainingArmies.Intersect(allyArmies).ToArray();
            var remainingEnemies = battleResult.RemainingArmies.Intersect(enemyArmies).ToArray();


            var remainingAttackerStrength = remainingAttackers.Sum(a => a.StrengthEstimate);
            var remainingDefenderStrength = remainingEnemies.Sum(a => a.StrengthEstimate);
            
            SetDestroyed(battleResult.DestroyedArmies, wasTrapped: false);
            SetRouted(battleResult.RoutingArmies, 1);

            Army[] actualRemainingArmies;
            Army[] withdrawingArmies;

            if(remainingAttackerStrength >= remainingDefenderStrength)
            {
                actualRemainingArmies = remainingAttackers;
                withdrawingArmies = remainingEnemies;
            }
            else
            {
                actualRemainingArmies = remainingEnemies;
                withdrawingArmies = remainingAttackers;
            }

            SetRouted(withdrawingArmies, 0.5);
            SetStunned(actualRemainingArmies);

            foreach (var army in actualRemainingArmies)
            {
                foreach(var unit in army.Units)
                {
                    unit.SetInstantAnimation(army, "skirmishing".Yield());
                }
            }
            
            battleResult.Name = "Battle of " + sourceArmy.Cell.Region.Name;
            
            if (engagedArmies.Any(a => a.Owner == _playerData.PlayerTribe))
            {
                _notificationsSystem.AddNotification(_armyNotificationBuilder.BuildBattleNotification(battleResult.Name, sourceArmy.Cell, battleResult));
            }

            _simulationEventBus.SendEvent(new BattleOccurredSimulationEvent(battleResult));

            var wars = _warHelper.AllWars
                .Where(w => w.IsOngoing)
                .Where(w => (allyArmies.Any(a => w.Aggressor == a.Owner) && enemyArmies.Any(a => w.Defender == a.Owner))
                    || (enemyArmies.Any(a => w.Aggressor == a.Owner) && allyArmies.Any(a => w.Defender == a.Owner)))
                .ToArray();

            foreach (var war in wars)
            {
                var score = battleResult.AttackerScore - battleResult.DefenderScore;

                if(enemyArmies.Any(a => war.Aggressor == a.Owner) && allyArmies.Any(a => war.Defender == a.Owner))
                {
                    score = battleResult.DefenderScore - battleResult.AttackerScore;
                }


                war.AddEvent(new WarEvent(
                    battleResult.Name,
                    score,
                    _worldManager.Month,
                    "battle",
                    string.Empty,
                    sourceArmy.Cell.Position));
            }
        }

        public void DoSkirmishing(Army attackingArmy, Army targetArmy)
        {
            var attackers = attackingArmy.Units.ToList();

            if (!attackingArmy.Units.Any() || !targetArmy.Units.Any())
            {
                return;
            }

            _random.Shuffle(attackers);

            foreach (var attacker in attackers)
            {
                if (targetArmy.Units.Any())
                {
                    var target = _random.Choose(targetArmy.Units);
                    
                    ResolveSkirmishAttack(attacker, target, targetArmy, attackingArmy);
                }
            }
        }

        private void ResolveSkirmishAttack(
            IUnit attacker,
            IUnit target,
            Army targetArmy,
            Army sourceArmy)
        {
            var random = new Random();
            var hitChance = attacker.SkirmishSkill * 0.05;

            var attackingIndividuals = Math.Ceiling(attacker.HealthPercent * attacker.IndividualCount);

            var beforeHealthPerc = target.HealthPercent;

            for (var i = 0; i < attackingIndividuals; i++)
            {
                if (target.Health > 0)
                {
                    if (_random.NextDouble() < hitChance)
                    {
                        attacker.SetInstantAnimation(sourceArmy, "skirmishing".Yield());

                        foreach (var attack in attacker.RangedAttacks)
                        {
                            var damage = _combatHelper.ResolveRangedAttack(
                                random,
                                attack.Skill,
                                target.BlockSkill,
                                attack.Damage,
                                target.Armor,
                                attack.ArmorPenetration);

                            target.Health -= damage;
                        }

                        foreach (var attack in attacker.MeleeAttacks)
                        {
                            var damage = _combatHelper.ResolveMeleeAttack(
                                random,
                                attack.Skill,
                                target.DefenseSkill,
                                target.BlockSkill,
                                attack.Damage,
                                target.Armor,
                                attack.ArmorPenetration);

                            target.Health -= damage;
                        }
                    }

                    if (target.Health <= 0)
                    {
                        _unitHelper.RemoveUnit(targetArmy, target, removedViolently: true);
                    }
                }
            }
        }

        private void SetDestroyed(IEnumerable<Army> armies, bool wasTrapped)
        {
            foreach(var army in armies)
            {
                var generalDied = false;
                if(army.General != null && _random.Next() < 0.5)
                {
                    generalDied = true;
                    army.General.IsDead = true;
                    _simulationEventBus.SendEvent(new NotablePersonDiedSimulationEvent(army.General));
                }

                _unitHelper.RemoveArmy(army);

                if(army.Owner == _playerData.PlayerTribe)
                {
                    _notificationsSystem
                        .AddNotification(_armyNotificationBuilder
                            .BuildArmyDestroyedNotification(army, army.Cell.Position, generalDied, wasTrapped));
                }
            }
        }

        public void SetStunned(IEnumerable<Army> armies)
        {
            foreach(var army in armies)
            {
                army.IsLocked = true;

                if (_stunnedTimers.ContainsKey(army))
                {
                    _stunnedTimers[army] = 0.35;
                }
                else
                {
                    _stunnedTimers.Add(army, 0.35);
                }
            }
        }

        public void SetRouted(IEnumerable<Army> routingArmies, double durationFactor)
        {
            foreach (var army in routingArmies)
            {
                army.IsLocked = true;

                if (_routedTimers.ContainsKey(army))
                {
                    _routedTimers[army] = 0.75 * durationFactor;
                }
                else
                {
                    _routedTimers.Add(army, 0.75 * durationFactor);
                }
            }
        }

        public void DoSiegeAssault(Settlement settlement, Tribe initializer)
        {
            var settlementLocation = settlement.EconomicActors
                .OfType<ICellDevelopment>()
                .Where(cd => cd is VillageDevelopment || cd is DistrictDevelopment)
                .Select(cd => cd.Cell)
                .First();

            // Recruit all levies
            var tempDefenders = new List<Army>();
            var nextArmy = new Army(settlement.Owner, settlementLocation, _worldManager);
            foreach(var recruitCategory in _gamedataTracker.RecruitmentSlotCategories)
            {
                var recruits = settlement.RecruitmentSlots[recruitCategory];
                for(var i = 0; i < recruits; i++)
                {
                    _aiHelper.RecruitWarriors(settlement, nextArmy);

                    if(nextArmy.IsFull)
                    {
                        tempDefenders.Add(nextArmy);
                        nextArmy = new Army(settlement.Owner, settlementLocation, _worldManager);
                    }
                }
            }

            if(nextArmy.Units.Any())
            {
                tempDefenders.Add(nextArmy);
            }
            else
            {
                _unitHelper.RemoveArmy(nextArmy);
            }

            if(settlement.Governor != null && tempDefenders.Any())
            {
                settlement.Governor.AssignTo(tempDefenders.First());
            }

            var nearbyArmies = settlementLocation.Yield()
                .Concat(settlementLocation.Neighbors)
                .SelectMany(c => c.WorldActors.OfType<Army>())
                .Where(a => (settlementLocation.Position.ToVector() - a.Position).Length() <= Constants.DIST_SKIRMISH_MAX)
                .Concat(settlement.SiegedBy)
                .Distinct()
                .ToArray();

            var enemyArmies = nearbyArmies
                .Where(a => settlement.Owner.IsHostileTo(a.Owner))
                .ToArray();

            var allyArmies = nearbyArmies
                .Where(a => !settlement.Owner.IsHostileTo(a.Owner) && enemyArmies.Any(ea => a.Owner.IsHostileTo(ea.Owner)))
                .ToArray();

            var engagedArmies = allyArmies.Concat(enemyArmies).ToArray();

            var battleResult = _combatAutoresolver.AutoResolveCombat(settlementLocation, enemyArmies, allyArmies, fixedSeed: null);

            foreach (var army in engagedArmies)
            {
                foreach (var unit in army.Units.ToArray())
                {
                    if (unit.Health <= 0)
                    {
                        _unitHelper.RemoveUnit(army, unit, removedViolently: true);
                    }
                }
            }
            
            var settlementCaptured = enemyArmies.Any(a => a.Owner == initializer) && battleResult.AttackerWon;

            if(settlementCaptured)
            {
                SetDestroyed(battleResult.DestroyedArmies, wasTrapped: false);
                SetRouted(battleResult.RoutingArmies, 1);
                SetStunned(battleResult.RemainingArmies);
            }
            else
            {
                SetDestroyed(battleResult.DestroyedArmies, wasTrapped: false);
                SetRouted(battleResult.RoutingArmies.Except(allyArmies), 1);
            }

            var battleName = "Battle of " + settlement.Name;

            if (engagedArmies.Any(a => a.Owner == _playerData.PlayerTribe))
            {
                var playerAttacker = enemyArmies.Any(a => a.Owner == _playerData.PlayerTribe);

                _notificationsSystem.AddNotification(
                    _armyNotificationBuilder
                        .BuildSiegeAssaultNotification(
                            battleName,
                            settlementLocation,
                            battleResult,
                            settlementCaptured,
                            playerAttacker,
                            settlement));
            }

            var oldSettlementOwner = settlement.Owner;

            if (settlementCaptured)
            {
                _simulationEventBus.SendEvent(new SettlementOwnerChangedSimulationEvent
                {
                    OldOwner = oldSettlementOwner,
                    NewOwner = initializer,
                    Settlement = settlement
                });
                settlement.ChangeOwner(initializer);
            }

            battleResult.Name = battleName;

            _simulationEventBus.SendEvent(new BattleOccurredSimulationEvent(battleResult));

            var allyTribes = allyArmies.Select(a => a.Owner).Concat(oldSettlementOwner.Yield()).Distinct().ToArray();
            var enemyTribes = enemyArmies.Select(a => a.Owner).Distinct().ToArray();
            
            var wars = _warHelper.AllWars
                .Where(w => w.IsOngoing)
                .Where(w => (allyTribes.Any(t => w.Aggressor == t) && enemyTribes.Any(t => w.Defender == t))
                    || (enemyTribes.Any(t => w.Aggressor == t) && allyTribes.Any(t => w.Defender == t)))
                .ToArray();

            foreach (var war in wars)
            {
                var score = battleResult.AttackerScore - battleResult.DefenderScore;
                
                if (enemyArmies.Any(a => war.Aggressor == a.Owner) && allyArmies.Any(a => war.Defender == a.Owner))
                {
                    score = battleResult.DefenderScore - battleResult.AttackerScore;
                }
                
                war.AddEvent(new WarEvent(
                    battleName,
                    score,
                    _worldManager.Month,
                    "battle",
                    string.Empty,
                    settlementLocation.Position));

                if(settlementCaptured)
                {
                    var captureScore = 0;

                    if(oldSettlementOwner == war.Aggressor)
                    {
                        captureScore = -1 * settlement.Population.Count;
                    }
                    else if (oldSettlementOwner == war.Defender)
                    {
                        captureScore = settlement.Population.Count;
                    }

                    if (captureScore != 0)
                    {
                        war.AddEvent(new WarEvent(
                            settlement.Name + " Captured",
                            captureScore,
                            _worldManager.Month,
                            "capture",
                            string.Empty,
                            settlementLocation.Position));
                    }
                }
            }


            // Disband the temporarily recruited armies
            if (tempDefenders.Any() 
                && tempDefenders.First().General != null 
                && !tempDefenders.First().General.IsDead
                && !settlementCaptured)
            {
                tempDefenders.First().General.AssignTo(settlement);
            }

            foreach(var army in tempDefenders)
            {
                foreach(var unit in army.Units.ToArray())
                {
                    _unitHelper.Disband(army, unit);
                }

                _unitHelper.RemoveArmy(army);
            }
        }
    }
}
