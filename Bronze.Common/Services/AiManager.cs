﻿using Bronze.Contracts;
using Bronze.Contracts.Data.Diplomacy;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Bronze.Common.Services
{
    public class AiManager : AbstractBackgroundCalculatorSubSimulator, IWorldStateService, ISubSimulator
    {
        private static readonly object LOCK = new object();
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IWorldManager _worldManager;
        private readonly List<Action> _results;
        private Stopwatch _timeSinceCalc;

        private Queue<Tribe> _deepCaculationQueue;
        
        private int _nextIndex;

        public AiManager(
            IGameInterface gameInterface,
            IGamedataTracker gamedataTracker,
            IWorldManager worldManager)
            :base(gameInterface)
        {
            _gamedataTracker = gamedataTracker;
            _worldManager = worldManager;
            _nextIndex = 0;

            _deepCaculationQueue = new Queue<Tribe>();
            _results = new List<Action>();

            _timeSinceCalc = Stopwatch.StartNew();
        }

        public void Clear()
        {
            _nextIndex = 0;
            _deepCaculationQueue.Clear();
            lock(LOCK)
            {
                _results.Clear();
            }
            _timeSinceCalc.Restart();
        }

        public void Initialize(WorldState worldState)
        {
        }

        public void LoadCustomData(SerializedObject worldRoot)
        {
        }
        
        public void SaveCustomData(SerializedObject worldRoot)
        {
        }
        
        public override void StepSimulation(double elapsedMonths)
        {
            base.StepSimulation(elapsedMonths);

            if (_worldManager.Tribes.Any())
            {
                _nextIndex = _nextIndex % _worldManager.Tribes.Count;
                
                var tribe = _worldManager.Tribes[_nextIndex];

                if (!tribe.IsExtinct)
                {
                    tribe.Controller.DoFastAI();
                    
                    lock (LOCK)
                    {
                        if (!_deepCaculationQueue.Contains(tribe))
                        {
                            _deepCaculationQueue.Enqueue(tribe);
                        }
                    }
                }

                _nextIndex += 1;
            }
        }
        
        protected override void ApplyResults()
        {
            Action[] results;
            lock(LOCK)
            {
                results = _results.ToArray();
                _results.Clear();
            }

            foreach(var result in results)
            {
                result.Invoke();
            }
        }

        protected override void StartCalculations()
        {

            if (_timeSinceCalc.ElapsedMilliseconds > 2000)
            {

                Tribe aiTribe = null;
                
                lock (LOCK)
                {
                    if (_deepCaculationQueue.Any())
                    {
                        aiTribe = _deepCaculationQueue.Dequeue();
                    }
                }

                if (aiTribe != null)
                {
                    var results = new List<Action>();

                    results.AddRange(aiTribe.Controller.DoDeepAI());

                    lock (LOCK)
                    {
                        _results.AddRange(results);
                    }
                }
                _timeSinceCalc.Restart();
            }
        }
    }
}
