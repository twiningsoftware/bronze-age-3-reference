﻿using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.AI;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.Services
{
    public class DistrictGenerator : AbstractBackgroundCalculatorSubSimulator, IDistrictGenerator
    {
        private static readonly object LOCK = new object();

        private readonly IGameInterface _gameInterface;
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IWorldManager _worldManager;
        private readonly ICityPrefabPlacer _cityPrefabPlacer;

        private List<District> _generationQueue;
        private List<GenerationResult> _generationResults;
        private List<District> _previouslyQueued;

        public DistrictGenerator(
            IGameInterface gameInterface,
            IGamedataTracker gamedataTracker,
            IWorldManager worldManager,
            ICityPrefabPlacer cityPrefabPlacer)
                :base(gameInterface)
        {
            _gameInterface = gameInterface;
            _gamedataTracker = gamedataTracker;
            _worldManager = worldManager;
            _cityPrefabPlacer = cityPrefabPlacer;
            _generationQueue = new List<District>();
            _generationResults = new List<GenerationResult>();
            _previouslyQueued = new List<District>();
        }

        public void QueueForGeneration(District district)
        {
            lock (LOCK)
            {
                if (!_previouslyQueued.Contains(district))
                {
                    _previouslyQueued.Add(district);
                    _generationQueue.Add(district);
                }
            }
        }

        protected override void ApplyResults()
        {
            IEnumerable<GenerationResult> results;
            lock (LOCK)
            {
                results = _generationResults.ToArray();
                _generationResults.Clear();
            }

            foreach (var result in results)
            {
                result.District.Tiles = result.Tiles;
                result.District.IsGenerated = true;

                GenUtils.LinkTilesToNeighbors(result.District.Settlement);
                result.District.Settlement.SetCalculationFlag("district generated");
                
                _cityPrefabPlacer.QueueForPlacement(result.District.Settlement, new[] { result.StartingPrefab }, true, new CityPrefab[0]);
            }
        }
        
        protected override void StartCalculations()
        {
            Queue<District> queue;
            lock (LOCK)
            {
                queue = new Queue<District>(_generationQueue);
                _generationQueue.Clear();
            }

            while (queue.Any())
            {
                var current = queue.Dequeue();
                var results = new GenerationResult
                {
                    District = current,
                    Tiles = new Tile[TilePosition.TILES_PER_CELL, TilePosition.TILES_PER_CELL]
                };

                var noiseGen = new SimplexNoiseGenerator(_worldManager.Seed.GetHashCode());
                var subNoiseGen = new SimplexNoiseGenerator((_worldManager.Seed + "subnoise").GetHashCode());
                var randomForDistrict = new Random(current.Cell.Position.GetHashCode());

                var tilesToBorderLayer = new DistrictGenerationLayer[TilePosition.TILES_PER_CELL, TilePosition.TILES_PER_CELL];
                var borderLayersPresent = new List<DistrictGenerationLayer>();

                for (var x = 0; x < TilePosition.TILES_PER_CELL; x++)
                {
                    for (var y = 0; y < TilePosition.TILES_PER_CELL; y++)
                    {
                        var tilePosition = new TilePosition(current.Cell, x, y);
                        Cell ul, ur, ll, lr;
                        float a, b;

                        var newTile = new Tile(tilePosition)
                        {
                            District = current,
                            Terrain = _gamedataTracker.Terrain.First(),
                            Decoration = null
                        };

                        results.Tiles[x, y] = newTile;

                        var noise = noiseGen.CoherentNoise(
                            tilePosition.ToVector().X,
                            tilePosition.ToVector().Y,
                            current.Cell.Position.Plane * 1000,
                            octaves: 1,
                            multiplier: 15,
                            amplitude: 1.5f);

                        var halfDistrict = TilePosition.TILES_PER_CELL / 2f;

                        if (x < TilePosition.TILES_PER_CELL / 2)
                        {
                            if (y < TilePosition.TILES_PER_CELL / 2)
                            {
                                a = x / halfDistrict + 0f + noise;
                                b = y / halfDistrict + 0f + noise;
                                ul = current.Cell.GetNeighbor(Facing.NorthWest) ?? current.Cell;
                                ur = current.Cell.GetNeighbor(Facing.North) ?? current.Cell;
                                ll = current.Cell.GetNeighbor(Facing.West) ?? current.Cell;
                                lr = current.Cell;

                                if (ur.Biome == ll.Biome && ll.Biome == current.Cell.Biome)
                                {
                                    ul = current.Cell;
                                }
                            }
                            else
                            {
                                a = x / halfDistrict + 0f + noise;
                                b = y / halfDistrict - 1f - noise;
                                ul = current.Cell.GetNeighbor(Facing.West) ?? current.Cell;
                                ur = current.Cell;
                                ll = current.Cell.GetNeighbor(Facing.SouthWest) ?? current.Cell;
                                lr = current.Cell.GetNeighbor(Facing.South) ?? current.Cell;

                                if (ul.Biome == lr.Biome && lr.Biome == current.Cell.Biome)
                                {
                                    ll = current.Cell;
                                }
                            }
                        }
                        else
                        {
                            if (y < TilePosition.TILES_PER_CELL / 2)
                            {
                                a = x / halfDistrict - 1f - noise;
                                b = y / halfDistrict + 0f + noise;
                                ul = current.Cell.GetNeighbor(Facing.North) ?? current.Cell;
                                ur = current.Cell.GetNeighbor(Facing.NorthEast) ?? current.Cell;
                                ll = current.Cell;
                                lr = current.Cell.GetNeighbor(Facing.East) ?? current.Cell;

                                if (ul.Biome == lr.Biome && lr.Biome == current.Cell.Biome)
                                {
                                    ur = current.Cell;
                                }
                            }
                            else
                            {
                                a = x / halfDistrict - 1f - noise;
                                b = y / halfDistrict - 1f - noise;
                                ul = current.Cell;
                                ur = current.Cell.GetNeighbor(Facing.East) ?? current.Cell;
                                ll = current.Cell.GetNeighbor(Facing.South) ?? current.Cell;
                                lr = current.Cell.GetNeighbor(Facing.SouthEast) ?? current.Cell;

                                if (ur.Biome == ll.Biome && ll.Biome == current.Cell.Biome)
                                {
                                    lr = current.Cell;
                                }
                            }
                        }


                        var layerNumbers = new[] { ul, ll, ur, lr }
                            .SelectMany(c => c.DistrictGenerationLayers)
                            .Select(l => l.Layer)
                            .Distinct()
                            .OrderBy(i => i)
                            .ToArray();

                        foreach (var layerNumber in layerNumbers)
                        {
                            GenerateTile(
                                newTile,
                                layerNumber,
                                ul, ur, ll, lr, a, b,
                                subNoiseGen,
                                randomForDistrict,
                                tilesToBorderLayer,
                                borderLayersPresent);
                        }
                    }
                }

                GenUtils.LinkTilesToNeighbors(results.Tiles);
                
                PlaceBorders(results, borderLayersPresent, tilesToBorderLayer, randomForDistrict);

                if (current.Cell.Feature != null)
                {
                    current.Cell.Feature.ApplyGeneration(_worldManager, current, results.Tiles);
                }


                results.StartingPrefab = results.District.Settlement.Race.CityPrefabs
                    .Where(p => p.IsCityStarter)
                    .FirstOrDefault();
                
                lock (LOCK)
                {
                    _generationResults.Add(results);
                }
            }
        }
        
        private void GenerateTile(
            Tile newTile,
            float layerNumber,
            Cell ul,
            Cell ur,
            Cell ll,
            Cell lr,
            float a,
            float b,
            SimplexNoiseGenerator subNoiseGen,
            Random random,
            DistrictGenerationLayer[,] tilesToLayer,
            List<DistrictGenerationLayer> layersPresent)
        {
            var cellWeights = new[]
                {
                    Tuple.Create(ul, (1-a) * (1-b)),
                    Tuple.Create(ur, a * (1-b)),
                    Tuple.Create(ll, (1-a) * b),
                    Tuple.Create(lr, a * b),
                }
                .ToArray();

            var layerWeights = new List<Tuple<DistrictGenerationLayer, float, Cell>>();
            foreach (var cellWeight in cellWeights)
            {
                var layers = cellWeight.Item1.DistrictGenerationLayers
                    .Where(l => l.Layer == layerNumber)
                    .ToArray();

                foreach (var layer in layers)
                {
                    var layerWeight = cellWeight.Item2 + layer.Weight;

                    if (layer.UsesNoise)
                    {
                        var mutatedX = (ul.Position.X + a) * TilePosition.TILES_PER_CELL;
                        var mutatedY = (ul.Position.Y + b) * TilePosition.TILES_PER_CELL;

                        var subNoise = subNoiseGen.CoherentNoise(
                            mutatedX,
                            mutatedY,
                            ul.Position.Plane * 1000,
                            octaves: layer.NoiseOctaves,
                            multiplier: layer.NoiseMultiplier,
                            amplitude: 1.5f);

                        if (layer.NoiseThresholdMin <= subNoise && subNoise <= layer.NoiseThresholdMax)
                        {
                            layerWeight += subNoise;
                        }
                        else
                        {
                            layerWeight = float.MinValue;
                        }
                    }

                    layerWeights.Add(Tuple.Create(layer, layerWeight, cellWeight.Item1));
                }
            }

            layerWeights = layerWeights
                .Where(l => l.Item2 >= 0)
                .OrderByDescending(l => l.Item2)
                .ToList();

            if (layerWeights.Any())
            {
                var winner = layerWeights[0];
                var winnerScore = layerWeights[0].Item2;

                var winningLayer = layerWeights[0].Item1;

                if (winningLayer.Border != null)
                {
                    if (!layersPresent.Contains(winningLayer))
                    {
                        layersPresent.Add(winningLayer);
                    }

                    if (winningLayer.TerrainOptions.Any())
                    {
                        tilesToLayer[newTile.Position.X, newTile.Position.Y] = winningLayer;
                    }
                }

                if (layerWeights.Count > 1)
                {
                    var secondPlace = layerWeights[1];
                    var secondPlaceScore = layerWeights[1].Item2;

                    var transition = winner.Item1.Transitions
                        .Where(t => secondPlace.Item3.Biome.AdjacencyGroups.Contains(t.ToAdjacency))
                        .FirstOrDefault();

                    if (transition != null && winnerScore - secondPlaceScore <= transition.Threshold)
                    {
                        newTile.Terrain = transition.Terrain;
                        SetDecoration(newTile, random, winner.Item1);
                    }
                    else
                    {
                        SetTerrain(newTile, random, winner.Item1);
                        SetDecoration(newTile, random, winner.Item1);
                    }
                }
                else
                {
                    SetTerrain(newTile, random, winner.Item1);
                    SetDecoration(newTile, random, winner.Item1);
                }
            }
        }

        private void SetTerrain(Tile newTile, Random random, DistrictGenerationLayer winner)
        {
            if (winner.TerrainOptions.Any())
            {
                newTile.Terrain = random.Choose(winner.TerrainOptions);
                newTile.Decoration = null;
            }
        }

        private void SetDecoration(Tile newTile, Random random, DistrictGenerationLayer winner)
        {
            var decorationLikelyhoods = new List<LikelyhoodFor<Decoration>>();

            decorationLikelyhoods.Add(new LikelyhoodFor<Decoration>
            {
                Value = null,
                Likelyhood = (int)(winner.NoDecorationWeight * 10000)
            });

            decorationLikelyhoods
                .AddRange(winner.DecorationOptions
                .Where(d => d.AcceptableTerrain.Contains(newTile.Terrain))
                .Select(d => new LikelyhoodFor<Decoration>
                {
                    Value = d.Decoration,
                    Likelyhood = (int)(d.Weight * 10000)
                }));

            newTile.Decoration = random.ChooseByLikelyhood(decorationLikelyhoods);
        }

        private void PlaceBorders(
            GenerationResult results, 
            List<DistrictGenerationLayer> borderLayersPresent, 
            DistrictGenerationLayer[,] tilesToBorderLayer,
            Random random)
        {
            if (borderLayersPresent.Any())
            {
                foreach (var layer in borderLayersPresent)
                {
                    var borderTiles = new List<Tile>();
                    var interruptionTiles = new List<Tile>();

                    for (var x = 0; x < TilePosition.TILES_PER_CELL; x++)
                    {
                        for (var y = 0; y < TilePosition.TILES_PER_CELL; y++)
                        {
                            if (tilesToBorderLayer[x, y] == layer)
                            {
                                var isBorder = false;

                                for (var i = -1; i < 2 && !isBorder; i++)
                                {
                                    for (var j = -1; j < 2 && !isBorder; j++)
                                    {
                                        var nx = x + i;
                                        var ny = y + j;

                                        if (i != 0 && j != 0
                                            && nx >= 0 && nx < TilePosition.TILES_PER_CELL
                                            && ny >= 0 && ny < TilePosition.TILES_PER_CELL)
                                        {
                                            isBorder = isBorder || tilesToBorderLayer[nx, ny] != layer;

                                        }
                                    }
                                }

                                if (isBorder)
                                {
                                    borderTiles.Add(results.Tiles[x, y]);
                                }
                            }
                        }
                    }

                    if (borderTiles.Any())
                    {
                        var interruptions = borderTiles.Count * layer.Border.InterruptionsPerTile;

                        for (var i = 0; i < interruptions; i++)
                        {
                            interruptionTiles.Add(random.Choose(borderTiles));
                        }

                        foreach(var interruptionTile in interruptionTiles.Distinct())
                        {
                            var range = random.Next(layer.Border.InterruptionMinLength, layer.Border.InterruptionMaxLength);

                            borderTiles.RemoveAll(t => t.Position.Distance(interruptionTile.Position) <= range);
                        }
                    }

                    var tooShort = new List<Tile>();
                    foreach(var tile in borderTiles)
                    {
                        var flood = Util.FloodFill(tile, t => t.OrthoNeighbors.Where(n => borderTiles.Contains(n)));

                        if(flood.Count() < layer.Border.MinimumLength)
                        {
                            tooShort.Add(tile);
                        }
                    }
                    
                    foreach (var tile in borderTiles.Except(tooShort))
                    {
                        tile.Terrain = layer.Border.Terrain;
                        tile.Decoration = null;
                    }
                }
            }
        }
        
        private class GenerationResult
        {
            public District District { get; set; }
            public Tile[,] Tiles { get; set; }
            public CityPrefab StartingPrefab { get; set; }
        }
    }
}
