﻿using Bronze.Common.UI.Modals;
using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.Services
{
    public class ArmyEngagedNotificationHandler : INotificationHandler
    {
        private readonly IDialogManager _dialogManager;

        public ArmyEngagedNotificationHandler(IDialogManager dialogManager)
        {
            _dialogManager = dialogManager;
        }

        public string Name => typeof(ArmyEngagedNotificationHandler).Name;

        public void Handle(Notification notification)
        {
            _dialogManager.PushModal<ArmiesEngagedModal>();
        }

        public void HandleIgnored(Notification notification)
        {
        }
    }
}
