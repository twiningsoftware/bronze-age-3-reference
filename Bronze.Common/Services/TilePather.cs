﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Priority_Queue;

namespace Bronze.Common.Services
{
    public class TilePather : ITilePather
    {
        private static readonly Facing[] ORTHO_FACINGS = new[] { Facing.North, Facing.South, Facing.East, Facing.West };

        public IEnumerable<Tile> DeterminePath(
            Tile start, 
            Tile goal, 
            Settlement settlement,
            Func<Tile, bool> isValid,
            bool penalizeTurning = false,
            float abortFactor = 2f,
            int stepSize = 1)
        {
            if(start == null || goal == null || !isValid(start) || !isValid(goal))
            {
                return Enumerable.Empty<Tile>();
            }

            var open = new SimplePriorityQueue<Tile, double>();
            var closed = new HashSet<Tile>();
            var distTo = new Dictionary<Tile, double>();
            var directionTo = new Dictionary<Tile, Facing?>();
            var estimatedTo = new Dictionary<Tile, double>();
            var cameFrom = new Dictionary<Tile, Tile>();

            var estimatedToGoal = start.Position.Distance(goal.Position);

            distTo.Add(start, 0);
            directionTo.Add(start, null);
            estimatedTo.Add(start, estimatedToGoal);
            cameFrom.Add(start, null);
            open.Enqueue(start, estimatedToGoal);


            var delta = goal.Position.ToVector() - start.Position.ToVector();
            var manhattanDist = Math.Abs(delta.X) + Math.Abs(delta.Y);

            var iterationLimit = Math.Pow(manhattanDist, 2) * abortFactor;
            
            while (open.Any())
            {
                var current = open.Dequeue();
                closed.Add(current);

                var atGoal = CheckForGoal(current, goal, stepSize);

                if (atGoal || iterationLimit <= 0)
                {
                    var path = ConstructPath(current, cameFrom);

                    if(iterationLimit > 0)
                    {
                        return path;
                    }
                    else // if we've hit the iteration limit, then need to find the closest approach to the goal
                    {
                        var trimmedPath = new List<Tile>();
                        var closeApproach = double.MaxValue;
                        foreach(var tile in path)
                        {
                            var dist = tile.Position.DistanceSq(goal.Position);
                            if(dist < closeApproach)
                            {
                                closeApproach = dist;
                                trimmedPath.Add(tile);
                            }
                            else
                            {
                                break;
                            }
                        }

                        return trimmedPath;
                    }
                }

                iterationLimit -= 1;

                var neighbors = GetNeighbors(current, stepSize)
                    .Where(n => isValid(n))
                    .Where(n => !closed.Contains(n))
                    .ToArray();

                foreach (var neighbor in neighbors)
                {
                    var dist = distTo[current] + 1;
                    var direction = current.Position.DirectionTo(neighbor.Position);

                    // Penalize changes in direction
                    if (penalizeTurning
                        && directionTo[current] != null 
                        && directionTo[current] != direction)
                    {
                        dist *= 2;
                    }

                    var estimated = dist + neighbor.Position.Distance(goal.Position);

                    if (!open.Contains(neighbor))
                    {
                        distTo.Add(neighbor, dist);
                        directionTo.Add(neighbor, direction);
                        estimatedTo.Add(neighbor, estimated);
                        cameFrom.Add(neighbor, current);
                        open.Enqueue(neighbor, estimated);
                    }
                    else if (estimated < estimatedTo[neighbor])
                    {
                        distTo[neighbor] = dist;
                        directionTo[neighbor] = direction;
                        estimatedTo[neighbor] = estimated;
                        cameFrom[neighbor] = current;
                        open.UpdatePriority(neighbor, estimated);
                    }
                }
            }

            return Enumerable.Empty<Tile>();
        }
        
        public IEnumerable<Tile> DeterminePath(
            Tile[] starts, 
            Tile[] goals, 
            Settlement activeSettlement, 
            Func<Tile, double> getHeuristic,
            Func<Tile, bool> isValid, 
            bool penalizeTurning = false, 
            float abortFactor = 2, 
            int stepSize = 1)
        {
            if (!starts.Any(t => t != null && isValid(t)) || !goals.Any(t => t != null && isValid(t)))
            {
                return Enumerable.Empty<Tile>();
            }

            var open = new SimplePriorityQueue<Tile, double>();
            var closed = new HashSet<Tile>();
            var distTo = new Dictionary<Tile, double>();
            var directionTo = new Dictionary<Tile, Facing?>();
            var estimatedTo = new Dictionary<Tile, double>();
            var cameFrom = new Dictionary<Tile, Tile>();

            foreach (var start in starts)
            {
                var estimatedToGoal = getHeuristic(start);

                distTo.Add(start, 0);
                directionTo.Add(start, null);
                estimatedTo.Add(start, estimatedToGoal);
                cameFrom.Add(start, null);
                open.Enqueue(start, estimatedToGoal);
            }

            var iterationLimit = Math.Pow(estimatedTo.Select(kvp => kvp.Value).Max(), 2) * abortFactor;

            while (open.Any())
            {
                var current = open.Dequeue();
                closed.Add(current);

                var atGoal = CheckForGoal(current, goals, stepSize);

                if (atGoal || iterationLimit <= 0)
                {
                    var path = ConstructPath(current, cameFrom);

                    if (iterationLimit > 0)
                    {
                        return path;
                    }
                    else // if we've hit the iteration limit, then need to find the closest approach to the goal
                    {
                        var trimmedPath = new List<Tile>();
                        var closeApproach = double.MaxValue;
                        foreach (var tile in path)
                        {
                            var dist = getHeuristic(tile);
                            if (dist < closeApproach)
                            {
                                closeApproach = dist;
                                trimmedPath.Add(tile);
                            }
                            else
                            {
                                break;
                            }
                        }

                        return trimmedPath;
                    }
                }

                iterationLimit -= 1;

                var neighbors = GetNeighbors(current, stepSize)
                    .Where(n => isValid(n))
                    .Where(n => !closed.Contains(n))
                    .ToArray();

                foreach (var neighbor in neighbors)
                {
                    var dist = distTo[current] + 1;
                    var direction = current.Position.DirectionTo(neighbor.Position);

                    // Penalize changes in direction
                    if (penalizeTurning
                        && directionTo[current] != null
                        && directionTo[current] != direction)
                    {
                        dist *= 2;
                    }

                    var estimated = dist + getHeuristic(neighbor);

                    if (!open.Contains(neighbor))
                    {
                        distTo.Add(neighbor, dist);
                        directionTo.Add(neighbor, direction);
                        estimatedTo.Add(neighbor, estimated);
                        cameFrom.Add(neighbor, current);
                        open.Enqueue(neighbor, estimated);
                    }
                    else if (estimated < estimatedTo[neighbor])
                    {
                        distTo[neighbor] = dist;
                        directionTo[neighbor] = direction;
                        estimatedTo[neighbor] = estimated;
                        cameFrom[neighbor] = current;
                        open.UpdatePriority(neighbor, estimated);
                    }
                }
            }

            return Enumerable.Empty<Tile>();
        }


        private bool CheckForGoal(Tile current, Tile goal, int stepSize)
        {
            for (var i = 0; i < stepSize; i++)
            {
                for (var j = 0; j < stepSize; j++)
                {
                    if (current.Position.X + i == goal.Position.X
                        && current.Position.Y + j == goal.Position.Y)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private bool CheckForGoal(Tile current, Tile[] goals, int stepSize)
        {
            for (var i = 0; i < stepSize; i++)
            {
                for (var j = 0; j < stepSize; j++)
                {
                    if(goals.Any(g=>current.Position == g.Position))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private IEnumerable<Tile> GetNeighbors(Tile current, int stepSize)
        {
            for (var i = 0; i < ORTHO_FACINGS.Length; i++)
            {
                var neighbor = current;
                for (var j = 0; j < stepSize && neighbor != null; j++)
                {
                    neighbor = neighbor.GetNeighbor(ORTHO_FACINGS[i]);
                }

                if (neighbor != null)
                {
                    yield return neighbor;
                }
            }
        }

        private IEnumerable<Tile> ConstructPath(
            Tile current,
            Dictionary<Tile, Tile> cameFrom)
        {
            var path = new List<Tile>();

            while (current != null)
            {
                path.Add(current);
                current = cameFrom[current];
            }

            path.Reverse();

            return path;
        }
    }
}
