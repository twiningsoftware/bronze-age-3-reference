﻿using Bronze.Contracts.Data.Dungeons;
using Bronze.Contracts.InjectableServices;
using System.Collections.Generic;

namespace Bronze.Common.Services
{
    /// <summary>
    /// Default implementation of IDungeonDataManager.
    /// </summary>
    public class DungeonDataManager : IDungeonDataManager
    {
        public List<DungeonDefinition> DungeonDefinitions { get; }

        public DungeonDataManager()
        {
            DungeonDefinitions = new List<DungeonDefinition>();
        }
    }
}
