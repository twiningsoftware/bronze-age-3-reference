﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.Simulation.Events;
using Bronze.Contracts.Data.Trade;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.Services
{
    public class TradeManager : ITradeManager, IWorldStateService, ISubSimulator
    {
        private List<TradeRoute> _tradeRoutes;
        private Dictionary<Settlement, List<TradeRoute>> _tradeRoutesBySource;
        private Dictionary<Settlement, List<TradeRoute>> _tradeRoutesByDestination;
        
        private readonly IWorldManager _worldManager;
        private readonly IGamedataTracker _gamedata;
        private readonly INotificationsSystem _notificationsSystem;
        private readonly IGenericNotificationBuilder _genericNotificationBuilder;
        private readonly IBattleHelper _battleHelper;
        private readonly ISimulationEventBus _simulationEventBus;
        private readonly IPlayerDataTracker _playerData;

        public IEnumerable<TradeRoute> AllTradeRoutes => _tradeRoutes;

        public TradeManager(
            IWorldManager worldManager,
            IGamedataTracker gamedata,
            INotificationsSystem notificationsSystem,
            IGenericNotificationBuilder genericNotificationBuilder,
            IBattleHelper battleHelper,
            ISimulationEventBus simulationEventBus,
            IPlayerDataTracker playerData)
        {
            _worldManager = worldManager;
            _gamedata = gamedata;
            _notificationsSystem = notificationsSystem;
            _genericNotificationBuilder = genericNotificationBuilder;
            _battleHelper = battleHelper;
            _simulationEventBus = simulationEventBus;
            _playerData = playerData;
            _tradeRoutes = new List<TradeRoute>();
            _tradeRoutesBySource = new Dictionary<Settlement, List<TradeRoute>>();
            _tradeRoutesByDestination = new Dictionary<Settlement, List<TradeRoute>>();

            _simulationEventBus.OnSimulationEvent += OnSimulationEvent;
        }

        private void OnSimulationEvent(ISimulationEvent evt)
        {
            if(evt is SettlementOwnerChangedSimulationEvent chownEvt)
            {
                var tradeRoutes = GetImportRoutes(chownEvt.Settlement)
                    .Concat(GetExportRoutes(chownEvt.Settlement))
                    .ToArray();

                foreach (var tradeRoute in tradeRoutes)
                {
                    RemoveTradeRoute(tradeRoute);
                }
            }
        }

        public void Clear()
        {
            _tradeRoutes.Clear();
            _tradeRoutesBySource.Clear();
            _tradeRoutesByDestination.Clear();
        }

        public IEnumerable<ItemRate> GetExportsFor(Settlement settlement)
        {
            if(_tradeRoutesBySource.ContainsKey(settlement))
            {
                return _tradeRoutesBySource[settlement]
                    .SelectMany(tr => tr.Exports)
                    .GroupBy(ir => ir.Item)
                    .Select(g => new ItemRate(g.Key, g.Sum(x => x.PerMonth)))
                    .ToArray();    
            }

            return Enumerable.Empty<ItemRate>();
        }

        public IEnumerable<ItemRate> GetImportsFor(Settlement settlement)
        {
            if (_tradeRoutesByDestination.ContainsKey(settlement))
            {
                return _tradeRoutesByDestination[settlement]
                    .SelectMany(tr => tr.Exports)
                    .GroupBy(ir => ir.Item)
                    .Select(g => new ItemRate(g.Key, g.Sum(x => x.PerMonth)))
                    .ToArray();
            }

            return Enumerable.Empty<ItemRate>();
        }

        public IEnumerable<TradeCapInfo> GetTradeCapsFor(Settlement settlement)
        {
            return settlement.EconomicActors
                .OfType<ITradeActor>()
                .GroupBy(x => x.TradeType)
                .Select(g => new TradeCapInfo(g.Key, g.Sum(x => x.TradeCapacity)))
                .ToArray();
        }

        public IEnumerable<TradePartnerInfo> GetTradePartnersFor(Settlement settlement)
        {
            var partners = new List<TradePartnerInfo>();

            // Add export parters
            if (_tradeRoutesBySource.ContainsKey(settlement))
            {
                foreach(var tradeRoute in _tradeRoutesBySource[settlement])
                {
                    var partner = partners.Where(tp => tp.Partner == tradeRoute.ToSettlement).FirstOrDefault();
                    if(partner == null)
                    {
                        partner = new TradePartnerInfo(tradeRoute.ToSettlement);
                        partners.Add(partner);
                    }

                    partner.Exports = partner.Exports.Concat(tradeRoute.Exports);
                }
            }

            // Add import parters
            if (_tradeRoutesByDestination.ContainsKey(settlement))
            {
                foreach (var tradeRoute in _tradeRoutesByDestination[settlement])
                {
                    var partner = partners.Where(tp => tp.Partner == tradeRoute.FromSettlement).FirstOrDefault();
                    if (partner == null)
                    {
                        partner = new TradePartnerInfo(tradeRoute.FromSettlement);
                        partners.Add(partner);
                    }

                    partner.Imports = partner.Imports.Concat(tradeRoute.Exports);
                }
            }

            // Coalece imports and exports by item
            foreach(var partner in partners)
            {
                partner.Imports = partner.Imports
                    .GroupBy(ir => ir.Item)
                    .Select(g => new ItemRate(g.Key, g.Sum(ir => ir.PerMonth)))
                    .ToArray();

                partner.Exports = partner.Exports
                    .GroupBy(ir => ir.Item)
                    .Select(g => new ItemRate(g.Key, g.Sum(ir => ir.PerMonth)))
                    .ToArray();
            }

            return partners;
        }

        public void Initialize(WorldState worldState)
        {
        }

        public void LoadCustomData(SerializedObject worldRoot)
        {
            var root = worldRoot.GetChild("trade_manager");

            var allSettlements = _worldManager.Tribes
                .SelectMany(t => t.Settlements)
                .ToDictionary(s => s.Id);
            
            foreach (var tradeRouteRoot in root.GetChildren("trade_route"))
            {
                var fromSettlementId = tradeRouteRoot.GetString("from_settlement_id");
                var fromActorId = tradeRouteRoot.GetString("from_id");
                var toSettlementId = tradeRouteRoot.GetString("to_settlement_id");
                var toActorId = tradeRouteRoot.GetString("to_id");
                var isTreatyRoute = tradeRouteRoot.GetOptionalBool("is_treaty_route");

                if(allSettlements.ContainsKey(fromSettlementId) && allSettlements.ContainsKey(toSettlementId))
                {
                    var fromSettlement = allSettlements[fromSettlementId];
                    var toSettlement = allSettlements[toSettlementId];

                    var fromActor = FindActor(fromSettlement, fromActorId);
                    var toActor = FindReciever(toSettlement, toActorId);

                    if(fromActor != null && toActor != null)
                    {
                        var tradeRoute = CreateTradeRoute(fromActor, toActor);

                        tradeRoute.IsTreatyRoute = isTreatyRoute;

                        foreach(var irRoot in tradeRouteRoot.GetChildren("item_rate"))
                        {
                            var item = irRoot.GetObjectReference(
                                "item",
                                _gamedata.Items,
                                i => i.Name);

                            tradeRoute.SetExportPerMonth(item, irRoot.GetDouble("per_month"));
                        }
                    }
                }
            }
        }

        private ITradeActor FindActor(Settlement settlement, string actorId)
        {
            return settlement.Districts
                .SelectMany(d => d.Structures)
                .AsEnumerable<IEconomicActor>()
                .Concat(settlement.Region.Cells
                    .Where(c => c.Development != null)
                    .Select(c => c.Development))
                .OfType<ITradeActor>()
                .Where(ta => ta.Id == actorId)
                .FirstOrDefault();
        }

        private ITradeReciever FindReciever(Settlement settlement, string actorId)
        {
            return settlement.Districts
                .SelectMany(d => d.Structures)
                .AsEnumerable<IEconomicActor>()
                .Concat(settlement.Region.Cells
                    .Where(c => c.Development != null)
                    .Select(c => c.Development))
                .OfType<ITradeReciever>()
                .Where(ta => ta.Id == actorId)
                .FirstOrDefault();
        }

        public void SaveCustomData(SerializedObject worldRoot)
        {
            var root = worldRoot.CreateChild("trade_manager");

            foreach(var tradeRoute in _tradeRoutes)
            {
                var tradeRouteRoot = root.CreateChild("trade_route");

                tradeRouteRoot.Set("from_settlement_id", tradeRoute.FromSettlement.Id);
                tradeRouteRoot.Set("from_id", tradeRoute.FromActor.Id);
                tradeRouteRoot.Set("to_settlement_id", tradeRoute.ToSettlement.Id);
                tradeRouteRoot.Set("to_id", tradeRoute.ToActor.Id);
                tradeRouteRoot.Set("is_treaty_route", tradeRoute.IsTreatyRoute);

                foreach(var itemRate in tradeRoute.Exports)
                {
                    var iqRoot = tradeRouteRoot.CreateChild("item_rate");
                    iqRoot.Set("item", itemRate.Item.Name);
                    iqRoot.Set("per_month", itemRate.PerMonth);
                }
            }
        }

        public void StepSimulation(double elapsedMonths)
        {
            foreach(var tradeRoute in _tradeRoutes.ToArray())
            {
                if(tradeRoute.FromActor.IsRemoved
                    || tradeRoute.ToActor.IsRemoved
                    || tradeRoute.FromSettlement.IsRemoved
                    || tradeRoute.ToSettlement.IsRemoved)
                {
                    RemoveTradeRoute(tradeRoute);


                    CellPosition? lookAt = null;

                    if(tradeRoute.FromActor is ICellDevelopment cellDevelopment)
                    {
                        lookAt = cellDevelopment.Cell.Position;
                    }
                    else if (tradeRoute.FromActor is IStructure structure)
                    {
                        lookAt = structure.UlTile.District.Cell.Position;
                    }

                    if (tradeRoute.FromSettlement.Owner == _playerData.PlayerTribe
                        || tradeRoute.ToSettlement.Owner == _playerData.PlayerTribe)
                    {
                        _notificationsSystem.AddNotification(_genericNotificationBuilder.BuildSimpleNotification(
                            "ui_notification_trade",
                            "Trade Route Lost",
                            $"A trade route between {tradeRoute.FromSettlement.Name} and {tradeRoute.ToSettlement.Name} has been lost.",
                            "Trade Route Lost",
                            $"A trade route between {tradeRoute.FromSettlement.Name} and {tradeRoute.ToSettlement.Name} has been lost becuase one of the structures has been demolished.",
                            lookAt));
                    }
                }
            }

            foreach (var settlement in _worldManager.Tribes.SelectMany(t => t.Settlements))
            {
                ProcessTradeFor(settlement, elapsedMonths);
            }
        }

        private void ProcessTradeFor(Settlement settlement, double elapsedMonths)
        {
            if(_tradeRoutesBySource.ContainsKey(settlement))
            {
                foreach(var tradeRoute in _tradeRoutesBySource[settlement].ToArray())
                {
                    tradeRoute.PathCalculator.Update();

                    var transmissionEfficiency = new[]
                    {
                        tradeRoute.FromActor.WorkerPercent,
                        tradeRoute.FromActor.UpkeepPercent,
                        tradeRoute.ToActor.WorkerPercent,
                        tradeRoute.ToActor.UpkeepPercent
                    }.Min();

                    var blockedTrade = Enumerable.Empty<MovementType>();

                    if(settlement.IsUnderSiege)
                    {
                       blockedTrade = settlement.SiegedBy
                           .SelectMany(a => a.Units.SelectMany(u => u.MovementModes).Select(mm => mm.MovementType))
                           .Distinct()
                           .ToArray();
                    }
                    
                    if (tradeRoute.PathCalculator.PathSuccessful
                        && !blockedTrade.Any(mt => tradeRoute.MovementType.IsSubsetOf(mt))
                        && !tradeRoute.PathCalculator.Path.Any(p => p.WorldActors.OfType<Army>().Any(a => settlement.Owner.IsHostileTo(a.Owner))))
                    {
                        foreach(var itemRate in tradeRoute.Exports)
                        {
                            if (tradeRoute.ToActor.Inventory.QuantityOf(itemRate.Item) < tradeRoute.ToActor.InventoryCapacity)
                            {
                                var qtyToSend = itemRate.PerMonth * elapsedMonths * transmissionEfficiency;

                                qtyToSend = Math.Min(qtyToSend, tradeRoute.FromActor.Inventory.QuantityOf(itemRate.Item));

                                if (qtyToSend > 0)
                                {
                                    tradeRoute.FromSettlement.LastMonthProduction[itemRate.Item] -= qtyToSend;
                                    tradeRoute.FromActor.Inventory.Remove(itemRate.Item, qtyToSend);

                                    tradeRoute.ToSettlement.LastMonthProduction[itemRate.Item] += qtyToSend;
                                    tradeRoute.ToActor.Inventory.Add(itemRate.Item, qtyToSend);
                                }
                            }
                        }
                    }
                }
            }
        }

        public void RemoveTradeRoute(TradeRoute tradeRoute)
        {
            tradeRoute.FromActor.TradeRoute = null;
            tradeRoute.ToActor.TradeRoutes.Remove(tradeRoute);
            tradeRoute.ToActor.OnTradeRoutesChanged();

            _tradeRoutes.Remove(tradeRoute);
            
            if(_tradeRoutesByDestination.ContainsKey(tradeRoute.ToSettlement))
            {
                _tradeRoutesByDestination[tradeRoute.ToSettlement].Remove(tradeRoute);
            }
            if (_tradeRoutesBySource.ContainsKey(tradeRoute.FromSettlement))
            {
                _tradeRoutesBySource[tradeRoute.FromSettlement].Remove(tradeRoute);
            }

            tradeRoute.FromSettlement.SetCalculationFlag("trade route removed");
            tradeRoute.ToSettlement.SetCalculationFlag("trade route removed");
        }

        public IEnumerable<TradeRoute> GetExportRoutes(Settlement settlement)
        {
            if (_tradeRoutesBySource.ContainsKey(settlement))
            {
                return _tradeRoutesBySource[settlement];
            }

            return Enumerable.Empty<TradeRoute>();
        }

        public IEnumerable<TradeRoute> GetImportRoutes(Settlement settlement)
        {
            if (_tradeRoutesByDestination.ContainsKey(settlement))
            {
                return _tradeRoutesByDestination[settlement];
            }

            return Enumerable.Empty<TradeRoute>();
        }
        
        public IEnumerable<TradeRoute> GetTradeRoutesFor(Tribe tribe)
        {
            return _tradeRoutes.Where(tr => tr.FromSettlement.Owner == tribe);
        }

        public TradeRoute CreateTradeRoute(ITradeActor from, ITradeReciever to)
        {
            var tradeRoute = new TradeRoute(
                from.TradeType,
                from,
                from.Settlement,
                to,
                to.Settlement);

            from.TradeRoute = tradeRoute;
            to.TradeRoutes.Add(tradeRoute);
            to.OnTradeRoutesChanged();
            
            _tradeRoutes.Add(tradeRoute);
            
            if(!_tradeRoutesByDestination.ContainsKey(tradeRoute.ToSettlement))
            {
                _tradeRoutesByDestination.Add(tradeRoute.ToSettlement, new List<TradeRoute>());
            }
            if (!_tradeRoutesBySource.ContainsKey(tradeRoute.FromSettlement))
            {
                _tradeRoutesBySource.Add(tradeRoute.FromSettlement, new List<TradeRoute>());
            }

            _tradeRoutesByDestination[tradeRoute.ToSettlement].Add(tradeRoute);
            _tradeRoutesBySource[tradeRoute.FromSettlement].Add(tradeRoute);

            tradeRoute.FromSettlement.SetCalculationFlag("trade route added");
            tradeRoute.ToSettlement.SetCalculationFlag("trade route added");

            return tradeRoute;
        }

        public void ReparentTradeRoute(
            ITradeActor oldTrader, 
            ITradeActor newTrader, 
            TradeRoute tradeRoute)
        {
            if(oldTrader.TradeType.IsSubsetOf(newTrader.TradeType))
            {
                var newRoute = CreateTradeRoute(newTrader, tradeRoute.ToActor);

                foreach(var export in tradeRoute.Exports)
                {
                    newRoute.SetExportPerMonth(export.Item, export.PerMonth);
                }

                RemoveTradeRoute(tradeRoute);
            }
        }

        public void ReparentTradeRoute(
            ITradeReciever oldReciever,
            ITradeReciever newReciever,
            TradeRoute tradeRoute)
        {
            var newRoute = CreateTradeRoute(tradeRoute.FromActor, newReciever);

            foreach (var export in tradeRoute.Exports)
            {
                newRoute.SetExportPerMonth(export.Item, export.PerMonth);
            }

            RemoveTradeRoute(tradeRoute);
        }
    }
}
