﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;
using Bronze.Common.Data.Game;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Generation;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.Exceptions;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.Services
{
    public class XmlReaderUtil : IXmlReaderUtil
    {
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IInjectionProvider _injectionProvider;
        private readonly ITextSanitizer _textSanitizer;

        public XmlReaderUtil(
            IGamedataTracker gamedataTracker,
            IInjectionProvider injectionProvider,
            ITextSanitizer textSanitizer)
        {
            _gamedataTracker = gamedataTracker;
            _injectionProvider = injectionProvider;
            _textSanitizer = textSanitizer;
        }

        public XElement Element(XDocument document, string descendantName)
        {
            return Element(document.Root, descendantName);
        }

        public XElement Element(XElement element, string descendantName)
        {
            var descendant = element.Elements(descendantName).FirstOrDefault();

            if (descendant == null)
            {
                throw DataLoadException.XmlMissingDescendant(element, descendantName);
            }

            return descendant;
        }

        public XElement ElementOrNull(XElement element, string descendantName)
        {
            return element.Elements(descendantName).FirstOrDefault();
        }

        public string AttributeValue(XElement element, string attributeName)
        {
            var attribute = element.Attribute(attributeName);

            if (attribute == null)
            {
                throw DataLoadException.XmlMissingAttribute(element, attributeName);
            }

            return attribute.Value;
        }

        public string OptionalAttributeValue(XElement element, string attributeName, string defaultValue)
        {
            var attribute = element.Attribute(attributeName);

            if (attribute == null)
            {
                return defaultValue;
            }

            return attribute.Value;
        }

        public int AttributeAsInt(XElement element, string attributeName)
        {
            var value = AttributeValue(element, attributeName);

            if (int.TryParse(value, out var i))
            {
                return i;
            }

            throw DataLoadException.XmlAttributeParse(element, attributeName, "integer", value);
        }

        public float AttributeAsFloat(XElement element, string attributeName)
        {
            var value = AttributeValue(element, attributeName);

            value = value.Replace(',', '.');

            if (float.TryParse(value, NumberStyles.Float, CultureInfo.InvariantCulture, out var f))
            {
                return f;
            }

            throw DataLoadException.XmlAttributeParse(element, attributeName, "float", value);
        }

        public double AttributeAsDouble(XElement element, string attributeName)
        {
            var value = AttributeValue(element, attributeName);

            value = value.Replace(',', '.');

            if (double.TryParse(value, NumberStyles.Float, CultureInfo.InvariantCulture, out var f))
            {
                return f;
            }

            throw DataLoadException.XmlAttributeParse(element, attributeName, "double", value);
        }

        public double OptionalAttributeAsDouble(XElement element, string attributeName)
        {
            var value = OptionalAttributeValue(element, attributeName, null);

            if(value == null)
            {
                return 0;
            }

            value = value.Replace(',', '.');

            if (double.TryParse(value, NumberStyles.Float, CultureInfo.InvariantCulture, out var f))
            {
                return f;
            }

            throw DataLoadException.XmlAttributeParse(element, attributeName, "double", value);
        }

        public bool AttributeAsBool(XElement element, string attributeName)
        {
            var value = AttributeValue(element, attributeName);

            if (bool.TryParse(value, out var val))
            {
                return val;
            }

            throw DataLoadException.XmlAttributeParse(element, attributeName, "boolean", value);
        }

        public TEnum AttributeAsEnum<TEnum>(XElement element, string attributeName) where TEnum : struct
        {
            var value = AttributeValue(element, attributeName);

            if (Enum.TryParse<TEnum>(value, true, out var e))
            {
                return e;
            }

            throw DataLoadException.XmlAttributeParse(element, attributeName, typeof(TEnum).Name, value);
        }

        public T ObjectReferenceLookup<T>(
            XElement element,
            string attributeName,
            IEnumerable<T> options,
            Func<T, string> matchDataForObject)
        {
            var matchValue = AttributeValue(element, attributeName);

            var match = options
                .Where(o => matchDataForObject(o) == matchValue)
                .FirstOrDefault();

            if (match != null)
            {
                return match;
            }

            throw DataLoadException.MissingDataReference(
                element.Name.ToString(),
                typeof(T).Name,
                matchValue);
        }

        public T OptionalObjectReferenceLookup<T>(
            XElement element,
            string attributeName,
            IEnumerable<T> options,
            Func<T, string> matchDataForObject)
        {
            var matchValue = OptionalAttributeValue(element, attributeName, null);

            if (matchValue == null)
            {
                return default(T);
            }

            var match = options
                .Where(o => matchDataForObject(o) == matchValue)
                .FirstOrDefault();

            if (match != null)
            {
                return match;
            }

            throw DataLoadException.MissingDataReference(
                element.Name.ToString(),
                typeof(T).Name,
                matchValue);
        }

        public CellAnimationLayer LoadCellSpriteLayer(XElement element)
        {
            return new CellAnimationLayer
            {
                AnimationName = null,
                ImageKey = AttributeValue(element, "image_key"),
                DrawLayer = AttributeAsFloat(element, "layer"),
                Adjacency = AttributeValue(element, "adjacency"),
                Type = AttributeAsEnum<SpriteType>(element, "type"),
                FrameCount = 1,
                LoopTime = 0,
                SoundEffects = element
                    .Elements("sound_effect")
                    .Select(e => LoadSoundEffectType(e))
                    .ToArray()
            };
        }

        public CellAnimationLayer LoadCellAnimationLayer(XElement element)
        {
            return new CellAnimationLayer
            {
                AnimationName = AttributeValue(element, "name"),
                ImageKey = AttributeValue(element, "image_key"),
                DrawLayer = AttributeAsFloat(element, "layer"),
                Adjacency = AttributeValue(element, "adjacency"),
                Type = AttributeAsEnum<SpriteType>(element, "type"),
                FrameCount = AttributeAsInt(element, "frame_count"),
                LoopTime = AttributeAsFloat(element, "loop_time"),
                SoundEffects = element
                    .Elements("sound_effect")
                    .Select(e => LoadSoundEffectType(e))
                    .ToArray()
            };
        }

        public IEnumerable<ITileDisplay> LoadTileDisplays(XElement element)
        {
            return element.Elements("sprite_animation")
                .Select(x => LoadSpriteAnimation(x))
                .OfType<ITileDisplay>()
                .Concat(element.Elements("tile_animation")
                    .Select(x => LoadTileAnimation(x)))
                .ToArray();
        }

        public SpriteAnimation LoadSpriteAnimation(XElement element)
        {
            if (string.IsNullOrWhiteSpace(OptionalAttributeValue(element, "name", null)))
            {
                return new SpriteAnimation
                {
                    DrawLayer = AttributeAsFloat(element, "layer"),
                    Height = AttributeAsInt(element, "height"),
                    Width = AttributeAsInt(element, "width"),
                    ImageKey = AttributeValue(element, "image_key"),
                    AnimationName = null,
                    FrameCount = 1,
                    LoopTime = 0,
                    SoundEffects = element
                        .Elements("sound_effect")
                        .Select(e => LoadSoundEffectType(e))
                        .ToArray()
                };
            }

            return new SpriteAnimation
            {
                DrawLayer = AttributeAsFloat(element, "layer"),
                Height = AttributeAsInt(element, "height"),
                Width = AttributeAsInt(element, "width"),
                ImageKey = AttributeValue(element, "image_key"),
                AnimationName = AttributeValue(element, "name"),
                LoopTime = AttributeAsFloat(element, "loop_time"),
                FrameCount = AttributeAsInt(element, "frame_count"),
                SoundEffects = element
                    .Elements("sound_effect")
                    .Select(e => LoadSoundEffectType(e))
                    .ToArray()
            };
        }

        private TileAnimationLayer LoadTileAnimation(XElement element)
        {
            var layer = new TileAnimationLayer
            {
                AnimationName = null,
                ImageKey = AttributeValue(element, "image_key"),
                DrawLayer = AttributeAsFloat(element, "layer"),
                Adjacency = AttributeValue(element, "adjacency"),
                TileLayout = AttributeAsEnum<TileLayout>(element, "layout"),
                SoundEffects = element
                    .Elements("sound_effect")
                    .Select(e => LoadSoundEffectType(e))
                    .ToArray()
            };

            return layer;
        }

        public ItemRate LoadItemRate(XElement e)
        {
            return new ItemRate(
                ObjectReferenceLookup(
                    e,
                    "item",
                    _gamedataTracker.Items,
                    i => i.Name),
                AttributeAsDouble(e, "per_month"));
        }

        public ItemQuantity LoadItemQuantity(XElement e)
        {
            return new ItemQuantity(
                ObjectReferenceLookup(
                    e,
                    "item",
                    _gamedataTracker.Items,
                    i => i.Name),
                AttributeAsDouble(e, "quantity"));
        }
        
        public TradeHaulerInfo LoadTradeHaulerInfo(XElement haulerElement)
        {
            return new TradeHaulerInfo
            {
                Animations = haulerElement
                        .Elements("animation")
                        .Select(ae => LoadIndividualAnimation(ae))
                        .ToArray()
            };
        }

        public IndividualAnimation LoadIndividualAnimation(XElement element)
        {
            return new IndividualAnimation
            {
                Name = AttributeValue(element, "name"),
                ImageKey = AttributeValue(element, "image_key"),
                FacingType = AttributeAsEnum<AnimationFacingType>(element, "facing_type"),
                FrameCount = AttributeAsInt(element, "frame_count"),
                Width = AttributeAsInt(element, "width"),
                Height = AttributeAsInt(element, "height"),
                LoopTime = AttributeAsFloat(element, "loop_time"),
                Layer = AttributeAsFloat(element, "layer"),
                SoundEffects = element
                    .Elements("sound_effect")
                    .Select(e => LoadSoundEffectType(e))
                    .ToArray()
            };
        }

        public WorkerNeed LoadWorkerNeed(XElement constructionElement, Race race)
        {
            var workerElement = Element(constructionElement, "workers");

            return new WorkerNeed
            {
                Caste = ObjectReferenceLookup(
                    workerElement,
                    "caste",
                    race.Castes,
                    c => c.Id),
                Need = AttributeAsInt(workerElement, "need")
            };
        }

        public InclusionFeature LoadInclusionFeature(XElement element)
        {
            return new InclusionFeature
            {
                Id = AttributeValue(element, "id"),
                CellsPerOccurance = AttributeAsInt(element, "cells_per_occurrence"),
                MinimumOccurances = AttributeAsInt(element, "minimum_occurrences"),
                Biome = OptionalObjectReferenceLookup(
                    element,
                    "biome",
                    _gamedataTracker.Biomes,
                    b => b.Id),
                CellFeature = OptionalObjectReferenceLookup(
                    element,
                    "cell_feature",
                    _gamedataTracker.CellFeatures,
                    cf => cf.Id),
                RiverSpawners = element
                    .Descendants("river_spawner")
                    .Select(e => LoadRiverSpawner(e))
                    .ToArray(),
                MidpathSpawners = element
                    .Descendants("midpath_spawner")
                    .Select(e => LoadMidpathSpawner(e))
                    .ToArray(),
                CellDevelopment = OptionalObjectReferenceLookup(
                    element,
                    "cell_development",
                    _gamedataTracker.CellDevelopments,
                    cd => cd.Id),
            };
        }

        public RiverSpawner LoadRiverSpawner(XElement element)
        {
            return new RiverSpawner
            {
                Id = AttributeValue(element, "id"),
                SpawnChance = AttributeAsDouble(element, "spawn_chance"),
                RiverType = ObjectReferenceLookup(
                    element,
                    "river_id",
                    _gamedataTracker.RiverTypes,
                    rt => rt.Id)
            };
        }

        public MidpathSpawner LoadMidpathSpawner(XElement element)
        {
            return new MidpathSpawner
            {
                Id = AttributeValue(element, "id"),
                SpawnChance = AttributeAsDouble(element, "spawn_chance"),
                MidpathType = ObjectReferenceLookup(
                    element,
                    "midpath_id",
                    _gamedataTracker.MidpathTypes,
                    rt => rt.Id)
            };
        }

        public BorderFeatureCondition LoadBorderFeatureCondition(XElement element)
        {
            var type = AttributeValue(element, "type");

            var biomeId = OptionalAttributeValue(element, "biome_id", null);
            var traitId = OptionalAttributeValue(element, "trait_id", null);

            var biome = OptionalObjectReferenceLookup(
                    element,
                    "biome_id",
                    _gamedataTracker.Biomes,
                    b => b.Id);

            var trait = OptionalObjectReferenceLookup(
                    element,
                    "trait_id",
                    _gamedataTracker.Traits,
                    t => t.Id);

            switch (type)
            {
                case "has_biome":
                    return new BorderFeatureCondition(c => c.Biome == biome);
                case "not_has_biome":
                    return new BorderFeatureCondition(c => c.Biome != biome);
                case "has_adjacent_biome":
                    return new BorderFeatureCondition(c => c.OrthoNeighbors.Any(n => n != null && n.Biome == biome));
                case "not_has_adjacent_biome":
                    return new BorderFeatureCondition(c => !c.OrthoNeighbors.Any(n => n != null && n.Biome == biome));
                case "has_trait":
                    return new BorderFeatureCondition(c => c.Traits.Contains(trait));
                case "not_has_trait":
                    return new BorderFeatureCondition(c => !c.Traits.Contains(trait));
                case "has_adjacent_trait":
                    return new BorderFeatureCondition(c => c.OrthoNeighbors.Any(n => n != null && n.Traits.Contains(trait)));
                case "not_has_adjacent_trait":
                    return new BorderFeatureCondition(c => !c.OrthoNeighbors.Any(n => n != null && n.Traits.Contains(trait)));
                case "world_top":
                    return new BorderFeatureCondition(c => c.GetNeighbor(Facing.North) == null);
                case "world_bottom":
                    return new BorderFeatureCondition(c => c.GetNeighbor(Facing.South) == null);
                default:
                    throw new ArgumentException("Unknown border feature type: " + type);
            }
        }

        public BorderFeature LoadBorderFeature(XElement element)
        {
            return new BorderFeature
            {
                Id = AttributeValue(element, "id"),
                CellsPerOccurance = AttributeAsInt(element, "cells_per_occurrence"),
                MinimumOccurances = AttributeAsInt(element, "minimum_occurrences"),
                MinLength = AttributeAsInt(element, "min_length"),
                MaxLength = AttributeAsInt(element, "max_length"),
                InclusionRadii = element
                    .Elements("inclusion_radii")
                    .Select(e => new InclusionRadius
                    {
                        Radius = AttributeAsInt(e, "radius"),
                        InclusionFeatures = e.Elements("inclusion_feature")
                                .Select(x => LoadInclusionFeature(x))
                                .ToArray()
                    })
                    .ToArray(),
                Interruptions = element
                    .Elements("border_interruption")
                    .Select(e => LoadBorderFeatureInterruption(e))
                    .ToArray(),
                StartConditions = element.Elements("start_condition")
                    .Select(e => LoadBorderFeatureCondition(e))
                    .ToArray(),
                EndConditions = element.Elements("end_condition")
                    .Select(e => LoadBorderFeatureCondition(e))
                    .ToArray(),
                PathConditions = element.Elements("path_condition")
                    .Select(e => LoadBorderFeatureCondition(e))
                    .ToArray(),
            };
        }

        private BorderFeatureInterruption LoadBorderFeatureInterruption(XElement element)
        {
            return new BorderFeatureInterruption
            {
                Id = AttributeValue(element, "id"),
                CellsPerOccurance = AttributeAsInt(element, "cells_per_occurrence"),
                MinimumOccurances = AttributeAsInt(element, "minimum_occurrences"),
                Biome = OptionalObjectReferenceLookup(
                    element,
                    "biome",
                    _gamedataTracker.Biomes,
                    b => b.Id),
                CellFeature = OptionalObjectReferenceLookup(
                    element,
                    "cell_feature",
                    _gamedataTracker.CellFeatures,
                    cf => cf.Id),
            };
        }

        public ISeedPoint LoadSeedPoint(
            XElement element,
            RegionFeature[] regionFeatures)
        {
            var typeName = AttributeValue(element, "type");

            var childrenPoints = element
                .Elements("seed_point")
                .Select(e => LoadSeedPoint(e, regionFeatures))
                .ToArray();

            var childrenGroups = element
                .Elements("seed_point_group")
                .Select(e => LoadSeedPointGroup(e, regionFeatures))
                .ToArray();

            var seedPoint = _injectionProvider.BuildNamed<ISeedPoint>(typeName);

            seedPoint.LoadFrom(
                this,
                element,
                regionFeatures,
                childrenPoints,
                childrenGroups);

            return seedPoint;
        }

        public SeedPointGroup LoadSeedPointGroup(
            XElement element,
            RegionFeature[] regionFeatures)
        {
            int pickAmount;
            var pickValue = AttributeValue(element, "pick");

            if (pickValue.Equals("all", StringComparison.CurrentCultureIgnoreCase))
            {
                pickAmount = int.MaxValue;
            }
            else
            {
                if (!int.TryParse(pickValue, out pickAmount))
                {
                    throw DataLoadException.XmlAttributeParse(
                        element,
                        "pick",
                        "integer or 'all'",
                        pickValue);
                }
            }

            return new SeedPointGroup
            {
                Id = AttributeValue(element, "id"),
                PickCount = AttributeAsInt(element, "pick"),
                Options = element.Elements("seed_point")
                    .Select(e => LoadSeedPoint(e, regionFeatures))
                    .ToArray()
            };
        }

        public IPopNeed LoadPopNeed(XElement element)
        {
            var type = AttributeValue(element, "type");

            if (type == "item")
            {
                return new ItemPopNeed
                {
                    Item = ObjectReferenceLookup(
                        element,
                        "item",
                        _gamedataTracker.Items,
                        i => i.Name),
                    PerMonthPerPop = AttributeAsDouble(element, "per_month_per_pop"),
                    ProsperityPerPop = OptionalAttributeAsDouble(element, "prosperity_per_pop")
                };
            }
            else if (type == "service")
            {
                return new ServicePopNeed
                {
                    Service = ObjectReferenceLookup(
                        element,
                        "service",
                        _gamedataTracker.ServiceTypes,
                        s => s.Name),
                    ProsperityPerPop = OptionalAttributeAsDouble(element, "prosperity_per_pop")
                };
            }
            else if (type == "authority")
            {
                return new AuthorityPopNeed
                {
                    ProsperityPerPop = OptionalAttributeAsDouble(element, "prosperity_per_pop")
                };
            }
            else
            {
                throw DataLoadException.InvalidValue(element, "type", "Unknown popneed type: " + type);
            }
        }

        public DistrictGenerationLayer LoadDistrictGenerationLayer(XElement element)
        {
            var layer = new DistrictGenerationLayer
            {
                Layer = AttributeAsFloat(element, "layer"),
                Weight = AttributeAsFloat(element, "weight"),
                NoDecorationWeight = AttributeAsFloat(element, "no_decoration_weight"),
                TerrainOptions = element.Elements("terrain_option")
                    .Select(o => ObjectReferenceLookup(
                        o,
                        "terrain",
                        _gamedataTracker.Terrain,
                        t => t.Id))
                    .ToArray(),
                Transitions = element.Elements("transition")
                    .Select(x => new DistrictGenerationTransition
                    {
                        ToAdjacency = AttributeValue(x, "to_adjacency"),
                        Terrain = ObjectReferenceLookup(x,
                            "terrain",
                            _gamedataTracker.Terrain,
                            t => t.Id),
                        Threshold = AttributeAsFloat(x, "threshold"),
                    })
                    .ToArray(),
                DecorationOptions = element.Elements("decoration_option")
                    .Select(x => new DecorationOption
                    {
                        Decoration = ObjectReferenceLookup(x,
                            "decoration",
                            _gamedataTracker.Decorations,
                            d => d.Id),
                        Weight = AttributeAsFloat(x, "weight"),
                        AcceptableTerrain = x.Elements("acceptable_terrain")
                                    .Select(y => ObjectReferenceLookup(
                                            y,
                                            "terrain",
                                            _gamedataTracker.Terrain,
                                            t => t.Id))
                                    .ToArray()
                    })
                    .ToArray(),
                Border = element.Elements("border")
                    .Select(x => new DistrictGenerationBorder
                    {
                        Terrain = ObjectReferenceLookup(x,
                            "terrain",
                            _gamedataTracker.Terrain,
                            t => t.Id),
                        InterruptionsPerTile = AttributeAsFloat(x, "interruptions_per_tile"),
                        InterruptionMinLength = AttributeAsInt(x, "interruption_min_length"),
                        InterruptionMaxLength = AttributeAsInt(x, "interruption_max_length"),
                        MinimumLength = AttributeAsInt(x, "min_length")
                    })
                    .FirstOrDefault()
            };

            var noiseElement = ElementOrNull(element, "noise");
            if (noiseElement != null)
            {
                layer.UsesNoise = true;
                layer.NoiseOctaves = AttributeAsInt(noiseElement, "octaves");
                layer.NoiseMultiplier = AttributeAsInt(noiseElement, "multiplier");
                layer.NoiseThresholdMax = AttributeAsFloat(noiseElement, "max");
                layer.NoiseThresholdMin = AttributeAsFloat(noiseElement, "min");
            }
            else
            {
                layer.UsesNoise = false;
            }


            return layer;
        }

        public TribeSpawner LoadTribeSpawner(XElement element)
        {
            var race = ObjectReferenceLookup(
                    element,
                    "race",
                    _gamedataTracker.Races,
                    r => r.Id);

            return new TribeSpawner
            {
                Id = AttributeValue(element, "id"),
                RegionsPerOccurance = AttributeAsInt(element, "regions_per_occurrence"),
                MinimumOccurances = AttributeAsInt(element, "minimum_occurrences"),
                MinTerritory = AttributeAsInt(element, "min_territory"),
                MaxTerritory = AttributeAsInt(element, "max_territory"),
                Race = race,
                StartingDevelopmentType = ObjectReferenceLookup(
                    element,
                    "starting_development",
                    race.AvailableCellDevelopments,
                    d => d.Id),
                StartingCaste = ObjectReferenceLookup(
                    element,
                    "starting_caste",
                    race.Castes,
                    c => c.Id),
                StartingPops = AttributeAsInt(element, "starting_pops"),
                TribeControllerType = ObjectReferenceLookup(
                    element,
                    "controller_type",
                    _gamedataTracker.ControllerTypes,
                    x => x.Id),
                StartingPeopleScheme = _injectionProvider.BuildNamed<IStartingPeopleScheme>(
                    AttributeValue(element, "starting_people_scheme")),
                RequiredTraits = element
                    .Elements("required")
                    .Select(e => ObjectReferenceLookup(
                        e,
                        "trait",
                        _gamedataTracker.Traits,
                        t => t.Id))
                    .ToArray(),

                PreferredTraits = element
                    .Elements("preferred")
                    .Select(e => ObjectReferenceLookup(
                        e,
                        "trait",
                        _gamedataTracker.Traits,
                        t => t.Id))
                    .ToArray(),

                NotPreferredTraits = element
                    .Elements("not_preferred")
                    .Select(e => ObjectReferenceLookup(
                        e,
                        "trait",
                        _gamedataTracker.Traits,
                        t => t.Id))
                    .ToArray(),

                PreferredNeighborTraits = element
                    .Elements("preferred_neighbor")
                    .Select(e => ObjectReferenceLookup(
                        e,
                        "trait",
                        _gamedataTracker.Traits,
                        t => t.Id))
                    .ToArray(),

                ForbiddenTraits = element
                    .Elements("forbidden")
                    .Select(e => ObjectReferenceLookup(
                        e,
                        "trait",
                        _gamedataTracker.Traits,
                        t => t.Id))
                    .ToArray(),
            };
        }

        private SoundEffectType LoadSoundEffectType(XElement element)
        {
            return new SoundEffectType
            {
                Mode = AttributeAsEnum<SoundEffectMode>(element, "mode"),
                SoundKey = AttributeValue(element, "key")
            };
        }

        public string SanitizeTextForFont(string input)
        {
            return _textSanitizer.SanitizeTextForFont(input);
        }
    }
}
