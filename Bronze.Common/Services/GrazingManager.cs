﻿using Bronze.Common.Data.World.Units;
using Bronze.Contracts;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Bronze.Common.Services
{
    public class GrazingManager : IGrazingManager, IDataLoader, IWorldStateService, ISubSimulator
    {
        public string ElementName => "grazing_manager";

        private readonly IXmlReaderUtil _xmlReaderUtil;
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IWorldManager _worldManager;
        private readonly Dictionary<Cell, double> _exhaustion;
        private readonly Dictionary<Biome, ExhaustionTransformation> _transformsByFrom;
        private readonly Dictionary<Biome, ExhaustionTransformation> _transformsByTo;
        
        public GrazingManager(
            IXmlReaderUtil xmlReaderUtil,
            IGamedataTracker gamedataTracker,
            IWorldManager worldManager)
        {
            _xmlReaderUtil = xmlReaderUtil;
            _gamedataTracker = gamedataTracker;
            _worldManager = worldManager;
            _exhaustion = new Dictionary<Cell, double>();
            _transformsByFrom = new Dictionary<Biome, ExhaustionTransformation>();
            _transformsByTo = new Dictionary<Biome, ExhaustionTransformation>();
        }

        public bool CanGraze(Cell cell, IGrazingUnit grazingUnit)
        {
            return cell.Traits.ContainsAll(grazingUnit.GrazingRequirements)
                && !cell.Traits.ContainsAny(grazingUnit.GrazingForbidden)
                && cell.Development == null
                && _transformsByFrom.ContainsKey(cell.Biome);
        }

        public void Clear()
        {
            _exhaustion.Clear();
        }

        public void DoGrazing(Army army, IGrazingUnit grazingUnit, double deltaMonths)
        {
            if(!_exhaustion.ContainsKey(army.Cell))
            {
                _exhaustion.Add(army.Cell, 0);
            }
            
            _exhaustion[army.Cell] += deltaMonths * grazingUnit.GrazingExhaustion;

            grazingUnit.Supplies = Math.Min(grazingUnit.MaxSupplies, grazingUnit.Supplies + grazingUnit.GrazingRecovery * deltaMonths);

            var campedArmy = (army.CurrentOrder as ArmyCampOrder)?.CampedArmy;
            if(campedArmy != null && campedArmy.Settlement != null)
            {
                var boostFactor = 1.0;

                if (army.Cell.Owner == army.Owner)
                {
                    foreach (var bonusType in grazingUnit.BonusedBy)
                    {
                        boostFactor += campedArmy.CurrentBonuses[bonusType];
                    }
                }

                var producedItems = grazingUnit.GrazingProduction
                    .Select(ir => ir.ToQuantity(deltaMonths * boostFactor))
                    .ToArray();

                campedArmy.Inventory.AddAll(producedItems);

                foreach(var iq in producedItems)
                {
                    campedArmy.Settlement.LastMonthProduction[iq.Item] += iq.Quantity;
                }
            }

            if (army.Units.OfType<IGrazingUnit>().All(u => u.SuppliesPercent > 0.95)
                && !army.IsFull)
            {
                var clone = grazingUnit.UnitType.CreateUnit(grazingUnit.Equipment);
                clone.Supplies = 0.5;
                army.Units.Add(clone);
            }
        }

        public void Initialize(WorldState worldState)
        {
        }

        public void Load(XElement element)
        {
            var transforms = element.Elements("exhaustion")
                .Select(e => new ExhaustionTransformation
                {
                    From = _xmlReaderUtil.ObjectReferenceLookup(
                        e,
                        "from_biome",
                        _gamedataTracker.Biomes,
                        b => b.Id),
                    To = _xmlReaderUtil.ObjectReferenceLookup(
                        e,
                        "to_biome",
                        _gamedataTracker.Biomes,
                        b => b.Id),
                    Threshold = _xmlReaderUtil.AttributeAsDouble(e, "threshold")
                })
                .ToArray();

            foreach(var transform in transforms)
            {
                if(_transformsByFrom.ContainsKey(transform.From))
                {
                    _transformsByFrom[transform.From] = transform;
                }
                else
                {
                    _transformsByFrom.Add(transform.From, transform);
                }

                if (_transformsByTo.ContainsKey(transform.To))
                {
                    _transformsByTo[transform.To] = transform;
                }
                else
                {
                    _transformsByTo.Add(transform.To, transform);
                }
            }
        }

        public void LoadCustomData(SerializedObject worldRoot)
        {
            var serviceRoot = worldRoot.GetOptionalChild("grazing_manager");

            _exhaustion.Clear();


            foreach (var exhaustionRoot in serviceRoot.GetChildren("exhaustion"))
            {
                var pos = CellPosition.DeserializeFrom(exhaustionRoot.GetChild("cell_pos"));
                var exhaustion = exhaustionRoot.GetDouble("val");

                var cell = _worldManager.GetCell(pos);

                if(cell != null)
                {
                    _exhaustion.Add(cell, exhaustion);
                }
            }
        }

        public void PostLoad(XElement element)
        {
        }

        public void SaveCustomData(SerializedObject worldRoot)
        {
            var serviceRoot = worldRoot.CreateChild("grazing_manager");

            foreach (var kvp in _exhaustion)
            {
                var exhaustionRoot = serviceRoot.CreateChild("exhaustion");

                kvp.Key.Position.SerializeTo(exhaustionRoot.CreateChild("cell_pos"));
                exhaustionRoot.Set("val", kvp.Value);
            }
        }

        public void StepSimulation(double elapsedMonths)
        {
            foreach(var cell in _exhaustion.Keys.ToArray())
            {
                _exhaustion[cell] -= elapsedMonths;

                if(_exhaustion[cell] <= 0)
                {
                    _exhaustion.Remove(cell);

                    if(_transformsByTo.ContainsKey(cell.Biome))
                    {
                        cell.Biome = _transformsByTo[cell.Biome].From;
                    }
                }
                else if(_transformsByFrom.ContainsKey(cell.Biome))
                {
                    if(_exhaustion[cell] > _transformsByFrom[cell.Biome].Threshold)
                    {
                        cell.Biome = _transformsByFrom[cell.Biome].To;
                    }
                }
            }
        }

        private class ExhaustionTransformation
        {
            public Biome From { get; set; }
            public Biome To { get; set; }
            public double Threshold { get; set; }
        }
    }
}
