﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.Services
{
    public class DiplomacyHelper : IDiplomacyHelper
    {
        private readonly INameSource _nameSource;
        private readonly IPlayerDataTracker _playerData;
        private readonly IWorldManager _worldManager;
        private readonly IWarHelper _warHelper;
        private readonly ITreatyHelper _treatyHelper;
        private readonly IGamedataTracker _gamedataTracker;

        public DiplomacyHelper(
            INameSource nameSource,
            IPlayerDataTracker playerData,
            IWorldManager worldManager,
            IWarHelper warHelper,
            ITreatyHelper treatyHelper,
            IGamedataTracker gamedataTracker)
        {
            _nameSource = nameSource;
            _playerData = playerData;
            _worldManager = worldManager;
            _warHelper = warHelper;
            _treatyHelper = treatyHelper;
            _gamedataTracker = gamedataTracker;
        }

        public string GenerateText(
            Tribe aiTribe, 
            string pattern, 
            IEnumerable<string> additionalContext = null,
            Dictionary<string, string> additionalVariables = null)
        {
            var context = new List<string>
            {
                aiTribe.Race.Name,
                "player_" + _playerData.PlayerTribe.Race.Name
            };
            context.AddRange(additionalContext ?? Enumerable.Empty<string>());

            var relativePower = GetRelativePower(aiTribe, _playerData.PlayerTribe);
            if(relativePower > 0.66)
            {
                context.Add("power_stronger");
            }
            else if (relativePower < 0.33)
            {
                context.Add("power_weaker");
            }
            else
            {
                context.Add("power_same");
            }

            var trust = GetTrustFor(aiTribe);
            if(trust > 30)
            {
                context.Add("trust_high");
            }
            else if(trust > -30)
            {
                context.Add("trust_neutral");
            }
            else
            {
                context.Add("trust_low");
            }

            var influence = GetInfluenceFor(aiTribe);
            if (influence > 30)
            {
                context.Add("influence_high");
            }
            else if (influence > 0)
            {
                context.Add("influence_neutral");
            }
            else
            {
                context.Add("influence_low");
            }

            if (_warHelper.AreAtWar(_playerData.PlayerTribe, aiTribe))
            {
                context.Add("hostility_war");
            }

            var treatyValue = SumTreatyValues(aiTribe, _playerData.PlayerTribe);
            if(treatyValue > 0)
            {
                context.Add("treaty_positive");
            }
            else if(treatyValue < 0)
            {
                context.Add("treaty_negative");
            }
            else
            {
                context.Add("treaty_neutral");
            }

            if (_treatyHelper.GetTreatiesFor(aiTribe, _playerData.PlayerTribe).Any(t => t.IsCoercing(aiTribe)))
            {
                context.Add("treaty_coercing");
            }

            var variables = new Dictionary<string, string>
            {
                {"own_ruler", aiTribe.Ruler.Name },
                {"own_tribe", aiTribe.Name },
                {"own_race", aiTribe.Race.Name },
                {"player_ruler", _playerData.PlayerTribe.Ruler.Name },
                {"player_tribe", _playerData.PlayerTribe.Name },
                {"player_race", _playerData.PlayerTribe.Race.Name }
            };

            if (additionalVariables != null)
            {
                foreach (var variable in additionalVariables)
                {
                    variables.Add(variable.Key, variable.Value);
                }
            }
            
            return _nameSource.EvaluateDialog(
                pattern,
                context,
                variables);
        }

        public int GetInfluenceFor(Tribe tribe)
        {
            var influence = 0.0;

            for (var i = 0; i < tribe.DiplomaticMemories.Count; i++)
            {
                var memory = tribe.DiplomaticMemories[i];
                if (memory != null)
                {
                    var strength = memory.GetStrengthAt(_worldManager.Month);

                    if (strength != 0)
                    {
                        influence += strength * memory.Influence;
                    }
                }
            }

            return (int)Math.Round(influence, 0);
        }

        public double GetRelativePower(Tribe from, Tribe to)
        {
            var fromPower = Math.Max(1, SumPower(from));
            var toPower = Math.Max(1, SumPower(to));

            return fromPower / (fromPower + toPower);
        }

        private double SumPower(Tribe tribe)
        {
            var recruitStrength = 0.0;

            foreach (var settlement in tribe.Settlements)
            {
                foreach (var category in _gamedataTracker.RecruitmentSlotCategories)
                {
                    if (settlement.RecruitmentSlots.ContainsKey(category))
                    {
                        recruitStrength += category.StrengthEstimate * (int)settlement.RecruitmentSlots[category];
                    }
                }
            }
            
            var armyStrength = tribe.Armies
               .Select(a => a.StrengthEstimate)
               .Sum();

            return recruitStrength + armyStrength;
        }

        public int GetTrustFor(Tribe tribe)
        {
            var trust = 0.0;
            
            for(var i = 0; i < tribe.DiplomaticMemories.Count; i++)
            {
                var memory = tribe.DiplomaticMemories[i];
                if (memory != null)
                {
                    var strength = memory.GetStrengthAt(_worldManager.Month);

                    if (strength != 0)
                    {
                        trust += strength * memory.Trust;
                    }
                }
            }

            return (int)Math.Round(trust, 0);
        }

        public double GetTrustMod(Tribe aiTribe)
        {
            var trust = GetTrustFor(aiTribe);

            if(trust < 0)
            {
                return -trust / 100.0;
            }
            else
            {
                return -trust / 200.0;
            }
        }

        public double ScoreConvoCheck(NotablePersonStatType statType, Tribe aiTribe)
        {
            var aiStat = aiTribe.Ruler.GetStat(statType);
            var playerStat = _playerData.PlayerTribe.Ruler.GetStat(statType);

            return (playerStat - aiStat) * 0.05 + 0.5;
        }

        public int SumTreatyValues(Tribe fromTribe, Tribe toTribe)
        {
            int sum;
            if(fromTribe == _playerData.PlayerTribe)
            {
                sum = Math.Max(0, GetInfluenceFor(toTribe));
            }
            else
            {
                sum = Math.Max(0, GetInfluenceFor(fromTribe));
            }

            foreach (var treaty in _treatyHelper.GetTreatiesFor(fromTribe, toTribe))
            {
                sum += treaty.GetValueDetailFor(fromTribe).Sum(tv => tv.Value);
            }

            return sum;
        }
    }
}
