﻿using Bronze.Common.Data.World.CellDevelopments;
using Bronze.Contracts;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Bronze.Common.Services
{
    public class HerdSpawner : IDataLoader, IWorldStateService, ISubSimulator
    {
        public string ElementName => "herd_spawner";

        private readonly IXmlReaderUtil _xmlReaderUtil;
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IWorldManager _worldManager;
        private readonly IUnitHelper _unitHelper;
        private readonly IPlayerDataTracker _playerData;
        private readonly INotificationsSystem _notificationsSystem;
        private readonly IGenericNotificationBuilder _genericNotificationBuilder;
        private readonly Dictionary<Race, SpawnParameters> _spawnParams;

        private double _timeSinceCheck;

        public HerdSpawner(
            IXmlReaderUtil xmlReaderUtil,
            IGamedataTracker gamedataTracker,
            IWorldManager worldManager,
            IUnitHelper unitHelper,
            IPlayerDataTracker playerData,
            INotificationsSystem notificationsSystem,
            IGenericNotificationBuilder genericNotificationBuilder)
        {
            _xmlReaderUtil = xmlReaderUtil;
            _gamedataTracker = gamedataTracker;
            _worldManager = worldManager;
            _unitHelper = unitHelper;
            _playerData = playerData;
            _notificationsSystem = notificationsSystem;
            _genericNotificationBuilder = genericNotificationBuilder;
            _spawnParams = new Dictionary<Race, SpawnParameters>();
        }
        
        public void Clear()
        {
            _timeSinceCheck = 0.9;
        }
        
        public void Initialize(WorldState worldState)
        {
        }

        public void Load(XElement element)
        {
            foreach(var spawnElement in element.Elements("for_race"))
            {
                var race = _xmlReaderUtil.ObjectReferenceLookup(
                    spawnElement,
                    "race",
                    _gamedataTracker.Races,
                    b => b.Id);

                var param = new SpawnParameters
                {
                    Race = race,
                    CasteToTake = _xmlReaderUtil.ObjectReferenceLookup(
                        spawnElement,
                        "caste",
                        race.Castes,
                        c => c.Id),
                    HerderType = _xmlReaderUtil.ObjectReferenceLookup(
                        spawnElement,
                        "herder_unit",
                        _gamedataTracker.UnitTypes,
                        b => b.Id),
                    PopPercToTake = _xmlReaderUtil.AttributeAsDouble(
                        spawnElement, 
                        "pop_perc"),
                    PopPerUnit = _xmlReaderUtil.AttributeAsDouble(
                        spawnElement, 
                        "pop_per_unit")
                };

                if (_spawnParams.ContainsKey(param.Race))
                {
                    _spawnParams[param.Race] = param;
                }
                else
                {
                    _spawnParams.Add(param.Race, param);
                }
            }
        }

        public void LoadCustomData(SerializedObject worldRoot)
        {
        }

        public void PostLoad(XElement element)
        {
        }

        public void SaveCustomData(SerializedObject worldRoot)
        {
        }

        public void StepSimulation(double elapsedMonths)
        {
            _timeSinceCheck += elapsedMonths;

            if(_timeSinceCheck > 1)
            {
                _timeSinceCheck = 0;
                
                foreach(var tribe in _worldManager.Tribes)
                {
                    foreach(var settlement in tribe.Settlements)
                    {
                        if(_spawnParams.ContainsKey(settlement.Race) && !settlement.IsRemoved)
                        {
                            var param = _spawnParams[settlement.Race];

                            var localHerders = settlement.Region.WorldActors
                                .OfType<Army>()
                                .Where(a => a.Owner == settlement.Owner)
                                .Where(a => a.Units.OfType<IGrazingUnit>().Any())
                                .Any();

                            if(!localHerders && settlement.PopulationByCaste[param.CasteToTake] > 2)
                            {
                                var popToTake = settlement.PopulationByCaste[param.CasteToTake] * param.PopPercToTake;
                                var unitsToMake = Math.Floor(popToTake / param.PopPerUnit);

                                if(unitsToMake > 0)
                                {
                                    var popsToRemove = unitsToMake * param.PopPerUnit;
                                    for(var i = 0; i < popsToRemove; i++)
                                    {
                                        var pop = settlement.Population
                                            .Where(p => p.Caste == param.CasteToTake)
                                            .FirstOrDefault();

                                        if (pop != null)
                                        {
                                            settlement.Population.Remove(pop);
                                        }
                                    }

                                    settlement.SetCalculationFlag("pop removed for herd spawning");
                                    
                                    var spawnLoc = settlement.EconomicActors
                                        .OfType<ICellDevelopment>()
                                        .Where(cd => cd is DistrictDevelopment || cd is VillageDevelopment)
                                        .FirstOrDefault();

                                    if (spawnLoc != null)
                                    {
                                        var army = _unitHelper.CreateArmy(settlement.Owner, spawnLoc.Cell);

                                        for(var i = 0; i < unitsToMake; i++)
                                        {
                                            var unit = param.HerderType.CreateUnit(param.HerderType.EquipmentSets.First());
                                            unit.Supplies = 1;
                                            army.Units.Add(unit);
                                        }

                                        if (settlement.Owner == _playerData.PlayerTribe)
                                        {
                                            _notificationsSystem.AddNotification(
                                                _genericNotificationBuilder.BuildSimpleNotification(
                                                    param.HerderType.EquipmentSets.First().IconKey,
                                                    $"Villagers Leave {settlement.Name}",
                                                    "Villagers have left a settlement to start herding.",
                                                    $"Villagers Leave {settlement.Name}",
                                                    $"Some villagers have left {settlement.Name} to start raising herds in the nearby land.",
                                                    spawnLoc.Cell.Position));
                                    }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private class SpawnParameters
        {
            public Race Race { get; set; }
            public IUnitType HerderType { get; set; }
            public Caste CasteToTake { get; set; }
            public double PopPerUnit { get; set; }
            public double PopPercToTake { get; set; }
        }
    }
}
