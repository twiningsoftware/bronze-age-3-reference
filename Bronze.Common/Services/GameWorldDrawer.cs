﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI;

namespace Bronze.Common.Services
{
    public class GameWorldDrawer : IGameWorldDrawer
    {
        private const float MAP_SCALE_FACTOR = 4f;
        public static readonly Vector2 UNIT_LAYERING_POS_OFFSET = new Vector2(0, -Constants.LARGE_HEX_VERTICAL_STEP / 2);

        private readonly IPlayerDataTracker _playerData;
        private readonly IWorldManager _worldManager;
        private readonly IUserSettings _userSettings;
        private readonly IAudioService _audioService;
        private readonly IGameInterface _gameInterface;
        private readonly IPerformanceTracker _performanceTracker;
        private readonly IWorldEffectManager _worldEffectManager;
        
        private Vector2 _screenCenter;
        private float _displayHeight;
        
        public GameWorldDrawer(
            IPlayerDataTracker playerData,
            IWorldManager worldManager,
            IUserSettings userSettings,
            IAudioService audioService,
            IGameInterface gameInterface,
            IPerformanceTracker performanceTracker,
            IWorldEffectManager worldEffectManager)
        {
            _playerData = playerData;
            _worldManager = worldManager;
            _userSettings = userSettings;
            _audioService = audioService;
            _gameInterface = gameInterface;
            _performanceTracker = performanceTracker;
            _worldEffectManager = worldEffectManager;
            _screenCenter = Vector2.Zero;
            _displayHeight = 1;
        }
        
        public void DrawWorld(
            IDrawingService drawingService,
            IEnumerable<IMapInteractionComponent> mapInteractionComponents)
        {
            _audioService.ClearAmbience();

            _displayHeight = drawingService.DisplayHeight;

            _screenCenter = new Vector2(
                drawingService.DisplayWidth / 2,
                drawingService.DisplayHeight / 2);

            switch(_playerData.ViewMode)
            {
                case ViewMode.Settlement:
                    DrawSettlementAsDetailed(drawingService, mapInteractionComponents);
                    break;
                case ViewMode.Summary: // TODO special rendering
                case ViewMode.Detailed:
                    DrawWorldAsDetailed(drawingService, mapInteractionComponents);
                    break;
            }
        }

        private void DrawWorldAsDetailed(
            IDrawingService drawingService,
            IEnumerable<IMapInteractionComponent> mapInteractionComponents)
        {
            var stopwatch = Stopwatch.StartNew();
            var margin = 3;

            var viewport = Rectangle.AroundCenter(
               new Vector2(
                   _playerData.ViewCenter.X / Constants.CELL_SIZE_PIXELS,
                   _playerData.ViewCenter.Y / Constants.CELL_SIZE_PIXELS),
               (int)(drawingService.DisplayWidth / Constants.CELL_SIZE_PIXELS / Util.ViewModeToScale(_playerData.ViewMode)) + margin,
               (int)(drawingService.DisplayHeight / Constants.CELL_SIZE_PIXELS / Util.ViewModeToScale(_playerData.ViewMode)) + margin);

            var cellsToDraw = _worldManager.GetCells(_playerData.CurrentPlane, viewport);
            _performanceTracker.LogRenderTime("get cells", stopwatch.ElapsedTicks);
            stopwatch.Restart();
            foreach (var component in mapInteractionComponents)
            {
                component.DrawWorld(drawingService, cellsToDraw, _playerData.ViewMode);
            }
            _performanceTracker.LogRenderTime("interaction components", stopwatch.ElapsedTicks);
            stopwatch.Restart();

            var perCellStopwatch = new Stopwatch();
            var getLayerTicks = 0L;
            var drawLayerTicks = 0L;
            var drawBorderTicks = 0L;
            foreach (var cell in cellsToDraw)
            {
                stopwatch.Reset();
                try
                {
                    if (_playerData.PlayerTribe.IsExplored(cell))
                    {
                        var layers = Enumerable.Empty<CellAnimationLayer>();

                        switch(_playerData.ViewMode)
                        {
                            case ViewMode.Detailed:
                                layers = cell.DetailLayers;
                                break;
                            case ViewMode.Summary:
                                layers = cell.SummaryLayers;
                                break;
                        }

                        // Don't add sound effects if the game is paused
                        if (!_userSettings.SimulationPaused)
                        {
                            foreach(var layer in layers)
                            {
                                foreach (var effect in layer.SoundEffects.Where(e => e.Mode == SoundEffectMode.Ambient))
                                {
                                    _audioService.AddAmbience(effect.SoundKey);
                                }
                            }
                        }

                        getLayerTicks += stopwatch.ElapsedTicks;
                        stopwatch.Restart();
                        
                        DrawCell(drawingService, cell, layers, _playerData.ViewMode);

                        drawLayerTicks += stopwatch.ElapsedTicks;
                        stopwatch.Restart();

                        if (_userSettings.ShowRegionBorders && cell.IsOnBorder)
                        {
                            var adjacencies = new bool[8];
                            var internalAdjacencies = new bool[8];
                            for (var i = 0; i < adjacencies.Length; i++)
                            {
                                var neighbor = cell.GetNeighbor((Facing)i);

                                adjacencies[i] = cell.Owner != null && neighbor != null && neighbor.Region != cell.Region && neighbor.Owner != cell.Owner;

                                internalAdjacencies[i] = neighbor != null && neighbor.Owner == cell.Owner && neighbor.Region != cell.Region;
                            }

                            var primaryColorization = BronzeColor.White;
                            var secondaryColorization = BronzeColor.White;
                            if (cell.Region.Settlement != null)
                            {
                                primaryColorization = cell.Region.Settlement.Owner.PrimaryColor;
                                secondaryColorization = cell.Region.Settlement.Owner.SecondaryColor;
                            }

                            switch (_playerData.ViewMode)
                            {
                                case ViewMode.Detailed:
                                    DrawCellAsAdjacency(
                                        drawingService,
                                        "cell_regionborders_internal",
                                        10,
                                        cell,
                                        internalAdjacencies,
                                        secondaryColorization,
                                        Constants.CELL_SIZE_PIXELS,
                                        false,
                                        0);
                                    DrawCellAsAdjacency(
                                        drawingService,
                                        "cell_regionborders",
                                        11,
                                        cell,
                                        adjacencies,
                                        secondaryColorization,
                                        Constants.CELL_SIZE_PIXELS,
                                        false,
                                        0);
                                    DrawCellAsAdjacency(
                                        drawingService,
                                        "cell_regionborders_inner",
                                        12,
                                        cell,
                                        adjacencies,
                                        primaryColorization,
                                        Constants.CELL_SIZE_PIXELS,
                                        false,
                                        0);
                                    break;
                                case ViewMode.Summary:
                                    DrawCellAsAdjacency(
                                        drawingService,
                                        "cell_regionborders_summary_internal",
                                        10,
                                        cell,
                                        internalAdjacencies,
                                        secondaryColorization,
                                        Constants.CELL_SIZE_SUMMARY_PIXELS,
                                        false,
                                        0);
                                    DrawCellAsAdjacency(
                                        drawingService,
                                        "cell_regionborders_summary",
                                        11,
                                        cell,
                                        adjacencies,
                                        secondaryColorization,
                                        Constants.CELL_SIZE_SUMMARY_PIXELS,
                                        false,
                                        0);
                                    DrawCellAsAdjacency(
                                        drawingService,
                                        "cell_regionborders_summary_inner",
                                        12,
                                        cell,
                                        adjacencies,
                                        primaryColorization,
                                        Constants.CELL_SIZE_SUMMARY_PIXELS,
                                        false,
                                        0);
                                    break;
                            }

                            drawBorderTicks += stopwatch.ElapsedTicks;
                            stopwatch.Restart();
                        }
                        
                        if(_playerData.PlayerTribe.IsVisible(cell))
                        {
                            foreach(var army in cell.WorldActors)
                            {
                                DrawWorldActor(drawingService, army);
                            }
                        }
                        else
                        {
                            switch (_playerData.ViewMode)
                            {
                                case ViewMode.Detailed:
                                    DrawCellAsSimple(
                                        drawingService,
                                        "cell_explored_mask",
                                        10,
                                        cell.Position,
                                        BronzeColor.None,
                                        Constants.CELL_SIZE_PIXELS,
                                        true,
                                        0);
                                    break;
                                case ViewMode.Summary:
                                    DrawCellAsSimple(
                                        drawingService,
                                        "cell_explored_mask_summary",
                                        10,
                                        cell.Position,
                                        BronzeColor.None,
                                        Constants.CELL_SIZE_SUMMARY_PIXELS,
                                        false,
                                        0);
                                    break;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    _gameInterface.OnException(ex);
                }
            }
            _performanceTracker.LogRenderTime("draw cells", stopwatch.ElapsedTicks);
            stopwatch.Restart();
            perCellStopwatch.Stop();

            foreach(var effect in _worldEffectManager.GetActiveEffects())
            {
                if (effect.PlaneId == _playerData.CurrentPlane)
                {
                    effect.Draw(drawingService);
                }
            }
            _performanceTracker.LogRenderTime("draw effects", stopwatch.ElapsedTicks);
            stopwatch.Stop();
            

            _performanceTracker.LogRenderTime("draw cells: get layers", getLayerTicks);
            _performanceTracker.LogRenderTime("draw cells: draw layers", drawLayerTicks);
            _performanceTracker.LogRenderTime("draw cells: draw borders", drawBorderTicks);
        }

        #region Cell drawing methods
        public void DrawCell(
            IDrawingService drawingService, 
            Cell cell, 
            IEnumerable<CellAnimationLayer> layers, 
            ViewMode viewMode)
        {
            var colorization = cell.Region.Settlement?.Owner?.PrimaryColor ?? BronzeColor.None;

            var adjacencies = new bool[8];

            int drawSize = 1;
            switch(viewMode)
            {
                case ViewMode.Detailed:
                    drawSize = Constants.CELL_SIZE_PIXELS;
                    break;
                case ViewMode.Summary:
                    drawSize = Constants.CELL_SIZE_SUMMARY_PIXELS;
                    break;
            }

            foreach (var animationLayer in layers)
            {
                if (animationLayer.Type != SpriteType.Simple)
                {
                    for (var i = 0; i < adjacencies.Length; i++)
                    {
                        var neighbor = cell.GetNeighbor((Facing)i);

                        if (neighbor != null)
                        {
                            adjacencies[i] = neighbor.AdjacencyGroups.Contains(animationLayer.Adjacency);
                        }
                        else
                        {
                            adjacencies[i] = _worldManager.EdgeAdjacencies.Contains(animationLayer.Adjacency);
                        }
                    }
                }

                if (animationLayer.Type == SpriteType.Adjacency)
                {
                    DrawCellAsAdjacency(
                        drawingService, 
                        animationLayer.ImageKey, 
                        animationLayer.DrawLayer, 
                        cell, 
                        adjacencies,
                        colorization,
                        drawSize,
                        viewMode == ViewMode.Detailed,
                        drawingService.GetFrameForAnimation(animationLayer.FrameCount, animationLayer.LoopTime));
                }
                else if (animationLayer.Type == SpriteType.Tiles || animationLayer.Type == SpriteType.AutoVariantTiles)
                {
                    if (viewMode == ViewMode.Detailed)
                    {
                        DrawCellAsTiles(
                            drawingService,
                            animationLayer.ImageKey,
                            animationLayer.DrawLayer,
                            cell,
                            adjacencies,
                            colorization,
                            drawingService.GetFrameForAnimation(animationLayer.FrameCount, animationLayer.LoopTime),
                            doVariations: animationLayer.Type == SpriteType.AutoVariantTiles);
                    }
                    else if (viewMode == ViewMode.Summary)
                    {
                        drawingService.OptimizedTileDraw(
                            animationLayer.ImageKey,
                            colorization,
                            cell.Position.ToVector() * Constants.TILE_SIZE_PIXELS,
                            animationLayer.DrawLayer,
                            adjacencies,
                            true,
                            TileLayout.Standard);
                    }
                }
                else if (animationLayer.Type == SpriteType.Simple)
                {
                    DrawCellAsSimple(
                        drawingService, 
                        animationLayer.ImageKey, 
                        animationLayer.DrawLayer, 
                        cell.Position,
                        colorization,
                        drawSize,
                        viewMode == ViewMode.Detailed,
                        drawingService.GetFrameForAnimation(animationLayer.FrameCount, animationLayer.LoopTime));
                }
            }
        }

        private void DrawCellAsTiles(
            IDrawingService drawingService,
            string imageKey,
            float drawLayer,
            Cell cell,
            bool[] adjacencies,
            BronzeColor colorization,
            int frame,
            bool doVariations)
        {
            var xOffset = 320 * frame;

            if(doVariations)
            {
                imageKey = imageKey + "_v" + cell.TileVariation;
            }

            //// Draw Center
            drawingService.DrawAnimationFrame(
                imageKey,
                colorization,
                new Rectangle(xOffset, 32, 64, 64),
                cell.Position.ToVector() * Constants.CELL_SIZE_PIXELS,
                1,
                false,
                drawLayer + 0.0f,
                true);

            if (adjacencies[(int)Facing.North])
            {
                drawingService.DrawAnimationFrame(
                    imageKey,
                    colorization,
                    new Rectangle(xOffset + 224, 0, 64, 64),
                    (cell.Position.ToVector() * Constants.CELL_SIZE_PIXELS) + new Vector2(0, -64),
                    1,
                    false,
                    drawLayer - 0.1f,
                    true);
            }
            else
            {
                drawingService.DrawAnimationFrame(
                    imageKey,
                    colorization,
                    new Rectangle(xOffset + 96, 0, 64, 64),
                    (cell.Position.ToVector() * Constants.CELL_SIZE_PIXELS) + new Vector2(0, -64),
                    1,
                    false,
                    drawLayer - 0.1f,
                    true);
            }

            if (adjacencies[(int)Facing.South])
            {
                drawingService.DrawAnimationFrame(
                    imageKey,
                    colorization,
                    new Rectangle(xOffset + 224, 192, 64, 64),
                    (cell.Position.ToVector() * Constants.CELL_SIZE_PIXELS) + new Vector2(0, 32),
                    1,
                    false,
                    drawLayer + 0.2f,
                    true);
            }
            else
            {
                drawingService.DrawAnimationFrame(
                    imageKey,
                    colorization,
                    new Rectangle(xOffset + 96, 192, 64, 64),
                    (cell.Position.ToVector() * Constants.CELL_SIZE_PIXELS) + new Vector2(0, 32),
                    1,
                    false,
                    drawLayer + 0.2f,
                    true);
            }

            if (adjacencies[(int)Facing.West])
            {
                drawingService.DrawAnimationFrame(
                    imageKey,
                    colorization,
                    new Rectangle(xOffset + 96, 96, 32, 32),
                    (cell.Position.ToVector() * Constants.CELL_SIZE_PIXELS) + new Vector2(-48, -16),
                    1,
                    false,
                    drawLayer + 0.0f,
                    true);

                drawingService.DrawAnimationFrame(
                    imageKey,
                    colorization,
                    new Rectangle(xOffset + 96, 160, 32, 32),
                    (cell.Position.ToVector() * Constants.CELL_SIZE_PIXELS) + new Vector2(-48, 16),
                    1,
                    false,
                    drawLayer + 0.1f,
                    true);
            }
            else
            {
                drawingService.DrawAnimationFrame(
                    imageKey,
                    colorization,
                    new Rectangle(xOffset + 64, 96, 32, 32),
                    (cell.Position.ToVector() * Constants.CELL_SIZE_PIXELS) + new Vector2(-48, -16),
                    1,
                    false,
                    drawLayer + 0.0f,
                    true);

                drawingService.DrawAnimationFrame(
                    imageKey,
                    colorization,
                    new Rectangle(xOffset + 64, 160, 32, 32),
                    (cell.Position.ToVector() * Constants.CELL_SIZE_PIXELS) + new Vector2(-48, 16),
                    1,
                    false,
                    drawLayer + 0.1f,
                    true);
            }

            if (adjacencies[(int)Facing.East])
            {
                drawingService.DrawAnimationFrame(
                    imageKey,
                    colorization,
                    new Rectangle(xOffset + 128, 96, 32, 32),
                    (cell.Position.ToVector() * Constants.CELL_SIZE_PIXELS) + new Vector2(48, -16),
                    1,
                    false,
                    drawLayer + 0.0f,
                    true);

                drawingService.DrawAnimationFrame(
                    imageKey,
                    colorization,
                    new Rectangle(xOffset + 128, 160, 32, 32),
                    (cell.Position.ToVector() * Constants.CELL_SIZE_PIXELS) + new Vector2(48, 16),
                    1,
                    false,
                    drawLayer + 0.1f,
                    true);
            }
            else
            {
                drawingService.DrawAnimationFrame(
                    imageKey,
                    colorization,
                    new Rectangle(xOffset + 160, 96, 32, 32),
                    (cell.Position.ToVector() * Constants.CELL_SIZE_PIXELS) + new Vector2(48, -16),
                    1,
                    false,
                    drawLayer + 0.0f,
                    true);

                drawingService.DrawAnimationFrame(
                    imageKey,
                    colorization,
                    new Rectangle(xOffset + 160, 160, 32, 32),
                    (cell.Position.ToVector() * Constants.CELL_SIZE_PIXELS) + new Vector2(48, 16),
                    1,
                    false,
                    drawLayer + 0.1f,
                    true);
            }

            if (adjacencies[(int)Facing.NorthWest] && adjacencies[(int)Facing.North] && adjacencies[(int)Facing.West])
            {
                drawingService.DrawAnimationFrame(
                    imageKey,
                    colorization,
                    new Rectangle(xOffset + 192, 0, 32, 64),
                    (cell.Position.ToVector() * Constants.CELL_SIZE_PIXELS) + new Vector2(-48, -64),
                    1,
                    false,
                    drawLayer - 0.1f,
                    true);
            }
            else if (adjacencies[(int)Facing.NorthWest] && adjacencies[(int)Facing.North] && !adjacencies[(int)Facing.West])
            {
                drawingService.DrawAnimationFrame(
                    imageKey,
                    colorization,
                    new Rectangle(xOffset + 192, 64, 32, 64),
                    (cell.Position.ToVector() * Constants.CELL_SIZE_PIXELS) + new Vector2(-48, -64),
                    1,
                    false,
                    drawLayer - 0.1f,
                    true);
            }
            else if (adjacencies[(int)Facing.NorthWest] && !adjacencies[(int)Facing.North] && adjacencies[(int)Facing.West])
            {
                drawingService.DrawAnimationFrame(
                    imageKey,
                    colorization,
                    new Rectangle(xOffset + 224, 64, 32, 64),
                    (cell.Position.ToVector() * Constants.CELL_SIZE_PIXELS) + new Vector2(-48, -64),
                    1,
                    false,
                    drawLayer - 0.1f,
                    true);
            }
            else if (adjacencies[(int)Facing.NorthWest] && !adjacencies[(int)Facing.North] && !adjacencies[(int)Facing.West])
            {
                drawingService.DrawAnimationFrame(
                    imageKey,
                    colorization,
                    new Rectangle(xOffset + 64, 0, 32, 64),
                    (cell.Position.ToVector() * Constants.CELL_SIZE_PIXELS) + new Vector2(-48, -64),
                    1,
                    false,
                    drawLayer - 0.1f,
                    true);
            }
            else if (!adjacencies[(int)Facing.NorthWest] && adjacencies[(int)Facing.North] && adjacencies[(int)Facing.West])
            {
                drawingService.DrawAnimationFrame(
                    imageKey,
                    colorization,
                    new Rectangle(xOffset + 32, 160, 32, 64),
                    (cell.Position.ToVector() * Constants.CELL_SIZE_PIXELS) + new Vector2(-48, -64),
                    1,
                    false,
                    drawLayer - 0.1f,
                    true);
            }
            else if (!adjacencies[(int)Facing.NorthWest] && adjacencies[(int)Facing.North] && !adjacencies[(int)Facing.West])
            {
                drawingService.DrawAnimationFrame(
                    imageKey,
                    colorization,
                    new Rectangle(xOffset + 192, 64, 32, 64),
                    (cell.Position.ToVector() * Constants.CELL_SIZE_PIXELS) + new Vector2(-48, -64),
                    1,
                    false,
                    drawLayer - 0.1f,
                    true);
            }
            else if (!adjacencies[(int)Facing.NorthWest] && !adjacencies[(int)Facing.North] && adjacencies[(int)Facing.West])
            {
                drawingService.DrawAnimationFrame(
                    imageKey,
                    colorization,
                    new Rectangle(xOffset + 224, 64, 32, 64),
                    (cell.Position.ToVector() * Constants.CELL_SIZE_PIXELS) + new Vector2(-48, -64),
                    1,
                    false,
                    drawLayer - 0.1f,
                    true);
            }
            else if (!adjacencies[(int)Facing.NorthWest] && !adjacencies[(int)Facing.North] && !adjacencies[(int)Facing.West])
            {
                drawingService.DrawAnimationFrame(
                    imageKey,
                    colorization,
                    new Rectangle(xOffset + 64, 0, 32, 64),
                    (cell.Position.ToVector() * Constants.CELL_SIZE_PIXELS) + new Vector2(-48, -64),
                    1,
                    false,
                    drawLayer - 0.1f,
                    true);
            }

            if (adjacencies[(int)Facing.NorthEast] && adjacencies[(int)Facing.North] && adjacencies[(int)Facing.East])
            {
                drawingService.DrawAnimationFrame(
                    imageKey,
                    colorization,
                    new Rectangle(xOffset + 288, 0, 32, 64),
                    (cell.Position.ToVector() * Constants.CELL_SIZE_PIXELS) + new Vector2(48, -64),
                    1,
                    false,
                    drawLayer - 0.1f,
                    true);
            }
            else if (adjacencies[(int)Facing.NorthEast] && adjacencies[(int)Facing.North] && !adjacencies[(int)Facing.East])
            {
                drawingService.DrawAnimationFrame(
                    imageKey,
                    colorization,
                    new Rectangle(xOffset + 288, 64, 32, 64),
                    (cell.Position.ToVector() * Constants.CELL_SIZE_PIXELS) + new Vector2(48, -64),
                    1,
                    false,
                    drawLayer - 0.1f,
                    true);
            }
            else if (adjacencies[(int)Facing.NorthEast] && !adjacencies[(int)Facing.North] && adjacencies[(int)Facing.East])
            {
                drawingService.DrawAnimationFrame(
                    imageKey,
                    colorization,
                    new Rectangle(xOffset + 256, 64, 32, 64),
                    (cell.Position.ToVector() * Constants.CELL_SIZE_PIXELS) + new Vector2(48, -64),
                    1,
                    false,
                    drawLayer - 0.1f,
                    true);
            }
            else if (adjacencies[(int)Facing.NorthEast] && !adjacencies[(int)Facing.North] && !adjacencies[(int)Facing.East])
            {
                drawingService.DrawAnimationFrame(
                    imageKey,
                    colorization,
                    new Rectangle(xOffset + 160, 0, 32, 64),
                    (cell.Position.ToVector() * Constants.CELL_SIZE_PIXELS) + new Vector2(48, -64),
                    1,
                    false,
                    drawLayer - 0.1f,
                    true);
            }
            else if (!adjacencies[(int)Facing.NorthEast] && adjacencies[(int)Facing.North] && adjacencies[(int)Facing.East])
            {
                drawingService.DrawAnimationFrame(
                    imageKey,
                    colorization,
                    new Rectangle(xOffset + 0, 160, 32, 64),
                    (cell.Position.ToVector() * Constants.CELL_SIZE_PIXELS) + new Vector2(48, -64),
                    1,
                    false,
                    drawLayer - 0.1f,
                    true);
            }
            else if (!adjacencies[(int)Facing.NorthEast] && adjacencies[(int)Facing.North] && !adjacencies[(int)Facing.East])
            {
                drawingService.DrawAnimationFrame(
                    imageKey,
                    colorization,
                    new Rectangle(xOffset + 288, 64, 32, 64),
                    (cell.Position.ToVector() * Constants.CELL_SIZE_PIXELS) + new Vector2(48, -64),
                    1,
                    false,
                    drawLayer - 0.1f,
                    true);
            }
            else if (!adjacencies[(int)Facing.NorthEast] && !adjacencies[(int)Facing.North] && adjacencies[(int)Facing.East])
            {
                drawingService.DrawAnimationFrame(
                    imageKey,
                    colorization,
                    new Rectangle(xOffset + 256, 64, 32, 64),
                    (cell.Position.ToVector() * Constants.CELL_SIZE_PIXELS) + new Vector2(48, -64),
                    1,
                    false,
                    drawLayer - 0.1f,
                    true);
            }
            else if (!adjacencies[(int)Facing.NorthEast] && !adjacencies[(int)Facing.North] && !adjacencies[(int)Facing.East])
            {
                drawingService.DrawAnimationFrame(
                    imageKey,
                    colorization,
                    new Rectangle(xOffset + 160, 0, 32, 64),
                    (cell.Position.ToVector() * Constants.CELL_SIZE_PIXELS) + new Vector2(48, -64),
                    1,
                    false,
                    drawLayer - 0.1f,
                    true);
            }

            if (adjacencies[(int)Facing.SouthWest] && adjacencies[(int)Facing.South] && adjacencies[(int)Facing.West])
            {
                drawingService.DrawAnimationFrame(
                    imageKey,
                    colorization,
                    new Rectangle(xOffset + 192, 192, 32, 64),
                    (cell.Position.ToVector() * Constants.CELL_SIZE_PIXELS) + new Vector2(-48, 32),
                    1,
                    false,
                    drawLayer + 0.2f,
                    true);
            }
            else if (adjacencies[(int)Facing.SouthWest] && adjacencies[(int)Facing.South] && !adjacencies[(int)Facing.West])
            {
                drawingService.DrawAnimationFrame(
                    imageKey,
                    colorization,
                    new Rectangle(xOffset + 192, 128, 32, 64),
                    (cell.Position.ToVector() * Constants.CELL_SIZE_PIXELS) + new Vector2(-48, 32),
                    1,
                    false,
                    drawLayer + 0.2f,
                    true);
            }
            else if (adjacencies[(int)Facing.SouthWest] && !adjacencies[(int)Facing.South] && adjacencies[(int)Facing.West])
            {
                drawingService.DrawAnimationFrame(
                    imageKey,
                    colorization,
                    new Rectangle(xOffset + 224, 128, 32, 64),
                    (cell.Position.ToVector() * Constants.CELL_SIZE_PIXELS) + new Vector2(-48, 32),
                    1,
                    false,
                    drawLayer + 0.2f,
                    true);
            }
            else if (adjacencies[(int)Facing.SouthWest] && !adjacencies[(int)Facing.South] && !adjacencies[(int)Facing.West])
            {
                drawingService.DrawAnimationFrame(
                    imageKey,
                    colorization,
                    new Rectangle(xOffset + 64, 192, 32, 64),
                    (cell.Position.ToVector() * Constants.CELL_SIZE_PIXELS) + new Vector2(-48, 32),
                    1,
                    false,
                    drawLayer + 0.2f,
                    true);
            }
            else if (!adjacencies[(int)Facing.SouthWest] && adjacencies[(int)Facing.South] && adjacencies[(int)Facing.West])
            {
                drawingService.DrawAnimationFrame(
                    imageKey,
                    colorization,
                    new Rectangle(xOffset + 32, 96, 32, 64),
                    (cell.Position.ToVector() * Constants.CELL_SIZE_PIXELS) + new Vector2(-48, 32),
                    1,
                    false,
                    drawLayer + 0.2f,
                    true);
            }
            else if (!adjacencies[(int)Facing.SouthWest] && adjacencies[(int)Facing.South] && !adjacencies[(int)Facing.West])
            {
                drawingService.DrawAnimationFrame(
                    imageKey,
                    colorization,
                    new Rectangle(xOffset + 192, 128, 32, 64),
                    (cell.Position.ToVector() * Constants.CELL_SIZE_PIXELS) + new Vector2(-48, 32),
                    1,
                    false,
                    drawLayer + 0.2f,
                    true);
            }
            else if (!adjacencies[(int)Facing.SouthWest] && !adjacencies[(int)Facing.South] && adjacencies[(int)Facing.West])
            {
                drawingService.DrawAnimationFrame(
                    imageKey,
                    colorization,
                    new Rectangle(xOffset + 224, 128, 32, 64),
                    (cell.Position.ToVector() * Constants.CELL_SIZE_PIXELS) + new Vector2(-48, 32),
                    1,
                    false,
                    drawLayer + 0.2f,
                    true);
            }
            else if (!adjacencies[(int)Facing.SouthWest] && !adjacencies[(int)Facing.South] && !adjacencies[(int)Facing.West])
            {
                drawingService.DrawAnimationFrame(
                    imageKey,
                    colorization,
                    new Rectangle(xOffset + 64, 192, 32, 64),
                    (cell.Position.ToVector() * Constants.CELL_SIZE_PIXELS) + new Vector2(-48, 32),
                    1,
                    false,
                    drawLayer + 0.2f,
                    true);
            }

            if (adjacencies[(int)Facing.SouthEast] && adjacencies[(int)Facing.South] && adjacencies[(int)Facing.East])
            {
                drawingService.DrawAnimationFrame(
                    imageKey,
                    colorization,
                    new Rectangle(xOffset + 288, 192, 32, 64),
                    (cell.Position.ToVector() * Constants.CELL_SIZE_PIXELS) + new Vector2(48, 32),
                    1,
                    false,
                    drawLayer + 0.2f,
                    true);
            }
            else if (adjacencies[(int)Facing.SouthEast] && adjacencies[(int)Facing.South] && !adjacencies[(int)Facing.East])
            {
                drawingService.DrawAnimationFrame(
                    imageKey,
                    colorization,
                    new Rectangle(xOffset + 288, 64, 32, 64),
                    (cell.Position.ToVector() * Constants.CELL_SIZE_PIXELS) + new Vector2(48, 32),
                    1,
                    false,
                    drawLayer + 0.2f,
                    true);
            }
            else if (adjacencies[(int)Facing.SouthEast] && !adjacencies[(int)Facing.South] && adjacencies[(int)Facing.East])
            {
                drawingService.DrawAnimationFrame(
                    imageKey,
                    colorization,
                    new Rectangle(xOffset + 256, 128, 32, 64),
                    (cell.Position.ToVector() * Constants.CELL_SIZE_PIXELS) + new Vector2(48, 32),
                    1,
                    false,
                    drawLayer + 0.2f,
                    true);
            }
            else if (adjacencies[(int)Facing.SouthEast] && !adjacencies[(int)Facing.South] && !adjacencies[(int)Facing.East])
            {
                drawingService.DrawAnimationFrame(
                    imageKey,
                    colorization,
                    new Rectangle(xOffset + 160, 192, 32, 64),
                    (cell.Position.ToVector() * Constants.CELL_SIZE_PIXELS) + new Vector2(48, 32),
                    1,
                    false,
                    drawLayer + 0.2f,
                    true);
            }
            else if (!adjacencies[(int)Facing.SouthEast] && adjacencies[(int)Facing.South] && adjacencies[(int)Facing.East])
            {
                drawingService.DrawAnimationFrame(
                    imageKey,
                    colorization,
                    new Rectangle(xOffset + 0, 96, 32, 64),
                    (cell.Position.ToVector() * Constants.CELL_SIZE_PIXELS) + new Vector2(48, 32),
                    1,
                    false,
                    drawLayer + 0.2f,
                    true);
            }
            else if (!adjacencies[(int)Facing.SouthEast] && adjacencies[(int)Facing.South] && !adjacencies[(int)Facing.East])
            {
                drawingService.DrawAnimationFrame(
                    imageKey,
                    colorization,
                    new Rectangle(xOffset + 288, 128, 32, 64),
                    (cell.Position.ToVector() * Constants.CELL_SIZE_PIXELS) + new Vector2(48, 32),
                    1,
                    false,
                    drawLayer + 0.2f,
                    true);
            }
            else if (!adjacencies[(int)Facing.SouthEast] && !adjacencies[(int)Facing.South] && adjacencies[(int)Facing.East])
            {
                drawingService.DrawAnimationFrame(
                    imageKey,
                    colorization,
                    new Rectangle(xOffset + 256, 128, 32, 64),
                    (cell.Position.ToVector() * Constants.CELL_SIZE_PIXELS) + new Vector2(48, 32),
                    1,
                    false,
                    drawLayer + 0.2f,
                    true);
            }
            else if (!adjacencies[(int)Facing.SouthEast] && !adjacencies[(int)Facing.South] && !adjacencies[(int)Facing.East])
            {
                drawingService.DrawAnimationFrame(
                    imageKey,
                    colorization,
                    new Rectangle(xOffset + 160, 192, 32, 64),
                    (cell.Position.ToVector() * Constants.CELL_SIZE_PIXELS) + new Vector2(48, 32),
                    1,
                    false,
                    drawLayer + 0.2f,
                    true);
            }
        }

        private void DrawCellAsAdjacency(
            IDrawingService drawingService,
            string imageKey,
            float drawLayer,
            Cell cell,
            bool[] adjacencies,
            BronzeColor colorization,
            int cellSize,
            bool drawInRows,
            int frame)
        {
            var rowHeight = cellSize / 4;
            var yOffset = cellSize / 8;
            var xOffset = cellSize * frame;

            var adjacencyDrawOrder = new[]
            {
                0,
                3,
                1,
                5,
                2,
                4,
                7,
                6,
                8
            };

            for (var i = 0; i < adjacencyDrawOrder.Length; i++)
            {
                var n = adjacencyDrawOrder[i];

                if (n == 0 || adjacencies[n - 1])
                {
                    if (drawInRows)
                    {
                        drawingService.DrawAnimationFrame(
                            imageKey,
                            colorization,
                            new Rectangle(
                                xOffset,
                                n * cellSize,
                                cellSize,
                                rowHeight),
                            cell.Position.ToVector() * cellSize + new Vector2(0, -3 * yOffset),
                            1,
                            false,
                            drawLayer - 0.1f + 0.01f * i,
                            true);

                        drawingService.DrawAnimationFrame(
                            imageKey,
                            colorization,
                            new Rectangle(
                                xOffset,
                                n * cellSize + rowHeight,
                                cellSize,
                                rowHeight),
                            cell.Position.ToVector() * cellSize + new Vector2(0, -1 * yOffset),
                            1,
                            false,
                            drawLayer + 0.0f + 0.01f * i,
                            true);

                        drawingService.DrawAnimationFrame(
                            imageKey,
                            colorization,
                            new Rectangle(
                                xOffset,
                                n * cellSize + 2 * rowHeight,
                                cellSize,
                                rowHeight),
                            cell.Position.ToVector() * cellSize + new Vector2(0, yOffset),
                            1,
                            false,
                            drawLayer + 0.1f + 0.01f * i,
                            true);

                        drawingService.DrawAnimationFrame(
                            imageKey,
                            colorization,
                            new Rectangle(
                                xOffset,
                                n * cellSize + 3 * rowHeight,
                                cellSize,
                                rowHeight),
                            cell.Position.ToVector() * cellSize + new Vector2(0, 3 * yOffset),
                            1,
                            false,
                            drawLayer + 0.2f + 0.01f * i,
                            true);
                    }
                    else
                    {
                        drawingService.DrawAnimationFrame(
                            imageKey,
                            colorization,
                            new Rectangle(
                                xOffset,
                                n * cellSize,
                                cellSize,
                                cellSize),
                            cell.Position.ToVector() * cellSize,
                            1,
                            false,
                            drawLayer + 0.01f * i,
                            true);
                    }
                }
            }
        }

        public void DrawCellAsSimple(
            IDrawingService drawingService,
            string imageKey,
            float drawLayer,
            CellPosition position,
            BronzeColor colorization,
            int cellSize,
            bool drawInRows,
            int frame)
        {
            var rowHeight = cellSize / 4;
            var yOffset = cellSize / 8;

            var xOffset = cellSize * frame;

            if (drawInRows)
            {
                drawingService.DrawAnimationFrame(
                    imageKey,
                    colorization,
                    new Rectangle(
                        xOffset,
                        0,
                        cellSize,
                        rowHeight),
                    position.ToVector() * cellSize + new Vector2(0, -3 * yOffset),
                    1,
                    false,
                    drawLayer - 0.1f,
                    true);

                drawingService.DrawAnimationFrame(
                    imageKey,
                    colorization,
                    new Rectangle(
                        xOffset,
                        rowHeight,
                        cellSize,
                        rowHeight),
                    position.ToVector() * cellSize + new Vector2(0, -1 * yOffset),
                    1,
                    false,
                    drawLayer + 0.0f,
                    true);

                drawingService.DrawAnimationFrame(
                    imageKey,
                    colorization,
                    new Rectangle(
                        xOffset,
                        2 * rowHeight,
                        cellSize,
                        rowHeight),
                    position.ToVector() * cellSize + new Vector2(0, yOffset),
                    1,
                    false,
                    drawLayer + 0.1f,
                    true);

                drawingService.DrawAnimationFrame(
                    imageKey,
                    colorization,
                    new Rectangle(
                        xOffset,
                        3 * rowHeight,
                        cellSize,
                        rowHeight),
                    position.ToVector() * cellSize + new Vector2(0, 3 * yOffset),
                    1,
                    false,
                    drawLayer + 0.2f,
                    true);
            }
            else
            {
                drawingService.DrawAnimationFrame(
                    imageKey,
                    colorization,
                    new Rectangle(
                        xOffset,
                        0,
                        cellSize,
                        cellSize),
                    position.ToVector() * cellSize,
                    1,
                    false,
                    drawLayer,
                    true);
            }
        }
        #endregion

        private void DrawWorldActor(IDrawingService drawingService, IWorldActor worldActor)
        {
            worldActor.Draw(drawingService, _playerData.ViewMode, _playerData.SelectedArmy == worldActor);

            // Don't add sound effects if the game is paused
            if (!_userSettings.SimulationPaused)
            {
                foreach (var effect in worldActor.GetSoundEffects())
                {
                    _audioService.AddAmbience(effect.SoundKey);
                }
            }
        }

        private void DrawSettlementAsDetailed(
            IDrawingService drawingService,
            IEnumerable<IMapInteractionComponent> mapInteractionComponents)
        {
            var stopwatch = Stopwatch.StartNew();
            var margin = 3;

            var viewport = Rectangle.AroundCenter(
               new Vector2(
                   _playerData.ViewCenter.X / Constants.TILE_SIZE_PIXELS,
                   _playerData.ViewCenter.Y / Constants.TILE_SIZE_PIXELS),
               (int)(drawingService.DisplayWidth / Constants.TILE_SIZE_PIXELS) + margin,
               (int)(drawingService.DisplayHeight / Constants.TILE_SIZE_PIXELS) + margin);

            var generatedDistricts = _playerData.ActiveSettlement.Districts
                .Where(d => d.IsGenerated)
                .ToArray();

            var tilesToDraw = generatedDistricts
                .SelectMany(d => d.Tiles.ToEnumerable<Tile>().Where(t => viewport.Contains(t.Position.ToVector())))
                .ToArray();

            _performanceTracker.LogRenderTime("get tiles", stopwatch.ElapsedTicks);
            stopwatch.Restart();

            var structuresToDraw = generatedDistricts
                .SelectMany(d => d.Structures)
                //.Where(s=>)  TODO viewport culling of structures
                .ToArray();

            foreach (var component in mapInteractionComponents)
            {
                component.DrawTiles(drawingService, tilesToDraw);
            }
            _performanceTracker.LogRenderTime("interaction components", stopwatch.ElapsedTicks);
            stopwatch.Restart();

            foreach (var tile in tilesToDraw)
            {
                var layers = tile.Terrain.DisplayLayers
                        .Concat(tile.Decoration?.DisplayLayers ?? Enumerable.Empty<ITileDisplay>());

                // Don't add sound effects if the game is paused
                if (!_userSettings.SimulationPaused)
                {
                    foreach (var layer in layers)
                    {
                        foreach (var effect in layer.SoundEffects.Where(e => e.Mode == SoundEffectMode.Ambient))
                        {
                            _audioService.AddAmbience(effect.SoundKey);
                        }
                    }
                }

                DrawTile(drawingService, tile, layers.OfType<TileAnimationLayer>());
                DrawSprite(drawingService, tile, layers.OfType<SpriteAnimation>());
            }

            _performanceTracker.LogRenderTime("draw tiles", stopwatch.ElapsedTicks);
            stopwatch.Restart();

            foreach (var structure in structuresToDraw)
            {
                DrawStructure(drawingService, structure.UlTile, structure.AnimationLayers, structure.TilesWide, structure.TilesHigh);

                // Don't add sound effects if the game is paused
                if (!_userSettings.SimulationPaused)
                {
                    foreach (var layer in structure.AnimationLayers)
                    {
                        foreach (var effect in layer.SoundEffects.Where(e => e.Mode == SoundEffectMode.Ambient))
                        {
                            _audioService.AddAmbience(effect.SoundKey);
                        }
                    }
                }
            }
            _performanceTracker.LogRenderTime("draw structures", stopwatch.ElapsedTicks);
            stopwatch.Restart();
        }
        
        public void DrawTile(
            IDrawingService drawingService, 
            Tile tile, 
            IEnumerable<TileAnimationLayer> layers)
        {
            if(tile == null)
            {
                return;
            }

            try
            {
                foreach (var animationLayer in layers)
                {
                    var adjacencies = new bool[8];
                    for (var i = 0; i < adjacencies.Length; i++)
                    {
                        var neighbor = tile.GetNeighbor((Facing)i);

                        if (neighbor != null)
                        {
                            adjacencies[i] = neighbor.Terrain.AdjacencyGroups.Contains(animationLayer.Adjacency)
                                || (neighbor.Structure != null && neighbor.Structure.AdjacencyGroups.Contains(animationLayer.Adjacency));
                        }
                        else
                        {
                            adjacencies[i] = true;
                        }
                    }

                    animationLayer.DrawTile(
                        drawingService,
                        _playerData?.ActiveSettlement?.Owner?.PrimaryColor ?? BronzeColor.None,
                        tile,
                        adjacencies);
                }
            }
            catch (Exception ex)
            {
                _gameInterface.OnException(ex);
            }
        }

        public void DrawSprite(
            IDrawingService drawingService,
            Tile tile,
            IEnumerable<SpriteAnimation> layers)
        {
            if (tile == null)
            {
                return;
            }

            try
            {
                foreach (var layer in layers)
                {
                    var frame = drawingService.GetFrameForAnimation(layer.FrameCount, layer.LoopTime);

                    drawingService.DrawAnimationFrame(
                        layer.ImageKey,
                        _playerData?.ActiveSettlement?.Owner?.PrimaryColor ?? BronzeColor.None,
                        new Rectangle(layer.Width * frame, 0, layer.Width, layer.Height),
                        tile.Position.ToVector() * Constants.TILE_SIZE_PIXELS,
                        1,
                        false,
                        layer.DrawLayer,
                        true);
                }
            }
            catch (Exception ex)
            {
                _gameInterface.OnException(ex);
            }
        }

        public void DrawStructure(
            IDrawingService drawingService, 
            Tile ulTile, 
            IEnumerable<ITileDisplay> animationLayers, 
            int tilesWide, 
            int tilesHigh)
        {
            try
            {
                var tileFootprint = new List<Tile>();
                for (var i = 0; i < tilesWide; i++)
                {
                    for (var j = 0; j < tilesHigh; j++)
                    {
                        var tile = ulTile.District.Settlement.GetTile(ulTile.Position.Relative(i, j));
                        if (tile != null)
                        {
                            tileFootprint.Add(tile);
                        }
                    }
                }

                foreach(var layer in animationLayers.OfType<SpriteAnimation>())
                {
                    var center = (ulTile.Position.ToVector() + new Vector2(tilesWide / 2f, tilesHigh / 2f)) * Constants.TILE_SIZE_PIXELS - new Vector2(16, 16);

                    var frame = drawingService.GetFrameForAnimation(layer.FrameCount, layer.LoopTime);

                    drawingService.DrawAnimationFrame(
                        layer.ImageKey,
                        _playerData?.ActiveSettlement?.Owner?.PrimaryColor ?? BronzeColor.None,
                        new Rectangle(frame * layer.Width, 0, layer.Width, layer.Height),
                        center,
                        1,
                        false,
                        layer.DrawLayer,
                        true);
                }

                foreach(var tile in tileFootprint)
                {
                    DrawTile(drawingService, tile, animationLayers.OfType<TileAnimationLayer>());
                }
            }
            catch (Exception ex)
            {
                _gameInterface.OnException(ex);
            }
        }

        public Vector2 TransformFromScreen(Vector2 screenPos)
        {
            var viewScale = Util.ViewModeToScale(_playerData.ViewMode);
            
            return (screenPos
                    - _screenCenter
                    + _playerData.ViewCenter * viewScale)
                / viewScale;
        }

        public Vector2 TransformToScreen(Vector2 worldPos)
        {
            var viewScale = Util.ViewModeToScale(_playerData.ViewMode);

            return worldPos * viewScale
                + _screenCenter
                - _playerData.ViewCenter * viewScale;
        }
        
        public void DrawSelectionBox(IDrawingService drawingService, IStructure structure)
        {
            var selectedLayer = new TileAnimationLayer
            {
                ImageKey = UiImages.TileSelected,
                DrawLayer = 10
            };

            foreach(var tile in structure.Footprint)
            {
                var adjacencies = new bool[8];
                for (var i = 0; i < adjacencies.Length; i++)
                {
                    var neighbor = tile.GetNeighbor((Facing)i);

                    if (neighbor != null)
                    {
                        adjacencies[i] = neighbor.Structure == structure;
                    }
                    else
                    {
                        adjacencies[i] = true;
                    }
                }
                
                selectedLayer.DrawTile(drawingService, BronzeColor.None, tile, adjacencies);
            }
        }

        public void DrawRedX(
            IDrawingService drawingService, 
            CellPosition position, 
            ViewMode viewMode)
        {
            DrawCellIcon(
                drawingService,
                position,
                viewMode,
                "ui_cell_invalid",
                "ui_cell_invalid_summary");
        }

        public void DrawPathdot(
            IDrawingService drawingService, 
            CellPosition position, 
            ViewMode viewMode)
        {
            DrawCellIcon(
                drawingService,
                position,
                viewMode,
                "ui_cell_pathdot_detailed",
                "ui_cell_pathdot_summary");
        }

        public void DrawSelectionBox(
            IDrawingService drawingService, 
            CellPosition position, 
            ViewMode viewMode)
        {
            DrawCellIcon(
                drawingService,
                position,
                viewMode,
                "ui_cell_detailed_selectbox",
                "ui_cell_summary_selectbox");
        }

        public void DrawCellIcon(
            IDrawingService drawingService,
            CellPosition position,
            ViewMode viewMode,
            string detailedImage,
            string summaryImage)
        {
            if(viewMode == ViewMode.Detailed && !string.IsNullOrEmpty(detailedImage))
            {
                var size = drawingService.GetImageSize(detailedImage);
                
                drawingService.DrawAnimationFrame(
                    detailedImage,
                    BronzeColor.None,
                    new Rectangle(
                        0,
                        0,
                        (int)size.X,
                        (int)size.Y),
                    position.ToVector() * Constants.CELL_SIZE_PIXELS,
                    1,
                    false,
                    10,
                    true);
            }
            else if (viewMode == ViewMode.Summary && !string.IsNullOrEmpty(summaryImage))
            {
                DrawCellAsSimple(
                    drawingService,
                    summaryImage,
                    10,
                    position,
                    BronzeColor.None,
                    Constants.CELL_SIZE_SUMMARY_PIXELS,
                    false,
                    0);
            }
        }
    }
}
