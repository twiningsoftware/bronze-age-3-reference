﻿using System.Collections.Generic;
using Bronze.Common.UI.Modals;
using Bronze.Contracts.Help;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.Services
{
    public class HelpManager : IHelpManager
    {
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IPlayerDataTracker _playerData;
        private readonly IDialogManager _dialogManager;
        private Dictionary<string, HelpPage> _pages;

        public HelpPage StartingPage { get; private set; }

        public HelpManager(
            IGamedataTracker gamedataTracker,
            IPlayerDataTracker playerData,
            IDialogManager dialogManager)
        {
            _gamedataTracker = gamedataTracker;
            _playerData = playerData;
            _dialogManager = dialogManager;
            _pages = new Dictionary<string, HelpPage>();
        }

        public void AddPage(HelpPage page)
        {
            if (StartingPage == null || page.IsStartingPage)
            {
                StartingPage = page;
            }

            if (_pages.ContainsKey(page.Id))
            {
                _pages[page.Id] = page;
            }
            else
            {
                _pages.Add(page.Id, page);
            }
        }

        public void Clear()
        {
            _pages.Clear();
            StartingPage = null;
        }

        public HelpPage GetPage(string pageId)
        {
            //if (pageId == HelpConstants.PAGE_BUILTIN_PLAYER_RACE
            //    && _playerData.PlayerTribe != null
            //    && _pages.ContainsKey(_playerData.PlayerTribe?.Race?.Id))
            //{
            //    return _pages[_playerData.PlayerTribe?.Race?.Id];
            //}

            //if (pageId != null && _pages.ContainsKey(pageId))
            //{
            //    return _pages[pageId];
            //}

            return null;
        }

        public void CreateAutoPages()
        {
            //    foreach (var race in _gamedataTracker.Races)
            //    {
            //        AddPage(BuildPageFor(race));
            //    }

            //    AddPage(BuildItemPage());

            //    AddPage(BuildInfrastructurePage());

            //    AddPage(BuildBonusPage());

            //    AddPage(BuildStrucutresPage());

            //    AddPage(BuildUnitsPage());
        }

        public void OpenHelpDialog()
        {
            _dialogManager.PushModal<HelpModal, HelpModalContext>(new HelpModalContext());
        }

        //private HelpPage BuildPageFor(Race race)
        //{
        //    var overviewTopic = new HelpTopic
        //    {
        //        Id = race.Id,
        //        ImageKey = race.IdlePopKey,
        //        Title = race.Name,
        //        Body = new[]
        //        {
        //            new TextHelpBody(race.Description)
        //        }
        //    };

        //    var structureCategories = race.AvailableStructures
        //        .Where(s => !string.IsNullOrWhiteSpace(s.Category))
        //        .GroupBy(s => s.Category)
        //        .ToArray();

        //    var uncategorizedStructures = race.AvailableStructures
        //        .Where(s => string.IsNullOrWhiteSpace(s.Category))
        //        .ToArray();

        //    var structureTopics = BuildRaceStructureTopic(uncategorizedStructures)
        //        .Yield()
        //        .Concat(
        //            structureCategories
        //                .Select(s => BuildRaceStructureTopic(s.Key, s)))
        //        .Where(t => t.PageLinks.Any())
        //        .ToArray();

        //    var unitTopic = new HelpTopic
        //    {
        //        Id = "units",
        //        Title = race.Name + " Units",
        //        PageLinks = race.AvailableUnits
        //            .Select(u => new HelpLink(HelpConstants.PAGE_BUILTIN_UNITS, u.Id))
        //            .ToArray()
        //    };

        //    return new HelpPage
        //    {
        //        Id = race.Id,
        //        Name = race.Name,
        //        Topics = overviewTopic.Yield()
        //            .Concat(structureTopics)
        //            .Concat(unitTopic.Yield())
        //            .ToArray()
        //    };
        //}

        //private HelpTopic BuildRaceStructureTopic(IEnumerable<StructureData> structures)
        //{
        //    return new HelpTopic
        //    {
        //        Id = "uncategorized_structures",
        //        Title = "Uncategorized Structures",
        //        PageLinks = structures
        //            .Select(s => new HelpLink(HelpConstants.PAGE_BUILTIN_STRUCTURES, s.Id))
        //            .ToArray()
        //    };
        //}

        //private HelpTopic BuildRaceStructureTopic(
        //    string category,
        //    IEnumerable<StructureData> structures)
        //{
        //    return new HelpTopic
        //    {
        //        Id = category,
        //        Title = category + " Structures",
        //        PageLinks = structures
        //            .Select(s => new HelpLink(HelpConstants.PAGE_BUILTIN_STRUCTURES, s.Id))
        //            .ToArray()
        //    };
        //}

        //private HelpPage BuildItemPage()
        //{
        //    return new HelpPage
        //    {
        //        Id = HelpConstants.PAGE_BUILTIN_ITEMS,
        //        Name = "Items",
        //        Topics = _gamedataTracker.Items
        //            .Select(i => new HelpTopic
        //            {
        //                Id = i.Name,
        //                Title = i.Name,
        //                ImageKey = i.IconKey,
        //                Body = new[]
        //                {
        //                    new TextHelpBody(i.Description)
        //                }
        //            })
        //            .ToArray()
        //    };
        //}

        //private HelpPage BuildInfrastructurePage()
        //{
        //    return new HelpPage
        //    {
        //        Id = HelpConstants.PAGE_BUILTIN_INFRASTRUCTURE,
        //        Name = "Infrastructure",
        //        Topics = _gamedataTracker.Infrastructure
        //            .Select(i => new HelpTopic
        //            {
        //                Id = i.Name,
        //                Title = i.Name,
        //                ImageKey = i.IconKey,
        //                Body = new[]
        //                {
        //                    new TextHelpBody(i.Description)
        //                }
        //            })
        //            .ToArray()
        //    };
        //}

        //private HelpPage BuildBonusPage()
        //{
        //    return new HelpPage
        //    {
        //        Id = HelpConstants.PAGE_BUILTIN_BONUSES,
        //        Name = "Bonuses",
        //        Topics = _gamedataTracker.BonusTypes
        //            .Select(bt => BuildTopicFor(bt))
        //            .ToArray()
        //    };
        //}

        //private HelpTopic BuildTopicFor(BonusType bt)
        //{
        //    var body = new List<AbstractHelpBody>();

        //    body.Add(new TextHelpBody("Produced By"));
        //    body.Add(new TableHelpBody(4, _gamedataTracker.StructureData
        //        .Where(sd => sd.OperationBonuses.Any(ob => ob.Type == bt))
        //        .Select(sd => new TextHelpBody(sd.TypeName))
        //        .ToArray()));

        //    body.Add(new TextHelpBody("Consumed By"));
        //    body.Add(new TableHelpBody(2, _gamedataTracker.StructureData
        //        .Where(sd => sd.ConstructionInputs.Any(ir => ir.Bonuses.Any(b => b == bt))
        //            || (sd.Transformation?.Inputs?.Any(ir => ir.Bonuses.Any(b => b == bt)) ?? false)
        //            || (sd.Transformation?.Outputs?.Any(ir => ir.Bonuses.Any(b => b == bt)) ?? false)
        //            || sd.InfrastructureSupply.Any(id=>id.Bonuses.Any(b=> b== bt))
        //            || sd.InfrastructureCost.Any(id => id.Bonuses.Any(b => b == bt)))
        //        .Select(sd => new TextHelpBody(sd.TypeName))
        //        .ToArray()));

        //    return new HelpTopic
        //    {
        //        Id = bt.Name,
        //        Title = bt.Name,
        //        ImageKey = bt.IconKey,
        //        Body = body.ToArray(),
        //        PageLinks = new HelpLink[0]
        //    };
        //}

        //private HelpPage BuildStrucutresPage()
        //{
        //    return new HelpPage
        //    {
        //        Id = HelpConstants.PAGE_BUILTIN_STRUCTURES,
        //        Name = "Structures",
        //        Topics = _gamedataTracker.StructureData
        //            .Select(sd => BuildTopicFor(sd))
        //            .ToArray()
        //    };
        //}

        //private HelpTopic BuildTopicFor(StructureData structure)
        //{
        //    var body = new List<AbstractHelpBody>();
        //    body.Add(new TextHelpBody(structure.Description));
        //    var statBlock = new List<AbstractHelpBody>();
        //    statBlock.Add(new IconTextHelpBody(KnownImages.IconHealth, structure.Health.ToString(), "Health"));
        //    statBlock.Add(new IconTextHelpBody(KnownImages.IconDefense, structure.Defense.ToString(), "Defense"));
        //    if (structure.HasRangedAttack)
        //    {
        //        statBlock.Add(new IconTextHelpBody(KnownImages.IconStrength, structure.RangedAttackStrength.ToString(), "Strength"));
        //        statBlock.Add(new IconTextHelpBody(KnownImages.IconAttackMelee, structure.RangedAttack.ToString(), "Ranged Attack"));
        //    }
        //    body.Add(new TableHelpBody(4, statBlock));

        //    var pageLinks = new List<HelpLink>();

        //    if (structure.UnitSupport?.UnitData != null)
        //    {
        //        pageLinks.Add(new HelpLink(
        //            HelpConstants.PAGE_BUILTIN_UNITS,
        //            structure.UnitSupport?.UnitData?.Id));

        //        body.Add(new TextHelpBody("Supports: " + structure.UnitSupport?.UnitData?.TypeName));
        //    }

        //    foreach(var upgrade in structure.UpgradesTo)
        //    {
        //        pageLinks.Add(new HelpLink(
        //            HelpConstants.PAGE_BUILTIN_STRUCTURES,
        //            upgrade.Id));
        //    }

        //    pageLinks.Add(new HelpLink(HelpConstants.PAGE_BUILTIN_ITEMS));
        //    pageLinks.Add(new HelpLink(HelpConstants.PAGE_BUILTIN_INFRASTRUCTURE));

        //    body.Add(new TextHelpBody("CONSTRUCTION"));
        //    var constructionBlock = new List<AbstractHelpBody>();
        //    constructionBlock.Add(new IconTextHelpBody(KnownImages.IconTime, Util.FriendlyTimeDisplay(structure.ConstructionTimeMonths), "Construction Time"));
        //    constructionBlock.Add(new IconTextHelpBody("ui_pop_human_working", "1", "Workers"));
        //    constructionBlock.AddRange(structure.ConstructionInputs
        //        .Select(i => new IconTextHelpBody(i.Item.IconKey, "- " + i.PerMonth, i.Item.Name, BronzeColor.Red)));
        //    body.Add(new TableHelpBody(4, constructionBlock));

        //    body.Add(new TextHelpBody("OPERATION"));
        //    var operationBlock = new List<AbstractHelpBody>();
        //    if (structure.WorkerNeed > 0)
        //    {
        //        operationBlock.Add(new IconTextHelpBody("ui_pop_human_working", structure.WorkerNeed.ToString(), "Workers"));
        //    }
        //    if (structure.Transformation != null)
        //    {
        //        operationBlock.AddRange(structure.Transformation.Inputs
        //            .Select(i => new IconTextHelpBody(i.Item.IconKey, "- " + i.PerMonth, i.Item.Name, BronzeColor.Red)));
        //    }
        //    operationBlock.AddRange(structure.OperationInputs
        //        .Select(i => new IconTextHelpBody(i.Item.IconKey, "- " + i.PerMonth, i.Item.Name, BronzeColor.Red)));
        //    operationBlock.AddRange(structure.InfrastructureCost
        //        .Select(ic => new IconTextHelpBody(ic.Infrastructure.IconKey, "- " + ic.Amount, ic.Infrastructure.Name, BronzeColor.Red)));
        //    if (structure.Transformation != null)
        //    {
        //        operationBlock.AddRange(structure.Transformation.Outputs
        //            .Select(i => new IconTextHelpBody(i.Item.IconKey, "+ " + i.PerMonth, i.Item.Name, BronzeColor.Green)));
        //    }
        //    operationBlock.AddRange(structure.InfrastructureSupply
        //        .Select(ic => new IconTextHelpBody(ic.Infrastructure.IconKey, "+ " + ic.Amount, ic.Infrastructure.Name, BronzeColor.Green)));
        //    operationBlock.AddRange(structure.OperationBonuses
        //        .Select(b=> new IconTextHelpBody(
        //            b.Type.IconKey,
        //            (b.Amount * 100).ToString() + "%",
        //            b.Type.Name,
        //            b.Amount > 0 ? BronzeColor.Green : BronzeColor.Red)));

        //    foreach (var bonus in structure.OperationBonuses.Select(b => b.Type))
        //    {
        //        pageLinks.Add(new HelpLink(HelpConstants.PAGE_BUILTIN_BONUSES, bonus.Name));
        //    }

        //    body.Add(new TableHelpBody(4, operationBlock));


        //    var allBonuses = structure.ConstructionInputs
        //        .Concat(structure.OperationInputs)
        //        .Concat(structure.Transformation?.Inputs ?? Enumerable.Empty<ItemRate>())
        //        .Concat(structure.Transformation?.Outputs ?? Enumerable.Empty<ItemRate>())
        //        .SelectMany(ir => ir.Bonuses)
        //        .Concat(structure.InfrastructureCost
        //            .Concat(structure.InfrastructureSupply)
        //            .SelectMany(ic => ic.Bonuses))
        //        .Distinct()
        //        .ToArray();

        //    if(allBonuses.Any())
        //    {
        //        body.Add(new TextHelpBody("Affected By"));
        //        body.Add(new TableHelpBody(4,
        //            allBonuses
        //            .Select(b => new IconTextHelpBody(b.IconKey, " ", tooltip: b.Name))
        //            .ToArray()));

        //        foreach(var bonus in allBonuses)
        //        {
        //            pageLinks.Add(new HelpLink(HelpConstants.PAGE_BUILTIN_BONUSES, bonus.Name));
        //        }
        //    }

        //    return new HelpTopic
        //    {
        //        Id = structure.Id,
        //        Title = structure.TypeName,
        //        Body = body.ToArray(),
        //        PageLinks = pageLinks.ToArray()
        //    };
        //}

        //private HelpPage BuildUnitsPage()
        //{
        //    return new HelpPage
        //    {
        //        Id = HelpConstants.PAGE_BUILTIN_UNITS,
        //        Name = "Units",
        //        Topics = _gamedataTracker.UnitData
        //            .Select(ud => BuildTopicFor(ud))
        //            .ToArray()
        //    };
        //}

        //private HelpTopic BuildTopicFor(UnitData unit)
        //{
        //    var pageLinks = new List<HelpLink>();

        //    var supportingStructures = _gamedataTracker.StructureData
        //        .Where(sd => sd.UnitSupport?.UnitData == unit)
        //        .ToArray();

        //    foreach (var supportingStructure in supportingStructures)
        //    {
        //        pageLinks.Add(new HelpLink(HelpConstants.PAGE_BUILTIN_STRUCTURES, supportingStructure.Id));
        //    }

        //    var body = new List<AbstractHelpBody>();
        //    body.Add(new TextHelpBody(unit.Description));

        //    var statBlock = new List<AbstractHelpBody>();
        //    statBlock.Add(new IconTextHelpBody(KnownImages.IconStrength, unit.Strength.ToString(), "Strength"));
        //    statBlock.Add(new IconTextHelpBody(KnownImages.IconDefense, unit.Defense.ToString(), "Defense"));
        //    statBlock.Add(new IconTextHelpBody(KnownImages.IconVision, unit.Vision.ToString(), "Vision Range"));
        //    if (unit.HasMeleeAttack)
        //    {
        //        statBlock.Add(new IconTextHelpBody(KnownImages.IconAttackMelee, unit.MeleeAttack.ToString(), "Melee Attack"));
        //    }
        //    if (unit.HasRangedAttack)
        //    {
        //        statBlock.Add(new IconTextHelpBody(KnownImages.IconAttackRanged, unit.RangedAttack.ToString(), "Ranged Attack"));
        //        statBlock.Add(new IconTextHelpBody(KnownImages.IconRangedAttackRange, unit.RangedAttackRange.ToString(), "Ranged Attack Range"));
        //    }
        //    if (unit.CanTransport)
        //    {
        //        statBlock.Add(new IconTextHelpBody(KnownImages.IconTransportCapacity, unit.TransportCapacity.ToString(), "Transport Capacity"));
        //    }
        //    if (unit.InventoryCapacity > 0)
        //    {
        //        statBlock.Add(new IconTextHelpBody(KnownImages.IconCanTrade, unit.InventoryCapacity.ToString(), "Cargo Capacity"));
        //    }
        //    body.Add(new TableHelpBody(4, statBlock));

        //    if (unit.CanCapture)
        //    {
        //        body.Add(new TextHelpBody("Can capture settlements."));
        //    }
        //    if (unit.CanSettle)
        //    {
        //        body.Add(new TextHelpBody("Can form or join settlements."));
        //    }
        //    if (unit.CanTrade)
        //    {
        //        body.Add(new TextHelpBody("Can trade."));
        //    }
        //    if (unit.CanTransport)
        //    {
        //        body.Add(new TextHelpBody("Can transport units."));
        //    }
        //    if (unit.CanBeTransported)
        //    {
        //        body.Add(new TextHelpBody("Can be transported."));
        //    }

        //    return new HelpTopic
        //    {
        //        Id = unit.Id,
        //        Title = unit.TypeName,
        //        ImageKey = unit.IconKey,
        //        Body = body.ToArray(),
        //        PageLinks = pageLinks.ToArray()
        //    };
        //}
    }
}
