﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Generation;
using Bronze.Contracts.InjectableServices;
using System.Collections.Generic;

namespace Bronze.Common.Services
{
    public class GamedataTracker : IGamedataTracker
    {
        public List<Biome> Biomes { get; }
        public List<RiverType> RiverTypes { get; }
        public List<MidpathType> MidpathTypes { get; }
        public List<Terrain> Terrain { get; }
        public List<WorldType> WorldTypes { get; }
        public List<Race> Races { get; }
        public List<ICellFeature> CellFeatures { get; }
        public List<Trait> Traits { get; }
        public List<ICellDevelopmentType> CellDevelopments { get; }
        public List<Decoration> Decorations { get; }
        public List<IStructureType> StructureTypes { get; }
        public List<Item> Items { get; }
        public List<BonusType> BonusTypes { get; }
        public List<ServiceType> ServiceTypes { get; }
        public List<Recipie> Recipies { get; }
        public List<MovementType> MovementTypes { get; }
        public List<IUnitType> UnitTypes { get; }
        public List<UnitCategory> UnitCategories { get; }
        public List<PeoplePart> PeopleParts { get; }
        public List<NotablePersonSkill> Skills { get; }
        public List<ITransportType> TransportTypes { get; }
        public List<RecruitmentSlotCategory> RecruitmentSlotCategories { get; }
        public List<ITribeControllerType> ControllerTypes { get; }
        public List<HaulerInfo> HaulerInfos { get; }
        public List<Cult> Cults { get; }

        public GamedataTracker()
        {
            Biomes = new List<Biome>();
            Terrain = new List<Terrain>();
            RiverTypes = new List<RiverType>();
            MidpathTypes = new List<MidpathType>();
            WorldTypes = new List<WorldType>();
            Races = new List<Race>();
            CellFeatures = new List<ICellFeature>();
            Traits = new List<Trait>();
            CellDevelopments = new List<ICellDevelopmentType>();
            Decorations = new List<Decoration>();
            StructureTypes = new List<IStructureType>();
            Items = new List<Item>();
            BonusTypes = new List<BonusType>();
            ServiceTypes = new List<ServiceType>();
            Recipies = new List<Recipie>();
            MovementTypes = new List<MovementType>();
            UnitTypes = new List<IUnitType>();
            UnitCategories = new List<UnitCategory>();
            PeopleParts = new List<PeoplePart>();
            Skills = new List<NotablePersonSkill>();
            TransportTypes = new List<ITransportType>();
            RecruitmentSlotCategories = new List<RecruitmentSlotCategory>();
            ControllerTypes = new List<ITribeControllerType>();
            HaulerInfos = new List<HaulerInfo>();
            Cults = new List<Cult>();
        }

        public void ClearGameData()
        {
            Biomes.Clear();
            RiverTypes.Clear();
            MidpathTypes.Clear();
            Terrain.Clear();
            WorldTypes.Clear();
            Races.Clear();
            CellFeatures.Clear();
            Traits.Clear();
            CellDevelopments.Clear();
            Decorations.Clear();
            StructureTypes.Clear();
            Items.Clear();
            BonusTypes.Clear();
            ServiceTypes.Clear();
            Recipies.Clear();
            MovementTypes.Clear();
            UnitTypes.Clear();
            UnitCategories.Clear();
            PeopleParts.Clear();
            Skills.Clear();
            TransportTypes.Clear();
            RecruitmentSlotCategories.Clear();
            ControllerTypes.Clear();
            HaulerInfos.Clear();
            Cults.Clear();
        }
    }
}
