﻿using Bronze.Contracts;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Generation;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;
using UNM.Parser;
using UNM.Parser.Implementation;
using UNM.Parser.Interfaces;

namespace Bronze.Common.Services
{
    public class UnmNameSource : INameSource, IWorldStateService
    {
        private readonly INamelistSource _namelistSource;
        private readonly IWorldManager _worldManager;
        private readonly IGameInterface _gameInterface;
        private INameParser _nameParser;

        private List<string> _tribeNames;
        private List<string> _settlementNames;

        public UnmNameSource(
            INamelistSource namelistSource,
            IWorldManager worldManager,
            IGameInterface gameInterface)
        {
            _namelistSource = namelistSource;
            _worldManager = worldManager;
            _gameInterface = gameInterface;
            InitializeForSeed(new Random().Next());

            _tribeNames = new List<string>();
            _settlementNames = new List<string>();
        }

        public void Clear()
        {
            _tribeNames.Clear();
            _settlementNames.Clear();
        }

        public void InitializeForSeed(int seed)
        {
            _nameParser = new NameParser(_namelistSource, seed);
            _nameParser.Initialize();
        }

        public void Initialize(WorldState worldState)
        {
            _nameParser = new NameParser(_namelistSource, worldState.Seed.GetHashCode());
            _nameParser.Initialize();

            _tribeNames.AddRange(worldState.AllTribes.Select(t => t.Name));
            _settlementNames.AddRange(worldState.AllTribes.SelectMany(t => t.Settlements.Select(s => s.Name)));
        }

        public void SaveCustomData(SerializedObject worldRoot)
        {
        }

        public void LoadCustomData(SerializedObject worldRoot)
        {
        }
        
        public string MakeWorldName(WorldType worldType)
        {
            return _nameParser.Process(new PatternProcessingParameters("<^" + worldType.WorldNamePattern + ">")
            {
                CapitalizationScheme = CapitalizationScheme.BY_WORDS,
            });
        }

        public string MakeTribeName(Race race)
        {
            var name = GenerateWithRaceAndDuplicateFallback(
                "<^" + race.TribeNamePattern + ">",
                new[]
                {
                    race.Name
                },
                new Dictionary<string, string>(),
                CapitalizationScheme.BY_WORDS,
                _tribeNames);

            _tribeNames.Add(name);

            return name;
        }

        public string MakeSettlementName(Race race, Cell cell)
        {
            var context = new[]
                {
                    race.Name
                }
                .Concat(cell.Traits.Select(t => t.Id))
                .Concat(cell.Neighbors.SelectMany(n => n.Traits).Distinct().Select(t => "neighbor_" + t.Id))
                .ToArray();
        

            var name = GenerateWithRaceAndDuplicateFallback(
                "<^" + race.SettlementNamePattern + ">",
                context,
                new Dictionary<string, string>(),
                CapitalizationScheme.BY_WORDS,
                _settlementNames);

            _settlementNames.Add(name);

            return name;
        }

        public string MakeArmyName(Race race)
        {
            try
            {
                return _nameParser.Process(new PatternProcessingParameters(
                    "<^" + race.ArmyNamePattern + ">")
                {
                    CapitalizationScheme = CapitalizationScheme.BY_WORDS,
                    Context = BuildContextForRace(race)
                        .Concat("is_army".Yield())
                        .ToArray()
                });
            }
            catch (PatternParseException ex)
            {
                throw new PatternParseException(ExpandMessage(ex), ex);
            }
        }

        private IEnumerable<string> BuildContextForRace(Race race)
        {
            return race.Traits
                .Select(t => t.Id)
                .Concat(("race_" + race.Name).Yield())
                .ToArray();
        }

        private string GenerateWithRaceAndDuplicateFallback(string pattern,
            IEnumerable<string> context,
            Dictionary<string, string> variables,
            CapitalizationScheme capitalizationScheme,
            IEnumerable<string> duplicates)
        {
            try
            {
                return GenerateWithRaceFallback(
                    pattern,
                    context,
                    variables,
                    capitalizationScheme,
                    duplicates.ToList());
            }
            catch (Exception)
            {
            }

            try
            {
                return GenerateWithRaceFallback(
                    pattern,
                    context,
                    variables,
                    capitalizationScheme,
                    new List<string>());
            }
            catch (PatternParseException ex)
            {
                throw new PatternParseException(ExpandMessage(ex), ex);
            }
        }

        private string GenerateWithRaceFallback(
            string pattern, 
            IEnumerable<string> context, 
            Dictionary<string, string> variables,
            CapitalizationScheme capitalizationScheme,
            List<string> uniqueCheck)
        {
            try
            {
                return _nameParser.Process(new PatternProcessingParameters(pattern)
                {
                    CapitalizationScheme = capitalizationScheme,
                    Context = context,
                    Variables = variables,
                    UniqueCheck = uniqueCheck
                });
            }
            catch (Exception)
            {
            }

            try
            {
                return _nameParser.Process(new PatternProcessingParameters(pattern)
                {
                    CapitalizationScheme = capitalizationScheme,
                    Context = context
                        .Concat("RaceFallback".Yield())
                        .ToArray(),
                    Variables = variables,
                    UniqueCheck = uniqueCheck
                });
            }
            catch (PatternParseException ex)
            {
                throw new PatternParseException(ExpandMessage(ex), ex);
            }
        }

        private string ExpandMessage(Exception ex)
        {
            var msg = "";

            while(ex != null)
            {
                msg += ex.Message + " ";
                ex = ex.InnerException;
            }

            return msg;
        }

        public string MakeRegionName(string regionNamePattern, Region region)
        {
            try
            {
                return _nameParser.Process(new PatternProcessingParameters(
                    "<^" + regionNamePattern + ">")
                {
                    CapitalizationScheme = CapitalizationScheme.BY_WORDS,
                    Context = BuildContextForRegion(region)
                });
            }
            catch (PatternParseException ex)
            {
                throw new PatternParseException(ExpandMessage(ex), ex);
            }
        }

        private IEnumerable<string> BuildContextForRegion(Region region)
        {
            return region.Biome.Name.Yield()
                .Concat(GetCommonTraitsFor(region).Select(t => t.Id))
                .Concat(region.Neighbors.Select(r => "adjacent_" + r.Biome.Name.Yield().Concat(GetCommonTraitsFor(r).Select(t => "adjacent_" + t.Id))))
                .ToArray();
        }

        private IEnumerable<Trait> GetCommonTraitsFor(Region region)
        {
            var cellCount = region.Cells.Count();

            var allCellTraits = region.Cells
                .SelectMany(c => c.Traits)
                .Distinct()
                .ToArray();

            var traitCounts = allCellTraits
                .Select(t => Tuple.Create(t, region.Cells.Where(c => c.Traits.Contains(t)).Count()))
                .ToArray();

            var commonTraits = traitCounts
                .Where(t => t.Item2 > cellCount * 0.3)
                .Select(t => t.Item1)
                .ToArray();

            return commonTraits;
        }

        public string MakePersonName(Race race, bool isMale)
        {
            try
            {
                var context = new List<string>();
                if (isMale)
                {
                    context.Add("ismale");
                }
                else
                {
                    context.Add("isfemale");
                }

                return _nameParser.Process(new PatternProcessingParameters(
                    "<^" + race.PersonNamePattern + ">")
                {
                    CapitalizationScheme = CapitalizationScheme.BY_WORDS,
                    Context = context
                });
            }
            catch (PatternParseException ex)
            {
                throw new PatternParseException(ExpandMessage(ex), ex);
            }
        }

        public string EvaluateDialog(
            string pattern, 
            List<string> context, 
            Dictionary<string, string> variables)
        {
            try
            {
                return GenerateWithRaceFallback(
                    pattern,
                    context,
                    variables,
                    CapitalizationScheme.BY_SENTENCE,
                    new List<string>());
            }
            catch(Exception ex)
            {
                _gameInterface.OnException(ex);
            }
            return pattern;
        }
    }
}
