﻿using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.AI;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.Services
{
    public class CityPrefabPlacer : AbstractBackgroundCalculatorSubSimulator, ICityPrefabPlacer
    {
        private static readonly object LOCK = new object();

        private readonly IGameInterface _gameInterface;
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IWorldManager _worldManager;
        private readonly ITilePather _tilePather;
        private readonly ISettlementLogiCalculator _settlementLogiCalculator;

        private List<PlacementAttempt> _queue;
        private List<PlacementResult> _results;

        public CityPrefabPlacer(
            IGameInterface gameInterface,
            IGamedataTracker gamedataTracker,
            IWorldManager worldManager,
            ITilePather tilePather,
            ISettlementLogiCalculator settlementLogiCalculator)
                :base(gameInterface)
        {
            _gameInterface = gameInterface;
            _gamedataTracker = gamedataTracker;
            _worldManager = worldManager;
            _tilePather = tilePather;
            _settlementLogiCalculator = settlementLogiCalculator;
            _queue = new List<PlacementAttempt>();
            _results = new List<PlacementResult>();
        }

        public void QueueForPlacement(
            Settlement settlement, 
            CityPrefab[] prefabs, 
            bool instantBuild,
            CityPrefab[] storageDistFallback)
        {
            lock (LOCK)
            {
                _queue.Add(new PlacementAttempt
                {
                    Settlement = settlement,
                    Prefabs = prefabs,
                    StorageDistFallback = storageDistFallback,
                    InstantBuild = instantBuild
                });
            }
        }

        protected override void ApplyResults()
        {
            IEnumerable<PlacementResult> results;
            lock (LOCK)
            {
                results = _results.ToArray();
                _results.Clear();
            }

            foreach (var result in results)
            {
                PlacePrefab(result.Settlement, result.Prefab, result.UlTile, result.InstantBuild, result.AdditionalPlacements);
            }
        }

        private void PlacePrefab(Settlement settlement, CityPrefab prefab, TilePosition ulTilePos, bool instantBuild, List<Tuple<TilePosition, IStructureType>> additionalPlacements)
        {
            foreach (var structurePlacement in prefab.StructurePlacements)
            {
                var ulTile = settlement
                    .GetTile(
                        ulTilePos
                            .Relative(structurePlacement.RelativeX, structurePlacement.RelativeY));

                if (structurePlacement.Structure.CanBePlaced(ulTile))
                {
                    var structure = structurePlacement.Structure.Place(ulTile);

                    // For things without inputs, like roads, cheat them to instant build to avoid hammering settlement calculations
                    if (instantBuild || !structurePlacement.Structure.ConstructionCost.Any())
                    {
                        structure.ConstructionProgress = structure.ConstructionMonths;
                        structure.UnderConstruction = false;
                        structure.OnConstructionFinished();
                    }

                    if (structurePlacement.Recipie != null)
                    {
                        structure.ActiveRecipie = structurePlacement.Recipie;

                        if(instantBuild)
                        {
                            foreach(var output in structurePlacement.Recipie.Outputs)
                            {
                                structure.Inventory.Add(output.Item, structure.InventoryCapacity);
                            }
                        }
                    }
                }
            }

            foreach (var tuple in additionalPlacements)
            {
                var structureType = tuple.Item2;

                var ulTile = settlement.GetTile(tuple.Item1);

                if (structureType.CanBePlaced(ulTile))
                {
                    var structure = structureType.Place(ulTile);

                    // For things without inputs, like roads, cheat them to instant build to avoid hammering settlement calculations
                    if (instantBuild || !structureType.ConstructionCost.Any())
                    {
                        structure.ConstructionProgress = structure.ConstructionMonths;
                        structure.UnderConstruction = false;
                        structure.OnConstructionFinished();
                    }
                }
            }
        }

        protected override void StartCalculations()
        {
            Queue<PlacementAttempt> queue;
            lock (LOCK)
            {
                queue = new Queue<PlacementAttempt>(_queue);
                _queue.Clear();
            }

            while (queue.Any())
            {
                var current = queue.Peek();
                var results = new PlacementResult
                {
                    Settlement = current.Settlement,
                    InstantBuild = current.InstantBuild
                };

                foreach(var prefab in current.Prefabs)
                {
                    if (results.Prefab == null)
                    {
                        var position = TryToPlace(current, prefab);

                        if (position != null)
                        {
                            results.Prefab = prefab;
                            results.UlTile = position.Value;

                            if(prefab.IsCityStarter)
                            {
                                TryToPlaceRoads(current.Settlement, results);
                            }
                        }
                    }
                }

                if(results.Prefab == null && current.StorageBlocked)
                {
                    foreach (var prefab in current.StorageDistFallback)
                    {
                        if (results.Prefab == null)
                        {
                            var position = TryToPlace(current, prefab);

                            if (position != null)
                            {
                                results.Prefab = prefab;
                                results.UlTile = position.Value;
                            }
                        }
                    }
                }

                if (results.Prefab != null)
                {
                    lock (LOCK)
                    {
                        _results.Add(results);
                    }
                }

                queue.Dequeue();
            }
        }
        
        private TilePosition? TryToPlace(PlacementAttempt placementAttempt, CityPrefab prefab)
        {
            if (!placementAttempt.Settlement.Districts.Any())
            {
                return null;
            }

            var centerTile = new TilePosition(
                placementAttempt.Settlement.Districts.First().Cell,
                TilePosition.TILES_PER_CELL / 2,
                TilePosition.TILES_PER_CELL / 2);

            var orderedTiles = placementAttempt.Settlement.Districts.First().AllTiles
                .OrderBy(t => t.Position.DistanceSq(centerTile))
                .ToArray();
            
            foreach (var tile in orderedTiles)
            {
                var canPlace = true;
                var overlap = false;

                foreach (var structurePlacement in prefab.StructurePlacements)
                {
                    if (canPlace)
                    {
                        var ulTile = placementAttempt.Settlement
                            .GetTile(tile.Position
                                .Relative(structurePlacement.RelativeX, structurePlacement.RelativeY));

                        if(ulTile != null)
                        {
                            if (ulTile.Structure == null
                                || !ulTile.Structure.Traits.ContainsAny(prefab.OverlapAllowances)
                                || !structurePlacement.Structure.Traits.ContainsAny(prefab.OverlapAllowances))
                            {
                                canPlace = structurePlacement.Structure.CanBePlaced(ulTile);
                            }
                            else
                            {
                                overlap = true;
                            }
                        }
                    }
                }

                if (canPlace)
                {
                    var prefabFootprint = prefab.StructurePlacements
                        .SelectMany(sp => Util.CalculateFootprint(placementAttempt.Settlement.GetTile(tile.Position.Relative(sp.RelativeX, sp.RelativeY)), sp.Structure))
                        .ToArray();

                    if (!overlap && canPlace && !prefab.IsCityStarter && prefab.ConnectivityTraits.Any())
                    {

                        var connectivity = prefabFootprint
                            .SelectMany(t => t.OrthoNeighbors)
                            .Where(t => t.Traits.Intersect(prefab.ConnectivityTraits).Any())
                            .Any();

                        canPlace = canPlace && connectivity;
                    }

                    if (canPlace && !prefab.IsCityStarter && prefab.DistanceToStorage != null)
                    {
                        var actors = _settlementLogiCalculator
                            .FloodFillToActors(
                                prefabFootprint
                                    .SelectMany(t => t.OrthoNeighbors)
                                    .Distinct()
                                    .ToArray(),
                                prefab.ConnectivityTraits,
                                prefab.DistanceToStorage.Value);

                        var storageNear = actors.OfType<IItemStorageProvider>().Any();

                        placementAttempt.StorageBlocked = placementAttempt.StorageBlocked || !storageNear;

                        canPlace = canPlace && storageNear;
                    }

                    if (canPlace)
                    {
                        return tile.Position;
                    }
                }
            }
            
            return null;
        }

        private void TryToPlaceRoads(Settlement settlement, PlacementResult results)
        {
            if(results.Prefab.ToBorderRoads == null)
            {
                return;
            }
            
            var totalWidth = results.Prefab.StructurePlacements
                .Select(sp => sp.RelativeX + sp.Structure.TilesWide)
                .Max();
            var totalHeight = results.Prefab.StructurePlacements
                .Select(sp => sp.RelativeY + sp.Structure.TilesHigh)
                .Max();

            var center = results.UlTile.Relative(totalWidth / 2, totalHeight / 2);

            var totalFootprints = results.Prefab.StructurePlacements
                .SelectMany(sp => Util.CalculateFootprint(results.Settlement.GetTile(results.UlTile.Relative(sp.RelativeX, sp.RelativeY)), sp.Structure))
                .Distinct()
                .ToArray();

            var occupiedTiles = new HashSet<Tile>(totalFootprints);

            var roadStarts = totalFootprints
                .SelectMany(t => t.OrthoNeighbors)
                .Distinct()
                .Where(t => results.Prefab.ToBorderRoads.CanBePlaced(t))
                .ToArray();

            PathRoadToEdge(
                occupiedTiles, 
                roadStarts, 
                center, 
                results, 
                Facing.North,
                t => t.Position.Y);

            PathRoadToEdge(
                occupiedTiles,
                roadStarts,
                center,
                results,
                Facing.West,
                t => t.Position.X);

            PathRoadToEdge(
                occupiedTiles,
                roadStarts,
                center,
                results,
                Facing.South,
                t => TilePosition.TILES_PER_CELL - t.Position.Y);

            PathRoadToEdge(
                occupiedTiles,
                roadStarts,
                center,
                results,
                Facing.East,
                t => TilePosition.TILES_PER_CELL - t.Position.X);
        }

        private void PathRoadToEdge(
            HashSet<Tile> occupiedTiles, 
            Tile[] roadStarts, 
            TilePosition center, 
            PlacementResult results, 
            Facing edge,
            Func<Tile, double> heuristic)
        {
            var goals = results.Settlement.Districts
                .SelectMany(d => d.AllTiles)
                .Where(t => t.GetNeighbor(edge) == null)
                .ToArray();

            var path = _tilePather.DeterminePath(
                roadStarts,
                goals,
                results.Settlement,
                heuristic,
                t => results.Prefab.ToBorderRoads.CanBePlaced(t) && !occupiedTiles.Contains(t),
                penalizeTurning: true,
                abortFactor: 5,
                stepSize: Math.Max(results.Prefab.ToBorderRoads.TilesWide, results.Prefab.ToBorderRoads.TilesHigh));

            if (path != null && path.Any())
            {
                foreach (var tile in path)
                {
                    results.AdditionalPlacements.Add(Tuple.Create(tile.Position, results.Prefab.ToBorderRoads));
                }
            }
        }

        public bool IsPendingPacement(Settlement settlement)
        {
            lock(LOCK)
            {
                return _queue.Any(pa => pa.Settlement == settlement)
                    || _results.Any(pr => pr.Settlement == settlement);
            }
        }

        private class PlacementAttempt
        {
            public Settlement Settlement { get; set; }
            public CityPrefab[] Prefabs { get; set; }
            public bool InstantBuild { get; set; }
            public CityPrefab[] StorageDistFallback { get; set; }
            public bool StorageBlocked { get; set; }
        }

        private class PlacementResult
        {
            public TilePosition UlTile { get; set; }
            public Settlement Settlement { get; set; }
            public CityPrefab Prefab { get; set; }
            public bool InstantBuild { get; set; }
            public List<Tuple<TilePosition, IStructureType>> AdditionalPlacements { get; }

            public PlacementResult()
            {
                AdditionalPlacements = new List<Tuple<TilePosition, IStructureType>>();
            }
        }
    }
}
