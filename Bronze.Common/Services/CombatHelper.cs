﻿using Bronze.Contracts;
using Bronze.Contracts.InjectableServices;
using System;

namespace Bronze.Common.Services
{
    public class CombatHelper : ICombatHelper
    {
        public double ResolveMeleeAttack(
            Random random,
            int attackSkill, 
            int defenseSkill, 
            int blockSkill, 
            double damage, 
            int armor,
            int armorPenetration)
        {
            var hitChance = (attackSkill - defenseSkill) * 0.05 + 0.5;
            var blockChance = blockSkill * 0.05 + 0.2;

            var effectiveArmor = Util.Clamp(Math.Max(0, armor - armorPenetration) / 10, 0, 0.95);

            if(random.NextDouble() < hitChance)
            {
                if (blockSkill > 0 && random.NextDouble() < blockChance)
                {
                    return 0;
                }

                return damage * (1 - effectiveArmor);
            }

            return 0;
        }

        public double ResolveRangedAttack(
            Random random,
            int attackSkill,
            int blockSkill,
            double damage,
            int armor,
            int armorPenetration)
        {
            var hitChance = attackSkill * 0.05 + 0.5;
            var blockChance = blockSkill * 0.05 + 0.2;

            var effectiveArmor = Util.Clamp(Math.Max(0, armor - armorPenetration) / 10, 0, 0.95);

            if (random.NextDouble() < hitChance)
            {
                if (blockSkill > 0 && random.NextDouble() < blockChance)
                {
                    return 0;
                }

                return damage * (1 - effectiveArmor);
            }

            return 0;
        }
    }
}
