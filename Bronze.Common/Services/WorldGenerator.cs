﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Bronze.Contracts.Exceptions;
using Bronze.Common.Generation;
using Priority_Queue;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.Data.Generation;
using Bronze.Contracts.Delegates;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data;

namespace Bronze.Common.Services
{
    public class WorldGenerator : IWorldGenerator
    {
        public event GenerationFinishedHandler OnGenerationFinished;

        private readonly IWorldStateService[] _worldStateServices;
        private readonly INameSource _nameSource;
        private readonly ILosTracker _losTracker;
        private readonly IRegionFeaturePlacer _regionFeaturePlacer;
        private readonly BronzeColor[] _tribeColors;
        private readonly IPeopleHelper _peopleHelper;
        private int _nextColorIndex;

        public WorldGenerator(
            IEnumerable<IWorldStateService> worldStateServices,
            INameSource nameSource,
            ILosTracker losTracker,
            IRegionFeaturePlacer regionFeaturePlacer,
            IPeopleHelper peopleHelper)
        {
            _worldStateServices = worldStateServices.ToArray();
            _nameSource = nameSource;
            _losTracker = losTracker;
            _regionFeaturePlacer = regionFeaturePlacer;
            _peopleHelper = peopleHelper;

            _tribeColors = Enum.GetValues(typeof(BronzeColor))
                .Cast<BronzeColor>()
                .Where(c => c != BronzeColor.None)
                .ToArray();
        }

        public void GenerateNewWorld(
            ReportProgress reportProgress,
            WorldgenParameters parameters)
        {
            var stopwatch = Stopwatch.StartNew();
            foreach (var service in _worldStateServices)
            {
                service.Clear();
            }

            var context = new GenerationContext(
                new Random(parameters.Seed.GetHashCode()),
                parameters);

            _nextColorIndex = 0;

            var colorPermutations = _tribeColors
                .SelectMany(c1 => _tribeColors.Select(c2 => Tuple.Create(c1, c2)))
                .Where(t => t.Item1 != parameters.PlayerTribe.PrimaryColor && t.Item2 != parameters.PlayerTribe.SecondaryColor)
                .ToArray();

            context.Random.Shuffle(colorPermutations);

            _nameSource.InitializeForSeed(parameters.Seed.GetHashCode());

            var planesToProcess = new List<PlaneFeature>(parameters.WorldType.PlaneFeatures);

            var count = 0.0;
            
            for(var i = 0; i < planesToProcess.Count; i++)
            {
                context.PlaneToId.Add(planesToProcess[i].Id, i + 1);
            }

            while (planesToProcess.Any())
            {
                var nextPlane = planesToProcess
                    .Where(pf => string.IsNullOrEmpty(pf.MirrorsPlane) || context.Planes.Keys.Any(k => k.Id == pf.MirrorsPlane))
                    .FirstOrDefault();

                if (nextPlane == null)
                {
                    throw new GenerationException("Could find a plane to generate. Plane mirroring order could not be handled. Look for missing mirror targets or circular mirroring.");
                }

                planesToProcess.Remove(nextPlane);

                var newPlane = GeneratePlane(
                    context,
                    nextPlane,
                    colorPermutations,
                    (stepProgress) => reportProgress((count / parameters.WorldType.PlaneFeatures.Length) * 0.5, "Generating " + nextPlane.Name, stepProgress));
                
                context.Planes.Add(nextPlane, newPlane);
                count += 1;
            }
            
            context.Random.Shuffle(context.TribePlacements);
            
            count = 0.0;
            var claimedRegions = new List<Region>();
            var placedTribes = new List<Tribe>();
            foreach(var tribePlacement in context.TribePlacements.OrderByDescending(p => p.HighPriority))
            {
                var wasPlaced = PlaceTribe(tribePlacement, claimedRegions, context.Random, parameters.Hostility);

                if(wasPlaced)
                {
                    placedTribes.Add(tribePlacement.Tribe);
                }

                if(!wasPlaced && tribePlacement.Tribe == parameters.PlayerTribe)
                {
                    throw new GenerationException($"Could not place settlement for player's tribe.");
                }

                reportProgress(0.6, "Placing Tribes", count / context.TribePlacements.Count);
                count += 1;
            }

            if (!placedTribes.Contains(parameters.PlayerTribe))
            {
                placedTribes.Add(parameters.PlayerTribe);
            }
            
            count = 0.0;
            foreach (var tribe in placedTribes)
            {
                tribe.Ruler = tribe.NotablePeople.OrderBy(p => p.BirthDate).FirstOrDefault();

                if (tribe.Ruler != null)
                {
                    tribe.Heir = tribe.Ruler.Children
                        .Where(c => c.IsMale && !c.IsDead)
                        .OrderBy(p => p.BirthDate)
                        .FirstOrDefault();
                }
                
                reportProgress(0.7, "Seeding Tribes", count / placedTribes.Count);
                count += 1;
            }

            var worldState = new WorldState(
                parameters.WorldName,
                parameters.Seed,
                Constants.START_MONTH,
                parameters.Hostility,
                parameters.WorldType.Size,
                context.Planes.Values.ToArray(),
                parameters.PlayerTribe,
                placedTribes,
                placedTribes.SelectMany(t => t.NotablePeople),
                parameters.Preview,
                parameters.WorldType.EdgeAdjacencies,
                parameters.WorldType.WorldTraits);

            count = 0.0;
            foreach (var service in _worldStateServices)
            {
                reportProgress(0.9, "Final Initialization", count / _worldStateServices.Length);
                service.Initialize(worldState);
                count += 1;
            }

            foreach(var settlement in parameters.PlayerTribe.Settlements)
            {
                _losTracker.ExploreAround(parameters.PlayerTribe, settlement.Region);
            }

            _losTracker.GodVision = parameters.Preview;

            stopwatch.Stop();
            OnGenerationFinished?.Invoke(parameters, stopwatch.Elapsed);
        }

        private Plane GeneratePlane(
            GenerationContext context,
            PlaneFeature planeFeature,
            Tuple<BronzeColor, BronzeColor>[] colorPermutations,
            ReportStepProgress reportProgress)
        {
            context.ClearForNewPlane();

            reportProgress(0.00);
            var startingRegionId = 1;
            Plane parentPlane = null;

            if (context.Planes.Any())
            {
                startingRegionId = context.Planes.Values.SelectMany(p => p.Regions).Max(r => r.Id) + 1;
            }

            if (!string.IsNullOrWhiteSpace(planeFeature.MirrorsPlane))
            {
                parentPlane = context.Planes
                    .Where(kvp => kvp.Key.Id == planeFeature.MirrorsPlane)
                    .Select(kvp => kvp.Value)
                    .FirstOrDefault();

                if (parentPlane == null)
                {
                    throw new GenerationException("Could not find expected parent plane: " + planeFeature.MirrorsPlane);
                }
            }

            var planeId = context.PlaneToId[planeFeature.Id];
            // Build out the cells
            var cells = new Cell[context.Parameters.WorldType.Size, context.Parameters.WorldType.Size];
            for (var x = 0; x < context.Parameters.WorldType.Size; x++)
            {
                for (var y = 0; y < context.Parameters.WorldType.Size; y++)
                {
                    cells[x, y] = new Cell(new CellPosition(planeId, x, y));
                }
            }

            reportProgress(0.10);
            Util.LinkAdjacentCells(cells);

            // Build out the regions
            reportProgress(0.15);
            IEnumerable<Region> regions;
            if (parentPlane == null)
            {
                regions = BuildRegions(startingRegionId, context.Random, cells, planeFeature.RegionGenerator);
            }
            else
            {
                regions = CopyRegions(startingRegionId, cells, parentPlane);
            }

            reportProgress(0.25);
            Util.LinkAdjacentRegions(regions);

            var plane = new Plane(
                planeId,
                planeFeature.Name,
                regions,
                planeFeature.NativePlaneChanging);
            
            context.PlacedRegionFeatures.Add(plane, new List<PlacedRegionFeature>());
            
            // Execute Seed Points
            reportProgress(0.30);

            _regionFeaturePlacer.FillOutFeature(
                context,
                plane,
                planeFeature.Biome,
                planeFeature.CellFeature,
                parentPlane,
                planeFeature.RegionMirrors,
                Enumerable.Empty<InclusionFeature>(),
                Enumerable.Empty<BorderFeature>(),
                Enumerable.Empty<SubFeature>(),
                regions);

            GenUtils.EvaluateSeedPointsGroups(
                context,
                planeFeature.Id,
                context.Parameters.WorldType.Size,
                0,
                0,
                planeFeature.SeedPoints,
                planeFeature.SeedPointGroups);

            var seedPointBuckets = context.PlannedSeedPoints[planeFeature.Id]
                .GroupBy(spp => spp.Item1.Priority)
                .OrderBy(g => g.Key)
                .ToArray();

            var count = 0.0;
            foreach (var priorityBucket in seedPointBuckets)
            {
                PlaceSeedPoints(
                    context, 
                    plane, 
                    0, 0, 
                    plane.Regions.Except(context.MirroredRegions).ToArray(), 
                    priorityBucket.ToList());

                count += 1;

                reportProgress(0.30 + count / seedPointBuckets.Length * 0.3);
            }
            
            PlaceBorderFeatures(
                context,
                plane,
                progress => reportProgress(0.60 + count / context.BorderFeaturesToPlace.Count * 0.15));

            FillOutRivers(
                context,
                plane,
                progress => reportProgress(0.75 + count / context.RiverSpawners.Count * 0.8));

            var midpathNetwork = DetermineMidpathNetwork(
                context,
                plane,
                progress => reportProgress(0.83 + count / context.RiverSpawners.Count * 0.2));

            FillOutMidpaths(
                context,
                plane,
                midpathNetwork,
                progress => reportProgress(0.85 + count / context.RiverSpawners.Count * 0.5));

            // Find out the "innermost" region feature to own each region
            var regionFeatureOwnership = new Dictionary<Region, string>();
            foreach(var placedRegion in context.PlacedRegionFeatures[plane])
            {
                foreach(var region in placedRegion.ContainedRegions)
                {
                    if(!regionFeatureOwnership.ContainsKey(region))
                    {
                        regionFeatureOwnership.Add(region, placedRegion.RegionFeature.Id);
                    }
                    else
                    {
                        regionFeatureOwnership[region] = placedRegion.RegionFeature.Id;
                    }
                }
            }
            var distinctFeatureIds = context.PlacedRegionFeatures[plane]
                .Select(prf => prf.RegionFeature.Id)
                .Distinct()
                .ToArray();
            var distinctFeatures = distinctFeatureIds
                .Select(id => context.PlacedRegionFeatures[plane].Where(prf => prf.RegionFeature.Id == id).First())
                .ToArray();

            foreach(var distinctFeature in distinctFeatures)
            {
                var ownedRegions = regionFeatureOwnership
                    .Where(r => r.Value == distinctFeature.RegionFeature.Id)
                    .Select(r => r.Key)
                    .ToArray();

                var spawners = GenUtils.DetermineFeaturesToPlace(
                        context.Random,
                        distinctFeature.RegionFeature.TribeSpawners,
                        ownedRegions.Length,
                        x => x.RegionsPerOccurance,
                        x => x.MinimumOccurances);

                foreach(var spawner in spawners)
                {
                    _nextColorIndex = (_nextColorIndex + 1) % colorPermutations.Length;

                    var tribe = new Tribe
                    {
                        Race = spawner.Race,
                        PrimaryColor = colorPermutations[_nextColorIndex].Item1,
                        SecondaryColor = colorPermutations[_nextColorIndex].Item2,
                        Name = _nameSource.MakeTribeName(spawner.Race),
                    };

                    spawner.StartingPeopleScheme.CreateInitialRulers(context.Random, tribe);

                    context.TribePlacements.Add(new TribePlacement(
                        tribe,
                        ownedRegions,
                        spawner.RequiredTraits,
                        spawner.PreferredTraits,
                        spawner.NotPreferredTraits,
                        spawner.PreferredNeighborTraits,
                        new Trait[0],
                        new Trait[0],
                        new Trait[0],
                        spawner.StartingDevelopmentType,
                        spawner.StartingCaste,
                        spawner.StartingPops,
                        context.Random.Next(spawner.MinTerritory, spawner.MaxTerritory),
                        false,
                        spawner.TribeControllerType,
                        new EmpirePlacementSeedDevelopment[0]));
                }
            }
            
            count = 0.0;
            foreach (var region in plane.Regions)
            {
                region.Name = _nameSource.MakeRegionName(planeFeature.RegionNamePattern, region);
                
                reportProgress(0.9 + plane.Regions.Count() / count * 0.10);
            }

            reportProgress(1.00);
            return plane;
        }
        
        private IEnumerable<Region> BuildRegions(
            int startingRegionId,
            Random random,
            Cell[,] cells,
            IRegionGenerator regionGenerator)
        {
            var tentativeRegions = regionGenerator.FormRegions(random, cells);
            
            var regions = new List<Region>();
            foreach (var tentativeRegion in tentativeRegions.Where(tr => tr.Value.Any()))
            {

                var newRegion = new Region(startingRegionId);
                regions.Add(newRegion);
                startingRegionId += 1;

                foreach (var cell in tentativeRegion.Value)
                {
                    newRegion.AddCell(cell);
                }
            }

            return regions;
        }

        private IEnumerable<Region> CopyRegions(
            int startingRegionId,
            Cell[,] cells,
            Plane parent)
        {
            var regions = new List<Region>();
            foreach (var regionToCopy in parent.Regions)
            {
                var newRegion = new Region(startingRegionId);
                regions.Add(newRegion);
                startingRegionId += 1;

                foreach (var cellToCopy in regionToCopy.Cells)
                {
                    newRegion.AddCell(cells[cellToCopy.Position.X, cellToCopy.Position.Y]);
                }
            }

            return regions;
        }

        private void PlaceSeedPoints(
            GenerationContext context,
            Plane plane,
            int basisX,
            int basisY,
            Region[] regions,
            List<Tuple<ISeedPoint, System.Numerics.Vector2>> seedPointPlacements)
        {
            context.Random.Shuffle(seedPointPlacements);

            var seedPointCells = seedPointPlacements
                .ToDictionary(spp => spp.Item1, spp => new List<Cell>());

            foreach (var cell in regions.SelectMany(r => r.Cells))
            {
                var nearSeedPointPlacement = seedPointPlacements
                    .Select(spp => Tuple.Create(spp, (cell.Position.ToVector() - spp.Item2).Length()))
                    .Where(t => t.Item2 <= t.Item1.Item1.InfluenceRange)
                    .OrderBy(t => t.Item1.Item1.InfluenceWeight / Math.Max(0.5, t.Item2))
                    .Select(t => t.Item1)
                    .FirstOrDefault();

                if (nearSeedPointPlacement != null)
                {
                    seedPointCells[nearSeedPointPlacement.Item1].Add(cell);
                }
            }


            var seedPointRegions = seedPointPlacements
                .ToDictionary(spp => spp.Item1, spp => new List<Region>());
            foreach (var region in regions)
            {
                var nearSeedPointPlacement = seedPointPlacements
                    .Where(t => t.Item1.ClaimsRegions)
                    .Select(spp => Tuple.Create(spp, (region.BoundingBox.Center.ToVector2() - spp.Item2).Length()))
                    .Where(t => t.Item2 <= t.Item1.Item1.InfluenceRange)
                    .OrderByDescending(t => t.Item1.Item1.InfluenceWeight / Math.Max(0.5, t.Item2))
                    .Select(t => t.Item1)
                    .FirstOrDefault();

                if (nearSeedPointPlacement != null)
                {
                    seedPointRegions[nearSeedPointPlacement.Item1].Add(region);
                }
            }

            foreach (var seedPointPlacement in seedPointPlacements)
            {
                seedPointPlacement.Item1.Generate(
                    context,
                    plane,
                    seedPointPlacement.Item2,
                    seedPointCells[seedPointPlacement.Item1],
                    seedPointRegions[seedPointPlacement.Item1]);
            }
        }

        private void FillOutRivers(
            GenerationContext context,
            Plane plane,
            ReportStepProgress reportProgress)
        {
            var placedRivers = plane.Regions
                .SelectMany(r => r.Cells)
                .ToDictionary(c => c, c => (RiverType)null);

            var riverFlow = plane.Regions
                .SelectMany(r => r.Cells)
                .ToDictionary(c => c, c => (Cell)null);

            var mergeThresholds = plane.Regions
                .SelectMany(r => r.Cells)
                .ToDictionary(c => c, c => 0);

            var isBorderCell = plane.Regions
                .SelectMany(r => r.Cells)
                .ToDictionary(c => c, c => c.Neighbors.Any(n => n != null && n.Region != c.Region));

            var count = 0.0;

            // First path all the rivers
            foreach (var start in context.RiverSpawners.Keys)
            {
                var river = context.RiverSpawners[start].RiverType;

                if (placedRivers[start] == null)
                {
                    var path = PathRiver(
                        context.Random,
                        start,
                        placedRivers,
                        isBorderCell,
                        riverFlow,
                        river);

                    // The path runs backwards, from end to start
                    var flowTo = (Cell)null;
                    foreach (var cell in path)
                    {
                        mergeThresholds[cell] += river.MergeToAdds;

                        // Place rivers in cells without rivers
                        if (placedRivers[cell] == null || river.MergeSize > placedRivers[cell].MergeSize)
                        {
                            placedRivers[cell] = river;
                            riverFlow[cell] = flowTo;
                        }
                        else // Merge and upgrade rivers.
                        {

                            if (placedRivers[cell].MergeTo != null && mergeThresholds[cell] > placedRivers[cell].MergeToThreshold)
                            {
                                placedRivers[cell] = placedRivers[cell].MergeTo;
                            }
                        }

                        flowTo = cell;
                    }
                }

                count += 1;
                reportProgress(count / context.RiverSpawners.Count * 0.5);
            }

            var cellsWithRivers = new List<Cell>();
            var riverCells = new Dictionary<RiverType, List<Cell>>();

            // Apply the rivers to the world
            foreach (var kvp in placedRivers.Where(kvp => kvp.Value != null))
            {
                var cell = kvp.Key;
                var river = kvp.Value;

                // Don't place rivers when shortcutting
                if (!river.ShortcutConditions.All(c => c.IsValid(cell)))
                {
                    if (river.Biome != null
                        && (!river.BiomeChangeConditions.Any() || river.BiomeChangeConditions.All(c => c.IsValid(cell))))
                    {
                        cell.Biome = river.Biome;
                    }

                    cell.Feature = river.CellFeature;
                    cell.Development = null;

                    cellsWithRivers.Add(cell);

                    if(!riverCells.ContainsKey(river))
                    {
                        riverCells.Add(river, new List<Cell>());
                    }
                    riverCells[river].Add(cell);
                }
            }

            var cellsWithInclusions = new HashSet<Cell>();
            foreach(var river in riverCells.Keys)
            {
                var area = riverCells[river];

                var fordArea = area
                    .Where(a => (area.Contains(a.GetNeighbor(Facing.North)) && area.Contains(a.GetNeighbor(Facing.South)))
                        || (area.Contains(a.GetNeighbor(Facing.West)) && area.Contains(a.GetNeighbor(Facing.East))))
                    .ToArray();
                
                _regionFeaturePlacer.PlaceInclusions(
                    context,
                    plane,
                    river.Fords,
                    fordArea);

                if(river.DeltaConditions.Any())
                {
                    var deltaArea = area
                        .Where(a => river.DeltaConditions.All(c => c.IsValid(a)))
                        .ToArray();

                    PlaceInclusionRadii(
                        context,
                        plane,
                        deltaArea,
                        river.DeltaInclusionRadii,
                        cellsWithInclusions,
                        c => placedRivers[c] == null && river.InclusionConditions.All(ic => ic.IsValid(c)));
                }
                
                PlaceInclusionRadii(
                    context,
                    plane,
                    area,
                    river.InclusionRadii,
                    cellsWithInclusions,
                    c => placedRivers[c] == null && river.InclusionConditions.All(ic => ic.IsValid(c))); 
            }
        }

        private IEnumerable<Cell> PathRiver(
            Random random,
            Cell start,
            Dictionary<Cell, RiverType> placedRivers,
            Dictionary<Cell, bool> isBorderCell,
            Dictionary<Cell, Cell> riverFlow,
            RiverType river)
        {
            var foundBorder = false;
            
            var closed = new HashSet<Cell>();
            var cameFrom = new Dictionary<Cell, Cell>();
            var distTo = new Dictionary<Cell, double>();
            var open = new SimplePriorityQueue<Cell, double>();

            cameFrom.Add(start, null);
            distTo.Add(start, 0);
            open.Enqueue(start, 0);

            while (open.Any())
            {
                var current = open.Dequeue();
                closed.Add(current);

                foundBorder = foundBorder || isBorderCell[current];

                // Once we've reached a border, don't consider any non-border cells
                if (foundBorder && !isBorderCell[current])
                {
                    continue;
                }

                // The river is too long, abort
                if (distTo[current] >= river.MaxLength)
                {
                    return Enumerable.Empty<Cell>();
                }

                var ends = current.OrthoNeighbors
                    .Where(n => n != null)
                    .Where(n => river.EndConditions.All(c => c.IsValid(n)) 
                        || placedRivers[n]?.MergeGroup == river.MergeGroup)
                    .ToArray();
                
                // Found an end to the river, terminate.
                if (ends.Any())
                {
                    var toMerge = ends
                        .Where(e => placedRivers[e]?.MergeGroup == river.MergeGroup)
                        .Where(e => riverFlow[e] != null)
                        .ToArray();


                    // Found a river to flow into, join it
                    if (toMerge.Any())
                    {
                        var mergeTarget = random.Choose(toMerge);
                        cameFrom[mergeTarget] = current;
                        current = mergeTarget;
                        while (riverFlow[current] != null)
                        {
                            cameFrom[riverFlow[current]] = current;
                            current = riverFlow[current];
                        }
                    }
                    else // move to the endpoint
                    {
                        var end = random.Choose(ends);
                        cameFrom[end] = current;
                        current = end;
                    }

                    // Construct the path
                    var path = new List<Cell>();
                    while (current != null)
                    {
                        path.Add(current);
                        current = cameFrom[current];
                    }

                    // River is long enough, return the path
                    if (path.Count >= river.MinLength)
                    {
                        return path;
                    }
                    else // river is too short, abort
                    {
                        return Enumerable.Empty<Cell>();
                    }
                }
                
                // Potential next steps must not have a river already, and either meet all of the path conditions or all of the shortcut conditions
                // Once we've reached a border, don't consider any non-border cells
                var potentials = current.OrthoNeighbors
                    .Where(n => n != null)
                    .Where(n => river.PathConditions.All(c => c.IsValid(n)) || river.ShortcutConditions.All(c => c.IsValid(n)))
                    .Where(n => placedRivers[n] == null)
                    .Where(n => !foundBorder || isBorderCell[n])
                    .ToArray();

                foreach (var potential in potentials)
                {
                    if (!closed.Contains(potential))
                    {
                        var dist = distTo[current] + 1;

                        // The rivers should prefer shortcuts
                        if (river.ShortcutConditions.All(c => c.IsValid(potential)))
                        {
                            dist = distTo[current] + 0.001;
                        }

                        if (cameFrom.ContainsKey(potential))
                        {
                            if (distTo[potential] < dist)
                            {
                                distTo[potential] = dist;
                                cameFrom[potential] = current;
                                open.UpdatePriority(potential, dist);
                            }
                        }
                        else
                        {
                            distTo.Add(potential, dist);
                            cameFrom.Add(potential, current);
                            open.Enqueue(potential, dist);
                        }
                    }
                }
            }

            return Enumerable.Empty<Cell>();
        }

        private Dictionary<Cell, bool> DetermineMidpathNetwork(
            GenerationContext context,
            Plane plane,
            ReportStepProgress reportProgress)
        {
            var network = new Dictionary<Cell, bool>();
            
            var size = context.Parameters.WorldType.Size;

            var cellLookup = plane.Regions.SelectMany(r => r.Cells)
                .ToDictionary(c => c.Position, c => c);
            for (var x = 0; x < size; x++)
            {
                for (var y = 0; y < size; y++)
                {
                    var actualPos = new CellPosition(
                        plane.Id,
                        x,
                        y);

                    var lookupPos = new CellPosition(
                        plane.Id,
                        size - x - 1,
                        size - y - 1);
                    
                    network.Add(
                        cellLookup[actualPos],
                        cellLookup[lookupPos].Neighbors.Any(n => n.Region != cellLookup[lookupPos].Region));
                }
            }

            return network;
        }
        
        private IEnumerable<Cell> PathBetweenCenters(
            Cell start,
            Cell goal)
        {
            var closed = new HashSet<Cell>();
            var cameFrom = new Dictionary<Cell, Cell>();
            var distTo = new Dictionary<Cell, double>();
            var open = new SimplePriorityQueue<Cell, double>();
            
            cameFrom.Add(start, null);
            distTo.Add(start, 0);

            while (open.Any())
            {
                var current = open.Dequeue();
                closed.Add(current);
                
                if(start == goal)
                {
                    // Construct the path
                    var path = new List<Cell>();
                    while (current != null)
                    {
                        path.Add(current);
                        current = cameFrom[current];
                    }

                    return path;
                }

                var potentials = current.OrthoNeighbors.ToArray();
                
                foreach (var potential in potentials)
                {
                    if (!closed.Contains(potential))
                    {
                        var dist = distTo[current];
                        
                        if (cameFrom.ContainsKey(potential))
                        {
                            if (distTo[potential] < dist)
                            {
                                distTo[potential] = dist;
                                cameFrom[potential] = current;
                                open.UpdatePriority(potential, dist);
                            }
                        }
                        else
                        {
                            distTo.Add(potential, dist);
                            cameFrom.Add(potential, current);
                            open.Enqueue(potential, dist);
                        }
                    }
                }
            }

            return Enumerable.Empty<Cell>();
        }
        
        private void FillOutMidpaths(
            GenerationContext context,
            Plane plane,
            Dictionary<Cell, bool> midpathNetwork,
            ReportStepProgress reportProgress)
        {
            var placedRivers = plane.Regions
                .SelectMany(r => r.Cells)
                .ToDictionary(c => c, c => (MidpathType)null);

            var riverFlow = plane.Regions
                .SelectMany(r => r.Cells)
                .ToDictionary(c => c, c => (Cell)null);

            var mergeThresholds = plane.Regions
                .SelectMany(r => r.Cells)
                .ToDictionary(c => c, c => 0);
            
            var count = 0.0;

            // First path all the rivers
            foreach (var start in context.MidpathSpawners.Keys)
            {
                var river = context.MidpathSpawners[start].MidpathType;

                if (placedRivers[start] == null)
                {
                    var path = PathMidpath(
                        context.Random,
                        start,
                        placedRivers,
                        riverFlow,
                        river,
                        midpathNetwork);

                    // The path runs backwards, from end to start
                    var flowTo = (Cell)null;
                    foreach (var cell in path)
                    {
                        mergeThresholds[cell] += river.MergeToAdds;

                        // Place rivers in cells without rivers
                        if (placedRivers[cell] == null || river.MergeSize > placedRivers[cell].MergeSize)
                        {
                            placedRivers[cell] = river;
                            riverFlow[cell] = flowTo;
                        }
                        else // Merge and upgrade rivers.
                        {
                            if (placedRivers[cell].MergeTo != null && mergeThresholds[cell] > placedRivers[cell].MergeToThreshold)
                            {
                                placedRivers[cell] = placedRivers[cell].MergeTo;
                            }
                        }

                        flowTo = cell;
                    }
                }

                count += 1;
                reportProgress(count / context.RiverSpawners.Count * 0.5);
            }

            var cellsWithRivers = new List<Cell>();
            var riverCells = new Dictionary<MidpathType, List<Cell>>();

            // Apply the rivers to the world
            foreach (var kvp in placedRivers.Where(kvp => kvp.Value != null))
            {
                var cell = kvp.Key;
                var river = kvp.Value;

                // Don't place rivers when shortcutting
                if (!river.ShortcutConditions.All(c => c.IsValid(cell)))
                {
                    if (river.Biome != null
                        && (!river.BiomeChangeConditions.Any() || river.BiomeChangeConditions.All(c => c.IsValid(cell))))
                    {
                        cell.Biome = river.Biome;
                    }

                    cell.Feature = river.CellFeature;
                    cell.Development = null;

                    cellsWithRivers.Add(cell);

                    if (!riverCells.ContainsKey(river))
                    {
                        riverCells.Add(river, new List<Cell>());
                    }
                    riverCells[river].Add(cell);
                }
            }

            var cellsWithInclusions = new HashSet<Cell>();
            foreach (var river in riverCells.Keys)
            {
                var area = riverCells[river];

                var fordArea = area
                    .Where(a => (area.Contains(a.GetNeighbor(Facing.North)) && area.Contains(a.GetNeighbor(Facing.South)))
                        || (area.Contains(a.GetNeighbor(Facing.West)) && area.Contains(a.GetNeighbor(Facing.East))))
                    .ToArray();

                _regionFeaturePlacer.PlaceInclusions(
                    context,
                    plane,
                    river.Fords,
                    fordArea);

                if (river.DeltaConditions.Any())
                {
                    var deltaArea = area
                        .Where(a => river.DeltaConditions.All(c => c.IsValid(a)))
                        .ToArray();

                    PlaceInclusionRadii(
                        context,
                        plane,
                        deltaArea,
                        river.DeltaInclusionRadii,
                        cellsWithInclusions,
                        c => placedRivers[c] == null && river.InclusionConditions.All(ic => ic.IsValid(c)));
                }

                PlaceInclusionRadii(
                    context,
                    plane,
                    area,
                    river.InclusionRadii,
                    cellsWithInclusions,
                    c => placedRivers[c] == null && river.InclusionConditions.All(ic => ic.IsValid(c)));
            }
        }
        
        private IEnumerable<Cell> PathMidpath(
            Random random,
            Cell start,
            Dictionary<Cell, MidpathType> placedRivers,
            Dictionary<Cell, Cell> riverFlow,
            MidpathType river,
            Dictionary<Cell, bool> midpathNetwork)
        {
            var foundNetwork = false;

            var closed = new HashSet<Cell>();
            var cameFrom = new Dictionary<Cell, Cell>();
            var distTo = new Dictionary<Cell, double>();
            var open = new SimplePriorityQueue<Cell, double>();

            cameFrom.Add(start, null);
            distTo.Add(start, 0);
            open.Enqueue(start, 0);

            while (open.Any())
            {
                var current = open.Dequeue();
                closed.Add(current);

                foundNetwork = foundNetwork || midpathNetwork[current];

                // Once we've reached a border, don't consider any non-border cells
                if (foundNetwork && !midpathNetwork[current])
                {
                    continue;
                }

                // The river is too long, abort
                if (distTo[current] >= river.MaxLength)
                {
                    return Enumerable.Empty<Cell>();
                }

                var ends = current.OrthoNeighbors
                    .Where(n => n != null)
                    .Where(n => river.EndConditions.All(c => c.IsValid(n))
                        || placedRivers[n]?.MergeGroup == river.MergeGroup)
                    .ToArray();

                // Found an end to the river, terminate.
                if (ends.Any())
                {
                    var toMerge = ends
                        .Where(e => placedRivers[e]?.MergeGroup == river.MergeGroup)
                        .Where(e => riverFlow[e] != null)
                        .ToArray();


                    // Found a river to flow into, join it
                    if (toMerge.Any())
                    {
                        var mergeTarget = random.Choose(toMerge);
                        cameFrom[mergeTarget] = current;
                        current = mergeTarget;
                        while (riverFlow[current] != null)
                        {
                            cameFrom[riverFlow[current]] = current;
                            current = riverFlow[current];
                        }
                    }
                    else // move to the endpoint
                    {
                        var end = random.Choose(ends);
                        cameFrom[end] = current;
                        current = end;
                    }

                    // Construct the path
                    var path = new List<Cell>();
                    while (current != null)
                    {
                        path.Add(current);
                        current = cameFrom[current];
                    }

                    // River is long enough, return the path
                    if (path.Count >= river.MinLength)
                    {
                        return path;
                    }
                    else // river is too short, abort
                    {
                        return Enumerable.Empty<Cell>();
                    }
                }

                // Potential next steps must not have a river already, and either meet all of the path conditions or all of the shortcut conditions
                // Once we've reached a border, don't consider any non-border cells
                var potentials = current.OrthoNeighbors
                    .Where(n => n != null)
                    .Where(n => river.PathConditions.All(c => c.IsValid(n)) || river.ShortcutConditions.All(c => c.IsValid(n)))
                    .Where(n => placedRivers[n] == null)
                    .Where(n => !foundNetwork || midpathNetwork[n])
                    .ToArray();

                foreach (var potential in potentials)
                {
                    if (!closed.Contains(potential))
                    {
                        var dist = distTo[current] + 1;

                        // The rivers should prefer shortcuts
                        if (river.ShortcutConditions.All(c => c.IsValid(potential)))
                        {
                            dist = distTo[current] + 0.001;
                        }

                        if (cameFrom.ContainsKey(potential))
                        {
                            if (distTo[potential] < dist)
                            {
                                distTo[potential] = dist;
                                cameFrom[potential] = current;
                                open.UpdatePriority(potential, dist);
                            }
                        }
                        else
                        {
                            distTo.Add(potential, dist);
                            cameFrom.Add(potential, current);
                            open.Enqueue(potential, dist);
                        }
                    }
                }
            }

            return Enumerable.Empty<Cell>();
        }


        private void PlaceBorderFeatures(
            GenerationContext context,
            Plane plane,
            ReportStepProgress reportProgress)
        {
            var count = 0.0;

            var borderFeaturePaths = new Dictionary<BorderFeature, List<Cell>>();
            var takenCells = new HashSet<Cell>();

            foreach (var placement in context.BorderFeaturesToPlace)
            {
                foreach (var feature in placement.ChosenFeatures)
                {
                    var startOptions = placement.OutsideBorder
                        .Where(c => !takenCells.Contains(c))
                        .Where(c => feature.StartConditions.All(s => s.IsValid(c)))
                        .ToArray();

                    if (startOptions.Any())
                    {
                        var current = context.Random.Choose(startOptions);
                        var isDone = false;
                        var cellsForBorder = new List<Cell>();
                        cellsForBorder.Add(current);
                        var targetSize = context.Random.Next(feature.MinLength, feature.MaxLength);

                        while (!isDone && cellsForBorder.Count < targetSize)
                        {
                            var endOptions = current.OrthoNeighbors
                                .Where(n => placement.OutsideBorder.Contains(n))
                                .Where(n => !cellsForBorder.Contains(n))
                                .Where(n => !takenCells.Contains(n))
                                .Where(n => feature.EndConditions.Any() && feature.EndConditions.All(c => c.IsValid(n)))
                                .ToArray();

                            if (endOptions.Any())
                            {
                                cellsForBorder.Add(context.Random.Choose(endOptions));
                                isDone = true;
                            }
                            else
                            {
                                var nextOptions = current.OrthoNeighbors
                                    .Where(n => placement.OutsideBorder.Contains(n))
                                    .Where(n => !cellsForBorder.Contains(n))
                                    .Where(n => !takenCells.Contains(n))
                                    .Where(n => feature.PathConditions.All(c => c.IsValid(n)))
                                    .ToArray();

                                if (nextOptions.Any())
                                {
                                    current = context.Random.Choose(nextOptions);
                                    cellsForBorder.Add(current);
                                }
                                else
                                {
                                    isDone = true;
                                }
                            }
                        }

                        // If the feature reached the target length, schedule it for placement
                        if (cellsForBorder.Count > feature.MinLength)
                        {
                            // First, apply interruptions
                            var interruptionsToApply = GenUtils.DetermineFeaturesToPlace(
                                context.Random,
                                feature.Interruptions,
                                cellsForBorder.Count,
                                f => f.CellsPerOccurance,
                                f => f.MinimumOccurances);

                            foreach (var interruption in interruptionsToApply)
                            {
                                if (cellsForBorder.Any())
                                {
                                    var start = context.Random.Choose(cellsForBorder);
                                    var area = start.Yield();

                                    var maximumRadius = feature.InclusionRadii
                                        .Select(r => r.Radius)
                                        .Concat(0.Yield()) // just in case there aren't any
                                        .Max();

                                    for (var i = 0; i < maximumRadius + 1; i++)
                                    {
                                        area = area.Concat(area.SelectMany(c => c.Neighbors)).ToArray();
                                    }

                                    foreach (var cell in area)
                                    {
                                        cellsForBorder.Remove(cell);
                                    }
                                }
                            }

                            foreach (var cell in cellsForBorder)
                            {
                                takenCells.Add(cell);

                                if (!borderFeaturePaths.ContainsKey(feature))
                                {
                                    borderFeaturePaths.Add(feature, new List<Cell>());
                                }

                                borderFeaturePaths[feature].Add(cell);
                            }
                        }
                    }
                }

                count += 1;
                reportProgress(count / context.BorderFeaturesToPlace.Count * 0.5);
            }

            count = 0;

            var cellsWithInclusions = new HashSet<Cell>();

            foreach (var feature in borderFeaturePaths.Keys.Where(b => b.InclusionRadii.Any()))
            {
                var area = borderFeaturePaths[feature].AsEnumerable();

                PlaceInclusionRadii(
                    context,
                    plane,
                    area,
                    feature.InclusionRadii,
                    cellsWithInclusions,
                    c => feature.PathConditions.All(con => con.IsValid(c)));
                
                count += 1;
                reportProgress(count / borderFeaturePaths.Count * 0.5);
            }
        }

        private void PlaceInclusionRadii(
            GenerationContext context,
            Plane plane,
            IEnumerable<Cell> area, 
            InclusionRadius[] inclusionRadii, 
            HashSet<Cell> cellsWithInclusions,
            Func<Cell, bool> cellIsValid)
        {
            var originalArea = area;
            
            foreach (var inclusionRadius in inclusionRadii.OrderBy(r => r.Radius))
            {
                for (var i = 0; i < inclusionRadius.Radius; i++)
                {
                    area = area
                        .SelectMany(c => c.OrthoNeighbors)
                        .Concat(area)
                        .ToArray();
                }

                area = area
                    .Where(c => cellIsValid(c))
                    .Where(c => !cellsWithInclusions.Contains(c))
                    .Where(c => Math.Sqrt(originalArea.Min(a => a.Position.DistanceSq(c.Position))) <= inclusionRadius.Radius)
                    .ToArray();

                foreach (var cell in area)
                {
                    cellsWithInclusions.Add(cell);
                }

                _regionFeaturePlacer.PlaceInclusions(
                        context,
                        plane,
                        inclusionRadius.InclusionFeatures,
                        area);
            }
        }

        private bool PlaceTribe(
            TribePlacement placement, 
            List<Region> claimedRegions,
            Random random,
            double worldHostility)
        {
            var territory = FindTerritory(placement, claimedRegions);
            var wasPlaced = false;

            foreach(var region in territory)
            {
                var startCell = region.Cells
                    .Where(c => c.Region.Settlement == null)
                    .Where(c => placement.StartingDevelopmentType.CanBePlaced(placement.Tribe, c))
                    .Where(c => !c.Traits.Intersect(placement.ForbiddenTraits).Any())
                    .Where(c => c.Traits.ContainsAll(placement.RequiredTraits))
                    .OrderByDescending(c => GenUtils.ScoreCell(
                        c,
                        placement.RequiredTraits,
                        placement.PreferredTraits,
                        new Trait[0],
                        placement.PreferredNeighborTraits,
                        placement.NotPreferredNeighborTraits,
                        placement.RequiredNeighborTraits))
                    .ThenBy(c => (c.Position.ToVector() - region.BoundingBox.Center.ToVector2()).Length())
                    .FirstOrDefault();

                if (startCell != null)
                {
                    placement.StartingDevelopmentType.Place(placement.Tribe, startCell, instantBuild: true);
                    wasPlaced = true;
                    claimedRegions.Add(region);

                    var settlement = startCell.Region.Settlement;
                    if(settlement != null)
                    {
                        for(var i = 0; i < placement.StartingPops; i++)
                        {
                            Cult cultMembership = null;

                            if (placement.StartingCaste.CultLikelyhoods.Any())
                            {
                                cultMembership = random.ChooseByLikelyhood(placement.StartingCaste.CultLikelyhoods);
                            }

                            settlement.Population.Add(new Pop(
                                placement.Tribe.Race, 
                                placement.StartingCaste,
                                cultMembership));
                        }
                    }

                    var orderedCells = region.Cells
                        .OrderBy(c => c.Position.DistanceSq(startCell.Position))
                        .ToArray();

                    foreach(var seedDevelopment in placement.SeedDevelopments)
                    {
                        var pos = orderedCells
                            .Where(c => seedDevelopment.Development.CanBePlaced(placement.Tribe, c))
                            .Where(c => c.Feature == null || !c.Feature.Traits.ContainsAny(seedDevelopment.PreserveTraits))
                            .FirstOrDefault();

                        if (pos != null)
                        {
                            seedDevelopment.Development.Place(placement.Tribe, pos, true);
                            pos.Development.ActiveRecipie = seedDevelopment.Recipie;
                        }
                        else
                        {
                            pos = orderedCells
                                .Where(c => seedDevelopment.Development.CouldBePlacedWithoutFeature(placement.Tribe, c))
                                .Where(c => c.Feature == null || c.Feature.Traits.ContainsAny(seedDevelopment.ClearTraits))
                                .Where(c => c.Feature == null || !c.Feature.Traits.ContainsAny(seedDevelopment.PreserveTraits))
                                .FirstOrDefault();

                            if (pos != null)
                            {
                                seedDevelopment.Development.Place(placement.Tribe, pos, true);
                                pos.Feature = null;
                                pos.Development.ActiveRecipie = seedDevelopment.Recipie;
                            }
                        }
                    }

                }
            }

            if(wasPlaced && placement.TribeControllerType != null)
            {
                placement.Tribe.Controller = placement.TribeControllerType
                    .BuildController(
                    placement.Tribe,
                    random,
                    worldHostility,
                    Constants.START_MONTH);
            }

            return wasPlaced;
        }

        public IEnumerable<Region> FindTerritory(
            TribePlacement tribePlacement, 
            List<Region> claimedRegions)
        {
            var territories = new List<Tuple<float, Region[]>>();
            var potentialRegions = tribePlacement.Regions.Except(claimedRegions);
            var regionScores = tribePlacement.Regions
                .ToDictionary(r => r,
                    r => GenUtils.ScoreRegion(
                        r, 
                        tribePlacement.RequiredTraits, 
                        tribePlacement.PreferredTraits, 
                        tribePlacement.NotPreferredTraits, 
                        tribePlacement.PreferredNeighborTraits));

            foreach (var region in potentialRegions)
            {
                var regionsInTerritory = new List<Region>();
                regionsInTerritory.Add(region);
                
                var done = regionsInTerritory.Count >= tribePlacement.SettlementCount;
                while(!done)
                {
                    var highestScoringNeighbor = regionsInTerritory
                        .SelectMany(r => r.Neighbors)
                        .Except(regionsInTerritory)
                        .Where(n => potentialRegions.Contains(n))
                        .OrderByDescending(r => regionScores[r])
                        .FirstOrDefault();

                    if(highestScoringNeighbor != null)
                    {
                        regionsInTerritory.Add(highestScoringNeighbor);

                        done = regionsInTerritory.Count >= tribePlacement.SettlementCount;
                    }
                    else
                    {
                        done = true;
                    }
                }

                var territoryScore = regionsInTerritory
                    .Select(r => regionScores[r])
                    .Sum();

                if(regionsInTerritory.Count >= tribePlacement.SettlementCount
                    && territoryScore > 0)
                {
                    territories.Add(Tuple.Create(territoryScore, regionsInTerritory.ToArray()));
                }
            }

            return territories
                .OrderByDescending(t => t.Item1)
                .Select(t => t.Item2)
                .FirstOrDefault()
                ?? Enumerable.Empty<Region>();
        }
    }
}
