﻿using Bronze.Common.Data.Game.Units;
using Bronze.Common.Data.World.CellDevelopments;
using Bronze.Common.Data.World.Units;
using Bronze.Contracts;
using Bronze.Contracts.Data.Diplomacy;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.Services
{
    public class AiHelper : IAiHelper
    {
        private readonly IUnitHelper _unitHelper;
        private readonly IWorldManager _worldManager;
        private readonly IDialogManager _dialogManager;
        private readonly IDiplomacyHelper _diplomacyHelper;
        private readonly IWarHelper _warHelper;
        private readonly Random _random;

        public AiHelper(
            IUnitHelper unitHelper,
            IWorldManager worldManager,
            IDialogManager dialogManager,
            IDiplomacyHelper diplomacyHelper,
            IWarHelper warHelper)
        {
            _unitHelper = unitHelper;
            _worldManager = worldManager;
            _dialogManager = dialogManager;
            _diplomacyHelper = diplomacyHelper;
            _warHelper = warHelper;
            _random = new Random();
        }

        public void CheckForArmyStates(Tribe tribe, Dictionary<string, SerializedObject> armyStates)
        {
            foreach(var army in tribe.Armies)
            {
                if(!armyStates.ContainsKey(army.Id))
                {
                    var armyState = new SerializedObject("army_state");
                    armyStates.Add(army.Id, armyState);
                    armyState.Set("army_id", army.Id);

                    if (army.Units.Any(u => u is IGrazingUnit))
                    {
                        armyState.Set("role", "grazing");
                    }
                    else
                    {
                        armyState.Set("role", "imperial");
                    }
                }
            }
        }

        public void SetSettlementIntruderStates(Tribe tribe, Dictionary<string, SerializedObject> settlementStates)
        {
            foreach (var settlement in tribe.Settlements.Where(s=> settlementStates.ContainsKey(s.Id)))
            {
                var settlementState = settlementStates[settlement.Id];
                var hasIntruder = false;
                var intruderStrength = 0.0;
                var localStrength = 0.0;

                foreach (var army in settlement.Region.WorldActors.OfType<Army>())
                {
                    if (tribe.IsHostileTo(army.Owner))
                    {
                        hasIntruder = true;
                        intruderStrength += army.StrengthEstimate;
                    }
                    else if (tribe == army.Owner)
                    {
                        localStrength += army.StrengthEstimate;
                    }
                }
                settlementState.Set("has_intruder", hasIntruder);
                settlementState.Set("intruder_strength", intruderStrength);
                settlementState.Set("local_strength", localStrength);
            }
        }
        
        public void FormImperialArmies(
            Tribe tribe,
            Dictionary<string, SerializedObject> armyStates)
        {
            var hasArmyWithCapacity = tribe.Armies
                .Where(a => armyStates.ContainsKey(a.Id))
                .Where(a => armyStates[a.Id].GetString("role") == "imperial")
                .Where(a => !a.IsFull)
                .Any();

            var settlementWithRecruits = tribe.Settlements
                .Select(s => Tuple.Create(s, s.RecruitmentSlots.Where(kvp => kvp.Key.StrengthEstimate > 0).Sum(kvp => (int)kvp.Value)))
                .Where(t => t.Item2 > 0)
                .OrderByDescending(t => t.Item2)
                .Select(t => t.Item1)
                .FirstOrDefault();

            if (!hasArmyWithCapacity && settlementWithRecruits != null)
            {
                var general = tribe.NotablePeople
                    .Where(np => np.IsIdle)
                    .FirstOrDefault();

                var availableRecruits = settlementWithRecruits
                    .RecruitmentSlots
                    .Where(kvp => kvp.Value >= 1)
                    .Select(kvp => kvp.Key)
                    .ToArray();

                if (availableRecruits.Any() && general != null)
                {
                    var potentialRecruits = settlementWithRecruits.Race
                        .AvailableUnits
                        .Concat(settlementWithRecruits.EconomicActors.OfType<IRecruitableUnitTypeSource>().SelectMany(s => s.ProvidedUnitTypes))
                        .Where(u => settlementWithRecruits.RecruitmentSlots[u.RecruitmentCategory] >= 1)
                        .SelectMany(u => u.EquipmentSets)
                        .ToArray();
                    
                    var position = settlementWithRecruits
                        .EconomicActors
                        .OfType<ICellDevelopment>()
                        .Where(cd => cd is VillageDevelopment || cd is DistrictDevelopment)
                        .Select(cd => cd.Cell)
                        .Where(c => potentialRecruits.Any(es => es.CanPath(c)))
                        .FirstOrDefault();

                    if (position != null)
                    {
                        var army = _unitHelper.CreateArmy(settlementWithRecruits.Owner, position);

                        var armyState = new SerializedObject("army_state");
                        armyState.Set("army_id", army.Id);
                        armyState.Set("role", "imperial");
                        armyStates.Add(army.Id, armyState);

                        general.AssignTo(army);

                        

                        RecruitWarriors(settlementWithRecruits, army);
                    }
                }
            }
        }

        public void FormColonizerArmy(
            Tribe tribe,
            Dictionary<string, SerializedObject> armyStates)
        {
            var colonizers = tribe.Race
                .AvailableUnits
                .SelectMany(u => u.EquipmentSets.Select(e => Tuple.Create(u, e)))
                .Where(t => t.Item2.AvailableActions.Any(a => a is ArmySettleAction))
                .ToArray();

            var settlementWithColonizer = tribe.Settlements
                .Where(s => colonizers.Any(c => s.RecruitmentSlots[c.Item1.RecruitmentCategory] >= 1))
                .FirstOrDefault();

            if (settlementWithColonizer != null)
            {
                var general = tribe.NotablePeople
                    .Where(np => np.IsIdle)
                    .FirstOrDefault();

                var colonizer = colonizers
                    .Where(t => settlementWithColonizer.RecruitmentSlots[t.Item1.RecruitmentCategory] >= 1)
                    .FirstOrDefault();
                
                if (general != null && colonizer != null)
                {
                    var position = settlementWithColonizer
                        .EconomicActors
                        .OfType<ICellDevelopment>()
                        .Where(cd => cd is VillageDevelopment || cd is DistrictDevelopment)
                        .Select(cd => cd.Cell)
                        .Where(c => colonizer.Item2.CanPath(c))
                        .FirstOrDefault();

                    if (position != null)
                    {
                        var army = _unitHelper.CreateArmy(settlementWithColonizer.Owner, position);
                        
                        var armyState = new SerializedObject("army_state");
                        armyState.Set("army_id", army.Id);
                        armyState.Set("role", "imperial");
                        armyState.Set("is_colonizer", true);
                        armyStates.Add(army.Id, armyState);

                        general.AssignTo(army);
                        
                        army.Units.Add(_unitHelper.RecruitUnit(colonizer.Item1, colonizer.Item2, settlementWithColonizer));
                    }
                }
            }
        }

        public void FormLevyDefenseArmies(
            Tribe tribe,
            Settlement settlement, 
            Dictionary<string, SerializedObject> armyStates)
        {
            var general = tribe.NotablePeople
                .Where(np => np.IsIdle)
                .FirstOrDefault();

            var availableRecruits = settlement
                .RecruitmentSlots
                .Where(kvp => kvp.Value >= 1)
                .Select(kvp => kvp.Key)
                .ToArray();

            if (availableRecruits.Any() && general != null)
            {
                var potentialRecruits = settlement.Race
                    .AvailableUnits
                    .Concat(settlement.EconomicActors.OfType<IRecruitableUnitTypeSource>().SelectMany(s => s.ProvidedUnitTypes))
                    .Where(u => settlement.RecruitmentSlots[u.RecruitmentCategory] >= 1)
                    .SelectMany(u => u.EquipmentSets)
                    .Where(es => es.StrengthEstimate > 0)
                    .ToArray();

                var position = settlement
                    .EconomicActors
                    .OfType<ICellDevelopment>()
                    .Where(cd => cd is VillageDevelopment || cd is DistrictDevelopment)
                    .Select(cd => cd.Cell)
                    .Where(c => potentialRecruits.Any(es => es.CanPath(c)))
                    .FirstOrDefault();

                if (position != null)
                {
                    var army = _unitHelper.CreateArmy(settlement.Owner, position);

                    var armyState = new SerializedObject("army_state");
                    armyState.Set("army_id", army.Id);
                    armyState.Set("role", "levy");
                    armyState.Set("settlement_id", settlement.Id);

                    general.AssignTo(army);

                    armyStates.Add(army.Id, armyState);

                    RecruitWarriors(settlement, army);
                }
            }
        }

        public bool TryOrderArmyLevyPatrol(Tribe tribe, Army army, SerializedObject armyState, Random random)
        {
            var homeSettlement = tribe.Settlements.FirstOrDefault(s => s.Id == armyState.GetString("settlement_id"));

            if (homeSettlement != null)
            {
                var intruderArmy = homeSettlement.Region
                    .WorldActors.OfType<Army>()
                    .Where(a=>tribe.IsHostileTo(a.Owner))
                    .OrderBy(a => (a.Position - army.Position).LengthSquared())
                    .FirstOrDefault();

                if (intruderArmy != null)
                {
                    army.CurrentOrder = new ArmyApproachOrder(army, intruderArmy, _worldManager);

                    return true;
                }
                else
                {
                    var patrolCells = homeSettlement.Region.Cells
                        .Where(c => army.CanPath(c))
                        .ToArray();

                    if (patrolCells.Any())
                    {
                        army.CurrentOrder = new ArmyMoveOrder(army, random.Choose(patrolCells));
                    }
                }
            }

            return false;
        }

        public bool TryOrderArmyLevyDisband(Tribe tribe, Army army, SerializedObject armyState, Random random)
        {
            var homeSettlement = tribe.Settlements.FirstOrDefault(s => s.Id == armyState.GetString("settlement_id"));

            if (homeSettlement != null)
            {
                if (army.Cell.Region == homeSettlement.Region)
                {
                    foreach (var unit in army.Units.ToArray())
                    {
                        _unitHelper.Disband(army, unit);
                    }
                }
                else
                {
                    var homeCell = homeSettlement.Region.Cells
                        .Where(c => army.CanPath(c))
                        .OrderBy(c => c.Position.DistanceSq(army.Cell.Position))
                        .FirstOrDefault();

                    if (homeCell != null)
                    {
                        army.CurrentOrder = new ArmyMoveOrder(army, homeCell);
                    }
                    else
                    {
                        foreach (var unit in army.Units.ToArray())
                        {
                            _unitHelper.Disband(army, unit);
                        }
                    }
                }
            }
            else
            {
                if (homeSettlement == null || army.Cell.Region == homeSettlement.Region)
                {
                    foreach (var unit in army.Units.ToArray())
                    {
                        _unitHelper.Disband(army, unit);
                    }
                }
            }

            return true;
        }

        public bool TryOrderArmyDisband(Tribe tribe, Army army)
        {
            if (army.Cell.Owner == army.Owner)
            {
                foreach (var unit in army.Units.ToArray())
                {
                    _unitHelper.Disband(army, unit);
                }
            }
            else
            {
                var homeCell = tribe.Settlements
                    .SelectMany(s => s.Region.Cells)
                    .Where(c => army.CanPath(c))
                    .OrderBy(c => c.Position.DistanceSq(army.Cell.Position))
                    .FirstOrDefault();

                if (homeCell != null)
                {
                    army.CurrentOrder = new ArmyMoveOrder(army, homeCell);
                }
                else
                {
                    foreach (var unit in army.Units.ToArray())
                    {
                        _unitHelper.Disband(army, unit);
                    }
                }
            }

            return true;
        }

        public bool TryOrderArmyPatrol(Tribe tribe, Army army)
        {
            var intruderArmy = tribe.Settlements
                .SelectMany(s => s.Region.WorldActors.OfType<Army>())
                .Where(a => tribe.IsHostileTo(a.Owner))
                .OrderBy(a => (a.Position - army.Position).LengthSquared())
                .FirstOrDefault();

            if (intruderArmy != null)
            {
                army.CurrentOrder = new ArmyApproachOrder(army, intruderArmy, _worldManager);
                return true;
            }

            return false;
        }

        public bool TryOrderArmyGatherUnits(Tribe tribe, Army army)
        {
            if (!army.IsFull)
            {
                var potentialRecruits = tribe.Race
                    .AvailableUnits
                    .SelectMany(u => u.EquipmentSets.Select(e => Tuple.Create(u, e)))
                    .Where(t => t.Item2.CanPath(army.Cell))
                    .Where(t=>t.Item2.StrengthEstimate > 0)
                    .ToArray();

                var settlementWithRecruits = tribe.Settlements
                    .Where(s => potentialRecruits.Any(r => s.RecruitmentSlots[r.Item1.RecruitmentCategory] >= 1))
                    .OrderBy(s => (s.Region.BoundingBox.Center.ToVector2() - army.Cell.Position.ToVector()).LengthSquared())
                    .FirstOrDefault();
                
                if (settlementWithRecruits != null)
                {
                    if (army.Cell.Region == settlementWithRecruits.Region)
                    {
                        RecruitWarriors(settlementWithRecruits, army);
                        
                        return true;
                    }
                    else
                    {
                        var moveCell = settlementWithRecruits.Region.Cells
                            .Where(c => army.CanPath(c))
                            .OrderBy(c => c.Position.DistanceSq(army.Cell.Position))
                            .FirstOrDefault();

                        if (moveCell != null)
                        {
                            army.CurrentOrder = new ArmyMoveOrder(army, moveCell);
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        public bool TryOrderArmyRaiding(Tribe tribe, Army army, bool isAggressive, bool isCamping, double minSupplyPerc)
        {
            if (isAggressive && !isCamping && minSupplyPerc > 0.3)
            {
                var raidTarget = tribe.CurrentWars
                    .Select(w => w.Aggressor == tribe ? w.Defender : w.Aggressor)
                    .SelectMany(t => t.Settlements)
                    .SelectMany(s => s.Region.Cells)
                    .Where(c => c.Development != null && c.Development.CanBeRaided)
                    .OrderBy(c => c.Position.DistanceSq(army.Cell.Position))
                    .FirstOrDefault();

                if (raidTarget != null)
                {
                    army.CurrentOrder = new ArmyRaidOrder(army, raidTarget, _warHelper);
                    return true;
                }
            }

            return false;
        }

        public bool TryOrderArmyCamping(Tribe tribe, Army army, bool isCamping)
        {
            if (!isCamping && !(army.CurrentOrder is ArmyCampOrder))
            {
                var campCell = tribe.Settlements
                    .OrderBy(s => (s.Region.BoundingBox.Center.ToVector2() - army.Cell.Position.ToVector()).LengthSquared())
                    .Take(1)
                    .SelectMany(s => s.Region.Cells)
                    .Where(c => army.CanPath(c))
                    .OrderBy(c => c.Position.DistanceSq(army.Cell.Position))
                    .FirstOrDefault();

                if (campCell != null)
                {
                    army.CurrentOrder = new ArmyCampOrder(army, campCell);
                    return true;
                }
            }

            return false;
        }

        public bool TryOrderArmySiege(Tribe tribe, Army army, Region targetRegion, bool takeByForce, bool isCamping, double minSupplyPerc)
        {
            if (takeByForce 
                && targetRegion?.Settlement != null 
                && !isCamping 
                && minSupplyPerc > 0.3
                && tribe.IsHostileTo(targetRegion.Owner))
            {
                var war = tribe.CurrentWars
                    .Where(w => w.Aggressor == targetRegion.Owner || w.Defender == targetRegion.Owner)
                    .FirstOrDefault();

                var actionGiven = false;

                if(war != null && war.WarScore * (war.Aggressor == tribe ? 1 : -1) < 80)
                {
                    actionGiven = TryOrderArmyRaiding(tribe, army, true, isCamping, minSupplyPerc);
                }

                if(!actionGiven)
                {
                    var siegeTarget = targetRegion.Settlement
                        .EconomicActors
                        .OfType<ICellDevelopment>()
                        .Where(cd => cd is VillageDevelopment || cd is DistrictDevelopment)
                        .Select(cd => cd.Cell)
                        .FirstOrDefault();

                    if (siegeTarget != null)
                    {
                        army.CurrentOrder = army.AvailableActions
                            .Where(a => a is ArmyInteractAction)
                            .Select(a => a.BuildOrderFor(army, siegeTarget))
                            .FirstOrDefault();

                        return true;
                    }
                }
            }

            return false;
        }

        public bool TryOrderArmyColonize(
            Tribe tribe, 
            Army army, 
            bool isColonizer, 
            StrategicTarget strategicTarget,
            Dictionary<Trait, int> settlementPlacementGoalScore,
            Dictionary<Trait, int> settlementPlacementNeighborScores)
        {
            if(isColonizer && strategicTarget != null 
                && !strategicTarget.TakeByForce 
                && strategicTarget.TargetRegion.Owner == null)
            {
                var settleAction = army.AvailableActions
                    .OfType<ArmySettleAction>()
                    .FirstOrDefault();

                if (settleAction != null)
                {
                    var target = strategicTarget.TargetRegion.Cells
                        .Where(c => settleAction.IsValidFor(army, c))
                        .Select(c => Tuple.Create(c, ScoreCell(c, settlementPlacementGoalScore, settlementPlacementNeighborScores)))
                        .OrderBy(c => c.Item1.Position.DistanceSq(army.Cell.Position))
                        .ThenByDescending(t => t.Item2)
                        .Select(t => t.Item1)
                        .FirstOrDefault();

                    if(target != null)
                    {
                        army.CurrentOrder = settleAction.BuildOrderFor(army, target);
                        return true;
                    }
                }
            }

            return false;
        }

        public bool TryOrderCreepCleanup(Tribe tribe, Army army)
        {
            if (army.Cell.Owner == tribe)
            {
                var creepCamp = army.Cell.Region.Cells
                    .Where(c => c.Development != null && (c.Development is CreepCampDevelopment cc && !cc.IsBeingPacified || c.Development is CreepAttackableDevelopment))
                    .OrderBy(c => c.Position.DistanceSq(army.Cell.Position))
                    .FirstOrDefault();

                var interactAction = army.AvailableActions
                    .Where(a => a is ArmyInteractAction)
                    .FirstOrDefault();

                if (creepCamp != null && interactAction != null)
                {
                    army.CurrentOrder = interactAction.BuildOrderFor(army, creepCamp);
                    return true;
                }
            }

            return false;
        }

        private int ScoreCell(
            Cell cell, 
            Dictionary<Trait, int> settlementPlacementGoalScore, 
            Dictionary<Trait, int> settlementPlacementNeighborScores)
        {
            var score = 0;

            score += cell.Traits
                .Where(t => settlementPlacementGoalScore.ContainsKey(t))
                .Select(t => settlementPlacementGoalScore[t])
                .Sum();

            score += cell.Traits
                .Where(t => settlementPlacementNeighborScores.ContainsKey(t))
                .Select(t => settlementPlacementNeighborScores[t])
                .Sum();

            return score;
        }

        public void RecruitWarriors(Settlement settlement, Army army)
        {
            var stockpiles = settlement
                .EconomicActors
                .OfType<IArmoryStorageProvider>()
                .ToArray();

            foreach (var category in settlement.RecruitmentSlots.Where(kvp => kvp.Value >= 1).Select(kvp => kvp.Key).ToArray())
            {
                var potentialUnits = settlement.Race
                    .AvailableUnits
                    .Concat(settlement.EconomicActors.OfType<IRecruitableUnitTypeSource>().SelectMany(s => s.ProvidedUnitTypes))
                    .Where(u => u.RecruitmentCategory == category)
                    .SelectMany(u => u.EquipmentSets.Select(e => Tuple.Create(u, e)))
                    .Where(t => t.Item2.CanPath(army.Cell))
                    .Where(t => t.Item2.StrengthEstimate > 0)
                    .Where(t => !t.Item2.RecruitmentCost.Any() || t.Item2.RecruitmentCost.All(iq => stockpiles.Sum(s => s.Inventory.QuantityOf(iq.Item)) >= iq.Quantity))
                    .ToArray();

                while (settlement.RecruitmentSlots[category] >= 1
                    && potentialUnits.Any()
                    && !army.IsFull)
                {
                    var choices = potentialUnits
                        .GroupBy(t => t.Item2.StrengthEstimate)
                        .OrderByDescending(g => g.Key)
                        .FirstOrDefault();

                    if(choices != null && choices.Any())
                    {
                        var choice = _random.Choose(choices);

                        army.Units.Add(_unitHelper.RecruitUnit(
                            choice.Item1,
                            choice.Item2,
                            settlement));

                        potentialUnits = potentialUnits
                            .Where(t => settlement.RecruitmentSlots[t.Item1.RecruitmentCategory] >= 1)
                            .Where(t => !t.Item2.RecruitmentCost.Any() || t.Item2.RecruitmentCost.All(iq => stockpiles.Sum(s => s.Inventory.QuantityOf(iq.Item)) >= iq.Quantity))
                            .ToArray();
                    }
                }
            }
        }
    }
}
