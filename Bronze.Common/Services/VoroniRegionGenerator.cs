﻿using Bronze.Contracts;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.Services
{
    public class VoroniRegionGenerator : IRegionGenerator
    {
        private static readonly int VORONI_TARGET_SIZE = 40;

        public string RegionStyle => "voroni";

        public Dictionary<CellPosition, List<Cell>> FormRegions(Random random, Cell[,] cells)
        {
            var allCells = new List<Cell>();
            for (var x = 0; x < cells.GetLength(0); x++)
            {
                for (var y = 0; y < cells.GetLength(1); y++)
                {
                    allCells.Add(cells[x, y]);
                }
            }

            var planeId = allCells.First().Position.Plane;

            var pointCount = (cells.GetLength(0) * cells.GetLength(1)) / VORONI_TARGET_SIZE;

            // Place key points
            var keyPoints = new List<CellPosition>();
            for(var i = 0; i < pointCount; i++)
            {
                var newPos = new CellPosition(
                    planeId,
                    random.Next(0, cells.GetLength(0)),
                    random.Next(0, cells.GetLength(1)));

                if(!keyPoints.Contains(newPos))
                {
                    keyPoints.Add(newPos);
                }
            }

            var tentativeRegions = MapToClosest(keyPoints, cells, allCells);

            // Split too large regions
            foreach (var tentativeRegion in tentativeRegions)
            {
                if (tentativeRegion.Value.Count > VORONI_TARGET_SIZE * 1.5)
                {
                    keyPoints.Remove(tentativeRegion.Key);

                    for(var i = 0; i < 2; i++)
                    {
                        var newKey = random.Choose(tentativeRegion.Value).Position;

                        if(!keyPoints.Contains(newKey))
                        {
                            keyPoints.Add(newKey);
                        }
                    }
                }
            }
            tentativeRegions = MapToClosest(keyPoints, cells, allCells);

            // Cull too small regions
            foreach (var tentativeRegion in tentativeRegions)
            {
                if (tentativeRegion.Value.Count < VORONI_TARGET_SIZE * 0.75)
                {
                    keyPoints.Remove(tentativeRegion.Key);
                }
            }
            tentativeRegions = MapToClosest(keyPoints, cells, allCells);

            // Cull Nonadjacent Orphans
            var cellToRegion = tentativeRegions
                .SelectMany(tr => tr.Value.Select(c => Tuple.Create(c, tr.Key)))
                .ToDictionary(t => t.Item1, t => t.Item2);

            foreach (var cell in cellToRegion.Keys.ToArray())
            {
                if (!cell.OrthoNeighbors.Any(n => cellToRegion[cell] == cellToRegion[n]))
                {
                    var oldRegion = cellToRegion[cell];
                    var newRegion = cellToRegion[random.Choose(cell.OrthoNeighbors)];

                    cellToRegion[cell] = newRegion;
                    tentativeRegions[oldRegion].Remove(cell);
                    tentativeRegions[newRegion].Add(cell);
                }
            }

            return tentativeRegions;
        }

        private static Dictionary<CellPosition, List<Cell>>  MapToClosest(List<CellPosition> keyPoints, Cell[,] cells, List<Cell> allCells)
        {
            var claims = allCells.ToDictionary(c => c.Position, c => new List<CellPosition>());

            var initialSweep = VORONI_TARGET_SIZE / 2;

            foreach(var keypoint in keyPoints)
            {
                for(var x = -initialSweep; x <= initialSweep; x++)
                {
                    for (var y = -initialSweep; y <= initialSweep; y++)
                    {
                        var toCheck = new CellPosition(keypoint.Plane, keypoint.X + x, keypoint.Y + y);

                        if(claims.ContainsKey(toCheck))
                        {
                            claims[toCheck].Add(keypoint);
                        }
                    }
                }
            }

            var tentativeRegions = keyPoints
                .ToDictionary(k => k, k => new List<Cell>());

            foreach(var claim in claims)
            {
                var candidates = claim.Value;

                if(!candidates.Any())
                {
                    candidates = keyPoints;
                }

                var owner = candidates.GetLowest(p => p.DistanceSq(claim.Key));

                tentativeRegions[owner].Add(cells[claim.Key.X, claim.Key.Y]);
            }

            return tentativeRegions;
        }
    }
}
