﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bronze.Contracts;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.Services
{
    public class PeopleGenerator : IPeopleGenerator
    {
        private readonly INameSource _nameSource;
        private readonly INotablePersonGenerationAddon[] _generationAddons;

        public PeopleGenerator(
            INameSource nameSource,
            INotablePersonGenerationAddon[] generationAddons)
        {
            _nameSource = nameSource;
            _generationAddons = generationAddons;
        }

        public NotablePerson CreatePerson(
            Random random,
            Tribe owner,
            Race race,
            bool isMale, 
            double birthDate, 
            NotablePerson father, 
            NotablePerson mother,
            bool inDynasty)
        {
            var parts = new List<PortraitPartPlacement>();
            
            foreach(var group in race.PeoplePartGroups)
            {
                var options = new List<PeoplePart>();

                if (father != null && group.Inherited && father.PortraitParts.Any(p => p.GroupId == group.Id))
                {
                    options.Add(father.PortraitParts.First(p => p.GroupId == group.Id).Part);
                }
                else
                {
                    options.Add(random.Choose(group.Options));
                }

                if (mother != null && group.Inherited && mother.PortraitParts.Any(p => p.GroupId == group.Id))
                {
                    options.Add(mother.PortraitParts.First(p => p.GroupId == group.Id).Part);
                }
                else
                {
                    options.Add(random.Choose(group.Options));
                }

                parts.Add(new PortraitPartPlacement
                {
                    GroupId = group.Id,
                    Part = random.Choose(options)
                });
            }

            var startingSkills = new List<NotablePersonSkillPlacement>();
            foreach(var group in race.StartingSkills)
            {
                for(var i = 0; i < group.PickCount; i++)
                {
                    var options = group.Options.ToList();

                    if (group.LikelyhoodNone > 0)
                    {
                        options.Add(new LikelyhoodFor<NotablePersonSkill>()
                        {
                            Likelyhood = group.LikelyhoodNone,
                            Value = null
                        });
                    }

                    options.RemoveAll(l => startingSkills.Any(ss => ss.Skill == l.Value));

                    if(options.Any())
                    {
                        var pick = random.ChooseByLikelyhood(options);
                        if(pick != null)
                        {
                            startingSkills.Add(new NotablePersonSkillPlacement
                            {
                                Skill = pick,
                                Level = pick.Levels.Where(l => l.Level == 0).FirstOrDefault()
                            });
                        }
                    }
                }
            }
            
            if (father != null)
            {
                foreach (var skillPlacement in father.Skills)
                {
                    if(random.NextDouble() < skillPlacement.Skill.InheritChance
                        && !startingSkills.Any(s => s.Skill == skillPlacement.Skill))
                    {
                        startingSkills.Add(new NotablePersonSkillPlacement
                        {
                            Skill = skillPlacement.Skill,
                            Level = skillPlacement.Skill.Levels.Where(l => l.Level == 0).FirstOrDefault()
                        });
                    }
                }
            }

            if (mother != null)
            {
                foreach (var skillPlacement in mother.Skills)
                {
                    if (random.NextDouble() < skillPlacement.Skill.InheritChance
                        && !startingSkills.Any(s => s.Skill == skillPlacement.Skill))
                    {
                        startingSkills.Add(new NotablePersonSkillPlacement
                        {
                            Skill = skillPlacement.Skill,
                            Level = skillPlacement.Skill.Levels.Where(l => l.Level == 0).FirstOrDefault()
                        });
                    }
                }
            }

            startingSkills = startingSkills
                .Where(s => s.Level != null)
                .ToList();
            
            var person = new NotablePerson
            {
                BirthDate = birthDate,
                Father = father,
                IsMale = isMale,
                Mother = mother,
                Name = _nameSource.MakePersonName(race, isMale),
                Owner = owner,
                BirthTribe = owner,
                Race = race,
                PortraitParts = parts,
                Skills = startingSkills,
                InDynasty = inDynasty
            };

            var possibleCults = new List<LikelyhoodFor<Cult>>();

            if (person.Father != null)
            {
                person.Father.Children.Add(person);

                if (person.Father.CultMembership != null)
                {
                    possibleCults.Add(new LikelyhoodFor<Cult>
                    {
                        Likelyhood = 1,
                        Value = person.Father.CultMembership
                    });
                }
            }
            if (person.Mother != null)
            {
                person.Mother.Children.Add(person);

                if (person.Mother.CultMembership != null)
                {
                    possibleCults.Add(new LikelyhoodFor<Cult>
                    {
                        Likelyhood = 1,
                        Value = person.Mother.CultMembership
                    });
                }
            }

            if(!possibleCults.Any())
            {
                possibleCults.AddRange(race.StartingPeopleCultLikelyhoods);
            }

            if(possibleCults.Any())
            {
                person.CultMembership = random.ChooseByLikelyhood(possibleCults);
            }
            
            foreach(var addon in _generationAddons)
            {
                addon.Apply(
                    random,
                    owner,
                    race,
                    inDynasty,
                    person);
            }
            
            return person;
        }
    }
}
