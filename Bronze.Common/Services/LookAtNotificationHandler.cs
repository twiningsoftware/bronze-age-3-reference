﻿using Bronze.Contracts.Data;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.Services
{
    public class LookAtNotificationHandler : INotificationHandler
    {
        private readonly IWorldManager _worldManager;
        private readonly IPlayerDataTracker _playerData;
        
        public string Name => typeof(LookAtNotificationHandler).Name;

        public LookAtNotificationHandler(
            IWorldManager worldManager,
            IPlayerDataTracker playerData)
        {
            _worldManager = worldManager;
            _playerData = playerData;
        }
        
        public void HandleIgnored(Notification notification)
        {
        }

        public void Handle(Notification notification)
        {

            if (notification.Data.ContainsKey("region"))
            {
                var region = _worldManager.GetRegion(int.Parse(notification.Data["region"]));

                if (region != null)
                {
                    _playerData.LookAt(region.Center);
                }
            }
            else if(notification.Data.ContainsKey("look_plain"))
            {
                var lookAt = new CellPosition(
                    int.Parse(notification.Data["look_plain"]),
                    int.Parse(notification.Data["look_x"]),
                    int.Parse(notification.Data["look_y"]));

                _playerData.LookAt(lookAt);
            }

        }
    }
}
