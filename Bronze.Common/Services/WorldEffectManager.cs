﻿using Bronze.Contracts;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace Bronze.Common.Services
{
    public class WorldEffectManager : IWorldEffectManager, ISubSimulator, IWorldStateService
    {
        private readonly IPlayerDataTracker _playerData;
        private readonly List<WorldEffect> _activeEffects;
        private readonly Random _random;


        public WorldEffectManager(IPlayerDataTracker playerData)
        {
            _playerData = playerData;
            _activeEffects = new List<WorldEffect>();
            _random = new Random();
        }

        public void AddDeathEffect(
            Army army, 
            IUnit unit, 
            Vector2 worldPosition)
        {
            if (army.Cell.Position.Plane == _playerData.CurrentPlane && _playerData.PlayerTribe.IsVisible(army.Cell))
            {
                var deathEffect = (army.TransportType?.IndividualAnimations ?? unit.IndividualAnimations)
                    .Where(a => a.Name == "death")
                    .FirstOrDefault();

                if (deathEffect != null)
                {
                    _activeEffects.Add(new WorldEffect
                    {
                        AnimationTime = (float)_random.NextDouble() * -0.1f,
                        Facing = army.Facing,
                        FrameCount = deathEffect.FrameCount,
                        Height = deathEffect.Height,
                        ImageKey = deathEffect.ImageKey,
                        IsFacingByRow = true,
                        Layer = deathEffect.Layer,
                        PlaneId = _playerData.CurrentPlane,
                        TimeToLive = deathEffect.LoopTime,
                        Width = deathEffect.Width,
                        WorldPosition = worldPosition,
                        Colorization = army.Owner.PrimaryColor
                    });
                }
            }
        }

        public void Clear()
        {
            _activeEffects.Clear();
        }

        public IEnumerable<WorldEffect> GetActiveEffects()
        {
            return _activeEffects;
        }

        public void Initialize(WorldState worldState)
        {
        }

        public void LoadCustomData(SerializedObject worldRoot)
        {
        }

        public void SaveCustomData(SerializedObject worldRoot)
        {
        }

        public void StepSimulation(double elapsedMonths)
        {
            for(var i = 0; i < _activeEffects.Count; i++)
            {
                _activeEffects[i].AnimationTime += (float)(elapsedMonths / Constants.MONTHS_PER_SECOND);
            }

            _activeEffects.RemoveAll(e => e.AnimationTime >= e.TimeToLive);
        }
    }
}
