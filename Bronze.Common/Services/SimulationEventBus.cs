﻿using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.Services
{
    public class SimulationEventBus : ISimulationEventBus
    {
        public event SimulationEventHandler OnSimulationEvent;

        public void SendEvent(ISimulationEvent evt)
        {
            OnSimulationEvent?.Invoke(evt);
        }
    }
}
