﻿using Bronze.Common.Data.World.CellDevelopments;
using Bronze.Common.Data.World.Units;
using Bronze.Common.InjectableServices;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.UI.Modals;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Bronze.Common.Services
{
    public class CreepBanditManager : ICreepBanditManager, ISubSimulator, IWorldStateService
    {
        private readonly IWorldManager _worldManager;
        private readonly IUnitHelper _unitHelper;
        private readonly IWarHelper _warHelper;
        private readonly IGamedataTracker _gamedataTracker;
        private readonly BronzeColor[] _tribeColors;
        private readonly IPlayerDataTracker _playerData;
        private readonly IGenericNotificationBuilder _genericNotificationBuilder;
        private readonly INotificationsSystem _notificationsSystem;
        private readonly IDialogManager _dialogManager;
        private readonly IBattleHelper _battleHelper;
        private readonly ILosTracker _losTracker;
        private Random _random;
        private readonly List<CreepCampDevelopment> _developments;
        private readonly Dictionary<string, CreepCampState> _campStates;

        public CreepBanditManager(
            IWorldManager worldManager,
            IUnitHelper unitHelper,
            IWarHelper warHelper,
            IGamedataTracker gamedataTracker,
            IPlayerDataTracker playerData,
            IGenericNotificationBuilder genericNotificationBuilder,
            INotificationsSystem notificationsSystem,
            IDialogManager dialogManager,
            IBattleHelper battleHelper,
            ILosTracker losTracker)
        {
            _worldManager = worldManager;
            _unitHelper = unitHelper;
            _warHelper = warHelper;
            _gamedataTracker = gamedataTracker;
            _playerData = playerData;
            _genericNotificationBuilder = genericNotificationBuilder;
            _notificationsSystem = notificationsSystem;
            _dialogManager = dialogManager;
            _battleHelper = battleHelper;
            _losTracker = losTracker;

            _developments = new List<CreepCampDevelopment>();
            _campStates = new Dictionary<string, CreepCampState>();
            _random = new Random();

            _tribeColors = Enum.GetValues(typeof(BronzeColor))
                .Cast<BronzeColor>()
                .Where(c => c != BronzeColor.None)
                .ToArray();
        }

        public void Clear()
        {
            _developments.Clear();
            _campStates.Clear();
        }

        public void Deregister(CreepCampDevelopment development)
        {
            if(_developments.Contains(development))
            {
                _developments.Remove(development);

                if (_campStates.ContainsKey(development.Id))
                {
                    _campStates.Remove(development.Id);
                }
            }
        }

        public void Initialize(WorldState worldState)
        {
            _random = new Random(worldState.Seed.GetHashCode());
        }
        
        public void Register(CreepCampDevelopment development)
        {
            if (!_developments.Contains(development))
            {
                _developments.Add(development);
                if (!_campStates.ContainsKey(development.Id))
                {
                    _campStates.Add(development.Id, new CreepCampState
                    {
                        DevelopmentId = development.Id,
                        SpawnTimer = development.BanditSpawnTimeMax * ((1.5 + _random.NextDouble()) - _worldManager.Hostility),
                        KnownToPlayer = false,
                        Army = null
                    });
                }
            }
        }

        public void SaveCustomData(SerializedObject worldRoot)
        {
            var serviceRoot = worldRoot.CreateChild("creep_bandit_manager");

            foreach(var campState in _campStates.Values)
            {
                var child = serviceRoot.CreateChild("camp_state");
                child.Set("id", campState.DevelopmentId);
                child.Set("army_id", campState.Army?.Id);
                child.Set("spawn_timer", campState.SpawnTimer);
                child.Set("known_to_player", campState.KnownToPlayer);
            }
        }

        public void LoadCustomData(SerializedObject worldRoot)
        {
            var serviceRoot = worldRoot.GetOptionalChild("creep_bandit_manager");

            var allArmies = _worldManager.Tribes
                .SelectMany(t => t.Armies)
                .ToArray();

            if (serviceRoot != null)
            {
                foreach (var child in serviceRoot.GetChildren("camp_state"))
                {
                    var developmentId = child.GetString("id");

                    var armyId = child.GetOptionalString("army_id");

                    Army army = null;
                    if (armyId != null)
                    {
                        army = allArmies.FirstOrDefault(a => a.Id == armyId);
                    }

                    _campStates.Add(developmentId,
                        new CreepCampState
                        {
                            DevelopmentId = developmentId,
                            Army = army,
                            KnownToPlayer = child.GetBool("known_to_player"),
                            SpawnTimer = child.GetDouble("spawn_timer")
                        });
                }
            }
        }
        
        public void StepSimulation(double elapsedMonths)
        {
            var stopwatch = Stopwatch.StartNew();

            for(var i = 0; i < _developments.Count; i++)
            {
                var development = _developments[i];

                if (!_campStates.ContainsKey(development.Id))
                {
                    _campStates.Add(development.Id, new CreepCampState
                    {
                        DevelopmentId = development.Id,
                        SpawnTimer = development.BanditSpawnTimeMax * ((1.5 + _random.NextDouble()) - _worldManager.Hostility),
                        KnownToPlayer = false,
                        Army = null
                    });
                }

                var state = _campStates[development.Id];
                
                if(!state.KnownToPlayer && _playerData.PlayerTribe.IsVisible(development.Cell))
                {
                    state.KnownToPlayer = true;

                    if (!_losTracker.GodVision)
                    {
                        if (development.Cell.Owner == _playerData.PlayerTribe)
                        {
                            _notificationsSystem.AddNotification(_genericNotificationBuilder.BuildSimpleNotification(
                                "ui_notification_creep_camp_domestic",
                                development.Name + " Discovered",
                                $"A {development.Name} has been discovered near {development.Cell.Region.Settlement.Name}.",
                                development.Name + " Discovered",
                                $"Scouts have discovered a {development.Name} near our settlement of {development.Cell.Region.Settlement.Name}. We need to either pacify or elimiate it before it becomes a threat.",
                                development.Cell.Position));
                        }
                        else
                        {
                            _notificationsSystem.AddNotification(_genericNotificationBuilder.BuildSimpleNotification(
                                "ui_notification_creep_camp_foreign",
                                development.Name + " Discovered",
                                $"Scouts have discovered a {development.Name}.",
                                development.Name + " Discovered",
                                $"Scouts have discovered a {development.Name} in {development.Cell.Region.Name}. It may pose a threat to us.",
                                development.Cell.Position));
                        }
                    }
                }

                if (state.Army != null && !state.Army.Units.Any())
                {
                    _unitHelper.RemoveArmy(state.Army);
                    state.Army = null;
                }

                if (!development.IsBeingPacified || development.PacificationPercent < 0)
                {
                    if (state.Army == null)
                    {
                        state.SpawnTimer -= elapsedMonths;

                        if (state.SpawnTimer <= 0)
                        {
                            var randomFactor = (
                                _random.NextDouble()
                                + (1 - _worldManager.Hostility))
                                 / 2;


                            state.SpawnTimer = randomFactor * (development.BanditSpawnTimeMax - development.BanditSpawnTimeMin) + development.BanditSpawnTimeMin;

                            var tribe = GetOrMakeBanditTribe(development.Name, development.BanditRace);
                            
                            var army = _unitHelper.CreateArmy(tribe, development.Cell);
                            state.Army = army;

                            var unitCount = _random.Next(development.BanditCountMin, development.BanditCountMax);

                            for(var j = 0; j < unitCount; j++)
                            {
                                army.Units.Add(development.BanditUnit.CreateUnit(development.BanditUnit.EquipmentSets.First()));
                            }
                        }
                    }
                    else if (state.Army.CurrentOrder.IsIdle)
                    {
                        var army = state.Army;
                        
                        if (army.CurrentOrder.IsIdle 
                            && _random.NextDouble() < _worldManager.Hostility)
                        {
                            var raidTarget = army.Cell.Neighbors
                                .Where(c => c.Development != null && c.Development != development)
                                .Where(c => army.CanPath(c))
                                .Where(c => c.Development.CanBeRaided)
                                .OrderBy(c => c.Position.DistanceSq(army.Cell.Position))
                                .FirstOrDefault();

                            if(raidTarget != null)
                            {
                                army.CurrentOrder = new ArmyRaidOrder(army, raidTarget, _warHelper);
                            }
                        }

                        if (army.CurrentOrder.IsIdle)
                        {
                            var destroyTarget = army.Cell.Neighbors
                                .Where(c => c.Development != null && c.Development != development && c.Development is CreepCampDevelopment)
                                .Where(c => army.CanPath(c))
                                .OrderBy(c => c.Position.DistanceSq(army.Cell.Position))
                                .FirstOrDefault();

                            if (destroyTarget != null)
                            {
                                army.CurrentOrder = new ArmyInteractOrder(army, destroyTarget, _battleHelper);
                            }
                        }

                        if (army.CurrentOrder.IsIdle
                            && army.Units.Min(u => u.SuppliesPercent) < 0.1
                            && development.CanPlaceOffshoot(army.Cell))
                        {
                            development.PlaceOffshoot(army.Cell);
                            _unitHelper.RemoveArmy(army);
                            state.Army = null;
                        }
                        
                        if(army.CurrentOrder.IsIdle)
                        {
                            var wanderCells = army.Cell.OrthoNeighbors
                                .Where(c => army.CanPath(c))
                                .ToArray();

                            if(wanderCells.Any())
                            {
                                army.CurrentOrder = new ArmyMoveOrder(army, _random.Choose(wanderCells));
                            }
                        }
                    }
                }
                else
                {
                    if (state.Army != null)
                    {
                        _unitHelper.RemoveArmy(state.Army);
                        state.Army = null;
                    }
                }
            }
        }

        private Tribe GetOrMakeBanditTribe(string developmentName, Race banditRace)
        {
            var banditTribe = _worldManager.Tribes
                    .Where(t => t.Name == developmentName + " Bandits")
                    .FirstOrDefault();

            if (banditTribe == null)
            {
                banditTribe = new Tribe
                {
                    Race = banditRace,
                    PrimaryColor = _random.Choose(_tribeColors),
                    SecondaryColor = _random.Choose(_tribeColors),
                    Name = developmentName + " Bandits",
                    IsBandits = true
                };
                _worldManager.Tribes.Add(banditTribe);
            }

            return banditTribe;
        }

        public void HandleArmyAttack(CreepCampDevelopment development, Army attackingArmy)
        {
            if(attackingArmy.Owner == _playerData.PlayerTribe)
            {
                _dialogManager.PushModal<ConfirmModal, ConfirmModalContext>(
                    new ConfirmModalContext
                    {
                        Title = $"Attack {development.Name}?",
                        Body = $"We can attack the {development.Name} and destroy it, they will defend themselves, however.\nShall we attack?",
                        YesAction = () => ResolveArmyAttack(development, attackingArmy),
                        NoAction = () => { }
                    });
            }
            else
            {
                ResolveArmyAttack(development, attackingArmy);
            }
        }

        public void HandleArmyAttack(CreepAttackableDevelopment development, Army attackingArmy)
        {
            var tribe = GetOrMakeBanditTribe(development.Name, development.DefenderRace);

            var defenderArmy = _unitHelper.CreateArmy(tribe, development.Cell);
            
            for (var j = 0; j < development.DefenderCount; j++)
            {
                var defenderUnit = development.DefenderUnit.CreateUnit(development.DefenderUnit.EquipmentSets.First());
                defenderUnit.Morale = development.DefenderStrength;
                defenderUnit.Health = development.DefenderStrength * defenderUnit.MaxHealth;
                defenderArmy.Units.Add(defenderUnit);
            }

            var startingStrength = defenderArmy.Units.Sum(s => s.Equipment.StrengthEstimate);

            _battleHelper.RunBattle(attackingArmy);

            var endingStrength = defenderArmy.Units.Sum(s => s.StrengthEstimate);

            if (startingStrength > 0)
            {
                development.DefenderStrength = endingStrength / startingStrength;
            }
            else
            {
                development.DefenderStrength = 0;
            }

            if (!defenderArmy.Units.Any()
                || attackingArmy.StrengthEstimate >= defenderArmy.StrengthEstimate * 2)
            {
                development.Cell.Development = null;
            }
            else
            {
                _battleHelper.SetRouted(attackingArmy.Yield(), 0.5);
            }

            _unitHelper.RemoveArmy(defenderArmy);
        }

        private void ResolveArmyAttack(CreepCampDevelopment development, Army attackingArmy)
        {
            var tribe = GetOrMakeBanditTribe(development.Name, development.BanditRace);

            var defenderArmy = _unitHelper.CreateArmy(tribe, development.Cell);

            var unitCount = _random.Next(development.BanditCountMin, development.BanditCountMax);

            for (var j = 0; j < unitCount; j++)
            {
                var defenderUnit = development.BanditUnit.CreateUnit(development.BanditUnit.EquipmentSets.First());
                defenderUnit.Morale = development.DefenderStrength;
                defenderUnit.Health = development.DefenderStrength * defenderUnit.MaxHealth;
                defenderArmy.Units.Add(defenderUnit);
            }

            var startingStrength = defenderArmy.Units.Sum(s => s.Equipment.StrengthEstimate);

            _battleHelper.RunBattle(attackingArmy);

            var endingStrength = defenderArmy.Units.Sum(s => s.StrengthEstimate);

            if(startingStrength > 0)
            {
                development.DefenderStrength = endingStrength / startingStrength;
            }
            else
            {
                development.DefenderStrength = 0;
            }

            if (!defenderArmy.Units.Any()
                || attackingArmy.StrengthEstimate >= defenderArmy.StrengthEstimate * 2)
            {
                development.Cell.Development = null;
            }
            else
            {
                _battleHelper.SetRouted(attackingArmy.Yield(), 0.5);
            }

            _unitHelper.RemoveArmy(defenderArmy);
        }

        private class CreepCampState
        {
            public string DevelopmentId { get; set; }
            public bool KnownToPlayer { get; set; }
            public double SpawnTimer { get; set; }
            public Army Army { get; set; }
        }
    }
}
