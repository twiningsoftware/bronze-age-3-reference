﻿using Bronze.Common.UI.Modals;
using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.Services
{
    public class SiegedNotificationHandler : INotificationHandler
    {
        private readonly IDialogManager _dialogManager;

        public SiegedNotificationHandler(IDialogManager dialogManager)
        {
            _dialogManager = dialogManager;
        }

        public string Name => typeof(SiegedNotificationHandler).Name;

        public void Handle(Notification notification)
        {
            _dialogManager.PushModal<SiegeInfoModal>();
        }

        public void HandleIgnored(Notification notification)
        {
        }
    }
}
