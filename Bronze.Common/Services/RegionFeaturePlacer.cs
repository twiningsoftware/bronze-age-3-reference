﻿using System.Collections.Generic;
using System.Linq;
using Bronze.Common.Generation;
using Bronze.Contracts;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Generation;
using Bronze.Contracts.Data.World;

namespace Bronze.Common.Services
{
    public class RegionFeaturePlacer : IRegionFeaturePlacer
    {
        public void FillOutRegion(
            GenerationContext context, 
            Plane plane,
            RegionFeature regionFeature,
            IEnumerable<Region> regions)
        {
            context.PlacedRegionFeatures[plane].Add(new PlacedRegionFeature(regionFeature, regions));

            FillOutFeature(
                context,
                plane,
                regionFeature.Biome,
                regionFeature.CellFeature,
                null,
                Enumerable.Empty<RegionMirror>(),
                regionFeature.InclusionFeatures,
                regionFeature.BorderFeatures,
                regionFeature.SubFeatures,
                regions);
        }

        public void FillOutFeature(
            GenerationContext context,
            Plane plane,
            Biome biome,
            ICellFeature cellFeature,
            Plane mirrorsPlane,
            IEnumerable<RegionMirror> regionMirrors,
            IEnumerable<InclusionFeature> inclusions,
            IEnumerable<BorderFeature> borderFeatures,
            IEnumerable<SubFeature> subFeatures,
            IEnumerable<Region> regions)
        {
            FillOutFeature(
                context,
                plane,
                regions,
                biome,
                cellFeature,
                mirrorsPlane,
                regionMirrors,
                subFeatures,
                borderFeatures,
                inclusions);
        }

        public void PlaceInclusions(
            GenerationContext context, 
            Plane plane, 
            IEnumerable<InclusionFeature> inclusions, 
            IEnumerable<Cell> cells)
        {
            PlaceInclusions(
                context,
                cells.Distinct().ToArray(),
                inclusions);
        }

        private void FillOutFeature(
            GenerationContext context,
            Plane plane,
            IEnumerable<Region> regions,
            Biome biome,
            ICellFeature cellFeature,
            Plane mirrorsPlane,
            IEnumerable<RegionMirror> regionMirrors,
            IEnumerable<SubFeature> subFeatures,
            IEnumerable<BorderFeature> borderFeatures,
            IEnumerable<InclusionFeature> inclusionFeatures)
        {
            foreach (var region in regions)
            {
                region.Biome = biome;

                foreach (var cell in region.Cells)
                {
                    cell.Biome = biome;
                    cell.Feature = cellFeature;
                    cell.Development = null;
                }
            }

            if (mirrorsPlane != null)
            {
                var mirroredRegions = new List<Region>();
                foreach (var mirror in regionMirrors)
                {
                    var toMirror = context.PlacedRegionFeatures[mirrorsPlane]
                        .Where(prf => prf.RegionFeature.Id == mirror.MirrorsRegion)
                        .ToArray();

                    foreach (var placedRegionFeature in toMirror)
                    {
                        if (!mirroredRegions.Intersect(placedRegionFeature.ContainedRegions).Any())
                        {
                            var mirroringRegions = placedRegionFeature.ContainedRegions
                                .Select(r => r.Cells.First())
                                .Select(x => regions
                                    .Where(r => r.Cells.Any(c => c.Position.X == x.Position.X && c.Position.Y == x.Position.Y))
                                    .FirstOrDefault())
                                .Where(r => r != null)
                                .Distinct()
                                .ToArray();

                            mirroredRegions.AddRange(mirroringRegions);

                            FillOutFeature(
                                context,
                                plane,
                                mirroringRegions,
                                mirror.Biome,
                                mirror.CellFeature,
                                mirrorsPlane,
                                mirror.RegionMirrors,
                                mirror.SubFeatures,
                                mirror.BorderFeatures,
                                mirror.InclusionFeatures);
                        }
                    }
                }

                context.MirroredRegions.AddRange(mirroredRegions);

                regions = regions
                    .Except(mirroredRegions)
                    .ToArray();
            }

            //Inclusions first, so they can be overriden by subfeatures as appropriate

            if (inclusionFeatures.Any())
            {
                PlaceInclusions(
                    context,
                    regions.SelectMany(r => r.Cells).ToArray(),
                    inclusionFeatures);
            }

            if (subFeatures.Any())
            {
                PlaceSubfeatures(
                    context,
                    plane,
                    subFeatures,
                    regions);
            }

            if (borderFeatures.Any())
            {
                var outsideBorder = regions
                    .SelectMany(r => r.Cells)
                    .Where(c => c.Neighbors.Any(n => !regions.Contains(n.Region)))
                    .ToArray();

                var chosenBorderFeatures = GenUtils.DetermineFeaturesToPlace(
                    context.Random,
                    borderFeatures,
                    outsideBorder.Length,
                    f => f.CellsPerOccurance,
                    f => f.MinimumOccurances);

                context.BorderFeaturesToPlace.Add(new BorderFeaturePlacement(
                    outsideBorder,
                    chosenBorderFeatures));
            }
        }

        private void PlaceInclusions(
            GenerationContext context,
            Cell[] cells,
            IEnumerable<InclusionFeature> inclusionsToChooseFrom)
        {
            var inclusionsToPlace = GenUtils.DetermineFeaturesToPlace(
                context.Random,
                inclusionsToChooseFrom.ToArray(),
                cells.Length,
                i => i.CellsPerOccurance,
                i => i.MinimumOccurances);

            var cellHasInclusionLookup = cells.ToDictionary(c => c, c => false);
            foreach (var inclusion in inclusionsToPlace)
            {
                var potenials = cells
                    .Where(c => !cellHasInclusionLookup[c])
                    .ToArray();

                if (potenials.Any())
                {
                    var cell = context.Random.Choose(potenials);

                    cellHasInclusionLookup[cell] = true;

                    if (inclusion.Biome != null)
                    {
                        cell.Biome = inclusion.Biome;
                    }

                    cell.Feature = inclusion.CellFeature;
                    
                    if(inclusion.CellDevelopment != null && inclusion.CellDevelopment.CanBePlaced(null, cell))
                    {
                        inclusion.CellDevelopment.Place(null, cell, true);
                    }
                    else
                    {
                        cell.Development = null;
                    }

                    var riverSpawner = inclusion.RiverSpawners
                        .Where(s => context.Random.NextDouble() < s.SpawnChance)
                        .FirstOrDefault();

                    if (riverSpawner != null 
                        && !context.RiverSpawners.ContainsKey(cell) 
                        && !context.MidpathSpawners.ContainsKey(cell))
                    {
                        context.RiverSpawners.Add(cell, riverSpawner);
                    }

                    var midpathSpawner = inclusion.MidpathSpawners
                        .Where(s => context.Random.NextDouble() < s.SpawnChance)
                        .FirstOrDefault();

                    if (midpathSpawner != null 
                        && !context.RiverSpawners.ContainsKey(cell) 
                        && !context.MidpathSpawners.ContainsKey(cell))
                    {
                        context.MidpathSpawners.Add(cell, midpathSpawner);
                    }
                }
            }
        }

        private void PlaceSubfeatures(
            GenerationContext context,
            Plane plane,
            IEnumerable<SubFeature> featuresToChooseFrom,
            IEnumerable<Region> regions)
        {
            var featuresToPlace = GenUtils.DetermineFeaturesToPlace(
                context.Random,
                featuresToChooseFrom.ToArray(),
                regions.Count(),
                f => f.RegionsPerOccurance,
                f => f.MinimumOccurances);

            featuresToPlace = featuresToPlace
                .OrderBy(rf => rf.Priority)
                .ToArray();

            var claimedRegions = regions.ToDictionary(c => c, c => false);
            foreach (var subfeature in featuresToPlace)
            {
                TryToPlaceFeature(
                    context,
                    plane,
                    subfeature,
                    regions,
                    claimedRegions);
            }
        }

        private void TryToPlaceFeature(
            GenerationContext context,
            Plane plane,
            SubFeature subFeature,
            IEnumerable<Region> regions,
            Dictionary<Region, bool> claimedRegions)
        {
            var invalidRegions = claimedRegions
                .Where(kvp => kvp.Value)
                .Select(kvp => kvp.Key)
                // add cells on world borders
                .Concat(regions.Where(r => r.Cells.Any(c => c.Neighbors == null)))
                // add the border of regions outside the area
                .Concat(regions.SelectMany(c => c.Neighbors).Except(regions))
                .Distinct()
                .ToArray();

            // Flood fill out from claimed regions to account for a margin around the subfeature
            for (var i = 0; i < subFeature.Margin; i++)
            {
                invalidRegions = invalidRegions
                    .SelectMany(r => r.Neighbors)
                    .Concat(invalidRegions)
                    .ToArray();
            }

            var regionsInPlacementOrder = regions
                .Except(invalidRegions)
                .ToList();
            context.Random.Shuffle(regionsInPlacementOrder);

            var targetSize = context.Random.Next(subFeature.MinSize, subFeature.MaxSize);

            foreach (var featureStartRegion in regionsInPlacementOrder)
            {
                var regionInFeatureLookup = regions
                    .ToSafeDictionary(r => r, c => false);

                var regionsInFeature = new List<Region>();
                regionsInFeature.Add(featureStartRegion);
                regionInFeatureLookup[featureStartRegion] = true;
                var growthBlocked = false;

                while (regionsInFeature.Count < targetSize && !growthBlocked)
                {
                    Region nextRegion = null;

                    nextRegion = regionsInPlacementOrder
                        .Where(c => !regionInFeatureLookup[c])
                        .Where(c => c.Neighbors.Where(n => regionInFeatureLookup[n]).Any())
                        .OrderByDescending(c => c.Neighbors.Where(n => regionInFeatureLookup[n]).Count())
                        .FirstOrDefault();

                    if (nextRegion != null)
                    {
                        regionInFeatureLookup[nextRegion] = true;
                        regionsInFeature.Add(nextRegion);
                        growthBlocked = false;
                    }
                    else
                    {
                        growthBlocked = true;
                    }
                }

                if (regionsInFeature.Count >= subFeature.MinSize)
                {
                    foreach (var region in regionsInFeature)
                    {
                        claimedRegions[region] = true;
                    }
                    
                    FillOutRegion(
                        context,
                        plane,
                        subFeature.RegionFeature,
                        regionsInFeature);

                    // bail out so we don't try to keep placing the feature
                    return;
                }
            }
        }
    }
}
