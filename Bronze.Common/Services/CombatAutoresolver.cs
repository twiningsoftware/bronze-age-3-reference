﻿using Bronze.Common.Data.Combat.Autoresolve;
using Bronze.Contracts;
using Bronze.Contracts.Data.Diplomacy;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.Services
{
    public class CombatAutoresolver : ICombatAutoresolver
    {
        private static readonly double TIME_STEP = 0.25;
        private readonly ICombatHelper _combatHelper;
        private readonly IWorldManager _worldManager;
        
        private Random _random;

        public CombatAutoresolver(
            ICombatHelper combatHelper,
            IWorldManager worldManager)
        {
            _combatHelper = combatHelper;
            _worldManager = worldManager;
            _random = new Random();
        }

        public PitchedBattleResult AutoResolveCombat(
            Cell location,
            IEnumerable<Army> attackers, 
            IEnumerable<Army> defenders,
            int? fixedSeed)
        {
            var battlefieldWidth = Math.Max(1, Math.Max(attackers.Sum(a => a.Units.Count) / 2, defenders.Sum(a => a.Units.Count) / 2));


            if(fixedSeed != null)
            {
                _random = new Random(fixedSeed.Value);
            }

            var startingStrength = attackers.Concat(defenders).Sum(a => a.StrengthEstimate);
            var attackerBeforeStrength = attackers.Sum(a => a.StrengthEstimate);
            var defenderBeforeStrength = defenders.Sum(a => a.StrengthEstimate);
            
            foreach (var army in attackers.Concat(defenders))
            {
                if(army.General != null)
                {
                    foreach (var unit in army.Units)
                    {
                        unit.Morale += army.General.GetBonusBattleStartMorale();
                    }
                }
            }

            var attackerRanges = attackers.SelectMany(a => a.Units.SelectMany(u => u.MeleeAttacks.Select(m => m.Range).Concat(u.RangedAttacks.Select(r => r.Range)))).ToArray();
            var defenderRanges = attackers.SelectMany(a => a.Units.SelectMany(u => u.MeleeAttacks.Select(m => m.Range).Concat(u.RangedAttacks.Select(r => r.Range)))).ToArray();

            var attackerMaxRange = Constants.TILE_SIZE_PIXELS * 10;
            if(attackerRanges.Any())
            {
                attackerMaxRange = Math.Max(attackerMaxRange, attackerRanges.Max());
            }

            var defenderMaxRange = Constants.TILE_SIZE_PIXELS * 10;
            if (defenderRanges.Any())
            {
                defenderMaxRange = Math.Max(defenderMaxRange, defenderRanges.Max());
            }

            var unitStates = attackers.SelectMany(a => a.Units.Select(u => new UnitState(u, a, -attackerMaxRange, true)))
                .Concat(defenders.SelectMany(a => a.Units.Select(u => new UnitState(u, a, defenderMaxRange, false))))
                .ToList();

            _random.Shuffle(unitStates);

            var initialUnitHealth = unitStates
                .ToDictionary(s => s.Unit, s => s.Unit.HealthPercent);

            var deadUnits = new List<UnitState>();
            var routedUnits = new List<UnitState>();

            var detailedResults = new List<UnitStateSnapshot>();
            var routTimer = Constants.COMBAT_ROUT_TIME_SECONDS;

            for (var second = 0.0; second < Constants.COMBAT_LIMIT_BASE_SECONDS; second += TIME_STEP)
            {
                var newlyDeadUnits = unitStates
                    .Where(u => u.IsDead)
                    .Except(deadUnits)
                    .ToArray();

                foreach(var unitState in newlyDeadUnits)
                {
                    deadUnits.Add(unitState);

                    var allies = unitStates
                        .Where(u => !u.IsDead && !u.IsRouting && u.IsAttacker == unitState.IsAttacker)
                        .ToArray();

                    foreach(var ally in allies)
                    {
                        ally.ApplyMoraleShock(0.1);
                    }
                }

                var newlyRoutedUnits = unitStates
                    .Where(u => u.IsRouting && !u.IsDead)
                    .Except(routedUnits)
                    .ToArray();
                foreach (var unitState in newlyRoutedUnits)
                {
                    routedUnits.Add(unitState);

                    var allies = unitStates
                        .Where(u => !u.IsDead && !u.IsRouting && u.IsAttacker == unitState.IsAttacker)
                        .ToArray();

                    foreach (var ally in allies)
                    {
                        ally.ApplyMoraleShock(0.1);
                    }
                }

                foreach (var unit in unitStates)
                {
                    detailedResults.Add(new UnitStateSnapshot
                    {
                        Unit = unit.Unit,
                        HealthPercent = unit.Individuals.Sum(i => i.Health) / unit.Unit.MaxHealth,
                        MoralePercent = Util.Clamp(unit.Unit.Morale, 0, 1),
                        Position = unit.Position,
                        Time = second,
                        MadeMeleeAttack = unit.MadeMeleeAttack,
                        MadeRangedAttack = unit.MadeRangedAttack,
                        WasAttacked = unit.WasAttacked,
                        IsAttacker = unit.IsAttacker
                    });
                    unit.MadeMeleeAttack = false;
                    unit.MadeRangedAttack = false;
                    unit.WasAttacked = false;
                }
                
                var allAttackersDead = !unitStates.Where(u => u.IsAttacker && !u.IsDead).Any();
                var allAttackersDeadOrRouting = !unitStates.Where(u => u.IsAttacker && !u.IsDead && !u.IsRouting).Any();
                var allDefendersDead = !unitStates.Where(u => !u.IsAttacker && !u.IsDead).Any();
                var allDefendersDeadOrRouting = !unitStates.Where(u => !u.IsAttacker && !u.IsDead && !u.IsRouting).Any();

                if(allAttackersDead || allDefendersDead)
                {
                    return FinalizeBattle(
                        location,
                        attackers,
                        defenders,
                        unitStates,
                        initialUnitHealth,
                        startingStrength,
                        false,
                        attackerBeforeStrength,
                        defenderBeforeStrength,
                        detailedResults);
                }
                else if (allAttackersDeadOrRouting || allDefendersDeadOrRouting)
                {
                    routTimer -= TIME_STEP;

                    if(routTimer <= 0)
                    {
                        return FinalizeBattle(
                            location,
                            attackers,
                            defenders,
                            unitStates,
                            initialUnitHealth,
                            startingStrength,
                            false,
                            attackerBeforeStrength,
                            defenderBeforeStrength,
                            detailedResults);
                    }
                }
                
                var attackerFrontLine = unitStates
                    .Where(u => u.IsAttacker && !u.IsDead)
                    .Max(u => u.Position);

                var defenderFrontLine = unitStates
                    .Where(u => !u.IsAttacker && !u.IsDead)
                    .Min(u => u.Position);

                if(attackerFrontLine > defenderFrontLine)
                {
                    foreach(var unit in unitStates.Where(u => u.IsAttacker && !u.IsDead))
                    {
                        unit.Position = Math.Min(unit.Position, defenderFrontLine);
                    }

                    attackerFrontLine = defenderFrontLine;
                }

                var unitsToAct = unitStates
                    .Where(us => !us.IsDead)
                    .OrderBy(us => Math.Abs(us.Position - (us.IsAttacker ? defenderFrontLine : attackerFrontLine)))
                    .ToArray();

                var attackerLeadUnits = unitsToAct
                    .Where(us => us.IsAttacker)
                    .Take(battlefieldWidth)
                    .ToArray();

                var defenderLeadUnits = unitsToAct
                    .Where(us => !us.IsAttacker)
                    .Take(battlefieldWidth)
                    .ToArray();
                
                foreach (var unit in unitsToAct.Where(us => !us.IsDead))
                {
                    var enemies = unitStates.Where(u => u.IsAttacker != unit.IsAttacker && !u.IsDead).ToArray();
                    var enemyFrontLine = unit.IsAttacker ? defenderFrontLine : attackerFrontLine;
                    var enemyLeadUnits = unit.IsAttacker ? defenderLeadUnits : attackerLeadUnits;
                    var ownLeadUnits = unit.IsAttacker ? attackerLeadUnits : defenderLeadUnits;

                    unit.StepSimulation(
                        unitStates,
                        location, 
                        _random,
                        _combatHelper,
                        TIME_STEP, 
                        enemyFrontLine, 
                        enemies,
                        enemyLeadUnits,
                        ownLeadUnits.Contains(unit));
                }
            }

            return FinalizeBattle(
                location, 
                attackers, 
                defenders, 
                unitStates, 
                initialUnitHealth, 
                startingStrength, 
                true,
                attackerBeforeStrength,
                defenderBeforeStrength,
                detailedResults);
        }
        
        private PitchedBattleResult FinalizeBattle(
            Cell location,
            IEnumerable<Army> attackers,
            IEnumerable<Army> defenders,
            List<UnitState> unitStates, 
            Dictionary<IUnit, double> initialUnitHealth,
            double startingStrength,
            bool timedout,
            double attackerBeforeStrength,
            double defenderBeforeStrength,
            List<UnitStateSnapshot> detailedResults)
        {
            var attackingUnits = unitStates
                .Where(us => us.IsAttacker)
                .Select(us => us.Unit)
                .ToArray();
            var defendingUnits = unitStates
                .Where(us => !us.IsAttacker)
                .Select(us => us.Unit)
                .ToArray();

            var allArmies = attackers.Concat(defenders).ToArray();
            
            var attackerScore = 0.0;
            var defenderScore = 0.0;
            
            var attackerSurvived = false;
            var defenderSurvived = false;

            foreach (var unitState in unitStates)
            {
                // Routing units can be trampled, cut down, or scatter after the battle.
                if(unitState.Unit.Morale <= 0)
                {
                    foreach(var individual in unitState.Individuals.ToArray())
                    {
                        if (_random.Next(0, 10) > unitState.Unit.ResolveSkill)
                        {
                            individual.Health = 0;
                        }
                    }
                }
                
                unitState.Unit.Health = unitState.Individuals.Sum(i => Math.Max(0, i.Health));
                unitState.Unit.Morale = Util.Clamp(unitState.Unit.Morale, 0, 1);
                
                var deltaHealthPercent = initialUnitHealth[unitState.Unit] - unitState.Unit.HealthPercent;

                if(unitState.IsAttacker)
                {
                    attackerSurvived = attackerSurvived || (unitState.Unit.Health > 0);
                    defenderScore += unitState.Unit.Equipment.StrengthEstimate * deltaHealthPercent;
                }
                else
                {
                    defenderSurvived = defenderSurvived || (unitState.Unit.Health > 0);
                    attackerScore += unitState.Unit.Equipment.StrengthEstimate * deltaHealthPercent;
                }
            }

            var attackerRemains = attackingUnits.Any(u => u.Morale > 0 && u.Health > 0);
            var defenderRemains = defendingUnits.Any(u => u.Morale > 0 && u.Health > 0);

            var destroyedArmies = allArmies
                .Where(a => !a.Units.Any(u => u.Health > 0))
                .ToArray();

            var routedArmies = allArmies
                .Except(destroyedArmies)
                .Where(a => a.Units.All(u => u.Morale <= 0))
                .ToArray();

            var remainingArmies = allArmies
                .Except(destroyedArmies)
                .Except(routedArmies)
                .ToArray();

            var allUnits = attackingUnits
                .Concat(defendingUnits)
                .ToArray();

            var survivingUnits = allUnits
                .Where(u => u.Health > 0)
                .ToArray();

            var deadUnits = allUnits
                .Where(u => u.Health <= 0)
                .ToArray();

            return new PitchedBattleResult
            {
                TimedOut = timedout,
                AttackerScore = attackerScore,
                DefenderScore = defenderScore,
                AttackerWon = attackerRemains && !defenderRemains,
                DefenderWon = defenderRemains && !attackerRemains,
                DestroyedArmies = destroyedArmies,
                RemainingArmies = remainingArmies,
                RoutingArmies = routedArmies,
                Attackers = attackers,
                Defenders = defenders,
                TotalStrength = startingStrength,
                AttackerLeaders = attackers.Select(a => a.General).Where(g => g != null).ToList(),
                DefenderLeaders = defenders.Select(a => a.General).Where(g => g != null).ToList(),
                LostAttackers = unitStates
                    .Where(us => us.IsAttacker && us.IsDead)
                    .Select(us => us.Unit)
                    .ToArray(),
                SurvivingAttackers = unitStates
                    .Where(us => us.IsAttacker && !us.IsDead)
                    .Select(us => us.Unit)
                    .ToArray(),
                LostDefenders = unitStates
                    .Where(us => !us.IsAttacker && us.IsDead)
                    .Select(us => us.Unit)
                    .ToArray(),
                SurvivingDefenders = unitStates
                    .Where(us => !us.IsAttacker && !us.IsDead)
                    .Select(us => us.Unit)
                    .ToArray(),
                AttackerBeforeStrength = attackerBeforeStrength,
                AttackerAfterStrength = unitStates
                    .Where(us => us.IsAttacker && !us.IsDead)
                    .Sum(us => us.Unit.StrengthEstimate),
                DefenderBeforeStrength = defenderBeforeStrength,
                DefenderAfterStrength = unitStates
                    .Where(us => !us.IsAttacker && !us.IsDead)
                    .Sum(us => us.Unit.StrengthEstimate),
                Location = location,
                DetailedResults = detailedResults
            };
        }
    }
}
