﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Bronze.Common.Data.World.CellDevelopments;
using Bronze.Common.Data.World.Structures;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Priority_Queue;

namespace Bronze.Common.Services
{
    public class GraphBasedSettlementLogiCalculator : ISettlementLogiCalculator
    {
        private readonly IGamedataTracker _gamedataTracker;
        private readonly ILogger _logger;
        private readonly IPerformanceTracker _performanceTracker;

        public GraphBasedSettlementLogiCalculator(
            IGamedataTracker gamedataTracker,
            ILogger logger,
            IPerformanceTracker performanceTracker)
        {
            _gamedataTracker = gamedataTracker;
            _logger = logger;
            _performanceTracker = performanceTracker;
        }

        public IEnumerable<DeliveryRoute> CalculateDeliveryRoutes(CalculationResults current)
        {
            var inputItems = current.ActiveEconomicActors
                .ToDictionary(ea => ea, ea => ea.NeededInputs.Select(ir => ir.Item).ToList());
            
            var outputItems = current.ActiveEconomicActors
                .ToDictionary(ea => ea, ea => ea.ExpectedOutputs.Select(ir => ir.Item).ToList());

            foreach (var actor in current.ActiveEconomicActors)
            {
                if (actor is IItemStorageProvider itemStorageProvider)
                {
                    inputItems[actor].AddRange(_gamedataTracker.Items.Where(i => itemStorageProvider.CanStore(i)));
                    outputItems[actor].AddRange(_gamedataTracker.Items.Where(i => itemStorageProvider.CanStore(i)));
                }

                if(actor.UnderConstruction)
                {
                    inputItems[actor].AddRange(actor.ConstructionCost.Select(iq => iq.Item));
                }

                if (actor.IsUpgrading)
                {
                    inputItems[actor].AddRange(actor.UpgradeCost.Select(iq => iq.Item));
                }
            }

            var deliveryRoutes = new List<DeliveryRoute>();

            foreach (var actor in current.ActiveEconomicActors.Where(ea => outputItems[ea].Any()))
            {
                var totalDeliveryArea = new List<Tile>();

                foreach(var hauler in actor.LogiSourceHaulers)
                {
                    var cameFrom = FloodFillOut(actor.CalculateLogiDoors(hauler.MovementTypes), hauler);

                    totalDeliveryArea.AddRange(cameFrom.Keys);

                    var potentialDestinations = cameFrom.Keys
                        .SelectMany(t => t.OrthoNeighbors.Where(n => n.Structure != null))
                        .Select(t => t.Structure)
                        .Where(s => s != null && s != actor && inputItems.ContainsKey(s))
                        .Distinct()
                        .OfType<IEconomicActor>()
                        .ToList();

                    if (cameFrom.Keys.Any(t => t.IsBorderTile) || actor.IsCellLevel)
                    {
                        potentialDestinations.AddRange(
                            current.ActiveEconomicActors
                            .Where(s => s.IsCellLevel));
                    }

                    foreach(var destination in potentialDestinations)
                    {
                        if (outputItems[actor].Intersect(inputItems[destination]).Any())
                        {
                            var actualPath = BuildActualPath(actor, destination, cameFrom, hauler);

                            if (actualPath != null)
                            {
                                deliveryRoutes.Add(new DeliveryRoute(
                                    actor,
                                    destination,
                                    hauler,
                                    actualPath,
                                    actor is ICellDevelopment || destination is ICellDevelopment));
                            }
                        }
                    }
                }

                current.DeliveryAreas.Add(actor, totalDeliveryArea.Distinct().ToArray());
            }

            return deliveryRoutes;
        }
        
        public Dictionary<Tile, Tile> FloodFillOut(Tile[] logiDoors, HaulerInfo hauler)
        {
            var open = new SimplePriorityQueue<Tile, double>();
            var distTo = new Dictionary<Tile, double>();
            var closed = new HashSet<Tile>();
            var cameFrom = new Dictionary<Tile, Tile>();

            foreach (var tile in logiDoors)
            {
                open.Enqueue(tile, 0);
                distTo.Add(tile, 0);
                cameFrom.Add(tile, null);
            }

            while(open.Any())
            {
                var current = open.Dequeue();
                closed.Add(current);
                
                foreach(var neighbor in current.OrthoNeighbors)
                {
                    if(hauler.MovementTypes.Any(m => neighbor.Traits.ContainsAll(m.RequiredTraits))
                        && !closed.Contains(neighbor))
                    {
                        var dist = distTo[current] + neighbor.LogiDistance;

                        if (dist <= hauler.HaulDistance)
                        {
                            if (open.Contains(neighbor))
                            {
                                open.UpdatePriority(neighbor, dist);
                                distTo[neighbor] = dist;
                                cameFrom[neighbor] = current;
                            }
                            else
                            {
                                open.Enqueue(neighbor, dist);
                                distTo.Add(neighbor, dist);
                                cameFrom.Add(neighbor, current);
                            }
                        }
                    }
                }
            }

            var result = new Dictionary<Tile, Tile>();
            foreach(var tile in closed)
            {
                result.Add(tile, cameFrom[tile]);
            }

            return result;
        }

        private Tile[] BuildActualPath(
            IEconomicActor source,
            IEconomicActor destination, 
            Dictionary<Tile, Tile> cameFrom,
            HaulerInfo hauler)
        {
            if(source.IsCellLevel && destination.IsCellLevel)
            {
                return new Tile[0];
            }

            var start = destination.CalculateLogiDoors(hauler.MovementTypes)
                .Select(t => Tuple.Create(t, t.OrthoNeighbors.FirstOrDefault(n => cameFrom.ContainsKey(n))))
                .Where(t => t.Item2 != null)
                .FirstOrDefault();
            
            if(start != null)
            {
                var path = new List<Tile>();
                path.Add(start.Item1);
                var current = start.Item2;

                while(current != null)
                {
                    path.Add(current);
                    current = cameFrom[current];
                }

                path.Reverse();

                return path.ToArray();
            }

            return null;
        }

        public IEnumerable<IEconomicActor> FloodFillToActors(
            Tile[] from, 
            Trait[] reqiredTraits, 
            double maximumDistance)
        {
            var open = new SimplePriorityQueue<Tile, double>();
            var distTo = new Dictionary<Tile, double>();
            var closed = new HashSet<Tile>();
            var cameFrom = new Dictionary<Tile, Tile>();

            foreach (var tile in from.Where(t => t.Traits.ContainsAll(reqiredTraits)))
            {
                open.Enqueue(tile, 0);
                distTo.Add(tile, 0);
                cameFrom.Add(tile, null);
            }

            while (open.Any())
            {
                var current = open.Dequeue();
                closed.Add(current);

                foreach (var neighbor in current.OrthoNeighbors)
                {
                    if (neighbor.Traits.ContainsAll(reqiredTraits)
                        && !closed.Contains(neighbor))
                    {
                        var dist = distTo[current] + neighbor.LogiDistance;

                        if (dist <= maximumDistance)
                        {
                            if (open.Contains(neighbor))
                            {
                                open.UpdatePriority(neighbor, dist);
                                distTo[neighbor] = dist;
                                cameFrom[neighbor] = current;
                            }
                            else
                            {
                                open.Enqueue(neighbor, dist);
                                distTo.Add(neighbor, dist);
                                cameFrom.Add(neighbor, current);
                            }
                        }
                    }
                }
            }

            return closed.SelectMany(c => c.OrthoNeighbors)
                .Select(t => t.Structure)
                .Where(s => s != null)
                .ToArray();
        }
    }
}
