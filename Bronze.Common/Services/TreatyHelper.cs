﻿using Bronze.Contracts.Data.Diplomacy;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.Services
{
    public class TreatyHelper : ITreatyHelper, IWorldStateService, ISubSimulator
    {
        private readonly IWorldManager _worldManager;
        private readonly IPlayerDataTracker _playerData;
        private readonly INotificationsSystem _notificationsSystem;
        private readonly IInjectionProvider _injectionProvider;

        private readonly List<ITreaty> _treaties;
        private readonly Dictionary<Tribe, List<ITreaty>> _treatiesByTribe;

        public TreatyHelper(
            IWorldManager worldManager,
            IPlayerDataTracker playerData,
            INotificationsSystem notificationsSystem,
            IInjectionProvider injectionProvider)
        {
            _worldManager = worldManager;
            _playerData = playerData;
            _notificationsSystem = notificationsSystem;
            _injectionProvider = injectionProvider;

            _treaties = new List<ITreaty>();
            _treatiesByTribe = new Dictionary<Tribe, List<ITreaty>>();
        }

        public void AddTreaty(ITreaty treaty)
        {
            if(!_treaties.Contains(treaty))
            {
                AddTreatyToCollections(treaty);

                treaty.OnRatification();
            }
        }

        private void AddTreatyToCollections(ITreaty treaty)
        {
            _treaties.Add(treaty);

            if (!_treatiesByTribe.ContainsKey(treaty.From))
            {
                _treatiesByTribe.Add(treaty.From, new List<ITreaty>());
            }

            if (!_treatiesByTribe.ContainsKey(treaty.To))
            {
                _treatiesByTribe.Add(treaty.To, new List<ITreaty>());
            }

            _treatiesByTribe[treaty.From].Add(treaty);
            _treatiesByTribe[treaty.To].Add(treaty);
        }

        public void Clear()
        {
            _treaties.Clear();
            _treatiesByTribe.Clear();
        }

        public IEnumerable<ITreaty> GetTreatiesFor(Tribe tribe)
        {
            if(_treatiesByTribe.ContainsKey(tribe))
            {
                return _treatiesByTribe[tribe];
            }

            return Enumerable.Empty<ITreaty>();
        }

        public IEnumerable<ITreaty> GetTreatiesFor(Tribe tribe1, Tribe tribe2)
        {
            if (_treatiesByTribe.ContainsKey(tribe1))
            {
                return _treatiesByTribe[tribe1].Where(t => t.To == tribe2 || t.From == tribe2).ToArray();
            }

            return Enumerable.Empty<ITreaty>();
        }

        public void Initialize(WorldState worldState)
        {
        }

        public void LoadCustomData(SerializedObject worldRoot)
        {
            var serviceRoot = worldRoot.GetChild("treaty_helper");
            foreach (var treatyRoot in serviceRoot.GetChildren("treaty"))
            {
                var typeName = treatyRoot.GetString("typename");

                var treaty = _injectionProvider.BuildNamed<ITreaty>(typeName);
                treaty.LoadFrom(_worldManager, treatyRoot);

                AddTreatyToCollections(treaty);
            }
        }

        public void SaveCustomData(SerializedObject worldRoot)
        {
            var serviceRoot = worldRoot.CreateChild("treaty_helper");

            foreach (var treaty in _treaties)
            {
                var treatyRoot = serviceRoot.CreateChild("treaty");
                treatyRoot.Set("typename", treaty.GetType().Name);
                treaty.SerializeTo(treatyRoot);
            }
        }

        public void StepSimulation(double elapsedMonths)
        {
            foreach(var treaty in _treaties.ToArray())
            {
                treaty.StepSimulation(elapsedMonths);
            }
        }

        public void RemoveTreaty(ITreaty treaty)
        {
            treaty.OnRemoval();

            _treaties.Remove(treaty);

            if (_treatiesByTribe.ContainsKey(treaty.From))
            {
                _treatiesByTribe[treaty.From].Remove(treaty);
            }

            if (_treatiesByTribe.ContainsKey(treaty.To))
            {
                _treatiesByTribe[treaty.To].Remove(treaty);
            }
        }
    }
}
