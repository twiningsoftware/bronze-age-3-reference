﻿using Bronze.Contracts;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.Simulation.Events;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.Services
{
    public class LosTracker : ILosTracker, IWorldStateService, ISubSimulator
    {
        private readonly IWorldManager _worldManager;
        private readonly ITradeManager _tradeManager;
        private readonly IWorldMinimapDrawer _worldMinimapDrawer;
        private readonly IPlayerDataTracker _playerData;
        private readonly ISimulationEventBus _simulationEventBus;

        private int _planeCount;
        private int _worldSize;
        private int _nextTribeIndex;

        private Dictionary<Tribe, List<Tribe>> _losInheritence;
        
        public bool GodVision { get; set; }

        public LosTracker(
            IWorldManager worldManager,
            ITradeManager tradeManager,
            IWorldMinimapDrawer worldMinimapDrawer,
            IPlayerDataTracker playerDataTracker,
            ISimulationEventBus simulationEventBus)
        {
            _worldManager = worldManager;
            _tradeManager = tradeManager;
            _worldMinimapDrawer = worldMinimapDrawer;
            _playerData = playerDataTracker;
            _simulationEventBus = simulationEventBus;

            _losInheritence = new Dictionary<Tribe, List<Tribe>>();
        }

        public void Clear()
        {
            _planeCount = 0;
            _worldSize = 0;
            _nextTribeIndex = 0;
            GodVision = false;
            _losInheritence.Clear();
        }

        public void Initialize(WorldState worldState)
        {
            _planeCount = worldState.Planes.Max(p => p.Id) + 1;
            _worldSize = worldState.Size;
        }

        public void SaveCustomData(SerializedObject root)
        {
            var serviceRoot = root.CreateChild("los_tracker");

            foreach(var tribe in _losInheritence.Keys)
            {
                var inheritRoot = serviceRoot.CreateChild("los_inherit");

                inheritRoot.Set("id", tribe.Id);

                foreach(var otherTribe in _losInheritence[tribe])
                {
                    inheritRoot.CreateChild("from").Set("id", otherTribe.Id);
                }
            }
        }

        public void LoadCustomData(SerializedObject root)
        {
            var serviceRoot = root.GetChild("los_tracker");

            foreach (var inheritRoot in serviceRoot.GetChildren("los_inherit"))
            {
                var tribe = inheritRoot.GetObjectReference(
                    "id",
                    _worldManager.Tribes,
                    t => t.Id);

                foreach (var otherTribeRoot in inheritRoot.GetChildren("from"))
                {
                    var otherTribe = otherTribeRoot.GetObjectReference(
                        "id",
                        _worldManager.Tribes,
                        t => t.Id);

                    SetLosCopy(otherTribe, tribe);
                }
            }
        }
        
        public void StepSimulation(double elapsedMonths)
        {
            if (_worldManager.Tribes.Any())
            {
                _nextTribeIndex = _nextTribeIndex % _worldManager.Tribes.Count;
                var currentTribe = _worldManager.Tribes[_nextTribeIndex];
                _nextTribeIndex += 1;

                DoCalculationsFor(currentTribe);
            }
        }

        private void DoCalculationsFor(Tribe tribe)
        {
            if(tribe.WorldExplored.GetLength(0) == 0)
            {
                tribe.WorldExplored = new bool[_planeCount, _worldSize, _worldSize];
            }
            if (tribe.WorldVisibility.GetLength(0) == 0)
            {
                tribe.WorldVisibility = new bool[_planeCount, _worldSize, _worldSize];
            }

            if (!GodVision)
            {
                foreach (var plane in _worldManager.Planes)
                {
                    for (var x = 0; x < _worldSize; x++)
                    {
                        for (var y = 0; y < _worldSize; y++)
                        {
                            tribe.WorldVisibility[plane.Id, x, y] = false;
                        }
                    }
                }

                var sourceTribes = tribe.Yield();
                if(_losInheritence.ContainsKey(tribe))
                {
                    sourceTribes = sourceTribes.Concat(_losInheritence[tribe]).ToArray();
                }

                foreach (var army in sourceTribes.SelectMany(t => t.Armies))
                {
                    FillOutVisibility(tribe, army.Cell, army.VisionRange);
                }

                var cellsWithDevelopments = sourceTribes.SelectMany(t => t.Settlements)
                    .SelectMany(s => s.Region.Cells)
                    .Where(c => c.Development != null)
                    .Where(c => c.Development.VisionRange > 0);

                foreach (var cell in cellsWithDevelopments)
                {
                    FillOutVisibility(tribe, cell, cell.Development.VisionRange);
                }
                
                foreach (var cell in sourceTribes.SelectMany(t => t.Settlements).SelectMany(s => s.Region.Cells))
                {
                    if(tribe == _playerData.PlayerTribe && !tribe.WorldExplored[cell.Position.Plane, cell.Position.X, cell.Position.Y])
                    {
                        _simulationEventBus.SendEvent(new CellExploredSimulationEvent(cell));
                    }

                    tribe.WorldExplored[cell.Position.Plane, cell.Position.X, cell.Position.Y] = true;
                }

                foreach (var tradeRoute in sourceTribes.SelectMany(t => _tradeManager.GetTradeRoutesFor(t)))
                {
                    foreach(var cell in tradeRoute.PathCalculator.Path)
                    {
                        if (tribe == _playerData.PlayerTribe && !tribe.WorldExplored[cell.Position.Plane, cell.Position.X, cell.Position.Y])
                        {
                            _simulationEventBus.SendEvent(new CellExploredSimulationEvent(cell));
                        }

                        tribe.WorldVisibility[cell.Position.Plane, cell.Position.X, cell.Position.Y] = true;
                        tribe.WorldExplored[cell.Position.Plane, cell.Position.X, cell.Position.Y] = true;
                    }
                }
            }
            else
            {
                foreach (var plane in _worldManager.Planes)
                {
                    for (var x = 0; x < _worldSize; x++)
                    {
                        for (var y = 0; y < _worldSize; y++)
                        {
                            if (!tribe.WorldExplored[plane.Id, x, y] && tribe == _playerData.PlayerTribe)
                            {
                                _worldMinimapDrawer.PromptRedraw(plane.Id);
                            }

                            tribe.WorldVisibility[plane.Id, x, y] = true;
                            tribe.WorldExplored[plane.Id, x, y] = true;
                        }
                    }
                }
            }
        }
        
        private void FillOutVisibility(Tribe tribe, Cell cell, int visionRange)
        {
            var exploreRange = visionRange + 1;
            
            for(var i = -exploreRange; i <= exploreRange; i++)
            {
                for (var j = -exploreRange; j <= exploreRange; j++)
                {
                    var x = cell.Position.X + i;
                    var y = cell.Position.Y + j;

                    if(x >= 0 && x < _worldSize && y >= 0 && y < _worldSize)
                    {
                        var distSq = i * i + j * j;

                        var wasExplored = tribe.WorldExplored[cell.Position.Plane, x, y];

                        if (distSq <= visionRange * visionRange)
                        {
                            tribe.WorldVisibility[cell.Position.Plane, x, y] = true;
                            tribe.WorldExplored[cell.Position.Plane, x, y] = true;
                        }
                        else if (distSq <= exploreRange * exploreRange)
                        {
                            tribe.WorldExplored[cell.Position.Plane, x, y] = true;
                        }
                        
                        if(!wasExplored && tribe.WorldExplored[cell.Position.Plane, x, y] && tribe == _playerData.PlayerTribe)
                        {
                            _worldMinimapDrawer.PromptRedraw(cell.Position.Plane);

                            _simulationEventBus.SendEvent(
                                new CellExploredSimulationEvent(
                                    _worldManager.GetCell(new CellPosition(cell.Position.Plane, x, y))));
                        }
                    }
                }
            }
        }

        public void ExploreAround(Tribe tribe, Region region)
        {
            if (tribe.WorldExplored.GetLength(0) == 0)
            {
                tribe.WorldExplored = new bool[_planeCount, _worldSize, _worldSize];
            }
            if (tribe.WorldVisibility.GetLength(0) == 0)
            {
                tribe.WorldVisibility = new bool[_planeCount, _worldSize, _worldSize];
            }
            
            var cells = region.Cells
                .SelectMany(c => c.Neighbors)
                .Concat(region.Cells)
                .SelectMany(c => c.Neighbors)
                .Concat(region.Cells);

            foreach (var cell in cells)
            {
                if (!tribe.WorldExplored[cell.Position.Plane, cell.Position.X, cell.Position.Y] && tribe == _playerData.PlayerTribe)
                {
                    _worldMinimapDrawer.PromptRedraw(cell.Position.Plane);
                }

                tribe.WorldExplored[cell.Position.Plane, cell.Position.X, cell.Position.Y] = true;
            }
        }

        public IEnumerable<Cell> GetVisionFor(Army army)
        {
            var visionSq = army.VisionRange * army.VisionRange;

            for (var i = -army.VisionRange; i <= army.VisionRange; i++)
            {
                for (var j = -army.VisionRange; j <= army.VisionRange; j++)
                {
                    var x = army.Cell.Position.X + i;
                    var y = army.Cell.Position.Y + j;

                    if (x >= 0 && x < _worldSize && y >= 0 && y < _worldSize)
                    {
                        var distSq = i * i + j * j;

                        if (distSq <= visionSq)
                        {
                            yield return _worldManager.GetCell(new CellPosition(army.Cell.Position.Plane, x, y));
                        }
                    }
                }
            }
        }

        public void SetLosCopy(Tribe from, Tribe to)
        {
            if (!_losInheritence.ContainsKey(to))
            {
                _losInheritence.Add(to, new List<Tribe>());
            }

            _losInheritence[to].Add(from);
        }

        public void RemoveLosCopy(Tribe from, Tribe to)
        {
            if (_losInheritence.ContainsKey(to))
            {
                _losInheritence[to].RemoveAll(t => t == from);
            }
        }
    }
}
