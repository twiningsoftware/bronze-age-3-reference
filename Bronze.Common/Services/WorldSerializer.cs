﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using Bronze.Common.Serialization;
using Bronze.Contracts;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.Delegates;
using Bronze.Contracts.Exceptions;
using Bronze.Contracts.InjectableServices;
using Newtonsoft.Json;

namespace Bronze.Common.Services
{
    public class WorldSerializer : IWorldSerializer
    {
        public const int SCHEMA_0 = 0;

        public int CurrentSchema => SCHEMA_0;

        private readonly IWorldManager _worldManager;
        private readonly IModManager _modManager;
        private readonly IPlayerDataTracker _playerData;
        private readonly IWorldStateService[] _worldStateServices;
        private readonly ISavefileStreamSource _savefileStreamSource;
        private readonly INameSource _nameSource;
        private readonly IGamedataTracker _gamedataTracker;
        private readonly ISettlementCalculator _settlementCalculator;
        private readonly IGameInterface _gameInterface;
        
        public WorldSerializer(
            IWorldManager worldManager,
            IPlayerDataTracker playerData,
            IModManager modManager,
            IEnumerable<IWorldStateService> worldStateServices,
            ISavefileStreamSource savefileStreamSource,
            INameSource nameSource,
            IGamedataTracker gamedataTracker,
            ISettlementCalculator settlementCalculator,
            IWorldSimulator worldSimulator,
            IGameInterface gameInterface)
        {
            _worldManager = worldManager;
            _playerData = playerData;
            _modManager = modManager;
            _worldStateServices = worldStateServices.ToArray();
            _savefileStreamSource = savefileStreamSource;
            _nameSource = nameSource;
            _gamedataTracker = gamedataTracker;
            _settlementCalculator = settlementCalculator;
            _gameInterface = gameInterface;
        }

        public void LoadWorld(ReportProgress report, SaveSummary saveSummary)
        {
            var file = ReadWorldFromDisk(saveSummary, progress => report(0, "Reading File", progress));

            // TODO schema upgrading

            if(file.Summary.SchemaVersion != CurrentSchema)
            {
                throw new FileLoadException("Save file is incompatable and cannot be upgraded.");
            }

            foreach (var service in _worldStateServices)
            {
                service.Clear();
            }

            try
            {
                var worldSize = file.Root.GetInt("size");

                _nameSource.InitializeForSeed(file.Summary.Seed.GetHashCode());

                var planes = ReadWorldStructure(
                    file,
                    worldSize,
                    progress => report(0.1, "Reading Planes", progress));

                var tribes = ReadTribes(
                    file,
                    planes.SelectMany(p => p.Regions).ToArray(),
                    progress => report(0.7, "Reading Tribes", progress));

                var notablePeople = ReadPeople(
                    file,
                    tribes,
                    progress => report(0.75, "Reading People", progress));

                var edgeAdjacencies = file.Root.GetChildren("edge_adjacency")
                    .Select(so => so.GetString("group"))
                    .ToArray();

                var worldTraits = file.Root.GetChildren("world_trait")
                    .Select(x => x.GetObjectReference(
                        "trait",
                        _gamedataTracker.Traits,
                        t => t.Id))
                    .ToArray();

                var worldState = new WorldState(
                    saveSummary.WorldName,
                    saveSummary.Seed,
                    file.Root.GetDouble("month"),
                    file.Root.GetDouble("hostility"),
                    worldSize,
                    planes,
                    file.Root.GetObjectReference(
                        "player_tribe_id",
                        tribes,
                        t => t.Id),
                    tribes,
                    notablePeople,
                    false,
                    edgeAdjacencies,
                    worldTraits);

                var count = 0.0;
                foreach (var service in _worldStateServices)
                {
                    report(0.8, "Initialization", count / _worldStateServices.Length);
                    service.Initialize(worldState);
                    count += 1;
                }

                _worldManager.PlaytimeSeconds = file.Summary.PlayedTimeSeconds;

                count = 0.0;
                foreach (var service in _worldStateServices)
                {
                    service.LoadCustomData(file.Root);

                    count += 1;
                    report(0.9, "Reading Custom Data", count / _worldStateServices.Length);
                }

                report(1.0, "Processing", 0);

                foreach (var settlement in _worldManager.Tribes.SelectMany(t => t.Settlements))
                {
                    if (settlement.NeedsCalculation)
                    {
                        settlement.ClearCalculationFlag();
                        _settlementCalculator.QueueCalculation(settlement);
                    }
                }

                _settlementCalculator.ForceCalculations(progress => report(1.0, "Processing", progress));
            }
            catch(Exception ex)
            {
                throw new WorldLoadException(saveSummary, ex.Message, ex);
            }
        }

        public void SaveWorld(ReportProgress report)
        {
            var file = new SaveFile();

            report(0, "Writing Summary", 0);

            file.Summary.ActiveMods = _modManager
                .GetActiveMods()
                .Select(m => m.Name)
                .ToArray();
            file.Summary.LastPlayed = DateTime.Now;
            file.Summary.PlayedTimeSeconds = _worldManager.PlaytimeSeconds;
            file.Summary.Seed = _worldManager.Seed;
            file.Summary.WorldName = _worldManager.WorldName;
            file.Summary.PlayerRace = _playerData.PlayerTribe.Race.Name;
            file.Summary.SchemaVersion = CurrentSchema;
            file.Summary.GameVersion = _gameInterface.AssemblyVersion.ToString();

            file.Root.Set("month", _worldManager.Month);
            file.Root.Set("hostility", _worldManager.Hostility);
            file.Root.Set("player_tribe_id", _playerData.PlayerTribe.Id);
            file.Root.Set("size", _worldManager.WorldSize);

            AddWorldStructure(file, progress => report(0, "Writing Planes", progress));

            AddTribes(file, progress => report(0.8, "Writing Tribes", progress));

            AddPeople(file, progress => report(0.85, "Writing People", progress));

            foreach(var edgeAdjacency in _worldManager.EdgeAdjacencies)
            {
                file.Root.CreateChild("edge_adjacency").Set("group", edgeAdjacency);
            }

            foreach (var worldTrait in _worldManager.WorldTraits)
            {
                file.Root.CreateChild("world_trait").Set("trait", worldTrait.Id);
            }

            var count = 0.0;
            foreach(var service in _worldStateServices)
            {
                service.SaveCustomData(file.Root);

                count += 1;
                report(0.9, "Writing Custom Data", count / _worldStateServices.Length);
            }
            
            WriteWorldToDisk(file, progress => report(1, "Writing File", progress));
        }
        
        private void AddWorldStructure(SaveFile file, ReportStepProgress report)
        {
            var planesRoot = file.Root.CreateChild("planes");

            var count = 0.0;
            var allPlanes = _worldManager.Planes.ToArray();
            foreach (var plane in allPlanes)
            {
                var planeRoot = planesRoot.CreateChild("plane");

                plane.SerializeTo(planeRoot);

                count += 1;
                report(count / allPlanes.Length);
            }
        }

        private IEnumerable<Plane> ReadWorldStructure(SaveFile file, int worldSize, ReportStepProgress report)
        {
            var planes = new List<Plane>();

            var planesRoot = file.Root.GetChild("planes");
            
            var count = 0.0;
            var planeRoots = planesRoot.GetChildren("plane").ToArray();
            foreach (var planeRoot in planeRoots)
            {
                var plane = Plane.DeserializeFrom(_gamedataTracker, planeRoot);
                planes.Add(plane);

                count += 1;
                report(count / planeRoots.Length * 3);

                var cells = new Cell[worldSize, worldSize];

                foreach (var region in plane.Regions)
                {
                    foreach (var cell in region.Cells)
                    {
                        cells[cell.Position.X, cell.Position.Y] = cell;
                    }
                }
                
                Util.LinkAdjacentCells(cells);

                count += 1;
                report(count / planeRoots.Length * 3);
                
                Util.LinkAdjacentRegions(plane.Regions);

                count += 1;
                report(count / planeRoots.Length * 3);
            }

            // TODO perform linking between planes?

            return planes;
        }

        private void AddTribes(SaveFile file, ReportStepProgress report)
        {
            var tribesRoot = file.Root.CreateChild("tribes");

            var count = 0.0;
            var allPlanes = _worldManager.Tribes.ToArray();
            foreach (var tribe in allPlanes)
            {
                var tribeRoot = tribesRoot.CreateChild("tribe");

                tribe.SerializeTo(tribeRoot);

                count += 1;
                report(count / allPlanes.Length);
            }
        }

        private IEnumerable<Tribe> ReadTribes(
            SaveFile file, 
            IEnumerable<Region> regions,
            ReportStepProgress report)
        {
            var tribes = new List<Tribe>();

            var tribesRoot = file.Root.GetChild("tribes");

            var count = 0.0;
            var triberoots = tribesRoot.GetChildren("tribe").ToArray();
            foreach (var tribeRoot in triberoots)
            {
                tribes.Add(Tribe.DeserializeFrom(_worldManager, _gamedataTracker, regions, tribeRoot));
                
                count += 1;
                report(count / triberoots.Length);
            }

            return tribes;
        }

        private void AddPeople(SaveFile file, ReportStepProgress report)
        {
            var peopleRoot = file.Root.CreateChild("notable_people");

            var count = 0.0;
            var allPeople = _worldManager.Tribes.SelectMany(t => t.NotablePeople).ToArray();
            foreach (var person in allPeople)
            {
                var personRoot = peopleRoot.CreateChild("notable_person");

                person.SerializeTo(personRoot);

                count += 1;
                report(count / allPeople.Length);
            }
        }

        private IEnumerable<NotablePerson> ReadPeople(
            SaveFile file,
            IEnumerable<Tribe> tribes,
            ReportStepProgress report)
        {
            var people = new List<NotablePerson>();

            var peopleRoot = file.Root.GetChild("notable_people");

            var count = 0.0;
            var peopleRoots = peopleRoot.GetChildren("notable_person").ToArray();
            foreach (var personRoot in peopleRoots)
            {
                var person = NotablePerson.DeserializeFrom(_gamedataTracker, tribes, personRoot);
                person.Owner.NotablePeople.Add(person);
                people.Add(person);

                count += 1;
                report(0.5 + count / peopleRoots.Length);
            }
            
            foreach (var personRoot in peopleRoots)
            {
                NotablePerson.DeserializePostLinking(_gamedataTracker, people, personRoot);
                
                count += 1;
                report(0.5 + count / peopleRoots.Length);
            }

            return people;
        }

        private void WriteWorldToDisk(SaveFile file, ReportStepProgress report)
        {
            var sanitizedName = MakeValidFileName(file.WorldName);


            using (var saveStream = _savefileStreamSource.OpenWriteStream(sanitizedName).Result)
            using (var archive = new ZipArchive(saveStream, ZipArchiveMode.Create))
            {
                report(0);
                var summaryEntry = archive.CreateEntry("summary.json");
                using (var stream = summaryEntry.Open())
                using (var streamWriter = new StreamWriter(stream))
                {

                    streamWriter.Write(JsonConvert.SerializeObject(file.Summary));
                    streamWriter.Flush();
                }

                report(0.2);
                var bodyEntry = archive.CreateEntry("body.json");
                using (var stream = bodyEntry.Open())
                using (var streamWriter = new StreamWriter(stream))
                {

                    streamWriter.Write(JsonConvert.SerializeObject(file.Root));
                    streamWriter.Flush();
                }

                report(1);
            }
        }

        private SaveFile ReadWorldFromDisk(SaveSummary saveSummary, ReportStepProgress report)
        {
            var file = new SaveFile();

            using (var saveStream = _savefileStreamSource.OpenReadStream(saveSummary.FileName).Result)
            using (var archive = new ZipArchive(saveStream, ZipArchiveMode.Read))
            {
                

                report(0);
                var summaryEntry = archive.Entries
                            .Where(e => e.Name == "summary.json")
                            .FirstOrDefault();
                using (var stream = summaryEntry.Open())
                using (var streamReader = new StreamReader(stream))
                {
                    file.Summary = JsonConvert.DeserializeObject<SaveSummary>(streamReader.ReadToEnd());
                }
                
                report(0.2);
                var bodyEntry = archive.Entries
                            .Where(e => e.Name == "body.json")
                            .FirstOrDefault();
                using (var stream = bodyEntry.Open())
                using (var streamReader = new StreamReader(stream))
                {
                    file.Root = JsonConvert.DeserializeObject<SerializedObject>(streamReader.ReadToEnd());
                }

                report(1);
            }

            return file;
        }


        // From https://stackoverflow.com/a/847251
        private static string MakeValidFileName(string name)
        {
            string invalidChars = System.Text.RegularExpressions.Regex.Escape(new string(System.IO.Path.GetInvalidFileNameChars()));
            string invalidRegStr = string.Format(@"([{0}]*\.+$)|([{0}]+)", invalidChars);

            return System.Text.RegularExpressions.Regex.Replace(name, invalidRegStr, "_");
        }
    }
}
