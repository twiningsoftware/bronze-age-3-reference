﻿using Bronze.Common.UI.Modals;
using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.Services
{
    public class HasAttritionNotificationHandler : INotificationHandler
    {
        private readonly IDialogManager _dialogManager;

        public HasAttritionNotificationHandler(IDialogManager dialogManager)
        {
            _dialogManager = dialogManager;
        }

        public string Name => typeof(HasAttritionNotificationHandler).Name;

        public void Handle(Notification notification)
        {
            _dialogManager.PushModal<HasAttritionModal>();
        }

        public void HandleIgnored(Notification notification)
        {
        }
    }
}
