﻿using System.Linq;
using System.Xml.Linq;
using Bronze.Contracts.Data.Dungeons;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.DataLoaders
{
    /// <summary>
    /// This is called during the data load process to load the new data files that define dungeons.
    /// </summary>
    public class DungeonDataLoader : IDataLoader
    {
        private readonly IXmlReaderUtil _xmlReaderUtil;
        private readonly IDungeonDataManager _dungeonDataManager;
        private readonly IInjectionProvider _injectionProvider;
        private readonly IGamedataTracker _gamedataTracker;

        public string ElementName => "dungeon_definition";

        public DungeonDataLoader(
            IXmlReaderUtil xmlReaderUtil,
            IDungeonDataManager dungeonDataManager,
            IInjectionProvider injectionProvider,
            IGamedataTracker gamedataTracker)
        {
            _xmlReaderUtil = xmlReaderUtil;
            _dungeonDataManager = dungeonDataManager;
            _injectionProvider = injectionProvider;
            _gamedataTracker = gamedataTracker;
        }

        public void Load(XElement element)
        {
            var overviewElement = _xmlReaderUtil.Element(element, "overview");

            var dungeonDefinition = new DungeonDefinition
            {
                Id = _xmlReaderUtil.AttributeValue(element, "id"),
                Name = _xmlReaderUtil.AttributeValue(element, "name"),
                RoomTypes = element.Elements("room_type")
                    .Select(e => LoadRoomType(e))
                    .ToArray(),
                OverviewDescription = overviewElement.Value,
                OverviewImage = _xmlReaderUtil.AttributeValue(overviewElement, "image"),
                AcceptableBodyguardCategories = element
                    .Elements("acceptable_bodyguard")
                    .Select(e => _xmlReaderUtil.ObjectReferenceLookup(
                        e,
                        "category",
                        _gamedataTracker.UnitCategories,
                        uc => uc.Id))
                    .ToArray()
            };
            
            dungeonDefinition.StartingRoomId = _xmlReaderUtil.AttributeValue(element, "starting_room_id");

            dungeonDefinition.RoomDefinitions = element
                .Elements("room")
                .Select(e => new DungeonRoomDefinition
                {
                    Id = _xmlReaderUtil.AttributeValue(e, "id"),
                    RoomTypeOptions = e.Elements("option")
                        .Select(x => _xmlReaderUtil.ObjectReferenceLookup(
                            x,
                            "room_type_id",
                            dungeonDefinition.RoomTypes,
                            rt => rt.Id))
                        .ToArray()
                })
                .ToArray();

            foreach(var roomTypeRoot in element.Elements("room_type"))
            {
                LoadRoomTypeResolutions(dungeonDefinition, roomTypeRoot);
            }

            _dungeonDataManager.DungeonDefinitions.RemoveAll(d => d.Id == dungeonDefinition.Id);
            _dungeonDataManager.DungeonDefinitions.Add(dungeonDefinition);
        }
        
        private DungeonRoomType LoadRoomType(XElement element)
        {
            return new DungeonRoomType
            {
                Id = _xmlReaderUtil.AttributeValue(element, "id"),

                InitialStates = element.Elements("initial_state")
                    .Select(e => new DungeonRoomInitialState
                    {
                        State = _xmlReaderUtil.AttributeValue(e, "state"),
                        Value = _xmlReaderUtil.AttributeValue(e, "value")
                    })
                    .ToArray(),

                Descriptions = element.Elements("description")
                    .Select(e => new DungeonRoomDescription
                    {
                        Title = _xmlReaderUtil.AttributeValue(e, "title"),
                        Image = _xmlReaderUtil.AttributeValue(e, "image"),
                        Description = e.Value,
                        IfState = new IfState
                        {
                            State = _xmlReaderUtil.AttributeValue(e, "state"),
                            Value = _xmlReaderUtil.AttributeValue(e, "value")
                        }
                    })
                    .ToArray(),

                EscapeConditions = _xmlReaderUtil.Element(element, "escape_possible")
                    .Elements("if_state")
                    .Select(e => new IfState
                    {
                        State = _xmlReaderUtil.AttributeValue(e, "state"),
                        Value = _xmlReaderUtil.AttributeValue(e, "value")
                    })
                    .ToArray()
            };
        }

        private void LoadRoomTypeResolutions(DungeonDefinition dungeonDefinition, XElement element)
        {
            var roomType = _xmlReaderUtil.ObjectReferenceLookup(
                element,
                "id",
                dungeonDefinition.RoomTypes,
                r => r.Id);

            roomType.Resolutions = element.Elements("resolution")
                    .Select(e => _injectionProvider
                        .BuildNamed<IRoomResolution>(_xmlReaderUtil.AttributeValue(e, "type"))
                            .LoadFrom(_injectionProvider, _xmlReaderUtil, e, dungeonDefinition))
                    .ToArray();
        }

        public void PostLoad(XElement element)
        {
        }
    }
}
