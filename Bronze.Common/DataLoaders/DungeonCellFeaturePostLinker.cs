﻿using Bronze.Common.Data.Dungeons;
using Bronze.Common.DataLoaders;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Bronze.Common.DataLoaders
{
    public class DungeonCellFeaturePostLinker : DataLoaderBase
    {
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IXmlReaderUtil _xmlReaderUtil;
        private readonly IDungeonDataManager _dungeonDataManager;

        public override string ElementName => "cell_development";

        public DungeonCellFeaturePostLinker(
            IGamedataTracker gamedataTracker,
            IXmlReaderUtil xmlReaderUtil,
            IDungeonDataManager dungeonDataManager)
        {
            _gamedataTracker = gamedataTracker;
            _xmlReaderUtil = xmlReaderUtil;
            _dungeonDataManager = dungeonDataManager;

        }

        public override void Load(XElement element)
        {
        }

        public override void PostLoad(XElement element)
        {
            var cellDevelopment = _xmlReaderUtil.ObjectReferenceLookup(
                element,
                "id",
                _gamedataTracker.CellDevelopments,
                cf => cf.Id);

            if (cellDevelopment is DungeonDevelopmentType dungeonDevelopmentType)
            {
                dungeonDevelopmentType.DungeonDefinition = _xmlReaderUtil
                    .ObjectReferenceLookup(
                        element,
                        "dungeon_id",
                        _dungeonDataManager.DungeonDefinitions,
                        dd => dd.Id);
            }
        }
    }
}
