﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.InjectableServices;
using System;
using System.Linq;
using System.Xml.Linq;

namespace Bronze.Common.DataLoaders
{
    public class PeoplePartDataLoader : DataLoaderBase
    {
        private readonly IGamedataTracker _gamedataTracker;

        public override string ElementName => "people_part";

        public PeoplePartDataLoader(IGamedataTracker gamedataTracker)
        {
            _gamedataTracker = gamedataTracker;
        }

        public override void Load(XElement element)
        {
            var part = new PeoplePart
            {
                Id = AttributeValue(element, "id"),
                Layer = AttributeAsDouble(element, "layer"),
                BaseKeys = LoadKeys(element),
                DeadKeys = element
                    .Elements("dead_role")
                    .Select(e => LoadKeys(e))
                    .FirstOrDefault(),
                GeneralKeys = element
                    .Elements("general_role")
                    .Select(e => LoadKeys(e))
                    .FirstOrDefault(),
                RulerKeys = element
                    .Elements("ruler_role")
                    .Select(e => LoadKeys(e))
                    .FirstOrDefault()
            };
            
            _gamedataTracker.PeopleParts.RemoveAll(x => x.Id == part.Id);
            _gamedataTracker.PeopleParts.Add(part);
        }

        private PeoplePartKeys LoadKeys(XElement element)
        {
            return new PeoplePartKeys
            {
                BaseKey = element.Elements("base")
                    .Attributes("key")
                    .Select(a => a.Value)
                    .FirstOrDefault(),
                MaleKey = element.Elements("male")
                    .Attributes("key")
                    .Select(a => a.Value)
                    .FirstOrDefault(),
                FemaleKey = element.Elements("female")
                    .Attributes("key")
                    .Select(a => a.Value)
                    .FirstOrDefault(),
            };
        }

        public override void PostLoad(XElement element)
        {
        }
    }
}