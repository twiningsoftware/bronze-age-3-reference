﻿using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.InjectableServices;
using System.Linq;
using System.Xml.Linq;

namespace Bronze.Common.DataLoaders
{
    public class TerrainDataLoader : DataLoaderBase
    {
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IXmlReaderUtil _xmlReaderUtil;

        public override string ElementName => "terrain";

        public TerrainDataLoader(
            IGamedataTracker gamedataTracker, 
            IXmlReaderUtil xmlReaderUtil)
        {
            _gamedataTracker = gamedataTracker;
            _xmlReaderUtil = xmlReaderUtil;
        }
        
        public override void Load(XElement element)
        {
            var terrain = new Terrain
            {
                Id = AttributeValue(element, "id"),
                Name = AttributeValue(element, "name"),
                AdjacencyGroups = element.Elements("adjacency_group")
                    .Select(e => AttributeValue(e, "group"))
                    .ToArray(),
                DisplayLayers = _xmlReaderUtil.LoadTileDisplays(element),
                Traits = element.Elements("trait")
                    .Select(e => ObjectReferenceLookup(e, "trait",
                        _gamedataTracker.Traits,
                        t => t.Id))
                        .ToArray(),
                NotTraits = element.Elements("not_trait")
                    .Select(e => ObjectReferenceLookup(e, "trait",
                        _gamedataTracker.Traits,
                        t => t.Id))
                        .ToArray(),
                LogiDistance = _xmlReaderUtil.AttributeAsDouble(_xmlReaderUtil.Element(element, "logi"), "distance"),
                MinimapColor = _xmlReaderUtil.AttributeAsEnum<MinimapColor>(element, "minimap_color")
            };

            _gamedataTracker.Terrain.RemoveAll(b => b.Id == terrain.Id);
            _gamedataTracker.Terrain.Add(terrain);

        }

        public override void PostLoad(XElement element)
        {
        }
    }
}
