﻿using Bronze.Contracts.Exceptions;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;

namespace Bronze.Common.DataLoaders
{
    public abstract class DataLoaderBase : IDataLoader
    {
        public abstract string ElementName { get; }

        public abstract void Load(XElement element);

        public abstract void PostLoad(XElement element);

        protected XElement Descendant(XDocument document, string descendantName)
        {
            return Descendant(document.Root, descendantName);
        }

        protected XElement Descendant(XElement element, string descendantName)
        {
            var descendant = element.Descendants(descendantName).FirstOrDefault();

            if (descendant == null)
            {
                throw DataLoadException.XmlMissingDescendant(element, descendantName);
            }

            return descendant;
        }

        protected XElement DescendantOrNull(XElement element, string descendantName)
        {
            return element.Descendants(descendantName).FirstOrDefault();
        }

        protected string AttributeValue(XElement element, string attributeName)
        {
            var attribute = element.Attribute(attributeName);

            if (attribute == null)
            {
                throw DataLoadException.XmlMissingAttribute(element, attributeName);
            }

            return attribute.Value;
        }

        protected string OptionalAttributeValue(XElement element, string attributeName, string defaultValue)
        {
            var attribute = element.Attribute(attributeName);

            if (attribute == null)
            {
                return defaultValue;
            }

            return attribute.Value;
        }

        protected int AttributeAsInt(XElement element, string attributeName)
        {
            var value = AttributeValue(element, attributeName);

            if (int.TryParse(value, out var i))
            {
                return i;
            }

            throw DataLoadException.XmlAttributeParse(element, attributeName, "integer", value);
        }

        protected float AttributeAsFloat(XElement element, string attributeName)
        {
            var value = AttributeValue(element, attributeName);

            value = value.Replace(',', '.');

            if (float.TryParse(value, NumberStyles.Float, CultureInfo.InvariantCulture, out var f))
            {
                return f;
            }

            throw DataLoadException.XmlAttributeParse(element, attributeName, "float", value);
        }

        protected double AttributeAsDouble(XElement element, string attributeName)
        {
            var value = AttributeValue(element, attributeName);

            value = value.Replace(',', '.');

            if (double.TryParse(value, NumberStyles.Float, CultureInfo.InvariantCulture, out var f))
            {
                return f;
            }

            throw DataLoadException.XmlAttributeParse(element, attributeName, "double", value);
        }
        
        protected bool AttributeAsBool(XElement element, string attributeName)
        {
            var value = AttributeValue(element, attributeName);

            if (bool.TryParse(value, out var val))
            {
                return val;
            }

            throw DataLoadException.XmlAttributeParse(element, attributeName, "boolean", value);
        }

        protected TEnum AttributeAsEnum<TEnum>(XElement element, string attributeName) where TEnum : struct
        {
            var value = AttributeValue(element, attributeName);

            if (Enum.TryParse<TEnum>(value, true, out var e))
            {
                return e;
            }

            throw DataLoadException.XmlAttributeParse(element, attributeName, typeof(TEnum).Name, value);
        }

        protected T ObjectReferenceLookup<T>(
            XElement element, 
            string attributeName, 
            IEnumerable<T> options, 
            Func<T, string> matchDataForObject)
        {
            var matchValue = AttributeValue(element, attributeName);

            var match = options
                .Where(o => matchDataForObject(o) == matchValue)
                .FirstOrDefault();

            if(match != null)
            {
                return match;
            }

            throw DataLoadException.MissingDataReference(
                element.Name.ToString(),
                typeof(T).Name,
                matchValue);
        }

        protected T OptionalObjectReferenceLookup<T>(
            XElement element,
            string attributeName,
            IEnumerable<T> options,
            Func<T, string> matchDataForObject)
        {
            var matchValue = OptionalAttributeValue(element, attributeName, null);

            if(matchValue == null)
            {
                return default(T);
            }

            var match = options
                .Where(o => matchDataForObject(o) == matchValue)
                .FirstOrDefault();

            if (match != null)
            {
                return match;
            }

            throw DataLoadException.MissingDataReference(
                element.Name.ToString(),
                typeof(T).Name,
                matchValue);
        }
    }
}
