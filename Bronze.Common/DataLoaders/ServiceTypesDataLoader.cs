﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.InjectableServices;
using System.Linq;
using System.Xml.Linq;

namespace Bronze.Common.DataLoaders
{
    public class ServiceTypesDataLoader : DataLoaderBase
    {
        private readonly IGamedataTracker _gamedataTracker;

        public override string ElementName => "service";

        public ServiceTypesDataLoader(IGamedataTracker gamedataTracker)
        {
            _gamedataTracker = gamedataTracker;
        }

        public override void Load(XElement element)
        {
            var serviceName = AttributeValue(element, "name");
            _gamedataTracker.ServiceTypes.RemoveAll(i => i.Name == serviceName);

            var lookupInt = 0;
            if (_gamedataTracker.ServiceTypes.Any())
            {
                lookupInt = _gamedataTracker.ServiceTypes.Max(i => i.LookupIndex) + 1;
            }

            var item = new ServiceType(
                serviceName,
                AttributeValue(element, "icon"),
                AttributeValue(element, "area_icon"),
                Descendant(element, "description").Value,
                lookupInt);

            _gamedataTracker.ServiceTypes.RemoveAll(i => i.Name == item.Name);
            _gamedataTracker.ServiceTypes.Add(item);
        }

        public override void PostLoad(XElement element)
        {
        }
    }
}