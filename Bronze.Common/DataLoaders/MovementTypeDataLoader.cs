﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.InjectableServices;
using System.Linq;
using System.Xml.Linq;

namespace Bronze.Common.DataLoaders
{
    public class MovementTypeDataLoader : DataLoaderBase
    {
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IXmlReaderUtil _xmlReaderUtil;

        public override string ElementName => "movement_type";

        public MovementTypeDataLoader(IGamedataTracker gamedataTracker, IXmlReaderUtil xmlReaderUtil)
        {
            _gamedataTracker = gamedataTracker;
            _xmlReaderUtil = xmlReaderUtil;
        }

        public override void Load(XElement element)
        {
            var movementType = new MovementType
            {
                Id = AttributeValue(element, "id"),
                Name = AttributeValue(element, "name"),
                IconKey = AttributeValue(element, "icon"),
                RequiredTraits = element
                    .Elements("required_trait")
                    .Select(x => ObjectReferenceLookup(
                        x,
                        "id",
                        _gamedataTracker.Traits,
                        t => t.Id))
                    .ToArray(),
                AnimationNames = element.Elements("animation")
                    .Select(e => AttributeValue(e, "name"))
                    .ToArray(),
                SlowedBy = element.Elements("slowed_by")
                    .Select(x => ObjectReferenceLookup(
                        x,
                        "trait",
                        _gamedataTracker.Traits,
                        t => t.Id))
                    .ToArray(),
                SpedBy = element.Elements("sped_by")
                    .Select(x => ObjectReferenceLookup(
                        x,
                        "trait",
                        _gamedataTracker.Traits,
                        t => t.Id))
                    .ToArray(),
                PreventedBy = element.Elements("prevented_by")
                    .Select(x => ObjectReferenceLookup(
                        x,
                        "trait",
                        _gamedataTracker.Traits,
                        t => t.Id))
                    .ToArray(),
            };

            _gamedataTracker.MovementTypes.RemoveAll(i => i.Id == movementType.Id);
            _gamedataTracker.MovementTypes.Add(movementType);
        }

        public override void PostLoad(XElement element)
        {
        }
    }
}