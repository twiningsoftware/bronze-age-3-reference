﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.InjectableServices;
using System.Linq;
using System.Xml.Linq;

namespace Bronze.Common.DataLoaders
{
    public class RecipieDataLoader : DataLoaderBase
    {
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IXmlReaderUtil _xmlReaderUtil;

        public override string ElementName => "recipe";

        public RecipieDataLoader(IGamedataTracker gamedataTracker, IXmlReaderUtil xmlReaderUtil)
        {
            _gamedataTracker = gamedataTracker;
            _xmlReaderUtil = xmlReaderUtil;
        }

        public override void Load(XElement element)
        {
            var recipie = new Recipie
            {
                Id = AttributeValue(element, "id"),
                Verb = AttributeValue(element, "verb"),
                IconKey = AttributeValue(element, "icon"),
                Name = AttributeValue(element, "name"),
                Inputs = element.Elements("input")
                    .Select(x => _xmlReaderUtil.LoadItemRate(x))
                    .ToArray(),
                Outputs = element.Elements("output")
                    .Select(x => _xmlReaderUtil.LoadItemRate(x))
                    .ToArray(),
                RequiredTraits = element
                    .Elements("required")
                    .Select(e => _xmlReaderUtil.ObjectReferenceLookup(
                        e,
                        "trait",
                        _gamedataTracker.Traits,
                        t => t.Id))
                    .ToArray(),
                BoostingTraits = element
                    .Elements("boosted_by")
                    .Select(e => _xmlReaderUtil.ObjectReferenceLookup(
                        e,
                        "trait",
                        _gamedataTracker.Traits,
                        t => t.Id))
                    .ToArray(),
                SlowingTraits = element
                    .Elements("slowed_by")
                    .Select(e => _xmlReaderUtil.ObjectReferenceLookup(
                        e,
                        "trait",
                        _gamedataTracker.Traits,
                        t => t.Id))
                    .ToArray(),
                AnimationNames = element
                    .Elements("animation")
                    .Select(e => _xmlReaderUtil.AttributeValue(e, "name"))
                    .ToArray(),
                BonusedBy = element
                    .Elements("bonused_by")
                    .Select(e => _xmlReaderUtil.ObjectReferenceLookup(
                        e,
                        "bonus",
                        _gamedataTracker.BonusTypes,
                        b => b.Id))
                    .ToArray(),
            };

            _gamedataTracker.Recipies.RemoveAll(i => i.Id == recipie.Id);
            _gamedataTracker.Recipies.Add(recipie);
        }

        public override void PostLoad(XElement element)
        {
        }
    }
}