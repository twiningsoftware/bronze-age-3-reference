﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Bronze.Common.Mutators;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Exceptions;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.DataLoaders
{
    public class DataMutatorLoader : IDataMutatorLoader
    {
        public void LoadMutators(XElement element, List<IDataMutator> dataMutators)
        {
            if(element.Name == "remove_element")
            {
                dataMutators.Add(new RemoveElementDataMutator(
                    AttributeValue(element, "xpath")));
            }
            else if (element.Name == "add_element")
            {
                var child = element.Descendants().FirstOrDefault();

                if(child == null)
                {
                    throw DataLoadException.XmlMissingDescendant(element, "of any type");
                }

                dataMutators.Add(new AddElementDataMutator(
                    AttributeValue(element, "xpath"),
                    child.ToString()));
            }
            else if (element.Name == "set_attribute")
            {
                dataMutators.Add(new SetAttributeDataMutator(
                    AttributeValue(element, "xpath"),
                    AttributeValue(element, "attribute"),
                    AttributeValue(element, "value")));
            }
        }

        protected string AttributeValue(XElement element, string attributeName)
        {
            var attribute = element.Attribute(attributeName);

            if (attribute == null)
            {
                throw DataLoadException.XmlMissingAttribute(element, attributeName);
            }

            return attribute.Value;
        }
    }
}
