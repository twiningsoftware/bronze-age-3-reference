﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.InjectableServices;
using System.Linq;
using System.Xml.Linq;

namespace Bronze.Common.DataLoaders
{
    public class BonusTypeDataLoader : DataLoaderBase
    {
        private readonly IGamedataTracker _gamedataTracker;

        public override string ElementName => "bonus_type";

        public BonusTypeDataLoader(IGamedataTracker gamedataTracker)
        {
            _gamedataTracker = gamedataTracker;
        }

        public override void Load(XElement element)
        {
            var typeId = AttributeValue(element, "id");
            _gamedataTracker.BonusTypes.RemoveAll(i => i.Id == typeId);

            var lookupInt = 0;
            if (_gamedataTracker.BonusTypes.Any())
            {
                lookupInt = _gamedataTracker.BonusTypes.Max(i => i.LookupIndex) + 1;
            }

            var bonusType = new BonusType(
                typeId,
                AttributeValue(element, "name"),
                AttributeValue(element, "icon"),
                lookupInt);
            
            _gamedataTracker.BonusTypes.Add(bonusType);
        }

        public override void PostLoad(XElement element)
        {
        }
    }
}