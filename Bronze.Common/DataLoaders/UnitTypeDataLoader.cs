﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.InjectableServices;
using System.Xml.Linq;

namespace Bronze.Common.DataLoaders
{
    public class UnitTypeDataLoader : DataLoaderBase
    {
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IInjectionProvider _injectionProvider;
        private readonly IXmlReaderUtil _xmlReaderUtil;

        public override string ElementName => "unit_type";

        public UnitTypeDataLoader(
            IGamedataTracker gamedataTracker, 
            IInjectionProvider injectionProvider,
            IXmlReaderUtil xmlReaderUtil)
        {
            _gamedataTracker = gamedataTracker;
            _injectionProvider = injectionProvider;
            _xmlReaderUtil = xmlReaderUtil;
        }
        
        public override void Load(XElement element)
        {
            var typeName = AttributeValue(element, "type");

            var unitType = _injectionProvider.BuildNamed<IUnitType>(typeName);
            unitType.LoadFrom(_gamedataTracker, _xmlReaderUtil, _injectionProvider, element);

            _gamedataTracker.UnitTypes.RemoveAll(u => u.Id == unitType.Id);
            _gamedataTracker.UnitTypes.Add(unitType);
        }

        public override void PostLoad(XElement element)
        {
        }
    }
}
