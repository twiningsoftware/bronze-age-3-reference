﻿using System;
using System.Linq;
using System.Xml.Linq;
using Bronze.Contracts.Data.AI;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Exceptions;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.DataLoaders
{
    public class CityPrefabDataLoader : DataLoaderBase
    {
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IXmlReaderUtil _xmlReaderUtil;
        private readonly IInjectionProvider _injectionProvider;

        public override string ElementName => "city_prefab";

        public CityPrefabDataLoader(
            IGamedataTracker gamedataTracker, 
            IXmlReaderUtil xmlReaderUtil,
            IInjectionProvider injectionProvider)
        {
            _gamedataTracker = gamedataTracker;
            _xmlReaderUtil = xmlReaderUtil;
            _injectionProvider = injectionProvider;
        }

        public override void Load(XElement element)
        {
            var race = _xmlReaderUtil.ObjectReferenceLookup(
                element,
                "race",
                _gamedataTracker.Races,
                r => r.Id);

            var prefab = new CityPrefab
            {
                Id = _xmlReaderUtil.AttributeValue(element, "id"),
                IsCityStarter = _xmlReaderUtil.OptionalAttributeValue(element, "is_city_starter", "false").ToLower() == "true",
                StructurePlacements = element.Elements("structure")
                    .Select(e => LoadStructurePlacement(race, e))
                    .ToArray(),
                RequiredItemAccess = element.Elements("req_item_access")
                    .Select(e => _xmlReaderUtil.ObjectReferenceLookup(
                        e,
                        "item",
                        _gamedataTracker.Items,
                        i => i.Name))
                    .ToArray(),
                ToBorderRoads = element
                    .Elements("to_border")
                    .Select(e => _xmlReaderUtil.ObjectReferenceLookup(
                        e,
                        "structure_id",
                        _gamedataTracker.StructureTypes,
                        s => s.Id))
                    .FirstOrDefault(),
                ConnectivityTraits = element
                    .Elements("connectivity")
                    .Select(e => _xmlReaderUtil.ObjectReferenceLookup(
                        e,
                        "trait_id",
                        _gamedataTracker.Traits,
                        s => s.Id))
                    .ToArray(),
                OverlapAllowances = element
                    .Elements("overlap_allowed")
                    .Select(e => _xmlReaderUtil.ObjectReferenceLookup(
                        e,
                        "trait_id",
                        _gamedataTracker.Traits,
                        s => s.Id))
                    .ToArray(),
                DistanceToStorage = element
                    .Elements("storage_distance")
                    .Select(e => _xmlReaderUtil.AttributeAsDouble(e, "dist") as double?)
                    .FirstOrDefault()
            };


            race.CityPrefabs.RemoveAll(cp => cp.Id == prefab.Id);
            race.CityPrefabs.Add(prefab);
        }

        private CityprefabStructurePlacement LoadStructurePlacement(Race race, XElement element)
        {
            var structure = _xmlReaderUtil.ObjectReferenceLookup(
                    element,
                    "structure_id",
                    race.AvailableStructures,
                    s => s.Id);

            return new CityprefabStructurePlacement
            {
                Structure = structure,
                Recipie = _xmlReaderUtil.OptionalObjectReferenceLookup(
                    element,
                    "recipie_id",
                    structure.Recipies,
                    r => r.Id),
                RelativeX = _xmlReaderUtil.AttributeAsInt(element, "x"),
                RelativeY = _xmlReaderUtil.AttributeAsInt(element, "y")
            };
        }

        public override void PostLoad(XElement element)
        {
        }
    }
}
