﻿using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.InjectableServices;
using System.Xml.Linq;

namespace Bronze.Common.DataLoaders
{
    public class DecorationDataLoader : DataLoaderBase
    {
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IXmlReaderUtil _xmlReaderUtil;

        public override string ElementName => "decoration";

        public DecorationDataLoader(
            IGamedataTracker gamedataTracker,
            IXmlReaderUtil xmlReaderUtil)
        {
            _gamedataTracker = gamedataTracker;
            _xmlReaderUtil = xmlReaderUtil;
        }

        public override void Load(XElement element)
        {
            var decoration = new Decoration
            {
                Id = AttributeValue(element, "id"),
                Name = AttributeValue(element, "name"),
                DisplayLayers = _xmlReaderUtil.LoadTileDisplays(element),
                MinimapColor = _xmlReaderUtil.AttributeAsEnum<MinimapColor>(element, "minimap_color")
            };

            _gamedataTracker.Decorations.RemoveAll(d => d.Id == decoration.Id);
            _gamedataTracker.Decorations.Add(decoration);
        }

        public override void PostLoad(XElement element)
        {
        }
    }
}
