﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.InjectableServices;
using System.Xml.Linq;

namespace Bronze.Common.DataLoaders
{
    public class TraitDataLoader : DataLoaderBase
    {
        private readonly IGamedataTracker _gamedataTracker;

        public override string ElementName => "trait";

        public TraitDataLoader(IGamedataTracker gamedataTracker)
        {
            _gamedataTracker = gamedataTracker;
        }
        
        public override void Load(XElement element)
        {
            var cellFeature = new Trait
            {
                Id = AttributeValue(element, "id"),
                Name = AttributeValue(element, "name"),
                IconKey = AttributeValue(element, "icon"),
                Description = Descendant(element, "description").Value,
                Hidden = DescendantOrNull(element, "hidden") != null
            };

            _gamedataTracker.Traits.RemoveAll(b => b.Id == cellFeature.Id);
            _gamedataTracker.Traits.Add(cellFeature);
        }

        public override void PostLoad(XElement element)
        {
        }
    }
}
