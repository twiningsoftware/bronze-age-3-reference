﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.InjectableServices;
using System.Xml.Linq;

namespace Bronze.Common.DataLoaders
{
    public class CellFeatureDataLoader : DataLoaderBase
    {
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IInjectionProvider _injectionProvider;
        private readonly IXmlReaderUtil _xmlReaderUtil;

        public override string ElementName => "cell_feature";

        public CellFeatureDataLoader(
            IGamedataTracker gamedataTracker,
            IInjectionProvider injectionProvider,
            IXmlReaderUtil xmlReaderUtil)
        {
            _gamedataTracker = gamedataTracker;
            _injectionProvider = injectionProvider;
            _xmlReaderUtil = xmlReaderUtil;
        }

        public override void Load(XElement element)
        {
            var typeName = AttributeValue(element, "type");

            var developmentType = _injectionProvider.BuildNamed<ICellFeature>(typeName);
            developmentType.LoadFrom(_gamedataTracker, _xmlReaderUtil, element);

            _gamedataTracker.CellFeatures.RemoveAll(r => r.Id == developmentType.Id);
            _gamedataTracker.CellFeatures.Add(developmentType);
        }

        public override void PostLoad(XElement element)
        {
        }
    }
}
