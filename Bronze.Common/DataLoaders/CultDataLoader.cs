﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.InjectableServices;
using System.Linq;
using System.Xml.Linq;

namespace Bronze.Common.DataLoaders
{
    public class CultDataLoader : DataLoaderBase
    {
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IXmlReaderUtil _xmlReaderUtil;
        private readonly IInjectionProvider _injectionProvider;

        public override string ElementName => "cult";

        public CultDataLoader(
            IGamedataTracker gamedataTracker,
            IXmlReaderUtil xmlReaderUtil,
            IInjectionProvider injectionProvider)
        {
            _gamedataTracker = gamedataTracker;
            _xmlReaderUtil = xmlReaderUtil;
            _injectionProvider = injectionProvider;
        }

        public override void Load(XElement element)
        {
            var cultId = AttributeValue(element, "id");
            _gamedataTracker.Cults.RemoveAll(c => c.Id == cultId);

            var lookupInt = 0;
            if (_gamedataTracker.Cults.Any())
            {
                lookupInt = _gamedataTracker.Cults.Max(i => i.LookupIndex) + 1;
            }

            var cult = new Cult(
                cultId,
                AttributeValue(element, "name"),
                AttributeValue(element, "adjective"),
                AttributeValue(element, "icon"),
                lookupInt,
                AttributeAsDouble(element, "own_pop_influence"));

            foreach(var settlementBonusElement in element.Elements("settlement_bonus"))
            {
                var bonus = _xmlReaderUtil.ObjectReferenceLookup(
                    settlementBonusElement,
                    "bonus",
                    _gamedataTracker.BonusTypes,
                    b => b.Id);

                var fullAmount = _xmlReaderUtil.AttributeAsDouble(
                    settlementBonusElement,
                    "full_amount");

                cult.SettlementBonuses[bonus] = fullAmount;
            }

            foreach (var cultRecruitsElement in element.Elements("cult_recruits"))
            {
                var category = _xmlReaderUtil.ObjectReferenceLookup(
                    cultRecruitsElement,
                    "category",
                    _gamedataTracker.RecruitmentSlotCategories,
                    b => b.Id);

                var fullAmount = _xmlReaderUtil.AttributeAsInt(
                    cultRecruitsElement,
                    "full_amount");

                cult.RecruitmentSlots.Add(category, fullAmount);
            }
            
            _gamedataTracker.Cults.Add(cult);
        }

        public override void PostLoad(XElement element)
        {
            var cult = _xmlReaderUtil.ObjectReferenceLookup(
                element,
                "id",
                _gamedataTracker.Cults,
                c => c.Id);

            foreach(var cultRelationElement in element.Elements("cult_relation"))
            {
                var otherCult = _xmlReaderUtil.ObjectReferenceLookup(
                    cultRelationElement,
                    "cult",
                    _gamedataTracker.Cults,
                    c => c.Id);

                var relation = _xmlReaderUtil.AttributeAsDouble(
                    cultRelationElement,
                    "relation");

                cult.CultRelations[otherCult] = relation;
            }

            foreach (var riteElement in element.Elements("rite"))
            {
                var typeName = AttributeValue(riteElement, "type");

                var rite = _injectionProvider.BuildNamed<ICultRite>(typeName);
                rite.LoadFrom(_gamedataTracker, _xmlReaderUtil, riteElement, cult);
                cult.Rites.Add(rite);
            }
        }
    }
}