﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.InjectableServices;
using System.Xml.Linq;

namespace Bronze.Common.DataLoaders
{
    public class TribeControllerTypeDataLoader : DataLoaderBase
    {
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IInjectionProvider _injectionProvider;
        private readonly IXmlReaderUtil _xmlReaderUtil;

        public override string ElementName => "tribe_controller";

        public TribeControllerTypeDataLoader(
            IGamedataTracker gamedataTracker,
            IInjectionProvider injectionProvider,
            IXmlReaderUtil xmlReaderUtil)
        {
            _gamedataTracker = gamedataTracker;
            _injectionProvider = injectionProvider;
            _xmlReaderUtil = xmlReaderUtil;
        }
        
        public override void Load(XElement element)
        {
            var typeName = AttributeValue(element, "type");

            var controllerType = _injectionProvider.BuildNamed<ITribeControllerType>(typeName);
            controllerType.LoadFrom(_gamedataTracker, _xmlReaderUtil, element);

            _gamedataTracker.ControllerTypes.RemoveAll(s => s.Id == controllerType.Id);
            _gamedataTracker.ControllerTypes.Add(controllerType);
        }

        public override void PostLoad(XElement element)
        {
        }
    }
}
