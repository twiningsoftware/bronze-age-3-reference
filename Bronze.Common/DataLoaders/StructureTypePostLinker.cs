﻿using Bronze.Contracts.InjectableServices;
using System.Linq;
using System.Xml.Linq;

namespace Bronze.Common.DataLoaders
{
    public class StructureTypePostLinker : DataLoaderBase
    {
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IXmlReaderUtil _xmlReaderUtil;

        public override string ElementName => "structure_type";

        public StructureTypePostLinker(
            IGamedataTracker gamedataTracker, 
            IXmlReaderUtil xmlReaderUtil)
        {
            _gamedataTracker = gamedataTracker;
            _xmlReaderUtil = xmlReaderUtil;
        }
        
        public override void Load(XElement element)
        {
            var id = AttributeValue(element, "id");

            var structureType = _gamedataTracker.StructureTypes
                .Where(s => s.Id == id)
                .FirstOrDefault();

            if(structureType != null)
            {
                structureType.PostLink(_gamedataTracker, _xmlReaderUtil, element);
            }
        }

        public override void PostLoad(XElement element)
        {
        }
    }
}
