﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.InjectableServices;
using System.Linq;
using System.Xml.Linq;

namespace Bronze.Common.DataLoaders
{
    public class HaulerInfoDataLoader : DataLoaderBase
    {
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IXmlReaderUtil _xmlReaderUtil;

        public override string ElementName => "hauler_info";

        public HaulerInfoDataLoader(
            IGamedataTracker gamedataTracker,
            IXmlReaderUtil xmlReaderUtil)
        {
            _gamedataTracker = gamedataTracker;
            _xmlReaderUtil = xmlReaderUtil;
        }

        public override void Load(XElement element)
        {
            var haulerInfo = new HaulerInfo
            {
                Id = AttributeValue(element, "id"),
                HaulDistance = AttributeAsDouble(element, "haul_distance"),
                Capacity = AttributeAsInt(element, "capacity"),
                MovementTypes = element
                    .Elements("movement_type")
                        .Select(mte => ObjectReferenceLookup(
                            mte,
                            "id",
                            _gamedataTracker.MovementTypes,
                            mt => mt.Id))
                    .ToArray(),
                Animations = element
                        .Elements("animation")
                        .Select(ae => _xmlReaderUtil.LoadIndividualAnimation(ae))
                        .ToArray()
            };

            _gamedataTracker.HaulerInfos.RemoveAll(x => x.Id == haulerInfo.Id);
            _gamedataTracker.HaulerInfos.Add(haulerInfo);
        }

        public override void PostLoad(XElement element)
        {
        }
    }
}