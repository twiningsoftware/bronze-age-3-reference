﻿using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.InjectableServices;
using System.Linq;
using System.Xml.Linq;

namespace Bronze.Common.DataLoaders
{
    public class BiomeDataLoader : DataLoaderBase
    {
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IXmlReaderUtil _xmlReaderUtil;

        public override string ElementName => "biome";

        public BiomeDataLoader(
            IGamedataTracker gamedataTracker,
            IXmlReaderUtil xmlReaderUtil)
        {
            _gamedataTracker = gamedataTracker;
            _xmlReaderUtil = xmlReaderUtil;
        }
        
        public override void Load(XElement element)
        {
            var biome = new Biome
            {
                Id = AttributeValue(element, "id"),
                Name = AttributeValue(element, "name"),
                MinimapColor = AttributeAsEnum<MinimapColor>(element, "minimap_color"),
                AdjacencyGroups = element.Elements("adjacency_group")
                    .Select(e => AttributeValue(e, "group"))
                    .ToArray(),
                DetailLayers = element
                    .Elements("detailed_sprite")
                    .Select(e => _xmlReaderUtil.LoadCellSpriteLayer(e))
                    .Concat(element
                        .Elements("detailed_animation")
                        .Select(e => _xmlReaderUtil.LoadCellAnimationLayer(e)))
                    .ToArray(),
                SummaryLayers = element
                    .Elements("summary_sprite")
                    .Select(e => _xmlReaderUtil.LoadCellSpriteLayer(e))
                    .Concat(element
                        .Elements("summary_animation")
                        .Select(e => _xmlReaderUtil.LoadCellAnimationLayer(e)))
                    .ToArray(),
                Traits = element.Elements("trait")
                    .Select(e => ObjectReferenceLookup(e, "trait",
                        _gamedataTracker.Traits,
                        t => t.Id))
                        .ToArray(),
                NotTraits = element.Elements("not_trait")
                    .Select(e => ObjectReferenceLookup(e, "trait",
                        _gamedataTracker.Traits,
                        t => t.Id))
                        .ToArray(),
                DistrictGenerationLayers = element
                    .Elements("district_generation_layer")
                    .Select(e => _xmlReaderUtil.LoadDistrictGenerationLayer(e))
                    .ToArray()
            };

            _gamedataTracker.Biomes.RemoveAll(b => b.Id == biome.Id);
            _gamedataTracker.Biomes.Add(biome);
        }

        public override void PostLoad(XElement element)
        {
        }
    }
}
