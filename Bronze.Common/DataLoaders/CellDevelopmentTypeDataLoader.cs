﻿using System.Xml.Linq;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.DataLoaders
{
    public class CellDevelopmentTypeDataLoader : DataLoaderBase
    {
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IInjectionProvider _injectionProvider;
        private readonly IXmlReaderUtil _xmlReaderUtil;

        public override string ElementName => "cell_development";

        public CellDevelopmentTypeDataLoader(
            IGamedataTracker gamedataTracker,
            IInjectionProvider injectionProvider,
            IXmlReaderUtil xmlReaderUtil)
        {
            _gamedataTracker = gamedataTracker;
            _injectionProvider = injectionProvider;
            _xmlReaderUtil = xmlReaderUtil;
        }
        
        public override void Load(XElement element)
        {
            var typeName = AttributeValue(element, "type");

            var developmentType = _injectionProvider.BuildNamed<ICellDevelopmentType>(typeName);
            developmentType.LoadFrom(_gamedataTracker, _xmlReaderUtil, element);

            _gamedataTracker.CellDevelopments.RemoveAll(r => r.Id == developmentType.Id);
            _gamedataTracker.CellDevelopments.Add(developmentType);
        }

        public override void PostLoad(XElement element)
        {
            var typeName = AttributeValue(element, "type");

            var developmentType = _xmlReaderUtil.ObjectReferenceLookup(
                element,
                "id",
                _gamedataTracker.CellDevelopments,
                cd => cd.Id);

            developmentType.PostLoad(_gamedataTracker, _xmlReaderUtil, element);
        }
    }
}
