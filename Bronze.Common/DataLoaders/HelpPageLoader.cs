﻿using System.Linq;
using System.Xml.Linq;
using Bronze.Common.Help;
using System;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.Help;

namespace Bronze.Common.DataLoaders
{
    public class HelpPageLoader : DataLoaderBase
    {
        private readonly IHelpManager _wikiManager;
        public override string ElementName => "help_page";

        public HelpPageLoader(IHelpManager wikiManager)
        {
            _wikiManager = wikiManager;
        }

        public override void Load(XElement element)
        {
            var page = new HelpPage
            {
                Id = AttributeValue(element, "id"),
                Name = AttributeValue(element, "name"),
                IsStartingPage = AttributeAsBool(element, "start_page"),
                Topics = element
                    .Descendants("topic")
                    .Select(e => LoadTopic(e))
                    .ToArray()
            };

            _wikiManager.AddPage(page);
        }

        public override void PostLoad(XElement element)
        {
        }

        private HelpTopic LoadTopic(XElement element)
        {
            return new HelpTopic
            {
                Id = AttributeValue(element, "id"),
                Title = AttributeValue(element, "title"),
                ImageKey = AttributeValue(element, "image"),
                Body = element.Descendants()
                    .Where(e=>e.Name == "text" || e.Name == "icon_text")
                    .Select(e => LoadHelpBody(e))
                    .ToArray(),
                PageLinks = element.Descendants("link_to")
                    .Select(e => new HelpLink(AttributeValue(e, "page"), AttributeValue(e, "topic")))
                    .ToArray()
            };
        }

        private IHelpBody LoadHelpBody(XElement e)
        {
            if(e.Name == "text")
            {
                return new TextHelpBody(e.Value);
            }
            else if (e.Name == "icon_text")
            {
                return new IconTextHelpBody(AttributeValue(e, "icon"), e.Value, "");
            }

            throw new ArgumentException("Unkown help body element " + e.Name);
        }
    }
}
