﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.InjectableServices;
using System.Xml.Linq;

namespace Bronze.Common.DataLoaders
{
    public class UnitCategoryDataLoader : DataLoaderBase
    {
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IXmlReaderUtil _xmlReaderUtil;

        public override string ElementName => "unit_category";

        public UnitCategoryDataLoader(
            IGamedataTracker gamedataTracker,
            IXmlReaderUtil xmlReaderUtil)
        {
            _gamedataTracker = gamedataTracker;
            _xmlReaderUtil = xmlReaderUtil;
        }

        public override void Load(XElement element)
        {
            var category = new UnitCategory
            {
                Id = _xmlReaderUtil.AttributeValue(element, "id"),
                Name = _xmlReaderUtil.AttributeValue(element, "name"),
                IconKey = _xmlReaderUtil.AttributeValue(element, "icon")
            };

            _gamedataTracker.UnitCategories.RemoveAll(c => c.Id == category.Id);
            _gamedataTracker.UnitCategories.Add(category);
        }

        public override void PostLoad(XElement element)
        {
        }
    }
}