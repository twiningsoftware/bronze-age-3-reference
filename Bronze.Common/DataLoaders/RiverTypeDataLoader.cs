﻿using Bronze.Contracts.Data.Generation;
using Bronze.Contracts.InjectableServices;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Bronze.Common.DataLoaders
{
    public class RiverTypeDataLoader : DataLoaderBase
    {
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IXmlReaderUtil _xmlReaderUtil;
        private readonly IInjectionProvider _injectionProvider;
        private readonly IRegionGenerator[] _regionGenerators;

        public override string ElementName => "river_type";

        public RiverTypeDataLoader(
            IGamedataTracker gamedataTracker,
            IXmlReaderUtil xmlReaderUtil,
            IInjectionProvider injectionProvider,
            IEnumerable<IRegionGenerator> regionGenerators)
        {
            _gamedataTracker = gamedataTracker;
            _xmlReaderUtil = xmlReaderUtil;
            _injectionProvider = injectionProvider;
            _regionGenerators = regionGenerators.ToArray();
        }

        public override void Load(XElement element)
        {
            var riverType = new RiverType
            {
                Id = AttributeValue(element, "id"),
                MergeGroup = AttributeValue(element, "merge_group"),
                MergeSize = AttributeAsInt(element, "merge_size"),
                MinLength = AttributeAsInt(element, "min_length"),
                MaxLength = AttributeAsInt(element, "max_length"),
                Biome = OptionalObjectReferenceLookup(
                    element,
                    "biome",
                    _gamedataTracker.Biomes,
                    b => b.Id),
                CellFeature = OptionalObjectReferenceLookup(
                    element,
                    "cell_feature",
                    _gamedataTracker.CellFeatures,
                    cf => cf.Id),
                Fords = _xmlReaderUtil.Element(element, "fords")
                    .Elements()
                    .Select(e => _xmlReaderUtil.LoadInclusionFeature(e))
                    .ToArray(),
                InclusionConditions = element.Elements("inclusion_condition")
                        .Select(e => _xmlReaderUtil.LoadBorderFeatureCondition(e))
                        .ToArray(),
                ShortcutConditions = element.Elements("shortcut_condition")
                        .Select(e => _xmlReaderUtil.LoadBorderFeatureCondition(e))
                        .ToArray(),
                EndConditions = element.Elements("end_condition")
                        .Select(e => _xmlReaderUtil.LoadBorderFeatureCondition(e))
                        .ToArray(),
                PathConditions = element.Elements("path_condition")
                        .Select(e => _xmlReaderUtil.LoadBorderFeatureCondition(e))
                        .ToArray(),
                InclusionRadii = element
                    .Elements("inclusion_radii")
                    .Select(e => new InclusionRadius
                    {
                        Radius = AttributeAsInt(e, "radius"),
                        InclusionFeatures = e.Elements("inclusion_feature")
                                .Select(x => _xmlReaderUtil.LoadInclusionFeature(x))
                                .ToArray()
                    })
                    .ToArray(),
                DeltaConditions = _xmlReaderUtil.Element(element, "delta")
                    .Elements("delta_condition")
                    .Select(e => _xmlReaderUtil.LoadBorderFeatureCondition(e))
                    .ToArray(),
                DeltaInclusionRadii = _xmlReaderUtil.Element(element, "delta")
                    .Elements("inclusion_radii")
                    .Select(e => new InclusionRadius
                    {
                        Radius = AttributeAsInt(e, "radius"),
                        InclusionFeatures = e.Elements("inclusion_feature")
                                .Select(x => _xmlReaderUtil.LoadInclusionFeature(x))
                                .ToArray()
                    })
                    .ToArray(),
                BiomeChangeConditions = element.Elements("biome_change_condition")
                    .Select(e => _xmlReaderUtil.LoadBorderFeatureCondition(e))
                    .ToArray(),
            };



            _gamedataTracker.RiverTypes.RemoveAll(rt => rt.Id == riverType.Id);
            _gamedataTracker.RiverTypes.Add(riverType);
        }

        public override void PostLoad(XElement element)
        {
            var riverType = _xmlReaderUtil
                .ObjectReferenceLookup(
                    element,
                    "id",
                    _gamedataTracker.RiverTypes,
                    rt => rt.Id);

            var mergeElement = element.Element("merge");

            if (mergeElement != null)
            {
                riverType.MergeTo = _xmlReaderUtil
                    .ObjectReferenceLookup(
                        mergeElement,
                        "to",
                        _gamedataTracker.RiverTypes,
                        rt => rt.Id);

                riverType.MergeToThreshold = _xmlReaderUtil
                    .AttributeAsInt(mergeElement, "threshold");

                riverType.MergeToAdds = _xmlReaderUtil
                    .AttributeAsInt(mergeElement, "adds");
            }
        }
    }
}
