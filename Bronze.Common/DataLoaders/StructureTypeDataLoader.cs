﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.InjectableServices;
using System.Xml.Linq;

namespace Bronze.Common.DataLoaders
{
    public class StructureTypeDataLoader : DataLoaderBase
    {
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IInjectionProvider _injectionProvider;
        private readonly IXmlReaderUtil _xmlReaderUtil;

        public override string ElementName => "structure_type";

        public StructureTypeDataLoader(
            IGamedataTracker gamedataTracker,
            IInjectionProvider injectionProvider,
            IXmlReaderUtil xmlReaderUtil)
        {
            _gamedataTracker = gamedataTracker;
            _injectionProvider = injectionProvider;
            _xmlReaderUtil = xmlReaderUtil;
        }
        
        public override void Load(XElement element)
        {
            var typeName = AttributeValue(element, "type");

            var structureType = _injectionProvider.BuildNamed<IStructureType>(typeName);
            structureType.LoadFrom(_gamedataTracker, _xmlReaderUtil, element);

            _gamedataTracker.StructureTypes.RemoveAll(s => s.Id == structureType.Id);
            _gamedataTracker.StructureTypes.Add(structureType);
        }

        public override void PostLoad(XElement element)
        {
        }
    }
}
