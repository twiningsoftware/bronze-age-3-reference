﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.InjectableServices;
using System.Linq;
using System.Xml.Linq;

namespace Bronze.Common.DataLoaders
{
    public class TransportTypeDataLoader : DataLoaderBase
    {
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IInjectionProvider _injectionProvider;
        private readonly IXmlReaderUtil _xmlReaderUtil;

        public override string ElementName => "transport_type";

        public TransportTypeDataLoader(
            IGamedataTracker gamedataTracker,
            IInjectionProvider injectionProvider,
            IXmlReaderUtil xmlReaderUtil)
        {
            _gamedataTracker = gamedataTracker;
            _injectionProvider = injectionProvider;
            _xmlReaderUtil = xmlReaderUtil;
        }

        public override void Load(XElement element)
        {
            var typeName = AttributeValue(element, "type");

            var transportType = _injectionProvider.BuildNamed<ITransportType>(typeName);
            transportType.LoadFrom(_gamedataTracker, _xmlReaderUtil, _injectionProvider, element);

            _gamedataTracker.TransportTypes.RemoveAll(u => u.Id == transportType.Id);
            _gamedataTracker.TransportTypes.Add(transportType);
        }

        public override void PostLoad(XElement element)
        {
            var id = AttributeValue(element, "id");

            var transportType = _gamedataTracker.TransportTypes
                .Where(tt => tt.Id == id)
                .FirstOrDefault();

            if (transportType != null)
            {
                transportType.PostLink(_gamedataTracker, _xmlReaderUtil, element);
            }
        }
    }
}
