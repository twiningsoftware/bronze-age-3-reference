﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Generation;
using Bronze.Contracts.InjectableServices;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Bronze.Common.DataLoaders
{
    public class WorldTypeDataLoader : DataLoaderBase
    {
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IXmlReaderUtil _xmlReaderUtil;
        private readonly IInjectionProvider _injectionProvider;
        private readonly IRegionGenerator[] _regionGenerators;

        public override string ElementName => "world_type";

        public WorldTypeDataLoader(
            IGamedataTracker gamedataTracker,
            IXmlReaderUtil xmlReaderUtil,
            IInjectionProvider injectionProvider,
            IEnumerable<IRegionGenerator> regionGenerators)
        {
            _gamedataTracker = gamedataTracker;
            _xmlReaderUtil = xmlReaderUtil;
            _injectionProvider = injectionProvider;
            _regionGenerators = regionGenerators.ToArray();
        }

        public override void Load(XElement element)
        {
            var regionFeatureElements = element
                    .Elements("region_feature")
                    .ToArray();

            var regionFeatures = regionFeatureElements
                    .Select(e => LoadRegionFeature(e))
                    .ToArray();

            for (var i = 0; i < regionFeatureElements.Length; i++)
            {
                CrossLinkRegionFeatures(regionFeatureElements[i], regionFeatures[i], regionFeatures);
            }

            var worldType = new WorldType
            {
                Id = _xmlReaderUtil.AttributeValue(element, "id"),
                Name = _xmlReaderUtil.AttributeValue(element, "name"),
                Description = _xmlReaderUtil.Element(element, "description").Value,
                WorldNamePattern = _xmlReaderUtil.AttributeValue(element, "world_name_pattern"),
                Size = _xmlReaderUtil.AttributeAsInt(element, "size"),
                PlaneFeatures = element.Elements("plane_feature")
                    .Select(e => LoadPlaneFeature(e, regionFeatures))
                    .ToArray(),
                RegionFeatures = regionFeatures,
                EdgeAdjacencies = element.Elements("edge_adjacency")
                    .Select(e => _xmlReaderUtil.AttributeValue(e, "group"))
                    .ToArray(),
                WorldTraits = element.Elements("world_trait")
                    .Select(e => _xmlReaderUtil.ObjectReferenceLookup(
                        e,
                        "trait",
                        _gamedataTracker.Traits,
                        t => t.Id))
                    .ToArray()
            };

            _gamedataTracker.WorldTypes.RemoveAll(wt => wt.Id == worldType.Id);
            _gamedataTracker.WorldTypes.Add(worldType);
        }

        public override void PostLoad(XElement element)
        {
        }

        private PlaneFeature LoadPlaneFeature(XElement element, RegionFeature[] regionFeatures)
        {
            var planeFeature = new PlaneFeature
            {
                Id = AttributeValue(element, "id"),
                MirrorsPlane = AttributeValue(element, "mirrors_plane"),
                Name = AttributeValue(element, "name"),
                DisplayOrder = AttributeAsInt(element, "display_order"),
                RegionNamePattern = AttributeValue(element, "region_name_pattern"),
                Biome = ObjectReferenceLookup(
                    element,
                    "biome",
                    _gamedataTracker.Biomes,
                    b => b.Id),
                RegionGenerator = ObjectReferenceLookup(
                    element,
                    "region_style",
                    _regionGenerators,
                    g => g.RegionStyle),
                RegionMirrors = element
                    .Elements("region_mirror")
                    .Select(e => LoadRegionMirror(e, regionFeatures))
                    .ToArray(),
                SeedPoints = element
                    .Elements("seed_point")
                    .Select(e => _xmlReaderUtil.LoadSeedPoint(e, regionFeatures))
                    .ToArray(),
                SeedPointGroups = element
                    .Elements("seed_point_group")
                    .Select(e => _xmlReaderUtil.LoadSeedPointGroup(e, regionFeatures))
                    .ToArray(),
                CellFeature = OptionalObjectReferenceLookup(
                    element,
                    "cell_feature",
                    _gamedataTracker.CellFeatures,
                    cf => cf.Id),
                NativePlaneChanging = element
                    .Elements("native_plane_changing")
                    .Select(e => _xmlReaderUtil.ObjectReferenceLookup(e, "movement_type", _gamedataTracker.MovementTypes, m => m.Id))
                    .ToArray()
            };
            
            return planeFeature;
        }
        
        private RegionMirror LoadRegionMirror(XElement element, RegionFeature[] regionFeatures)
        {
            return new RegionMirror
            {
                Id = AttributeValue(element, "id"),
                MirrorsRegion = AttributeValue(element, "mirrors_region"),
                Biome = ObjectReferenceLookup(
                    element,
                    "biome",
                    _gamedataTracker.Biomes,
                    b => b.Id),
                RegionMirrors = element
                    .Elements("region_mirror")
                    .Select(e => LoadRegionMirror(e, regionFeatures))
                    .ToArray(),
                SubFeatures = element
                    .Elements("sub_feature")
                    .Select(e => LoadSubFeature(e, regionFeatures))
                    .ToArray(),
                BorderFeatures = element
                    .Elements("border_feature")
                    .Select(e => _xmlReaderUtil.LoadBorderFeature(e))
                    .ToArray(),
                InclusionFeatures = element
                    .Elements("inclusion_feature")
                    .Select(e => _xmlReaderUtil.LoadInclusionFeature(e))
                    .ToArray(),
                CellFeature = OptionalObjectReferenceLookup(
                    element,
                    "cell_feature",
                    _gamedataTracker.CellFeatures,
                    cf => cf.Id)
            };
        }

        private RegionFeature LoadRegionFeature(XElement element)
        {
            return new RegionFeature
            {
                Id = AttributeValue(element, "id"),
                Biome = ObjectReferenceLookup(
                    element,
                    "biome",
                    _gamedataTracker.Biomes,
                    b => b.Id),
                BorderFeatures = element
                    .Elements("border_feature")
                    .Select(e => _xmlReaderUtil.LoadBorderFeature(e))
                    .ToArray(),
                InclusionFeatures = element
                    .Elements("inclusion_feature")
                    .Select(e => _xmlReaderUtil.LoadInclusionFeature(e))
                    .ToArray(),
                CellFeature = OptionalObjectReferenceLookup(
                    element,
                    "cell_feature",
                    _gamedataTracker.CellFeatures,
                    cf => cf.Id),
                TribeSpawners = element.Elements("tribe_spawner")
                    .Select(e => _xmlReaderUtil.LoadTribeSpawner(e))
                    .ToArray(),
            };
        }
        
        private void CrossLinkRegionFeatures(
            XElement element, 
            RegionFeature regionFeature, 
            RegionFeature[] regionFeatures)
        {
            regionFeature.SubFeatures = element
                .Elements("sub_feature")
                .Select(e => LoadSubFeature(e, regionFeatures))
                .ToArray();
        }

        private SubFeature LoadSubFeature(XElement element, RegionFeature[] regionFeatures)
        {
            return new SubFeature
            {
                Id = _xmlReaderUtil.AttributeValue(element, "id"),
                Priority = _xmlReaderUtil.AttributeAsInt(element, "priority"),
                RegionsPerOccurance = _xmlReaderUtil.AttributeAsInt(element, "regions_per_occurrence"),
                MinimumOccurances = _xmlReaderUtil.AttributeAsInt(element, "minimum_occurrences"),
                MinSize = _xmlReaderUtil.AttributeAsInt(element, "min_size"),
                MaxSize = _xmlReaderUtil.AttributeAsInt(element, "max_size"),
                Margin = _xmlReaderUtil.AttributeAsInt(element, "margin"),
                RegionFeature = _xmlReaderUtil.ObjectReferenceLookup(
                        element,
                        "region_feature",
                        regionFeatures,
                        x => x.Id)
            };
        }
    }
}
