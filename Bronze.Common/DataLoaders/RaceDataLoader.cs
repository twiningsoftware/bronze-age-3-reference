﻿using System.Linq;
using System.Xml.Linq;
using Bronze.Contracts;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Exceptions;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.DataLoaders
{
    public class RaceDataLoader : DataLoaderBase
    {
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IXmlReaderUtil _xmlReaderUtil;
        private readonly IInjectionProvider _injectionProvider;

        public override string ElementName => "race";

        public RaceDataLoader(
            IGamedataTracker gamedataTracker, 
            IXmlReaderUtil xmlReaderUtil,
            IInjectionProvider injectionProvider)
        {
            _gamedataTracker = gamedataTracker;
            _xmlReaderUtil = xmlReaderUtil;
            _injectionProvider = injectionProvider;
        }

        public override void Load(XElement element)
        {
            var race = new Race
            {
                Id = AttributeValue(element, "id"),
                Name = AttributeValue(element, "name"),
                ArmyFlag = AttributeValue(element, "army_flag"),
                ArmyFlagIcon = AttributeValue(element, "army_flag_icon"),
                TribeNamePattern = AttributeValue(element, "tribe_name_pattern"),
                SettlementNamePattern = AttributeValue(element, "settlement_name_pattern"),
                ArmyNamePattern = AttributeValue(element, "army_name_pattern"),
                PersonNamePattern = AttributeValue(element, "person_name_pattern"),
                Description = Descendant(element, "description").Value,
                IsPlayerAvailable = AttributeAsBool(element, "player_available"),
                MarriageCompatabilityGroup = AttributeValue(element, "marriage_compatability"),
                Castes = element.Elements("caste")
                    .Select(e => LoadCaste(e))
                    .ToArray(),
                Traits = element.Elements("trait")
                    .Select(e => ObjectReferenceLookup(e, "trait",
                        _gamedataTracker.Traits,
                        t => t.Id))
                        .ToArray(),
                StartingPeopleScheme = _injectionProvider.BuildNamed<IStartingPeopleScheme>(
                    _xmlReaderUtil.AttributeValue(element, "starting_people_scheme")),
                AgeMaturity = AttributeAsDouble(element, "age_maturity"),
                AgeOld = AttributeAsDouble(element, "age_old"),
                PregnancyTime = AttributeAsDouble(element, "pregnancy_time"),
                BasePregnancyChance = AttributeAsDouble(element, "pregnancy_chance"),
                PeoplePartGroups = _xmlReaderUtil.Element(element, "people_parts")
                    .Elements("part_group")
                    .Select(e => LoadPeoplePartGroup(e))
                    .ToArray(),
                StartingSkills = _xmlReaderUtil.Element(element, "people_skills")
                    .Elements("group")
                    .Select(g => new StartingSkillGroup
                    {
                        PickCount = _xmlReaderUtil.AttributeAsInt(g, "picks"),
                        LikelyhoodNone = _xmlReaderUtil.AttributeAsInt(g, "likelyhood_none"),
                        Options = g.Elements("starting_skill")
                            .Select(e => new Contracts.LikelyhoodFor<NotablePersonSkill>
                            {
                                Likelyhood = _xmlReaderUtil.AttributeAsInt(e, "likelyhood"),
                                Value = _xmlReaderUtil.ObjectReferenceLookup(
                                    e,
                                    "skill",
                                    _gamedataTracker.Skills,
                                    s => s.Id)
                            })
                            .ToArray()
                    })
                    .ToArray(),
                StartingPeopleCultLikelyhoods = _xmlReaderUtil.Element(element, "people_cult")
                    .Elements("starting_cult_chance")
                    .Select(e => new LikelyhoodFor<Cult>
                    {
                        Value = _xmlReaderUtil.ObjectReferenceLookup(
                            e,
                            "cult",
                            _gamedataTracker.Cults,
                            c => c.Id),
                        Likelyhood = _xmlReaderUtil.AttributeAsInt(
                            e,
                            "likelyhood")
                    })
                    .ToArray()
            };

            foreach(var caste in race.Castes)
            {
                caste.Race = race;
            }

            foreach (var casteElement in element.Elements("caste"))
            {
                CasteAscentionLinking(race, casteElement);
            }
            
            var defaultCastes = race.Castes.Where(c => c.IsDefault).Count();
            if (defaultCastes < 1)
            {
                throw DataLoadException.MiscError(element, "Race doesn't have default caste.");
            }
            else if (defaultCastes > 1)
            {
                throw DataLoadException.MiscError(element, "Race has more than one default caste.");
            }

            _gamedataTracker.Races.RemoveAll(r => r.Id == race.Id);
            _gamedataTracker.Races.Add(race);
        }
        
        private Caste LoadCaste(XElement element)
        {
            return new Caste
            {
                Id = AttributeValue(element, "id"),
                Name = AttributeValue(element, "name"),
                NamePlural = AttributeValue(element, "name_plural"),
                IsDefault = AttributeAsBool(element, "is_default"),
                IconKey = AttributeValue(element, "icon"),
                WorkerIcon = AttributeValue(element, "worker_icon"),
                BigIconKey = AttributeValue(element, "big_icon"),
                GrowthRate = AttributeAsDouble(element, "growth_rate"),
                IndividualAnimations = element
                    .Elements("animation")
                    .Select(e => _xmlReaderUtil.LoadIndividualAnimation(e))
                    .ToArray(),
                SatisfactionBonusedBy = element
                    .Elements("satisfaction_bonused_by")
                    .Select(e => _xmlReaderUtil.ObjectReferenceLookup(
                        e,
                        "bonus",
                        _gamedataTracker.BonusTypes,
                        b => b.Id))
                    .ToArray(),
                GrowthBonusedBy = element
                    .Elements("bonus_pop_growth")
                    .Select(e => _xmlReaderUtil.ObjectReferenceLookup(
                        e,
                        "bonus",
                        _gamedataTracker.BonusTypes,
                        b => b.Id))
                    .ToArray(),
                CultLikelyhoods = element
                    .Elements("starting_cult_chance")
                    .Select(e => new LikelyhoodFor<Cult>
                    {
                        Value = _xmlReaderUtil.ObjectReferenceLookup(
                            e,
                            "cult",
                            _gamedataTracker.Cults,
                            c => c.Id),
                        Likelyhood = _xmlReaderUtil.AttributeAsInt(
                            e,
                            "likelyhood")
                    })
                    .ToArray()
            };
        }

        private void CasteAscentionLinking(Race race, XElement casteElement)
        {
            var caste = race.Castes
                .Where(c => c.Id == AttributeValue(casteElement, "id"))
                .FirstOrDefault();

            if (caste != null)
            {
                caste.AscendsTo = casteElement
                    .Elements("ascends_to")
                    .Select(e => ObjectReferenceLookup(
                        e,
                        "caste",
                        race.Castes,
                        c => c.Id))
                    .ToArray();
            }
        }

        public override void PostLoad(XElement element)
        {
            var race = ObjectReferenceLookup(
                element,
                "id",
                _gamedataTracker.Races,
                r => r.Id);

            race.PopRecruitmentSources = element
                .Elements("pop_recruitment")
                .Select(e => new PopRecruitmentSource
                {
                    Caste = _xmlReaderUtil.ObjectReferenceLookup(
                        e,
                        "caste",
                        race.Castes,
                        c => c.Id),
                    Category = _xmlReaderUtil.ObjectReferenceLookup(
                        e,
                        "category",
                        _gamedataTracker.RecruitmentSlotCategories,
                        c => c.Id),
                    PopPerSlot = _xmlReaderUtil.AttributeAsInt(e, "pop_per_slot")
                })
                .ToArray();
        }

        private PeoplePartGroup LoadPeoplePartGroup(XElement element)
        {
            return new PeoplePartGroup
            {
                Id = _xmlReaderUtil.AttributeValue(element, "id"),
                Inherited = _xmlReaderUtil.AttributeAsBool(element, "inherit"),
                Options = element.Elements("option")
                    .Select(e => _xmlReaderUtil.ObjectReferenceLookup(
                        e,
                        "part",
                        _gamedataTracker.PeopleParts,
                        p => p.Id))
                    .ToArray()
            };
        }
    }
}
