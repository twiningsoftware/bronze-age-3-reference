﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.InjectableServices;
using System.Linq;
using System.Xml.Linq;

namespace Bronze.Common.DataLoaders
{
    public class ItemDataLoader : DataLoaderBase
    {
        private readonly IGamedataTracker _gamedataTracker;

        public override string ElementName => "item";

        public ItemDataLoader(IGamedataTracker gamedataTracker)
        {
            _gamedataTracker = gamedataTracker;
        }

        public override void Load(XElement element)
        {
            var itemName = AttributeValue(element, "name");
            _gamedataTracker.Items.RemoveAll(i => i.Name == itemName);

            var lookupInt = 0;
            if (_gamedataTracker.Items.Any())
            {
                lookupInt = _gamedataTracker.Items.Max(i => i.LookupIndex) + 1;
            }

            var item = new Item(
                itemName,
                AttributeValue(element, "icon"),
                AttributeValue(element, "icon_big"),
                Descendant(element, "description").Value,
                lookupInt);

            _gamedataTracker.Items.RemoveAll(i => i.Name == item.Name);
            _gamedataTracker.Items.Add(item);
        }

        public override void PostLoad(XElement element)
        {
        }
    }
}