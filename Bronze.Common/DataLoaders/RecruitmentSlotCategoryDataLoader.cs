﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.InjectableServices;
using System.Linq;
using System.Xml.Linq;

namespace Bronze.Common.DataLoaders
{
    public class RecruitmentSlotCategoryDataLoader : DataLoaderBase
    {
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IXmlReaderUtil _xmlReaderUtil;

        public override string ElementName => "recruitment_slot_category";

        public RecruitmentSlotCategoryDataLoader(IGamedataTracker gamedataTracker, IXmlReaderUtil xmlReaderUtil)
        {
            _gamedataTracker = gamedataTracker;
            _xmlReaderUtil = xmlReaderUtil;
        }

        public override void Load(XElement element)
        {
            var category = new RecruitmentSlotCategory
            {
                Id = AttributeValue(element, "id"),
                Name = AttributeValue(element, "name"),
                RegenTimeMonths = AttributeAsDouble(element, "regen_months"),
                StrengthEstimate = AttributeAsDouble(element, "strength_estimate"),
            };

            _gamedataTracker.RecruitmentSlotCategories.RemoveAll(x => x.Id == category.Id);
            _gamedataTracker.RecruitmentSlotCategories.Add(category);
        }

        public override void PostLoad(XElement element)
        {
        }
    }
}