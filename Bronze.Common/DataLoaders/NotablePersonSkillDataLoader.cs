﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Exceptions;
using Bronze.Contracts.InjectableServices;
using System.Linq;
using System.Xml.Linq;

namespace Bronze.Common.DataLoaders
{
    public class NotablePersonSkillDataLoader : DataLoaderBase
    {
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IXmlReaderUtil _xmlReaderUtil;

        public override string ElementName => "skill";

        public NotablePersonSkillDataLoader(
            IGamedataTracker gamedataTracker,
            IXmlReaderUtil xmlReaderUtil)
        {
            _gamedataTracker = gamedataTracker;
            _xmlReaderUtil = xmlReaderUtil;
        }

        public override void Load(XElement element)
        {
            var skill = new NotablePersonSkill
            {
                Id = AttributeValue(element, "id"),
                InheritChance = AttributeAsDouble(element, "inherit_chance"),
                Levels = element.Elements("level")
                    .Select(e => new NotablePersonSkillLevel
                    {
                        Level = AttributeAsInt(e, "level"),
                        Name = AttributeValue(e, "name"),
                        Description = AttributeValue(e, "description"),
                        StatBonuses = e.Elements("statbonus")
                            .Select(s => new NotablePersonSkillStatBonus
                            {
                                Stat = AttributeAsEnum<NotablePersonStatType>(s, "stat"),
                                Amount = AttributeAsInt(s, "amount")
                            })
                            .ToArray(),
                        MinorStats = e.Elements("minorbonus")
                            .ToDictionary(s => AttributeValue(s, "stat"),
                                s => AttributeAsDouble(s, "amount")),
                        CultInfluence = e.Elements("influencebonus")
                            .ToDictionary(
                                i => _xmlReaderUtil.ObjectReferenceLookup(
                                    i,
                                    "cult",
                                    _gamedataTracker.Cults,
                                    c => c.Id),
                                i => _xmlReaderUtil.AttributeAsDouble(i, "per_pop"))
                    })
                    .ToArray()
            };

            if (!skill.Levels.Any(l => l.Level == 0))
            {
                throw DataLoadException.MiscError(element, $"Skill {skill.Id} has no definition for level 0.");
            }
            
            _gamedataTracker.Skills.RemoveAll(x => x.Id == skill.Id);
            _gamedataTracker.Skills.Add(skill);
        }

        public override void PostLoad(XElement element)
        {
        }
    }
}