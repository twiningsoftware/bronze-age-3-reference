﻿using System.Linq;
using System.Xml.Linq;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.DataLoaders
{
    public class CellDevelopmentTypePostLinker : DataLoaderBase
    {
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IXmlReaderUtil _xmlReaderUtil;

        public override string ElementName => "cell_development";

        public CellDevelopmentTypePostLinker(
            IGamedataTracker gamedataTracker,
            IXmlReaderUtil xmlReaderUtil)
        {
            _gamedataTracker = gamedataTracker;
            _xmlReaderUtil = xmlReaderUtil;
        }
        
        public override void Load(XElement element)
        {
            var id = AttributeValue(element, "id");

            var cellDevelopmentType = _gamedataTracker.CellDevelopments
                .Where(s => s.Id == id)
                .FirstOrDefault();

            if(cellDevelopmentType != null)
            {
                cellDevelopmentType.PostLink(_gamedataTracker, _xmlReaderUtil, element);
            }
        }

        public override void PostLoad(XElement element)
        {
        }
    }
}
