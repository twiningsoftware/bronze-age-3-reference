﻿using Bronze.Common.Data.World.CellDevelopments;
using Bronze.Common.UI.Modals;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Diplomacy;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.UI;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.NotificationBuilders
{
    public class DiplomaticNotificationBuilder : INotificationHandler, IDiplomaticNotificationBuilder
    {
        private readonly IWorldManager _worldManager;
        private readonly IDialogManager _dialogManager;
        
        public string Name => typeof(DiplomaticNotificationBuilder).Name;

        public DiplomaticNotificationBuilder(
            IWorldManager worldManager,
            IDialogManager dialogManager)
        {
            _worldManager = worldManager;
            _dialogManager = dialogManager;;
        }
        
        public void Handle(Notification notification)
        {
            if (notification.Data.ContainsKey("other_tribe"))
            {
                var otherTribe = _worldManager.Tribes
                    .Where(t => t.Id == notification.Data["other_tribe"])
                    .FirstOrDefault();

                if(otherTribe != null)
                {
                    if (notification.Data["diplo_action"] == DiploConstants.ACTION_TYPE_NEGOTIATE)
                    {
                        _dialogManager.PushModal<DiplomacyNegotiationModal, DiplomacyNegotiationModalContext>(
                            new DiplomacyNegotiationModalContext
                            {
                                OtherTribe = otherTribe,
                                Callback = success => {}
                            });
                    }
                    else
                    {
                        _dialogManager.PushModal<DiplomacyModal, DiplomacyModalContext>(
                            new DiplomacyModalContext
                            {
                                InitialAction = notification.Data["diplo_action"],
                                OtherTribe = otherTribe
                            });
                    }
                }
            }
        }

        public void HandleIgnored(Notification notification)
        {
            if (notification.Data.ContainsKey("other_tribe")
                && notification.Data.ContainsKey("diplo_action")
                && notification.Data["diplo_action"] == DiploConstants.ACTION_TYPE_FIRSTMEET)
            {
                var otherTribe = _worldManager.Tribes
                    .Where(t => t.Id == notification.Data["other_tribe"])
                    .FirstOrDefault();

                // TODO add a negative memory for being ignored
            }
        }

        public Notification BuildDiplomaticDiscoveryNotification(Tribe otherTribe, CellPosition lookAt)
        {
            var data = new Dictionary<string, string>
            {
                { "diplo_action", DiploConstants.ACTION_TYPE_FIRSTMEET },
                { "other_tribe", otherTribe.Id }
            };

            if (lookAt != null && lookAt != default(CellPosition))
            {
                data.Add("look_plain", lookAt.Plane.ToString());
                data.Add("look_x", lookAt.X.ToString());
                data.Add("look_y", lookAt.Y.ToString());
            }

            return new Notification
            {
                AutoOpen = false,
                ExpireDate = _worldManager.Month + 4,
                Handler = typeof(DiplomaticNotificationBuilder).Name,
                Icon = "ui_notification_diplomacy",
                SoundEffect = UiSounds.NotificationMinor,
                QuickDismissable = true,
                DismissOnOpen = true,
                SummaryTitle = $"{otherTribe.Name} Discovered",
                SummaryBody = $"We have discovered a new tribe.",
                Data = data
            };
        }

        public Notification BuildNegotiationRequestNotification(Tribe otherTribe)
        {
            var data = new Dictionary<string, string>
            {
                { "diplo_action", DiploConstants.ACTION_TYPE_NEGOTIATE },
                { "other_tribe", otherTribe.Id }
            };
            
            if(otherTribe.Capital != null)
            {
                var lookAt = otherTribe.Capital
                    .EconomicActors
                    .OfType<ICellDevelopment>()
                    .Where(cd => cd is VillageDevelopment || cd is DistrictDevelopment)
                    .Select(cd => cd.Cell.Position)
                    .FirstOrDefault();

                data.Add("look_plain", lookAt.Plane.ToString());
                data.Add("look_x", lookAt.X.ToString());
                data.Add("look_y", lookAt.Y.ToString());
            }
            
            return new Notification
            {
                AutoOpen = false,
                ExpireDate = _worldManager.Month + 4,
                Handler = typeof(DiplomaticNotificationBuilder).Name,
                Icon = "ui_notification_diplomacy",
                SoundEffect = UiSounds.NotificationMinor,
                QuickDismissable = true,
                DismissOnOpen = true,
                SummaryTitle = $"Offer From {otherTribe.Name}",
                SummaryBody = $"The {otherTribe.Name} have an offer for us.",
                Data = data
            };
        }
    }
}
