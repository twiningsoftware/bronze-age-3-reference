﻿using Bronze.Common.UI.Modals;
using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;
using System.Linq;

namespace Bronze.Common.NotificationBuilders
{
    public class ItemDiscoveryNotificationHandler : INotificationHandler
    {
        public string Name => typeof(ItemDiscoveryNotificationHandler).Name;
        
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IDialogManager _dialogManager;

        public ItemDiscoveryNotificationHandler(
            IGamedataTracker gamedataTracker,
            IDialogManager dialogManager)
        {
            _gamedataTracker = gamedataTracker;
            _dialogManager = dialogManager;
        }

        public void Handle(Notification notification)
        {
            if (notification.Data.ContainsKey("item"))
            {
                var item = _gamedataTracker.Items
                    .Where(i => i.Name == notification.Data["item"])
                    .FirstOrDefault();

                if (item != null)
                {
                    _dialogManager.PushModal<ItemDiscoveryModal, Notification>(notification);
                }
            }
        }
        
        public void HandleIgnored(Notification notification)
        {
        }
    }
}
