﻿using Bronze.Common.UI.Modals;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.UI;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.NotificationBuilders
{
    public class EmpireNotificationBuilder : INotificationHandler, IEmpireNotificationBuilder
    {
        private readonly IWorldManager _worldManager;
        private readonly IDialogManager _dialogManager;
        
        public string Name => typeof(EmpireNotificationBuilder).Name;

        public EmpireNotificationBuilder(
            IWorldManager worldManager,
            IDialogManager dialogManager)
        {
            _worldManager = worldManager;
            _dialogManager = dialogManager;;
        }
        
        public void Handle(Notification notification)
        {
            if(notification.Data.ContainsKey("tribe"))
            {
                var tribe = _worldManager.Tribes
                    .Where(t => t.Id == notification.Data["tribe"])
                    .FirstOrDefault();

                if(tribe != null && notification.Data.ContainsKey("settlement"))
                {
                    var settlement = tribe.Settlements
                        .Where(s => s.Id == notification.Data["settlement"])
                        .FirstOrDefault();

                    if(settlement != null)
                    {
                        _dialogManager.PushModal<GenericNotificationModal, GenericNotificationModalContext>(
                            new GenericNotificationModalContext
                            {
                                Title = $"New Capital for {tribe.Name}",
                                Body = new[]
                                {
                                    $"{settlement.Name} is the new capital of {tribe.Name}"
                                },
                                LookAt = settlement.Districts.FirstOrDefault()?.Cell?.Position
                                    ?? settlement.Region.Cells.FirstOrDefault()?.Position
                            });
                    }
                }
            }
        }

        public void HandleIgnored(Notification notification)
        {
        }

        public Notification BuildCapitalChanged(Tribe tribe, Settlement capital)
        {
            return new Notification
            {
                AutoOpen = false,
                ExpireDate = _worldManager.Month + 2,
                Handler = typeof(EmpireNotificationBuilder).Name,
                Icon = "ui_is_capital",
                SoundEffect = UiSounds.NotificationMinor,
                QuickDismissable = true,
                DismissOnOpen = true,
                SummaryTitle = "New Capital",
                SummaryBody = $"{capital.Name} has become the capital of the {tribe.Name}.",
                Data = new Dictionary<string, string> { { "settlement", capital.Id}, { "tribe", tribe.Id} }
            };
        }
    }
}
