﻿using Bronze.Common.Data.World.CellDevelopments;
using Bronze.Common.Services;
using Bronze.Common.UI.Modals;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.UI;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.NotificationBuilders
{
    public class GenericNotificationBuilder : INotificationHandler, IGenericNotificationBuilder
    {
        private readonly IWorldManager _worldManager;
        private readonly IDialogManager _dialogManager;
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IPlayerDataTracker _playerData;

        public string Name => typeof(GenericNotificationBuilder).Name;

        public GenericNotificationBuilder(
            IWorldManager worldManager,
            IDialogManager dialogManager,
            IGamedataTracker gamedataTracker,
            IPlayerDataTracker playerData)
        {
            _worldManager = worldManager;
            _dialogManager = dialogManager;
            _gamedataTracker = gamedataTracker;
            _playerData = playerData;
        }
        
        public void Handle(Notification notification)
        {
            if(notification.Data.ContainsKey("title")
                && notification.Data.ContainsKey("body"))
            {
                CellPosition? lookAt = null;
                NotablePerson person = null;
                Tribe tribeA = null;
                Tribe tribeB = null;
                NotablePersonSkillPlacement skillPlacement = null;

                if (notification.Data.ContainsKey("look_plain"))
                {
                    lookAt = new CellPosition(
                        int.Parse(notification.Data["look_plain"]),
                        int.Parse(notification.Data["look_x"]),
                        int.Parse(notification.Data["look_y"]));
                }

                if(notification.Data.ContainsKey("person_id"))
                {
                    var personId = notification.Data["person_id"];

                    person = _worldManager.Tribes
                        .Select(t => t.NotablePeople.Where(p => p.Id == personId).FirstOrDefault())
                        .Where(p => p != null)
                        .FirstOrDefault();
                }

                if (notification.Data.ContainsKey("tribe_id"))
                {
                    var tribeId = notification.Data["tribe_id"];

                    tribeA = _worldManager.Tribes
                        .Where(t => t.Id == tribeId)
                        .FirstOrDefault();
                }

                if (notification.Data.ContainsKey("tribe_id2"))
                {
                    var tribeId = notification.Data["tribe_id2"];

                    tribeB = _worldManager.Tribes
                        .Where(t => t.Id == tribeId)
                        .FirstOrDefault();
                }

                if (notification.Data.ContainsKey("skill_id"))
                {
                    var skillId = notification.Data["skill_id"];

                    var skill = _gamedataTracker.Skills
                        .Where(s => s.Id == skillId)
                        .FirstOrDefault();

                    if(skill != null)
                    {
                        var level = skill.Levels
                            .Where(l => l.Level.ToString() == notification.Data["skill_level"])
                            .FirstOrDefault();

                        if(level != null)
                        {
                            skillPlacement = new NotablePersonSkillPlacement
                            {
                                Skill = skill,
                                Level = level
                            };
                        }
                    }
                }

                Buff buff = null;

                if(notification.Data.ContainsKey("settlement_id"))
                {
                    var settlementId = notification.Data["settlement_id"];
                    var settlement = _playerData.PlayerTribe.Settlements
                        .Where(s => s.Id == settlementId)
                        .FirstOrDefault();

                    if(settlement != null && notification.Data.ContainsKey("buff_id"))
                    {
                        var buffId = notification.Data["buff_id"];

                        buff = settlement.Buffs
                            .Where(b => b.Id == buffId)
                            .FirstOrDefault();
                    }
                }

                _dialogManager.PushModal<GenericNotificationModal, GenericNotificationModalContext>(
                    new GenericNotificationModalContext
                    {
                        Title = notification.Data["title"],
                        Body = new[]
                        {
                            notification.Data["body"]
                        },
                        LookAt = lookAt,
                        NotablePerson = person,
                        TribeA = tribeA,
                        TribeB = tribeB,
                        SkillPlacement = skillPlacement,
                        Buff = buff
                    });
            }
        }
        
        public void HandleIgnored(Notification notification)
        {
        }

        public Notification BuildSimpleNotification(
            string icon, 
            string summaryTitle, 
            string summaryBody, 
            string title, 
            string body, 
            CellPosition? lookAt,
            bool autoOpen = false)
        {
            var data = new Dictionary<string, string>
            {
                { "title", title },
                { "body", body }
            };

            if(lookAt != null)
            {
                data.Add("look_plain", lookAt.Value.Plane.ToString());
                data.Add("look_x", lookAt.Value.X.ToString());
                data.Add("look_y", lookAt.Value.Y.ToString());
            }

            return new Notification
            {
                AutoOpen = autoOpen,
                ExpireDate = _worldManager.Month + 2,
                Handler = typeof(GenericNotificationBuilder).Name,
                Icon = icon,
                SoundEffect = UiSounds.NotificationMinor,
                QuickDismissable = true,
                DismissOnOpen = true,
                SummaryTitle = summaryTitle,
                SummaryBody = summaryBody,
                Data = data
            };
        }

        public Notification BuildPersonNotification(
            string icon, 
            string summaryTitle, 
            string summaryBody, 
            string title, 
            string body, 
            NotablePerson person, 
            CellPosition? lookAt)
        {
            var notification = BuildSimpleNotification(icon, summaryTitle, summaryBody, title, body, lookAt);

            notification.Data.Add("person_id", person.Id);

            return notification;
        }

        public Notification BuildTribeNotification(
            string icon,
            string summaryTitle,
            string summaryBody,
            string title,
            string body,
            Tribe tribe)
        {
            var lookAt = tribe?.Capital
                ?.EconomicActors
                ?.OfType<ICellDevelopment>()
                ?.Where(x => x is VillageDevelopment || x is DistrictDevelopment)
                ?.Select(x => x.Cell.Position)
                ?.FirstOrDefault();

            var notification = BuildSimpleNotification(icon, summaryTitle, summaryBody, title, body, lookAt);

            notification.Data.Add("tribe_id", tribe.Id);

            return notification;
        }

        public Notification BuildTribeNotification(
            string icon,
            string summaryTitle,
            string summaryBody,
            string title,
            string body,
            Tribe tribeA,
            Tribe tribeB)
        {
            var notification = BuildSimpleNotification(icon, summaryTitle, summaryBody, title, body, null);

            notification.Data.Add("tribe_id", tribeA.Id);

            notification.Data.Add("tribe_id2", tribeB.Id);

            return notification;
        }

        public Notification BuildArmyNotification(
            string icon,
            string summaryTitle,
            string summaryBody,
            string title,
            string body,
            Army army)
        {
            var notification = BuildSimpleNotification(icon, summaryTitle, summaryBody, title, body, army.Cell.Position);
            
            return notification;
        }

        public Notification BuildPersonSkillNotification(
            string icon, 
            string summaryTitle, 
            string summaryBody, 
            string title, 
            string body, 
            NotablePerson person, 
            NotablePersonSkillPlacement skillPlacement,
            CellPosition? lookAt)
        {
            var notification = BuildSimpleNotification(icon, summaryTitle, summaryBody, title, body, lookAt);

            notification.Data.Add("person_id", person.Id);
            notification.Data.Add("skill_id", skillPlacement.Skill.Id);
            notification.Data.Add("skill_level", skillPlacement.Level.Level.ToString());

            return notification;
        }

        public Notification BuildBuffNotification(
            string icon, 
            string summaryTitle, 
            string summaryBody, 
            string title, 
            string body, 
            Settlement settlement, 
            Buff buff, 
            bool autoOpen = false)
        {
            var notification = BuildSimpleNotification(
                icon, 
                summaryTitle, 
                summaryBody, 
                title, 
                body,
                settlement.EconomicActors.OfType<ICellDevelopment>()
                        .Where(cd => cd is VillageDevelopment || cd is DistrictDevelopment)
                        .Select(cd => cd.Cell.Position)
                        .FirstOrDefault());

            notification.Data.Add("settlement_id", settlement.Id);
            notification.Data.Add("buff_id", buff.Id);

            if (buff.ExpireMonth > 0)
            {
                notification.ExpireDate = Math.Min(notification.ExpireDate, buff.ExpireMonth);
            }
            
            return notification;
        }

        public Notification BuildLookAtNotification(
            string icon, 
            string summaryTitle, 
            CellPosition lookAt)
        {
            return new Notification
            {
                AutoOpen = false,
                ExpireDate = _worldManager.Month + 2,
                Handler = typeof(LookAtNotificationHandler).Name,
                Icon = icon,
                SoundEffect = UiSounds.NotificationMinor,
                QuickDismissable = true,
                DismissOnOpen = true,
                SummaryTitle = summaryTitle,
                SummaryBody = "Click to zoom to location.",
                Data = new Dictionary<string, string>
                {
                    { "look_plain", lookAt.Plane.ToString() },
                    { "look_x", lookAt.X.ToString()},
                    { "look_y", lookAt.Y.ToString()}
                }
            };
        }
    }
}
