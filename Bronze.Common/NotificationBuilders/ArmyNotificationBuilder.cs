﻿using Bronze.Common.UI.Modals;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Diplomacy;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.UI;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.NotificationBuilders
{
    public class ArmyNotificationBuilder : IArmyNotificationBuilder, INotificationHandler
    {
        private readonly IPlayerDataTracker _playerData;
        private readonly IGenericNotificationBuilder _genericNotificationBuilder;
        private readonly IWorldManager _worldManager;
        private readonly IDialogManager _dialogManager;
        
        public ArmyNotificationBuilder(
            IGenericNotificationBuilder genericNotificationBuilder,
            IPlayerDataTracker playerData,
            IWorldManager worldManager,
            IDialogManager dialogManager)
        {
            _genericNotificationBuilder = genericNotificationBuilder;
            _playerData = playerData;
            _worldManager = worldManager;
            _dialogManager = dialogManager;
        }
        
        public string Name => typeof(ArmyNotificationBuilder).Name;

        public Notification BuildArmyDestroyedNotification(Army army, CellPosition position, bool generalDied, bool wasTrapped)
        {
            var body = $"{army.Name} has been destroyed.";

            if (generalDied)
            {
                body += $"\nGeneral {army.General.Name} was unable to excape, and has died.";
            }
            else if(army.General != null)
            {
                body += $"\nGeneral {army.General.Name} managed to escape and is available for other duties.";
            }

            return _genericNotificationBuilder.BuildSimpleNotification(
                "ui_notification_armydestroyed",
                $"{army.Name} Destroyed",
                "An army has been destroyed.",
                $"{army.Name} Destroyed",
                body,
                position);
        }

        public Notification BuildArmyLostNotification(Army army, CellPosition position)
        {
            var body = $"{army.Name} no longer has any units in it, and so the army has been disbanded.";

            if(army.General != null)
            {
                body += $"\nGeneral {army.General.Name} has returned home and is available for other duties.";
            }

            return _genericNotificationBuilder.BuildSimpleNotification(
                "ui_notification_armygone",
                $"{army.Name} Disbanded",
                "An army has been disbanded.",
                $"{army.Name} Disbanded",
                body,
                position);
        }

        public Notification BuildAttritionDeathNotification(string locationName, IUnit unit, CellPosition? position)
        {
            return _genericNotificationBuilder.BuildSimpleNotification(
                "ui_notification_attritiondeath",
                $"Attrition in {locationName}",
                $"A unit in {locationName} has died.",
                $"Attrition in {locationName}",
                $"Due to lack of supplies, a {unit.Name} in {locationName} has been lost.",
                position);
        }

        public Notification BuildBattleNotification(string battleName, Cell location, PitchedBattleResult battleResult)
        {
            var playerAggressor = battleResult.Attackers.Any(a => a.Owner == _playerData.PlayerTribe);

            var scoreForPlayer = playerAggressor ? battleResult.AttackerScore : battleResult.DefenderScore;

            var playerWon = playerAggressor ? battleResult.AttackerWon : battleResult.DefenderWon;
            var playerLost = playerAggressor ? battleResult.DefenderWon : battleResult.AttackerWon;

            var allies = (playerAggressor ? battleResult.Attackers : battleResult.Defenders)
                .Select(a => a.Owner)
                .Distinct();

            var enemies = (playerAggressor ? battleResult.Defenders : battleResult.Attackers)
                .Select(a => a.Owner)
                .Distinct();

            var fraction = scoreForPlayer / Math.Max(1, battleResult.AttackerScore + battleResult.DefenderScore);

            string title;
            string summary;
            string body;
            string iconKey;

            var otherTribe = enemies.First();
            
            if (fraction < 0.3 && playerLost)
            {
                title = "Crushing Defeat";
                iconKey = "ui_notification_battleloss";
                summary = $"Defeat at the {battleName}";
                body = $"{otherTribe.Name} have delt us a crushing defeat at the {battleName}, hopefully we can somehow manage to recover.";
            }
            else if (fraction < 0.4 && playerLost)
            {
                title = "Decisive Defeat";
                iconKey = "ui_notification_battleloss";
                summary = $"Defeat at the {battleName}";
                body = $"We have been decisively defeated by {otherTribe.Name} in the {battleName}. We cannot afford many such losses.";
            }
            else if (playerLost)
            {
                title = "Narrow Defeat";
                iconKey = "ui_notification_battleloss";
                summary = $"Defeat at the {battleName}";
                body = $"We have suffered a narrow defeat in the {battleName} at the hands of {otherTribe.Name}. We cannot let them gain the upper hand.";
            }
            else if (fraction < 0.5 && playerWon)
            {
                title = "Pyrrhic Victory";
                iconKey = "ui_notification_battlewin";
                summary = $"Pyrrhic victory at the {battleName}";
                body = $"Despite our losses, we have managed to win victory in the {battleName}. Any more such victories may lose us the war.";
            }
            
            else if (fraction < 0.6 && playerWon)
            {
                title = "Narrow Victory";
                iconKey = "ui_notification_battlewin";
                summary = $"Victory at the {battleName}";
                body = $"We have narrowly defeated {otherTribe.Name} at the {battleName}, we must press the attack and not let them recover.";
            }
            else if (fraction < 0.7 && playerWon)
            {
                title = "Decisive Victory";
                iconKey = "ui_notification_battlewin";
                summary = $"Victory at the {battleName}";
                body = $"We have decisively defeated {otherTribe.Name} at the {battleName}. They cannot afford many such losses.";
            }
            else if (playerWon)
            {
                title = "Crushing Victory";
                iconKey = "ui_notification_battlewin";
                summary = $"Victory at the {battleName}";
                body = $"We have crushed {otherTribe.Name} at the {battleName}, victory is within our grasp!";
            }
            else
            {
                title = "Indecisive Battle";
                iconKey = "ui_notification_battledraw";
                summary = $"Stalemate at the {battleName}";
                body = $"The {battleName} has ended indecisively, now is the time to gain the momentum against the {otherTribe.Name}.";
            }

            var data = new Dictionary<string, string>
            {
                { "title", title },
                { "body", body },
                { "look_plain", location.Position.Plane.ToString() },
                { "look_x", location.Position.X.ToString() },
                { "look_y", location.Position.Y.ToString() }
            };

            if(playerAggressor)
            {
                data.Add("ally_leaders", string.Join(",", battleResult.AttackerLeaders.Select(l => l.Id)));
                data.Add("enemy_leaders", string.Join(",", battleResult.DefenderLeaders.Select(l => l.Id)));
                data.Add("ally_survivors", string.Join(",", battleResult.SurvivingAttackers.Select(u => u.UnitType.Id)));
                data.Add("ally_dead", string.Join(",", battleResult.LostAttackers.Select(u => u.UnitType.Id)));
                data.Add("enemy_survivors", string.Join(",", battleResult.SurvivingDefenders.Select(u => u.UnitType.Id)));
                data.Add("enemy_dead", string.Join(",", battleResult.LostDefenders.Select(u => u.UnitType.Id)));
                data.Add("ally_strength_before", battleResult.AttackerBeforeStrength.ToString("N0"));
                data.Add("ally_strength_after", battleResult.AttackerAfterStrength.ToString("N0"));
                data.Add("enemy_strength_before", battleResult.DefenderBeforeStrength.ToString("N0"));
                data.Add("enemy_strength_after", battleResult.DefenderAfterStrength.ToString("N0"));
                data.Add("ally_score", scoreForPlayer.ToString("N0"));
            }
            else
            {
                data.Add("enemy_leaders", string.Join(",", battleResult.AttackerLeaders.Select(l => l.Id)));
                data.Add("ally_leaders", string.Join(",", battleResult.DefenderLeaders.Select(l => l.Id)));
                data.Add("enemy_survivors", string.Join(",", battleResult.SurvivingAttackers.Select(u => u.UnitType.Id)));
                data.Add("enemy_dead", string.Join(",", battleResult.LostAttackers.Select(u => u.UnitType.Id)));
                data.Add("ally_survivors", string.Join(",", battleResult.SurvivingDefenders.Select(u => u.UnitType.Id)));
                data.Add("ally_dead", string.Join(",", battleResult.LostDefenders.Select(u => u.UnitType.Id)));
                data.Add("enemy_strength_before", battleResult.AttackerBeforeStrength.ToString("N0"));
                data.Add("enemy_strength_after", battleResult.AttackerAfterStrength.ToString("N0"));
                data.Add("ally_strength_before", battleResult.DefenderBeforeStrength.ToString("N0"));
                data.Add("ally_strength_after", battleResult.DefenderAfterStrength.ToString("N0"));
            }
            
            return new Notification
            {
                AutoOpen = false,
                ExpireDate = _worldManager.Month + 2,
                Handler = typeof(ArmyNotificationBuilder).Name,
                Icon = iconKey,
                SoundEffect = UiSounds.NotificationMinor,
                QuickDismissable = true,
                DismissOnOpen = true,
                SummaryTitle = title,
                SummaryBody = summary,
                Data = data
            };
        }

        public Notification BuildSiegeAssaultNotification(
            string battleName, 
            Cell settlementLocation, 
            PitchedBattleResult battleResult, 
            bool settlementCaptured, 
            bool playerAttacker, 
            Settlement settlement)
        {
            var playerAggressor = battleResult.Attackers.Any(a => a.Owner == _playerData.PlayerTribe);

            var scoreForPlayer = playerAggressor ? battleResult.AttackerScore : battleResult.DefenderScore;

            var playerWon = playerAggressor ? battleResult.AttackerWon : battleResult.DefenderWon;
            var playerLost = playerAggressor ? battleResult.DefenderWon : battleResult.AttackerWon;

            var allies = (playerAggressor ? battleResult.Attackers : battleResult.Defenders)
                .Select(a => a.Owner)
                .Distinct();

            var enemies = (playerAggressor ? battleResult.Defenders : battleResult.Attackers)
                .Select(a => a.Owner)
                .Distinct();

            var fraction = scoreForPlayer / Math.Max(1, battleResult.AttackerScore + battleResult.DefenderScore);

            string title;
            string summary;
            string body;
            string iconKey;
            
            if(settlementCaptured && playerAggressor)
            {
                title = "Settlement Captured";
                iconKey = "ui_notification_battlewin";
                summary = $"{settlement.Name} Captured!";
                body = $"Our armies have captured {settlement.Name}.";
            }
            else if(!settlementCaptured && playerAggressor)
            {
                title = "Assault Repelled";
                iconKey = "ui_notification_battleloss";
                summary = $"Assault on {settlement.Name} was repelled.";
                body = $"Our armies have failed to capture {settlement.Name}.";
            }
            else if (settlementCaptured && !playerAggressor)
            {
                title = "Settlement Lost";
                iconKey = "ui_notification_battleloss";
                summary = $"{settlement.Name} Lost!";
                body = $"Our armies have been defeated, and {settlement.Name} has been captured!";
            }
            else
            {
                title = "Settlement Defenfed";
                iconKey = "ui_notification_battlewin";
                summary = $"Assault on {settlement.Name} defended.";
                body = $"Our armies have failed to capture {settlement.Name}.";
            }
            
            var data = new Dictionary<string, string>
            {
                { "title", title },
                { "body", body },
                { "look_plain", settlementLocation.Position.Plane.ToString() },
                { "look_x", settlementLocation.Position.X.ToString() },
                { "look_y", settlementLocation.Position.Y.ToString() }
            };

            if (playerAggressor)
            {
                data.Add("ally_leaders", string.Join(",", battleResult.AttackerLeaders.Select(l => l.Id)));
                data.Add("enemy_leaders", string.Join(",", battleResult.DefenderLeaders.Select(l => l.Id)));
                data.Add("ally_survivors", string.Join(",", battleResult.SurvivingAttackers.Select(u => u.UnitType.Id)));
                data.Add("ally_dead", string.Join(",", battleResult.LostAttackers.Select(u => u.UnitType.Id)));
                data.Add("enemy_survivors", string.Join(",", battleResult.SurvivingDefenders.Select(u => u.UnitType.Id)));
                data.Add("enemy_dead", string.Join(",", battleResult.LostDefenders.Select(u => u.UnitType.Id)));
                data.Add("ally_strength_before", battleResult.AttackerBeforeStrength.ToString("N0"));
                data.Add("ally_strength_after", battleResult.AttackerAfterStrength.ToString("N0"));
                data.Add("enemy_strength_before", battleResult.DefenderBeforeStrength.ToString("N0"));
                data.Add("enemy_strength_after", battleResult.DefenderAfterStrength.ToString("N0"));
            }
            else
            {
                data.Add("enemy_leaders", string.Join(",", battleResult.AttackerLeaders.Select(l => l.Id)));
                data.Add("ally_leaders", string.Join(",", battleResult.DefenderLeaders.Select(l => l.Id)));
                data.Add("enemy_survivors", string.Join(",", battleResult.SurvivingAttackers.Select(u => u.UnitType.Id)));
                data.Add("enemy_dead", string.Join(",", battleResult.LostAttackers.Select(u => u.UnitType.Id)));
                data.Add("ally_survivors", string.Join(",", battleResult.SurvivingDefenders.Select(u => u.UnitType.Id)));
                data.Add("ally_dead", string.Join(",", battleResult.LostDefenders.Select(u => u.UnitType.Id)));
                data.Add("enemy_strength_before", battleResult.AttackerBeforeStrength.ToString("N0"));
                data.Add("enemy_strength_after", battleResult.AttackerAfterStrength.ToString("N0"));
                data.Add("ally_strength_before", battleResult.DefenderBeforeStrength.ToString("N0"));
                data.Add("ally_strength_after", battleResult.DefenderAfterStrength.ToString("N0"));
            }

            return new Notification
            {
                AutoOpen = false,
                ExpireDate = _worldManager.Month + 2,
                Handler = typeof(ArmyNotificationBuilder).Name,
                Icon = iconKey,
                SoundEffect = UiSounds.NotificationMinor,
                QuickDismissable = true,
                DismissOnOpen = true,
                SummaryTitle = title,
                SummaryBody = summary,
                Data = data
            };
        }

        public Notification BuildUnitWentHomeNotification(string locationName, IUnit unit, CellPosition? position)
        {
            return _genericNotificationBuilder.BuildSimpleNotification(
                "ui_notification_gonehome",
                $"Desertion in {locationName}",
                $"A unit in {locationName} has deserted.",
                $"Desertion in {locationName}",
                $"A {unit.Name} in {locationName} has served too long, and has deserted.",
                position);
        }

        public void Handle(Notification notification)
        {
            var lookAt = new CellPosition(
                    int.Parse(notification.Data["look_plain"]),
                    int.Parse(notification.Data["look_x"]),
                    int.Parse(notification.Data["look_y"]));

            var allPeople = _worldManager.Tribes.SelectMany(t => t.NotablePeople).ToArray();

            var allyLeaders = notification.Data["ally_leaders"]
                .Split(',')
                .Select(id => allPeople.Where(p => p.Id == id).FirstOrDefault())
                .Where(p => p != null)
                .ToArray();

            var enemyLeaders = notification.Data["enemy_leaders"]
                .Split(',')
                .Select(id => allPeople.Where(p => p.Id == id).FirstOrDefault())
                .Where(p => p != null)
                .ToArray();

            var allySurvivors = notification.Data["ally_survivors"]
                .Split(',')
                .Where(s => !string.IsNullOrWhiteSpace(s))
                .Count();

            var allyDead = notification.Data["ally_dead"]
                .Split(',')
                .Where(s => !string.IsNullOrWhiteSpace(s))
                .Count();

            var enemySurvivors = notification.Data["enemy_survivors"]
                .Split(',')
                .Where(s => !string.IsNullOrWhiteSpace(s))
                .Count();

            var enemyDead = notification.Data["enemy_dead"]
                .Split(',')
                .Where(s => !string.IsNullOrWhiteSpace(s))
                .Count();

            _dialogManager.PushModal<BattleResultNotificationModal, BattleResultNotificationModalContext>(
                new BattleResultNotificationModalContext
                {
                    Title = notification.Data["title"],
                    Body = new[]
                    {
                        notification.Data["body"]
                    },
                    LookAt = lookAt,
                    AllySurvivors = allySurvivors,
                    EnemySurvivors = enemySurvivors,
                    AllyDead = allyDead,
                    EnemyDead = enemyDead,
                    AllyLeaders = allyLeaders,
                    EnemyLeaders = enemyLeaders,
                    AllyStrengthBefore = int.Parse(notification.Data["ally_strength_before"]),
                    AllyStrengthAfter = int.Parse(notification.Data["ally_strength_after"]),
                    EnemyStrengthBefore = int.Parse(notification.Data["enemy_strength_before"]),
                    EnemyStrengthAfter = int.Parse(notification.Data["enemy_strength_after"])
                });
        }

        public void HandleIgnored(Notification notification)
        {
        }
    }
}
