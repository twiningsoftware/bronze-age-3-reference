﻿using Bronze.Common.ConsoleCommands;
using Bronze.Common.Data.Diplomacy;
using Bronze.Common.Data.Dungeons;
using Bronze.Common.Data.Dungeons.ResolutionActions;
using Bronze.Common.Data.Game.AI;
using Bronze.Common.Data.Game.AI.SettlementRoles;
using Bronze.Common.Data.Game.CellDevelopments;
using Bronze.Common.Data.Game.CellFeatures;
using Bronze.Common.Data.Game.Rites;
using Bronze.Common.Data.Game.Structures;
using Bronze.Common.Data.Game.Units;
using Bronze.Common.Data.Generation;
using Bronze.Common.Data.World.AI;
using Bronze.Common.Data.World.CellDevelopments;
using Bronze.Common.Data.World.Structures;
using Bronze.Common.Data.World.Units;
using Bronze.Common.DataLoaders;
using Bronze.Common.NotificationBuilders;
using Bronze.Common.Services;
using Bronze.Common.Simulation;
using Bronze.Common.Simulation.Crises;
using Bronze.Common.Simulation.NotablePersonEvents;
using Bronze.Common.Simulation.SettlementEvents;
using Bronze.Common.UI.MapInteraction;
using Bronze.Common.UI.Modals;
using Bronze.Common.UI.Overlays;
using Bronze.Common.UI.Widgets;
using Bronze.Contracts;
using Bronze.Contracts.Data.Dungeons;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Generation;
using Bronze.UI.Modals;

namespace Bronze.Common
{
    public class CommonInjectionModule : IInjectionModule
    {
        public void RegisterInjections(IInjector injector)
        {
            // Data Loaders (Order is Important)
            injector.RegisterService<DataMutatorLoader>();
            injector.RegisterService<UnitCategoryDataLoader>();
            injector.RegisterService<RecruitmentSlotCategoryDataLoader>();
            injector.RegisterService<ItemDataLoader>();
            injector.RegisterService<BonusTypeDataLoader>();
            injector.RegisterService<CultDataLoader>();
            injector.RegisterService<PeoplePartDataLoader>();
            injector.RegisterService<NotablePersonSkillDataLoader>();
            injector.RegisterService<ServiceTypesDataLoader>();
            injector.RegisterService<TraitDataLoader>();
            injector.RegisterService<RecipieDataLoader>();
            injector.RegisterService<MovementTypeDataLoader>();
            injector.RegisterService<HaulerInfoDataLoader>();
            injector.RegisterService<TerrainDataLoader>();
            injector.RegisterService<DecorationDataLoader>();
            injector.RegisterService<CellFeatureDataLoader>();
            injector.RegisterService<BiomeDataLoader>();
            injector.RegisterService<TransportTypeDataLoader>();
            injector.RegisterService<RaceDataLoader>();
            injector.RegisterService<CellDevelopmentTypeDataLoader>();
            injector.RegisterService<CellDevelopmentTypePostLinker>();
            injector.RegisterService<StructureTypeDataLoader>();
            injector.RegisterService<StructureTypePostLinker>();
            injector.RegisterService<UnitTypeDataLoader>();
            injector.RegisterService<HelpPageLoader>();
            injector.RegisterService<RiverTypeDataLoader>();
            injector.RegisterService<MidpathTypeDataLoader>();
            injector.RegisterService<TribeControllerTypeDataLoader>();
            injector.RegisterService<WorldTypeDataLoader>();
            injector.RegisterService<CityPrefabDataLoader>();
            injector.RegisterService<CityPrefabPlacer>();
            injector.RegisterService<DungeonDataLoader>();
            injector.RegisterService<DungeonCellFeaturePostLinker>();

            // Services
            injector.RegisterService<DistrictGenerator>();
            injector.RegisterService<EmpireNotificationBuilder>();
            injector.RegisterService<ItemDiscoveryNotificationHandler>();
            injector.RegisterService<GamedataTracker>();
            injector.RegisterService<GameWorldDrawer>();
            injector.RegisterService<IrrigationCalculator>();
            injector.RegisterService<LosTracker>();
            injector.RegisterService<NotificationsSystem>();
            injector.RegisterService<PlayerDataTracker>();
            injector.RegisterService<RegionFeaturePlacer>();
            injector.RegisterService<SettlementCalculator>();
            injector.RegisterService<SettlementSimulator>();
            injector.RegisterService<TilePather>();
            injector.RegisterService<TradeManager>();
            injector.RegisterService<UnitHelper>();
            injector.RegisterService<UnmNameSource>();
            injector.RegisterService<WorldGenerator>();
            injector.RegisterService<WorldSerializer>();
            injector.RegisterService<WorldSimulator>();
            injector.RegisterService<XmlReaderUtil>();
            injector.RegisterService<HelpManager>();
            injector.RegisterService<VoroniRegionGenerator>();
            injector.RegisterService<AiManager>();
            injector.RegisterService<AiSpy>();
            injector.RegisterService<GraphBasedSettlementLogiCalculator>();
            injector.RegisterService<GenericNotificationBuilder>();
            injector.RegisterService<PeopleHelper>();
            injector.RegisterService<DiplomaticDiscoveryWatcher>();
            injector.RegisterService<DiplomaticNotificationBuilder>();
            injector.RegisterService<DiplomacyHelper>();
            injector.RegisterService<WarHelper>();
            injector.RegisterService<WarNotificationHandler>();
            injector.RegisterService<TreatyHelper>();
            injector.RegisterService<ArmyNotificationBuilder>();
            injector.RegisterService<HasAttritionNotificationHandler>();
            injector.RegisterService<AttritionWatcher>();
            injector.RegisterService<BattleHelper>();
            injector.RegisterService<ArmyEngagedNotificationHandler>();
            injector.RegisterService<CombatHelper>();
            injector.RegisterService<CombatAutoresolver>();
            injector.RegisterService<SimulationEventBus>();
            injector.RegisterService<TribeExtinctionWatcher>();
            injector.RegisterService<SettlementCalculator>();
            injector.RegisterService<AiHelper>();
            injector.RegisterService<SiegingNotificationHandler>();
            injector.RegisterService<SiegedNotificationHandler>();
            injector.RegisterService<PeopleGenerator>();
            injector.RegisterService<EnemyAlertWatcher>();
            injector.RegisterService<NewSettlementWatcher>();
            injector.RegisterService<LookAtNotificationHandler>();
            injector.RegisterService<HumitteTutorialWatcher>();
            injector.RegisterService<CreepBanditManager>();
            injector.RegisterService<WorldEffectManager>();
            injector.RegisterService<GrazingManager>();
            injector.RegisterService<HerdSpawner>();
            injector.RegisterService<DiplomacyBonusWatcher>();
            injector.RegisterService<SiegeDebuffWatcher>();
            injector.RegisterService<MarriageOptionNotificationBuilder>();
            injector.RegisterService<DungeonDataManager>();
            injector.RegisterService<RunDungeonCommand>();
            injector.RegisterService<DungeonDiscoveryWatcher>();
            injector.RegisterService<CultInfluenceSimulator>();

            // Modals
            injector.RegisterDialog<EscapeMenuModal>();
            injector.RegisterDialog<ConfirmModal>();
            injector.RegisterDialog<ListOfStringsModal>();
            injector.RegisterDialog<HelpModal>();
            injector.RegisterDialog<GameoverModal>();
            injector.RegisterDialog<CellDetailsModal>();
            injector.RegisterDialog<TileDetailsModal>();
            injector.RegisterDialog<ConstructionInfoModal>();
            injector.RegisterDialog<RecruitmentModal>();
            injector.RegisterDialog<EmpireEconomyModal>();
            injector.RegisterDialog<SettlementTradeModal>();
            injector.RegisterDialog<TradeRouteEditorModal>();
            injector.RegisterDialog<ItemDiscoveryModal>();
            injector.RegisterDialog<ColorPickerModal>();
            injector.RegisterDialog<GenericNotificationModal>();
            injector.RegisterDialog<DynastyInfoModal>();
            injector.RegisterDialog<NotablePersonDetailsModal>();
            injector.RegisterDialog<NotablePersonPickerModal>();
            injector.RegisterDialog<DiplomacyModal>();
            injector.RegisterDialog<DiplomacyListModal>();
            injector.RegisterDialog<WarStatusModal>();
            injector.RegisterDialog<DiplomacyNegotiationModal>();
            injector.RegisterDialog<ArmySplitModal>();
            injector.RegisterDialog<HasAttritionModal>();
            injector.RegisterDialog<UnitInfoModal>();
            injector.RegisterDialog<ArmyMergeModal>();
            injector.RegisterDialog<ArmiesEngagedModal>();
            injector.RegisterDialog<BattleResultNotificationModal>();
            injector.RegisterDialog<SiegeInfoModal>();
            injector.RegisterDialog<UnitEquipmentModal>();
            injector.RegisterDialog<CreepCampTalkModal>();
            injector.RegisterDialog<PickerModal>();
            injector.RegisterDialog<TradeActorPickerModal>();
            injector.RegisterDialog<TradeRecieverPickerModal>();
            injector.RegisterDialog<TradeTreatyEditorModal>();
            injector.RegisterDialog<TradeSettlementTreatyEditorModal>();
            injector.RegisterDialog<MarriageTreatyBuilderModal>();
            injector.RegisterDialog<MarriageOptionModal>();
            injector.RegisterDialog<RunDungeonDialog>();

            // Map Interaction Components
            injector.RegisterService<MapScrolling>();
            injector.RegisterService<MouseSelector>();
            injector.RegisterService<ViewModeSwitcher>();
            injector.RegisterService<LogiPathDrawer>();
            injector.RegisterService<ServiceProviderHighlighter>();
            injector.RegisterService<StructureIconDrawer>();
            injector.RegisterService<ArmyOrderIssuer>();
            injector.RegisterService<MovementArrowDrawer>();
            injector.RegisterService<TradePathDrawer>();
            injector.RegisterService<TradeRouteBorderPathDrawer>();
            injector.RegisterService<StructurePlacer>();
            injector.RegisterService<StructureDemolisher>();
            injector.RegisterService<DevelopmentPlacer>();
            injector.RegisterService<DevelopmentDemolisher>();
            injector.RegisterService<CultRiteTargeter>();

            // Overlays
            injector.RegisterService<LogiCongestionOverlay>();

            // Cell Development Types
            injector.RegisterDevelopmentType<DistrictDevelopmentType, DistrictDevelopment>();
            injector.RegisterDevelopmentType<SimpleDevelopmentType, SimpleDevelopment>();
            injector.RegisterDevelopmentType<ClearFeatureDevelopmentType, ClearFeatureDevelopment>();
            injector.RegisterDevelopmentType<VillageDevelopmentType, VillageDevelopment>();
            injector.RegisterDevelopmentType<TraderDevelopmentType, TraderDevelopment>();
            injector.RegisterDevelopmentType<IrrigationDevelopmentType, IrrigationDevelopment>();
            injector.RegisterDevelopmentType<TradeRecieverDevelopmentType, TradeRecieverDevelopment>();
            injector.RegisterDevelopmentType<TraderAndTransportDevelopmentType, TraderAndTransportDevelopment>();
            injector.RegisterDevelopmentType<TransportAnchorageDevelopmentType, TransportAnchorageDevelopment>();
            injector.RegisterDevelopmentType<BarracksDevelopmentType, BarracksDevelopment>();
            injector.RegisterDevelopmentType<CreepCampDevelopmentType, CreepCampDevelopment>();
            injector.RegisterDevelopmentType<CouplingDevelopmentType, CouplingDevelopment>();
            injector.RegisterDevelopmentType<CreepAttackableDevelopmentType, CreepAttackableDevelopment>();
            injector.RegisterDevelopmentType<DungeonDevelopmentType, DungeonDevelopment>();
            injector.RegisterDevelopmentType<ShrineDevelopmentType, ShrineDevelopment>();

            // Structure Types
            injector.RegisterStructureType<HousingStructureType, HousingStructure>();
            injector.RegisterStructureType<SimpleStructureType, SimpleStructure>();
            injector.RegisterStructureType<DistributorStructureType, DistributorStructure>();
            injector.RegisterStructureType<TaxCollectorStructureType, TaxCollectorStructure>();
            injector.RegisterStructureType<BarracksStructureType, BarracksStructure>();
            injector.RegisterStructureType<TraderStructureType, TraderStructure>();
            injector.RegisterStructureType<SimpleServiceProviderStructureType, SimpleServiceProviderStructure>();
            injector.RegisterStructureType<UpgraderStructureType, UpgraderStructure>();
            injector.RegisterStructureType<SupportUpgradableStructureType, SupportUpgradableStructure>();
            injector.RegisterStructureType<TradeRecieverStructureType, TradeRecieverStructure>();
            injector.RegisterStructureType<ItemStorageStructureType, ItemStorageStructure>();
            injector.RegisterStructureType<AdministratorStructureType, AdministratorStructure>();
            injector.RegisterStructureType<TempleStructureType, TempleStructure>();

            // Tribe Controllers
            injector.RegisterControllerType<BasicTribeControllerType, BasicTribeController>();
            injector.RegisterControllerType<BanditTribeControllerType, BanditTribeController>();

            // Unit Types
            injector.RegisterUnitType<SimpleUnitType, SimpleUnit>();
            injector.RegisterUnitType<GrazingUnitType, GrazingUnit>();

            // Transport Types
            injector.RegisterTransportType<BoatsTransportType>();

            // Army Actions
            injector.RegisterNamedType<ArmySettleAction, IArmyAction>();
            injector.RegisterNamedType<ArmyIdleAction, IArmyAction>();
            injector.RegisterNamedType<ArmyBuildRoadAction, IArmyAction>();
            injector.RegisterNamedType<ArmyCampAction, IArmyAction>();
            injector.RegisterNamedType<ArmyInteractAction, IArmyAction>();
            injector.RegisterNamedType<ArmyDisembarkAction, IArmyAction>();
            injector.RegisterNamedType<ArmyRaidAction, IArmyAction>();
            injector.RegisterNamedType<ArmyApproachAction, IArmyAction>();
            injector.RegisterNamedType<ArmyMergeAction, IArmyAction>();
            injector.RegisterNamedType<ArmySkirmishAction, IArmyAction>();
            injector.RegisterNamedType<ArmyRecoverAction, IArmyAction>();
            injector.RegisterNamedType<ArmyEatCorpseAction, IArmyAction>();

            // Cell Feature Types
            injector.RegisterNamedType<SimpleCellFeature, ICellFeature>();
            injector.RegisterNamedType<RiverCellFeature, ICellFeature>();

            // Seed Point Types
            injector.RegisterNamedType<RegionBlobSeedPoint, ISeedPoint>();
            injector.RegisterNamedType<InclusionSeedPoint, ISeedPoint>();
            injector.RegisterNamedType<CouplingSeedPoint, ISeedPoint>();
            injector.RegisterNamedType<EmpirePlacerSeedPoint, ISeedPoint>();
            injector.RegisterNamedType<RegionBlobWithCoreSeedPoint, ISeedPoint>();

            // Simulation Addons
            injector.RegisterService<ServiceProviderStructureSubCalculator>();
            injector.RegisterService<ServiceProviderDevelopmentSubCalculator>();
            injector.RegisterService<TraderToDistrictBorderTradePathCalculator>();
            injector.RegisterService<ItemDiscoveryWatcher>();
            injector.RegisterService<TradeRecieverToDistrictBorderTradePathCalculator>();
            injector.RegisterService<SettlementTraitMonitor>();
            injector.RegisterService<RulerAndHeirWatcher>();
            injector.RegisterService<ArmySkirmishInterrupter>();
            injector.RegisterService<WorldCorpseManager>();
            injector.RegisterService<RoadConnectivityChecker>();
            injector.RegisterService<NotablePersonSpousegenEvent>();

            // Starting People Schemes
            injector.RegisterNamedType<StandardFamilyPeopleScheme, IStartingPeopleScheme>();
            
            // Widgets
            injector.RegisterWidget<GameplayButtonRowWidget>();
            injector.RegisterWidget<ConsoleWindowWidget>();
            injector.RegisterWidget<PerformanceWindowWidget>();
            injector.RegisterWidget<GamePausedIndicator>();
            injector.RegisterWidget<CellHoverInfoWidget>();
            injector.RegisterWidget<TileHoverInfoWidget>();
            injector.RegisterWidget<EmpireOverviewWidget>();
            injector.RegisterWidget<SettlementMinimapWidget>();
            injector.RegisterWidget<TimeControlWidget>();
            injector.RegisterWidget<RegionManagementWidget>();
            injector.RegisterWidget<SettlementManagementWidget>();
            injector.RegisterWidget<PlacementMessagesWidget>();
            injector.RegisterWidget<OverlayPickerWidget>();
            injector.RegisterWidget<CellActivitiesWidget>();
            injector.RegisterWidget<CellActivitiesPreviewWidget>();
            injector.RegisterWidget<ArmyInfoWidget>();
            injector.RegisterWidget<IssueArmyOrderWidget>();
            injector.RegisterWidget<ArmyHoverInfoWidget>();
            injector.RegisterWidget<NotificationsWidget>();
            injector.RegisterWidget<DevelopmentRecipieSwitchWidget>();
            injector.RegisterWidget<StructureRecipieSwitchWidget>();
            injector.RegisterWidget<PlaneSwitcherWidget>();
            injector.RegisterWidget<WorldMinimapWidget>();
            injector.RegisterWidget<OutlinerWidget>();

            // AI Roles
            injector.RegisterNamedType<SubsistanceFarming, ISettlementRole>();
            injector.RegisterNamedType<BasicCity, ISettlementRole>();

            // Console Commands
            injector.RegisterService<ClearCommand>();
            injector.RegisterService<SpawnUnitCommand>();
            injector.RegisterService<SeeAllCommand>();
            injector.RegisterService<ClearFeatureCommand>();
            injector.RegisterService<PlaceFeatureCommand>();
            injector.RegisterService<ChangeTribeCommand>();
            injector.RegisterService<AiSpyCommand>();
            injector.RegisterService<WhatSoundsCommand>();
            injector.RegisterService<PerformanceSnapshotCommand>();
            injector.RegisterService<RegenRecruitsCommand>();
            injector.RegisterService<RebellionSpyCommand>();
            injector.RegisterService<RebellionToggleCommand>();
            injector.RegisterService<LogLevelCommand>();
            injector.RegisterService<SetCultCommand>();


            // Notable Person Events
            injector.RegisterService<NotablePersonPregnancyEvent>();
            injector.RegisterService<NotablePersonDeathEvent>();
            injector.RegisterService<NotablePersonAgingEvent>();
            injector.RegisterService<NotablePersonHealingEvent>();
            injector.RegisterService<NotablePersonLearningEvent>();
            injector.RegisterService<NotablePersonBattleSkillsEvent>();

            // Settlement Events
            injector.RegisterService<PeasantRebellionsEvent>();
            injector.RegisterService<NobleRebellionsEvent>();

            // Crises
            injector.RegisterService<GhulLairTrigger>();
            injector.RegisterService<CrowCultTrigger>();

            // Treaties
            injector.RegisterTreaty<TruceTreaty>();
            injector.RegisterTreaty<MilitaryAccessTreaty>();
            injector.RegisterTreaty<ExplorationTreaty>();
            injector.RegisterTreaty<TradeSettlementTreaty>();
            injector.RegisterTreaty<ThreatenTreaty>();
            injector.RegisterTreaty<TradeTreaty>();
            injector.RegisterTreaty<MarriageTreaty>();

            // Dungeon Room Definitions
            injector.RegisterNamedType<StatCheckResolution, IRoomResolution>();
            injector.RegisterNamedType<AutoPassResolution, IRoomResolution>();
            injector.RegisterNamedType<CombatResolution, IRoomResolution>();

            // Dungeon Resolution Actions
            injector.RegisterNamedType<RoomTransformResolutionAction, IResolutionAction>();
            injector.RegisterNamedType<TransitionRoomResolutionAction, IResolutionAction>();
            injector.RegisterNamedType<BodyguardHealthResolutionAction, IResolutionAction>();
            injector.RegisterNamedType<GrantSkillResolutionAction, IResolutionAction>();
            injector.RegisterNamedType<TransitionExitResolutionAction, IResolutionAction>();
            injector.RegisterNamedType<SetCurrentRoomStateResolutionAction, IResolutionAction>();
            injector.RegisterNamedType<AddSkillResolutionAction, IResolutionAction>();
            injector.RegisterNamedType<ChanceModifiedResolutionAction, IResolutionAction>();
            injector.RegisterNamedType<UnitRewardResolutionAction, IResolutionAction>();
            injector.RegisterNamedType<PickOptionResolutionAction, IResolutionAction>();
            injector.RegisterNamedType<TransformDevelopmentResolutionAction, IResolutionAction>();

            // Cult Rites
            injector.RegisterNamedType<BonusCultRite, ICultRite>();
            injector.RegisterNamedType<PurgeCultRite, ICultRite>();
            injector.RegisterNamedType<AdjacentBonusCultRite, ICultRite>();
            injector.RegisterNamedType<SettlementTargetBonusCultRite, ICultRite>();
            injector.RegisterNamedType<ArmyAttackCultRite, ICultRite>();
            injector.RegisterNamedType<AddCellFeatureCultRite, ICultRite>();
            injector.RegisterNamedType<AddUnitCultRite, ICultRite>();
        }
    }
}
