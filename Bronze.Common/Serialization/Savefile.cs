﻿using Bronze.Contracts.Data.Serialization;

namespace Bronze.Common.Serialization
{
    public class SaveFile
    {
        public string WorldName => Summary.WorldName;
        public SerializedObject Root { get; set; }
        public SaveSummary Summary { get; set; }
        
        public SaveFile()
        {
            Root = new SerializedObject("root");
            Summary = new SaveSummary();
        }
    }
}
