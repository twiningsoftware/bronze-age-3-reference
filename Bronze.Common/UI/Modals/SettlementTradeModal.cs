﻿using System.Collections.Generic;
using System.Linq;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Modals;

namespace Bronze.Common.UI.Modals
{
    public class SettlementTradeModal : AbstractModal, IModal<Settlement>
    {
        public override bool PausesGame => true;
        protected override int ModalWidth => Elements.Max(e => e.Bounds.Width);
        protected override int ModalHeight => Elements.Max(e => e.Bounds.Height);

        private readonly IDialogManager _dialogManager;
        private readonly IPlayerDataTracker _playerData;
        private readonly ITradeManager _tradeManager;
        private readonly IGamedataTracker _gamedataTracker;
        private StackGroup _stackGroup;

        public SettlementTradeModal(
            IDialogManager dialogManager,
            IPlayerDataTracker playerData,
            ITradeManager tradeManager,
            IGamedataTracker gamedataTracker)
            : base(dialogManager)
        {
            _dialogManager = dialogManager;
            _playerData = playerData;
            _tradeManager = tradeManager;
            _gamedataTracker = gamedataTracker;

            _stackGroup = new StackGroup
            {
                Orientation = Orientation.Vertical,
                HorizontalContentAlignment = HorizontalAlignment.Middle
            };
        }

        public override void OnLoad()
        {
            base.OnLoad();
            Elements.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new Spacer(5, 0),
                    _stackGroup,
                    new Spacer(5, 0)
                }
            });
        }

        public void Initialize(Settlement settlement)
        {
            _stackGroup.Children.Clear();
            _stackGroup.Children.Add(new Spacer(0, 20));
            _stackGroup.Children.Add(new Label("Trade for " + settlement.Name));
            _stackGroup.Children.Add(new Spacer(0, 10));

            var summaryColumn = new StackGroup
            {
                Orientation = Orientation.Vertical
            };
            var importsColumn = new StackGroup
            {
                Orientation = Orientation.Vertical
            };
            var exportsColumn = new StackGroup
            {
                Orientation = Orientation.Vertical
            };
            _stackGroup.Children.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new Spacer(5, 0),
                    summaryColumn,
                    new Spacer(20, 0),
                    importsColumn,
                    new Spacer(20, 0),
                    exportsColumn
                }
            });
        
            _stackGroup.Children.Add(new Spacer(0, 20));
            _stackGroup.Children.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new TextButton("New Route", () =>
                        {
                            _dialogManager.PushModal<TradeRouteEditorModal, TradeRouteEditorModalContext>(
                                new TradeRouteEditorModalContext
                                {
                                    Callback = route => Initialize(settlement)
                                });
                        },
                        KeyCode.N),
                    new Spacer(20, 0),
                    new TextButton("Close (Esc)", () => _dialogManager.PopModal(), KeyCode.Escape)
            }
            });
            _stackGroup.Children.Add(new Spacer(0, 20));

            summaryColumn.Children.Add(new Label("Net Production"));
            summaryColumn.Children.Add(new Spacer(200, 10));
            summaryColumn.Children.Add(UiBuilder.BuildDisplayFor(
                _gamedataTracker.Items
                    .Where(i => settlement.ItemAvailability[i])
                    .Select(i => new ItemRate(i, settlement.NetItemProduction[i])),
                perRow: 3));

            importsColumn.Children.Add(new Label("Imports"));
            importsColumn.Children.Add(new Spacer(200, 10));
            foreach (var group in _tradeManager.GetImportRoutes(settlement).GroupBy(tr => tr.FromSettlement))
            {
                var detailRows = group
                    .Select(tr => new StackGroup
                    {
                        Orientation = Orientation.Horizontal,
                        VerticalContentAlignment = VerticalAlignment.Middle,
                        Children = new List<IUiElement>
                        {
                            new Label(tr.FromActor.Name),
                            new Spacer(5, 0),
                            UiBuilder.BuildIconFor(tr.MovementType),
                            new Spacer(5, 0),
                            UiBuilder.BuildDisplayFor(tr.Exports),
                            new Spacer(5, 0),
                            new TextButton("Edit",
                                () =>
                                {
                                    _dialogManager.PushModal<TradeRouteEditorModal, TradeRouteEditorModalContext>(
                                        new TradeRouteEditorModalContext
                                        {
                                            Callback = route => Initialize(settlement),
                                            ExistingTradeRoute = tr
                                        });
                                },
                                disableCheck: () => tr.IsTreatyRoute,
                                tooltip: tr.IsTreatyRoute ? "This trade route is controlled by a treaty." : null),
                            new Spacer(5, 0),
                            new TextButton("X",
                                () =>
                                {
                                    _tradeManager.RemoveTradeRoute(tr);
                                    Initialize(settlement);
                                },
                                disableCheck: () => tr.IsTreatyRoute,
                                tooltip: tr.IsTreatyRoute ? "This trade route is controlled by a treaty." : "Remove the trade route"),
                            new Spacer(5, 0),
                            new Image(UiImages.TradeRouteInvalid, tooltip: "A path cannot be found for the trade route.")
                            {
                                VisibilityCheck = () => !tr.RouteValid
                            }
                        }
                    })
                    .ToList<IUiElement>();

                importsColumn.Children.Add(new Border(new StackGroup
                {
                    Orientation = Orientation.Horizontal,
                    Children = new List<IUiElement>
                    {
                        new Label(group.Key.Name),
                        new Spacer(10, 0),
                        new StackGroup
                        {
                            Orientation = Orientation.Vertical,
                            Children = detailRows
                        }
                    }
                },
                margin: 5));
            }

            importsColumn.Children.Add(new Spacer(0, 10));
            
            exportsColumn.Children.Add(new Label("Exports"));
            exportsColumn.Children.Add(new Spacer(200, 10));
            foreach (var group in _tradeManager.GetExportRoutes(settlement).GroupBy(tr => tr.ToSettlement))
            {
                var detailRows = group
                    .Select(tr => new StackGroup
                    {
                        Orientation = Orientation.Horizontal,
                        VerticalContentAlignment = VerticalAlignment.Middle,
                        Children = new List<IUiElement>
                        {
                            new Label(tr.ToActor.Name),
                            new Spacer(5, 0),
                            UiBuilder.BuildIconFor(tr.MovementType),
                            new Spacer(5, 0),
                            UiBuilder.BuildDisplayFor(tr.Exports),
                            new Spacer(5, 0),
                            new TextButton("Edit",
                                () =>
                                {
                                    _dialogManager.PushModal<TradeRouteEditorModal, TradeRouteEditorModalContext>(
                                        new TradeRouteEditorModalContext
                                        {
                                            Callback = route => Initialize(settlement),
                                            ExistingTradeRoute = tr
                                        });
                                }),
                            new Spacer(5, 0),
                            new TextButton("X",
                                () =>
                                {
                                    _tradeManager.RemoveTradeRoute(tr);
                                    Initialize(settlement);
                                },
                                tooltip: "Remove the trade route"),
                            new Spacer(5, 0),
                            new Image(UiImages.TradeRouteInvalid, tooltip: "A path cannot be found for the trade route.")
                            {
                                VisibilityCheck = () => !tr.RouteValid
                            }
                        }
                    })
                    .ToList<IUiElement>();

                exportsColumn.Children.Add(new Border(new StackGroup
                {
                    Orientation = Orientation.Horizontal,
                    Children = new List<IUiElement>
                    {
                        new Label(group.Key.Name),
                        new Spacer(10, 0),
                        new StackGroup
                        {
                            Orientation = Orientation.Vertical,
                            Children = detailRows
                        }
                    }
                },
                margin: 5));
            }

            exportsColumn.Children.Add(new Spacer(0, 10));
        }
    }
}
