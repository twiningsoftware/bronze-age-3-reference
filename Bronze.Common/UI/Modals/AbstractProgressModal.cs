﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Bronze.Contracts.Exceptions;
using Bronze.Contracts.Data;
using Bronze.Contracts.Delegates;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Modals;

namespace Bronze.Common.UI.Modals
{
    public abstract class AbstractProgressModal : AbstractModal
    {
        protected override int ModalWidth => 324;
        protected override int ModalHeight => 200;

        private readonly IDialogManager _dialogManager;
        
        private float _overallProgress;
        private Text _stepNameText;
        private float _stepProgress;

        private Task _task;
        protected Exception _exception { get; set; }

        protected abstract string Title { get; }

        protected AbstractProgressModal(IDialogManager dialogManager)
            : base(dialogManager)
        {
            _dialogManager = dialogManager;
        }
        
        public override void OnLoad()
        {
            base.OnLoad();

            _stepNameText = new Text("");

            Elements.Add(new StackGroup
            {
                HorizontalContentAlignment = HorizontalAlignment.Middle,
                VerticalContentAlignment = VerticalAlignment.Middle,
                Width = Width.Relative(1f),
                Height = Height.Relative(1f),
                Orientation = Orientation.Vertical,
                Children = new List<IUiElement>
                {
                    new Text(Title),
                    new Spacer(10,10),
                    new ScrollBar(Orientation.Horizontal, 0, 1, () => _overallProgress, val => {}),
                    new Spacer(10,10),
                    _stepNameText,
                    new Spacer(10,10),
                    new ScrollBar(Orientation.Horizontal, 0, 1, () => _stepProgress, val => { }),
                }
            });

            _task = Task.Run(() =>
            {
                try
                {
                    CreateWorkAction().Invoke(ReportOnProgress);
                }
                catch (Exception ex)
                {
                    _exception = ex;
                }
            });
        }
        
        protected void ReportOnProgress(
            double overallProgress,
            string stepName,
            double stepProgress)
        {
            _overallProgress = (float)overallProgress;
            _stepProgress = (float)stepProgress;
            _stepNameText.Content = stepName;
        }

        public override void Update(
            float elapsedSeconds, 
            InputState inputState, 
            IAudioService audioService)
        {
            base.Update(elapsedSeconds, inputState, audioService);

            if (_exception != null)
            {
                HandleException(_exception);
            }
            else if (_task.IsCompleted)
            {
                _dialogManager.PopModal();

                OnFinished();
            }
        }

        protected virtual void HandleException(Exception exception)
        {
            throw new FatalBronzeException("", _exception);
        }

        protected abstract Action<ReportProgress> CreateWorkAction();

        protected abstract void OnFinished();
    }
}
