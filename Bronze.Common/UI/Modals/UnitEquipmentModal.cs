﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Modals;

namespace Bronze.Common.UI.Modals
{
    public class UnitEquipmentModal : AbstractModal, IModal<UnitEquipmentModalContext>
    {
        public override bool PausesGame => true;
        protected override int ModalWidth => Elements.Max(e => e.Bounds.Width);
        protected override int ModalHeight => Elements.Max(e => e.Bounds.Height);

        private StackGroup _stackGroup;
        private readonly IDialogManager _dialogManager;
        private readonly IPlayerDataTracker _playerData;
        private readonly IUnitHelper _unitHelper;
        private readonly IGamedataTracker _gamedataTracker;
        private UnitEquipmentSet _activeEquipment;

        public UnitEquipmentModal(
            IDialogManager dialogManager,
            IPlayerDataTracker playerData,
            IUnitHelper unitHelper,
            IGamedataTracker gamedataTracker)
            : base(dialogManager)
        {
            _dialogManager = dialogManager;
            _playerData = playerData;
            _unitHelper = unitHelper;
            _gamedataTracker = gamedataTracker;

            _stackGroup = new StackGroup
            {
                Orientation = Orientation.Vertical,
                HorizontalContentAlignment = HorizontalAlignment.Middle
            };
        }

        public override void OnLoad()
        {
            base.OnLoad();
            Elements.Add(_stackGroup);
        }

        public void Initialize(UnitEquipmentModalContext context)
        {
            _stackGroup.Children.Clear();
            _stackGroup.Children.Add(new Spacer(0, 20));
            _stackGroup.Children.Add(new Label($"Recruiting From {context.Settlement.Name}"));
            _stackGroup.Children.Add(new Spacer(0, 10));

            var buttonItems = new List<IUiElement>();
            
            var detailsStack = new StackGroup
            {
                Orientation = Orientation.Vertical,
            };
            detailsStack.Children.Add(new Spacer(320, 0));

            SetActiveEquipment(
                detailsStack,
                context.Settlement,
                context.Unit,
                context.Unit.Equipment);

            var availableEquipment = context.Unit.UnitType.EquipmentSets
                .Where(es => es.RecruitmentCost.All(iq => _playerData.PlayerTribe.KnownItems[iq.Item] == true))
                .ToArray();

            buttonItems.AddRange(
                availableEquipment
                    .Select(es => new IconButton(
                        es.IconKey,
                        string.Empty,
                        () => SetActiveEquipment(detailsStack, context.Settlement, context.Unit, es),
                        highlightCheck: () => _activeEquipment == es,
                        tooltip: es.Name.Length > 0 ? $"{context.Unit.UnitType.Name} with {es.Name}" : context.Unit.UnitType.Name)
                    {
                        Colorization = _playerData.PlayerTribe.PrimaryColor
                    }));
            
            _stackGroup.Children.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new Spacer(10, 500),
                    new ScrollableList(10, buttonItems),
                    new Spacer(10, 500),
                    detailsStack,
                    new Spacer(10, 500),
                }
            });

            _stackGroup.Children.Add(new Spacer(0, 20));
            _stackGroup.Children.Add(new TextButton("Close (Esc)", () => _dialogManager.PopModal(), KeyCode.Escape));
            _stackGroup.Children.Add(new Spacer(0, 20));
        }

        private void SetActiveEquipment(
            StackGroup detailsStack, 
            Settlement sourceSettlement,
            IUnit unit,
            UnitEquipmentSet equipmentSet)
        {
            _activeEquipment = equipmentSet;
            detailsStack.Children.Clear();
            
            var stockpiles = sourceSettlement
                .EconomicActors
                .OfType<IArmoryStorageProvider>()
                .ToArray();

            var mainColumn = new StackGroup
            {
                Orientation = Orientation.Vertical,
                Width = Width.Fixed(200),
                Children = new List<IUiElement>
                {
                    new Text(unit.UnitType.Description),
                    new Spacer(0, 10),
                }
            };

            if(equipmentSet.Name.Length > 0)
            {
                mainColumn.Children.Add(new IconText(equipmentSet.IconKey, equipmentSet.Name));
                mainColumn.Children.Add(new Spacer(0, 5));
                mainColumn.Children.Add(new Text(equipmentSet.Description));
                mainColumn.Children.Add(new Spacer(0, 10));
            }

            if (equipmentSet.RecruitmentCost.Any())
            {
                mainColumn.Children.Add(new Spacer(0, 10));
                mainColumn.Children.Add(new Label("Recruitment Cost"));
                mainColumn.Children.Add(UiBuilder.BuildDisplayFor(equipmentSet.RecruitmentCost));
            }

            if (equipmentSet.UpkeepNeeds.Any())
            {
                mainColumn.Children.Add(new Spacer(0, 10));
                mainColumn.Children.Add(new Label("Upkeep"));
                mainColumn.Children.Add(UiBuilder.BuildDisplayFor(equipmentSet.UpkeepNeeds.Select(u => new ItemRate(u.Item, -u.PerMonth))));
            }

            var totalStocks = _gamedataTracker.Items
                .Select(i => new ItemQuantity(i, Math.Floor(stockpiles.Sum(s => s.Inventory.QuantityOf(i)))))
                .Where(iq => iq.Quantity > 0)
                .ToArray();

            var sideColumn = new StackGroup
            {
                Orientation = Orientation.Vertical
            };

            if(totalStocks.Any())
            {
                sideColumn.Children.AddRange(new IUiElement[]
                {
                    new Label("Armory Stocks"),
                    UiBuilder.BuildDisplayFor(totalStocks, 4),
                    new Spacer(0, 10),
                });
            }

            sideColumn.Children.AddRange(new IUiElement[]
            {
                new Label("Unit Stats"),
                    new StackGroup
                    {
                        Orientation = Orientation.Horizontal,
                        Children = new List<IUiElement>
                        {
                            new IconText("ui_stat_health", $"{equipmentSet.HealthPerIndividual * equipmentSet.IndividualCount}", tooltip: "Health"),
                            new Spacer(10, 0),
                            new IconText("ui_stat_supplies", $"{equipmentSet.SupplyCapacity}", tooltip: "Supplies"),
                            new Spacer(10, 0),
                            new IconText("ui_stat_vision", equipmentSet.VisionRange.ToString(), tooltip: "Unit vision range."),
                        }
                    },
                    new StackGroup
                    {
                        Orientation = Orientation.Horizontal,
                        Children = new List<IUiElement>
                        {
                            new IconText("ui_stat_armor", equipmentSet.Armor.ToString(), tooltip: "Armor: reduces damage taken."),
                            new Spacer(10, 0),
                            new IconText("ui_stat_defense", equipmentSet.DefenseSkill.ToString(), tooltip: "Defense: Ability to defend against melee attacks."),
                            new Spacer(10, 0),
                            new IconText("ui_stat_block", equipmentSet.BlockSkill.ToString(), tooltip: "Block: Ability to block melee and ranged attacks."),
                        }
                    },
                    new StackGroup
                    {
                        Orientation = Orientation.Horizontal,
                        Children = new List<IUiElement>
                        {
                            new IconText("ui_stat_skirmish", equipmentSet.SkirmishSkill.ToString(), tooltip: "Unit skirmish skill."),
                            new Spacer(10, 0),
                            new IconText("ui_stat_resolve", equipmentSet.ResolveSkill.ToString(), tooltip: "Resolve: Ability to resist morale shocks."),
                        }
                    },
                    
            });

            sideColumn.Children.AddRange(equipmentSet.MeleeAttacks
                    .Select(m => UiBuilder.BuildDisplayForMeleeAttack(m)));
            sideColumn.Children.AddRange(equipmentSet.RangedAttacks
                    .Select(m => UiBuilder.BuildDisplayForRangedAttack(m)));
            sideColumn.Children.AddRange(equipmentSet.MovementModes
                    .Select(mm => new IconText(mm.MovementType.IconKey, mm.Speed.ToString(), tooltip: mm.MovementType.Name + " Speed")));
            sideColumn.Children.AddRange(equipmentSet.AvailableActions
                    .Select(a => new IconText(a.Icon, a.Name, tooltip: a.Description)));
            
            detailsStack.Children.Add(new Label(unit.UnitType.Name));
            detailsStack.Children.Add(new Spacer(0, 10));
            
            detailsStack.Children.Add(new Spacer(0, 10));
            detailsStack.Children.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    mainColumn,
                    new Spacer(20, 0),
                    sideColumn
                }
            });
            
            var canTrain = true;
            var messages = new List<IUiElement>();
            
            if (equipmentSet.RecruitmentCost.Any())
            {
                var hasCosts = equipmentSet.RecruitmentCost
                    .All(iq => stockpiles.Sum(s => s.Inventory.QuantityOf(iq.Item)) >= iq.Quantity);

                if(!hasCosts)
                {
                    messages.Add(new Label($"Insufficient armory stockpiles of:"));
                    foreach(var cost in equipmentSet.RecruitmentCost)
                    {
                        var shortfall = cost.Quantity - stockpiles.Sum(s => s.Inventory.QuantityOf(cost.Item));

                        if(shortfall > 0)
                        {
                            messages.Add(new IconText(cost.Item.IconKey, cost.Item.Name));
                        }
                    }

                    canTrain = false;
                }
            }

            if(unit.Equipment == equipmentSet)
            {
                messages.Add(new Label($"Unit is already using this equipment."));
                canTrain = false;
            }
            
            if(messages.Any())
            {
                detailsStack.Children.Add(new Spacer(0, 10));
                detailsStack.Children.AddRange(messages);
            }

            detailsStack.Children.Add(new Spacer(0, 10));

            
            detailsStack.Children.Add(new TextButton(
                "Use Equipment",
                () =>
                {
                    _unitHelper.ChangeEquipment(unit, equipmentSet, sourceSettlement);

                    _dialogManager.PopAllModals();
                },
                KeyCode.U,
                disableCheck: () => !canTrain));
        }
    }

    public class UnitEquipmentModalContext
    {
        public IUnit Unit { get; set; }
        public Settlement Settlement { get; set; }
    }
}
