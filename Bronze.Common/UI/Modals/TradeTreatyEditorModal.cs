﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bronze.Common.Data.Diplomacy;
using Bronze.Common.Gameplay.UiElements;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Diplomacy;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Modals;

namespace Bronze.Common.UI.Modals
{
    public class TradeTreatyEditorModal : AbstractModal, IModal<TradeTreatyEditorModalContext>
    {
        public override bool PausesGame => true;
        protected override int ModalWidth => Elements.Max(e => e.Bounds.Width);
        protected override int ModalHeight => Elements.Max(e => e.Bounds.Height);

        private readonly IDialogManager _dialogManager;
        private readonly IPlayerDataTracker _playerData;
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IInjectionProvider _injectionProvider;
        private StackGroup _stackGroup;
        private TradePickerMinimapElement _minimap;

        private Tribe _fromTribe;
        private Settlement _fromSettlement;
        private ITradeActor _fromActor;
        private Tribe _toTribe;
        private Settlement _toSettlement;
        private ITradeReciever _toActor;
        private Dictionary<Item, double> _itemsToSend;
        private Dictionary<Item, int> _perItemLimits;
        private List<Item> _availableItems;
        private Dictionary<Item, double> _valuePerItem;
        private Action<TradeTreaty> _callback;
        private ITreaty[] _treatiesInNegotiation;
        private TradePathCalculator _tradePathCalculator;
        private TradeTreaty _existingTreaty;
        
        public TradeTreatyEditorModal(
            IDialogManager dialogManager,
            IPlayerDataTracker playerData,
            IGamedataTracker gamedataTracker,
            IInjectionProvider injectionProvider,
            IWorldManager worldManager)
            : base(dialogManager)
        {
            _dialogManager = dialogManager;
            _playerData = playerData;
            _gamedataTracker = gamedataTracker;
            _injectionProvider = injectionProvider;

            _itemsToSend = new Dictionary<Item, double>();
            _perItemLimits = new Dictionary<Item, int>();
            _availableItems = new List<Item>();
            _valuePerItem = new Dictionary<Item, double>();
            
            _stackGroup = new StackGroup
            {
                Orientation = Orientation.Vertical,
                HorizontalContentAlignment = HorizontalAlignment.Middle
            };

            _minimap = new TradePickerMinimapElement(playerData, worldManager)
            {
                Width = Width.Fixed(300),
                Height = Height.Fixed(300)
            };
        }

        public override void OnLoad()
        {
            base.OnLoad();
            Elements.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new Spacer(5, 200),
                    _stackGroup,
                    new Spacer(5, 200)
                }
            });
        }

        public void Initialize(TradeTreatyEditorModalContext context)
        {
            _fromTribe = context.From;
            _toTribe = context.To;
            _callback = context.Callback;
            _treatiesInNegotiation = context.TreatiesInNegotiation;
            _existingTreaty = context.ExistingTreaty;

            _fromActor = null;
            _toActor = null;
            _fromSettlement = null;
            _toSettlement = null;
            _itemsToSend.Clear();
            _perItemLimits.Clear();
            _availableItems.Clear();
            _valuePerItem.Clear();

            if(_existingTreaty != null)
            {
                _fromSettlement = _existingTreaty.FromActor.Settlement;
                _fromActor = _existingTreaty.FromActor;
                _toSettlement = _existingTreaty.ToActor.Settlement;
                _toActor = _existingTreaty.ToActor;
                CalculateTradePossibilities();

                foreach(var item in _existingTreaty.ItemsToSend.Keys)
                {
                    _itemsToSend[item] = _existingTreaty.ItemsToSend[item];
                }
                
                if(_existingTreaty.FromActor?.TradeRoute != null)
                {
                    foreach(var itemRate in _existingTreaty.FromActor.TradeRoute.Exports)
                    {
                        _perItemLimits[itemRate.Item] += (int)itemRate.PerMonth;
                    }
                }
            }

            RebuildContent();
        }

        private void RebuildContent()
        {
            var fromToPicker = new StackGroup
            {
                Orientation = Orientation.Vertical,
                Children = new List<IUiElement>
                {
                    _minimap,
                    new Spacer(0, 10),
                    new Label("From"),
                    new TextButton(_fromActor == null ? "Pick Trader" : $"{_fromSettlement.Name}, {_fromActor.Name}",
                        () => PickFrom(_fromTribe),
                        disableCheck: () => _existingTreaty != null,
                        tooltip: "Pick a source trader.")
                    {
                        HoverAction = () => _minimap.Highlight(_fromActor)
                    },
                    new Spacer(0, 10),
                    new Label("To"),
                    new TextButton(_toActor == null ? "Pick Trade Depot" : $"{_toSettlement.Name}, {_toActor.Name}",
                        () => PickTo(_toTribe),
                        disableCheck: () => _fromActor == null || _existingTreaty != null,
                        tooltip: "Pick a destination depot.")
                    {
                        HoverAction = () => _minimap.Highlight(_toActor)
                    },
                    new Spacer(0, 10),
                    new StackGroup
                    {
                        Orientation = Orientation.Horizontal,
                        Children = new List<IUiElement>
                        {
                            new Image("ui_trade_invalid", "Cannot determine path.")
                            {
                                VisibilityCheck = () => _tradePathCalculator != null && _tradePathCalculator.PathFinished && !_tradePathCalculator.PathSuccessful
                            },
                            new Image("ui_trade_calculating", "Determining path...")
                            {
                                VisibilityCheck = () => _tradePathCalculator != null && !_tradePathCalculator.PathFinished
                            },
                            new Image("ui_trade_valid", "Path determined.")
                            {
                                VisibilityCheck = () => _tradePathCalculator != null && _tradePathCalculator.PathFinished && _tradePathCalculator.PathSuccessful
                            },
                        }
                    }
                },
            };

            if (_fromActor != null)
            {
                fromToPicker.Children.Add(new Spacer(0, 10));
                fromToPicker.Children.Add(UiBuilder.BuildIconFor(_fromActor.TradeType));
                fromToPicker.Children.Add(new Label(string.Empty)
                {
                    ContentUpdater = () => $"Capacity {_itemsToSend.Values.Sum()}/{(int)_fromActor.TradeCapacity}",
                    ColorUpdater = () => _itemsToSend.Values.Sum() <= _fromActor.TradeCapacity ? BronzeColor.Green : BronzeColor.Yellow,
                });
            }

            IUiElement itemColumn;
            if (_fromActor != null && _toActor != null)
            {
                foreach (var item in _availableItems)
                {
                    if (!_itemsToSend.ContainsKey(item))
                    {
                        _itemsToSend.Add(item, 0);
                    }
                }

                itemColumn = new TableGroup<Item>(
                    new[] { "Item", _fromSettlement.Name, _toSettlement.Name, "Limit", "To Send", "Value" },
                    _availableItems,
                    item =>
                    {
                        return new AbstractUiElement[]
                        {
                            new IconText(item.IconKey, string.Empty, tooltip: item.Name),
                            UiBuilder.PerMonthRate(_fromSettlement.NetItemProduction[item]),
                            UiBuilder.PerMonthRate(_toSettlement.NetItemProduction[item]),
                            new Label(_perItemLimits[item].ToString())
                            {
                                ColorUpdater = () => _itemsToSend[item] <= _perItemLimits[item] ? BronzeColor.Green : BronzeColor.Red
                            },
                            new TextInput(
                                () => _itemsToSend[item].ToString(),
                                val =>
                                {
                                    if(int.TryParse(val, out var newVal ))
                                    {
                                        _itemsToSend[item] = newVal;
                                    }

                                    if(string.IsNullOrWhiteSpace(val))
                                    {
                                        _itemsToSend[item] = 0;
                                    }
                                },
                                50),
                            new Label("")
                            {
                                ContentUpdater = () =>
                                {
                                    return (_valuePerItem[item] * _itemsToSend[item]).ToString("N0");
                                }
                            }
                        };
                    },
                    15);
            }
            else
            {
                itemColumn = new StackGroup
                {
                    Orientation = Orientation.Vertical,
                    Children = new List<IUiElement>
                    {
                        new Spacer(300, 10),
                        new Label("Pick source and destination.")
                    }
                };
            }
            
            _stackGroup.Children.Clear();
            _stackGroup.Children.Add(new Spacer(0, 20));
            _stackGroup.Children.Add(new Label($"Trade Route from {_fromTribe.Name} to {_toTribe.Name}"));
            _stackGroup.Children.Add(new Spacer(0, 5));
            _stackGroup.Children.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new Spacer(10, 0),
                    fromToPicker,
                    new Spacer(20, 300),
                    itemColumn,
                    new Spacer(10, 0),
                }
            });
            
            _stackGroup.Children.Add(new Spacer(0, 10));
            _stackGroup.Children.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new TextButton("Save",
                        () =>
                        {
                            _dialogManager.PopModal();

                            if(_existingTreaty != null)
                            {
                                foreach(var item in _itemsToSend.Keys)
                                {
                                    _existingTreaty.ItemsToSend[item] = _itemsToSend[item];
                                }

                                _callback.Invoke(_existingTreaty);
                            }
                            else
                            {
                                _callback.Invoke(_injectionProvider
                                    .Build<TradeTreaty>().Initialize(
                                        _fromActor,
                                        _toActor,
                                        _itemsToSend));
                            }
                        },
                        KeyCode.S,
                        disableCheck: () =>
                            !_itemsToSend.Any(kvp => kvp.Value > 0)
                            || _itemsToSend.Values.Sum() > _fromActor.TradeCapacity
                            || _itemsToSend.Any(kvp => kvp.Value > 0 &&  kvp.Value > _perItemLimits[kvp.Key])
                            || (!_tradePathCalculator?.PathSuccessful ?? false)),
                    new Spacer(20, 0),
                    new TextButton("Cancel (Esc)",
                        () =>
                        {
                            _dialogManager.PopModal();
                            _callback(null);
                        }, KeyCode.Escape)
                }
            });
            _stackGroup.Children.Add(new Spacer(0, 20));
        }
        
        private void PickFrom(Tribe fromTribe)
        {
            var fromActors = fromTribe
                .Settlements
                .SelectMany(s => s.EconomicActors.OfType<ITradeActor>())
                .Where(ta => !ta.UnderConstruction && ta.TradeRoute == null)
                .Where(ta => !_treatiesInNegotiation.OfType<TradeTreaty>().Any(tt => tt.FromActor == ta))
                .ToArray();

            _dialogManager.PushModal<TradeActorPickerModal, TradeActorPickerModalContext>(
                new TradeActorPickerModalContext
                {
                    Title = "Pick Source Trader",
                    Options = fromActors,
                    Callback = o =>
                    {
                        if (o != null)
                        {
                            _fromSettlement = o.Settlement;
                            _fromActor = o;
                            _toSettlement = null;
                            _toActor = null;
                            _itemsToSend.Clear();
                            _availableItems.Clear();
                            _valuePerItem.Clear();
                            RebuildContent();
                            _tradePathCalculator = null;
                        }
                    }
                });
        }

        private void PickTo(Tribe toTribe)
        {
            var toActors = toTribe
                .Settlements
                .SelectMany(s => s.EconomicActors.OfType<ITradeReciever>())
                .Where(tr => !tr.UnderConstruction)
                .ToArray();

            _dialogManager.PushModal<TradeRecieverPickerModal, TradeRecieverPickerModalContext>(
                new TradeRecieverPickerModalContext
                {
                    Title = "Pick Destination",
                    Sender = _fromActor,
                    Options = toActors,
                    Callback = o =>
                    {
                        if (o != null)
                        {
                            _toSettlement = o.Settlement;
                            _toActor = o;
                            CalculateTradePossibilities();
                        }
                    }
                });
        }

        private void CalculateTradePossibilities()
        {
            _itemsToSend.Clear();
            _perItemLimits.Clear();
            _valuePerItem.Clear();

            _tradePathCalculator = new TradePathCalculator(
                _fromActor.Settlement.Owner,
                _fromActor.TradeType,
                _fromActor,
                _toActor)
            {
                RecalculateOnFailure = false
            };

            _availableItems = _gamedataTracker.Items
                .Where(i => _fromSettlement.ItemAvailability[i])
                .ToList();

            foreach (var item in _gamedataTracker.Items)
            {
                _itemsToSend.Add(item, 0);

                if (_fromSettlement.Owner == _playerData.PlayerTribe)
                {
                    _perItemLimits.Add(item, Math.Max(0, _toSettlement.Owner.Controller.ImportNeedOf(item, _toSettlement)));
                    _valuePerItem.Add(item, _toSettlement.Owner.Controller.TradeValueOf(item, _toSettlement));
                }
                else
                {
                    _perItemLimits.Add(item, _fromSettlement.Owner.Controller.ExportAvailabilityOf(item, _fromSettlement));
                    _valuePerItem.Add(item, _fromSettlement.Owner.Controller.TradeValueOf(item, _fromSettlement));
                }
            }

            RebuildContent();
        }

        public override void Update(float elapsedSeconds, InputState inputState, IAudioService audioService)
        {
            base.Update(elapsedSeconds, inputState, audioService);

            if (_tradePathCalculator != null)
            {
                _tradePathCalculator.Update();
            }

            if(_tradePathCalculator != null && _tradePathCalculator.PathFinished && _tradePathCalculator.PathSuccessful)
            {
                _minimap.ShowPath(_tradePathCalculator.Path);
            }
            else
            {
                _minimap.ClearPath();
            }
        }
    }

    public class TradeTreatyEditorModalContext
    {
        public Tribe From { get; set; }
        public Tribe To { get; set; }
        public Action<TradeTreaty> Callback { get; set; }
        public ITreaty[] TreatiesInNegotiation { get; set; } 
        public TradeTreaty ExistingTreaty { get; set; }
    }
}
