﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bronze.Common.Data.Diplomacy;
using Bronze.Common.Data.World.CellDevelopments;
using Bronze.Common.Gameplay.UiElements;
using Bronze.Common.UI.Elements;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Diplomacy;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Modals;

namespace Bronze.Common.UI.Modals
{
    public class TradeSettlementTreatyEditorModal : AbstractModal, IModal<TradeSettlementTreatyEditorModalContext>
    {
        public override bool PausesGame => true;
        protected override int ModalWidth => Elements.Max(e => e.Bounds.Width);
        protected override int ModalHeight => Elements.Max(e => e.Bounds.Height);

        private readonly IDialogManager _dialogManager;
        private readonly IPlayerDataTracker _playerData;
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IInjectionProvider _injectionProvider;
        private StackGroup _stackGroup;
        private StackGroup _detailsGroup;
        private TradePickerMinimapElement _minimap;

        private Tribe _fromTribe;
        private Tribe _toTribe;
        private Action<TradeSettlementTreaty> _callback;
        private ITreaty[] _treatiesInNegotiation;
        private Settlement _activeSettlement;

        public TradeSettlementTreatyEditorModal(
            IDialogManager dialogManager,
            IPlayerDataTracker playerData,
            IGamedataTracker gamedataTracker,
            IInjectionProvider injectionProvider,
            IWorldManager worldManager)
            : base(dialogManager)
        {
            _dialogManager = dialogManager;
            _playerData = playerData;
            _gamedataTracker = gamedataTracker;
            _injectionProvider = injectionProvider;
            
            _stackGroup = new StackGroup
            {
                Orientation = Orientation.Vertical,
                HorizontalContentAlignment = HorizontalAlignment.Middle
            };

            _minimap = new TradePickerMinimapElement(playerData, worldManager)
            {
                Width = Width.Fixed(300),
                Height = Height.Fixed(300)
            };
        }

        public override void OnLoad()
        {
            base.OnLoad();
            Elements.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new Spacer(5, 200),
                    _stackGroup,
                    new Spacer(5, 200)
                }
            });
        }

        public void Initialize(TradeSettlementTreatyEditorModalContext context)
        {
            _fromTribe = context.From;
            _toTribe = context.To;
            _callback = context.Callback;
            _treatiesInNegotiation = context.TreatiesInNegotiation;

            var settlements = _fromTribe.Settlements
                .Where(s => s != _fromTribe.Capital)
                .Where(s => !_treatiesInNegotiation.OfType<TradeSettlementTreaty>().Any(tst => tst.Settlement == s))
                .ToArray();

            var buttons = settlements
                .Select(s => new TextButton(
                    s.Name,
                    () =>
                    {
                        _activeSettlement = s;
                        _minimap.Highlight(_activeSettlement.EconomicActors.OfType<ICellDevelopment>().Where(cd => cd is DistrictDevelopment || cd is VillageDevelopment).FirstOrDefault());
                        RebuildContent();
                    },
                    highlightCheck: () => _activeSettlement == s))
                .ToArray();
            
            _detailsGroup = new StackGroup
            {
                Orientation = Orientation.Vertical
            };

            _stackGroup.Children.Clear();
            _stackGroup.Children.Add(new Spacer(0, 20));
            _stackGroup.Children.Add(new Label($"Pick A Settlement To Trade"));
            _stackGroup.Children.Add(new Spacer(0, 5));
            _stackGroup.Children.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new Spacer(10, 0),
                    _minimap,
                    new Spacer(20, 300),
                    new ScrollableList(10, buttons),
                    new Spacer(20, 300),
                    _detailsGroup,
                    new Spacer(10, 0),
                }
            });

            _stackGroup.Children.Add(new Spacer(0, 10));
            _stackGroup.Children.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new TextButton("Save",
                        () =>
                        {
                            _dialogManager.PopModal();
                            _callback.Invoke(_injectionProvider
                                .Build<TradeSettlementTreaty>().Initialize(
                                    _fromTribe,
                                    _toTribe,
                                    _activeSettlement,
                                    ((_fromTribe == _playerData.PlayerTribe) ? _toTribe : _fromTribe).Controller.ScoreRegion(_activeSettlement.Region)));
                        },
                        KeyCode.S,
                        disableCheck: () => _activeSettlement == null),
                    new Spacer(20, 0),
                    new TextButton("Cancel (Esc)",
                        () =>
                        {
                            _dialogManager.PopModal();
                            _callback(null);
                        }, KeyCode.Escape)
                }
            });
            _stackGroup.Children.Add(new Spacer(0, 20));

            RebuildContent();
        }

        private void RebuildContent()
        {
            _detailsGroup.Children.Clear();
            _detailsGroup.Children.Add(new Spacer(200, 0));

            if (_activeSettlement != null)
            {
                _detailsGroup.Children.Add(new Label(_activeSettlement.Name));
                _detailsGroup.Children.Add(new Spacer(0, 10));
                _detailsGroup.Children.Add(
                    new StackGroup
                    {
                        Orientation = Orientation.Horizontal,
                        Children = _activeSettlement.PopulationByCaste
                            .Where(kvp => kvp.Value > 0)
                            .SelectMany(kvp => new IUiElement[]
                            {
                            new IconText(kvp.Key.IconKey, kvp.Value.ToString(), tooltip: $"{kvp.Key.Race.Name} {kvp.Key.Name} population."),
                            new Spacer(10, 0)
                            })
                            .ToList()
                    });
                _detailsGroup.Children.Add(new Spacer(0, 10));

                _detailsGroup.Children.Add(new ItemRateDisplay(
                    () => _gamedataTracker.Items
                    .Where(i => _activeSettlement.ItemAvailability[i])
                    .ToDictionary(i => i, i => _activeSettlement.NetItemProduction[i])));
            }
        }
    }

    public class TradeSettlementTreatyEditorModalContext
    {
        public Tribe From { get; set; }
        public Tribe To { get; set; }
        public Action<TradeSettlementTreaty> Callback { get; set; }
        public ITreaty[] TreatiesInNegotiation { get; set; } 
    }
}
