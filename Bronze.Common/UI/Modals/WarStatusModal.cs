﻿using System.Collections.Generic;
using System.Linq;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Diplomacy;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Modals;

namespace Bronze.Common.UI.Modals
{
    public class WarStatusModal : AbstractModal
    {
        public override bool PausesGame => true;
        protected override int ModalWidth => Elements.Max(e => e.Bounds.Width);
        protected override int ModalHeight => Elements.Max(e => e.Bounds.Height);

        private readonly IDialogManager _dialogManager;
        private readonly IWorldManager _worldManager;
        private readonly IPlayerDataTracker _playerData;
        private readonly IDiplomacyHelper _diplomacyHelper;

        private StackGroup _content;

        public WarStatusModal(
            IDialogManager dialogManager,
            IWorldManager worldManager,
            IPlayerDataTracker playerData,
            IDiplomacyHelper diplomacyHelper)
            : base(dialogManager)
        {
            _dialogManager = dialogManager;
            _worldManager = worldManager;
            _playerData = playerData;
            _diplomacyHelper = diplomacyHelper;

            _content = new StackGroup
            {
                Orientation = Orientation.Vertical
            };
        }
        
        public override void OnLoad()
        {
            base.OnLoad();

            Elements.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new Spacer(10, 0),
                    _content,
                    new Spacer(10, 0)
                }
            });

            BuildWarList();
        }

        private void BuildWarList()
        {
            _content.Children.Clear();
            _content.HorizontalContentAlignment = HorizontalAlignment.Middle;

            _content.Children.AddRange(new List<IUiElement>
            {
                new Spacer(0, 10),
                new Label("Current Wars"),
                new Spacer(0, 5),
                new SizePadder(
                    new TableGroup<War>(
                        new [] {"Against", "Started", "Score"},
                        _playerData.PlayerTribe.CurrentWars,
                        w => BuildWarRow(w),
                        15),
                    minHeight: 300)
                {
                    VerticalAlignment = VerticalAlignment.Top
                },
                new Spacer(0, 5),
                new TextButton("Close (Esc)", () => _dialogManager.PopModal(), KeyCode.Escape),
                new Spacer(0, 10),
            });
        }

        private IEnumerable<IUiElement> BuildWarRow(War war)
        {
            var otherTribe = war.Aggressor == _playerData.PlayerTribe ? war.Defender : war.Aggressor;
            var score = war.Aggressor == _playerData.PlayerTribe ? war.WarScore: (-1 * war.WarScore);

            return new IUiElement[]
            {
                new TextButton(otherTribe.Name,
                    () => BuidWarDetail(war)),
                new Label(Util.FriendlyDateDisplay(war.StartDate)),
                new Label(score.ToString("F0"))
            };
        }

        private void BuidWarDetail(War war)
        {
            var otherTribe = war.Aggressor == _playerData.PlayerTribe ? war.Defender : war.Aggressor;
            var scoreMod = war.Aggressor == _playerData.PlayerTribe ? 1 : -1;

            _content.Children.Clear();
            _content.HorizontalContentAlignment = HorizontalAlignment.Middle;

            _content.Children.AddRange(new List<IUiElement>
            {
                new Spacer(0, 10),
                new Label($"War with {otherTribe.Name}"),
                new Spacer(0, 5),
                new StackGroup
                {
                    Orientation = Orientation.Horizontal,
                    Children = new List<IUiElement>
                    {
                        UiBuilder.BuildLeaderInfoBlock(_playerData.PlayerTribe.Ruler),
                        new Spacer(10, 0),
                        UiBuilder.BuildLeaderInfoBlock(otherTribe.Ruler),
                    }
                },
                new Spacer(0, 5),
                new StackGroup
                {
                    Orientation = Orientation.Horizontal,
                    Children = new List<IUiElement>
                    {
                        new Label($"Started: {Util.FriendlyDateDisplay(war.StartDate)}"),
                        new Spacer(20, 0),
                        new Label($"Score: {(int)(scoreMod * war.WarScore)}"),
                    }
                },
                new Spacer(0, 5),
                new SizePadder(
                    new TableGroup<WarEvent>(
                        new [] {"Event", "Date", "Score"},
                        war.Events.OrderByDescending(e => e.Date),
                        e => BuildEventRow(e, scoreMod),
                        7),
                    minHeight: 200)
                {
                    VerticalAlignment = VerticalAlignment.Top
                },
                new Spacer(0, 5),
                new StackGroup
                {
                    Orientation = Orientation.Horizontal,
                    Children = new List<IUiElement>
                    {
                        new TextButton("Negotiate", 
                            () => 
                            {
                                _dialogManager.PopModal();
                                _dialogManager.PushModal<DiplomacyNegotiationModal, DiplomacyNegotiationModalContext>(
                                    new DiplomacyNegotiationModalContext
                                    {
                                        OtherTribe = otherTribe,
                                        Callback = success => { }
                                    });
                            }, 
                            KeyCode.N, tooltip: "Try to negotiate peace."),
                        new Spacer(10, 0),
                        new TextButton("Back (Esc)", () => BuildWarList(), KeyCode.Escape),
                    }
                },
                new Spacer(0, 10),
            });
        }

        private IEnumerable<IUiElement> BuildEventRow(WarEvent warEvent, double scoreMod)
        {
            return new IUiElement[]
            {
                new Label(warEvent.Name),
                new Label(Util.FriendlyDateDisplay(warEvent.Date)),
                new Label((scoreMod * warEvent.WarScore).ToString("F0"))
            };
        }
    }
}
