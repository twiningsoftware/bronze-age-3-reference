﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bronze.Common.UI.Elements;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Modals;

namespace Bronze.Common.UI.Modals
{
    public class NotablePersonPickerModal : AbstractModal, IModal<NotablePersonPickerModalContext>
    {
        public override bool PausesGame => true;
        protected override int ModalWidth => Elements.Max(e => e.Bounds.Width);
        protected override int ModalHeight => Elements.Max(e => e.Bounds.Height);

        private readonly IDialogManager _dialogManager;
        private readonly IWorldManager _worldManager;
        private StackGroup _stackGroup;
        
        public NotablePersonPickerModal(
            IDialogManager dialogManager,
            IWorldManager worldManager)
            : base(dialogManager)
        {
            _dialogManager = dialogManager;
            _worldManager = worldManager;

            _stackGroup = new StackGroup
            {
                Orientation = Orientation.Vertical,
                HorizontalContentAlignment = HorizontalAlignment.Middle
            };
        }

        public override void OnLoad()
        {
            base.OnLoad();
            Elements.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new Spacer(5, 0),
                    _stackGroup,
                    new Spacer(5, 0)
                }
            });
        }

        public void Initialize(NotablePersonPickerModalContext context)
        {
            _stackGroup.Children.Clear();
            _stackGroup.Children.Add(new Spacer(0, 20));
            _stackGroup.Children.Add(new Label(context.Title));
            _stackGroup.Children.Add(new Spacer(0, 5));

            _stackGroup.Children.Add(
                new StackGroup
                {
                    Orientation = Orientation.Horizontal,
                    Children = new List<IUiElement>
                    {
                        UiBuilder.BuildPersonSummaryButton(_worldManager, _dialogManager, context.CurrentChoice),
                        new StackGroup
                        {
                            Orientation = Orientation.Vertical,
                            Children = new List<IUiElement>
                            {
                                new Label("Current: ", BronzeColor.Grey),
                                new Label(context.CurrentChoice?.Name ?? "none"),
                            }
                        }
                    }
                });

            _stackGroup.Children.Add(new Spacer(0, 5));

            var options = context.Options
                .ToArray();

            var headers = new IUiElement[]
            {
                new Spacer(0,0), // Name
                new Image(UiImages.TINY_TIME, "Age"),
                new Spacer(0,0), // Assignment
                new Image(UiImages.STAT_VALOR_HEADER, "Valor"),
                new Image(UiImages.STAT_WIT_HEADER, "Wit"),
                new Image(UiImages.STAT_CHARISMA_HEADER, "Charisma"),
                new Spacer(0,0),// Button
            };
            
            _stackGroup.Children.Add(new Spacer(0, 10));
            _stackGroup.Children.Add(new TableGroup<NotablePerson>(headers, options, x => BuildRow(x, context), 7));
            _stackGroup.Children.Add(new Spacer(0, 10));
            _stackGroup.Children.Add(new TextButton("Close (Esc)", () => _dialogManager.PopModal(), KeyCode.Escape));
            _stackGroup.Children.Add(new Spacer(0, 20));
        }

        private IEnumerable<IUiElement> BuildRow(
            NotablePerson person,
            NotablePersonPickerModalContext context)
        {
            string invalidReason = null;

            if(person == context.CurrentChoice)
            {
                invalidReason = "Already in role.";
            }
            else if(person.GeneralOf?.IsLocked ?? false)
            {
                invalidReason = "In combat.";
            }
            else
            {
                invalidReason = context.ValidCheck(person);
            }

            var age = (int)((_worldManager.Month - person.BirthDate) / 12);
            
            return new IUiElement[]
            {
                new ToLeftSizePadder(
                    new TextButton(person.Name, () => _dialogManager.PushModal<NotablePersonDetailsModal, NotablePerson>(person))
                    {
                        Tooltip = new SlicedBackground( 
                            new SmallNotablePersonPortrait(() => person))
                    },
                    75),
                new Label(age.ToString()),
                new ToLeftSizePadder(new Label(person.CurrentAssignment, BronzeColor.Grey), 200),
                new Label(person.GetStat(NotablePersonStatType.Valor).ToString()),
                new Label(person.GetStat(NotablePersonStatType.Wit).ToString()),
                new Label(person.GetStat(NotablePersonStatType.Charisma).ToString()),
                new TextButton(
                        "Pick",
                        () =>
                        {
                            context.Callback(person);
                            _dialogManager.PopModal();
                        },
                        disableCheck: () => invalidReason != null,
                        tooltip: invalidReason)
            };
        }
    }

    public class NotablePersonPickerModalContext
    {
        public string Title { get; set; }
        public NotablePerson CurrentChoice { get; set; }
        public Action<NotablePerson> Callback { get; set; }
        public NotablePerson[] Options { get; set; }
        public Func<NotablePerson, string> ValidCheck { get; set; }
    }
}
