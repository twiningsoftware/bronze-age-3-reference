﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Modals;

namespace Bronze.Common.UI.Modals
{
    public class ItemDiscoveryModal : AbstractModal, IModal<Notification>
    {
        public override bool PausesGame => true;
        protected override int ModalWidth => Elements.Max(e => e.Bounds.Width);
        protected override int ModalHeight => Elements.Max(e => e.Bounds.Height);
        
        private readonly IDialogManager _dialogManager;
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IPlayerDataTracker _playerData;
        private StackGroup _stackGroup;
        
        public ItemDiscoveryModal(
            IDialogManager dialogManager,
            IPlayerDataTracker playerData,
            IGamedataTracker gamedataTracker)
            : base(dialogManager)
        {
            _dialogManager = dialogManager;
            _playerData = playerData;
            _gamedataTracker = gamedataTracker;

            _stackGroup = new StackGroup
            {
                Orientation = Orientation.Vertical,
                HorizontalContentAlignment = HorizontalAlignment.Middle
            };
        }

        public override void OnLoad()
        {
            base.OnLoad();
            Elements.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new Spacer(5, 0),
                    _stackGroup,
                    new Spacer(5, 0)
                }
            });
        }

        public void Initialize(Notification notification)
        {
            var item = _gamedataTracker.Items
                .Where(i => i.Name == notification.Data["item"])
                .FirstOrDefault();

            if(item != null)
            {
                var availableStructures = _playerData.PlayerTribe.Race
                    .AvailableStructures
                    .Where(s => s.ConstructionCost.All(ir => _playerData.PlayerTribe.KnownItems[ir.Item]))
                    .ToArray();

                var newStructureIcons = availableStructures
                    .Where(s => !s.UpgradeOnly)
                    .Where(s => s.ConstructionCost.Any(ir => ir.Item == item))
                    .Select(s => UiBuilder.BuildIconFor(s))
                    .ToArray();

                var availableDeveopments = _playerData.PlayerTribe.Race
                    .AvailableCellDevelopments
                    .Where(s => s.ConstructionCost.All(ir => _playerData.PlayerTribe.KnownItems[ir.Item]))
                    .ToArray();

                var newDevelopmentIcons = availableDeveopments
                    .Where(d => !d.UpgradeOnly)
                    .Where(d => d.ConstructionCost.Any(ir => ir.Item == item))
                    .Select(d => UiBuilder.BuildIconFor(d))
                    .ToArray();

                var newRecipieIcons = availableStructures
                    .SelectMany(s => s.Recipies)
                    .Concat(availableDeveopments.SelectMany(d => d.Recipies))
                    .Where(r => r.Inputs.All(ir => _playerData.PlayerTribe.KnownItems[ir.Item]))
                    .Where(r => r.Inputs.Any(ir => ir.Item == item))
                    .Select(r => UiBuilder.BuildIconFor(r))
                    .ToArray();

                var newUnitEquipment = _playerData.PlayerTribe.Race.AvailableUnits
                    .Where(u => u.CanBeTrained)
                    .Where(u => _playerData.PlayerTribe.Settlements.Any(s => s.RecruitmentSlotsMax[u.RecruitmentCategory] > 0))
                    .SelectMany(u => u.EquipmentSets.Select(es => Tuple.Create(u, es)))
                    .Where(t => t.Item2.RecruitmentCost.Any(iq => iq.Item == item))
                    .Where(t => t.Item2.RecruitmentCost.All(iq => _playerData.PlayerTribe.KnownItems[iq.Item]))
                    .Select(t => UiBuilder.BuildIconFor(t.Item1, t.Item2, _playerData.PlayerTribe.PrimaryColor))
                    .ToArray();

                _stackGroup.Children.Clear();
                _stackGroup.Children.Add(new Spacer(400, 20));
                _stackGroup.Children.Add(new Text($"{item.Name} Discovered"));
                _stackGroup.Children.Add(new Spacer(0, 10));
                _stackGroup.Children.Add(new Text($"{item.Description}")
                {
                    Width = Width.Fixed(400)
                });

                var newStack = new StackGroup
                {
                    Orientation = Orientation.Horizontal
                };
                _stackGroup.Children.Add(new Spacer(0, 30));
                _stackGroup.Children.Add(newStack);
                
                if (newStructureIcons.Any())
                {
                    newStack.Children.Add(new Spacer(15, 0));
                    newStack.Children.Add(new StackGroup
                        {
                            Orientation = Orientation.Vertical,
                            Children = new Label("New Structures").Yield()
                                    .Concat(UiBuilder.DoRowLayout(newStructureIcons, 3, 5))
                                    .ToList()
                        });
                }
                 
                if (newDevelopmentIcons.Any())
                {
                    newStack.Children.Add(new Spacer(15, 0));
                    newStack.Children.Add(new StackGroup
                        {
                            Orientation = Orientation.Vertical,
                            Children = new Label("New Developments").Yield()
                                .Concat(UiBuilder.DoRowLayout(newDevelopmentIcons, 3, 5))
                                .ToList()
                        });
                }

                if (newRecipieIcons.Any())
                {
                    newStack.Children.Add(new Spacer(15, 0));
                    newStack.Children.Add(new StackGroup
                    {
                        Orientation = Orientation.Vertical,
                        Children = new Label("New Recipies").Yield()
                            .Concat(UiBuilder.DoRowLayout(newRecipieIcons, 3, 5))
                            .ToList()
                    });
                }

                if (newUnitEquipment.Any())
                {
                    newStack.Children.Add(new Spacer(15, 0));
                    newStack.Children.Add(new StackGroup
                    {
                        Orientation = Orientation.Vertical,
                        Children = new Label("New Units").Yield()
                            .Concat(UiBuilder.DoRowLayout(newUnitEquipment, 3, 5))
                            .ToList()
                    });
                }

                _stackGroup.Children.Add(new Spacer(0, 20));
                _stackGroup.Children.Add(new TextButton("Close (Esc)", () => _dialogManager.PopModal(), KeyCode.Escape));
                _stackGroup.Children.Add(new Spacer(0, 20));
            }
            else
            {
                _dialogManager.PopModal();
            }
        }
    }
}
