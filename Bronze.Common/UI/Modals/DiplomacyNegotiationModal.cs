﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Diplomacy;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Modals;

namespace Bronze.Common.UI.Modals
{
    public class DiplomacyNegotiationModal : AbstractModal, IModal<DiplomacyNegotiationModalContext>
    {
        public override bool PausesGame => true;
        protected override int ModalWidth => Elements.Max(e => e.Bounds.Width);
        protected override int ModalHeight => Elements.Max(e => e.Bounds.Height);

        private readonly IDialogManager _dialogManager;
        private readonly IWorldManager _worldManager;
        private readonly IPlayerDataTracker _playerData;
        private readonly ITreatyHelper _treatyHelper;
        private readonly IDiplomacyHelper _diplomacyHelper;

        private StackGroup _stackGroup;
        private StackGroup _weOfferColumn;
        private StackGroup _theyOfferColumn;
        private StackGroup _currentColumn;

        private Tribe _otherTribe;
        private List<ITreaty> _proposedTreaties;
        private List<ITreaty> _aiWants;
        private int _value;
        
        public DiplomacyNegotiationModal(
            IDialogManager dialogManager,
            IWorldManager worldManager,
            IPlayerDataTracker playerData,
            ITreatyHelper treatyHelper,
            IDiplomacyHelper diplomacyHelper)
            : base(dialogManager)
        {
            _dialogManager = dialogManager;
            _worldManager = worldManager;
            _playerData = playerData;
            _treatyHelper = treatyHelper;
            _diplomacyHelper = diplomacyHelper;

            _proposedTreaties = new List<ITreaty>();

            _stackGroup = new StackGroup
            {
                Orientation = Orientation.Vertical,
                HorizontalContentAlignment = HorizontalAlignment.Middle
            };

            _weOfferColumn = new StackGroup
            {
                Orientation = Orientation.Vertical,
            };
            _theyOfferColumn = new StackGroup
            {
                Orientation = Orientation.Vertical,
            };
            _currentColumn = new StackGroup
            {
                Orientation = Orientation.Vertical,
            };
        }

        public override void OnLoad()
        {
            base.OnLoad();

            Elements.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new Spacer(10, 0),
                    _stackGroup,
                    new Spacer(10, 0)
                }
            });
        }

        public void Initialize(DiplomacyNegotiationModalContext context)
        {
            _otherTribe = context.OtherTribe;

            _stackGroup.Children.Clear();
            _stackGroup.Children.Add(new Spacer(0, 10));
            _stackGroup.Children.Add(new Label($"Negotiating with {context.OtherTribe.Name}"));
            _stackGroup.Children.Add(new Spacer(0, 10));
            _stackGroup.Children.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    _weOfferColumn,
                    new Spacer(10, 300),
                    _currentColumn,
                    new Spacer(10, 300),
                    _theyOfferColumn
                }
            });
            _stackGroup.Children.Add(new Spacer(0, 10));
            
            _stackGroup.Children.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new TextButton("Accept",
                        () =>
                        {
                            ApplyNewTreaties();
                            _dialogManager.PopModal();
                            context.Callback(true);
                        },
                        KeyCode.A,
                        disableCheck: () => !IsAcceptable()),
                    new Spacer(20, 0),
                    new TextButton(
                        "Cancel (Esc)",
                        () => 
                        {
                            _dialogManager.PopModal();
                            context.Callback(false);

                            foreach(var treaty in _treatyHelper.GetTreatiesFor(_otherTribe, _playerData.PlayerTribe))
                            {
                                treaty.RevertChanges();
                            }
                        },
                        KeyCode.Escape)
                }
            });
            _stackGroup.Children.Add(new Spacer(0, 10));
            
            _proposedTreaties.Clear();
            _proposedTreaties.AddRange(_treatyHelper.GetTreatiesFor(_otherTribe, _playerData.PlayerTribe));
            _aiWants = _otherTribe.Controller.GetNegotiationAiWants(_proposedTreaties).ToList();
            _proposedTreaties.AddRange(_aiWants);

            RepopulateColumns();
        }

        private bool IsAcceptable()
        {
            return _value >= 0
                || (_proposedTreaties.ContainsAll(_aiWants) && _aiWants.Any() && !_proposedTreaties.Except(_aiWants).Any());
        }

        private void RepopulateColumns()
        {
            _weOfferColumn.Children.Clear();
            _weOfferColumn.Children.Add(new SizePadder(new Label("We Give"), minWidth: 150));
            _weOfferColumn.Children.Add(new Spacer(0, 10));
            _weOfferColumn.Children.AddRange(
                _otherTribe.Controller.GetNegotiationPlayerOptions(_proposedTreaties)
                    .Select(x => new TextButton(x.Name, () => AddTreatyOption(x), tooltip: x.Description)));

            _theyOfferColumn.Children.Clear();
            _theyOfferColumn.Children.Add(new SizePadder(new Label("They Give"), minWidth: 150));
            _theyOfferColumn.Children.Add(new Spacer(0, 10));
            _theyOfferColumn.Children.AddRange(
                _otherTribe.Controller.GetNegotiationAiOptions(_proposedTreaties)
                    .Select(x => new TextButton(x.Name, () => AddTreatyOption(x), tooltip: x.Description)));

            _currentColumn.Children.Clear();
            _currentColumn.Children.Add(new SizePadder(new IconText(string.Empty, "Balance: ")
            {
                ColorUpdater = () => _value < 0 ? BronzeColor.Red : (_value > 0 ? BronzeColor.Green : BronzeColor.Yellow),
                ContentUpdater = () => "Balance: " + _value,
                Tooltip = new SlicedBackground(new Text("")
                {
                    Width = Width.Fixed(200),
                    ContentUpdater = () => _value >= 0 ? "This deal is acceptable" : (IsAcceptable() ? "They will accept this deal only becuase their demands are met." : "This deal is unacceptable.")
                })
            }, minWidth: 150));
            _currentColumn.Children.Add(new Spacer(0, 10));
            _currentColumn.Children.AddRange(_proposedTreaties
                .Select(x => new StackGroup
                {
                    Orientation = Orientation.Horizontal,
                    Children = new List<IUiElement>
                    {
                        BuildTreatyButton(x, () => EditTreaty(x)),
                        new TextButton("X", () => RemoveProposedTreaty(x), disableCheck: () => !x.CanBeCancelled)
                    }
                }));
            
            _value = _proposedTreaties.Sum(t => t.GetValueDetailFor(_otherTribe).Sum(vd => vd.Value)) 
                    + Math.Max(0, _diplomacyHelper.GetInfluenceFor(_otherTribe));
        }

        private IUiElement BuildTreatyButton(ITreaty treaty, Action clickAction)
        {
            var treatyValue = treaty.GetValueDetailFor(_otherTribe);
            var treatyValueSum = treatyValue.Sum(t => t.Value);

            var stack = new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new Label(treaty.Name),
                    new Label(" " + treatyValueSum, treatyValueSum > 0 ? BronzeColor.Green : treatyValueSum < 0 ? BronzeColor.Red : BronzeColor.Yellow)
                }
            };

            return new ContainerButton(
                stack,
                clickAction)
            {
                Tooltip = new SlicedBackground(
                    new StackGroup
                    {
                        Orientation = Orientation.Vertical,
                        Width = Width.Fixed(200),
                        Children = new List<IUiElement>
                        {
                            new Text(treaty.Description)
                        }
                        .Concat(treatyValue
                            .Where(tv=>tv.Value != 0)
                            .Select(tv => new Text($"{tv.Reason}: {tv.Value}", tv.Value > 0 ? BronzeColor.Green : BronzeColor.Red)))
                        .ToList()
                    })
            };
        }

        private void RemoveProposedTreaty(ITreaty treaty)
        {
            _proposedTreaties.Remove(treaty);

            RepopulateColumns();
        }

        private void AddTreatyOption(ITreatyOption treatyOption)
        {
            if(!treatyOption.HasParameters)
            {
                _proposedTreaties.Add(treatyOption.BuildTreaty());
                RepopulateColumns();
            }
            else
            {
                treatyOption.ShowEditor(
                    newTreaty =>
                    {
                        if(newTreaty != null)
                        {
                            _proposedTreaties.Add(newTreaty);
                        }

                        RepopulateColumns();
                    },
                    _proposedTreaties);
            }
        }

        private void EditTreaty(ITreaty treaty)
        {
            treaty.ShowEditor(
                () => RepopulateColumns(),
                _proposedTreaties.Except(treaty.Yield()).ToArray());
        }

        private void ApplyNewTreaties()
        {
            var treatiesToRemove = _treatyHelper
                .GetTreatiesFor(_otherTribe, _playerData.PlayerTribe)
                .Except(_proposedTreaties)
                .ToArray();

            var treatiesToAdd = _proposedTreaties
                .Except(_treatyHelper.GetTreatiesFor(_otherTribe, _playerData.PlayerTribe))
                .ToArray();

            foreach (var treaty in treatiesToRemove)
            {
                _treatyHelper.RemoveTreaty(treaty);
            }

            foreach(var treaty in treatiesToAdd)
            {
                _treatyHelper.AddTreaty(treaty);
            }

            foreach(var treaty in _proposedTreaties.Except(treatiesToAdd))
            {
                treaty.ApplyChanges();
            }
        }
    }

    public class DiplomacyNegotiationModalContext
    {
        public Tribe OtherTribe { get; set; }
        public Action<bool> Callback { get; set; }
    }
}
