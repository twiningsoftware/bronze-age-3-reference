﻿using System.Collections.Generic;
using System.Linq;
using Bronze.Common.UI.Elements;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Modals;

namespace Bronze.Common.UI.Modals
{
    public class GenericNotificationModal : AbstractModal, IModal<GenericNotificationModalContext>
    {
        public override bool PausesGame => true;
        protected override int ModalWidth => Elements.Max(e => e.Bounds.Width);
        protected override int ModalHeight => Elements.Max(e => e.Bounds.Height);

        private readonly IDialogManager _dialogManager;
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IPlayerDataTracker _playerData;
        private readonly IWorldManager _worldManager;
        private StackGroup _stackGroup;

        public GenericNotificationModal(
            IDialogManager dialogManager,
            IPlayerDataTracker playerData,
            IGamedataTracker gamedataTracker,
            IWorldManager worldManager)
            : base(dialogManager)
        {
            _dialogManager = dialogManager;
            _playerData = playerData;
            _gamedataTracker = gamedataTracker;
            _worldManager = worldManager;

            _stackGroup = new StackGroup
            {
                Orientation = Orientation.Vertical,
                HorizontalContentAlignment = HorizontalAlignment.Middle
            };
        }

        public override void OnLoad()
        {
            base.OnLoad();
            Elements.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new Spacer(5, 0),
                    _stackGroup,
                    new Spacer(5, 0)
                }
            });
        }

        public void Initialize(GenericNotificationModalContext context)
        {
            var topRow = new StackGroup
            {
                Orientation = Orientation.Horizontal,
                VerticalContentAlignment = VerticalAlignment.Middle,
                Children = new List<IUiElement>
                {
                    new Spacer(20, 0),
                    new Label(context.Title)
                }
            };

            if (context.LookAt != null)
            {
                topRow.Children.Add(new Spacer(10, 0));
                topRow.Children.Add(new IconButton(
                    UiImages.IconView,
                    string.Empty,
                    () =>
                    {
                        _playerData.LookAt(context.LookAt.Value);
                    },
                    tooltip: "Zoom to the location."));
            }
            else
            {
                topRow.Children.Add(new Spacer(20, 0));
            }

            var bodyLeft = new StackGroup
            {
                Orientation = Orientation.Vertical
            };
            var bodyRight = new StackGroup
            {
                Orientation = Orientation.Vertical,
                Width = Width.Fixed(400)
            };

            foreach (var body in context.Body)
            {
                bodyRight.Children.Add(new Text(body));
                bodyRight.Children.Add(new Spacer(0, 15));
            }

            if(context.AdvisorIcon != null)
            {
                bodyLeft.Children.Add(new Image(context.AdvisorIcon));
            }

            if (context.NotablePerson != null)
            {
                bodyLeft.Children.Add(UiBuilder.BuildPersonSummaryButton(_worldManager, _dialogManager, context.NotablePerson));
            }
            if (context.TribeA != null)
            {
                bodyLeft.Children.Add(new BiColoredImage(
                    context.TribeA.Race.ArmyFlag,
                    context.TribeA.Race.ArmyFlagIcon,
                    context.TribeA.Name)
                {
                    PrimaryColorizationUpdater = () => context.TribeA.PrimaryColor,
                    SecondaryColorizationUpdater = () => context.TribeA.SecondaryColor
                });
            }
            if (context.TribeB != null)
            {
                bodyLeft.Children.Add(new Spacer(0, 10));

                bodyLeft.Children.Add(new BiColoredImage(
                    context.TribeB.Race.ArmyFlag,
                    context.TribeB.Race.ArmyFlagIcon,
                    context.TribeB.Name)
                {
                    PrimaryColorizationUpdater = () => context.TribeB.PrimaryColor,
                    SecondaryColorizationUpdater = () => context.TribeB.SecondaryColor
                });
            }
            if(context.SkillPlacement != null)
            {
                bodyRight.Children.Add(new Spacer(0, 10));
                bodyRight.Children.Add(UiBuilder.BuildDisplayFor(context.SkillPlacement));
            }

            if(context.Buff != null)
            {
                bodyRight.Children.Add(new Spacer(0, 10));
                bodyRight.Children.Add(new BuffDisplay(() => context.Buff.Yield()));
            }

            _stackGroup.Children.Clear();
            _stackGroup.Children.Add(new Spacer(0, 20));
            _stackGroup.Children.Add(topRow);
            _stackGroup.Children.Add(new Spacer(0, 10));
            _stackGroup.Children.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    bodyLeft,
                    new Spacer(20, 0),
                    bodyRight
                }
            });
            _stackGroup.Children.Add(new Spacer(0, 20));
            _stackGroup.Children.Add(new TextButton("Close (Esc)", () => _dialogManager.PopModal(), KeyCode.Escape));
            _stackGroup.Children.Add(new Spacer(0, 20));
        }
    }

    public class GenericNotificationModalContext
    {
        public string Title { get; set; }
        public IEnumerable<string> Body { get; set; }
        public string AdvisorIcon { get; set; }
        public CellPosition? LookAt { get; set; }
        public NotablePerson NotablePerson { get; set; }
        public Tribe TribeA { get; set; }
        public Tribe TribeB { get; set; }
        public NotablePersonSkillPlacement SkillPlacement { get; set; }
        public Buff Buff { get; set; }

        public GenericNotificationModalContext()
        {
            Title = string.Empty;
            Body = Enumerable.Empty<string>();
            LookAt = null;
            NotablePerson = null;
            TribeA = null;
            TribeB = null;
            AdvisorIcon = null;
            Buff = null;
        }
    }
}
