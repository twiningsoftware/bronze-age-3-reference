﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bronze.Common.Data.Diplomacy;
using Bronze.Common.Gameplay.UiElements;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Diplomacy;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Modals;

namespace Bronze.Common.UI.Modals
{
    public class MarriageTreatyBuilderModal : AbstractModal, IModal<MarriageTreatyBuilderModalContext>
    {
        public override bool PausesGame => true;
        protected override int ModalWidth => Elements.Max(e => e.Bounds.Width);
        protected override int ModalHeight => Elements.Max(e => e.Bounds.Height);

        private readonly IDialogManager _dialogManager;
        private readonly IPlayerDataTracker _playerData;
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IInjectionProvider _injectionProvider;
        private StackGroup _stackGroup;
        private TradePickerMinimapElement _minimap;

        private Tribe _fromTribe;
        private NotablePerson _fromSpouse;
        private double _fromSpouseScore;
        private Tribe _toTribe;
        private NotablePerson _toSpouse;
        private Action<MarriageTreaty> _callback;
        private ITreaty[] _treatiesInNegotiation;
        
        public MarriageTreatyBuilderModal(
            IDialogManager dialogManager,
            IPlayerDataTracker playerData,
            IGamedataTracker gamedataTracker,
            IInjectionProvider injectionProvider,
            IWorldManager worldManager)
            : base(dialogManager)
        {
            _dialogManager = dialogManager;
            _playerData = playerData;
            _gamedataTracker = gamedataTracker;
            _injectionProvider = injectionProvider;
            
            _stackGroup = new StackGroup
            {
                Orientation = Orientation.Vertical,
                HorizontalContentAlignment = HorizontalAlignment.Middle
            };

            _minimap = new TradePickerMinimapElement(playerData, worldManager)
            {
                Width = Width.Fixed(300),
                Height = Height.Fixed(300)
            };
        }

        public override void OnLoad()
        {
            base.OnLoad();
            Elements.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new Spacer(5, 200),
                    _stackGroup,
                    new Spacer(5, 200)
                }
            });
        }

        public void Initialize(MarriageTreatyBuilderModalContext context)
        {
            _fromTribe = context.From;
            _toTribe = context.To;
            _callback = context.Callback;
            _treatiesInNegotiation = context.TreatiesInNegotiation;
            
            RebuildContent();
        }

        private void RebuildContent()
        {
            var fromVerb = "";
            if (_fromTribe == _playerData.PlayerTribe)
            {
                fromVerb = "To Send From";
            }
            else
            {
                fromVerb = "To Recieve From";
            }

            var fromPicker = new StackGroup
            {
                Orientation = Orientation.Vertical,
                Children = new List<IUiElement>
                {
                    new Label($"Spouse {fromVerb } {_fromTribe.Name}"),
                    new TextButton(_fromSpouse == null ? "Pick" : $"{_fromSpouse.Name}",
                        () => PickFrom(_fromTribe),
                        tooltip: $"Pick a spouse from {_fromTribe.Name}."),
                    new Spacer(0, 10),
                    UiBuilder.BuildLeaderInfoBlock(_fromSpouse)
                }
            };

            var toPicker = new StackGroup
            {
                Orientation = Orientation.Vertical,
                Children = new List<IUiElement>
                {
                    new Label($"Spouse From {_toTribe.Name}"),
                    new TextButton(_toSpouse == null ? "Pick" : $"{_toSpouse.Name}",
                        () => PickTo(_toTribe),
                        tooltip: $"Pick a spouse from {_toTribe.Name}.",
                        disableCheck: () => _fromSpouse == null),
                    new Spacer(0, 10),
                    UiBuilder.BuildLeaderInfoBlock(_toSpouse)
                }
            };
            
            _stackGroup.Children.Clear();
            _stackGroup.Children.Add(new Spacer(0, 20));
            if(_fromTribe == _playerData.PlayerTribe)
            {
                _stackGroup.Children.Add(new Label($"Send a Spouse to {_toTribe.Name}"));
            }
            else
            {
                _stackGroup.Children.Add(new Label($"Recieve a Spouse from {_fromTribe.Name}"));
            }

            
            _stackGroup.Children.Add(new Spacer(0, 5));
            _stackGroup.Children.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new Spacer(10, 0),
                    fromPicker,
                    new Spacer(20, 300),
                    toPicker,
                    new Spacer(10, 0),
                }
            });
            
            _stackGroup.Children.Add(new Spacer(0, 5));
            _stackGroup.Children.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new Label("Value: "),
                    new Label(_fromSpouseScore.ToString(), _playerData.PlayerTribe == _fromTribe ? BronzeColor.Green : BronzeColor.Red)
                }
            });
            _stackGroup.Children.Add(new Spacer(0, 10));
            _stackGroup.Children.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new TextButton("Save",
                        () =>
                        {
                            _dialogManager.PopModal();

                            _callback.Invoke(_injectionProvider
                                .Build<MarriageTreaty>().Initialize(
                                _fromTribe,
                                _toTribe,
                                _fromSpouse,
                                _toSpouse,
                                _fromSpouseScore));
                        },
                        KeyCode.S,
                        disableCheck: () => _fromSpouse == null || _toSpouse == null),
                    new Spacer(20, 0),
                    new TextButton("Cancel (Esc)",
                        () =>
                        {
                            _dialogManager.PopModal();
                            _callback(null);
                        }, KeyCode.Escape)
                }
            });
            _stackGroup.Children.Add(new Spacer(0, 20));
        }
        
        private void PickFrom(Tribe fromTribe)
        {
            var options = fromTribe
                .NotablePeople
                .Where(np => !np.IsDead && np.IsMature && np.Spouse == null)
                .Except(_treatiesInNegotiation.OfType<MarriageTreaty>().Select(mt => mt.FromSpouse))
                .Except(_treatiesInNegotiation.OfType<MarriageTreaty>().Select(mt => mt.ToSpouse))
                .ToArray();

            _dialogManager.PushModal<NotablePersonPickerModal, NotablePersonPickerModalContext>(
                new NotablePersonPickerModalContext
                {
                    Title = "Pick a Spouse From " + _fromTribe.Name,
                    Options = options,
                    ValidCheck = (np) => null,
                    Callback = np =>
                    {
                        _fromSpouse = np;
                        _toSpouse = null;
                        _fromSpouseScore = _fromSpouse.GetStat(NotablePersonStatType.Charisma)
                            + _fromSpouse.GetStat(NotablePersonStatType.Valor)
                            + _fromSpouse.GetStat(NotablePersonStatType.Wit);
                        RebuildContent();
                    }
                });
        }

        private void PickTo(Tribe toTribe)
        {
            var options = toTribe
                .NotablePeople
                .Where(np => !np.IsDead && np.IsMature && np.Spouse == null)
                .Where(np => np.IsMale != _fromSpouse.IsMale)
                .Except(_treatiesInNegotiation.OfType<MarriageTreaty>().Select(mt => mt.FromSpouse))
                .Except(_treatiesInNegotiation.OfType<MarriageTreaty>().Select(mt => mt.ToSpouse))
                .ToArray();

            _dialogManager.PushModal<NotablePersonPickerModal, NotablePersonPickerModalContext>(
                new NotablePersonPickerModalContext
                {
                    Title = "Pick a Spouse From " + _toTribe.Name,
                    Options = options,
                    ValidCheck = (np) => null,
                    Callback = np =>
                    {
                        _toSpouse = np;
                        RebuildContent();
                    }
                });
        }
    }

    public class MarriageTreatyBuilderModalContext
    {
        public Tribe From { get; set; }
        public Tribe To { get; set; }
        public Action<MarriageTreaty> Callback { get; set; }
        public ITreaty[] TreatiesInNegotiation { get; set; } 
    }
}
