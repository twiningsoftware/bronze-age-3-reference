﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bronze.Common.UI.Elements;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Modals;

namespace Bronze.Common.UI.Modals
{
    public class NotablePersonDetailsModal : AbstractModal, IModal<NotablePerson>
    {
        public override bool PausesGame => true;
        protected override int ModalWidth => Elements.Max(e => e.Bounds.Width);
        protected override int ModalHeight => Elements.Max(e => e.Bounds.Height);

        private readonly IDialogManager _dialogManager;
        private readonly IWorldManager _worldManager;
        private StackGroup _stackGroup;
        
        public NotablePersonDetailsModal(
            IDialogManager dialogManager,
            IWorldManager worldManager)
            : base(dialogManager)
        {
            _dialogManager = dialogManager;
            _worldManager = worldManager;

            _stackGroup = new StackGroup
            {
                Orientation = Orientation.Vertical,
                HorizontalContentAlignment = HorizontalAlignment.Middle
            };
        }

        public override void OnLoad()
        {
            base.OnLoad();
            Elements.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new Spacer(5, 0),
                    _stackGroup,
                    new Spacer(5, 0)
                }
            });
        }

        public void Initialize(NotablePerson person)
        {
            var titles = new List<string>();
            
            if(person.Owner.Ruler == person)
            {
                titles.Add("Ruler");
            }
            if (person.Owner.Heir == person)
            {
                titles.Add("Heir");
            }

            titles.Add(person.CurrentAssignment);

            var title = string.Join(", ", titles.Where(s => !string.IsNullOrWhiteSpace(s)));
            
            _stackGroup.Children.Clear();
            _stackGroup.Children.Add(new Spacer(0, 10));
            _stackGroup.Children.Add(
                new StackGroup
                {
                    Orientation = Orientation.Horizontal,
                    Children = new List<IUiElement>
                    {
                        new NotablePersonPortrait(() => person),
                        new Spacer(10, 0),
                        new StackGroup
                        {
                            Orientation = Orientation.Vertical,
                            Children = new List<IUiElement>
                            {
                                new TextInput(() => person.Name, val => person.Name = val, 200),
                                new Label(title, BronzeColor.Grey),
                                new Label(person.IsDead ? "Dead" : $"Age: {(int)((_worldManager.Month - person.BirthDate) / 12)}"),
                                UiBuilder.BuildDisplayFor(person.CultMembership)
                            }
                        },
                        new Spacer(10, 0),
                        new StackGroup
                        {
                            Orientation = Orientation.Vertical,
                            Children = new List<IUiElement>
                            {
                                UiBuilder.BuildStatDisplay(person, NotablePersonStatType.Valor),
                                UiBuilder.BuildStatDisplay(person, NotablePersonStatType.Wit),
                                UiBuilder.BuildStatDisplay(person, NotablePersonStatType.Charisma)
                            }
                        }
                    }
                });

            var relationColumn = new StackGroup
            {
                Orientation = Orientation.Vertical,
                Children = new List<IUiElement>
                {
                    new Spacer(150, 0),
                    new StackGroup
                    {
                        Orientation = Orientation.Horizontal,
                        Children = new List<IUiElement>
                        {
                            new SizePadder(new Label("Father"), 50),
                            new SizePadder(new Label("Mother"), 50),
                            new SizePadder(new Label("Spouse"), 50),
                        }
                    },
                    new StackGroup
                    {
                        Orientation = Orientation.Horizontal,
                        Children = new List<IUiElement>
                        {
                            new SizePadder(UiBuilder.BuildPersonSummaryButton(_worldManager, _dialogManager, person.Father), 50),
                            new SizePadder(UiBuilder.BuildPersonSummaryButton(_worldManager, _dialogManager, person.Mother), 50),
                            new SizePadder(UiBuilder.BuildPersonSummaryButton(_worldManager, _dialogManager, person.Spouse), 50),
                        }
                    }
                }
            };
            
            if (person.Children.Any())
            {
                relationColumn.Children.Add(new Label("Children"));
                for(var i = 0; i < person.Children.Count; i+= 3)
                {
                    relationColumn.Children.Add(new StackGroup
                    {
                        Orientation = Orientation.Horizontal,
                        Children = person.Children.Skip(i)
                            .Take(3)
                            .Select(c => new SizePadder(UiBuilder.BuildPersonSummaryButton(_worldManager, _dialogManager, c), 50))
                            .ToList<IUiElement>()
                    });

                    relationColumn.Children.Add(new Spacer(0, 5));
                }
            }

            var skillColumn = new StackGroup
            {
                Orientation = Orientation.Vertical,
                Children = new List<IUiElement>
                {
                    new Spacer(200, 0),
                    new ScrollableList(10, person.Skills.Select(s => UiBuilder.BuildDisplayFor(s)).ToArray())
                }
            };
            
            _stackGroup.Children.Add(new Spacer(0, 5));
            _stackGroup.Children.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    skillColumn,
                    new Spacer(20, 0),
                    relationColumn
                }
            });
            _stackGroup.Children.Add(new Spacer(0, 20));
            _stackGroup.Children.Add(new TextButton("Close (Esc)", () => _dialogManager.PopModal(), KeyCode.Escape));
            _stackGroup.Children.Add(new Spacer(0, 20));
        }
    }
}
