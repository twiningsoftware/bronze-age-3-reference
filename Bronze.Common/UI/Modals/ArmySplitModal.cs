﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bronze.Common.UI.Elements;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Modals;

namespace Bronze.Common.UI.Modals
{
    public class ArmySplitModal : AbstractModal, IModal<ArmySplitModalContext>
    {
        public override bool PausesGame => true;
        protected override int ModalWidth => Elements.Max(e => e.Bounds.Width);
        protected override int ModalHeight => Elements.Max(e => e.Bounds.Height);

        private StackGroup _stackGroup;
        private readonly IDialogManager _dialogManager;
        private readonly IPlayerDataTracker _playerData;
        private readonly IUnitHelper _unitHelper;
        private readonly IWorldManager _worldManager;

        private ArmySplitModalContext _context;
        private List<IUnit> _stayUnits;
        private List<IUnit> _splitUnits;
        private NotablePerson _splitLeader;

        public ArmySplitModal(
            IDialogManager dialogManager,
            IPlayerDataTracker playerData,
            IUnitHelper unitHelper,
            IWorldManager worldManager)
            : base(dialogManager)
        {
            _dialogManager = dialogManager;
            _playerData = playerData;
            _unitHelper = unitHelper;
            _worldManager = worldManager;

            _stackGroup = new StackGroup
            {
                Orientation = Orientation.Vertical,
                HorizontalContentAlignment = HorizontalAlignment.Middle
            };

            _stayUnits = new List<IUnit>();
            _splitUnits = new List<IUnit>();
        }

        public override void OnLoad()
        {
            base.OnLoad();
            Elements.Add(_stackGroup);
        }

        public void Initialize(ArmySplitModalContext context)
        {
            _context = context;

            _stayUnits.AddRange(_context.SourceArmy.Units);

            Rebuild();
        }

        private void Rebuild()
        {
            _stackGroup.Children.Clear();
            _stackGroup.Children.Add(new Spacer(0, 20));

            BuildContentFor(
                    _context.SourceArmy.Owner,
                    _context.SourceArmy.General,
                    _context.SourceArmy.Name);

            _stackGroup.Children.Add(new Spacer(0, 10));
            _stackGroup.Children.Add(new Label(
                "The new army must have at least one unit.",
                BronzeColor.Yellow,
                () => !_splitUnits.Any()));
            _stackGroup.Children.Add(new Label(
                "The source army must have at least one unit.",
                BronzeColor.Yellow,
                () => !_stayUnits.Any()));
            _stackGroup.Children.Add(new Spacer(0, 10));
            _stackGroup.Children.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new TextButton(
                        "Accept", 
                        () =>
                        {
                            DoSplit();
                            _dialogManager.PopModal();
                        },
                        KeyCode.A,
                        disableCheck: () => !_stayUnits.Any() && _splitUnits.Any()),
                    new Spacer(20, 0),
                    new TextButton("Close (Esc)", () => _dialogManager.PopModal(), KeyCode.Escape)
                }
            });
            _stackGroup.Children.Add(new Spacer(0, 20));
        }

        private void DoSplit()
        {
            var ownerTribe = _context.SourceArmy?.Owner;
            var sourceTransport = _context?.SourceArmy?.TransportType;

            Cell position;

            if(sourceTransport == null)
            {
                position = _context.PotentialSpawns
                    .Where(c => _splitUnits.All(u => u.CanPath(c)))
                    .Where(c => c.Owner == null || c.Owner.AllowsAccess(ownerTribe))
                    .FirstOrDefault();
            }
            else
            {
                position = _context.PotentialSpawns
                    .Where(c => sourceTransport.CanPath(c))
                    .Where(c => c.Owner == null || c.Owner.AllowsAccess(ownerTribe))
                    .FirstOrDefault();
            }

            if (position != null)
            {
                var army = _unitHelper.CreateArmy(ownerTribe, position);

                if(_context.SourceArmy != null)
                {
                    army.TransportType = _context.SourceArmy.TransportType;
                }

                if (_splitLeader != null)
                {
                    _splitLeader.AssignTo(army);
                }
                
                army.Units.AddRange(_splitUnits);

                foreach (var unit in _splitUnits)
                {
                    _context.SourceArmy.Units.Remove(unit);
                }

                // If the player doesn't have an army selected, and just created one
                // then helpfully select  that army.
                if (ownerTribe == _playerData.PlayerTribe && _playerData.SelectedArmy == null)
                {
                    _playerData.SelectedArmy = army;
                }
            }
        }

        private void BuildContentFor(
            Tribe sourceTribe,
            NotablePerson sourceLeader, 
            string sourceName)
        {
            var stayElements = new List<IUiElement>();
            var splitElements = new List<IUiElement>();
            
            for (var i = 0; i < Constants.UNITS_PER_ARMY; i++)
            {
                if (i < _stayUnits.Count)
                {
                    var unit = _stayUnits[i];

                    stayElements.Add(new ContainerButton(
                        new UnitSlotDisplay(sourceTribe, unit),
                        () =>
                        {
                            _splitUnits.Add(unit);
                            _stayUnits.Remove(unit);
                            Rebuild();
                        },
                        disableCheck: () => _splitUnits.Count >= Constants.UNITS_PER_ARMY));
                }
                else
                {
                    stayElements.Add(new Spacer(UnitSlotDisplay.ExpectedWidth, UnitSlotDisplay.ExpectedHeight));
                }

                if (i < _splitUnits.Count)
                {
                    var unit = _splitUnits[i];

                    splitElements.Add(new ContainerButton(
                        new UnitSlotDisplay(sourceTribe, unit),
                        () =>
                        {
                            _stayUnits.Add(unit);
                            _splitUnits.Remove(unit);
                            Rebuild();
                        },
                        disableCheck: () => _stayUnits.Count >= Constants.UNITS_PER_ARMY));
                }
                else
                {
                    splitElements.Add(new Spacer(UnitSlotDisplay.ExpectedWidth, UnitSlotDisplay.ExpectedHeight));
                }
            }
            
            _stackGroup.Children.Add(new Label($"Form A New Army"));
            _stackGroup.Children.Add(new Spacer(0, 10));
            _stackGroup.Children.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new Spacer(10, 0),
                    new StackGroup
                    {
                        Orientation = Orientation.Vertical,
                        Children = new List<IUiElement>
                        {
                            new SizePadder(new Label(sourceName), minWidth: 200),
                            UiBuilder.BuildSmallLeaderInfoBlock(sourceLeader, _worldManager),
                            new Spacer(200, 10),
                            new StackGroup
                            {
                                Orientation = Orientation.Horizontal,
                                Children = new List<IUiElement>
                                {
                                    new StackGroup
                                    {
                                        Orientation = Orientation.Vertical,
                                        Children = stayElements
                                            .Take(Constants.UNITS_PER_ARMY / 2)
                                            .ToList()
                                    },
                                    new Spacer(5, 0),
                                    new StackGroup
                                    {
                                        Orientation = Orientation.Vertical,
                                        Children = stayElements
                                            .Skip(Constants.UNITS_PER_ARMY / 2)
                                            .ToList()
                                    }
                                }
                            }
                        }
                    },
                    new Spacer(20, 0),
                    new StackGroup
                    {
                        Orientation = Orientation.Vertical,
                        Children = new List<IUiElement>
                        {
                            new SizePadder(new Label("New Army"), minWidth: 200),
                            new StackGroup
                            {
                                Orientation = Orientation.Horizontal,
                                Children = new List<IUiElement>
                                {
                                    UiBuilder.BuildSmallLeaderInfoBlock(_splitLeader, _worldManager),
                                    new Spacer(10, 0),
                                    new TextButton("Change Leader",
                                        () =>
                                        {
                                            _dialogManager.PushModal<NotablePersonPickerModal,NotablePersonPickerModalContext>(
                                                new NotablePersonPickerModalContext
                                                {
                                                    Title = "Pick a General",
                                                    Options = _playerData.PlayerTribe.NotablePeople
                                                        .Where(np => !np.IsDead && np.IsMature)
                                                        .ToArray(),
                                                    CurrentChoice = _splitLeader,
                                                    Callback = pick =>
                                                    {
                                                        _splitLeader = pick;
                                                        Rebuild();
                                                    },
                                                    ValidCheck = np => null
                                                });
                                        })
                                }
                            },
                            new Spacer(200, 10),
                            new StackGroup
                            {
                                Orientation = Orientation.Horizontal,
                                Children = new List<IUiElement>
                                {
                                    new StackGroup
                                    {
                                        Orientation = Orientation.Vertical,
                                        Children = splitElements
                                            .Take(Constants.UNITS_PER_ARMY / 2)
                                            .ToList()
                                    },
                                    new Spacer(5, 0),
                                    new StackGroup
                                    {
                                        Orientation = Orientation.Vertical,
                                        Children = splitElements
                                            .Skip(Constants.UNITS_PER_ARMY / 2)
                                            .ToList()
                                    }
                                }
                            }
                        }
                    },
                    new Spacer(10, 0)
                }
            });
        }
    }

    public class ArmySplitModalContext
    {
        public Army SourceArmy { get; set; }
        public IEnumerable<Cell> PotentialSpawns { get; set; }
    }
}
