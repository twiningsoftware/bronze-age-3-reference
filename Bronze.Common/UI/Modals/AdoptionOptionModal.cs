﻿using System.Collections.Generic;
using System.Linq;
using Bronze.Common.Data.World.CellDevelopments;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Modals;

namespace Bronze.Common.UI.Modals
{
    public class AdoptionOptionModal : AbstractModal, IModal<AdoptionOptionModalContext>
    {
        public override bool PausesGame => true;
        protected override int ModalWidth => Elements.Max(e => e.Bounds.Width);
        protected override int ModalHeight => Elements.Max(e => e.Bounds.Height);

        private readonly IDialogManager _dialogManager;
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IPlayerDataTracker _playerData;
        private readonly IWorldManager _worldManager;
        private StackGroup _stackGroup;

        public AdoptionOptionModal(
            IDialogManager dialogManager,
            IPlayerDataTracker playerData,
            IGamedataTracker gamedataTracker,
            IWorldManager worldManager)
            : base(dialogManager)
        {
            _dialogManager = dialogManager;
            _playerData = playerData;
            _gamedataTracker = gamedataTracker;
            _worldManager = worldManager;

            _stackGroup = new StackGroup
            {
                Orientation = Orientation.Vertical,
                HorizontalContentAlignment = HorizontalAlignment.Middle
            };
        }

        public override void OnLoad()
        {
            base.OnLoad();
            Elements.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new Spacer(5, 0),
                    _stackGroup,
                    new Spacer(5, 0)
                }
            });
        }

        public void Initialize(AdoptionOptionModalContext context)
        {
            var topRow = new StackGroup
            {
                Orientation = Orientation.Horizontal,
                VerticalContentAlignment = VerticalAlignment.Middle,
                Children = new List<IUiElement>
                {
                    new Spacer(20, 0),
                    new Label(context.Title)
                }
            };

            if(context.FromSettlement != null)
            {
                var settlementLocation = context.FromSettlement
                    .EconomicActors
                    .OfType<ICellDevelopment>()
                    .Where(cd => cd is VillageDevelopment || cd is DistrictDevelopment)
                    .First();

                topRow.Children.Add(new Spacer(10, 0));
                topRow.Children.Add(new IconButton(
                    UiImages.IconView,
                    string.Empty,
                    () =>
                    {
                        _playerData.LookAt(settlementLocation.Cell.Position);
                    },
                    tooltip: "Zoom to the location."));
            }
            else
            {
                topRow.Children.Add(new Spacer(20, 0));
            }

            var bodyLeft = new StackGroup
            {
                Orientation = Orientation.Vertical
            };
            var bodyRight = new StackGroup
            {
                Orientation = Orientation.Vertical,
                Width = Width.Fixed(400)
            };
            
            foreach (var body in context.Body)
            {
                bodyRight.Children.Add(new Text(body));
                bodyRight.Children.Add(new Spacer(0, 15));
            }
            
            bodyLeft.Children.Add(UiBuilder.BuildPersonSummaryButton(_worldManager, _dialogManager, context.Adoptee));
            
            _stackGroup.Children.Clear();
            _stackGroup.Children.Add(new Spacer(0, 20));
            _stackGroup.Children.Add(topRow);
            _stackGroup.Children.Add(new Spacer(0, 10));
            _stackGroup.Children.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    bodyLeft,
                    new Spacer(20, 0),
                    bodyRight
                }
            });
            
            _stackGroup.Children.Add(new Spacer(0, 20));
            _stackGroup.Children.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new TextButton("Accept",
                        () =>
                        {
                            _playerData.PlayerTribe.NotablePeople.Add(context.Adoptee);

                            if (_playerData.PlayerTribe.Ruler.IsMale)
                            {
                                context.Adoptee.Father = _playerData.PlayerTribe.Ruler;
                                context.Adoptee.Mother = _playerData.PlayerTribe.Ruler.Spouse;
                                context.Adoptee.Father.Children.Add(context.Adoptee);
                                if (context.Adoptee.Mother != null)
                                {
                                    context.Adoptee.Mother.Children.Add(context.Adoptee);
                                }
                            }
                            context.Adoptee.Mother = _playerData.PlayerTribe.Ruler;
                            context.Adoptee.Father = _playerData.PlayerTribe.Ruler.Spouse;
                            context.Adoptee.Mother.Children.Add(context.Adoptee);
                            if (context.Adoptee.Father != null)
                            {
                                context.Adoptee.Father.Children.Add(context.Adoptee);
                            }

                            _dialogManager.PopModal();
                        }, KeyCode.A),
                    new Spacer(20, 0),
                    new TextButton("Reject (Esc)",
                        () => _dialogManager.PopModal(), 
                        KeyCode.Escape)
                }
            });
            _stackGroup.Children.Add(new Spacer(0, 20));
        }
    }

    public class AdoptionOptionModalContext
    {
        public string Title { get; set; }
        public IEnumerable<string> Body { get; set; }
        public NotablePerson Adoptee { get; set; }
        public Settlement FromSettlement { get; set; }
        public Buff FromSettlementBuff { get; set; }
    }
}
