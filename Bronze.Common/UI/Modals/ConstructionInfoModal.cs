﻿using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Modals;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.UI.Modals
{
    public class ConstructionInfoModal : AbstractModal, IModal<ConstructionInfoModalContext>
    {
        public override bool PausesGame => true;
        protected override int ModalWidth => Elements.Max(e => e.Bounds.Width);
        protected override int ModalHeight => Elements.Max(e => e.Bounds.Height);

        private StackGroup _columnRow;
        private StackGroup _optionColumn;
        private StackGroup _contentColumn;
        private readonly IDialogManager _dialogManager;
        private readonly IPlayerDataTracker _playerData;

        private ICellDevelopmentType _activeCellDevelopmentType;
        private IStructureType _activeStructureType;
        private ConstructionInfoModalContext _context;

        public ConstructionInfoModal(
            IDialogManager dialogManager,
            IPlayerDataTracker playerData)
            : base(dialogManager)
        {
            _dialogManager = dialogManager;
            _playerData = playerData;

            _columnRow = new StackGroup
            {
                Orientation = Orientation.Horizontal,
                HorizontalContentAlignment = HorizontalAlignment.Middle
            };

            _optionColumn = new StackGroup()
            {
                Orientation = Orientation.Vertical
            };

            _contentColumn = new StackGroup()
            {
                Orientation = Orientation.Vertical
            };
        }

        public override void OnLoad()
        {
            base.OnLoad();
            Elements.Add(new StackGroup
            {
                Orientation = Orientation.Vertical,
                Children = new List<IUiElement>
                {
                    new Spacer(0, 20),
                    _columnRow,
                    new Spacer(0, 10)
                }
            });
        }

        public void Initialize(ConstructionInfoModalContext context)
        {
            _context = context;

            _columnRow.Children.Clear();
            _optionColumn.Children.Clear();
            _contentColumn.Children.Clear();

            if (context.CellDevelopmentTypes.Any())
            {
                InitializeFor(context.CellDevelopmentTypes, context.Cell);
            }
            else if (context.StructureTypes.Any())
            {
                InitializeFor(context.StructureTypes);
            }

            if(_optionColumn.Children.Any())
            {
                _columnRow.Children = new List<IUiElement>
                {
                    new Spacer(10, 0),
                    _optionColumn,
                    new Spacer(10, 0),
                    _contentColumn,
                    new Spacer(10, 0),
                };
            }
            else
            {
                _columnRow.Children = new List<IUiElement>
                {
                    new Spacer(10, 0),
                    _contentColumn,
                    new Spacer(10, 0),
                };
            }
        }

        private void InitializeFor(IEnumerable<ICellDevelopmentType> cellDevelopmentTypes, Cell cell)
        {
            if(cellDevelopmentTypes.Count() > 1)
            {
                _optionColumn.Children.AddRange(cellDevelopmentTypes
                    .Select(cdt => new IconButton(
                            cdt.IconKey,
                            cdt.Name,
                            () => SetContent(cdt, cell),
                            highlightCheck: () => _activeCellDevelopmentType == cdt)));
            }

            SetContent(cellDevelopmentTypes.First(), cell);
        }

        private void SetContent(ICellDevelopmentType cellDevelopmentType, Cell cell)
        {
            _activeCellDevelopmentType = cellDevelopmentType;

            _contentColumn.Children.Clear();
            
            _contentColumn.Children.Add(cellDevelopmentType.BuildTypeInfoPanel(300, withName: true, cell: cell));
            _contentColumn.Children.Add(new Spacer(0, 10));
            _contentColumn.Children.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                    {
                        new TextButton(
                            "Build",
                            () =>
                            {
                                if(_context.Cell.Development == null)
                                {
                                    cellDevelopmentType.Place(_playerData.PlayerTribe, _context.Cell, instantBuild: false);
                                }
                                else
                                {
                                    _context.Cell.Development.UpgradeTo(cellDevelopmentType);
                                }
                                _dialogManager.PopAllModals();
                            },
                            KeyCode.B),
                        new Spacer(20, 0),
                        new TextButton(
                            "Cancel (Esc)",
                            () => _dialogManager.PopModal(),
                            KeyCode.Escape),
                    }
            });
        }

        private void InitializeFor(IEnumerable<IStructureType> structureTypes)
        {
            if (structureTypes.Count() > 1)
            {
                _optionColumn.Children.AddRange(structureTypes
                    .Select(st => new IconButton(
                            st.IconKey,
                            st.Name,
                            () => SetContent(st),
                            highlightCheck: () => _activeStructureType == st)));
            }

            SetContent(structureTypes.First());
        }

        private void SetContent(IStructureType structureType)
        {
            _activeStructureType = structureType;

            _contentColumn.Children.Clear();
            _contentColumn.Children.Add(structureType.BuildTypeInfoPanel(300, withName: true));
            _contentColumn.Children.Add(new Spacer(0, 10));
            _contentColumn.Children.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                    {
                        new TextButton(
                            "Build",
                            () =>
                            {
                                if(_context.Tile.Structure == null)
                                {
                                    structureType.Place(_context.Tile);
                                }
                                else
                                {
                                    _context.Tile.Structure.UpgradeTo(structureType);
                                }
                                _dialogManager.PopAllModals();
                            },
                            KeyCode.B),
                        new Spacer(20, 0),
                        new TextButton(
                            "Cancel (Esc)",
                            () => _dialogManager.PopModal(),
                            KeyCode.Escape),
                    }
            });
        }
    }
        
    public class ConstructionInfoModalContext
    {
        public Cell Cell { get; set; }
        public Tile Tile { get; set; }
        public IEnumerable<ICellDevelopmentType> CellDevelopmentTypes { get; set; }
        public IEnumerable<IStructureType> StructureTypes { get; set; }

        public ConstructionInfoModalContext()
        {
            CellDevelopmentTypes = Enumerable.Empty<ICellDevelopmentType>();
            StructureTypes = Enumerable.Empty<IStructureType>();
        }
    }
}
