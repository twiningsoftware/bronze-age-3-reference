﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bronze.Common.Gameplay.UiElements;
using Bronze.Common.UI.Elements;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Modals;

namespace Bronze.Common.UI.Modals
{
    public class TradeActorPickerModal : AbstractModal, IModal<TradeActorPickerModalContext>
    {
        public override bool PausesGame => true;
        protected override int ModalWidth => Elements.Max(e => e.Bounds.Width);
        protected override int ModalHeight => Elements.Max(e => e.Bounds.Height);

        private readonly IDialogManager _dialogManager;
        private readonly IWorldManager _worldManager;
        private readonly IGamedataTracker _gamedataTracker;
        private StackGroup _stackGroup;
        private TradePickerMinimapElement _minimap;

        public TradeActorPickerModal(
            IDialogManager dialogManager,
            IWorldManager worldManager,
            IGamedataTracker gamedataTracker,
            IPlayerDataTracker playerData)
            : base(dialogManager)
        {
            _dialogManager = dialogManager;
            _worldManager = worldManager;
            _gamedataTracker = gamedataTracker;

            _stackGroup = new StackGroup
            {
                Orientation = Orientation.Vertical,
                HorizontalContentAlignment = HorizontalAlignment.Middle
            };
            _minimap = new TradePickerMinimapElement(playerData, _worldManager)
            {
                Width = Width.Fixed(300),
                Height = Height.Fixed(300)
            };
        }

        public override void OnLoad()
        {
            base.OnLoad();
            Elements.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new Spacer(5, 0),
                    _stackGroup,
                    new Spacer(5, 0)
                }
            });
        }

        public void Initialize(TradeActorPickerModalContext context)
        {
            _stackGroup.Children.Clear();
            _stackGroup.Children.Add(new Spacer(0, 20));
            _stackGroup.Children.Add(new Label(context.Title));
            _stackGroup.Children.Add(new Spacer(0, 5));


            var fromOptionsBySettlement = context.Options
                .GroupBy(ta => ta.Settlement)
                .ToArray();

            var list = new List<IUiElement>();

            foreach (var option in fromOptionsBySettlement)
            {
                var itemRate = _gamedataTracker.Items
                    .Where(i => option.Key.ItemAvailability[i])
                    .ToDictionary(i => i, i => option.Key.NetItemProduction[i]);

                list.Add(new SizePadder(new IconText(string.Empty, option.Key.Name)
                {
                    Tooltip = new Border(new ItemRateDisplay(() => itemRate), 5)
                },
                minHeight: 42));

                foreach (var trader in option)
                {
                    list.Add(new StackGroup
                    {
                        Orientation = Orientation.Horizontal,
                        VerticalContentAlignment = VerticalAlignment.Middle,
                        Children = new List<IUiElement>
                        {
                            new Spacer(10, 0),
                            new TextButton(trader.Name,
                                () =>
                                {
                                    _dialogManager.PopModal();
                                    context.Callback(trader);
                                })
                            {
                                HoverAction = () => _minimap.Highlight(trader)
                            },
                            new Spacer(10, 0),
                            UiBuilder.BuildIconFor(trader.TradeType),
                            new Label(trader.TradeCapacity.ToString()),
                        }
                    });
                }
            }
            
            if (!list.Any())
            {
                list.Add(new Label("No availble traders."));
            }
            
            _stackGroup.Children.Add(new Spacer(0, 10));
            _stackGroup.Children.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new ScrollableList(10, list),
                    new Spacer(10, 300),
                    _minimap
                }
            });
            _stackGroup.Children.Add(new Spacer(0, 10));
            _stackGroup.Children.Add(new TextButton(
                "Close (Esc)",
                () =>
                {
                    _dialogManager.PopModal();
                    context.Callback(null);
                },
                KeyCode.Escape));
            _stackGroup.Children.Add(new Spacer(0, 20));
        }
    }

    public class TradeActorPickerModalContext
    {
        public string Title { get; set; }
        public IEnumerable<ITradeActor> Options { get; set; }
        public Action<ITradeActor> Callback { get; set; }
    }
}
