﻿using System.Collections.Generic;
using System.Linq;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Modals;
using Bronze.UI.Widgets;

namespace Bronze.Common.UI.Modals
{
    public class EscapeMenuModal : AbstractModal
    {
        protected override int ModalWidth => 300;
        protected override int ModalHeight => Elements.Max(e => e.Bounds.Height);
        public override bool PausesGame => true;

        private readonly IGameInterface _gameInterface;
        private readonly IUserSettings _userSettings;
        private readonly IWorldManager _worldManager;
        private readonly IPlayerDataTracker _playerData;
        private readonly IDialogManager _dialogManager;
        private readonly ISavefileStreamSource _savefileStreamSource;

        public EscapeMenuModal(
            IGameInterface gameInterface,
            IDialogManager dialogManager, 
            IUserSettings userSettings,
            IWorldManager worldManager,
            IPlayerDataTracker playerData,
            ISavefileStreamSource savefileStreamSource)
            : base(dialogManager)
        {
            _gameInterface = gameInterface;
            _userSettings = userSettings;
            _worldManager = worldManager;
            _playerData = playerData;
            _dialogManager = dialogManager;
            _savefileStreamSource = savefileStreamSource;
        }
        
        public override void OnLoad()
        {
            base.OnLoad();

            Elements.Add(new StackGroup
            {
                HorizontalContentAlignment = HorizontalAlignment.Middle,
                VerticalContentAlignment = VerticalAlignment.Middle,
                Width = Width.Relative(1f),
                Orientation = Orientation.Vertical,
                Children = new List<IUiElement>
                {
                    new Spacer(0, 20),
                    new Text("MENU"),
                    new Spacer(10, 10),
                    new TextButton("Save and Quit",
                        () =>
                        {
                            _gameInterface.SaveAndMainMenu();
                        },
                        KeyCode.Q),
                    new Spacer(10, 10),
                    new TextButton("Abandon World",
                        () =>
                        {
                            _dialogManager.PushModal<ConfirmModal, ConfirmModalContext>(
                                new ConfirmModalContext
                                {
                                    Title = "Are You Sure?",
                                    Body = "The world will be permenantly deleted.",
                                    YesAction = () =>
                                    {
                                        _savefileStreamSource.DeleteFile(_worldManager.WorldName);
                                        _gameInterface.MainMenu();
                                    },
                                    NoAction = () => {}
                                });
                        },
                        KeyCode.A),
                    new Spacer(10, 10),
                    new Text("Music Volume"),
                    new VolumeWidget(
                        () => _userSettings.MusicVolume,
                        val => _userSettings.MusicVolume = val),
                    new Spacer(10, 10),
                    new Text("Effects Volume"),
                    new VolumeWidget(
                        () => _userSettings.EffectsVolume,
                        val => _userSettings.EffectsVolume = val),
                    new Spacer(10, 10),
                    new Text("Autosave Interval"),
                    new SetNumberWidget(
                        () => (int)_userSettings.AutosaveInterval,
                        val => _userSettings.AutosaveInterval = Util.Clamp(val, Constants.AUTOSAVE_MIN, Constants.AUTOSAVE_DISABLE),
                        () =>
                        {
                            if(_userSettings.AutosaveInterval > Constants.AUTOSAVE_MAX)
                            {
                                return "disabled";
                            }

                            return (int)(_userSettings.AutosaveInterval / 60) + " minutes";
                        },
                        120,
                        80),
                    new Spacer(10, 10),
                    new ToggleButton("Display: Full Screen", "Display: Windowed",
                        val => _userSettings.IsFullScreen = val,
                        () => _userSettings.IsFullScreen,
                        KeyCode.D),
                    new Spacer(10, 10),
                    new Text("Enemy Alerts"),
                    new StackGroup
                    {
                        Orientation = Orientation.Horizontal,
                        Children = new List<IUiElement>
                        {
                            new TextButton(
                                "<",
                                () => _userSettings.AlertLevel = (AlertLevel)((int)_userSettings.AlertLevel - 1),
                                disableCheck: () => (int)_userSettings.AlertLevel == 0),
                            new SizePadder(new Text(string.Empty)
                                {
                                    Width = Width.Fixed(150),
                                    ContentUpdater = () => Util.FriendlyDisplay(_userSettings.AlertLevel)
                                },
                                150,
                                32)
                            {
                                HorizontalAlignment = HorizontalAlignment.Middle,
                                VerticalAlignment = VerticalAlignment.Middle,
                            },
                            new TextButton(
                                ">",
                                () => _userSettings.AlertLevel = (AlertLevel)((int)_userSettings.AlertLevel + 1),
                                disableCheck: () => (int)_userSettings.AlertLevel == 2),
                        }
                    },
                    new Spacer(10, 10),
                    new ToggleButton("Tutorial: Enabled", "Tutorial: Disabled",
                        val => _userSettings.TutorialEnabled = val,
                        () => _userSettings.TutorialEnabled),
                    new Spacer(10, 10),
                    new ToggleButton("Advice: Active", "Advice: Passive",
                        val => _userSettings.ActiveAdvice = val,
                        () => _userSettings.ActiveAdvice),
                    new Spacer(10, 10),
                    new ToggleButton("Error Reporting: On", "Error Reporting: Off",
                        val => _userSettings.AnalyticsEnabled = val ? 2 : 1,
                        () => _userSettings.AnalyticsEnabled == 2,
                        tooltip: "Anonymously report errors and user data? (It's really helpful.)"),
                    new Spacer(10, 10),
                    new ToggleButton("Optimized Tile Drawing: On", "Optimized Tile Drawing: Off",
                        val => _userSettings.OptimizeTileDrawing = val,
                        () => _userSettings.OptimizeTileDrawing,
                        tooltip: "Optimized tile drawing may cause graphical issues with AMD video cards."),
                    new Spacer(10, 10),
                    new TextButton("Resume (esc)",
                        () =>
                        {
                            _dialogManager.PopModal();
                        },
                        KeyCode.Escape),
                    new Spacer(0, 20)
                }
            });
        }
    }
}
