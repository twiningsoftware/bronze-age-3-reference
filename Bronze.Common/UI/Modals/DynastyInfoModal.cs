﻿using System.Collections.Generic;
using System.Linq;
using Bronze.Common.UI.Elements;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Modals;

namespace Bronze.Common.UI.Modals
{
    public class DynastyInfoModal : AbstractModal, IModal<Tribe>
    {
        public override bool PausesGame => true;
        protected override int ModalWidth => Elements.Max(e => e.Bounds.Width);
        protected override int ModalHeight => Elements.Max(e => e.Bounds.Height);

        private readonly IDialogManager _dialogManager;
        private readonly IWorldManager _worldManager;
        private StackGroup _stackGroup;
        
        public DynastyInfoModal(
            IDialogManager dialogManager,
            IWorldManager worldManager)
            : base(dialogManager)
        {
            _dialogManager = dialogManager;
            _worldManager = worldManager;

            _stackGroup = new StackGroup
            {
                Orientation = Orientation.Vertical,
                HorizontalContentAlignment = HorizontalAlignment.Middle
            };
        }

        public override void OnLoad()
        {
            base.OnLoad();
            Elements.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new Spacer(5, 0),
                    _stackGroup,
                    new Spacer(5, 0)
                }
            });
        }

        public void Initialize(Tribe tribe)
        {
            _stackGroup.Children.Clear();
            _stackGroup.Children.Add(new Spacer(0, 20));
            _stackGroup.Children.Add(new Label(tribe.Name + " Dynasty"));
            _stackGroup.Children.Add(new Spacer(0, 10));

            var scrollList = new List<IUiElement>();

            _stackGroup.Children.Add(new Border(
                new StackGroup
                {
                    Orientation = Orientation.Horizontal,
                    Children = new List<IUiElement>
                    {
                        new NotablePersonPortrait(() => tribe.Ruler),
                        new Spacer(10, 0),
                        new StackGroup
                        {
                            Orientation = Orientation.Vertical,
                            Children = new List<IUiElement>
                            {
                                new Label(tribe.Ruler.Name),
                                new Label($"Age: {(int)((_worldManager.Month - tribe.Ruler.BirthDate) / 12)}"),
                                new TextButton(
                                    "Details",
                                    () =>
                                    {
                                        _dialogManager.PushModal<NotablePersonDetailsModal, NotablePerson>(tribe.Ruler);
                                    })
                            }
                        },
                        new Spacer(10, 0),
                        new StackGroup
                        {
                            Orientation = Orientation.Vertical,
                            Children = new List<IUiElement>
                            {
                                UiBuilder.BuildStatDisplay(tribe.Ruler, NotablePersonStatType.Valor),
                                UiBuilder.BuildStatDisplay(tribe.Ruler, NotablePersonStatType.Wit),
                                UiBuilder.BuildStatDisplay(tribe.Ruler, NotablePersonStatType.Charisma)
                            }
                        }
                    }
                }));

            var livingPeople = tribe.NotablePeople
                .Where(p => !p.IsDead && p != tribe.Ruler)
                .OrderBy(p => p.BirthDate)
                .ToArray();
            var rowSize = 5;
            for (var i = 0; i < livingPeople.Length; i += rowSize)
            {
                scrollList.Add(new StackGroup
                {
                    Orientation = Orientation.Horizontal,
                    Children = livingPeople.Skip(i)
                        .Take(rowSize)
                        .Select(c => UiBuilder.BuildPersonSummaryButton(_worldManager, _dialogManager, c))
                        .ToList<IUiElement>()
                });
            }
            
            _stackGroup.Children.Add(new Spacer(0, 5));
            _stackGroup.Children.Add(new ScrollableList(4, scrollList));
            _stackGroup.Children.Add(new Spacer(0, 20));
            _stackGroup.Children.Add(new TextButton("Close (Esc)", () => _dialogManager.PopModal(), KeyCode.Escape));
            _stackGroup.Children.Add(new Spacer(0, 20));
        }
    }
}
