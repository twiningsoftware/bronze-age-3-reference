﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bronze.Common.Data.Game.Units;
using Bronze.Common.Data.World.CellDevelopments;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Modals;

namespace Bronze.Common.UI.Modals
{
    public class RecruitmentModal : AbstractModal, IModal<RecruitmentModalContext>
    {
        public override bool PausesGame => true;
        protected override int ModalWidth => Elements.Max(e => e.Bounds.Width);
        protected override int ModalHeight => Elements.Max(e => e.Bounds.Height);

        private StackGroup _stackGroup;
        private readonly IDialogManager _dialogManager;
        private readonly IPlayerDataTracker _playerData;
        private readonly IUnitHelper _unitHelper;
        private readonly IGamedataTracker _gamedataTracker;
        private IUnitType _activeUnit;
        private UnitEquipmentSet _activeEquipment;

        public RecruitmentModal(
            IDialogManager dialogManager,
            IPlayerDataTracker playerData,
            IUnitHelper unitHelper,
            IGamedataTracker gamedataTracker)
            : base(dialogManager)
        {
            _dialogManager = dialogManager;
            _playerData = playerData;
            _unitHelper = unitHelper;
            _gamedataTracker = gamedataTracker;

            _stackGroup = new StackGroup
            {
                Orientation = Orientation.Vertical,
                HorizontalContentAlignment = HorizontalAlignment.Middle
            };
        }

        public override void OnLoad()
        {
            base.OnLoad();
            Elements.Add(_stackGroup);
        }

        public void Initialize(RecruitmentModalContext context)
        {
            _stackGroup.Children.Clear();
            _stackGroup.Children.Add(new Spacer(0, 20));
            if(context.Settlement != null)
            {
                _stackGroup.Children.Add(new Label($"Recruiting From {context.Settlement.Name}"));
            }
            else
            {
                _stackGroup.Children.Add(new Label($"Unit Picker"));
            }
            _stackGroup.Children.Add(new Spacer(0, 10));

            var buttonItems = new List<IUiElement>();
            
            var detailsStack = new StackGroup
            {
                Orientation = Orientation.Vertical,
            };
            detailsStack.Children.Add(new Spacer(320, 0));
            
            var defaultSet = false;

            IUnitType[] availableUnits;

            if (context.Settlement != null)
            {
                availableUnits = context.Settlement.Race.AvailableUnits
                    .Concat(context.Settlement.EconomicActors.OfType<IRecruitableUnitTypeSource>().SelectMany(s => s.ProvidedUnitTypes))
                    .Where(u => u.CanBeTrained)
                    .ToArray();
            }
            else
            {
                availableUnits = _gamedataTracker.UnitTypes.ToArray();
            }

            foreach(var category in _gamedataTracker.RecruitmentSlotCategories)
            {
                if(context.Settlement == null || context.Settlement.RecruitmentSlotsMax[category] > 0)
                { 
                    var unitsForCategory = availableUnits
                        .Where(u => u.RecruitmentCategory == category)
                        .ToArray();

                    if (unitsForCategory.Any())
                    {
                        if (context.Settlement != null)
                        {
                            buttonItems.Add(new SizePadder(
                                new IconText(string.Empty, category.Name)
                                {
                                    ContentUpdater = () => $"{category.Name} {(int)context.Settlement.RecruitmentSlots[category]} / {context.Settlement.RecruitmentSlotsMax[category]}",
                                    Tooltip = new SlicedBackground(new IconText(string.Empty, "At limit.")
                                    {
                                        ContentUpdater = () =>
                                        {
                                            if (context.Settlement.RecruitmentSlots[category] < context.Settlement.RecruitmentSlotsMax[category])
                                            {
                                                var raw = context.Settlement.RecruitmentSlots[category];
                                                var whole = (int)raw;
                                                var remainer = raw - whole;

                                                return $"More in {Util.FriendlyTimeDisplay((1 - remainer) * category.RegenTimeMonths)}.";
                                            }

                                            return $"At limit.";
                                        }
                                    })
                                },
                                minHeight: 42));
                        }
                        else
                        {
                            buttonItems.Add(new SizePadder(
                                new IconText(string.Empty, category.Name),
                                minHeight: 42));
                        }
                        
                        foreach (var unitType in unitsForCategory)
                        {
                            var availableEquipment = unitType.EquipmentSets
                                .Where(es => es.RecruitmentCost.All(iq => context.Settlement == null || _playerData.PlayerTribe.KnownItems[iq.Item] == true))
                                .ToArray();

                            if(context.DestinationArmy != null)
                            {
                                availableEquipment = availableEquipment
                                    .Where(es => es.CanPath(context.DestinationArmy.Cell))
                                    .ToArray();
                            }

                            if (availableEquipment.Any())
                            {
                                if (!defaultSet && (context.Settlement == null || context.Settlement.RecruitmentSlots[unitType.RecruitmentCategory] >= 1))
                                {
                                    defaultSet = true;

                                    SetActiveUnit(
                                        context,
                                        detailsStack,
                                        context.Settlement,
                                        unitType,
                                        availableEquipment.First());
                                }

                                var buttons = availableEquipment
                                        .Select(es => new IconButton(
                                            es.IconKey,
                                            string.Empty,
                                            () => SetActiveUnit(context, detailsStack, context.Settlement, unitType, es),
                                            highlightCheck: () => _activeUnit == unitType && _activeEquipment == es,
                                            tooltip: es.Name.Length > 0 ? $"{unitType.Name} with {es.Name}" : unitType.Name)
                                        {
                                            Colorization = (context.DestinationArmy?.Owner ?? _playerData.PlayerTribe).PrimaryColor
                                        })
                                        .ToArray();

                                for (var i = 0; i < buttons.Length; i += 4)
                                {
                                    buttonItems.Add(new StackGroup
                                    {
                                        Orientation = Orientation.Horizontal,
                                        Children = buttons.Skip(i).Take(4).ToList<IUiElement>()
                                    });
                                }
                            }
                        }
                    }
                }
            }

            _stackGroup.Children.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new Spacer(10, 500),
                    new StackGroup
                    {
                        Orientation = Orientation.Vertical,
                        Children = new List<IUiElement>
                        {
                            new ScrollableList(15, buttonItems)
                            {
                                Width = Width.Fixed(180)
                            },
                            new Spacer(0, 10)
                        }
                    },
                    new Spacer(10, 500),
                    new StackGroup
                    {
                        Orientation = Orientation.Vertical,
                        Children = new List<IUiElement>
                        {
                            new SizePadder(detailsStack, minHeight: 500)
                            {
                                VerticalAlignment = VerticalAlignment.Top
                            },
                            new Spacer(0, 20),
                            new TextButton("Close (Esc)", () => _dialogManager.PopModal(), KeyCode.Escape),
                            new Spacer(0, 20),
                        }
                    },
                    new Spacer(10, 500),
                }
            });
        }

        private void SetActiveUnit(
            RecruitmentModalContext context,
            StackGroup detailsStack, 
            Settlement sourceSettlement,
            IUnitType unitType,
            UnitEquipmentSet equipmentSet)
        {
            _activeUnit = unitType;
            _activeEquipment = equipmentSet;
            detailsStack.Children.Clear();

            if (unitType == null)
            {
                return;
            }

            IArmoryStorageProvider[] stockpiles = new IArmoryStorageProvider[0];

            if (sourceSettlement != null)
            {
                stockpiles = sourceSettlement
                    .EconomicActors
                    .OfType<IArmoryStorageProvider>()
                    .ToArray();
            }

            var mainColumn = new StackGroup
            {
                Orientation = Orientation.Vertical,
                Width = Width.Fixed(300),
                Children = new List<IUiElement>
                {
                    new Text(unitType.Description),
                    new Spacer(0, 10),
                }
            };

            if(equipmentSet.Name.Length > 0)
            {
                mainColumn.Children.Add(new IconText(equipmentSet.IconKey, equipmentSet.Name)
                {
                    IconColorization = (context.DestinationArmy?.Owner ?? _playerData.PlayerTribe).PrimaryColor
                });
                mainColumn.Children.Add(new Spacer(0, 5));
                mainColumn.Children.Add(new Text(equipmentSet.Description));
                mainColumn.Children.Add(new Spacer(0, 10));
            }

            if (equipmentSet.RecruitmentCost.Any())
            {
                mainColumn.Children.Add(new Spacer(0, 10));
                mainColumn.Children.Add(new Label("Recruitment Cost"));
                mainColumn.Children.Add(UiBuilder.BuildDisplayFor(equipmentSet.RecruitmentCost));
            }

            if (equipmentSet.UpkeepNeeds.Any())
            {
                mainColumn.Children.Add(new Spacer(0, 10));
                mainColumn.Children.Add(new Label("Upkeep"));
                mainColumn.Children.Add(UiBuilder.BuildDisplayFor(equipmentSet.UpkeepNeeds.Select(u => new ItemRate(u.Item, -u.PerMonth)), 4));
            }

            if(_activeUnit is GrazingUnitType grazingUnitType)
            {
                mainColumn.Children.Add(new Spacer(0, 10));
                mainColumn.Children.Add(new Label("Production"));
                mainColumn.Children.Add(UiBuilder.BuildDisplayFor(grazingUnitType.GrazingProduction, 4));
            }

            var totalStocks = _gamedataTracker.Items
                .Select(i => new ItemQuantity(i, Math.Floor(stockpiles.Sum(s => s.Inventory.QuantityOf(i)))))
                .Where(iq => iq.Quantity > 0)
                .ToArray();

            var sideColumn = new StackGroup
            {
                Orientation = Orientation.Vertical
            };

            if(totalStocks.Any())
            {
                sideColumn.Children.AddRange(new IUiElement[]
                {
                    new Label("Armory Stocks"),
                    UiBuilder.BuildDisplayFor(totalStocks, 4),
                    new Spacer(0, 10),
                });
            }

            sideColumn.Children.AddRange(new IUiElement[]
            {
                new Label("Unit Stats"),
                    new StackGroup
                    {
                        Orientation = Orientation.Horizontal,
                        Children = new List<IUiElement>
                        {
                            new IconText("ui_stat_health", $"{equipmentSet.HealthPerIndividual * equipmentSet.IndividualCount}", tooltip: "Health"),
                            new Spacer(10, 0),
                            new IconText("ui_stat_supplies", $"{equipmentSet.SupplyCapacity}", tooltip: "Supplies"),
                            new Spacer(10, 0),
                            new IconText("ui_stat_vision", equipmentSet.VisionRange.ToString(), tooltip: "Unit vision range."),
                        }
                    },
                    new StackGroup
                    {
                        Orientation = Orientation.Horizontal,
                        Children = new List<IUiElement>
                        {
                            new IconText("ui_stat_armor", equipmentSet.Armor.ToString(), tooltip: "Armor: reduces damage taken."),
                            new Spacer(10, 0),
                            new IconText("ui_stat_defense", equipmentSet.DefenseSkill.ToString(), tooltip: "Defense: Ability to defend against melee attacks."),
                            new Spacer(10, 0),
                            new IconText("ui_stat_block", equipmentSet.BlockSkill.ToString(), tooltip: "Block: Ability to block melee and ranged attacks."),
                        }
                    },
                    new StackGroup
                    {
                        Orientation = Orientation.Horizontal,
                        Children = new List<IUiElement>
                        {
                            new IconText("ui_stat_skirmish", equipmentSet.SkirmishSkill.ToString(), tooltip: "Unit skirmish skill."),
                            new Spacer(10, 0),
                            new IconText("ui_stat_resolve", equipmentSet.ResolveSkill.ToString(), tooltip: "Resolve: Ability to resist morale shocks."),
                        }
                    },
                    
            });

            sideColumn.Children.AddRange(equipmentSet.MeleeAttacks
                    .Select(m => UiBuilder.BuildDisplayForMeleeAttack(m)));
            sideColumn.Children.AddRange(equipmentSet.RangedAttacks
                    .Select(m => UiBuilder.BuildDisplayForRangedAttack(m)));
            sideColumn.Children.AddRange(equipmentSet.MovementModes
                    .Select(mm => new IconText(mm.MovementType.IconKey, mm.Speed.ToString(), tooltip: mm.MovementType.Name + " Speed")));
            sideColumn.Children.AddRange(equipmentSet.AvailableActions
                    .Where(a => !(a is ArmySkirmishAction) && !(a is ArmyApproachAction) && !(a is ArmyInteractAction) && !(a is ArmyMoveAction) && !(a is ArmyMergeAction))
                    .Select(a => new IconText(a.Icon, a.Name, tooltip: a.Description)));

            detailsStack.Children.Add(new Label(unitType.Name));
            detailsStack.Children.Add(new Spacer(0, 10));
            
            detailsStack.Children.Add(new Spacer(0, 10));
            detailsStack.Children.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    mainColumn,
                    new Spacer(30, 0),
                    sideColumn
                }
            });

            Cell armyPosition = null;

            if(context.DestinationArmy != null)
            {
                armyPosition = context.DestinationArmy.Cell;
            }
            else
            {
                armyPosition = context.Settlement.EconomicActors
                    .OfType<ICellDevelopment>()
                    .Where(cd => cd is VillageDevelopment || cd is DistrictDevelopment || cd is IRecruitmentSlotProvider)
                    .OrderBy(cd => (cd is VillageDevelopment || cd is DistrictDevelopment) ? 0 : 1)
                    .SelectMany(cd => cd.Cell.Yield().Concat(cd.Cell.Neighbors))
                    .Where(c => equipmentSet.CanPath(c))
                    .Where(c => c.Owner == null || c.Owner.AllowsAccess(context.Settlement.Owner))
                    .FirstOrDefault();
            }
            
            var canTrain = true;
            var messages = new List<IUiElement>();
            
            if (sourceSettlement != null && sourceSettlement.RecruitmentSlots[unitType.RecruitmentCategory] < 1)
            {
                messages.Add(new Label($"Not enough {unitType.RecruitmentCategory.Name} recruits."));
                canTrain = false;
            }

            if(context.DestinationArmy != null && context.DestinationArmy.IsFull)
            {
                messages.Add(new Label($"Army is full."));
                canTrain = false;
            }

            if(armyPosition == null)
            {
                messages.Add(new Label($"No room to create army."));
                canTrain = false;
            }

            if (equipmentSet.RecruitmentCost.Any() && context.Settlement != null)
            {
                var hasCosts = equipmentSet.RecruitmentCost
                    .All(iq => stockpiles.Sum(s => s.Inventory.QuantityOf(iq.Item)) >= iq.Quantity);

                if(!hasCosts)
                {
                    messages.Add(new Label($"Insufficient armory stockpiles of:"));
                    foreach(var cost in equipmentSet.RecruitmentCost)
                    {
                        var shortfall = cost.Quantity - stockpiles.Sum(s => s.Inventory.QuantityOf(cost.Item));

                        if(shortfall > 0)
                        {
                            messages.Add(new IconText(cost.Item.IconKey, cost.Item.Name));
                        }
                    }

                    canTrain = false;
                }
            }
            
            if(messages.Any())
            {
                detailsStack.Children.Add(new Spacer(0, 10));
                detailsStack.Children.AddRange(messages);
            }

            detailsStack.Children.Add(new Spacer(0, 10));

            
            detailsStack.Children.Add(new TextButton(
                "Recruit",
                () =>
                {
                    if(context.DestinationArmy != null)
                    {
                        context.DestinationArmy.Units.Add(
                            _unitHelper.RecruitUnit(unitType, equipmentSet, sourceSettlement));
                    }
                    else
                    {
                        var army = _unitHelper.CreateArmy(context.Settlement.Owner, armyPosition);

                        army.Units.Add(
                            _unitHelper.RecruitUnit(unitType, equipmentSet, sourceSettlement));

                        if (_playerData.PlayerTribe == army.Owner)
                        {
                            if (_playerData.SelectedArmy == null)
                            {
                                _playerData.SelectedArmy = army;
                            }

                            if (context.DestinationArmy == null)
                            {
                                context.DestinationArmy = army;
                            }
                        }
                    }
                    
                    SetActiveUnit(
                        context,
                        detailsStack,
                        sourceSettlement,
                        unitType,
                        equipmentSet);

                    context.PickedCallback?.Invoke();
                },
                KeyCode.T,
                disableCheck: () => !canTrain));
        }
    }

    public class RecruitmentModalContext
    {
        public Settlement Settlement { get; set; }
        public Army DestinationArmy { get; set; }
        public Action PickedCallback { get; set; }
    }
}
