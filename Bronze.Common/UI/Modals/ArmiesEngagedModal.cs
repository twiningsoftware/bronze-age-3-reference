﻿using System.Collections.Generic;
using System.Linq;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Modals;

namespace Bronze.Common.UI.Modals
{
    public class ArmiesEngagedModal : AbstractModal
    {
        public override bool PausesGame => true;
        protected override int ModalWidth => Elements.Max(e => e.Bounds.Width);
        protected override int ModalHeight => Elements.Max(e => e.Bounds.Height);

        private readonly IDialogManager _dialogManager;
        private readonly IWorldManager _worldManager;
        private readonly IPlayerDataTracker _playerData;
        private readonly IDiplomacyHelper _diplomacyHelper;
        private readonly IBattleHelper _battleHelper;
        
        private StackGroup _content;
        
        public ArmiesEngagedModal(
            IDialogManager dialogManager,
            IWorldManager worldManager,
            IPlayerDataTracker playerData,
            IDiplomacyHelper diplomacyHelper,
            IBattleHelper battleHelper)
            : base(dialogManager)
        {
            _dialogManager = dialogManager;
            _worldManager = worldManager;
            _playerData = playerData;
            _diplomacyHelper = diplomacyHelper;
            _battleHelper = battleHelper;

            _content = new StackGroup
            {
                Orientation = Orientation.Vertical
            };
        }
        
        public override void OnLoad()
        {
            base.OnLoad();

            Elements.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new Spacer(10, 0),
                    _content,
                    new Spacer(10, 0)
                }
            });

            _content.Children.Clear();
            _content.HorizontalContentAlignment = HorizontalAlignment.Middle;

            _content.Children.AddRange(new List<IUiElement>
            {
                new Spacer(0, 10),
                new Label("Current Battles"),
                new Spacer(0, 5),
                new SizePadder(
                    new TableGroup<Army>(
                        new [] {"Army", "Strength", "Location", ""},
                        _playerData.PlayerTribe.Armies.Where(a => a.SkirmishingWith.Any()),
                        a => BuildRow(a),
                        15),
                    minHeight: 300)
                {
                    VerticalAlignment = VerticalAlignment.Top
                },
                new Spacer(0, 5),
                new TextButton("Close (Esc)", () => _dialogManager.PopModal(), KeyCode.Escape),
                new Spacer(0, 10),
            });
        }
        
        private IEnumerable<IUiElement> BuildRow(Army sourceArmy)
        {
            var nearbyArmies = sourceArmy.Cell.Yield()
                .Concat(sourceArmy.Cell.Neighbors)
                .SelectMany(c => c.WorldActors.OfType<Army>())
                .Where(a => (sourceArmy.Position - a.Position).Length() <= Constants.DIST_SKIRMISH_MAX)
                .ToArray();

            var enemyArmies = nearbyArmies
                .Where(a => sourceArmy.Owner.IsHostileTo(a.Owner))
                .ToArray();

            var allyArmies = nearbyArmies
                .Where(a => !sourceArmy.Owner.IsHostileTo(a.Owner) && enemyArmies.Any(ea => a.Owner.IsHostileTo(ea.Owner)))
                .ToArray();
            
            var ownStrength = allyArmies.Sum(a => a.StrengthEstimate);
            var otherStrength = enemyArmies.Sum(a => a.StrengthEstimate);
            
            return new IUiElement[]
            {
                new Label(sourceArmy.Name),
                new Label($"{(int)ownStrength} v {(int)otherStrength}"),
                new Label(sourceArmy.Cell.Region.Name),
                new IconButton(
                    UiImages.IconView,
                    string.Empty,
                    () =>
                    {
                        _playerData.LookAt(sourceArmy.Cell.Position);
                    },
                    tooltip: "Zoom to the location.")
            };
        }
    }
}
