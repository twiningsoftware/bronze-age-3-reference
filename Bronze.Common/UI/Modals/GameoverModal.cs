﻿using System.Collections.Generic;
using System.Linq;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Modals;

namespace Bronze.Common.UI.Modals
{
    public class GameoverModal : AbstractModal
    {
        protected override int ModalWidth => Elements.Max(e => e.Bounds.Width);
        protected override int ModalHeight => Elements.Max(e => e.Bounds.Height);
        public override bool PausesGame => true;

        private readonly IDialogManager _dialogManager;
        private readonly IGameInterface _gameInterface;
        
        public GameoverModal(
            IDialogManager dialogManager,
            IGameInterface gameInterface)
            : base(dialogManager)
        {
            _dialogManager = dialogManager;
            _gameInterface = gameInterface;
        }
        
        public override void OnLoad()
        {
            base.OnLoad();

            Elements.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                HorizontalContentAlignment = HorizontalAlignment.Middle,
                VerticalContentAlignment = VerticalAlignment.Middle,
                Children = new List<IUiElement>
                {
                    new Spacer(20, 0),
                    new StackGroup
                    {
                        HorizontalContentAlignment = HorizontalAlignment.Middle,
                        VerticalContentAlignment = VerticalAlignment.Middle,
                        Orientation = Orientation.Vertical,
                        Children = new List<IUiElement>
                        {
                            new Spacer(0, 20),
                            new Label("GAME OVER"),
                            new Spacer(0, 10),
                            new Text("Your dynasty has failed. Your cities are without leaders, and your people are without hope.")
                            {
                                Width = Width.Fixed(250)
                            },
                            new Spacer(0, 5),
                            new Text("Better luck next time.")
                            {
                                Width = Width.Fixed(250)
                            },
                            new Spacer(0, 20),
                            new TextButton("Main Menu",
                            () => 
                            {
                                _gameInterface.MainMenu();
                            }),
                            new Spacer(0, 20),
                        }
                    },
                    new Spacer(20, 0),
                }
            });
        }
    }
}
