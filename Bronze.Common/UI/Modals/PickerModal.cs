﻿using System.Collections.Generic;
using System.Linq;
using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Modals;

namespace Bronze.Common.UI.Modals
{
    public class PickerModal : AbstractModal, IModal<PickerModalContext>
    {
        public override bool PausesGame => true;
        protected override int ModalWidth => Elements.Max(e => e.Bounds.Width);
        protected override int ModalHeight => Elements.Max(e => e.Bounds.Height);

        private readonly IDialogManager _dialogManager;
        private readonly IWorldManager _worldManager;
        private StackGroup _stackGroup;
        
        public PickerModal(
            IDialogManager dialogManager,
            IWorldManager worldManager)
            : base(dialogManager)
        {
            _dialogManager = dialogManager;
            _worldManager = worldManager;

            _stackGroup = new StackGroup
            {
                Orientation = Orientation.Vertical,
                HorizontalContentAlignment = HorizontalAlignment.Middle
            };
        }

        public override void OnLoad()
        {
            base.OnLoad();
            Elements.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new Spacer(5, 0),
                    _stackGroup,
                    new Spacer(5, 0)
                }
            });
        }

        public void Initialize(PickerModalContext context)
        {
            _stackGroup.Children.Clear();
            _stackGroup.Children.Add(new Spacer(0, 20));
            _stackGroup.Children.Add(new Label(context.Title));
            _stackGroup.Children.Add(new Spacer(0, 5));

            var scrollList = context.Options
                .Select(o => new SizePadder(
                    new TextButton(
                        context.TextForOption(o),
                        () =>
                        {
                            _dialogManager.PopModal();
                            context.Callback(o);
                        }),
                    minWidth: 200))
                .ToList<IUiElement>();
            
            _stackGroup.Children.Add(new Spacer(0, 10));
            _stackGroup.Children.Add(new ScrollableList(10, scrollList));
            _stackGroup.Children.Add(new Spacer(0, 10));
            _stackGroup.Children.Add(new TextButton(
                "Close (Esc)",
                () =>
                {
                    _dialogManager.PopModal();
                    context.CancelCallback?.Invoke();
                },
                KeyCode.Escape));
            _stackGroup.Children.Add(new Spacer(0, 20));
        }
    }
}
