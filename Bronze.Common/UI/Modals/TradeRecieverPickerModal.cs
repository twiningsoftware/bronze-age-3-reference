﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bronze.Common.Gameplay.UiElements;
using Bronze.Common.UI.Elements;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Modals;

namespace Bronze.Common.UI.Modals
{
    public class TradeRecieverPickerModal : AbstractModal, IModal<TradeRecieverPickerModalContext>
    {
        public override bool PausesGame => true;
        protected override int ModalWidth => Elements.Max(e => e.Bounds.Width);
        protected override int ModalHeight => Elements.Max(e => e.Bounds.Height);

        private readonly IDialogManager _dialogManager;
        private readonly IWorldManager _worldManager;
        private readonly IGamedataTracker _gamedataTracker;
        private StackGroup _stackGroup;
        private TradePickerMinimapElement _minimap;

        public TradeRecieverPickerModal(
            IDialogManager dialogManager,
            IWorldManager worldManager,
            IGamedataTracker gamedataTracker,
            IPlayerDataTracker playerData)
            : base(dialogManager)
        {
            _dialogManager = dialogManager;
            _worldManager = worldManager;
            _gamedataTracker = gamedataTracker;

            _stackGroup = new StackGroup
            {
                Orientation = Orientation.Vertical,
                HorizontalContentAlignment = HorizontalAlignment.Middle
            };
            
            _minimap = new TradePickerMinimapElement(playerData, _worldManager)
            {
                Width = Width.Fixed(300),
                Height = Height.Fixed(300)
            };
        }

        public override void OnLoad()
        {
            base.OnLoad();
            Elements.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new Spacer(5, 0),
                    _stackGroup,
                    new Spacer(5, 0)
                }
            });
        }

        public void Initialize(TradeRecieverPickerModalContext context)
        {
            _stackGroup.Children.Clear();
            _stackGroup.Children.Add(new Spacer(0, 20));
            _stackGroup.Children.Add(new Label(context.Title));
            _stackGroup.Children.Add(new Spacer(0, 5));


            var list = new List<IUiElement>();

            var fromOptionsBySettlement = context.Options
                .GroupBy(ta => ta.Settlement)
                .ToArray();

            foreach (var option in fromOptionsBySettlement)
            {
                var itemRate = _gamedataTracker.Items
                    .Where(i => option.Key.ItemAvailability[i])
                    .ToDictionary(i => i, i => option.Key.NetItemProduction[i]);

                list.Add(new IconText(string.Empty, option.Key.Name)
                {
                    Tooltip = new Border(new ItemRateDisplay(() => itemRate), 5)
                });

                foreach (var reciever in option)
                {
                    var movementTypes = new MovementType[0];

                    if (reciever is IStructure recieverStructure)
                    {
                        var tiles = recieverStructure.Footprint
                            .SelectMany(t => t.Neighbors)
                            .Except(recieverStructure.Footprint)
                            .ToArray();

                        movementTypes = _gamedataTracker.MovementTypes
                            .Where(m => tiles.Any(t => t.Traits.ContainsAll(m.RequiredTraits) && !t.Traits.ContainsAny(m.PreventedBy)))
                            .ToArray();
                    }
                    else if (reciever is ICellDevelopment recieverDevelopment)
                    {
                        var cells = recieverDevelopment.Cell
                            .Neighbors
                            .ToArray();

                        movementTypes = _gamedataTracker.MovementTypes
                            .Where(m => cells.Any(t => t.Traits.ContainsAll(m.RequiredTraits) && !t.Traits.ContainsAny(m.PreventedBy)))
                            .ToArray();
                    }
                    
                    list.Add(new StackGroup
                    {
                        Orientation = Orientation.Horizontal,
                        VerticalContentAlignment = VerticalAlignment.Middle,
                        Children = new List<IUiElement>
                        {
                            new Spacer(10,0),
                            new TextButton(
                                reciever.Name,
                                () =>
                                {
                                    _dialogManager.PopModal();
                                    context.Callback(reciever);
                                },
                                disableCheck: () => !movementTypes.Contains(context.Sender.TradeType))
                            {
                                HoverAction = () => _minimap.Highlight(reciever)
                            },
                            new Spacer(5,0),
                        }
                        .Concat(movementTypes
                            .Select(m => new IconText(m.IconKey, string.Empty, tooltip: "Accessible to " + m.Name)))
                        .ToList()
                    });
                }
            }

            if (!list.Any())
            {
                list.Add(new Label("No settlements with trade depots."));
            }

            _stackGroup.Children.Add(new Spacer(0, 10));
            _stackGroup.Children.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new ScrollableList(10, list),
                    new Spacer(10, 300),
                    _minimap
                }
            });
            _stackGroup.Children.Add(new Spacer(0, 10));
            _stackGroup.Children.Add(new TextButton(
                "Close (Esc)",
                () =>
                {
                    _dialogManager.PopModal();
                    context.Callback(null);
                },
                KeyCode.Escape));
            _stackGroup.Children.Add(new Spacer(0, 20));
        }
    }

    public class TradeRecieverPickerModalContext
    {
        public string Title { get; set; }
        public ITradeActor Sender { get; set; }
        public IEnumerable<ITradeReciever> Options { get; set; }
        public Action<ITradeReciever> Callback { get; set; }
    }
}
