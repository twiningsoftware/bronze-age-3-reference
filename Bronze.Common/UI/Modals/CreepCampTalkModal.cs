﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bronze.Common.Data.Game.CellDevelopments;
using Bronze.Common.Data.World.CellDevelopments;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Modals;

namespace Bronze.Common.UI.Modals
{
    public class CreepCampTalkModal : AbstractModal, IModal<CreepCampDevelopment>
    {
        public override bool PausesGame => true;
        protected override int ModalWidth => Elements.Max(e => e.Bounds.Width);
        protected override int ModalHeight => Elements.Max(e => e.Bounds.Height);

        private readonly IDialogManager _dialogManager;
        private readonly IWorldManager _worldManager;
        private readonly IPlayerDataTracker _playerData;
        private readonly IDiplomacyHelper _diplomacyHelper;
        private readonly INameSource _nameSource;
        private StackGroup _stackGroup;
        
        public CreepCampTalkModal(
            IDialogManager dialogManager,
            IWorldManager worldManager,
            IPlayerDataTracker playerData,
            IDiplomacyHelper diplomacyHelper,
            INameSource nameSource)
            : base(dialogManager)
        {
            _dialogManager = dialogManager;
            _worldManager = worldManager;
            _playerData = playerData;
            _diplomacyHelper = diplomacyHelper;
            _nameSource = nameSource;

            _stackGroup = new StackGroup
            {
                Orientation = Orientation.Vertical,
                HorizontalContentAlignment = HorizontalAlignment.Middle,
                Width = Width.Fixed(300)
            };
        }

        public override void OnLoad()
        {
            base.OnLoad();

            Elements.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new Spacer(10, 0),
                    _stackGroup,
                    new Spacer(10, 0)
                }
            });
        }

        public void Initialize(CreepCampDevelopment camp)
        {
            var buttonRow = new StackGroup
            {
                Orientation = Orientation.Horizontal
            };

            _stackGroup.Children.Clear();
            _stackGroup.Children.Add(new Spacer(0, 10));
            _stackGroup.Children.Add(new Label(camp.Name));
            _stackGroup.Children.Add(new Spacer(0, 10));
            _stackGroup.Children.Add(new Text(EvaluateResponse(camp, "<^_creep_camp_talk>")));
            _stackGroup.Children.Add(new Spacer(0, 10));
            if (camp.PacificationOptions.Any() && !camp.IsBeingPacified)
            {
                var pacificationGroup = new StackGroup
                {
                    Orientation = Orientation.Vertical,
                    HorizontalContentAlignment = HorizontalAlignment.Middle,
                    Children = new List<IUiElement>
                    {
                        new Label("Cost to Pacify"),
                        new Spacer(0, 10),
                    }
                };
                _stackGroup.Children.Add(pacificationGroup);

                foreach (var option in camp.PacificationOptions)
                {
                    pacificationGroup.Children.Add(
                        new StackGroup
                        {
                            Orientation = Orientation.Horizontal,
                            Children = new List<IUiElement>
                            {
                                new SizePadder(
                                    UiBuilder.BuildDisplayFor(option.Inputs.Select(ir => new ItemRate(ir.Item, ir.PerMonth * -1))),
                                    minWidth: 150)
                                {
                                    HorizontalAlignment = HorizontalAlignment.Left
                                },
                                new Spacer(20, 0),
                                new TextButton(
                                    "Pacify",
                                    () => StartPacifying(camp, option),
                                    tooltip: $"Pacify the {camp.Name}, by bribing them. They will aid us in return.")
                            }
                        });
                    pacificationGroup.Children.Add(new Spacer(0, 10));
                }
            }
            else if (camp.IsBeingPacified && camp.PacificationOptions.Any())
            {
                _stackGroup.Children.Add(new StackGroup
                {
                    Orientation = Orientation.Vertical,
                    HorizontalContentAlignment = HorizontalAlignment.Middle,
                    Children = new List<IUiElement>
                        {
                            new Label("Cost to Pacify"),
                            new Spacer(0, 10),
                            UiBuilder.BuildDisplayFor(camp.SelectedPacificationOption.Inputs.Select(ir => new ItemRate(ir.Item, ir.PerMonth * -1)))
                        }
                });
            }

            _stackGroup.Children.Add(new Spacer(0, 10));
            _stackGroup.Children.Add(buttonRow);
            _stackGroup.Children.Add(new Spacer(0, 10));

            if(camp.IsBeingPacified)
            {
                buttonRow.Children.Add(new TextButton(
                    "Stop Pacifying",
                    () => StopPacifying(camp),
                    tooltip: $"Stop pacification efforts, the {camp.Name} will become hostile."));
                buttonRow.Children.Add(new Spacer(10, 0));
            }
            buttonRow.Children.Add(new TextButton("Close (Esc)", () => _dialogManager.PopModal(), KeyCode.Escape));
        }

        private void StopPacifying(CreepCampDevelopment camp)
        {
            camp.SelectedPacificationOption = null;

            _stackGroup.Children.Clear();
            _stackGroup.Children.Add(new Spacer(0, 10));
            _stackGroup.Children.Add(new Label(camp.Name));
            _stackGroup.Children.Add(new Spacer(0, 10));
            _stackGroup.Children.Add(new Text(EvaluateResponse(camp, "<^_creep_camp_stop_pacifying>")));
            _stackGroup.Children.Add(new Spacer(0, 10));
            _stackGroup.Children.Add(new TextButton("Close (Esc)", () => _dialogManager.PopModal(), KeyCode.Escape));
            _stackGroup.Children.Add(new Spacer(0, 10));
        }

        private void StartPacifying(CreepCampDevelopment camp, CreepCampDevelopmentType.PacificationOption pacificationOption)
        {
            camp.SelectedPacificationOption = pacificationOption;

            _stackGroup.Children.Clear();
            _stackGroup.Children.Add(new Spacer(0, 10));
            _stackGroup.Children.Add(new Label(camp.Name));
            _stackGroup.Children.Add(new Spacer(0, 10));
            _stackGroup.Children.Add(new Text(EvaluateResponse(camp, "<^_creep_camp_start_pacifying>")));
            _stackGroup.Children.Add(new Spacer(0, 10));
            _stackGroup.Children.Add(new TextButton("Close (Esc)", () => _dialogManager.PopModal(), KeyCode.Escape));
            _stackGroup.Children.Add(new Spacer(0, 10));
        }

        private string EvaluateResponse(CreepCampDevelopment camp, string pattern)
        {
            var context = new List<string>
            {
                camp.DialogContext
            };

            if(camp.IsBeingPacified)
            {
                context.Add("is_being_pacified");
            }

            if (camp.IsPacified)
            {
                context.Add("is_pacified");
            }

            if(camp.PacificationPercent < -0.5)
            {
                context.Add("pacification_very_negative");
            }
            else if (camp.PacificationPercent < -0)
            {
                context.Add("pacification_negative");
            }
            else if (camp.PacificationPercent < 0.3)
            {
                context.Add("pacification_low");
            }
            else if (camp.PacificationPercent < 0.6)
            {
                context.Add("pacification_medium");
            }
            else
            {
                context.Add("pacification_high");
            }

            if(camp.PacificationIncreasing)
            {
                context.Add("pacification_increasing");
            }
            else
            {
                context.Add("pacification_decreasing");
            }

            var variables = new Dictionary<string, string>
            {
                {"player_ruler", _playerData.PlayerTribe.Ruler.Name },
                {"player_tribe", _playerData.PlayerTribe.Name },
                {"player_race", _playerData.PlayerTribe.Race.Name }
            };

            return _nameSource.EvaluateDialog(
                pattern,
                context,
                variables);
        }
    }
}
