﻿using System.Collections.Generic;
using System.Linq;
using Bronze.Common.UI.Elements;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Modals;

namespace Bronze.Common.UI.Modals
{
    public class CellDetailsModal : AbstractModal, IModal<Cell>
    {
        public override bool PausesGame => true;
        protected override int ModalWidth => Elements.Max(e => e.Bounds.Width);
        protected override int ModalHeight => Elements.Max(e => e.Bounds.Height);

        private StackGroup _stackGroup;
        private readonly IDialogManager _dialogManager;
        private readonly IPlayerDataTracker _playerData;

        public CellDetailsModal(
            IDialogManager dialogManager,
            IPlayerDataTracker playerData)
            : base(dialogManager)
        {
            _dialogManager = dialogManager;
            _playerData = playerData;

            _stackGroup = new StackGroup
            {
                Orientation = Orientation.Vertical,
                HorizontalContentAlignment = HorizontalAlignment.Middle
            };
        }

        public override void OnLoad()
        {
            base.OnLoad();
            Elements.Add(_stackGroup);
        }

        public void Initialize(Cell cell)
        {
            _stackGroup.Children.Clear();
            _stackGroup.Children.Add(new Spacer(0, 20));

            if(cell.Development != null)
            {
                _stackGroup.Children.Add(new Label($"{cell.Development.Name} ({cell.Biome.Name})"));
            }
            else if (cell.Feature != null)
            {
                _stackGroup.Children.Add(new Label($"{cell.Feature.Name} ({cell.Biome.Name})"));
            }
            else
            {
                _stackGroup.Children.Add(new Label(cell.Biome.Name));
            }
            _stackGroup.Children.Add(new Spacer(0, 10));


            var playerOwns = cell.Region.Settlement?.Owner == _playerData.PlayerTribe;
            var playerSees = _playerData.PlayerTribe.IsVisible(cell);

            _stackGroup.Children.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new IUiElement[]
                {
                    BuildTerrainColumn(cell.Biome),
                    cell.Feature?.BuildDetailColumn(),
                    cell.Development?.BuildDetailColumn(playerOwns, playerSees) ?? BuildActivityColumn(cell)
                }
                .Where(x => x != null)
                .SelectMany(x =>
                    new IUiElement[]
                    {
                        new Spacer(20, 0),
                        x
                    }
                )
                .Concat(new Spacer(20, 0).Yield())
                .ToList()
            });

            _stackGroup.Children.Add(new Spacer(0, 20));
            _stackGroup.Children.Add(new TextButton("Close (Esc)", () => _dialogManager.PopModal(), KeyCode.Escape));
            _stackGroup.Children.Add(new Spacer(0, 20));
        }

        private AbstractUiElement BuildTerrainColumn(Biome biome)
        {
            return new StackGroup
            {
                Orientation = Orientation.Vertical,
                Children = new List<IUiElement>
                {
                    new Label(biome.Name),
                    new Spacer(0, 5),
                    new TraitDisplay(() => biome.Traits)
                }
            };
        }

        private IUiElement BuildActivityColumn(Cell cell)
        {
            if(cell.Region.Settlement == null || cell.Region.Settlement.Owner != _playerData.PlayerTribe)
            {
                return null;
            }

            var activityOptions = (cell.Region.Settlement?.Race?.AvailableCellDevelopments ?? Enumerable.Empty<ICellDevelopmentType>())
                .Where(d => d.IsActivity)
                .Where(d => !d.UpgradeOnly)
                .Where(d => d.CanBePlaced(_playerData.PlayerTribe, cell))
                .ToArray();

            return new StackGroup
            {
                Orientation = Orientation.Vertical,
                Children = activityOptions
                    .Select(d => new IconButton(
                        d.IconKey, 
                        d.Name, 
                        () =>
                        {
                            _dialogManager.PushModal<ConstructionInfoModal, ConstructionInfoModalContext>(
                                new ConstructionInfoModalContext
                                {
                                    Cell = cell,
                                    CellDevelopmentTypes = d.Yield()
                                });
                        }))
                    .ToList<IUiElement>()
            };
        }
    }
}
