﻿using System.Collections.Generic;
using System.Linq;
using Bronze.Common.UI.Elements;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Modals;

namespace Bronze.Common.UI.Modals
{
    public class TileDetailsModal : AbstractModal, IModal<Tile>
    {
        public override bool PausesGame => true;
        protected override int ModalWidth => Elements.Max(e => e.Bounds.Width);
        protected override int ModalHeight => Elements.Max(e => e.Bounds.Height);

        private StackGroup _stackGroup;
        private readonly IDialogManager _dialogManager;

        public TileDetailsModal(IDialogManager dialogManager)
            : base(dialogManager)
        {
            _dialogManager = dialogManager;

            _stackGroup = new StackGroup
            {
                Orientation = Orientation.Vertical,
                HorizontalContentAlignment = HorizontalAlignment.Middle
            };
        }

        public override void OnLoad()
        {
            base.OnLoad();
            Elements.Add(_stackGroup);
        }

        public void Initialize(Tile tile)
        {
            _stackGroup.Children.Clear();
            _stackGroup.Children.Add(new Spacer(0, 20));

            if(tile.Structure != null)
            {
                _stackGroup.Children.Add(new Label($"{tile.Structure.Name} ({tile.Terrain.Name})"));
            }
            else
            {
                _stackGroup.Children.Add(new Label(tile.Terrain.Name));
            }
            _stackGroup.Children.Add(new Spacer(0, 10));

            _stackGroup.Children.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new IUiElement[]
                {
                    BuildTerrainColumn(tile.Terrain),
                    tile.Structure?.BuildDetailColumn()
                }
                .Where(x => x != null)
                .SelectMany(x =>
                    new IUiElement[]
                    {
                        new Spacer(20, 0),
                        x
                    }
                )
                .Concat(new Spacer(20, 0).Yield())
                .ToList()
            });

            _stackGroup.Children.Add(new Spacer(0, 20));
            _stackGroup.Children.Add(new TextButton("Close (Esc)", () => _dialogManager.PopModal(), KeyCode.Escape));
            _stackGroup.Children.Add(new Spacer(0, 20));
        }

        private AbstractUiElement BuildTerrainColumn(Terrain terrain)
        {
            return new StackGroup
            {
                Orientation = Orientation.Vertical,
                Children = new List<IUiElement>
                {
                    new Label(terrain.Name),
                    new Spacer(0, 5),
                    new TraitDisplay(() => terrain.Traits)
                }
            };
        }
    }
}
