﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bronze.Common.Data.Game.Units;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Modals;

namespace Bronze.Common.UI.Modals
{
    public class UnitInfoModal : AbstractModal, IModal<UnitInfoModalContext>
    {
        public override bool PausesGame => true;
        protected override int ModalWidth => Elements.Max(e => e.Bounds.Width);
        protected override int ModalHeight => Elements.Max(e => e.Bounds.Height);

        private StackGroup _stackGroup;
        private readonly IDialogManager _dialogManager;
        private readonly IPlayerDataTracker _playerData;
        private readonly IUnitHelper _unitHelper;

        public UnitInfoModal(
            IDialogManager dialogManager,
            IPlayerDataTracker playerData,
            IUnitHelper unitHelper)
            : base(dialogManager)
        {
            _dialogManager = dialogManager;
            _playerData = playerData;
            _unitHelper = unitHelper;

            _stackGroup = new StackGroup
            {
                Orientation = Orientation.Vertical,
                HorizontalContentAlignment = HorizontalAlignment.Middle
            };
        }

        public override void OnLoad()
        {
            base.OnLoad();
            Elements.Add(_stackGroup);
        }

        public void Initialize(UnitInfoModalContext context)
        {
            if (context.Unit == null)
            {
                _dialogManager.PopModal();
                return;
            }

            var unit = context.Unit;

            BuildContent(context, unit, unit.Equipment);
        }

        private void BuildContent(UnitInfoModalContext context, IUnit unit, UnitEquipmentSet equipment)
        {
            var mainColumn = new StackGroup
            {
                Orientation = Orientation.Vertical,
                Width = Width.Fixed(200),
                Children = new List<IUiElement>
                {
                    new Text(unit.Description)
                }
            };

            if (unit.Equipment.Name.Length > 0)
            {
                mainColumn.Children.Add(new IconText(equipment.IconKey, equipment.Name));
                mainColumn.Children.Add(new Spacer(0, 5));
                mainColumn.Children.Add(new Text(equipment.Description));
                mainColumn.Children.Add(new Spacer(0, 10));
            }

            if (unit.UpkeepNeeds.Any())
            {
                mainColumn.Children.Add(new Spacer(0, 10));
                mainColumn.Children.Add(new Label("Upkeep"));
                mainColumn.Children.Add(UiBuilder.BuildDisplayFor(equipment.UpkeepNeeds.Select(u => new ItemRate(u.Item, -u.PerMonth))));
            }

            if (unit is IGrazingUnit grazingUnit)
            {
                mainColumn.Children.Add(new Spacer(0, 10));
                mainColumn.Children.Add(new Label("Production"));
                mainColumn.Children.Add(UiBuilder.BuildDisplayFor(grazingUnit.GrazingProduction, 4));
            }

            var sideColumn = new StackGroup
            {
                Orientation = Orientation.Vertical,
                Children = new List<IUiElement>
                {
                    new IconText("ui_stat_health", $"{(int)unit.Health} / {unit.MaxHealth}", Util.PercentToColor(unit.HealthPercent), tooltip: "Health"),
                    new IconText("ui_stat_supplies", $"{(int)unit.Supplies} / {unit.MaxSupplies}", tooltip: "Months of supplies."),
                    new IconText("ui_stat_resolve", $"{(int)( 100 * unit.Morale)}%", tooltip: "Morale"),
                    new Label(unit.SufferingAttrition ? "Suffering Attrition!" : string.Empty, BronzeColor.Yellow),
                    new StackGroup
                    {
                        Orientation = Orientation.Horizontal,
                        Children = new List<IUiElement>
                        {
                            new IconText("ui_stat_vision", unit.VisionRange.ToString(), tooltip: "Vision range."),
                            new Spacer(10, 0),
                            new IconText("ui_stat_armor", unit.Equipment.Armor.ToString(), tooltip: "Armor: reduces damage taken."),
                            new Spacer(10, 0),
                            new IconText("ui_stat_defense", unit.Equipment.DefenseSkill.ToString(), tooltip: "Defense: Ability to defend against melee attacks."),
                        }
                    },
                    new StackGroup
                    {
                        Orientation = Orientation.Horizontal,
                        Children = new List<IUiElement>
                        {
                            new IconText("ui_stat_block", unit.Equipment.BlockSkill.ToString(), tooltip: "Block: Ability to block melee and ranged attacks."),
                            new Spacer(10, 0),
                            new IconText("ui_stat_skirmish", unit.Equipment.SkirmishSkill.ToString(), tooltip: "Skirmish: Ability to damage when skirmishing."),
                            new Spacer(10, 0),
                            new IconText("ui_stat_resolve", unit.Equipment.ResolveSkill.ToString(), tooltip: "Resolve: Ability to resist morale shocks."),
                        }
                    }
                }
                .Concat(unit.Equipment.MeleeAttacks
                    .Select(m => UiBuilder.BuildDisplayForMeleeAttack(m)))
                .Concat(unit.Equipment.RangedAttacks
                    .Select(m => UiBuilder.BuildDisplayForRangedAttack(m)))
                .Concat(unit.MovementModes
                    .Select(mm => new IconText(mm.MovementType.IconKey, mm.Speed.ToString(), tooltip: mm.MovementType.Name + " Speed")))
                .Concat(unit.AvailableActions
                    .Where(a => !(a is ArmySkirmishAction) && !(a is ArmyApproachAction) && !(a is ArmyInteractAction) && !(a is ArmyMoveAction) && !(a is ArmyMergeAction))
                    .Select(a => new IconText(a.Icon, a.Name, tooltip: a.Description)))
                        .ToList()
            };

            _stackGroup.Children.Clear();
            _stackGroup.Children.Add(new Spacer(0, 20));
            _stackGroup.Children.Add(new Label(unit.Name));
            _stackGroup.Children.Add(new Spacer(0, 10));
            _stackGroup.Children.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new Spacer(10, 0),
                    mainColumn,
                    new Spacer(20, 0),
                    sideColumn,
                    new Spacer(10, 0),
                }
            });
            _stackGroup.Children.Add(new Spacer(0, 20));

            if (context.Army.Owner == _playerData.PlayerTribe)
            {
                _stackGroup.Children.Add(new StackGroup
                {
                    Orientation = Orientation.Horizontal,
                    Children = new List<IUiElement>
                    {
                        new TextButton("Disband",
                            () =>
                            {
                                _unitHelper.Disband(context.Army, context.Unit);

                                _dialogManager.PopModal();
                            },
                            KeyCode.D,
                            tooltip: "Disband the unit, refunding population to the local settlement, if possible.",
                            disableCheck: () => context.Army.IsLocked),
                        new Spacer(20, 0),
                        new TextButton("Change Equipment",
                            () =>
                            {
                                _dialogManager.PushModal<UnitEquipmentModal, UnitEquipmentModalContext>(
                                    new UnitEquipmentModalContext
                                    {
                                        Unit = context.Unit,
                                        Settlement = context.Army.Cell.Region.Settlement
                                    });
                            },
                            KeyCode.C,
                            disableCheck: () => context.Army.Cell.Region.Owner != context.Army.Owner || context.Army.IsLocked,
                            tooltip: context.Army.Cell.Region.Owner != context.Army.Owner ? "Can only change equipment in your own territory." : "Change the unit's equipment.")
                    }
                });
            }

            _stackGroup.Children.Add(new TextButton("Close (Esc)", () => _dialogManager.PopModal(), KeyCode.Escape));
            _stackGroup.Children.Add(new Spacer(0, 20));
        }
    }

    public class UnitInfoModalContext
    {
        public Army Army { get; set; }
        public IUnit Unit { get; set; }
    }
}
