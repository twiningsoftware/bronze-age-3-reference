﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bronze.Common.Data.World.CellDevelopments;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Diplomacy;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Modals;

namespace Bronze.Common.UI.Modals
{
    public class HasAttritionModal : AbstractModal
    {
        public override bool PausesGame => true;
        protected override int ModalWidth => Elements.Max(e => e.Bounds.Width);
        protected override int ModalHeight => Elements.Max(e => e.Bounds.Height);

        private readonly IDialogManager _dialogManager;
        private readonly IPlayerDataTracker _playerData;

        private StackGroup _content;

        public HasAttritionModal(
            IDialogManager dialogManager,
            IPlayerDataTracker playerData)
            : base(dialogManager)
        {
            _dialogManager = dialogManager;
            _playerData = playerData;

            _content = new StackGroup
            {
                Orientation = Orientation.Vertical
            };
        }
        
        public override void OnLoad()
        {
            base.OnLoad();

            Elements.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new Spacer(10, 0),
                    _content,
                    new Spacer(10, 0)
                }
            });

            BuildList();
        }

        private void BuildList()
        {
            _content.Children.Clear();
            _content.HorizontalContentAlignment = HorizontalAlignment.Middle;

            var attritionSources = new List<Tuple<string, CellPosition>>();
            
            foreach (var army in _playerData.PlayerTribe.Armies)
            {
                if (army.Units.Any(u => u.SufferingAttrition))
                {
                    attritionSources.Add(Tuple.Create(army.Name, army.Cell.Position));
                }
            }

            _content.Children.AddRange(new List<IUiElement>
            {
                new Spacer(0, 10),
                new Label("Attrition"),
                new Spacer(0, 5),
                new Text("Without enough supplies, our units will start suffering casualties and desertion, and they may be lost entirely.")
                {
                    Width = Width.Fixed(300)
                },
                new Spacer(0, 10),
                new TableGroup<Tuple<string, CellPosition>>(new string[] {"Army/Garrison", "Location"}, attritionSources, BuildRow, 10),
                new Spacer(0, 5),
                new TextButton("Close (Esc)", () => _dialogManager.PopModal(), KeyCode.Escape),
                new Spacer(0, 10),
            });
        }

        private IEnumerable<IUiElement> BuildRow(Tuple<string, CellPosition> item)
        {
            return new IUiElement[]
            {
                new Label(item.Item1),
                new IconButton(
                    UiImages.IconView,
                    string.Empty,
                    () =>
                    {
                        _playerData.LookAt(item.Item2);
                        _dialogManager.PopModal();
                    },
                    tooltip: "Zoom to the location.")
            };
        }
    }
}
