﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bronze.Common.UI.Elements;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Modals;

namespace Bronze.Common.UI.Modals
{
    public class ArmyMergeModal : AbstractModal, IModal<ArmyMergeModalContext>
    {
        public override bool PausesGame => true;
        protected override int ModalWidth => Elements.Max(e => e.Bounds.Width);
        protected override int ModalHeight => Elements.Max(e => e.Bounds.Height);

        private StackGroup _stackGroup;
        private readonly IDialogManager _dialogManager;
        private readonly IPlayerDataTracker _playerData;
        private readonly IUnitHelper _unitHelper;
        private readonly IWorldManager _worldManager;

        private ArmyMergeModalContext _context;
        private List<IUnit> _fromUnits;
        private List<IUnit> _toUnits;

        public ArmyMergeModal(
            IDialogManager dialogManager,
            IPlayerDataTracker playerData,
            IUnitHelper unitHelper,
            IWorldManager worldManager)
            : base(dialogManager)
        {
            _dialogManager = dialogManager;
            _playerData = playerData;
            _unitHelper = unitHelper;
            _worldManager = worldManager;

            _stackGroup = new StackGroup
            {
                Orientation = Orientation.Vertical,
                HorizontalContentAlignment = HorizontalAlignment.Middle
            };

            _fromUnits = new List<IUnit>();
            _toUnits = new List<IUnit>();
        }

        public override void OnLoad()
        {
            base.OnLoad();
            Elements.Add(_stackGroup);
        }

        public void Initialize(ArmyMergeModalContext context)
        {
            _context = context;

            _fromUnits.AddRange(_context.FromArmy.Units);
            _toUnits.AddRange(_context.ToArmy.Units);
            
            Rebuild();
        }

        private void Rebuild()
        {
            _stackGroup.Children.Clear();
            _stackGroup.Children.Add(new Spacer(0, 20));

            BuildContentFor(
                _context.FromArmy,
                _context.ToArmy);

            _stackGroup.Children.Add(new Spacer(0, 10));

            _stackGroup.Children.Add(new Label(
                    $"{_context.ToArmy.Name} will disband without any units.",
                    BronzeColor.Yellow,
                    () => !_toUnits.Any()));

            _stackGroup.Children.Add(new Label(
                    $"{_context.FromArmy.Name} will disband without any units.",
                    BronzeColor.Yellow,
                    () => !_fromUnits.Any()));
            
            _stackGroup.Children.Add(new Spacer(0, 10));
            _stackGroup.Children.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new TextButton(
                        "Accept", 
                        () =>
                        {
                            DoMerge();
                            _dialogManager.PopModal();
                        },
                        KeyCode.A),
                    new Spacer(20, 0),
                    new TextButton("Close (Esc)", () => _dialogManager.PopModal(), KeyCode.Escape)
                }
            });
            _stackGroup.Children.Add(new Spacer(0, 20));
        }

        private void BuildContentFor(Army fromArmy, Army toArmy)
        {
            var fromElements = new List<IUiElement>();
            var toElements = new List<IUiElement>();

            for (var i = 0; i < Constants.UNITS_PER_ARMY; i++)
            {
                if (i < _fromUnits.Count)
                {
                    var unit = _fromUnits[i];

                    fromElements.Add(new ContainerButton(
                        new UnitSlotDisplay(fromArmy.Owner, unit),
                        () =>
                        {
                            _toUnits.Add(unit);
                            _fromUnits.Remove(unit);
                            Rebuild();
                        },
                        disableCheck: () => _toUnits.Count >= Constants.UNITS_PER_ARMY || !IsCompatable(unit, toArmy)));
                }
                else
                {
                    fromElements.Add(new Spacer(UnitSlotDisplay.ExpectedWidth + 10, UnitSlotDisplay.ExpectedHeight + 10));
                }

                if (i < _toUnits.Count)
                {
                    var unit = _toUnits[i];

                    toElements.Add(new ContainerButton(
                        new UnitSlotDisplay(fromArmy.Owner, unit),
                        () =>
                        {
                            _fromUnits.Add(unit);
                            _toUnits.Remove(unit);
                            Rebuild();
                        },
                        disableCheck: () => _fromUnits.Count >= Constants.UNITS_PER_ARMY || !IsCompatable(unit, fromArmy)));
                }
                else
                {
                    toElements.Add(new Spacer(UnitSlotDisplay.ExpectedWidth + 10, UnitSlotDisplay.ExpectedHeight + 10));
                }
            }

            _stackGroup.Children.Add(new Label($"Merge Armies"));
            _stackGroup.Children.Add(new Spacer(0, 10));
            _stackGroup.Children.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new Spacer(10, 0),
                    new StackGroup
                    {
                        Orientation = Orientation.Vertical,
                        Children = new List<IUiElement>
                        {
                            new SizePadder(new Label(fromArmy.Name), minWidth: 200),
                            UiBuilder.BuildSmallLeaderInfoBlock(fromArmy.General, _worldManager),
                            new Spacer(200, 10),
                            new StackGroup
                            {
                                Orientation = Orientation.Horizontal,
                                Children = new List<IUiElement>
                                {
                                    new StackGroup
                                    {
                                        Orientation = Orientation.Vertical,
                                        Children = fromElements
                                            .Take(Constants.UNITS_PER_ARMY / 2)
                                            .ToList()
                                    },
                                    new Spacer(5, 0),
                                    new StackGroup
                                    {
                                        Orientation = Orientation.Vertical,
                                        Children = fromElements
                                            .Skip(Constants.UNITS_PER_ARMY / 2)
                                            .ToList()
                                    }
                                }
                            }
                        }
                    },
                    new Spacer(20, 0),
                    new StackGroup
                    {
                        Orientation = Orientation.Vertical,
                        Children = new List<IUiElement>
                        {
                            new SizePadder(new Label(toArmy.Name), minWidth: 200),
                            UiBuilder.BuildSmallLeaderInfoBlock(toArmy.General, _worldManager),
                            new Spacer(200, 10),
                            new StackGroup
                            {
                                Orientation = Orientation.Horizontal,
                                Children = new List<IUiElement>
                                {
                                    new StackGroup
                                    {
                                        Orientation = Orientation.Vertical,
                                        Children = toElements
                                            .Take(Constants.UNITS_PER_ARMY / 2)
                                            .ToList()
                                    },
                                    new Spacer(5, 0),
                                    new StackGroup
                                    {
                                        Orientation = Orientation.Vertical,
                                        Children = toElements
                                            .Skip(Constants.UNITS_PER_ARMY / 2)
                                            .ToList()
                                    }
                                }
                            }
                        }
                    },
                    new Spacer(10, 0)
                }
            });
        }
        
        private bool IsCompatable(IUnit unit, Army army)
        {
            return (army.TransportType == null && unit.CanPath(army.Cell))
                || (army.TransportType != null && army.TransportType.CanTransport(unit));
        }

        private void DoMerge()
        {
            foreach(var unit in _fromUnits)
            {
                if (!_context.FromArmy.Units.Contains(unit))
                {
                    _context.FromArmy.Units.Add(unit);
                }
            }
            foreach (var unit in _toUnits)
            {
                if (!_context.ToArmy.Units.Contains(unit))
                {
                    _context.ToArmy.Units.Add(unit);
                }
            }
            foreach (var unit in _fromUnits)
            {
                if (_context.ToArmy.Units.Contains(unit))
                {
                    _unitHelper.RemoveUnit(_context.ToArmy, unit, removedViolently: false);
                }
            }
            foreach (var unit in _toUnits)
            {
                if (_context.FromArmy.Units.Contains(unit))
                {
                    _unitHelper.RemoveUnit(_context.FromArmy, unit, removedViolently: false);
                }
            }
        }
    }

    public class ArmyMergeModalContext
    {
        public Army FromArmy { get; set; }
        public Army ToArmy { get; set; }
    }
}
