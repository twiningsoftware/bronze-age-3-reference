﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bronze.Common.UI.Elements;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Modals;

namespace Bronze.Common.UI.Modals
{
    public class BattleResultNotificationModal : AbstractModal, IModal<BattleResultNotificationModalContext>
    {
        public override bool PausesGame => true;
        protected override int ModalWidth => Elements.Max(e => e.Bounds.Width);
        protected override int ModalHeight => Elements.Max(e => e.Bounds.Height);

        private readonly IDialogManager _dialogManager;
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IPlayerDataTracker _playerData;
        private readonly IWorldManager _worldManager;
        private StackGroup _stackGroup;

        public BattleResultNotificationModal(
            IDialogManager dialogManager,
            IPlayerDataTracker playerData,
            IGamedataTracker gamedataTracker,
            IWorldManager worldManager)
            : base(dialogManager)
        {
            _dialogManager = dialogManager;
            _playerData = playerData;
            _gamedataTracker = gamedataTracker;
            _worldManager = worldManager;

            _stackGroup = new StackGroup
            {
                Orientation = Orientation.Vertical,
                HorizontalContentAlignment = HorizontalAlignment.Middle
            };
        }

        public override void OnLoad()
        {
            base.OnLoad();
            Elements.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new Spacer(5, 0),
                    _stackGroup,
                    new Spacer(5, 0)
                }
            });
        }

        public void Initialize(BattleResultNotificationModalContext context)
        {
            var topRow = new StackGroup
            {
                Orientation = Orientation.Horizontal,
                VerticalContentAlignment = VerticalAlignment.Middle,
                Children = new List<IUiElement>
                {
                    new Spacer(20, 0),
                    new Label(context.Title)
                }
            };

            topRow.Children.Add(new IconButton(
                UiImages.IconView,
                string.Empty,
                () =>
                {
                    _playerData.LookAt(context.LookAt);
                },
                tooltip: "Zoom to the location."));

            var bodyTop = new StackGroup
            {
                Orientation = Orientation.Vertical,
                Width = Width.Fixed(400)
            };
            var bodyLeft = new StackGroup
            {
                Orientation = Orientation.Vertical
            };
            var bodyRight = new StackGroup
            {
                Orientation = Orientation.Vertical
            };

            foreach (var body in context.Body)
            {
                bodyTop.Children.Add(new Text(body));
            }
            
            bodyLeft.Children.Add(new Label("Allies"));
            bodyRight.Children.Add(new Label("Enemies"));

            AddContentToColumn(
                bodyLeft,
                context.AllyLeaders,
                context.AllySurvivors,
                context.AllyDead,
                context.AllyStrengthBefore,
                context.AllyStrengthAfter);

            AddContentToColumn(
                bodyRight,
                context.EnemyLeaders,
                context.EnemySurvivors,
                context.EnemyDead,
                context.EnemyStrengthBefore,
                context.EnemyStrengthAfter);
            
            _stackGroup.Children.Clear();
            _stackGroup.Children.Add(new Spacer(0, 20));
            _stackGroup.Children.Add(topRow);
            _stackGroup.Children.Add(new Spacer(0, 10));
            _stackGroup.Children.Add(bodyTop);
            _stackGroup.Children.Add(new Spacer(0, 10));
            _stackGroup.Children.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new SizePadder(bodyLeft, 200)
                    {
                        HorizontalAlignment = HorizontalAlignment.Middle
                    },
                    new Spacer(20, 0),
                    new SizePadder(bodyRight, 200)
                    {
                        HorizontalAlignment = HorizontalAlignment.Middle
                    }
                }
            });
            _stackGroup.Children.Add(new Spacer(0, 20));
            _stackGroup.Children.Add(new TextButton("Close (Esc)", () => _dialogManager.PopModal(), KeyCode.Escape));
            _stackGroup.Children.Add(new Spacer(0, 20));
        }

        private void AddContentToColumn(
            StackGroup column, 
            NotablePerson[] leaders, 
            int survivors, 
            int dead,
            int strengthBefore,
            int strengthAfter)
        {
            foreach(var leader in leaders)
            {
                column.Children.Add(new StackGroup
                {
                    Orientation = Orientation.Horizontal,
                    Children = new List<IUiElement>
                    {
                        new BiColoredImage(
                            leader.Owner.Race.ArmyFlag,
                            leader.Owner.Race.ArmyFlagIcon,
                            leader.Owner.Name)
                        {
                            PrimaryColorizationUpdater = () => leader.Owner.PrimaryColor,
                            SecondaryColorizationUpdater = () => leader.Owner.SecondaryColor
                        },
                        new Spacer(10, 0),
                        UiBuilder.BuildPersonSummaryButton(_worldManager, _dialogManager, leader)
                    }
                });
            }

            column.Children.Add(new Spacer(0, 20));
            column.Children.Add(new Label($"Units: {survivors + dead} > {survivors}"));
            column.Children.Add(new Spacer(0, 5));
            column.Children.Add(new Label($"Strength: {strengthBefore} > {strengthAfter}"));
        }
    }

    public class BattleResultNotificationModalContext
    {
        public string Title { get; set; }
        public IEnumerable<string> Body { get; set; }
        public CellPosition LookAt { get; set; }
        public int AllySurvivors { get; internal set; }
        public int EnemySurvivors { get; internal set; }
        public int AllyDead { get; internal set; }
        public int EnemyDead { get; internal set; }
        public NotablePerson[] AllyLeaders { get; internal set; }
        public NotablePerson[] EnemyLeaders { get; internal set; }
        public int AllyStrengthBefore { get; internal set; }
        public int AllyStrengthAfter { get; internal set; }
        public int EnemyStrengthBefore { get; internal set; }
        public int EnemyStrengthAfter { get; internal set; }
    }
}
