﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bronze.Common.Data.World.CellDevelopments;
using Bronze.Common.Data.World.Units;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Modals;

namespace Bronze.Common.UI.Modals
{
    public class SiegeInfoModal : AbstractModal, IModal<Settlement>
    {
        public override bool PausesGame => true;
        protected override int ModalWidth => Elements.Max(e => e.Bounds.Width);
        protected override int ModalHeight => Elements.Max(e => e.Bounds.Height);

        private readonly IDialogManager _dialogManager;
        private readonly IWorldManager _worldManager;
        private readonly IPlayerDataTracker _playerData;
        private readonly IDiplomacyHelper _diplomacyHelper;
        private readonly IBattleHelper _battleHelper;
        
        private StackGroup _content;

        private Settlement _forSettlement;
        
        public SiegeInfoModal(
            IDialogManager dialogManager,
            IWorldManager worldManager,
            IPlayerDataTracker playerData,
            IDiplomacyHelper diplomacyHelper,
            IBattleHelper battleHelper)
            : base(dialogManager)
        {
            _dialogManager = dialogManager;
            _worldManager = worldManager;
            _playerData = playerData;
            _diplomacyHelper = diplomacyHelper;
            _battleHelper = battleHelper;

            _content = new StackGroup
            {
                Orientation = Orientation.Vertical,
                HorizontalContentAlignment = HorizontalAlignment.Middle
            };
        }

        public void Initialize(Settlement initValue)
        {
            _forSettlement = initValue;
        }

        public override void OnLoad()
        {
            base.OnLoad();

            Elements.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new Spacer(10, 0),
                    _content,
                    new Spacer(10, 0)
                }
            });

            if (_forSettlement == null)
            {
                ShowList();
            }
            else
            {
                ShowDetail(_forSettlement);
            }
        }

        private void ShowList()
        {
            _content.Children.Clear();

            var settlements = _playerData.PlayerTribe.Armies
                .Select(a => a.CurrentOrder)
                .OfType<ArmyInteractOrder>()
                .Select(o => o.SiegedSettlement)
                .Where(ss => ss != null)
                .Concat(_playerData.PlayerTribe.Settlements
                    .Where(s => s.IsUnderSiege))
                .Distinct()
                .OrderBy(s => s.Name)
                .ToArray();

            _content.Children.Add(new Spacer(0, 10));

            _content.Children.AddRange(new List<IUiElement>
                {
                    new Label("Ongoing Sieges"),
                    new Spacer(0, 5),
                    new SizePadder(
                        new TableGroup<Settlement>(
                            new [] {"Settlement", "Owner", "Morale", "Attackers", "Defenders", "", ""},
                            settlements,
                            s => BuildRow(s),
                            15),
                        minHeight: 300)
                    {
                        VerticalAlignment = VerticalAlignment.Top
                    },
                    new Spacer(0, 5),
                    new TextButton("Close (Esc)", () => _dialogManager.PopModal(), KeyCode.Escape),
                    new Spacer(0, 10),
                });
        }

        private IEnumerable<IUiElement> BuildRow(Settlement settlement)
        {
            var averageSatisfaction = settlement.Population.Average(p => p.Satisfaction);

            var attackerStrength = settlement.SiegedBy.Sum(a => a.StrengthEstimate);

            // TODO garrisoning armies

            var defenderStrength = settlement.RecruitmentSlots.Sum(s => s.Key.StrengthEstimate * (int)s.Value);

            var settlementPosition = settlement.EconomicActors
                .OfType<ICellDevelopment>()
                .Where(cd => cd is VillageDevelopment || cd is DistrictDevelopment)
                .Select(cd => cd.Cell.Position)
                .FirstOrDefault();

            IUiElement actionButton = new Spacer(0, 0);

            if (_playerData.PlayerTribe.IsHostileTo(settlement.Owner))
            {
                actionButton = new TextButton(
                    "Assault",
                    () =>
                    {
                        _dialogManager.PopAllModals();
                        _battleHelper.DoSiegeAssault(settlement, _playerData.PlayerTribe);
                    },
                    KeyCode.A,
                    tooltip: "Assault the settlement.");
            }

            return new IUiElement[]
            {
                //new TextButton(settlement.Name, () => ShowDetail(settlement)),
                new Label(settlement.Name),
                new Label(settlement.Owner.Name),
                new Label($"{(int)(100 * averageSatisfaction)}%"),
                new Label($"{(int)attackerStrength}"),
                new Label($"{(int)defenderStrength}"),
                actionButton,
                new IconButton(
                    UiImages.IconView,
                    string.Empty,
                    () =>
                    {
                        _playerData.LookAt(settlementPosition);
                    },
                    tooltip: "Zoom to the location.")
            };
        }

        private void ShowDetail(Settlement settlement)
        {
            _content.Children.Clear();
            
            _content.Children.Add(new Spacer(0, 10));

            var attackerStrength = settlement.SiegedBy.Sum(a => a.StrengthEstimate);

            // TODO garrisoning armies

            var defenderStrength = settlement.RecruitmentSlots.Sum(s => s.Key.StrengthEstimate * (int)s.Value);

            _content.Children.AddRange(new List<IUiElement>
                {
                    new Label("Siege of " + settlement.Name),
                    new Spacer(0, 5),
                    new StackGroup
                    {
                        Orientation  = Orientation.Horizontal,
                        Children = new List<IUiElement>
                        {
                            new StackGroup
                            {
                                Orientation = Orientation.Vertical,
                                Children = new List<IUiElement>
                                {
                                    new Label("Attackers"),
                                    new Label(((int)attackerStrength).ToString())
                                }
                            },
                            new Spacer(20, 0),
                            new StackGroup
                            {
                                Orientation = Orientation.Vertical,
                                Children = new List<IUiElement>
                                {
                                    new Label("Defenders"),
                                    new Label(((int)defenderStrength).ToString())
                                }
                            }
                        }
                    },
                    new Spacer(0, 10),                    
                });

            var buttonRow = new StackGroup
            {
                Orientation = Orientation.Horizontal
            };

            

            if(_forSettlement == null)
            {
                buttonRow.Children.Add(new TextButton(
                    "Back (Esc)", 
                    () => ShowList(), 
                    KeyCode.Escape));
            }
            else
            {
                buttonRow.Children.Add(new TextButton(
                    "Close (Esc)", 
                    () => _dialogManager.PopModal(),
                    KeyCode.Escape));
            }

            _content.Children.Add(buttonRow);
            _content.Children.Add(new Spacer(0, 10));
        }
    }
}
