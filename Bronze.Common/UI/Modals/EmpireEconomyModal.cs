﻿using System.Collections.Generic;
using System.Linq;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Modals;

namespace Bronze.Common.UI.Modals
{
    public class EmpireEconomyModal : AbstractModal, IModal<Tribe>
    {
        public override bool PausesGame => true;
        protected override int ModalWidth => Elements.Max(e => e.Bounds.Width);
        protected override int ModalHeight => Elements.Max(e => e.Bounds.Height);

        private readonly IDialogManager _dialogManager;
        private readonly IPlayerDataTracker _playerData;
        private readonly ITradeManager _tradeManager;
        private readonly IGamedataTracker _gamedataTracker;
        private StackGroup _stackGroup;

        private int _startSettlement;
        private Tribe _tribe;

        private const int _settlementColumns = 3;
        private const int _columnWidth = 40;

        public EmpireEconomyModal(
            IDialogManager dialogManager,
            IPlayerDataTracker playerData,
            ITradeManager tradeManager,
            IGamedataTracker gamedataTracker)
            : base(dialogManager)
        {
            _dialogManager = dialogManager;
            _playerData = playerData;
            _tradeManager = tradeManager;
            _gamedataTracker = gamedataTracker;

            _stackGroup = new StackGroup
            {
                Orientation = Orientation.Horizontal
            };
        }

        public override void OnLoad()
        {
            base.OnLoad();
            Elements.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new Spacer(5, 0),
                    new StackGroup
                    {
                        Orientation = Orientation.Vertical,
                        HorizontalContentAlignment = HorizontalAlignment.Middle,
                        Children = new List<IUiElement>
                        {
                            new Spacer(0, 20),
                            new Label("Economic Overview"),
                            new Spacer(0, 10),
                            _stackGroup,
                            new Spacer(0, 10),
                            new TextButton("Close (Esc)", () => _dialogManager.PopModal(), KeyCode.Escape),
                            new Spacer(0, 20),
                        }
                    },
                    new Spacer(5, 0)
                }
            });
        }

        public void Initialize(Tribe tribe)
        {
            _tribe = tribe;
            _startSettlement = 0;
            RebuildContent();
        }

        public void RebuildContent()
        {
            _stackGroup.Children.Clear();

            var settlements = _tribe.Settlements
                .OrderByDescending(s => s.Population.Count)
                .Skip(_startSettlement)
                .Take(_settlementColumns)
                .ToArray();

            var items = _gamedataTracker.Items
                .Where(i => _tribe.KnownItems[i])
                .ToArray();

            _stackGroup.Children.Add(new TextButton(
                "<",
                () =>
                {
                    _startSettlement -= 1;
                    RebuildContent();
                },
                disableCheck: () => _startSettlement <= 0));

            _stackGroup.Children.Add(new TableGroup<Item>(
                new IUiElement[] { new Spacer(0,0) }
                    .Concat(settlements
                        .Select(s => new ContainerButton(
                            new SizePadder(new Label(s.Name), _columnWidth * 4),
                            () => _dialogManager.PushModal<SettlementTradeModal, Settlement>(s))))
                    .ToArray(),
                new Item[] { null }.Concat(items),
                i => BuildItemRow(i, settlements),
                15));

            _stackGroup.Children.Add(new TextButton(
                ">",
                () =>
                {
                    _startSettlement += 1;
                    RebuildContent();
                },
                disableCheck: () => _startSettlement + _settlementColumns >= _tribe.Settlements.Count));
        }

        private IEnumerable<IUiElement> BuildItemRow(Item item, Settlement[] settlements)
        {
            var results = new List<IUiElement>();

            if(item == null)
            {
                results.Add(new Spacer(0, 0));

                foreach(var settlement in settlements)
                {
                    results.Add(new StackGroup
                    {
                        Orientation = Orientation.Horizontal,
                        Children = new List<IUiElement>
                        {
                            new SizePadder(new Image("icon_overview_production", "Local production."), _columnWidth)
                            {
                                HorizontalAlignment = HorizontalAlignment.Right
                            },
                            new SizePadder(new Image("icon_overview_consumption", "Local consumption."), _columnWidth)
                            {
                                HorizontalAlignment = HorizontalAlignment.Right
                            },
                            new SizePadder(new Image("icon_overview_imports", "Imports"), _columnWidth)
                            {
                                HorizontalAlignment = HorizontalAlignment.Right
                            },
                            new SizePadder(new Image("icon_overview_exports", "Exports"), _columnWidth)
                            {
                                HorizontalAlignment = HorizontalAlignment.Right
                            },
                            new Spacer(20, 0)
                        }
                    });
                }
            }
            else
            {
                results.Add(new Image(item.IconKey, item.Name));

                foreach (var settlement in settlements)
                {
                    var idealProduction = settlement.EconomicActors
                        .Sum(ea => ea.ExpectedOutputs.Where(ir => ir.Item == item).Sum(ir => ir.PerMonth));
                    var idealConsumption = settlement.EconomicActors
                        .Sum(ea => ea.NeededInputs.Where(ir => ir.Item == item).Sum(ir => ir.PerMonth));
                    var tradeImports = _tradeManager.GetImportsFor(settlement)
                        .Where(ir => ir.Item == item)
                        .Sum(ir => ir.PerMonth);
                    var tradeExports = _tradeManager.GetExportsFor(settlement)
                        .Where(ir => ir.Item == item)
                        .Sum(ir => ir.PerMonth);

                    results.Add(new StackGroup
                    {
                        Orientation = Orientation.Horizontal,
                        Children = new List<IUiElement>
                        {
                            new SizePadder(new Label(idealProduction.ToString("F0"), idealProduction == 0 ? BronzeColor.Grey : BronzeColor.White), _columnWidth)
                            {
                                HorizontalAlignment = HorizontalAlignment.Right
                            },
                            new SizePadder(new Label(idealConsumption.ToString("F0"), idealConsumption == 0 ? BronzeColor.Grey : BronzeColor.White), _columnWidth)
                            {
                                HorizontalAlignment = HorizontalAlignment.Right
                            },
                            new SizePadder(new Label(tradeImports.ToString("F0"), tradeImports == 0 ? BronzeColor.Grey : BronzeColor.White), _columnWidth)
                            {
                                HorizontalAlignment = HorizontalAlignment.Right
                            },
                            new SizePadder(new Label(tradeExports.ToString("F0"), tradeExports == 0 ? BronzeColor.Grey : BronzeColor.White), _columnWidth)
                            {
                                HorizontalAlignment = HorizontalAlignment.Right
                            },
                            new Spacer(20, 0)
                        }
                    });
                }
            }

            return results;
        }
    }
}
