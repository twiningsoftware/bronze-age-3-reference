﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bronze.Common.UI.BattleSimulation;
using Bronze.Common.UI.Elements;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Dungeons;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Simulation.Events;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Modals;

namespace Bronze.Common.UI.Modals
{
    /// <summary>
    /// This is where the main work gets done. This dialog presents the user with the interface for exploring the dungeon.
    /// </summary>
    public class RunDungeonDialog : AbstractModal, IModal<RunDungeonDialogContext>
    {
        public override bool PausesGame => true;
        protected override int ModalWidth => Elements.Max(e => e.Bounds.Width);
        protected override int ModalHeight => Elements.Max(e => e.Bounds.Height);

        private readonly IDialogManager _dialogManager;
        private readonly IWorldManager _worldManager;
        private readonly INameSource _nameSource;
        private readonly ISimulationEventBus _simulationEventBus;
        private StackGroup _stackGroup;
        
        private StackGroup _explorationGroup;
        private StackGroup _sidebarGroup;

        private GeneratedDungeon _dungeon;
        private Cell _dungeonLocation;
        private NotablePerson _character;
        private IUnit _bodyguard;
        private Army _army;
        private Random _random;
        
        public RunDungeonDialog(
            IDialogManager dialogManager,
            IWorldManager worldManager,
            INameSource nameSource,
            ISimulationEventBus simulationEventBus)
            : base(dialogManager)
        {
            _dialogManager = dialogManager;
            _worldManager = worldManager;
            _nameSource = nameSource;
            _simulationEventBus = simulationEventBus;

            _stackGroup = new StackGroup
            {
                Orientation = Orientation.Vertical,
                HorizontalContentAlignment = HorizontalAlignment.Middle
            };
            
            _explorationGroup = new StackGroup
            {
                HorizontalContentAlignment = HorizontalAlignment.Middle,
                Orientation = Orientation.Vertical,
                Width = Width.Fixed(400),
                Height = Height.Fixed(600)
            };
            _sidebarGroup = new StackGroup
            {
                Orientation = Orientation.Vertical
            };
        }

        /// <summary>
        /// Called before Initialize, when the dialog opens.
        /// </summary>
        public override void OnLoad()
        {
            base.OnLoad();

            Elements.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new Spacer(10, 0),
                    _stackGroup,
                    new Spacer(10, 0)
                }
            });
        }

        /// <summary>
        /// Called when the dialog opens, after OnLoad. This is where all of the setup should
        /// happen, and the initial encounter should be displayed.
        /// </summary>
        /// <param name="context">The context information for running the dungeon.</param>
        public void Initialize(RunDungeonDialogContext context)
        {
            _random = new Random();
            _dungeon = context.GeneratedDungeon;
            _character = context.Character;
            _army = context.Army;
            _dungeon.SetToFirstRoom();
            _dungeonLocation = context.DungeonLocation;

            _stackGroup.Children.Clear();
            _sidebarGroup.Children.Clear();
            _explorationGroup.Children.Clear();

            _stackGroup.Children.Add(new Spacer(0, 10));
            _stackGroup.Children.Add(new Label("Exploring " + context.GeneratedDungeon.Name));
            _stackGroup.Children.Add(new Spacer(0, 10));
            _stackGroup.Children.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new StackGroup
                    {
                        Orientation = Orientation.Vertical,
                        Children = new List<IUiElement>
                        {
                            _explorationGroup
                        }
                    },
                    new Spacer(20, 0),
                    _sidebarGroup
                }
            });
            _stackGroup.Children.Add(new Spacer(0, 20));
            if (_character != null)
            {
                _sidebarGroup.Children.Add(new StackGroup
                {
                    Orientation = Orientation.Horizontal,
                    Children = new List<IUiElement>
                {
                    UiBuilder.BuildPersonSummaryButton(_worldManager, _dialogManager, _character),
                    new Spacer(5, 0),
                    new StackGroup
                    {
                        Orientation = Orientation.Vertical,
                        Children = new List<IUiElement>
                        {
                            UiBuilder.BuildStatDisplay(_character, NotablePersonStatType.Valor),
                            UiBuilder.BuildStatDisplay(_character, NotablePersonStatType.Wit),
                            UiBuilder.BuildStatDisplay(_character, NotablePersonStatType.Charisma)
                        }
                    }
                }
                });
            }

            _explorationGroup.Children.Clear();
            _explorationGroup.Children.Add(new Image(_dungeon.OverviewImage ?? string.Empty));
            _explorationGroup.Children.Add(new Text(GenerateText(_dungeon.OverviewDescription ?? string.Empty)));
            _explorationGroup.Children.Add(new Spacer(0, 10));

            if(_character == null)
            {
                _explorationGroup.Children.Add(new Text("You can explore this location, but the army must have a general to lead the exploration."));
            }
            else
            {
                var possibleUnits = _army.Units.Where(u => _dungeon.AcceptableBodyguardCategories.Contains(u.Category)).ToArray();

                _explorationGroup.Children.Add(new Text($"{_character.Name} can explore this location, which unit should accompany {_character.HimHer}?"));

                if(possibleUnits.Any())
                {
                    _explorationGroup.Children.Add(new Text($"Possible unit types: {string.Join(", ", _dungeon.AcceptableBodyguardCategories.Select(uc => uc.Name))}."));
                    _explorationGroup.Children.Add(new Spacer(0, 5));

                    var perRow = 5;

                    for (var i = 0; i < possibleUnits.Length; i += perRow)
                    {
                        _explorationGroup.Children.Add(
                            new StackGroup
                            {
                                Orientation = Orientation.Horizontal,
                                Children = possibleUnits
                                    .Skip(i)
                                    .Take(perRow)
                                    .Select(u => new ContainerButton(
                                        new UnitSlotDisplay(_army.Owner, u),
                                        () =>
                                        {
                                            _bodyguard = u;
                                            StartExploring();
                                        }))
                                    .ToList<IUiElement>()
                            });
                    }
                }
                else
                {
                    _explorationGroup.Children.Add(new Spacer(0, 5));
                    _explorationGroup.Children.Add(new Text($"You don't have any units in the army that can accompany {_character.Name}."));
                    _explorationGroup.Children.Add(new Text($"Possible unit types: {string.Join(", ", _dungeon.AcceptableBodyguardCategories.Select(uc => uc.Name))}."));
                }
            }

            _explorationGroup.Children.Add(new Spacer(0, 10));
            _explorationGroup.Children.Add(new TextButton("Leave", () =>_dialogManager.PopModal(), tooltip: "Leave without exploring the location"));
        }

        private void StartExploring()
        {
            _sidebarGroup.Children.Add(new Spacer(0, 20));
            _sidebarGroup.Children.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new UnitSlotDisplay(_army.Owner, _bodyguard),
                    new Spacer(5, 0),
                    new StackGroup
                    {
                        Orientation = Orientation.Vertical,
                        Children = new List<IUiElement>
                        {
                            new Label("Bodyguard", BronzeColor.Grey),
                            new Label(_bodyguard.Name),
                            new Label("%")
                            {
                                ContentUpdater = () => $"{(int)Math.Round(_bodyguard.HealthPercent * 100)}%",
                                ColorUpdater = () => _bodyguard.HealthPercent > 0.66 ? BronzeColor.Green : (_bodyguard.HealthPercent > 0.33 ? BronzeColor.Orange : BronzeColor.Red)
                            }
                        }
                    }
                }
            });

            DisplayRoom(_dungeon.CurrentRoom);
        }

        /// <summary>
        /// Display an encounter.
        /// </summary>
        /// <param name="room">The encounter to display.</param>
        private void DisplayRoom(DungeonRoom room)
        {
            _sidebarGroup.HideCheck = () => false;

            if (_bodyguard.Health <= 0)
            {
                AbortForDeadBodyguard();
            }
            else if ((_character.Skills.FirstOrDefault(s => s.Skill.Id == "injury_level")?.Level?.Level ?? 0) >= 4)
            {
                AbortForDeadCharacter();
            }
            else
            {
                var description = room.Description;

                _explorationGroup.Children.Clear();
                _explorationGroup.Children.Add(new Image(description?.Image ?? string.Empty));
                _explorationGroup.Children.Add(new Label(description?.Title ?? string.Empty));
                _explorationGroup.Children.Add(new Text(GenerateText(description?.Description ?? string.Empty)));

                _explorationGroup.Children.Add(new Spacer(0, 20));

                foreach (var resolution in room.Resolutions)
                {
                    _explorationGroup.Children.Add(new TextButton(
                        resolution.Text,
                        () => ApplyResolution(room, resolution)));
                }
            }
        }
        
        /// <summary>
        /// Resolve an encounter.
        /// </summary>
        /// <param name="room">The encouunter being resolved.</param>
        /// <param name="resolution">The encounter resolution the player picked.</param>
        private void ApplyResolution(
            DungeonRoom room, 
            IRoomResolution resolution)
        {
            _sidebarGroup.HideCheck = () => false;

            var resolutionData = resolution.GetResolution(
                _random, 
                _dungeonLocation,
                _dungeon,
                _character, 
                _bodyguard);
            
            var resolutionChildren = new List<IUiElement>();

            resolutionChildren.Add(new Text(resolutionData.Description));
            
            foreach (var action in resolutionData.ResolutionActions)
            {
                resolutionChildren.AddRange(action.Apply(_random, _dungeonLocation, _dungeon, _character, _bodyguard));
            }

            _explorationGroup.Children.Clear();

            var resultsStack = new StackGroup
            {
                Orientation = Orientation.Vertical,
                Width = Width.Fixed(400),
            };
            
            resultsStack.Children.Add(new Image(room.Description?.Image ?? string.Empty));
            resultsStack.Children.AddRange(resolutionChildren);
            resultsStack.Children.Add(new Spacer(0, 20));
            resultsStack.Children.Add(new TextButton(
                "Continue",
                () => DisplayRoom(_dungeon.CurrentRoom)));

            if (resolutionData.BattleResult != null)
            {
                var resultWidget = new BattleReplayWidget(resolutionData.BattleResult)
                {
                    Width = Width.Fixed(400),
                    Height = Height.Fixed(200),
                    TimeFactor = 3f
                };
                _explorationGroup.Children.Add(new StackGroup
                {
                    Orientation = Orientation.Vertical,
                    HorizontalContentAlignment = HorizontalAlignment.Middle,
                    HideCheck = () =>
                    {
                        return resultWidget.CurrentTime >= resultWidget.MaxTime;
                    },
                    Children = new List<IUiElement>
                    {
                        resultWidget,
                        new TextButton(
                            "Skip",
                            () => resultWidget.SkipToEnd())
                    }
                });

                _sidebarGroup.HideCheck = () => resultWidget.CurrentTime < resultWidget.MaxTime;

                resultsStack.HideCheck = () => resultWidget.CurrentTime < resultWidget.MaxTime;
            }

            
            
            _explorationGroup.Children.Add(resultsStack);
        }

        private string GenerateText(string text)
        {
            var context = new List<string>();
            var variables = new Dictionary<string, string>();

            variables.Add("character_name", _character?.Name ?? string.Empty);
            variables.Add("bodyguard_name", _bodyguard?.Name ?? string.Empty);

            foreach(var state in _dungeon.CurrentRoom.RoomState.Keys)
            {
                variables.Add(state, _dungeon.CurrentRoom.RoomState[state]);
            }
            
            return _nameSource.EvaluateDialog(
                text,
                context,
                variables);
        }

        private void AbortForDeadBodyguard()
        {
            _explorationGroup.Children.Clear();
            _explorationGroup.Children.Add(new Image(_dungeon.CurrentRoom.Description?.Image ?? string.Empty));
            
            if (_dungeon.CurrentRoom.CanBeEscaped)
            {
                _explorationGroup.Children.Add(new Text($"With {GetPronoun(_character)} bodyguard dead {_character.Name} flees to safety."));
            }
            else
            {
                _explorationGroup.Children.Add(new Text($"With {GetPronoun(_character)} bodyguard dead, {_character.Name} is lost, and never seen again."));
                _character.IsDead = true;
                _simulationEventBus.SendEvent(new NotablePersonDiedSimulationEvent(_character));
            }

            _explorationGroup.Children.Add(new Spacer(0, 20));
            _explorationGroup.Children.Add(new TextButton(
                "Continue",
                () => _dialogManager.PopModal()));
        }
        
        private void AbortForDeadCharacter()
        {
            _explorationGroup.Children.Clear();
            _explorationGroup.Children.Add(new Image(_dungeon.CurrentRoom.Description?.Image ?? string.Empty));

            if (_dungeon.CurrentRoom.CanBeEscaped)
            {
                _explorationGroup.Children.Add(new Text($"With {_character.Name} critically injured, the {_bodyguard.Name} carry {GetPronoun(_character)} her body to safety."));
            }
            else
            {
                _explorationGroup.Children.Add(new Text($"{_character.Name} is critically injured, and without {GetPronoun(_character)} leadership the {_bodyguard.Name} are unable to escape."));
                _bodyguard.Health = 0;
                _character.IsDead = true;
                _simulationEventBus.SendEvent(new NotablePersonDiedSimulationEvent(_character));
            }

            _explorationGroup.Children.Add(new Spacer(0, 20));
            _explorationGroup.Children.Add(new TextButton(
                "Continue",
                () => _dialogManager.PopModal()));
        }

        private string GetPronoun(NotablePerson character)
        {
            return character.IsMale ? "his" : "her";
        }
    }

    /// <summary>
    /// A handy package of all of the data needed to run a dungeon. 
    /// </summary>
    public class RunDungeonDialogContext
    {
        public GeneratedDungeon GeneratedDungeon { get; set; }
        public NotablePerson Character { get; set; }
        public Army Army { get; set; }
        public Cell DungeonLocation { get; set; }
    }
}
