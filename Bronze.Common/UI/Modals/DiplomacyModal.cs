﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bronze.Common.UI.Elements;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Diplomacy;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Modals;

namespace Bronze.Common.UI.Modals
{
    public class DiplomacyModal : AbstractModal, IModal<DiplomacyModalContext>
    {
        public override bool PausesGame => true;
        protected override int ModalWidth => Elements.Max(e => e.Bounds.Width);
        protected override int ModalHeight => Elements.Max(e => e.Bounds.Height);

        private readonly IDialogManager _dialogManager;
        private readonly IWorldManager _worldManager;
        private readonly IPlayerDataTracker _playerData;
        private readonly IDiplomacyHelper _diplomacyHelper;
        private StackGroup _stackGroup;
        
        private StackGroup _conversationGroup;
        private StackGroup _sidebarGroup;

        private Tribe _otherTribe;
        private Action _closeCallback;

        public DiplomacyModal(
            IDialogManager dialogManager,
            IWorldManager worldManager,
            IPlayerDataTracker playerData,
            IDiplomacyHelper diplomacyHelper)
            : base(dialogManager)
        {
            _dialogManager = dialogManager;
            _worldManager = worldManager;
            _playerData = playerData;
            _diplomacyHelper = diplomacyHelper;

            _stackGroup = new StackGroup
            {
                Orientation = Orientation.Vertical,
                HorizontalContentAlignment = HorizontalAlignment.Middle
            };
            
            
            _conversationGroup = new StackGroup
            {
                HorizontalContentAlignment = HorizontalAlignment.Middle,
                Orientation = Orientation.Vertical,
                Width = Width.Fixed(300),
                Height = Height.Fixed(400)
            };
            _sidebarGroup = new StackGroup
            {
                Orientation = Orientation.Vertical
            };
        }

        public override void OnLoad()
        {
            base.OnLoad();

            Elements.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new Spacer(10, 0),
                    _stackGroup,
                    new Spacer(10, 0)
                }
            });
        }

        public void Initialize(DiplomacyModalContext context)
        {
            _closeCallback = context.CloseCallback ?? (() => { });

            _stackGroup.Children.Clear();
            _stackGroup.Children.Add(new Spacer(0, 10));
            _stackGroup.Children.Add(new Label("Diplomacy"));
            _stackGroup.Children.Add(new Spacer(0, 10));
            _stackGroup.Children.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new StackGroup
                    {
                        Orientation = Orientation.Vertical,
                        Children = new List<IUiElement>
                        {
                            UiBuilder.BuildLeaderInfoBlock(context.OtherTribe.Ruler),
                            new Spacer(0, 10),
                            _conversationGroup
                        }
                    },
                    new Spacer(20, 0),
                    _sidebarGroup
                }
            });
            _stackGroup.Children.Add(new Spacer(0, 20));

            _otherTribe = context.OtherTribe;
            
            _sidebarGroup.Children.Add(UiBuilder.BuildPersonSummaryButton(_worldManager, _dialogManager, _playerData.PlayerTribe.Ruler));
            _sidebarGroup.Children.Add(UiBuilder.BuildStatDisplay(_playerData.PlayerTribe.Ruler, NotablePersonStatType.Valor));
            _sidebarGroup.Children.Add(UiBuilder.BuildStatDisplay(_playerData.PlayerTribe.Ruler, NotablePersonStatType.Wit));
            _sidebarGroup.Children.Add(UiBuilder.BuildStatDisplay(_playerData.PlayerTribe.Ruler, NotablePersonStatType.Charisma));
            _sidebarGroup.Children.Add(new Spacer(0, 10));

            var relativePower = _diplomacyHelper.GetRelativePower(_playerData.PlayerTribe, _otherTribe);
            if(relativePower > 0.66)
            {
                _sidebarGroup.Children.Add(new IconText(string.Empty, "Power: Weaker", tooltip: "They are weaker than we are."));
            }
            else if(relativePower < 0.33)
            {
                _sidebarGroup.Children.Add(new IconText(string.Empty, "Power: Stronger", tooltip: "They are stronger than we are."));
            }
            else
            {
                _sidebarGroup.Children.Add(new IconText(string.Empty, "Power: Equal", tooltip: "We are roughly equal in strength."));
            }

            _sidebarGroup.Children.Add(new IconText(string.Empty, "Influence: " + _diplomacyHelper.GetInfluenceFor(_otherTribe))
            {
                Tooltip = new SlicedBackground(UiBuilder.ListInfluenceMemories(_otherTribe, _worldManager.Month))
            });
            _sidebarGroup.Children.Add(new IconText(string.Empty, "Trust: " + _diplomacyHelper.GetTrustFor(_otherTribe))
            {
                Tooltip = new SlicedBackground(UiBuilder.ListTrustMemories(_otherTribe, _worldManager.Month))
            });
            
            ProcessResponse(_otherTribe.Controller.GetDiplomaticResponse(context.InitialAction, null));
        }

        private void ProcessResponse(DiplomaticResponse diplomaticResponse)
        {
            _conversationGroup.Children.Clear();
            _conversationGroup.Children.Add(
                new SizePadder(new Text(diplomaticResponse.ResponseText), minHeight: 300)
                {
                    VerticalAlignment = VerticalAlignment.Top
                });

            var effects = new List<IUiElement>();
            effects.Add(new Spacer(10, 0));

            if (diplomaticResponse.FormedMemory != null)
            {
                var influence = diplomaticResponse.FormedMemory.GetInfluenceAt(_worldManager.Month);
                var trust = diplomaticResponse.FormedMemory.GetTrustAt(_worldManager.Month);

                if(influence > 0)
                {
                    effects.Add(new Label($"+{influence} Influence", BronzeColor.Green));
                    effects.Add(new Spacer(10, 0));
                }
                if (influence < 0)
                {
                    effects.Add(new Label($"{influence} Influence", BronzeColor.Red));
                    effects.Add(new Spacer(10, 0));
                }
                if (trust > 0)
                {
                    effects.Add(new Label($"+{trust} Trust", BronzeColor.Green));
                    effects.Add(new Spacer(10, 0));
                }
                if (trust < 0)
                {
                    effects.Add(new Label($"{trust} Trust", BronzeColor.Red));
                    effects.Add(new Spacer(10, 0));
                }
            }

            _conversationGroup.Children.Add(
                new StackGroup
                {
                    Orientation = Orientation.Horizontal,
                    HorizontalContentAlignment = HorizontalAlignment.Middle,
                    Width = Width.Relative(1f),
                    Children = effects
                });

            _conversationGroup.Children.Add(new Spacer(0, 10));

            foreach(var row in diplomaticResponse.Actions)
            {
                _conversationGroup.Children.Add(
                    new StackGroup
                    {
                        Orientation = Orientation.Horizontal,
                        Children = row
                            .Select(a => new TextButton(
                                a.ButtonText,
                                () =>
                                {
                                    if(a.EndConvo)
                                    {
                                        _dialogManager.PopModal();
                                        _closeCallback?.Invoke();
                                    }
                                    else if (a.StartNegotition)
                                    {
                                        _dialogManager.PushModal<DiplomacyNegotiationModal, DiplomacyNegotiationModalContext>(
                                            new DiplomacyNegotiationModalContext
                                            {
                                                OtherTribe = _otherTribe,
                                                Callback = success => ProcessResponse(_otherTribe.Controller.GetDiplomaticResponse(DiploConstants.ACTION_TYPE_NEGOTIATE, success))
                                            });
                                    }
                                    else
                                    {
                                        ProcessResponse(_otherTribe.Controller.GetDiplomaticResponse(a.ActionType, a.ActionData));
                                    }
                                },
                                disableCheck: () => !a.IsEnabled,
                                tooltip: a.Tooltip))
                            .ToList<IUiElement>()
                    });
            }
        }
    }

    public class DiplomacyModalContext
    {
        public string InitialAction { get; set; }
        public Tribe OtherTribe { get; set; }
        public Action CloseCallback { get; set; }
    }
}
