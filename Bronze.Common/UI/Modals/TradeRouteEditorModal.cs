﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bronze.Common.Data.Diplomacy;
using Bronze.Common.Gameplay.UiElements;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.Trade;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Modals;

namespace Bronze.Common.UI.Modals
{
    public class TradeRouteEditorModal : AbstractModal, IModal<TradeRouteEditorModalContext>
    {
        public override bool PausesGame => true;
        protected override int ModalWidth => Elements.Max(e => e.Bounds.Width);
        protected override int ModalHeight => Elements.Max(e => e.Bounds.Height);

        private readonly IDialogManager _dialogManager;
        private readonly IPlayerDataTracker _playerData;
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IInjectionProvider _injectionProvider;
        private readonly ITradeManager _tradeManager;
        private StackGroup _stackGroup;
        private TradePickerMinimapElement _minimap;
        
        private Settlement _fromSettlement;
        private ITradeActor _fromActor;
        private Settlement _toSettlement;
        private ITradeReciever _toActor;
        private Dictionary<Item, double> _itemsToSend;
        private List<Item> _availableItems;
        private Action<TradeRoute> _callback;
        private TradePathCalculator _tradePathCalculator;
        private TradeRoute _existingRoute;
        
        public TradeRouteEditorModal(
            IDialogManager dialogManager,
            IPlayerDataTracker playerData,
            IGamedataTracker gamedataTracker,
            IInjectionProvider injectionProvider,
            IWorldManager worldManager,
            ITradeManager tradeManager)
            : base(dialogManager)
        {
            _dialogManager = dialogManager;
            _playerData = playerData;
            _gamedataTracker = gamedataTracker;
            _injectionProvider = injectionProvider;
            _tradeManager = tradeManager;

            _itemsToSend = new Dictionary<Item, double>();
            _availableItems = new List<Item>();
            
            _stackGroup = new StackGroup
            {
                Orientation = Orientation.Vertical,
                HorizontalContentAlignment = HorizontalAlignment.Middle
            };

            _minimap = new TradePickerMinimapElement(playerData, worldManager)
            {
                Width = Width.Fixed(300),
                Height = Height.Fixed(300)
            };
        }

        public override void OnLoad()
        {
            base.OnLoad();
            Elements.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new Spacer(5, 200),
                    _stackGroup,
                    new Spacer(5, 200)
                }
            });
        }

        public void Initialize(TradeRouteEditorModalContext context)
        {
            _callback = context.Callback;
            _existingRoute = context.ExistingTradeRoute;

            _fromActor = null;
            _toActor = null;
            _fromSettlement = null;
            _toSettlement = null;
            _itemsToSend.Clear();
            _availableItems.Clear();

            if(_existingRoute != null)
            {
                _fromSettlement = _existingRoute.FromActor.Settlement;
                _fromActor = _existingRoute.FromActor;
                _toSettlement = _existingRoute.ToActor.Settlement;
                _toActor = _existingRoute.ToActor;
                CalculateTradePossibilities();

                foreach(var itemRate in _existingRoute.Exports)
                {
                    _itemsToSend[itemRate.Item] = itemRate.PerMonth;
                }
            }

            RebuildContent();
        }

        private void RebuildContent()
        {
            var fromToPicker = new StackGroup
            {
                Orientation = Orientation.Vertical,
                Children = new List<IUiElement>
                {
                    _minimap,
                    new Spacer(0, 10),
                    new Label("From"),
                    new TextButton(_fromActor == null ? "Pick Trader" : $"{_fromSettlement.Name}, {_fromActor.Name}",
                        () => PickFrom(),
                        disableCheck: () => _existingRoute != null,
                        tooltip: "Pick a source trader.")
                    {
                        HoverAction = () => _minimap.Highlight(_fromActor)
                    },
                    new Spacer(0, 10),
                    new Label("To"),
                    new TextButton(_toActor == null ? "Pick Trade Depot" : $"{_toSettlement.Name}, {_toActor.Name}",
                        () => PickTo(),
                        disableCheck: () => _fromActor == null || _existingRoute != null,
                        tooltip: "Pick a destination depot.")
                    {
                        HoverAction = () => _minimap.Highlight(_toActor)
                    },
                    new Spacer(0, 10),
                    new StackGroup
                    {
                        Orientation = Orientation.Horizontal,
                        Children = new List<IUiElement>
                        {
                            new Image("ui_trade_invalid", "Cannot determine path.")
                            {
                                VisibilityCheck = () => _tradePathCalculator != null && _tradePathCalculator.PathFinished && !_tradePathCalculator.PathSuccessful
                            },
                            new Image("ui_trade_calculating", "Determining path...")
                            {
                                VisibilityCheck = () => _tradePathCalculator != null && !_tradePathCalculator.PathFinished
                            },
                            new Image("ui_trade_valid", "Path determined.")
                            {
                                VisibilityCheck = () => _tradePathCalculator != null && _tradePathCalculator.PathFinished && _tradePathCalculator.PathSuccessful
                            },
                        }
                    }
                },
            };

            if (_fromActor != null)
            {
                fromToPicker.Children.Add(new Spacer(0, 10));
                fromToPicker.Children.Add(UiBuilder.BuildIconFor(_fromActor.TradeType));
                fromToPicker.Children.Add(new Label(string.Empty)
                {
                    ContentUpdater = () => $"Capacity {_itemsToSend.Values.Sum()}/{(int)_fromActor.TradeCapacity}",
                    ColorUpdater = () => _itemsToSend.Values.Sum() <= _fromActor.TradeCapacity ? BronzeColor.Green : BronzeColor.Yellow,
                });
            }

            IUiElement itemColumn;
            if (_fromActor != null && _toActor != null)
            {
                foreach (var item in _availableItems)
                {
                    if (!_itemsToSend.ContainsKey(item))
                    {
                        _itemsToSend.Add(item, 0);
                    }
                }

                itemColumn = new TableGroup<Item>(
                    new[] { "Item", _fromSettlement.Name, _toSettlement.Name, "To Send"},
                    _availableItems,
                    item =>
                    {
                        return new AbstractUiElement[]
                        {
                            new IconText(item.IconKey, string.Empty, tooltip: item.Name),
                            UiBuilder.PerMonthRate(_fromSettlement.NetItemProduction[item]),
                            UiBuilder.PerMonthRate(_toSettlement.NetItemProduction[item]),
                            new TextInput(
                                () => _itemsToSend[item].ToString(),
                                val =>
                                {
                                    if(int.TryParse(val, out var newVal ))
                                    {
                                        _itemsToSend[item] = newVal;
                                    }

                                    if(string.IsNullOrWhiteSpace(val))
                                    {
                                        _itemsToSend[item] = 0;
                                    }
                                },
                                50)
                        };
                    },
                    15);
            }
            else
            {
                itemColumn = new StackGroup
                {
                    Orientation = Orientation.Vertical,
                    Children = new List<IUiElement>
                    {
                        new Label("Pick source and destination.")
                    }
                };
            }
            
            _stackGroup.Children.Clear();
            _stackGroup.Children.Add(new Spacer(0, 20));
            _stackGroup.Children.Add(new Label($"Domestic Trade Route"));
            _stackGroup.Children.Add(new Spacer(0, 5));
            _stackGroup.Children.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new Spacer(10, 0),
                    fromToPicker,
                    new Spacer(20, 300),
                    itemColumn,
                    new Spacer(10, 0),
                }
            });
            
            _stackGroup.Children.Add(new Spacer(0, 10));
            _stackGroup.Children.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new TextButton("Save",
                        () =>
                        {
                            _dialogManager.PopModal();

                            if(_existingRoute == null)
                            {
                                _existingRoute = _tradeManager.CreateTradeRoute(_fromActor, _toActor);
                            }

                            foreach(var item in _itemsToSend.Keys)
                            {
                                _existingRoute.SetExportPerMonth(item, _itemsToSend[item]);
                            }

                            _callback.Invoke(_existingRoute);
                        },
                        KeyCode.S,
                        disableCheck: () =>
                            !_itemsToSend.Any(kvp => kvp.Value > 0)
                            || _itemsToSend.Values.Sum() > _fromActor.TradeCapacity
                            || (!_tradePathCalculator?.PathSuccessful ?? false)),
                    new Spacer(20, 0),
                    new TextButton("Cancel (Esc)",
                        () =>
                        {
                            _dialogManager.PopModal();
                            _callback(null);
                        }, KeyCode.Escape)
                }
            });
            _stackGroup.Children.Add(new Spacer(0, 20));
        }
        
        private void PickFrom()
        {
            var fromActors = _playerData.PlayerTribe
                .Settlements
                .SelectMany(s => s.EconomicActors.OfType<ITradeActor>())
                .Where(ta => !ta.UnderConstruction && ta.TradeRoute == null)
                .ToArray();

            _dialogManager.PushModal<TradeActorPickerModal, TradeActorPickerModalContext>(
                new TradeActorPickerModalContext
                {
                    Title = "Pick Source Trader",
                    Options = fromActors,
                    Callback = o =>
                    {
                        if (o != null)
                        {
                            _fromSettlement = o.Settlement;
                            _fromActor = o;
                            _toSettlement = null;
                            _toActor = null;
                            _itemsToSend.Clear();
                            _availableItems.Clear();
                            RebuildContent();
                            _tradePathCalculator = null;
                        }
                    }
                });
        }

        private void PickTo()
        {
            var toActors = _playerData.PlayerTribe
                .Settlements
                .Where(s => s != _fromSettlement)
                .SelectMany(s => s.EconomicActors.OfType<ITradeReciever>())
                .Where(tr => !tr.UnderConstruction)
                .ToArray();

            _dialogManager.PushModal<TradeRecieverPickerModal, TradeRecieverPickerModalContext>(
                new TradeRecieverPickerModalContext
                {
                    Title = "Pick Destination",
                    Sender = _fromActor,
                    Options = toActors,
                    Callback = o =>
                    {
                        if (o != null)
                        {
                            _toSettlement = o.Settlement;
                            _toActor = o;
                            CalculateTradePossibilities();
                        }
                    }
                });
        }

        private void CalculateTradePossibilities()
        {
            _itemsToSend.Clear();

            _tradePathCalculator = new TradePathCalculator(
                _fromActor.Settlement.Owner,
                _fromActor.TradeType,
                _fromActor,
                _toActor)
            {
                RecalculateOnFailure = false
            };

            _availableItems = _gamedataTracker.Items
                .Where(i => _fromSettlement.ItemAvailability[i])
                .ToList();

            foreach (var item in _gamedataTracker.Items)
            {
                _itemsToSend.Add(item, 0);
            }

            RebuildContent();
        }

        public override void Update(float elapsedSeconds, InputState inputState, IAudioService audioService)
        {
            base.Update(elapsedSeconds, inputState, audioService);

            if (_tradePathCalculator != null)
            {
                _tradePathCalculator.Update();
            }

            if(_tradePathCalculator != null && _tradePathCalculator.PathFinished && _tradePathCalculator.PathSuccessful)
            {
                _minimap.ShowPath(_tradePathCalculator.Path);
            }
            else
            {
                _minimap.ClearPath();
            }
        }
    }

    public class TradeRouteEditorModalContext
    {
        public TradeRoute ExistingTradeRoute { get; set; }
        public Action<TradeRoute> Callback { get; set; }
    }
}
