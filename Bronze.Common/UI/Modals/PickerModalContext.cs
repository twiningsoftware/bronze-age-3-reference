﻿using Bronze.Contracts.UI;
using Bronze.UI.Elements;
using System;
using System.Collections.Generic;

namespace Bronze.Common.UI.Modals
{
    public class PickerModalContext
    {
        public string Title { get; set; }
        public IEnumerable<object> Options { get; set; }
        public Action<object> Callback { get; set; }
        public Action CancelCallback { get; set; }
        public Func<object, string> TextForOption { get; set; }
    }
}
