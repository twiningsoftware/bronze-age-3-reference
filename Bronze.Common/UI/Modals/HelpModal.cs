﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Help;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Modals;

namespace Bronze.Common.UI.Modals
{
    public class HelpModal : AbstractModal, IModal<HelpModalContext>
    {
        private static readonly int CONTENT_WIDTH = 400;
        protected override int ModalWidth => Elements.Max(e => e.Bounds.Width);
        protected override int ModalHeight => Elements.Max(e => e.Bounds.Height);
        public override bool PausesGame => true;

        private readonly IDialogManager _dialogManager;
        private readonly IHelpManager _wikiManager;

        private Stack<Tuple<HelpPage, HelpTopic>> _pageStack;

        private Text _pageTitle;
        private Text _topicTitle;
        private StackGroup _topicImageContainer;
        private StackGroup _topicBody;
        private StackGroup _topicList;

        private HelpPage _currentPage;
        private HelpTopic _currentTopic;
        
        public HelpModal(
            IDialogManager dialogManager,
            IHelpManager wikiManager)
            : base(dialogManager)
        {
            _dialogManager = dialogManager;
            _wikiManager = wikiManager;
            _pageStack = new Stack<Tuple<HelpPage, HelpTopic>>();
        }

        public void Initialize(HelpModalContext initValue)
        {
            var page = _wikiManager.GetPage(initValue.PageId);

            if (page != null)
            {
                var topic = page.Topics
                    .Where(t => t.Id == initValue.TopicId)
                    .FirstOrDefault();

                topic = topic ?? page.Topics.First();

                _currentPage = page;
                _currentTopic = topic;
            }
            else
            {
                page = _wikiManager.StartingPage;

                _currentPage = page;
                _currentTopic = page.Topics.First();
            }
        }

        public override void OnLoad()
        {
            base.OnLoad();

            _pageTitle = new Text("", BronzeColor.Grey)
            {
                Width = Width.Fixed(CONTENT_WIDTH)
            };
            _topicTitle = new Text("")
            {
                Width = Width.Fixed(CONTENT_WIDTH)
            };
            _topicImageContainer = new StackGroup();
            _topicList = new StackGroup
            {
                Orientation = Orientation.Vertical
            };
            _topicBody = new StackGroup
            {
                Orientation = Orientation.Vertical
            };

            Elements.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new Spacer(20, 0),
                    new StackGroup
                    {
                        Orientation = Orientation.Vertical,
                        HorizontalContentAlignment = HorizontalAlignment.Middle,
                        Children = new List<IUiElement>
                        {
                            new Spacer(0, 20),
                            new TextButton("Back (esc)",
                            () =>
                            {
                                if(_pageStack.Any())
                                {
                                    var prev = _pageStack.Pop();
                                    SetContent(prev.Item1, prev.Item2);
                                }
                                else
                                {
                                    _dialogManager.PopModal();
                                }
                            },
                            KeyCode.Escape),
                            new Spacer(0, 10),
                            _topicList,
                            new Spacer(0, 20),
                        }
                    },
                    new Spacer(20, 0),
                    new StackGroup
                    {
                        Orientation = Orientation.Vertical,
                        Children = new List<IUiElement>
                        {
                            new Spacer(CONTENT_WIDTH, 20),
                            new StackGroup
                            {
                                Orientation = Orientation.Vertical,
                                HorizontalContentAlignment = HorizontalAlignment.Middle,
                                Children = new List<IUiElement>
                                {
                                    _pageTitle,
                                    new Spacer(0, 10),
                                    new StackGroup
                                    {
                                        Orientation = Orientation.Horizontal,
                                        Children = new List<IUiElement>
                                        {
                                            _topicImageContainer,
                                            new Spacer(10, 0),
                                            _topicTitle
                                        }
                                    },
                                }
                            },
                            new Spacer(0, 10),
                            _topicBody,
                            new Spacer(CONTENT_WIDTH, 20),
                        }
                    },
                    new Spacer(20, 0),
                }
            });

            SetContent(_currentPage, _currentTopic);
        }

        private void SetContent(HelpPage page, HelpTopic topic)
        {
            var preserveTopicList = _currentPage == page && _topicList.Children.Any();
            
            _currentPage = page;
            _currentTopic = topic;

            _pageTitle.Content = page.Name.ToUpper();
            _topicTitle.Content = topic.Title;
            
            _topicImageContainer.Children.Clear();
            if (!string.IsNullOrWhiteSpace(topic.ImageKey))
            {
                _topicImageContainer.Children.Add(new Image(topic.ImageKey));
            }

            _topicBody.Children = new Spacer(CONTENT_WIDTH, 0).Yield()
                .AsEnumerable<IUiElement>()
                .Concat(
                    topic.Body
                    .SelectMany(hb => new IUiElement[] 
                    {
                        hb.BuildUiElement(),
                        new Spacer(0, 10)
                    }))
                    .ToList();

            if (!preserveTopicList)
            {
                _topicList.Children = new List<IUiElement>
                {
                    new ScrollableList(15, page.Topics
                    .Select(t => new TextButton(t.Title,
                        () =>
                        {
                            SetContent(page, t);
                        },
                        highlightCheck: () => t == _currentTopic))
                    .ToList<IUiElement>())
                    {
                        Width = Width.Fixed(200)
                    }
                };
            }

            var columns = new StackGroup[]
            {
                new StackGroup
                {
                    Orientation = Orientation.Vertical,
                    HorizontalContentAlignment = HorizontalAlignment.Middle
                },
                new StackGroup
                {
                    Orientation = Orientation.Vertical,
                    HorizontalContentAlignment = HorizontalAlignment.Middle
                },
                new StackGroup
                {
                    Orientation = Orientation.Vertical,
                    HorizontalContentAlignment = HorizontalAlignment.Middle
                }
            };
            
            var links = topic.PageLinks
                .Select(pl => CreatePageLinkButton(pl))
                .Where(pl => pl != null)
                .ToArray();

            var nextColumn = 0;
            foreach(var link in links)
            {
                columns[nextColumn].Children.Add(link);
                columns[nextColumn].Children.Add(new Spacer(0, 10));
                
                nextColumn = (nextColumn + 1) % columns.Length;
            }

            var row = new StackGroup
            {
                Orientation = Orientation.Horizontal
            };
            _topicBody.Children.Add(row);
            for (var i = 0; i < columns.Length; i++)
            {
                row.Children.Add(columns[i]);

                if(i < columns.Length - 1)
                {
                    row.Children.Add(new Spacer(30, 0));
                }
            }
        }

        private AbstractUiElement CreatePageLinkButton(HelpLink wikiLink)
        {
            var page = _wikiManager.GetPage(wikiLink.PageId);

            if(page != null)
            {
                var topic = page.Topics
                    .Where(t => t.Id == wikiLink.TopicId)
                    .FirstOrDefault();

                var buttonName = topic?.Title ?? page.Name;

                topic = topic ?? page.Topics.First();

                return new TextButton(buttonName,
                    () =>
                    {
                        _pageStack.Push(new Tuple<HelpPage, HelpTopic>(_currentPage, _currentTopic));
                        SetContent(page, topic);
                    });
            }

            return null;
        }
    }
}
