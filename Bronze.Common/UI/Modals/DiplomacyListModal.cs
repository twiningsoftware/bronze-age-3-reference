﻿using System.Collections.Generic;
using System.Linq;
using Bronze.Common.Data.World.CellDevelopments;
using Bronze.Common.UI.Elements;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Diplomacy;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Modals;

namespace Bronze.Common.UI.Modals
{
    public class DiplomacyListModal : AbstractModal
    {
        public override bool PausesGame => true;
        protected override int ModalWidth => Elements.Max(e => e.Bounds.Width);
        protected override int ModalHeight => Elements.Max(e => e.Bounds.Height);

        private readonly IDialogManager _dialogManager;
        private readonly IWorldManager _worldManager;
        private readonly IPlayerDataTracker _playerData;
        private readonly IDiplomacyHelper _diplomacyHelper;
        private readonly ITreatyHelper _treatyHelper;
        private readonly IWarHelper _warHelper;

        public DiplomacyListModal(
            IDialogManager dialogManager,
            IWorldManager worldManager,
            IPlayerDataTracker playerData,
            IDiplomacyHelper diplomacyHelper,
            ITreatyHelper treatyHelper,
            IWarHelper warHelper)
            : base(dialogManager)
        {
            _dialogManager = dialogManager;
            _worldManager = worldManager;
            _playerData = playerData;
            _diplomacyHelper = diplomacyHelper;
            _treatyHelper = treatyHelper;
            _warHelper = warHelper;
        }
        
        public override void OnLoad()
        {
            base.OnLoad();

            RebuildContent();
        }

        private void RebuildContent()
        {
            Elements.Clear();
            Elements.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new Spacer(10, 0),
                    new StackGroup
                    {
                        Orientation = Orientation.Vertical,
                        HorizontalContentAlignment = HorizontalAlignment.Middle,
                        Children = new List<IUiElement>
                        {
                            new Spacer(0, 10),
                            new Label("Known Tribes"),
                            new Spacer(0, 5),
                            new SizePadder(
                                new TableGroup<Tribe>(
                                    new [] {"", "", "", "Tribe", "Strength", "Pop", "Influence", "Trust", "Status"},
                                    _worldManager.Tribes
                                        .Where(t => _playerData.PlayerTribe.HasKnowledgeOf(t) && t != _playerData.PlayerTribe && !t.IsExtinct && !t.IsBandits),
                                    t => BuildRow(t),
                                    10),
                                minHeight: 300)
                            {
                                VerticalAlignment = VerticalAlignment.Top
                            },
                            new Spacer(0, 5),
                            new TextButton("Close (Esc)", () => _dialogManager.PopModal(), KeyCode.Escape),
                            new Spacer(0, 10),
                        }
                    },
                    new Spacer(10, 0)
                }
            });
        }

        private IEnumerable<IUiElement> BuildRow(Tribe tribe)
        {
            var treatyIcons = new List<IUiElement>();

            if(_warHelper.AreAtWar(_playerData.PlayerTribe, tribe))
            {
                treatyIcons.Add(new Image("treaty_war", "We are at war."));
            }

            treatyIcons.AddRange(
                _treatyHelper.GetTreatiesFor(_playerData.PlayerTribe, tribe)
                    .Select(t => new Image(t.SummaryIconKey, tooltip: t.Name)));

            CellPosition? lookAt = null;
            if(tribe.Capital != null)
            {
                lookAt = tribe.Capital.EconomicActors
                    .OfType<ICellDevelopment>()
                    .Where(cd => cd is VillageDevelopment || cd is DistrictDevelopment)
                    .Select(cd => (CellPosition?)cd.Cell.Position)
                    .FirstOrDefault();
            }

            IUiElement strengthElement;
            var relativePower = _diplomacyHelper.GetRelativePower(_playerData.PlayerTribe, tribe);
            if (relativePower > 0.66)
            {
                strengthElement = new IconText(string.Empty, "Power: Weaker", tooltip: "They are weaker than we are.");
            }
            else if (relativePower < 0.33)
            {
                strengthElement = new IconText(string.Empty, "Power: Stronger", tooltip: "They are stronger than we are.");
            }
            else
            {
                strengthElement = new IconText(string.Empty, "Power: Equal", tooltip: "We are roughly equal in strength.");
            }

            return new IUiElement[]
            {
                new BiColoredImage(tribe.Race.ArmyFlag, tribe.Race.ArmyFlagIcon, tooltip: tribe.Race.Name)
                {
                    PrimaryColorizationUpdater = () => tribe.PrimaryColor,
                    SecondaryColorizationUpdater = () => tribe.SecondaryColor
                },
                new ContainerButton(
                    new SmallNotablePersonPortrait(() => tribe.Ruler),
                    () => _dialogManager.PushModal<NotablePersonDetailsModal, NotablePerson>(tribe.Ruler),
                    () => tribe.Ruler == null),
                new IconButton(
                    UiImages.IconView,
                    string.Empty,
                    () =>
                    {
                        if(lookAt != null)
                        {
                            _playerData.LookAt(lookAt.Value);
                        }
                    },
                    tooltip: "Zoom to the tribe.",
                    disableCheck: () => lookAt == null),
                new TextButton(tribe.Name,
                    () => _dialogManager.PushModal<DiplomacyModal, DiplomacyModalContext>(
                        new DiplomacyModalContext
                        {
                            OtherTribe = tribe,
                            InitialAction = DiploConstants.ACTION_TYPE_GREETING,
                            CloseCallback = RebuildContent
                        })
                    ),
                strengthElement,
                new Label(tribe.Settlements.Sum(s => s.Population.Count).ToString()),
                new IconText(string.Empty, string.Empty, tooltip:"How much influence we have over them.")
                {
                    ContentUpdater = () =>
                    {
                        var influence = _diplomacyHelper.GetInfluenceFor(tribe);
                        return $"{(influence > 0 ? "+" : "")}{influence}";
                    },
                    ColorUpdater = () => _diplomacyHelper.GetInfluenceFor(tribe) < 0 ? BronzeColor.Red : BronzeColor.Green,
                    Tooltip = new SlicedBackground(UiBuilder.ListInfluenceMemories(tribe, _worldManager.Month))
                },
                new IconText(string.Empty, string.Empty, tooltip: "How much they trust us.")
                {
                    ContentUpdater = () =>
                    {
                        var trust = _diplomacyHelper.GetTrustFor(tribe);
                        return $"{(trust > 0 ? "+" : "")}{trust}";
                    },
                    ColorUpdater = () => _diplomacyHelper.GetTrustFor(tribe) < 0 ? BronzeColor.Red : BronzeColor.Green,
                    Tooltip = new SlicedBackground(UiBuilder.ListTrustMemories(tribe, _worldManager.Month))
                },
                new StackGroup
                {
                    Orientation = Orientation.Horizontal,
                    Children = treatyIcons
                }
            };
        }
    }
}
