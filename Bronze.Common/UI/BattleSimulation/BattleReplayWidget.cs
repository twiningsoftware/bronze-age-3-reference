﻿using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Diplomacy;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace Bronze.Common.UI.BattleSimulation
{
    public class BattleReplayWidget : AbstractUiElement
    {
        public Width Width { get; set; }
        public Height Height { get; set; }
        public override bool NeedsRelayout { get; }

        public double CurrentTime { get; private set; }
        public double MaxTime { get; private set; }
        public float TimeFactor { get; set; }
        
        private List<ReplayUnitRepresentation> _units;
        private double _positionMin;
        private double _positionMax;
        private double _yMax;
        private string _backgroundImage;

        public BattleReplayWidget(PitchedBattleResult battleResult)
        {
            Width = Width.Relative(1f);
            Height = Height.Relative(1f);

            CurrentTime = 0;
            MaxTime = battleResult.DetailedResults.Max(dr => dr.Time) + 1;
            TimeFactor = 1;


            _backgroundImage = battleResult.BackgroundImage;
            _positionMin = battleResult.DetailedResults.Min(s => s.Position);
            _positionMax = battleResult.DetailedResults.Max(s => s.Position);

            var timelineByUnit = battleResult.DetailedResults
                .GroupBy(uss => uss.Unit)
                .ToDictionary(g => g.Key, g => g.ToArray());

            _units = new List<ReplayUnitRepresentation>();
            var y = 0;
            foreach(var army in battleResult.Attackers)
            {
                foreach(var unit in army.Units)
                {
                    if(timelineByUnit.ContainsKey(unit))
                    {
                        _units.Add(new ReplayUnitRepresentation(
                            unit,
                            y,
                            timelineByUnit[unit],
                            army.Owner.PrimaryColor,
                            true));
                        y += 1;
                    }
                }
            }
            y = 0;
            foreach (var army in battleResult.Defenders)
            {
                foreach (var unit in army.Units)
                {
                    if (timelineByUnit.ContainsKey(unit))
                    {
                        _units.Add(new ReplayUnitRepresentation(
                            unit,
                            y,
                            timelineByUnit[unit],
                            army.Owner.PrimaryColor,
                            false));
                        y += 1;
                    }
                }
            }
            
            _yMax = _units.Max(u => u.Y);
        }

        public override void DoLayout(IDrawingService drawingService, Rectangle parentBounds)
        {
            Bounds = new Rectangle(
                parentBounds.X,
                parentBounds.Y,
                Width.CalculateWidth(parentBounds),
                Height.CalculateHeight(parentBounds));
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            if (Bounds.Width > 0 && Bounds.Height > 0)
            {
                if(!string.IsNullOrWhiteSpace(_backgroundImage))
                {
                    drawingService.DrawImage(_backgroundImage, Bounds, layer, false);
                }

                var positionOffset = _positionMin;
                var positionScale = (Bounds.Width - 32) / (_positionMax - _positionMin);
                var yScale = Math.Min(20, Bounds.Height / (_yMax + 1));
                var yOffset = (Bounds.Height  / 2) / (_yMax + 1);

                foreach(var unit in _units)
                {
                    unit.Draw(
                        drawingService, 
                        layer + LAYER_STEP, 
                        Bounds, 
                        positionOffset, 
                        positionScale, 
                        yOffset,
                        yScale);
                }
            }
        }

        public override void Update(InputState inputState, IAudioService audioService, float elapsedSeconds)
        {
            base.Update(inputState, audioService, elapsedSeconds);

            if (CurrentTime < MaxTime)
            {
                elapsedSeconds = elapsedSeconds / 1.5f * TimeFactor;

                var previousTime = CurrentTime;
                CurrentTime += elapsedSeconds;

                foreach(var unit in _units)
                {
                    unit.Update(CurrentTime, elapsedSeconds);
                }
            }
        }

        private class ReplayUnitRepresentation
        {
            public int Y { get; private set; }
            private IUnit _unit;
            private UnitStateSnapshot[] _timeline;
            private int _currentSnapshotIndex;
            private double _position;
            private BronzeColor _colorization;
            private bool _isAttacker;
            private int _attackerAdj;
            private IndividualAnimation _currentAnimation;
            private double _animationTime;

            public ReplayUnitRepresentation(IUnit unit, int y, UnitStateSnapshot[] timeline, BronzeColor colorization, bool isAttacker)
            {
                Y = y;
                _unit = unit;
                _timeline = timeline;
                _currentSnapshotIndex = 0;
                _colorization = colorization;
                _isAttacker = isAttacker;

                _attackerAdj = isAttacker ? -16 : 16;

                Update(0, 0);
            }

            public void Update(double currentTime, double elapsedTime)
            {
                if(_currentSnapshotIndex + 1 < _timeline.Length && _timeline[_currentSnapshotIndex + 1].Time <= currentTime)
                {
                    _currentSnapshotIndex += 1;

                    if (_timeline[_currentSnapshotIndex].MadeMeleeAttack && _currentAnimation?.Name != "melee")
                    {
                        _currentAnimation = _unit.IndividualAnimations.Where(a => a.Name == "melee").FirstOrDefault();
                        _animationTime = 0;
                    }

                    if (_timeline[_currentSnapshotIndex].MadeRangedAttack && _currentAnimation?.Name != "ranged")
                    {
                        _currentAnimation = _unit.IndividualAnimations.Where(a => a.Name == "ranged").FirstOrDefault();
                        _animationTime = 0;
                    }

                    if (_timeline[_currentSnapshotIndex].HealthPercent <= 0 && _currentAnimation?.Name != "death" && _currentAnimation?.Name != "corpse")
                    {
                        _currentAnimation = _unit.IndividualAnimations.Where(a => a.Name == "death").FirstOrDefault();
                        _animationTime = 0;
                    }

                    if (_timeline[_currentSnapshotIndex].HealthPercent > 0 
                        && _timeline[_currentSnapshotIndex].MoralePercent <= 0
                        && _currentAnimation?.Name != "walking")
                    {
                        _currentAnimation = _unit.IndividualAnimations.Where(a => a.Name == "walking").FirstOrDefault();
                        _animationTime = 0;
                    }
                }

                if (_currentSnapshotIndex + 1 < _timeline.Length)
                {
                    _position = Util.Lerp(
                        (float)_timeline[_currentSnapshotIndex].Position,
                        (float)_timeline[_currentSnapshotIndex + 1].Position,
                        (float)((currentTime - _timeline[_currentSnapshotIndex].Time) / (_timeline[_currentSnapshotIndex + 1].Time - _timeline[_currentSnapshotIndex].Time)));
                }
                else
                {
                    _position = _timeline[_currentSnapshotIndex].Position;
                }

                _animationTime += elapsedTime;

                if(_currentAnimation == null || _animationTime >= _currentAnimation.LoopTime)
                {
                    if (_timeline[_currentSnapshotIndex].HealthPercent > 0)
                    {
                        _currentAnimation = _unit.IndividualAnimations.Where(a => a.Name == "walking").FirstOrDefault();
                        _animationTime = 0;
                    }
                    else
                    {
                        _currentAnimation = _unit.IndividualAnimations.Where(a => a.Name == "corpse").FirstOrDefault();
                        _animationTime = 0;
                    }
                }
            }

            public void Draw(
                IDrawingService drawingService,
                float layer,
                Rectangle parentBounds,
                double positionOffset,
                double positionScale,
                double yStart,
                double yScale)
            {
                if (_currentAnimation != null)
                {
                    var facing = _isAttacker ? Facing.East : Facing.West;

                    if(_timeline[_currentSnapshotIndex].MoralePercent <= 0 && _timeline[_currentSnapshotIndex].HealthPercent > 0)
                    {
                        facing = _isAttacker ? Facing.West : Facing.East;
                    }

                    var drawPos = new Vector2(
                        (int)(parentBounds.X + (_position - positionOffset) * positionScale) + _attackerAdj,
                        (int)((parentBounds.Y + (Y * yScale)) + yStart));

                    if (_timeline[_currentSnapshotIndex].HealthPercent > 0)
                    {
                        drawingService
                            .DrawLine(
                                drawPos + new Vector2(-10, -12),
                                drawPos + new Vector2(10, -12),
                                4,
                                layer + 1,
                                BronzeColor.Grey,
                                false);

                        drawingService
                            .DrawLine(
                                drawPos + new Vector2(-10, -12),
                                drawPos + new Vector2(-10 + 20 * (float)_timeline[_currentSnapshotIndex].HealthPercent, -12),
                                2,
                                layer + 2,
                                Util.PercentToColor(_timeline[_currentSnapshotIndex].HealthPercent),
                                false);

                        drawingService
                            .DrawLine(
                                drawPos + new Vector2(-10, -10),
                                drawPos + new Vector2(10, -10),
                                2,
                                layer + 1,
                                BronzeColor.Grey,
                                false);

                        drawingService
                            .DrawLine(
                                drawPos + new Vector2(-10, -10),
                                drawPos + new Vector2(-10 + 20 * (float)_timeline[_currentSnapshotIndex].MoralePercent, -10),
                                2,
                                layer + 2,
                                BronzeColor.Blue,
                                false);
                    }

                    _currentAnimation.Draw(
                        drawingService,
                        drawPos,
                        Math.Min(_currentAnimation.LoopTime - 0.001f, _animationTime),
                        _colorization,
                        facing,
                        layer);
                }
            }
        }

        public void SkipToEnd()
        {
            CurrentTime = MaxTime;
        }
    }
}
