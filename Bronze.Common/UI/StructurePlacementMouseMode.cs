﻿using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.UI;
using System;

namespace Bronze.Common.UI
{
    public class StructurePlacementMouseMode : IMouseMode
    {
        public IStructureType StructureType { get; set; }
        public bool PlacementValid { get; set; }

        private Action _cancelAction;

        public StructurePlacementMouseMode(IStructureType structureType, Action cancelAction)
        {
            StructureType = structureType;
            _cancelAction = cancelAction;
        }

        public void OnCancel()
        {
            _cancelAction();
        }

        public bool IsValidFor(ViewMode viewMode)
        {
            return viewMode == ViewMode.Settlement;
        }
    }
}
