﻿using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.UI;
using System;

namespace Bronze.Common.UI
{
    public class CultRiteTargetingMouseMode : IMouseMode
    {
        public ITargetedCultRite CultRite { get; }
        public bool PlacementValid { get; set; }
        public Settlement ForSettlement { get; set; }

        private Action _cancelAction;

        public CultRiteTargetingMouseMode(
            ITargetedCultRite cultRite,
            Settlement forSettlement,
            Action cancelAction)
        {
            CultRite = cultRite;
            _cancelAction = cancelAction;
            ForSettlement = forSettlement;
        }

        public void OnCancel()
        {
            _cancelAction();
        }

        public bool IsValidFor(ViewMode viewMode)
        {
            return viewMode == ViewMode.Detailed || viewMode == ViewMode.Summary;
        }
    }
}
