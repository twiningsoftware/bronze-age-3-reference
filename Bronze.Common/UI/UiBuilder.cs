﻿using Bronze.Common.Data.Game;
using Bronze.Common.Data.World;
using Bronze.Common.UI.Elements;
using Bronze.Common.UI.Modals;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Bronze.Common.UI
{
    public static class UiBuilder
    {
        public static AbstractUiElement BuildDisplayFor(IEnumerable<ItemQuantity> itemQuantities, int perRow = int.MaxValue)
        {
            var itemQuatitiesToDisplay = itemQuantities
                .OrderBy(x => x.Item.Name)
                .ToArray();

            if (itemQuatitiesToDisplay.Length <= perRow)
            {
                return new StackGroup
                {
                    Orientation = Orientation.Horizontal,
                    Children = itemQuantities
                        .SelectMany(x => new IUiElement[]
                        {
                            new IconText(x.Item.IconKey, x.Quantity.ToString(), tooltip: x.Item.Name),
                            new Spacer(5, 0)
                        })
                        .ToList()
                };
            }

            var columnStack = new StackGroup
            {
                Orientation = Orientation.Vertical,
            };

            for (var i = 0; i < itemQuatitiesToDisplay.Length; i += perRow)
            {
                columnStack.Children.Add(new StackGroup
                {
                    Orientation = Orientation.Horizontal,
                    Children = itemQuatitiesToDisplay
                        .Skip(i)
                        .Take(perRow)
                        .SelectMany(x => new IUiElement[]
                            {
                                new IconText(x.Item.IconKey, x.Quantity.ToString(), tooltip: x.Item.Name),
                                new Spacer(5, 0)
                            })
                            .ToList()
                });
            }

            return columnStack;
        }

        public static AbstractUiElement BuildDisplayFor(IEnumerable<ItemRate> itemRates, int perRow = int.MaxValue)
        {
            var itemRatesToDisplay = itemRates
                .Where(x => x.PerMonth != 0)
                .OrderBy(x => x.PerMonth)
                .ThenBy(x => x.Item.Name)
                .ToArray();

            if (itemRatesToDisplay.Length <= perRow)
            {
                return new StackGroup
                {
                    Orientation = Orientation.Horizontal,
                    Children = itemRatesToDisplay
                        .SelectMany(x => new IUiElement[]
                        {
                            BuildItemRateDisplay(x.Item, x.PerMonth, colorizeRate: true),
                            new Spacer(5, 0)
                        })
                        .ToList()
                };
            }

            var columnStack = new StackGroup
            {
                Orientation = Orientation.Vertical,
            };

            for (var i = 0; i < itemRatesToDisplay.Length; i += perRow)
            {
                columnStack.Children.Add(new StackGroup
                {
                    Orientation = Orientation.Horizontal,
                    Children = itemRatesToDisplay
                        .Skip(i)
                        .Take(perRow)
                        .SelectMany(x => new IUiElement[]
                            {
                                new IconText(
                                    x.Item.IconKey,
                                    (x.PerMonth > 0 ? "+" : "" ) + ((int)x.PerMonth).ToString(),
                                    color: x.PerMonth > 0 ? BronzeColor.Green : BronzeColor.Red,
                                    tooltip: x.Item.Name),
                                new Spacer(5, 0)
                            })
                            .ToList()
                });
            }

            return columnStack;
        }

        public static IUiElement BuildDisplayFor(IUnitType unitType, UnitEquipmentSet equipmentSet, BronzeColor colorization)
        {
            var name = unitType.Name;

            if (!string.IsNullOrWhiteSpace(equipmentSet.Name))
            {
                name = $"{unitType.Name} with {equipmentSet.Name}";
            }

            return new IconText(
                equipmentSet.IconKey,
                name)
            {
                IconColorization = colorization
            };
        }

        public static IUiElement BuildDisplayFor(Cult cultMembership)
        {
            if(cultMembership == null)
            {
                return new IconText("cult_none", "No cult membership.");
            }
            else
            {
                return new IconText(cultMembership.IconKey, cultMembership.Name);
            }
        }

        public static IUiElement BuildTooltipFor(Cult cult, double membershipPercent)
        {
            return new SlicedBackground(
                new StackGroup
                {
                    Orientation = Orientation.Vertical,
                    Children = new List<IUiElement>
                        {
                            new Label(cult.Name)
                        }
                        .Concat(cult.SettlementBonuses.Keys
                            .Where(b => cult.SettlementBonuses[b] != 0)
                            .Select(b => new IconText(b.IconKey, (cult.SettlementBonuses[b] * membershipPercent).ToString("P0"))))
                        .Concat(cult.RecruitmentSlots.Keys
                            .Select(rc => new Label($"{(int)Math.Floor(cult.RecruitmentSlots[rc] * membershipPercent)} {rc.Name} recruits.")))
                        .ToList()
                });
        }

        public static IUiElement BuildItemRateDisplay(Item item, double perMonth, bool colorizeRate)
        {
            var color = BronzeColor.White;

            if (colorizeRate)
            {
                color = perMonth > 0 ? BronzeColor.Green : BronzeColor.Red;
            }

            return new IconText(
                item.IconKey,
                (perMonth > 0 ? "+" : "") + perMonth.ToString("F1", CultureInfo.InvariantCulture),
                color,
                tooltip: item.Name);
        }

        public static AbstractUiElement PerMonthRate(double PerMonth)
        {
            return new Label(
                (PerMonth > 0 ? "+" : "") + ((int)PerMonth).ToString(),
                color: PerMonth > 0 ? BronzeColor.Green : BronzeColor.Red);
        }

        public static IUiElement BuildLeaderInfoBlock(NotablePerson person)
        {
            return new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new NotablePersonPortrait(() => person),
                    new Spacer(10, 0),
                    new StackGroup
                    {
                        Orientation = Orientation.Vertical,
                        Children = new List<IUiElement>
                        {
                            new Label(person?.Name ?? string.Empty),
                            BuildStatDisplay(person, NotablePersonStatType.Valor),
                            BuildStatDisplay(person, NotablePersonStatType.Wit),
                            BuildStatDisplay(person, NotablePersonStatType.Charisma)
                        }
                    }
                }
            };
        }
        
        public static IUiElement BuildSmallLeaderInfoBlock(NotablePerson person, IWorldManager worldManager)
        {
            if (person == null)
            {
                return new StackGroup
                {
                    Orientation = Orientation.Horizontal,
                    Children = new List<IUiElement>
                    {

                        new SmallNotablePersonPortrait(() => person)
                    }
                };
            }
            else
            {
                return new StackGroup
                {
                    Orientation = Orientation.Horizontal,
                    Children = new List<IUiElement>
                    {

                        new SmallNotablePersonPortrait(() => person)
                        {
                            Tooltip = new Border(new StackGroup
                            {
                                Orientation = Orientation.Vertical,
                                Children = new List<IUiElement>
                                {
                                    BuildStatDisplay(person, NotablePersonStatType.Valor),
                                    BuildStatDisplay(person, NotablePersonStatType.Wit),
                                    BuildStatDisplay(person, NotablePersonStatType.Charisma)
                                }
                            })
                        },
                        new Spacer(10, 0),
                        new StackGroup
                        {
                            Orientation = Orientation.Vertical,
                            Children = new List<IUiElement>
                            {
                                new Label($"{person.Name} Age: {(int)((worldManager.Month - person.BirthDate) / 12)}"),
                                new Label(person.CurrentAssignment, BronzeColor.Grey)
                            }
                        }
                    }
                };
            }
        }

        public static IUiElement BuildStatDisplay(NotablePerson person, NotablePersonStatType statType)
        {
            return BuildStatDisplay(() => person, statType);
        }

        public static IUiElement BuildStatDisplay(Func<NotablePerson> getPerson, NotablePersonStatType statType)
        {
            string icon;
            var name = FriendlyStatName(statType);
            
            Func<string> tooltipUpdate;
            
            switch (statType)
            {
                case NotablePersonStatType.Charisma:
                    tooltipUpdate = () =>
                    {
                        var res = "Charm and diplomatic ability.";

                        var person = getPerson();
                        if (person != null)
                        {
                            res += $"\n  +{person.GetStat(NotablePersonStatType.Charisma) * Constants.AUTHORITY_PER_STAT} Authority";
                            res += $"\n  +{person.GetBonusBattleStartMorale():P0} morale at the start of battles";
                            res += $"\n  +{person.GetBonusMoraleRegen():F1} morale recovery per month";
                            res += $"\n  +{person.GetBonusSatisfaction ():F2} population satisfaction";
                            if (person.CultMembership != null)
                            {
                                res += $"\n  +{person.GetCultInfluence(person.CultMembership):F2} {person.CultMembership.Adjective} influence per pop";
                            }
                        }
                        
                        return res;
                    };
                    icon = UiImages.STAT_CHARISMA;
                    break;
                case NotablePersonStatType.Valor:
                    tooltipUpdate = () =>
                    {
                        var res = "Personal strength and ability to command.";

                        var person = getPerson();
                        if (person != null)
                        {
                            res += $"\n  +{person.GetStat(NotablePersonStatType.Valor) * Constants.AUTHORITY_PER_STAT} Authority";
                            res += $"\n  -{person.GetBonusMoraleShockReduction():P0} morale loss in battle";
                        }
                        
                        return res;
                    };
                    icon = UiImages.STAT_VALOR;
                    break;
                case NotablePersonStatType.Wit:
                    tooltipUpdate = () =>
                    {
                        var res = "Cunning and intelligence.";

                        var person = getPerson();
                        if (person != null)
                        {
                            res += $"\n  +{person.GetStat(NotablePersonStatType.Wit) * Constants.AUTHORITY_PER_STAT} Authority";
                            res += $"\n  +{person.GetBonusArmySpeed():F1} army movement speed";
                            res += $"\n  +{person.GetBonusProductivity():P0} productivity";
                        }


                        return res;
                    };
                    icon = UiImages.STAT_WIT;
                    break;
                default:
                    throw new Exception("Unexpected stat type: " + statType);
            }

            return new StackGroup
            {
                Orientation = Orientation.Horizontal,
                VerticalContentAlignment = VerticalAlignment.Middle,
                TooltipPosition = TooltipPosition.Above,
                Tooltip = new SlicedBackground(new Label(tooltipUpdate())
                {
                    ContentUpdater = tooltipUpdate
                }),
                Width = Width.Fixed(225),
                Children = new List<IUiElement>
                {
                    new SizePadder(new Label(name), 65)
                    {
                        HorizontalAlignment = HorizontalAlignment.Left
                    },
                }
                    .Concat(Enumerable.Range(0, 10)
                        .Select(i => new Image(icon)
                        {
                            VisibilityCheck = () => i < (getPerson()?.GetStat(statType) ?? 0)
                        }))
                    .ToList()
            };
        }

        public static IUiElement BuildDisplayFor(ITransportType transportType)
        {
            return new IconText(
                transportType.Icon,
                transportType.Name)
            {
                Tooltip = new SlicedBackground(
                    new StackGroup
                    {
                        Orientation = Orientation.Vertical,
                        Children = transportType.MovementModes
                            .Select(mm => new IconText(mm.MovementType.IconKey, $"{mm.MovementType.Name} {mm.Speed}"))
                            .ToList<IUiElement>()
                    })
            };
        }

        public static IUiElement BuildDisplayForRangedAttack(AttackInfo attack)
        {
            return new StackGroup
            {
                Orientation = Orientation.Vertical,
                Children = new List<IUiElement>
                {
                    new StackGroup
                    {
                        Orientation = Orientation.Horizontal,
                        Children = new List<IUiElement>
                        {
                            new IconText(
                                "ui_stat_ranged",
                                attack.Skill.ToString(),
                                tooltip: "Ranged attack skill."),
                            new Spacer(10, 0),
                            new IconText(
                                "ui_stat_strength",
                                attack.Damage.ToString(),
                                tooltip: "Attack damage."),
                            new Spacer(10, 0),
                            new IconText(
                                "ui_stat_ap",
                                attack.ArmorPenetration.ToString(),
                                tooltip: "Armor penetration.")
                        }
                    },
                    new StackGroup
                    {
                        Orientation = Orientation.Horizontal,
                        Children = new List<IUiElement>
                        {
                            new IconText(
                                "ui_stat_range",
                                (attack.Range / Constants.TILE_SIZE_PIXELS).ToString(),
                                tooltip: "Attack range."),
                            new Spacer(10, 0),
                            new IconText(
                                "ui_stat_ammo",
                                attack.Ammo.ToString(),
                                tooltip: "Attack ammunition.")
                        }
                    },
                }
            };
        }

        public static IUiElement BuildDisplayForMeleeAttack(AttackInfo attack)
        {
            return new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new IconText(
                        "ui_stat_melee",
                        attack.Skill.ToString(),
                        tooltip: "Melee attack skill."),
                    new Spacer(10, 0),
                    new IconText(
                        "ui_stat_strength",
                        attack.Damage.ToString(),
                        tooltip: "Attack damage."),
                    new Spacer(10, 0),
                    new IconText(
                        "ui_stat_ap",
                        attack.ArmorPenetration.ToString(),
                        tooltip: "Armor penetration."),
                }
            };
        }

        public static IUiElement BuildDisplayFor(IHousingProvider housingProvider, PopNeedState needState)
        {
            var row = new StackGroup
            {
                Orientation = Orientation.Horizontal
            };

            var needBoundsDescription = string.Empty;
            
            if (needState.Need is ItemPopNeed itemNeed)
            {
                row.Children.Add(BuildItemRateDisplay(itemNeed.Item, -itemNeed.PerMonthPerPop * needState.Residents, colorizeRate: false));
            }
            else if (needState.Need is ServicePopNeed serviceNeed)
            {
                row.Children.Add(new IconText(serviceNeed.Service.IconKey, serviceNeed.Service.Name));
            }
            else if (needState.Need is AuthorityPopNeed)
            {
                row.Children.Add(new IconText(UiImages.ICON_EMPIRE_AUTHORITY, "Authority", tooltip: "Based on the ruler's authority and the tribes total population."));
            }

            if(needState.Need.ProsperityPerPop > 0)
            {
                row.Children.Add(new Spacer(10, 0));
                row.Children.Add(new IconText(UiImages.SMALL_PROSPERITY, needState.BonusProsperity.ToString("F1")));
            }

            row.Children.Add(new Spacer(5, 0));
            row.Children.Add(new IconText(
                string.Empty,
                (int)(needState.Satisfaction * 100) + "%",
                needState.Satisfied ? BronzeColor.Green : BronzeColor.Red,
                needBoundsDescription));
            
            return row;
        }
        
        public static AbstractUiElement ListInfluenceMemories(Tribe otherTribe, double date)
        {
            return new StackGroup
            {
                Orientation = Orientation.Vertical,
                Children = new Label("How much influence we have over them.").Yield()
                .Concat<IUiElement>(otherTribe.DiplomaticMemories
                    .Where(m => (int)m.GetInfluenceAt(date) != 0)
                    .Select(m =>
                    {
                        var influence = (int)m.GetInfluenceAt(date);

                        return new Label($"{m.Description} {(influence > 0 ? "+" : "")}{influence}", influence > 0 ? BronzeColor.Green : BronzeColor.Red);
                    }))
                .ToList()
            };
        }

        public static AbstractUiElement ListTrustMemories(Tribe otherTribe, double date)
        {
            return new StackGroup
            {
                Orientation = Orientation.Vertical,
                Children = new Label("How much they trust us.").Yield()
                .Concat<IUiElement>(otherTribe.DiplomaticMemories
                    .Where(m => (int)m.GetTrustAt(date) != 0)
                    .Select(m =>
                    {
                        var trust = (int)m.GetTrustAt(date);

                        return new Label($"{m.Description} {(trust > 0 ? "+" : "")}{trust}", trust > 0 ? BronzeColor.Green : BronzeColor.Red);
                    }))
                .ToList()
            };
        }

        public static IUiElement BuildMinimapManagementButtons(IDialogManager dialogManager, Tribe tribe)
        {
            return new StackGroup
            {
                Orientation = Orientation.Vertical,
                Children = new List<IUiElement>
                {
                    new IconButton("ui_button_dynasty", string.Empty,
                    () =>
                    {
                        dialogManager.PushModal<DynastyInfoModal, Tribe>(tribe);
                    },
                    disableCheck: () => tribe == null,
                    tooltip: "Dynasty"),
                    new IconButton("ui_button_trade", string.Empty,
                    () =>
                    {
                        dialogManager.PushModal<EmpireEconomyModal, Tribe>(tribe);
                    },
                    disableCheck: () => tribe == null,
                    tooltip: "Economy"),
                    new IconButton("ui_button_diplomacy", string.Empty,
                    () =>
                    {
                        dialogManager.PushModal<DiplomacyListModal>();
                    },
                    disableCheck: () => tribe == null,
                    tooltip: "Diplomacy")
                }
            };
        }

        public static IUiElement BuildPersonSummaryButton(IWorldManager worldManager, IDialogManager dialogManager, NotablePerson person)
        {
            if (person == null)
            {
                return new ContainerButton(
                new SmallNotablePersonPortrait(() => person),
                () =>
                {
                },
                disableCheck: () => true);
            }
            else if (person.IsDead)
            {
                return new ContainerButton(
                new SmallNotablePersonPortrait(() => person),
                () =>
                {
                    dialogManager.PushModal<NotablePersonDetailsModal, NotablePerson>(person);
                },
                tooltip: "dead");
            }
            else
            {
                return new ContainerButton(
                    new SmallNotablePersonPortrait(() => person),
                    () =>
                    {
                        dialogManager.PushModal<NotablePersonDetailsModal, NotablePerson>(person);
                    })
                {
                    Tooltip = new Border(new StackGroup
                    {
                        Orientation = Orientation.Vertical,
                        Children = new List<IUiElement>
                            {
                                new Label($"{person.Name} Age: {(int)((worldManager.Month - person.BirthDate) / 12)}"),
                                BuildStatDisplay(person, NotablePersonStatType.Valor),
                                BuildStatDisplay(person, NotablePersonStatType.Wit),
                                BuildStatDisplay(person, NotablePersonStatType.Charisma)
                            }
                    })
                };
            }
        }

        public static IUiElement BuildDisplayFor(IPopNeed popNeed)
        {
            var row = new StackGroup
            {
                Orientation = Orientation.Horizontal
            };
            
            if (popNeed is ItemPopNeed itemNeed)
            {
                row.Children.Add(BuildItemRateDisplay(itemNeed.Item, -itemNeed.PerMonthPerPop, colorizeRate: true));
            }
            else if (popNeed is ServicePopNeed serviceNeed)
            {
                row.Children.Add(new IconText(serviceNeed.Service.IconKey, serviceNeed.Service.Name));
            }
            else if (popNeed is AuthorityPopNeed)
            {
                row.Children.Add(new IconText(UiImages.ICON_EMPIRE_AUTHORITY, "Authority"));
            }
            else
            {
                throw new ArgumentException("Unknown popneed type: " + popNeed.GetType().Name);
            }

            return row;
        }

        public static AbstractUiElement BuildDisplayFor(NotablePersonSkillPlacement skillPlacement)
        {
            var modifiedStatTypes = skillPlacement.Level.StatBonuses
                .Select(x => x.Stat)
                .Distinct();

            var tooltipChildren = new List<IUiElement>
            {
                new Text(skillPlacement.Level.Description)
                    {
                        Width = Width.Fixed(200)
                    }
            };

            foreach (var statType in modifiedStatTypes)
            {
                var amount = skillPlacement.Level.StatBonuses
                    .Where(sb => sb.Stat == statType)
                    .Sum(sb => sb.Amount);

                if (amount != 0)
                {
                    tooltipChildren.Add(new Label($"{FriendlyStatName(statType)} {(amount >= 0 ? "+" : "")}{amount}", amount < 0 ? BronzeColor.Red : BronzeColor.Green));
                }
            }

            foreach(var cult in skillPlacement.Level.CultInfluence.Keys)
            {
                var amount = skillPlacement.Level.CultInfluence[cult];
                tooltipChildren.Add(new IconText(cult.IconKey, $"{cult.Adjective} Influence {(amount >= 0 ? "+" : "")}{amount}"));
            }

            foreach(var kvp in skillPlacement.Level.MinorStats)
            {
                tooltipChildren.Add(new Label($"{kvp.Key} {(kvp.Value >= 0 ? "+" : "")}{(int)(100 * kvp.Value)}%", BronzeColor.Yellow));
            }

            return new IconText(string.Empty, skillPlacement.Level.Name)
            {
                Tooltip = new SlicedBackground(
                    new StackGroup
                    {
                        Orientation = Orientation.Vertical,
                        Children = tooltipChildren
                    })
            };
        }
        
        public static string FriendlyStatName(NotablePersonStatType statType)
        {
            switch (statType)
            {
                case NotablePersonStatType.Charisma:
                    return "Charisma";
                case NotablePersonStatType.Valor:
                    return "Valor";
                case NotablePersonStatType.Wit:
                    return "Wit";
                default:
                    throw new Exception("Unexpected stat type: " + statType);
            }
        }

        public static AbstractUiElement BuildServiceProvidedTypeRow(ServiceType serviceType, double serviceRange)
        {
            return new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new IconText(serviceType.IconKey, serviceType.Name, tooltip: serviceType.Description),
                    new Spacer(10, 0),
                    new Label("Range: " + (int)serviceRange)
                }
            };
        }

        public static AbstractUiElement BuildServiceProvidedSummaryRow(ServiceType serviceType, Func<double> getEfficiency)
        {
            return new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new IconText(serviceType.IconKey, serviceType.Name, tooltip: serviceType.Description),
                    new Spacer(10, 0),
                    new Label("%")
                    {
                        ContentUpdater = () => (int)(getEfficiency() * 100) + "%",
                        ColorUpdater = () => getEfficiency() >= 1 ? BronzeColor.Green : BronzeColor.Red
                    }
                }
            };
        }

        public static AbstractUiElement BuildServiceProvidedDetailRow(ServiceType serviceType, double serviceRange, Func<double> getEfficiency)
        {
            return new StackGroup
            {
                Orientation = Orientation.Vertical,
                Children = new List<IUiElement>
                {
                    new StackGroup
                    {
                        Orientation = Orientation.Horizontal,
                        Children = new List<IUiElement>
                        {
                            new IconText(serviceType.IconKey, serviceType.Name, tooltip: serviceType.Description),
                            new Spacer(10, 0),
                            new Label("Range: " + (int)serviceRange)
                        }
                    },
                    new Label("%")
                    {
                        ContentUpdater = () => (int)(getEfficiency() * 100) + "%",
                        ColorUpdater = () => getEfficiency() >= 1 ? BronzeColor.Green : BronzeColor.Red
                    }
                }
            };
        }

        public static AbstractUiElement BuildConstructionInfoPanel(IStructureType structureType)
        {
            return BuildConstructionInfoPanel(
                structureType.ConstructionMonths,
                structureType.ConstructionWorkers,
                structureType.ConstructionCost);
        }

        public static AbstractUiElement BuildConstructionInfoPanel(ICellDevelopmentType developmentType, Cell cell)
        {
            return BuildConstructionInfoPanel(
                developmentType.ConstructionMonths,
                developmentType.ConstructionWorkers,
                developmentType.ConstructionCost);
        }

        private static AbstractUiElement BuildConstructionInfoPanel(
            double constructionMonths,
            WorkerNeed constructionWorkers,
            IEnumerable<ItemQuantity> constructionCost)
        {
            return new StackGroup
            {
                Orientation = Orientation.Vertical,
                Children = new List<IUiElement>
                {
                    new Spacer(0,10),
                    new Label("   Construction"),
                    new Label("Time: " + Util.FriendlyTimeDisplay(constructionMonths)),
                    new IconText(constructionWorkers.Caste.IconKey, constructionWorkers.Need.ToString(), tooltip: constructionWorkers.Caste.NamePlural + " needed for construction"),
                    BuildDisplayFor(constructionCost)
                }
            };
        }

        public static AbstractUiElement BuildEconomicInfoPanel(IStructureType structureType)
        {
            return BuildEconomicInfoPanel(
                structureType.OperationWorkers,
                structureType.UpkeepInputs,
                structureType.Recipies,
                structureType.LogiSourceHaulers);
        }

        public static AbstractUiElement BuildEconomicInfoPanel(ICellDevelopmentType developmentType)
        {
            return BuildEconomicInfoPanel(
                developmentType.OperationWorkers,
                developmentType.UpkeepInputs,
                developmentType.Recipies,
                developmentType.LogiSourceHaulers);
        }

        private static AbstractUiElement BuildEconomicInfoPanel(
            WorkerNeed operationWorkers,
            IEnumerable<ItemRate> upkeepInputs,
            IEnumerable<Recipie> recipies,
            IEnumerable<HaulerInfo> logiSourceHaulers)
        {
            var stack = new StackGroup
            {
                Orientation = Orientation.Vertical
            };

            if (operationWorkers.Need != 0 || recipies.Any() || logiSourceHaulers.Any() || upkeepInputs.Any())
            {
                stack.Children.Add(new Spacer(0, 10));
                stack.Children.Add(new Label("   Operation"));

                if (operationWorkers.Need != 0)
                {
                    stack.Children.Add(new IconText(operationWorkers.Caste.IconKey, operationWorkers.Need.ToString(), tooltip: operationWorkers.Caste.NamePlural + " needed for operation"));
                }
                if (recipies.Any())
                {
                    stack.Children.Add(BuildRecipieRow(recipies));
                }
                if (upkeepInputs.Any())
                {
                    stack.Children.Add(BuildUpkeepRow(upkeepInputs));
                }
                foreach (var logiSourceHauler in logiSourceHaulers)
                {
                    stack.Children.Add(BuildHaulerinfoPanel(logiSourceHauler));
                }
            }

            return stack;
        }

        public static AbstractUiElement BuildEconomicActorHoverInfo(
            IGamedataTracker gamedataTracker,
            IEconomicActor actor,
            bool playerOwned,
            bool playerSees)
        {
            var stack = new StackGroup
            {
                Orientation = Orientation.Vertical
            };

            if(actor == null)
            {
                return stack;
            }

            stack.Children.Add(new Label(actor.Name));
            
            if (actor is ICellDevelopment cellDevelopment)
            {
                if (playerSees)
                {
                    stack.Children.Add(new Label("", displayCondition: () => cellDevelopment.Devastation > 0)
                    {
                        ContentUpdater = () => $"Devastation: {(int)(100 * cellDevelopment.Devastation)}%",
                        ColorUpdater = () => cellDevelopment.Devastation > 0.66 ? BronzeColor.Red : (cellDevelopment.Devastation > 0.33 ? BronzeColor.Orange : BronzeColor.Yellow)
                    });

                    if (playerOwned)
                    {
                        stack.Children.Add(new Label("Can Upgrade", BronzeColor.Yellow, displayCondition: () => cellDevelopment.CanManuallyUpgrade));
                    }

                    if (actor.IsUpgrading)
                    {
                        if (cellDevelopment.UpgradingTo.ConstructionMonths > 0)
                        {
                            stack.Children.Add(new Label("")
                            {
                                ContentUpdater = () =>
                                {
                                    if (cellDevelopment?.UpgradingTo != null)
                                    {
                                        return $"Upgrading {(int)(100 * actor?.UpgradeProgress / cellDevelopment?.UpgradingTo?.ConstructionMonths)}%";
                                    }

                                    return string.Empty;
                                }
                            });
                        }
                    }
                }
            }
            else if (actor is IStructure structure1)
            {
                if (playerSees)
                {
                    if (playerOwned)
                    {
                        stack.Children.Add(new Label("Can Upgrade", BronzeColor.Yellow, displayCondition: () => structure1.CanManuallyUpgrade));
                    }

                    if (actor.IsUpgrading)
                    {
                        if (structure1.UpgradingTo.ConstructionMonths > 0)
                        {
                            stack.Children.Add(new Label("")
                            {
                                ContentUpdater = () =>
                                {
                                    if (structure1?.UpgradingTo != null)
                                    {
                                        return $"Upgrading {(int)(100 * actor?.UpgradeProgress / structure1?.UpgradingTo?.ConstructionMonths)}%";
                                    }

                                    return string.Empty;
                                }
                            });
                        }
                    }
                }
            }

            if (playerSees)
            {
                if (actor.UnderConstruction)
                {
                    if (actor.ConstructionMonths > 0)
                    {
                        stack.Children.Add(new Label("")
                        {
                            ContentUpdater = () => $"Under Construction {(int)(100 * actor?.ConstructionProgress / actor?.ConstructionMonths)}%"
                        });
                    }
                    else
                    {
                        stack.Children.Add(new Label("Under Construction"));
                    }
                }
                else
                {
                    if (actor.ActiveRecipie != null)
                    {
                        stack.Children.Add(new Label(actor.ActiveRecipie.Verb)
                        {
                            ContentUpdater = () => $"{actor.ActiveRecipie.Verb}: {actor.ProductionProgress.ToString("P0")}"
                        });
                    }
                }

                stack.Children.Add(BuildFactoryStateDisplay(gamedataTracker, actor));

                if(playerSees)
                {
                    stack.Children.Add(new StackGroup
                    {
                        Orientation = Orientation.Horizontal,
                        VerticalContentAlignment = VerticalAlignment.Middle,
                        Children = gamedataTracker.BonusTypes
                            .Select(bt => new IconText(bt.IconKey, actor.CurrentBonuses[bt].ToString("P0"), tooltip: bt.Name)
                            {
                                ContentUpdater = () => actor.CurrentBonuses[bt].ToString("P0"),
                                DisplayCondition = () => actor.CurrentBonuses[bt] != 0
                            })
                            .ToList<IUiElement>()
                    });
                }
                
                if (actor.NeededInputs.Any() || actor.ExpectedOutputs.Any())
                {
                    stack.Children.Add(new Spacer(0, 5));

                    stack.Children.Add(BuildDisplayFor(
                        actor.NeededInputs
                            .GroupBy(ir => ir.Item)
                            .Select(g => new ItemRate(g.Key, g.Sum(x => x.PerMonth * -1)))
                            .Concat(actor.ExpectedOutputs)));
                }

                stack.Children.Add(new InventoryDisplay(actor.Inventory, 6));

                if (actor.WorkerNeed.Need > 0)
                {
                    stack.Children.Add(new Label("")
                    {
                        ContentUpdater = () => $"Workers {actor?.WorkerSupply} / {actor?.WorkerNeed?.Need}"
                    });
                }

                if (actor is IStructure structure)
                {
                    stack.Children.Add(new StackGroup
                    {
                        Orientation = Orientation.Vertical,
                        HideCheck = () => (structure?.Prosperity ?? 0) <= 0,
                        Children = new List<IUiElement>
                        {
                            new Spacer(0, 5),
                            new IconText(UiImages.SMALL_PROSPERITY, (structure?.Prosperity ?? 0).ToString("F"), tooltip: "Generated prosperity.")
                        }
                    });
                }
            }

            stack.Children.Add(actor.BuildHoverInfoColumn(playerOwned, playerSees));

            return stack;
        }

        public static IUiElement BuildFactoryStateDisplay(IGamedataTracker gamedataTracker, IEconomicActor actor)
        {
            switch (actor.FactoryState)
            {
                case FactoryState.NeedsInputs:
                    return new StackGroup
                    {
                        Orientation = Orientation.Horizontal,
                        Children = new List<IUiElement>
                        {
                            new Label("Missing needed items.", BronzeColor.Orange),
                        }
                    };
                case FactoryState.NoRecipie:
                    return new Label("No recipie selected", BronzeColor.Red);
                case FactoryState.OutputsFull:
                    return new Label("Storage is full.", BronzeColor.Red);
                case FactoryState.Operating:
                default:
                    return new Spacer(0, 0);
            }
        }

        public static AbstractUiElement BuildHaulerinfoPanel(HaulerInfo logiSourceHauler)
        {
            var stack = new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new Label($"Hauler"),
                    new Spacer(10, 0),
                    new Label($"Distance: {logiSourceHauler.HaulDistance}"),
                    new Spacer(10, 0),
                    new Label($"Path: "),
                }
            };

            foreach (var movemenType in logiSourceHauler.MovementTypes)
            {
                stack.Children.Add(BuildIconFor(movemenType));
            }

            return stack;
        }

        public static AbstractUiElement BuildPopDisplay(Settlement settlement)
        {
            return new StackGroup
            {
                Orientation = Orientation.Vertical,
                Children = settlement.Race.Castes
                    .SelectMany(caste => new IUiElement[]
                    {
                        new StackGroup
                        {
                            Orientation = Orientation.Horizontal,
                            HideCheck = () => settlement.PopulationByCaste[caste] < 1 && settlement.GrowthByCaste[caste] == 0,
                            Children = new List<IUiElement>
                            {
                                new IconText(caste.IconKey, string.Empty, tooltip: caste.Name + " population")
                                {
                                    ContentUpdater = () => settlement.PopulationByCaste[caste].ToString()
                                },
                                new Spacer(15, 0),
                                new IconText(UiImages.TINY_POP_HOUSING, string.Empty, tooltip: "Housing for " + caste.NamePlural)
                                {
                                    ContentUpdater = () => settlement.HousingByCaste[caste].ToString()
                                },
                                new Spacer(15, 0),
                                new IconText(caste.WorkerIcon, string.Empty, tooltip: "Available " + caste.Name + " workers.")
                                {
                                    ContentUpdater = () => Math.Max(0, settlement.PopulationByCaste[caste] - settlement.JobsByCaste[caste]).ToString()
                                }
                            }
                        },
                        new StackGroup
                        {
                            Orientation = Orientation.Horizontal,
                            HideCheck = () => settlement.PopulationByCaste[caste] < 1 && settlement.GrowthByCaste[caste] == 0,
                            Children = new List<IUiElement>
                            {
                                new IconText(UiImages.TINY_POP_HAPPINESS, string.Empty, tooltip: caste.Name + " happiness.")
                                {
                                    ContentUpdater = () => (int)(100 * settlement.SatisfactionByCaste[caste]) + "%"
                                },
                                new Spacer(15, 0),
                                new IconText(UiImages.TINY_POP_GROWTH, string.Empty, tooltip: caste.Name + " population growth per month.")
                                {
                                    ContentUpdater = () => "+" + Math.Round(settlement.GrowthByCaste[caste], 2)
                                }
                            }
                        },
                        new Spacer(0, 10)
                    })
                    .ToList()
            };
        }

        public static AbstractUiElement BuildArmySummary(Tribe playerTribe, Army army)
        {
            var commonIcon = army.Units
                .GroupBy(u => u.IconKey)
                .OrderByDescending(g => g.Count())
                .Select(g => g.Key)
                .FirstOrDefault() ?? string.Empty;

            var column = new StackGroup
            {
                Orientation = Orientation.Vertical,
                Children = new List<IUiElement>
                {
                    new IconText(commonIcon, army.Name, tooltip: $"{army.Owner.Name} army")
                }
            };

            if (army.Owner == playerTribe)
            {
                column.Children.Add(new ItemRateDisplay(
                    () => army.UpkeepInputs.ToDictionary(ir => ir.Item, ir => -1 * ir.PerMonth)));
            }

            return column;
        }

        public static AbstractUiElement BuildRecipieRow(IEnumerable<Recipie> recipies)
        {
            return new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = recipies
                    .Select(x => BuildIconFor(x))
                    .ToList()
            };
        }

        public static AbstractUiElement BuildUpkeepRow(IEnumerable<ItemRate> upkeepInputs)
        {
            return new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new Label("Upkeep: "),
                    BuildDisplayFor(upkeepInputs.Select(y => new ItemRate(y.Item, -1 * y.PerMonth)))
                }
            };
        }

        public static AbstractUiElement BuildRecipieButtonRow(IEnumerable<Recipie> recipies, IEconomicActor actor)
        {
            var row = new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>()
            };

            foreach (var recipie in recipies.Where(x => x.ShowToPlayer(actor)))
            {
                AbstractUiElement boostDisplay;
                if (recipie.BoostingTraits.Any())
                {
                    boostDisplay = new StackGroup
                    {
                        Orientation = Orientation.Horizontal,
                        Children = new Label("Boosted By: ").Yield<IUiElement>()
                            .Concat(recipie.BoostingTraits
                            .Select(t => new IconText(t.IconKey, string.Empty, tooltip: t.Name)))
                            .ToList()
                    };
                }
                else
                {
                    boostDisplay = new Spacer(0, 0);
                }

                IUiElement bonusDisplay;
                if (recipie.BonusedBy.Any())
                {
                    bonusDisplay = new StackGroup
                    {
                        Orientation = Orientation.Horizontal,
                        VerticalContentAlignment = VerticalAlignment.Middle,
                        Children = new Label("Affected By: ").Yield<IUiElement>()
                            .Concat(recipie.BonusedBy
                            .Select(b => new IconText(b.IconKey, string.Empty, tooltip: b.Name)))
                            .ToList()
                    };
                }
                else
                {
                    bonusDisplay = new Spacer(0, 0);
                }

                AbstractUiElement slowingDisplay;
                if (recipie.SlowingTraits.Any())
                {
                    slowingDisplay = new StackGroup
                    {
                        Orientation = Orientation.Horizontal,
                        VerticalContentAlignment = VerticalAlignment.Middle,
                        Children = new Label("Slowed By: ").Yield<IUiElement>()
                            .Concat(recipie.SlowingTraits
                            .Select(t => new IconText(t.IconKey, string.Empty, tooltip: t.Name)))
                            .ToList()
                    };
                }
                else
                {
                    slowingDisplay = new Spacer(0, 0);
                }

                AbstractUiElement requirementDisplay;
                if (recipie.RequiredTraits.Any())
                {
                    requirementDisplay = new StackGroup
                    {
                        Orientation = Orientation.Horizontal,
                        VerticalContentAlignment = VerticalAlignment.Middle,
                        Children = new Label("Requires: ").Yield<IUiElement>()
                            .Concat(recipie.RequiredTraits
                            .Select(t => new IconText(t.IconKey, string.Empty, tooltip: t.Name)))
                            .ToList()
                    };
                }
                else
                {
                    requirementDisplay = new Spacer(0, 0);
                }

                row.Children.Add(new IconButton(recipie.IconKey, string.Empty,
                    () =>
                    {
                        actor.ActiveRecipie = recipie;
                    },
                    highlightCheck: () => actor.ActiveRecipie == recipie)
                {
                    Tooltip = new SlicedBackground(new StackGroup
                    {
                        Orientation = Orientation.Vertical,
                        Children = new List<IUiElement>
                            {
                                new Label(recipie .Name),
                                BuildDisplayFor(recipie .Inputs.Select(y => new ItemRate(y.Item, -1 * y.PerMonth)).Concat(recipie .Outputs)),
                                boostDisplay,
                                slowingDisplay,
                                requirementDisplay,
                                bonusDisplay
                            }
                    })
                });
            }

            return row;
        }

        public static AbstractUiElement BuildIconFor(MovementType movementType)
        {
            return new IconText(movementType.IconKey, string.Empty)
            {
                Tooltip = new SlicedBackground(new StackGroup
                {
                    Orientation = Orientation.Horizontal,
                    Children = new List<IUiElement>
                        {
                            new Label(movementType.Name),
                            new Spacer(10, 0)
                        }
                        .Concat(movementType.RequiredTraits
                                .Select(t => new IconText(t.IconKey, string.Empty, tooltip: t.Name)))
                            .ToList()
                })
            };
        }

        public static IUiElement BuildIconFor(Recipie recipie)
        {
            IUiElement boostDisplay;
            if (recipie.BoostingTraits.Any())
            {
                boostDisplay = new StackGroup
                {
                    Orientation = Orientation.Horizontal,
                    VerticalContentAlignment = VerticalAlignment.Middle,
                    Children = new Label("Boosted By: ").Yield<IUiElement>()
                        .Concat(recipie.BoostingTraits
                        .Select(t => new IconText(t.IconKey, string.Empty, tooltip: t.Name)))
                        .ToList()
                };
            }
            else
            {
                boostDisplay = new Spacer(0, 0);
            }
            IUiElement bonusDisplay;
            if (recipie.BonusedBy.Any())
            {
                bonusDisplay = new StackGroup
                {
                    Orientation = Orientation.Horizontal,
                    VerticalContentAlignment = VerticalAlignment.Middle,
                    Children = new Label("Affected By: ").Yield<IUiElement>()
                        .Concat(recipie.BonusedBy
                        .Select(b => new IconText(b.IconKey, string.Empty, tooltip: b.Name)))
                        .ToList()
                };
            }
            else
            {
                bonusDisplay = new Spacer(0, 0);
            }

            IUiElement slowingDisplay;
            if (recipie.SlowingTraits.Any())
            {
                slowingDisplay = new StackGroup
                {
                    Orientation = Orientation.Horizontal,
                    VerticalContentAlignment = VerticalAlignment.Middle,
                    Children = new Label("Slowed By: ").Yield<IUiElement>()
                        .Concat(recipie.SlowingTraits
                        .Select(t => new IconText(t.IconKey, string.Empty, tooltip: t.Name)))
                        .ToList()
                };
            }
            else
            {
                slowingDisplay = new Spacer(0, 0);
            }

            AbstractUiElement requirementDisplay;
            if (recipie.RequiredTraits.Any())
            {
                requirementDisplay = new StackGroup
                {
                    Orientation = Orientation.Horizontal,
                    VerticalContentAlignment = VerticalAlignment.Middle,
                    Children = new Label("Requires: ").Yield<IUiElement>()
                        .Concat(recipie.RequiredTraits
                        .Select(t => new IconText(t.IconKey, string.Empty, tooltip: t.Name)))
                        .ToList()
                };
            }
            else
            {
                requirementDisplay = new Spacer(0, 0);
            }

            return new IconText(recipie.IconKey, string.Empty)
            {
                Tooltip = new SlicedBackground(new StackGroup
                {
                    Orientation = Orientation.Vertical,
                    Children = new List<IUiElement>
                    {
                        new Label(recipie.Name),
                        BuildDisplayFor(recipie.Inputs.Select(y => new ItemRate(y.Item, -1 * y.PerMonth)).Concat(recipie.Outputs)),
                        boostDisplay,
                        slowingDisplay,
                        requirementDisplay,
                        bonusDisplay,
                    }
                })
            };
        }

        public static AbstractUiElement BuildIconFor(IStructureType structureType)
        {
            return new IconText(structureType.IconKey, string.Empty)
            {
                Tooltip = new SlicedBackground(new StackGroup
                {
                    Orientation = Orientation.Vertical,
                    Children = new List<IUiElement>
                    {
                        new Label(structureType.Name),
                        new Text(structureType.Description)
                        {
                            Width = Width.Fixed(200)
                        }
                    }
                })
            };
        }

        public static IUiElement BuildIconFor(ICellDevelopmentType developmentType)
        {
            return new IconText(developmentType.IconKey, string.Empty)
            {
                Tooltip = new SlicedBackground(new StackGroup
                {
                    Orientation = Orientation.Vertical,
                    Children = new List<IUiElement>
                    {
                        new Label(developmentType.Name),
                        new Text(developmentType.Description)
                        {
                            Width = Width.Fixed(200)
                        }
                    }
                })
            };
        }

        public static IUiElement BuildIconFor(IUnitType unitType, UnitEquipmentSet equipmentSet, BronzeColor colorization)
        {
            if(equipmentSet.Name.Length > 0)
            {
                return new IconText(equipmentSet.IconKey, string.Empty, colorization, tooltip: $"{unitType.Name} with {equipmentSet.Name}");
            }
            else
            {
                return new IconText(equipmentSet.IconKey, string.Empty, colorization, tooltip: $"{unitType.Name}");
            }
        }

        public static AbstractUiElement BuildIconFor(ServiceType serviceType)
        {
            return new IconText(serviceType.IconKey, string.Empty)
            {
                Tooltip = new SlicedBackground(new StackGroup
                {
                    Orientation = Orientation.Vertical,
                    Children = new List<IUiElement>
                    {
                        new Label(serviceType.Name),
                        new Spacer(5, 0),
                        new Text(serviceType.Description)
                        {
                            Width = Width.Fixed(200)
                        },
                    }
                })
            };
        }

        public static IEnumerable<IUiElement> DoRowLayout(
            IEnumerable<IUiElement> elements,
            int itemsPerRow,
            int padding)
        {
            var results = new List<IUiElement>();

            for (var i = 0; i < elements.Count(); i += itemsPerRow)
            {
                results.Add(new StackGroup
                {
                    Orientation = Orientation.Horizontal,
                    Children = elements
                        .Skip(i)
                        .Take(itemsPerRow)
                        .SelectMany(x => new IUiElement[]
                        {
                            x,
                            new Spacer(0, padding)
                        })
                        .ToList()
                });
                results.Add(new Spacer(0, padding));
            }

            return results;
        }
    }
}
