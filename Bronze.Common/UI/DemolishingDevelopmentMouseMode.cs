﻿using Bronze.Contracts.Data;
using Bronze.Contracts.UI;
using System;

namespace Bronze.Common.UI
{
    public class DemolishingDevelopmentMouseMode : IMouseMode
    {
        private Action _cancelAction;

        public DemolishingDevelopmentMouseMode(Action cancelAction)
        {
            _cancelAction = cancelAction;
        }
        
        public void OnCancel()
        {
            _cancelAction();
        }

        public bool IsValidFor(ViewMode viewMode)
        {
            return viewMode == ViewMode.Detailed || viewMode == ViewMode.Summary;
        }
    }
}
