﻿using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.UI;
using System;

namespace Bronze.Common.UI
{
    public class DevelopmentPlacementMouseMode : IMouseMode
    {
        public ICellDevelopmentType DevelopmentType { get; }
        public bool PlacementValid { get; set; }
        public Settlement ForSettlement { get; set; }

        private Action _cancelAction;

        public DevelopmentPlacementMouseMode(
            ICellDevelopmentType developmentType, 
            Settlement forSettlement,
            Action cancelAction)
        {
            DevelopmentType = developmentType;
            _cancelAction = cancelAction;
            ForSettlement = forSettlement;
        }

        public void OnCancel()
        {
            _cancelAction();
        }

        public bool IsValidFor(ViewMode viewMode)
        {
            return viewMode == ViewMode.Detailed || viewMode == ViewMode.Summary;
        }
    }
}
