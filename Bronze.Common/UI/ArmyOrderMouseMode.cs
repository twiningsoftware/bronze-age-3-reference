﻿using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using System;

namespace Bronze.Common.UI
{
    public class ArmyOrderMouseMode : IMouseMode
    {
        public bool PlacementValid { get; set; }
        private Action _cancelAction;

        public Army Army { get; }
        public IArmyAction ArmyAction { get; set; }
        public bool TargetValid { get; set; }
        public Cell Target { get; set; }
        public ArmyPathCalculator PathCalculator { get; }

        public ArmyOrderMouseMode(Army army, IArmyAction armyAction, Action cancelAction, IWorldManager worldManager)
        {
            Army = army;
            ArmyAction = armyAction;
            _cancelAction = cancelAction;
            PathCalculator = new ArmyPathCalculator(army, worldManager);
        }
        
        public void OnCancel()
        {
            _cancelAction();
        }
        
        public bool IsValidFor(ViewMode viewMode)
        {
            return viewMode != ViewMode.Settlement;
        }
    }
}
