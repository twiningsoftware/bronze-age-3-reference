﻿using Bronze.Contracts.Data;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using System.Collections.Generic;

namespace Bronze.Common.UI.MapInteraction
{
    public class MovementArrowDrawer : IMapInteractionComponent
    {
        private readonly IPlayerDataTracker _playerData;
        private readonly IGameWorldDrawer _gameWorldDrawer;

        public MovementArrowDrawer(
            IPlayerDataTracker playerData,
            IGameWorldDrawer gameWorldDrawer)
        {
            _playerData = playerData;
            _gameWorldDrawer = gameWorldDrawer;
        }

        public void DrawWorld(IDrawingService drawingService, IEnumerable<Cell> cellsInView, ViewMode viewMode)
        {
            var activeArmy = _playerData.SelectedArmy ?? _playerData.HoveredArmy;

            if(activeArmy != null && activeArmy.CellPathCalculator.PathSuccessful)
            {
                foreach(var cell in activeArmy.CellPathCalculator.Path)
                {
                    if (cell.Position.Plane == _playerData.CurrentPlane)
                    {
                        _gameWorldDrawer.DrawPathdot(
                            drawingService,
                            cell.Position,
                            viewMode);
                    }
                }
            }
        }
        
        public void DrawTiles(IDrawingService drawingService, IEnumerable<Tile> tilesInView)
        {
        }

        public void Update(InputState inputState, float elapsedSeconds)
        {
        }
    }
}
