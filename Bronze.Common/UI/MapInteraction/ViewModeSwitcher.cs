﻿using Bronze.Contracts.Data;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.UI.MapInteraction
{
    public class ViewModeSwitcher : IMapInteractionComponent
    {
        private readonly IPlayerDataTracker _playerData;

        private float _cooldown;

        public ViewModeSwitcher(
            IPlayerDataTracker playerData)
        {
            _playerData = playerData;
        }

        public void Update(InputState inputState, float elapsedSeconds)
        {
            if (inputState.MouseInWindow && !inputState.MouseOverWidget)
            {
                _cooldown = Math.Max(0, _cooldown - elapsedSeconds);

                if (inputState.MouseScrollDelta > 0 && _cooldown <= 0)
                {
                    _cooldown = 0.2f;

                    switch (_playerData.ViewMode)
                    {
                        case ViewMode.Detailed:
                            if (_playerData.HoveredCell?.Region?.Settlement?.Owner == _playerData.PlayerTribe
                                && _playerData.HoveredCell.Region.Settlement.Districts.Any(d => d.Cell == _playerData.HoveredCell))
                            {
                                // switch to the settlement map
                                _playerData.ActiveSettlement = _playerData.HoveredCell.Region.Settlement;
                                _playerData.LookAt(new TilePosition(_playerData.ActiveSettlement.Districts.First().Cell, TilePosition.TILES_PER_CELL / 2, TilePosition.TILES_PER_CELL / 2));
                                _playerData.ViewMode = ViewMode.Settlement;
                            }
                            break;
                        case ViewMode.Summary:
                            _playerData.ViewMode = ViewMode.Detailed;
                            break;
                    }
                }
                else if (inputState.MouseScrollDelta < 0 && _cooldown <= 0)
                {
                    _cooldown = 0.2f;

                    switch (_playerData.ViewMode)
                    {
                        case ViewMode.Settlement:
                            _playerData.LookAt(_playerData.ActiveSettlement.Districts.First().Cell.Position);
                            _playerData.ActiveSettlement = null;
                            _playerData.ViewMode = ViewMode.Detailed;
                            break;
                        case ViewMode.Detailed:
                            _playerData.ViewMode = ViewMode.Summary;
                            break;
                    }
                }
            }
        }

        public void DrawWorld(IDrawingService drawingService, IEnumerable<Cell> cellsInView, ViewMode viewMode)
        {
        }

        public void DrawWorldAsMap(IDrawingService drawingService, IEnumerable<Region> regionsInView)
        {
        }

        public void DrawTiles(IDrawingService drawingService, IEnumerable<Tile> tilesInView)
        {
        }
    }
}
