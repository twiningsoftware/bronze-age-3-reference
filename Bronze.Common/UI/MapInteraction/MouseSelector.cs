﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using Bronze.Common.UI.Modals;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;

namespace Bronze.Common.UI.MapInteraction
{
    public class MouseSelector : IMapInteractionComponent
    {
        private readonly IPlayerDataTracker _playerData;
        private readonly IWorldManager _worldManager;
        private readonly IGameWorldDrawer _gameWorldDrawer;
        private readonly IDialogManager _dialogManager;
        
        private float _rightClickTime;
        
        public MouseSelector(
            IPlayerDataTracker playerData, 
            IWorldManager worldManager, 
            IGameWorldDrawer gameWorldDrawer,
            IDialogManager dialogManager)
        {
            _playerData = playerData;
            _worldManager = worldManager;
            _gameWorldDrawer = gameWorldDrawer;
            _dialogManager = dialogManager;
        }

        public void Update(InputState inputState, float elapsedSeconds)
        {
            if (inputState.MouseInWindow && !inputState.MouseOverWidget)
            {
                if(inputState.MouseRightPressed)
                {
                    _rightClickTime = 0;
                }
                if(inputState.MouseRightDown)
                {
                    _rightClickTime += elapsedSeconds;
                }
                if(inputState.MouseRightReleased && _rightClickTime < 0.2)
                {
                    if (_playerData.MouseMode != null)
                    {
                        _playerData.MouseMode.OnCancel();
                        _playerData.MouseMode = null;
                    }
                    else if(_playerData.SelectedCell != null)
                    {
                        _playerData.SelectedCell = null;
                    }
                }

                if(_playerData.SelectedArmy != null 
                    && _playerData.SelectedArmy.Owner != _playerData.PlayerTribe 
                    && !_playerData.PlayerTribe.IsVisible(_playerData.SelectedArmy.Cell))
                {
                    _playerData.SelectedArmy = null;
                }

                var mouseInWorld = _gameWorldDrawer.TransformFromScreen(inputState.MousePosition.ToVector2());

                // in world map mode
                if (_playerData.ActiveSettlement == null)
                {
                    _playerData.SelectedStructure = null;
                    _playerData.HoveredTile = null;

                    var mouseInCellCoords = mouseInWorld / Constants.CELL_SIZE_PIXELS + new Vector2(0.5f);

                    var hoveredPos = new CellPosition(
                        _playerData.CurrentPlane,
                        (int)mouseInCellCoords.X,
                        (int)mouseInCellCoords.Y);

                    _playerData.HoveredCell = _worldManager.GetCell(hoveredPos);

                    _playerData.HoveredArmy = DetermineHoveredArmy(mouseInWorld, _playerData.HoveredCell);

                    if (inputState.MouseReleased && _playerData.MouseMode == null)
                    {
                        if (_playerData.HoveredArmy != null
                            && _playerData.SelectedArmy != _playerData.HoveredArmy
                            && _playerData.PlayerTribe.IsVisible(_playerData.HoveredCell))
                        {
                            _playerData.SelectedCell = null;
                            _playerData.SelectedArmy = _playerData.HoveredArmy;
                        }
                        else if (_playerData.SelectedCell != null
                            && _playerData.SelectedCell == _playerData.HoveredCell)
                        {
                            _playerData.SelectedArmy = null;
                            _dialogManager.PushModal<CellDetailsModal, Cell>(_playerData.SelectedCell);
                            _playerData.SelectedArmy = null;
                            if (_playerData.SelectedCell == _playerData.HoveredCell)
                            {

                            }
                            else
                            if (_playerData.PlayerTribe.IsExplored(_playerData.HoveredCell))
                            {
                                _playerData.SelectedCell = _playerData.HoveredCell;
                            }
                        }
                        else if (_playerData.PlayerTribe.IsExplored(_playerData.HoveredCell))
                        {

                            _playerData.SelectedArmy = null;
                            _playerData.SelectedCell = _playerData.HoveredCell;
                        }
                        else
                        {
                            _playerData.SelectedCell = null;
                            _playerData.SelectedArmy = null;
                        }
                    }

                    if(_playerData.SelectedCell != null)
                    {
                        _playerData.ActiveRegion = _playerData.SelectedCell.Region;
                    }
                    else
                    {
                        var cellInViewCenter = _worldManager
                                .GetCell(new CellPosition(
                                        _playerData.CurrentPlane,
                                        (int)(_playerData.ViewCenter.X / Constants.CELL_SIZE_PIXELS + 0.5f),
                                        (int)(_playerData.ViewCenter.Y / Constants.CELL_SIZE_PIXELS + 0.5f)));

                        _playerData.ActiveRegion = cellInViewCenter?.Region;
                    }

                }
                else // in settlement map mode
                {
                    _playerData.SelectedCell = null;
                    _playerData.HoveredCell = null;
                    _playerData.ActiveRegion = null;
                    _playerData.SelectedArmy = null;
                    _playerData.HoveredArmy = null;

                    mouseInWorld = mouseInWorld / Constants.TILE_SIZE_PIXELS + new Vector2(0.5f);

                    var tileX = ((int)mouseInWorld.X) % TilePosition.TILES_PER_CELL;
                    var cellX = (int)(mouseInWorld.X / TilePosition.TILES_PER_CELL);
                    var tileY = ((int)mouseInWorld.Y) % TilePosition.TILES_PER_CELL;
                    var cellY = (int)(mouseInWorld.Y / TilePosition.TILES_PER_CELL);

                    // Deselect the structure if it's been removed
                    if(_playerData.SelectedStructure != null && _playerData.SelectedStructure.UlTile.Structure != _playerData.SelectedStructure)
                    {
                        _playerData.SelectedStructure = null;
                    }

                    var district = _playerData.ActiveSettlement.Districts
                        .Where(d => d.Cell.Position.X == cellX && d.Cell.Position.Y == cellY)
                        .FirstOrDefault();

                    if(district != null)
                    {
                        _playerData.HoveredTile = district.Tiles[tileX, tileY];

                        if (inputState.MouseReleased && _playerData.MouseMode == null)
                        {
                            if (_playerData.SelectedStructure == _playerData.HoveredTile.Structure && _playerData.SelectedStructure != null)
                            {
                                _dialogManager.PushModal<TileDetailsModal, Tile>(_playerData.SelectedStructure.UlTile);
                            }
                            else
                            {
                                _playerData.SelectedStructure = _playerData.HoveredTile.Structure;
                            }
                        }
                    }
                    else
                    {
                        _playerData.HoveredTile = null;
                    }
                }
            }
            else
            {
                _playerData.HoveredCell = null;
                _playerData.HoveredTile = null;
            }
        }

        private Army DetermineHoveredArmy(Vector2 hoveredPos, Cell hoveredCell)
        {
            if (hoveredCell != null)
            {
                var armies = hoveredCell.Yield()
                    .Concat(hoveredCell.Neighbors)
                    .SelectMany(c => c.WorldActors.OfType<Army>())
                    .Where(a => a != null)
                    .Select(a => Tuple.Create(a, (a.Position * Constants.CELL_SIZE_PIXELS - hoveredPos).Length()))
                    .Where(t => t.Item2 < Constants.CELL_SIZE_PIXELS / 2)
                    .OrderBy(t => t.Item2)
                    .ToArray();

                return armies.FirstOrDefault()?.Item1;
            }

            return null;
        }

        public void DrawWorld(IDrawingService drawingService, IEnumerable<Cell> cellsInView, ViewMode viewMode)
        {
            if(_playerData.SelectedCell != null && cellsInView.Contains(_playerData.SelectedCell))
            {
                _gameWorldDrawer.DrawSelectionBox(
                    drawingService,
                    _playerData.SelectedCell.Position,
                    viewMode);
            }
        }
        
        public void DrawTiles(IDrawingService drawingService, IEnumerable<Tile> tilesInView)
        {
            if(_playerData.SelectedStructure != null)
            {
                if(_playerData.SelectedStructure.Footprint.Any(t=> tilesInView.Contains(t)))
                {
                    _gameWorldDrawer.DrawSelectionBox(drawingService, _playerData.SelectedStructure);
                }
            }
        }
    }
}
