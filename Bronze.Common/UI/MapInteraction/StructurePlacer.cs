﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI;

namespace Bronze.Common.UI.MapInteraction
{
    public class StructurePlacer : IMapInteractionComponent
    {
        private readonly IPlayerDataTracker _playerData;
        private readonly IGameWorldDrawer _gameWorldDrawer;
        private readonly ITilePather _tilePather;
        private readonly IAudioService _audioService;
        private readonly ISettlementLogiCalculator _settlementLogiCalculator;

        private bool _isDragging;
        private Tile _dragStart;
        private IEnumerable<Tile> _dragTiles;
        private Tile _proposedUlTile;
        private Tile[] _deliveryArea;

        public StructurePlacer(
            IPlayerDataTracker playerData,
            IGameWorldDrawer gameWorldDrawer,
            ITilePather tilePather,
            IAudioService audioService,
            ISettlementLogiCalculator settlementLogiCalculator)
        {
            _playerData = playerData;
            _gameWorldDrawer = gameWorldDrawer;
            _tilePather = tilePather;
            _audioService = audioService;
            _settlementLogiCalculator = settlementLogiCalculator;
        }

        public void Update(InputState inputState, float elapsedSeconds)
        {
            var previousProposedTile = _proposedUlTile;
            _proposedUlTile = null;
            
            if (_playerData.MouseMode is StructurePlacementMouseMode structurePlacementMouseMode)
            {
                structurePlacementMouseMode.PlacementValid = false;

                if (_playerData.ActiveSettlement == null)
                {
                    _playerData.MouseMode = null;
                }

                if (!inputState.MouseInWindow
                    || inputState.MouseOverWidget
                    || _playerData.ActiveSettlement == null)
                {
                    _isDragging = false;
                    _dragTiles = Enumerable.Empty<Tile>();
                    return;
                }
                else
                {
                    if (_playerData.HoveredTile != null)
                    {
                        _proposedUlTile = _playerData.HoveredTile.District.Settlement.GetTile(
                            _playerData.HoveredTile.Position.Relative(
                                -1 * (int)(structurePlacementMouseMode.StructureType.TilesWide / 2f - 0.5f),
                                -1 * (int)(structurePlacementMouseMode.StructureType.TilesWide/ 2f - 0.5f)));
                    }

                    structurePlacementMouseMode.PlacementValid = structurePlacementMouseMode.StructureType.CanBePlaced(_proposedUlTile);

                    if (structurePlacementMouseMode.StructureType.PlacementType == StructurePlacementType.LineDrag)
                    {
                        if (!_isDragging && inputState.MouseDown)
                        {
                            if (structurePlacementMouseMode.StructureType.CanBePlaced(_proposedUlTile))
                            {
                                _isDragging = true;
                                _dragStart = _proposedUlTile;
                            }
                        }
                        else if (_isDragging && !inputState.MouseDown)
                        {
                            _isDragging = false;
                            foreach (var tile in _dragTiles)
                            {
                                if (structurePlacementMouseMode.StructureType.CanBePlaced(tile))
                                {
                                    structurePlacementMouseMode.StructureType.Place(tile);
                                }
                            }

                            _audioService.PlaySound(UiSounds.Dig);
                        }

                        if (_isDragging)
                        {
                            _dragTiles = _tilePather.DeterminePath(
                                _dragStart,
                                _proposedUlTile,
                                _playerData.ActiveSettlement,
                                t => structurePlacementMouseMode.StructureType.CanBePlaced(t),
                                penalizeTurning: true,
                                abortFactor: 5,
                                stepSize: Math.Max(structurePlacementMouseMode.StructureType.TilesWide, structurePlacementMouseMode.StructureType.TilesHigh));
                        }
                    }
                    else if (structurePlacementMouseMode.StructureType.PlacementType == StructurePlacementType.Single)
                    {
                        if (structurePlacementMouseMode.PlacementValid
                            && _proposedUlTile != previousProposedTile)
                        {
                            Task.Run(() => DetermineDeliveryArea(structurePlacementMouseMode.StructureType, _proposedUlTile));
                        }


                        if (inputState.MouseDown)
                        {
                            if (structurePlacementMouseMode.PlacementValid)
                            {
                                structurePlacementMouseMode.StructureType.Place(_proposedUlTile);

                                _audioService.PlaySound(UiSounds.Dig);
                            }
                        }
                    }
                }
            }
            else
            {
                _isDragging = false;
                _dragTiles = Enumerable.Empty<Tile>();
            }
        }

        private void DetermineDeliveryArea(IStructureType structureType, Tile proposedUlTile)
        {
            var result = new List<Tile>();
            var footprint = Util.CalculateFootprint(proposedUlTile, structureType).ToArray();

            foreach (var hauler in structureType.LogiSourceHaulers)
            {
                var area = _settlementLogiCalculator.FloodFillOut(footprint, hauler);

                result.AddRange(area.Keys);
            }

            _deliveryArea = result.Distinct().ToArray();
        }

        public void DrawWorld(IDrawingService drawingService, IEnumerable<Cell> cellsInView, ViewMode viewMode)
        {
        }

        public void DrawWorldAsMap(IDrawingService drawingService, IEnumerable<Region> regionsInView)
        {
        }

        public void DrawTiles(IDrawingService drawingService, IEnumerable<Tile> tilesInView)
        {
            if (_playerData.MouseMode is StructurePlacementMouseMode structurePlacementMouseMode && _proposedUlTile != null)
            {
                if (structurePlacementMouseMode.StructureType.PlacementType == StructurePlacementType.LineDrag)
                {
                    if (_isDragging)
                    {
                        foreach (var tile in _dragTiles)
                        {
                            for(var i = 0; i < structurePlacementMouseMode.StructureType.TilesWide; i++)
                            {
                                for(var j = 0; j < structurePlacementMouseMode.StructureType.TilesHigh; j++)
                                {
                                    var relTile = tile;
                                    if(i != 0 || j != 0)
                                    {
                                        relTile = tile.District.Settlement.GetTile(tile.Position.Relative(i, j));
                                    }

                                    if (relTile != null)
                                    {
                                        _gameWorldDrawer.DrawTile(
                                            drawingService,
                                            relTile,
                                            structurePlacementMouseMode.StructureType.AnimationLayers
                                                .OfType<TileAnimationLayer>()
                                                .Where(l => string.IsNullOrWhiteSpace(l.AnimationName) || l.AnimationName == "idle"));
                                    }
                                }
                            }

                            _gameWorldDrawer.DrawStructure(
                                drawingService,
                                tile,
                                structurePlacementMouseMode.StructureType.AnimationLayers.Where(l => string.IsNullOrWhiteSpace(l.AnimationName) || l.AnimationName == "idle"),
                                structurePlacementMouseMode.StructureType.TilesWide,
                                structurePlacementMouseMode.StructureType.TilesHigh);
                        }
                    }
                    else
                    {
                        if (structurePlacementMouseMode.PlacementValid)
                        {
                            for (var i = 0; i < structurePlacementMouseMode.StructureType.TilesWide; i++)
                            {
                                for (var j = 0; j < structurePlacementMouseMode.StructureType.TilesHigh; j++)
                                {
                                    var relTile = _proposedUlTile;
                                    if (i != 0 || j != 0)
                                    {
                                        relTile = _proposedUlTile.District.Settlement.GetTile(_proposedUlTile.Position.Relative(i, j));
                                    }

                                    if (relTile != null)
                                    {
                                        _gameWorldDrawer.DrawTile(
                                            drawingService,
                                            relTile,
                                            structurePlacementMouseMode.StructureType.AnimationLayers
                                                .OfType<TileAnimationLayer>()
                                                .Where(l => string.IsNullOrWhiteSpace(l.AnimationName) || l.AnimationName == "idle"));
                                    }
                                }
                            }

                            _gameWorldDrawer.DrawStructure(
                                drawingService,
                                _proposedUlTile,
                                structurePlacementMouseMode.StructureType.AnimationLayers.Where(l => string.IsNullOrWhiteSpace(l.AnimationName) || l.AnimationName == "idle"),
                                structurePlacementMouseMode.StructureType.TilesWide,
                                structurePlacementMouseMode.StructureType.TilesHigh);
                        }
                        else
                        {
                            _gameWorldDrawer.DrawSprite(
                                drawingService,
                                _proposedUlTile,
                                new[]
                                {
                                    new SpriteAnimation
                                    {
                                        DrawLayer = 10,
                                        Height = 32,
                                        Width = 32,
                                        ImageKey = UiImages.TileInvalid
                                    }
                                });
                        }
                    }
                }
                else
                {
                    if (structurePlacementMouseMode.PlacementValid)
                    {
                        _gameWorldDrawer.DrawStructure(
                            drawingService,
                            _proposedUlTile,
                            structurePlacementMouseMode.StructureType.AnimationLayers.Where(l => string.IsNullOrWhiteSpace(l.AnimationName) || l.AnimationName == "idle"),
                            structurePlacementMouseMode.StructureType.TilesWide,
                            structurePlacementMouseMode.StructureType.TilesHigh);

                        if(_deliveryArea != null)
                        {
                            foreach (var tile in _deliveryArea)
                            {
                                drawingService.DrawImage(
                                    "ui_tile_mask",
                                    tile.Position.ToVector() * Constants.TILE_SIZE_PIXELS,
                                    5,
                                    true);
                            }
                        }
                    }
                    else
                    {
                        for (var i = 0; i < structurePlacementMouseMode.StructureType.TilesWide; i++)
                        {
                            for (var j = 0; j < structurePlacementMouseMode.StructureType.TilesHigh; j++)
                            {
                                var tile = _playerData.ActiveSettlement.GetTile(_proposedUlTile.Position.Relative(i, j));

                                _gameWorldDrawer.DrawSprite(
                                    drawingService,
                                    tile,
                                    new[]
                                    {
                                        new SpriteAnimation
                                        {
                                            DrawLayer = 10,
                                            Height = 32,
                                            Width = 32,
                                            ImageKey = UiImages.TileInvalid
                                        }
                                    });
                            }
                        }
                    }
                }
            }
        }
    }
}
