﻿using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Trade;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.UI.MapInteraction
{
    public class TradePathDrawer : IMapInteractionComponent
    {
        private readonly IPlayerDataTracker _playerData;
        private readonly IGameWorldDrawer _gameWorldDrawer;
        private readonly IUserSettings _userSettings;
        private readonly ITradeManager _tradeManager;
        private readonly IAudioService _audioService;
        private readonly Random _random;

        private float _accumulatedTime;
        private Dictionary<TradeRoute, List<TraderSprite>> _traderSprites;
        private Dictionary<TradeRoute, double> _spawnTimers;

        public TradePathDrawer(
            IPlayerDataTracker playerData, 
            IGameWorldDrawer gameWorldDrawer,
            IUserSettings userSettings,
            ITradeManager tradeManager,
            IAudioService audioService)
        {
            _playerData = playerData;
            _gameWorldDrawer = gameWorldDrawer;
            _userSettings = userSettings;
            _tradeManager = tradeManager;
            _audioService = audioService;
            _accumulatedTime = 0;
            _random = new Random();

            _traderSprites = new Dictionary<TradeRoute, List<TraderSprite>>();
            _spawnTimers = new Dictionary<TradeRoute, double>();
        }

        public void DrawWorld(IDrawingService drawingService, IEnumerable<Cell> cellsInView, ViewMode viewMode)
        {
            foreach (var sprite in _traderSprites.Values.SelectMany(v => v))
            {
                if (_playerData.PlayerTribe.IsVisible(sprite.FromCell) || _playerData.PlayerTribe.IsVisible(sprite.ToCell))
                {
                    if (sprite.FromCell.Position.Plane == _playerData.CurrentPlane)
                    {
                        sprite.Draw(
                            drawingService,
                            _gameWorldDrawer,
                            _userSettings,
                            _audioService,
                            sprite.TradeRoute.FromSettlement.Owner.PrimaryColor,
                            viewMode);
                    }
                }
            }
        }
        
        public void DrawTiles(IDrawingService drawingService, IEnumerable<Tile> tilesInView)
        {
        }

        public void Update(InputState inputState, float elapsedSeconds)
        {
            if(_userSettings.SimulationPaused)
            {
                elapsedSeconds = 0;
            }
            elapsedSeconds *= _userSettings.SimulationSpeed;

            _accumulatedTime += elapsedSeconds;

            var allRoutes = _tradeManager.AllTradeRoutes;
            
            // Remove old routes
            foreach (var route in _traderSprites.Keys.ToArray())
            {
                if (!allRoutes.Contains(route))
                {
                    _traderSprites.Remove(route);
                    _spawnTimers.Remove(route);
                }
            }
            
            // Update sprites on all routes
            foreach (var route in allRoutes)
            {
                if(!_traderSprites.ContainsKey(route))
                {
                    _traderSprites.Add(route, new List<TraderSprite>());
                    _spawnTimers.Add(route, _random.NextDouble());
                }

                foreach (var sprite in _traderSprites[route].ToArray())
                {
                    sprite.Update(elapsedSeconds);

                    if (sprite.Finished)
                    {
                        _traderSprites[route].Remove(sprite);
                    }
                }
            }
            
            foreach(var route in _tradeManager.AllTradeRoutes)
            {
                _spawnTimers[route] -= elapsedSeconds;

                if (_spawnTimers[route] <= 0 
                    && route.Exports.Any()
                    && route.FromActor.WorkerPercent > 0
                    && route.ToActor.WorkerPercent > 0)
                {
                    _spawnTimers[route] = 1.5 / Constants.MONTHS_PER_SECOND + _random.NextDouble();
                    
                    _traderSprites[route].Add(new TraderSprite(route));
                }
            }
        }

        
        private class TraderSprite
        {
            private const float SPEED = 0.2f;
            public Cell FromCell => TradeRoute.PathCalculator.Path.Count > _currentIndex  ? TradeRoute.PathCalculator.Path[_currentIndex] : null;
            public Cell ToCell => TradeRoute.PathCalculator.Path.Count > _currentIndex + 1 ? TradeRoute.PathCalculator.Path[_currentIndex + 1] : FromCell;
            public float Progress { get; private set; }
            public IndividualAnimation Animation { get; private set; }

            public TradeRoute TradeRoute { get; }
            public bool Finished => _currentIndex >= TradeRoute.PathCalculator.Path.Count - 2 && Progress > 0.5;

            private int _currentIndex;

            private float _time;

            public TraderSprite(TradeRoute tradeRoute)
            {
                TradeRoute = tradeRoute;
                _currentIndex = 0;
                _time = 0;
                Progress = 0.5f;
                PickAnimation();
            }

            public void Update(float elapsedSeconds)
            {
                Progress += elapsedSeconds * SPEED;
                _time += elapsedSeconds;
                
                if(Progress > 1)
                {
                    Progress -= 1;

                    _currentIndex = Math.Min(_currentIndex + 1, TradeRoute.PathCalculator.Path.Count - 1);

                    PickAnimation();
                }
            }

            public void Draw(
                IDrawingService drawingService, 
                IGameWorldDrawer gameWorldDrawer, 
                IUserSettings userSettings,
                IAudioService audioService,
                BronzeColor settlementColor,
                ViewMode viewMode)
            {
                var from = FromCell;
                var to = ToCell;

                if(Animation != null && from != null && to != null)
                {
                    var fromVec = from.Position.ToVector();
                    var toVec = to.Position.ToVector();

                    var worldPos = (fromVec * (1 - Progress)) + (toVec * Progress);

                    var facing = Util.GetFacing(fromVec, toVec);

                    var pos = worldPos * Constants.CELL_SIZE_PIXELS;

                    if(viewMode == ViewMode.Summary)
                    {
                        pos = worldPos * Constants.CELL_SIZE_SUMMARY_PIXELS;
                    }
                    
                    Animation.Draw(
                        drawingService, 
                        pos, 
                        _time, 
                        settlementColor, 
                        facing,
                        0);

                    if (!userSettings.SimulationPaused)
                    {
                        foreach (var effect in Animation.SoundEffects.Where(e => e.Mode == SoundEffectMode.Ambient))
                        {
                            audioService.AddAmbience(effect.SoundKey);
                        }
                    }
                }
            }

            private void PickAnimation()
            {
                var animationNames = TradeRoute.MovementType.AnimationNames;

                Animation = TradeRoute.TradeHauler.Animations
                    .Where(a => animationNames.Contains(a.Name))
                    .FirstOrDefault();
            }
        }
    }
}
