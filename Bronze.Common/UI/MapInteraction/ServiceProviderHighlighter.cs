﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bronze.Common.Data.World.CellDevelopments;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI;

namespace Bronze.Common.UI.MapInteraction
{
    public class ServiceProviderHighlighter : IMapInteractionComponent
    {
        private const float DRAW_LAYER = 0.3f;

        private readonly IPlayerDataTracker _playerData;
        private readonly IGameWorldDrawer _gameWorldDrawer;

        private IStructureType _structureType;
        private Tile _activeTile;
        private Task<Tuple<IEnumerable<Tile>, IEnumerable<IStructure>>> _calculationTask;
        private IEnumerable<Tile> _serviceArea;
        private IEnumerable<IStructure> _structuresToHighlight;
        private SpriteAnimation[] _maskAnimationLayers;

        public ServiceProviderHighlighter(
            IPlayerDataTracker playerData,
            IGameWorldDrawer gameWorldDrawer)
        {
            _playerData = playerData;
            _gameWorldDrawer = gameWorldDrawer;

            _serviceArea = Enumerable.Empty<Tile>();
            _structuresToHighlight = Enumerable.Empty<IStructure>();

            _maskAnimationLayers = new[]
            {
                new SpriteAnimation
                {
                    DrawLayer = DRAW_LAYER,
                    Height = 32,
                    Width = 32,
                    ImageKey = UiImages.TileMask
                }
            };
        }

        public void Update(InputState inputState, float elapsedSeconds)
        {
            if (_playerData.MouseMode is StructurePlacementMouseMode structurePlacement)
            {
                if (structurePlacement.StructureType is IServiceProviderStructureType providerStructure)
                {
                    if (_structureType != structurePlacement.StructureType
                        || _activeTile != _playerData.HoveredTile)
                    {
                        StartCalculationsForProvider(providerStructure);
                    }
                }
                else
                {
                    //if (_structureType != structurePlacement.StructureType
                    //    || _activeTile != _playerData.HoveredTile)
                    //{
                    //    StartCalculationsForConsumer(structurePlacement.StructureType);
                    //}
                }
            }

            if (_calculationTask != null && _calculationTask.IsCompleted)
            {
                _serviceArea = _calculationTask.Result.Item1;
                _structuresToHighlight = _calculationTask.Result.Item2;
            }
        }

        private void StartCalculationsForProvider(IServiceProviderStructureType structureType)
        {
            _calculationTask = null;

            _structureType = structureType as IStructureType;
            _activeTile = _playerData.HoveredTile;

            if (_structureType == null || _activeTile == null)
            {
                _serviceArea = Enumerable.Empty<Tile>();
                _structuresToHighlight = Enumerable.Empty<IStructure>();
                return;
            }

            var hoveredTile = _playerData.HoveredTile;
            var districts = _playerData.HoveredTile.District.Settlement.Districts.ToArray();

            var ulTile = _playerData.HoveredTile.District.Settlement.GetTile(
                            _playerData.HoveredTile.Position.Relative(
                                -1 * (int)(_structureType.TilesWide / 2f - 0.5f),
                                -1 * (int)(_structureType.TilesWide / 2f - 0.5f)));

            _calculationTask = Task.Run(() =>
            {
                var serviceArea = structureType.CalculateServiceArea(ulTile);

                var structuresToHilight = serviceArea
                    .SelectMany(t => t.OrthoNeighbors)
                    .Select(t => t.Structure)
                    .Where(s => s != null)
                    .Where(s => structureType.CanService(s))
                    .Distinct()
                    .ToArray();

                return Tuple.Create<IEnumerable<Tile>, IEnumerable<IStructure>>(serviceArea, structuresToHilight);
            });
        }

        private void StartCalculationsForConsumer(IStructureType structureType)
        {
            _calculationTask = null;

            _structureType = structureType;
            _activeTile = _playerData.HoveredTile;

            if (_structureType == null || _activeTile == null)
            {
                _serviceArea = Enumerable.Empty<Tile>();
                _structuresToHighlight = Enumerable.Empty<IStructure>();
                return;
            }

            var hoveredTile = _playerData.HoveredTile;
            var districts = _playerData.HoveredTile.District.Settlement.Districts.ToArray();

            var ulTile = _playerData.HoveredTile.District.Settlement.GetTile(
                            _playerData.HoveredTile.Position.Relative(
                                -1 * (int)(_structureType.TilesWide / 2f - 0.5f),
                                -1 * (int)(_structureType.TilesWide / 2f - 0.5f)));

            _calculationTask = Task.Run(() =>
            {
                var footprint = Util.CalculateFootprint(ulTile, _structureType);

                var providersInRange = new List<IServiceProviderStructure>();
                var serviceArea = new List<Tile>();

                foreach (var district in districts)
                {
                    foreach (var structure in district.Structures)
                    {
                        if (structure is IServiceProviderStructure serviceProvider
                            && serviceProvider.CanService(structureType))
                        {
                            var tilesServed = serviceProvider.CalculateServiceArea();

                            if (tilesServed.Any(t => footprint.Intersect(t.Neighbors).Any()))
                            {
                                providersInRange.Add(serviceProvider);
                                serviceArea.AddRange(tilesServed.Except(serviceArea));
                            }
                        }
                    }
                }

                return Tuple.Create<IEnumerable<Tile>, IEnumerable<IStructure>>(
                    serviceArea,
                    providersInRange);
            });
        }

        public void DrawWorld(
            IDrawingService drawingService,
            IEnumerable<Cell> cellsInView,
            ViewMode viewMode)
        {
            if (viewMode != ViewMode.Detailed && viewMode != ViewMode.Summary)
            {
                return;
            }

            // For cell developments, take advantage of service providers only hitting adjacents (ergo calculations instant) to
            // skip the calculation complexity.

            var neighboringDevelopments = (_playerData.HoveredCell?.Neighbors ?? Enumerable.Empty<Cell>())
                .Select(c => c.Development)
                .Where(d => d != null)
                .ToArray();

            if (_playerData.MouseMode is DevelopmentPlacementMouseMode developmentPlacement && developmentPlacement.PlacementValid)
            {
                if (developmentPlacement.DevelopmentType is IServiceProviderDevelopmentType serviceProvider)
                {
                    foreach (var cell in serviceProvider.CalculateServiceArea(_playerData.HoveredCell))
                    {
                        _gameWorldDrawer.DrawCellIcon(
                            drawingService,
                            cell.Position,
                            viewMode,
                            serviceProvider.ServiceType.AreaIconKey,
                            serviceProvider.ServiceType.AreaIconKey);
                    }
                }
                else
                {
                    foreach (var provider in neighboringDevelopments.OfType<IServiceProviderDevelopment>())
                    {
                        foreach (var cell in provider.CellsServed)
                        {
                            _gameWorldDrawer.DrawCellIcon(
                                drawingService,
                                cell.Position,
                                viewMode,
                                provider.ServiceType.AreaIconKey,
                                provider.ServiceType.AreaIconKey);
                        }
                    }
                }
            }
            else if (_playerData.SelectedCell?.Development != null
                && _playerData.SelectedCell.Development is IServiceProviderDevelopment provider)
            {
                foreach (var cell in provider.CellsServed)
                {
                    _gameWorldDrawer.DrawCellIcon(
                        drawingService,
                        cell.Position,
                        viewMode,
                        provider.ServiceType.AreaIconKey,
                        provider.ServiceType.AreaIconKey);
                }

                foreach (var development in provider.ActorsServed.OfType<ICellDevelopment>())
                {
                    _gameWorldDrawer.DrawSelectionBox(
                        drawingService,
                        development.Cell.Position,
                        viewMode);
                }
            }
            else if (_playerData.SelectedCell?.Development != null)
            {
                foreach (var serviceProvider in _playerData.SelectedCell.Development.ServiceProviders.OfType<IServiceProviderDevelopment>())
                {
                    _maskAnimationLayers[0].ImageKey = serviceProvider.ServiceType.AreaIconKey;
                    _maskAnimationLayers[0].DrawLayer = 10 + serviceProvider.ServiceType.LookupIndex * 0.01f;

                    _gameWorldDrawer.DrawCellIcon(
                        drawingService,
                        serviceProvider.Cell.Position,
                        viewMode,
                        serviceProvider.ServiceType.AreaIconKey,
                        serviceProvider.ServiceType.AreaIconKey);

                    _gameWorldDrawer.DrawSelectionBox(
                        drawingService,
                        serviceProvider.Cell.Position,
                        viewMode);
                }
            }
        }
        
        public void DrawTiles(IDrawingService drawingService, IEnumerable<Tile> tilesInView)
        {
            if (_playerData.MouseMode is StructurePlacementMouseMode structurePlacement 
                && structurePlacement.PlacementValid
                && structurePlacement.StructureType is IServiceProviderStructureType serviceProviderType)
            {
                _maskAnimationLayers[0].ImageKey = serviceProviderType.ServiceType.AreaIconKey;
                _maskAnimationLayers[0].DrawLayer = 0.3f + serviceProviderType.ServiceType.LookupIndex * 0.01f;

                foreach (var tile in _serviceArea)
                {
                    _gameWorldDrawer.DrawSprite(
                        drawingService,
                        tile,
                        _maskAnimationLayers);
                }
                
                foreach (var structure in _structuresToHighlight)
                {
                    _gameWorldDrawer.DrawSelectionBox(drawingService, structure);
                }
            }
            else if (_playerData.SelectedStructure is IServiceProviderStructure provider)
            {
                _maskAnimationLayers[0].ImageKey = provider.ServiceType.AreaIconKey;
                _maskAnimationLayers[0].DrawLayer = 10 + provider.ServiceType.LookupIndex * 0.01f;

                foreach (var tile in provider.TilesServed)
                {
                    _gameWorldDrawer.DrawSprite(
                        drawingService,
                        tile,
                        _maskAnimationLayers);
                }

                foreach (var structure in provider.ActorsServed.OfType<IStructure>())
                {
                    _gameWorldDrawer.DrawSelectionBox(drawingService, structure);
                }
            }
            else if(_playerData.SelectedStructure != null)
            {
                foreach(var serviceProvider in _playerData.SelectedStructure.ServiceProviders.OfType<IServiceProviderStructure>())
                {
                    _maskAnimationLayers[0].ImageKey = serviceProvider.ServiceType.AreaIconKey;
                    _maskAnimationLayers[0].DrawLayer = 10 + serviceProvider.ServiceType.LookupIndex * 0.01f;
                    
                    _gameWorldDrawer.DrawSelectionBox(drawingService, serviceProvider);
                }
            }
        }
    }
}
