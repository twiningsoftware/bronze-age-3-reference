﻿using System.Collections.Generic;
using System.Linq;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI;

namespace Bronze.Common.UI.MapInteraction
{
    public class DevelopmentPlacer : IMapInteractionComponent
    {
        private readonly IPlayerDataTracker _playerData;
        private readonly IGameWorldDrawer _gameWorldDrawer;
        private readonly IAudioService _audioService;

        public DevelopmentPlacer(
            IPlayerDataTracker playerData,
            IGameWorldDrawer gameWorldDrawer,
            IAudioService audioService)
        {
            _playerData = playerData;
            _gameWorldDrawer = gameWorldDrawer;
            _audioService = audioService;
        }

        public void Update(InputState inputState, float elapsedSeconds)
        {
            if (_playerData.MouseMode is DevelopmentPlacementMouseMode developmentPlacementMouseMode)
            {
                developmentPlacementMouseMode.PlacementValid = false;

                if (_playerData.ActiveSettlement != null
                    || _playerData.ActiveRegion?.Settlement == null
                    || _playerData.ActiveRegion?.Settlement?.Owner != _playerData.PlayerTribe)
                {
                    _playerData.MouseMode = null;
                }

                if (!inputState.MouseInWindow
                    || inputState.MouseOverWidget
                    || _playerData.ActiveSettlement != null)
                {
                    return;
                }
                else
                {
                    developmentPlacementMouseMode.PlacementValid = developmentPlacementMouseMode.DevelopmentType.CanBePlaced(_playerData.PlayerTribe, _playerData.HoveredCell)
                        && _playerData.HoveredCell.Region.Settlement == developmentPlacementMouseMode.ForSettlement;

                    if (inputState.MouseDown)
                    {
                        if (developmentPlacementMouseMode.PlacementValid)
                        {
                            developmentPlacementMouseMode.DevelopmentType.Place(_playerData.PlayerTribe, _playerData.HoveredCell, instantBuild: false);

                            _audioService.PlaySound(UiSounds.Dig);
                        }
                    }
                }
            }
        }

        public void DrawWorld(
            IDrawingService drawingService, 
            IEnumerable<Cell> cellsInView,
            ViewMode viewMode)
        {
            if (_playerData.MouseMode is DevelopmentPlacementMouseMode developmentPlacementMouseMode && _playerData.HoveredCell != null)
            {
                if (developmentPlacementMouseMode.PlacementValid)
                {
                    if(viewMode == ViewMode.Detailed)
                    {
                        _gameWorldDrawer.DrawCell(
                            drawingService,
                            _playerData.HoveredCell,
                            developmentPlacementMouseMode.DevelopmentType.DetailLayers.Where(l => string.IsNullOrWhiteSpace(l.AnimationName) || l.AnimationName == "idle"),
                            viewMode);
                    }
                    else if (viewMode == ViewMode.Summary)
                    {
                        _gameWorldDrawer.DrawCell(
                            drawingService,
                            _playerData.HoveredCell,
                            developmentPlacementMouseMode.DevelopmentType.SummaryLayers.Where(l => string.IsNullOrWhiteSpace(l.AnimationName) || l.AnimationName == "idle"),
                            viewMode);
                    }
                }
                else
                {
                    _gameWorldDrawer.DrawRedX(
                        drawingService,
                        _playerData.HoveredCell.Position,
                        viewMode);
                }
            }
        }

        public void DrawTiles(IDrawingService drawingService, IEnumerable<Tile> tilesInView)
        {
        }

        public void DrawWorldAsMap(IDrawingService drawingService, IEnumerable<Region> regionsInView)
        {
        }
    }
}
