﻿using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using System.Collections.Generic;
using System.Numerics;

namespace Bronze.Common.UI.MapInteraction
{
    public class MapScrolling : IMapInteractionComponent
    {
        private readonly IPlayerDataTracker _playerData;
        private bool _mousePanning;
        
        public MapScrolling(IPlayerDataTracker playerData)
        {
            _playerData = playerData;
        }

        public void Update(InputState inputState, float elapsedSeconds)
        {
            var moveRateScale = 1 / Util.ViewModeToScale(_playerData.ViewMode);
            
            var moveRate = 600 * elapsedSeconds / 1 * moveRateScale;
            var viewDx = 0f;
            var viewDy = 0f;
            
            if (inputState.FocusedElement == null)
            {
                if (inputState.KeyDown(KeyCode.Up))
                {
                    viewDy -= moveRate;
                }
                if (inputState.KeyDown(KeyCode.Down))
                {
                    viewDy += moveRate;
                }
                if (inputState.KeyDown(KeyCode.Right))
                {
                    viewDx += moveRate;
                }
                if (inputState.KeyDown(KeyCode.Left))
                {
                    viewDx -= moveRate;
                }
            }

            if (!inputState.MouseOverWidget && inputState.MouseInWindow)
            {
                if (_mousePanning && inputState.MouseInWindow)
                {
                    if (inputState.MouseRightDown)
                    {
                        viewDx += inputState.MouseDelta.X / 1 * moveRateScale;
                        viewDy += inputState.MouseDelta.Y / 1 * moveRateScale;
                    }
                }
                else
                {
                    _mousePanning = inputState.MouseRightDown;
                }
            }
            else
            {
                _mousePanning = false;
            }

            if (viewDx != 0 || viewDy != 0)
            {
                _playerData.ViewCenter += new Vector2(viewDx, viewDy);
            }
        }

        public void DrawWorld(IDrawingService drawingService, IEnumerable<Cell> cellsInView, ViewMode viewMode)
        {
        }

        public void DrawWorldAsMap(IDrawingService drawingService, IEnumerable<Region> regionsInView)
        {
        }

        public void DrawTiles(IDrawingService drawingService, IEnumerable<Tile> tilesInView)
        {
        }
    }
}
