﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bronze.Common.Data.World.Structures;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Trade;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI;

namespace Bronze.Common.UI.MapInteraction
{
    public class TradeRouteBorderPathDrawer : IMapInteractionComponent
    {
        private readonly IPlayerDataTracker _playerData;
        private readonly IGameWorldDrawer _gameWorldDrawer;
        private readonly IUserSettings _userSettings;
        private readonly IAudioService _audioService;
        private readonly SpriteAnimation[] _pathdotAnimationLayers;
        private readonly Random _random;

        private Settlement _currentSettlement;
        private float _accumulatedTime;
        private Dictionary<TradeRoute, List<HaulerSprite>> _haulerSpritesByTradeRoute;
        private Dictionary<TradeRoute, double> _spawnTimers;

        public TradeRouteBorderPathDrawer(
            IPlayerDataTracker playerData,
            IGameWorldDrawer gameWorldDrawer,
            IUserSettings userSettings,
            IAudioService audioService)
        {
            _playerData = playerData;
            _gameWorldDrawer = gameWorldDrawer;
            _userSettings = userSettings;
            _audioService = audioService;
            _random = new Random();
            _accumulatedTime = 0;

            _haulerSpritesByTradeRoute = new Dictionary<TradeRoute, List<HaulerSprite>>();
            _spawnTimers = new Dictionary<TradeRoute, double>();
            
            _pathdotAnimationLayers = new[]
            {
                new SpriteAnimation
                {
                    DrawLayer = 0.3f,
                    Height = 16,
                    Width = 16,
                    ImageKey = UiImages.PathdotSmall
                }
            };
        }

        public void Update(InputState inputState, float elapsedSeconds)
        {
            if (_userSettings.SimulationPaused)
            {
                elapsedSeconds = 0;
            }
            elapsedSeconds *= _userSettings.SimulationSpeed;

            _accumulatedTime += elapsedSeconds;
            
            if (_playerData.ActiveSettlement != null)
            {
                var tradePaths = _playerData.ActiveSettlement.EconomicActors
                    .OfType<TraderStructure>()
                    .Where(ts => ts.TradeRoute != null && ts.ActiveToBorderPath != null)
                    .Select(ts => Tuple.Create(ts.TradeRoute, ts.ActiveToBorderPath, false))
                    .Concat(_playerData.ActiveSettlement.EconomicActors
                        .OfType<TradeRecieverStructure>()
                        .SelectMany(ts => ts.ActiveToBorderPaths)
                        .Select(t => Tuple.Create(t.Item1, t.Item2, true)))
                    .ToArray();

                if (_playerData.ActiveSettlement != _currentSettlement)
                {
                    _currentSettlement = _playerData.ActiveSettlement;
                    ClearState();
                    if (_currentSettlement != null)
                    {
                        AddNewPaths(tradePaths, prewarm: true);
                    }
                }

                if (_currentSettlement != null)
                {
                    AddNewPaths(tradePaths, prewarm: false);
                    UpdatePaths(tradePaths, elapsedSeconds);
                }
            }
            else
            {
                ClearState();
            }
        }

        private void UpdatePaths(Tuple<TradeRoute, TraderToDistrictBorderPath, bool>[] tradeRoutes, float elapsedSeconds)
        {
            // Remove old paths
            foreach (var path in _haulerSpritesByTradeRoute.Keys.ToArray())
            {
                // Let haulers on old paths finish first, then remove them
                if (!tradeRoutes.Any(t => path == t.Item1) && _haulerSpritesByTradeRoute[path].Count == 0)
                {
                    _haulerSpritesByTradeRoute.Remove(path);
                    _spawnTimers.Remove(path);
                }
            }

            // Update sprites on all paths
            foreach (var path in _haulerSpritesByTradeRoute.Keys)
            {
                foreach (var sprite in _haulerSpritesByTradeRoute[path].ToArray())
                {
                    sprite.Update(elapsedSeconds);

                    if (sprite.Finished)
                    {
                        _haulerSpritesByTradeRoute[path].Remove(sprite);
                    }
                }
            }

            // Only run the add logic for active paths
            foreach (var route in tradeRoutes)
            {
                _spawnTimers[route.Item1] -= elapsedSeconds;

                if (_spawnTimers[route.Item1] <= 0
                    && route.Item1.Exports.Any()
                    && route.Item1.FromActor.WorkerPercent > 0
                    && route.Item1.ToActor.WorkerPercent > 0)
                {
                    _spawnTimers[route.Item1] = (_random.NextDouble() + 0.5) / Constants.MONTHS_PER_SECOND;

                    _haulerSpritesByTradeRoute[route.Item1].Add(new HaulerSprite(route.Item1, route.Item2, route.Item3));
                }
            }
        }

        private void AddNewPaths(Tuple<TradeRoute, TraderToDistrictBorderPath, bool>[] tradeRoutes, bool prewarm)
        {
            var oldRoutes = _haulerSpritesByTradeRoute.Keys
                .Where(ts => !tradeRoutes.Any(t => t.Item1 == ts))
                .ToList();

            foreach (var route in tradeRoutes)
            {
                if (!_haulerSpritesByTradeRoute.ContainsKey(route.Item1))
                {
                    _haulerSpritesByTradeRoute.Add(route.Item1, new List<HaulerSprite>());
                    _spawnTimers.Add(route.Item1, _random.NextDouble());
                    
                    if (prewarm 
                        && route.Item2.Path.Any()
                        && route.Item1.Exports.Any()
                        && route.Item1.FromActor.WorkerPercent > 0
                        && route.Item1.ToActor.WorkerPercent > 0)
                    {
                        var startIndex = route.Item2.Path.Count() / 2;

                        var sprite = new HaulerSprite(route.Item1, route.Item2, route.Item3, startIndex);
                        _haulerSpritesByTradeRoute[route.Item1].Add(sprite);
                    }
                }
            }
        }

        private void ClearState()
        {
            _haulerSpritesByTradeRoute.Clear();
            _spawnTimers.Clear();
        }

        public void DrawWorld(IDrawingService drawingService, IEnumerable<Cell> cellsInView, ViewMode viewMode)
        {
        }

        public void DrawWorldAsMap(IDrawingService drawingService, IEnumerable<Region> regionsInView)
        {
        }

        public void DrawTiles(IDrawingService drawingService, IEnumerable<Tile> tilesInView)
        {
            var activeStructure = _playerData.SelectedStructure ?? _playerData.HoveredTile?.Structure;

            if(activeStructure is TraderStructure traderStructure)
            {
                foreach(var path in traderStructure.ToBorderPaths)
                {
                    foreach(var tile in path.Path)
                    {
                        _gameWorldDrawer.DrawSprite(
                            drawingService,
                            tile,
                            _pathdotAnimationLayers);
                    }
                }
            }

            if (activeStructure is TradeRecieverStructure tradeReciever)
            {
                foreach (var path in tradeReciever.ToBorderPaths.SelectMany(tbp => tbp.Value))
                {
                    foreach (var tile in path.Path)
                    {
                        _gameWorldDrawer.DrawSprite(
                            drawingService,
                            tile,
                            _pathdotAnimationLayers);
                    }
                }
            }

            if (_currentSettlement != null)
            {
                foreach (var sprite in _haulerSpritesByTradeRoute.Values.SelectMany(v => v))
                {
                    sprite.Draw(drawingService, _gameWorldDrawer, _currentSettlement.Owner.PrimaryColor);

                    // Don't add sound effects if the game is paused
                    if (!_userSettings.SimulationPaused)
                    {
                        foreach (var effect in sprite.Animation.SoundEffects.Where(e => e.Mode == SoundEffectMode.Ambient))
                        {
                            _audioService.AddAmbience(effect.SoundKey);
                        }
                    }
                }
            }
        }

        private class HaulerSprite
        {
            private const float SPEED = 1;
            public Tile FromTile => Path[_currentIndex];
            public Tile ToTile => Path[Math.Min(_currentIndex + 1, Path.Length - 1)];
            public float Progress { get; private set; }
            public IndividualAnimation Animation { get; private set; }

            public Tile[] Path { get; }
            public bool Finished => _currentIndex >= Path.Length - 2 && Progress > 0.5;
            private TradeRoute _tradeRoute;

            private int _currentIndex;
            private float _time;

            public HaulerSprite(TradeRoute tradeRoute, TraderToDistrictBorderPath toBorderPath, bool incoming, int startIndex = 0)
            {
                _tradeRoute = tradeRoute;

                Path = toBorderPath.Path.ToArray();
                // If the trader is reciving the trade route, flip the path
                if (incoming)
                {
                    Path = Path.Reverse().ToArray();
                }

                _currentIndex = startIndex;
                _time = 0;
                Progress = 0.5f;
                PickAnimation();
            }
            
            public void Update(float elapsedSeconds)
            {
                Progress += elapsedSeconds * SPEED;
                _time += elapsedSeconds;

                if (Progress > 1)
                {
                    Progress -= 1;

                    _currentIndex = Math.Min(_currentIndex + 1, Path.Length - 1);

                    PickAnimation();
                }
            }

            public void Draw(
                IDrawingService drawingService,
                IGameWorldDrawer gameWorldDrawer,
                BronzeColor settlementColor)
            {
                if (Animation != null)
                {
                    var fromVec = FromTile.Position.ToVector();
                    var toVec = ToTile.Position.ToVector();

                    var worldPos = (fromVec * (1 - Progress)) + (toVec * Progress);

                    var facing = Util.GetFacing(FromTile.Position.ToVector(), ToTile.Position.ToVector());

                    Animation.Draw(
                        drawingService,
                        worldPos * Constants.TILE_SIZE_PIXELS,
                        _time,
                        settlementColor,
                        facing,
                        0);
                }
            }

            private void PickAnimation()
            {
                var animationNames = _tradeRoute.MovementType.AnimationNames;

                Animation = _tradeRoute.TradeHauler.Animations
                    .Where(a => animationNames.Contains(a.Name))
                    .FirstOrDefault();
            }
        }
    }
}
