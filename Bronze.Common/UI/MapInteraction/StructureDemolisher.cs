﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI;

namespace Bronze.Common.UI.MapInteraction
{
    public class StructureDemolisher : IMapInteractionComponent
    {
        private readonly IPlayerDataTracker _playerData;
        private readonly IGameWorldDrawer _gameWorldDrawer;
        private readonly IAudioService _audioService;

        private bool _isDragging;
        private Tile _dragStart;
        private List<Tile> _dragTiles;

        public StructureDemolisher(
            IPlayerDataTracker playerData,
            IGameWorldDrawer gameWorldDrawer,
            IAudioService audioService)
        {
            _playerData = playerData;
            _gameWorldDrawer = gameWorldDrawer;
            _audioService = audioService;

            _dragTiles = new List<Tile>();
        }

        public void Update(InputState inputState, float elapsedSeconds)
        {
            if (_playerData.MouseMode is DemolishingStructureMouseMode demolishingStructureMouseMode)
            {
                if (_playerData.ActiveSettlement == null)
                {
                    _playerData.MouseMode = null;
                }

                if (!inputState.MouseInWindow
                    || inputState.MouseOverWidget
                    || _playerData.ActiveSettlement == null)
                {
                    _isDragging = false;
                    _dragTiles.Clear();
                }
                else
                {
                    if (!_isDragging && inputState.MouseDown)
                    {
                        _isDragging = true;
                        _dragStart = _playerData.HoveredTile;
                    }
                    else if (_isDragging && !inputState.MouseDown)
                    {
                        _isDragging = false;
                        var structuresToRemove = _dragTiles
                            .Select(t => t.Structure)
                            .Where(x => x != null)
                            .Distinct()
                            .ToArray();

                        _dragTiles.Clear();

                        foreach (var structure in structuresToRemove)
                        {
                            structure.UlTile.District.RemoveStructure(structure);
                        }

                        _audioService.PlaySound(UiSounds.Demolish);
                    }

                    if (_isDragging)
                    {
                        var minX = Math.Min(_dragStart.Position.X, _playerData.HoveredTile.Position.X);
                        var maxX = Math.Max(_dragStart.Position.X, _playerData.HoveredTile.Position.X);
                        var minY = Math.Min(_dragStart.Position.Y, _playerData.HoveredTile.Position.Y);
                        var maxY = Math.Max(_dragStart.Position.Y, _playerData.HoveredTile.Position.Y);
                        _dragTiles.Clear();

                        for (var x = minX; x <= maxX; x++)
                        {
                            for (var y = minY; y <= maxY; y++)
                            {
                                var pos = _dragStart.Position.Relative(x - _dragStart.Position.X, y - _dragStart.Position.Y);

                                var tile = _playerData.ActiveSettlement.GetTile(pos);

                                if (tile != null)
                                {
                                    _dragTiles.Add(tile);
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                _isDragging = false;
                _dragTiles.Clear();
            }
        }

        public void DrawWorld(IDrawingService drawingService, IEnumerable<Cell> cellsInView, ViewMode viewMode)
        {
        }

        public void DrawWorldAsMap(IDrawingService drawingService, IEnumerable<Region> regionsInView)
        {
        }

        public void DrawTiles(IDrawingService drawingService, IEnumerable<Tile> tilesInView)
        {
            if (_playerData.MouseMode is DemolishingStructureMouseMode demolishingStructureMouseMode)
            {
                var tilesToDemolish = _dragTiles
                    .Select(x => x.Structure)
                    .Where(x => x != null)
                    .Distinct()
                    .SelectMany(x => x.Footprint)
                    .ToArray();

                foreach(var tile in _dragTiles)
                {
                    _gameWorldDrawer.DrawSprite(
                        drawingService,
                        tile,
                        new[]
                        {
                            new SpriteAnimation
                            {
                                DrawLayer = 9,
                                Height = 32,
                                Width = 32,
                                ImageKey = UiImages.TileMask
                            }
                        });
                }

                foreach (var tile in tilesToDemolish)
                {
                    _gameWorldDrawer.DrawSprite(
                        drawingService,
                        tile,
                        new[]
                        {
                            new SpriteAnimation
                            {
                                DrawLayer = 10,
                                Height = 32,
                                Width = 32,
                                ImageKey = UiImages.TileInvalid
                            }
                        });
                }
            }
        }
    }
}
