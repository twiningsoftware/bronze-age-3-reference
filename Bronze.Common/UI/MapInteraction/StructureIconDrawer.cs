﻿using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using Bronze.Common.Data.World.Structures;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI;

namespace Bronze.Common.UI.MapInteraction
{
    public class StructureIconDrawer : IMapInteractionComponent
    {
        private readonly IPlayerDataTracker _playerData;

        public StructureIconDrawer(IPlayerDataTracker playerData)
        {
            _playerData = playerData;
        }
        
        public void DrawWorld(IDrawingService drawingService, IEnumerable<Cell> cellsInView, ViewMode viewMode)
        {
            if(viewMode != ViewMode.Detailed)
            {
                return;
            }

            var iconSpacing = new Vector2(-24, 0);

            foreach(var cell in cellsInView)
            {
                if (cell.Region.Owner == _playerData.PlayerTribe 
                    && cell.Development != null)
                {
                    // Draw in the upper-left corner of the cell
                    var drawPos = cell.Position.ToVector() * Constants.CELL_SIZE_PIXELS
                        + new Vector2(Constants.CELL_SIZE_PIXELS / 2 - 12, -Constants.CELL_SIZE_PIXELS / 2 + 12);
                    
                    if (cell.Development.IsUpgrading)
                    {
                        drawingService
                            .DrawImage(
                                KnownImages.IconIsUpgrading,
                                drawPos,
                                10,
                                true);
                        drawPos += iconSpacing;
                    }
                    else if (cell.Development.CanManuallyUpgrade && _playerData.SelectedCell != cell)
                    {
                        drawingService
                            .DrawImage(
                                KnownImages.IconUpgrade,
                                drawPos,
                                10,
                                true);
                        drawPos += iconSpacing;
                    }

                    switch(cell.Development.FactoryState)
                    {
                        case FactoryState.NeedsInputs:
                            drawingService
                                .DrawImage(
                                    KnownImages.IconFactoryNeedsInputs,
                                    drawPos,
                                    10,
                                    true);
                            drawPos += iconSpacing;
                            break;
                        case FactoryState.NoRecipie:
                            drawingService
                                .DrawImage(
                                    KnownImages.IconFactoryNoRecipie,
                                    drawPos,
                                    10,
                                    true);
                            drawPos += iconSpacing;
                            break;
                        case FactoryState.OutputsFull:
                            drawingService
                                .DrawImage(
                                    KnownImages.IconFactoryOutputsFull,
                                    drawPos,
                                    10,
                                    true);
                            drawPos += iconSpacing;
                            break;
                    }

                    if(cell.Development.WorkerPercent < 1)
                    {
                        drawingService
                                .DrawImage(
                                    KnownImages.IconNotEnoughWorkers,
                                    drawPos,
                                    10,
                                    true);
                        drawPos += iconSpacing;
                    }

                    if(cell.Development is IHousingProvider house && house.Residents.Any())
                    {
                        if(house.AverageSatisfaction < 0.3)
                        {
                            drawingService
                                .DrawImage(
                                    KnownImages.IconHouseSatisfactionLow,
                                    drawPos,
                                    10,
                                    true);
                            drawPos += iconSpacing;
                        }
                        else if(house.AverageSatisfaction < 0.8)
                        {
                            drawingService
                                .DrawImage(
                                    KnownImages.IconHouseSatisfactionMed,
                                    drawPos,
                                    10,
                                    true);
                            drawPos += iconSpacing;
                        }

                        if(house.Residents.Count < house.ProvidedHousing)
                        {
                            drawingService
                                .DrawImage(
                                    KnownImages.IconHouseEmpty,
                                    drawPos,
                                    10,
                                    true);
                            drawPos += iconSpacing;
                        }
                    }
                }
            }
        }

        public void DrawWorldAsMap(IDrawingService drawingService, IEnumerable<Region> regionsInView)
        {
        }

        public void DrawTiles(IDrawingService drawingService, IEnumerable<Tile> tilesInView)
        {
            var iconSpacing = new Vector2(24, 0);

            var structuresInView = tilesInView
                .Select(t => t.Structure)
                .Where(s => s != null)
                .Distinct()
                .ToArray();

            foreach(var structure in structuresInView)
            {
                var drawPos = structure.UlTile.Position.ToVector() * Constants.TILE_SIZE_PIXELS;

                if (structure is IUpgradableHousing house && !structure.UnderConstruction)
                {
                    if (house.Residents.Any())
                    {
                        if (house.AverageSatisfaction < 0.3)
                        {
                            drawingService
                                .DrawImage(
                                    KnownImages.IconHouseSatisfactionLow,
                                    drawPos,
                                    10,
                                    true);
                            drawPos += iconSpacing;
                        }
                        else if (house.AverageSatisfaction < 0.8)
                        {
                            drawingService
                                .DrawImage(
                                    KnownImages.IconHouseSatisfactionMed,
                                    drawPos,
                                    10,
                                    true);
                            drawPos += iconSpacing;
                        }

                        if (house.UpgradePercent > 0)
                        {
                            drawingService
                                .DrawImage(
                                    "ui_house_upgrading",
                                    drawPos,
                                    10,
                                    true);
                            drawPos += iconSpacing;
                        }
                        else if (house.DowngradePercent > 0)
                        {
                            drawingService
                                .DrawImage(
                                    "ui_house_downgrading",
                                    drawPos,
                                    10,
                                    true);
                            drawPos += iconSpacing;
                        }
                    }

                    if (house.Residents.Count < house.ProvidedHousing)
                    {
                        drawingService
                            .DrawImage(
                                KnownImages.IconHouseEmpty,
                                drawPos,
                                10,
                                true);
                        drawPos += iconSpacing;
                    }
                }

                if (structure is TraderStructure trader && !trader.ToBorderPaths.Any())
                {
                    drawingService
                        .DrawImage(
                            UiImages.TradeRouteInvalid,
                            drawPos,
                            10,
                            true);
                    drawPos += iconSpacing;
                }

                if (!(structure is HousingStructure) && structure.IsUpgrading)
                {
                    drawingService
                        .DrawImage(
                            KnownImages.IconIsUpgrading,
                            drawPos,
                            10,
                            true);
                    drawPos += iconSpacing;
                }
                else if (structure.CanManuallyUpgrade)
                {
                    drawingService
                        .DrawImage(
                            KnownImages.IconUpgrade,
                            drawPos,
                            10,
                            true);
                    drawPos += iconSpacing;
                }

                if(structure is SupportUpgradableStructure upgradableStructure && upgradableStructure.DowngradeTimer > 0)
                {
                    drawingService
                            .DrawImage(
                                "ui_house_downgrading",
                                drawPos,
                                10,
                                true);
                    drawPos += iconSpacing;
                }

                switch (structure.FactoryState)
                {
                    case FactoryState.NeedsInputs:
                        drawingService
                            .DrawImage(
                                KnownImages.IconFactoryNeedsInputs,
                                drawPos,
                                10,
                                true);
                        drawPos += iconSpacing;
                        break;
                    case FactoryState.NoRecipie:
                        drawingService
                            .DrawImage(
                                KnownImages.IconFactoryNoRecipie,
                                drawPos,
                                10,
                                true);
                        drawPos += iconSpacing;
                        break;
                    case FactoryState.OutputsFull:
                        drawingService
                            .DrawImage(
                                KnownImages.IconFactoryOutputsFull,
                                drawPos,
                                10,
                                true);
                        drawPos += iconSpacing;
                        break;
                }

                if (structure.WorkerPercent < 1)
                {
                    drawingService
                            .DrawImage(
                                KnownImages.IconNotEnoughWorkers,
                                drawPos,
                                10,
                                true);
                    drawPos += iconSpacing;
                }
            }
        }

        public void Update(InputState inputState, float elapsedSeconds)
        {
        }
    }
}
