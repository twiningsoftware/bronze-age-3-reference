﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using Bronze.Common.Data.Game.Units;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;

namespace Bronze.Common.UI.MapInteraction
{
    public class ArmyOrderIssuer : IMapInteractionComponent
    {
        private readonly IPlayerDataTracker _playerData;
        private readonly IGameWorldDrawer _gameWorldDrawer;
        private readonly IAudioService _audioService;

        private Vector2 _moveShortcutStartPos;
        private DateTime _moveShortcutStartTime;
        private Army _moveShortcutArmy;

        public ArmyOrderIssuer(
            IPlayerDataTracker playerData,
            IGameWorldDrawer gameWorldDrawer,
            IAudioService audioService)
        {
            _playerData = playerData;
            _gameWorldDrawer = gameWorldDrawer;
            _audioService = audioService;
        }

        public void Update(InputState inputState, float elapsedSeconds)
        {
            if (_playerData.MouseMode is ArmyOrderMouseMode armyOrdermouseMode)
            {
                if (_playerData.SelectedArmy == null
                    || _playerData.SelectedArmy?.Owner != _playerData.PlayerTribe
                    || armyOrdermouseMode.Army != _playerData.SelectedArmy
                    || _playerData.SelectedArmy.IsLocked)
                {
                    _playerData.MouseMode = null;
                    return;
                }

                if (!inputState.MouseInWindow
                    || inputState.MouseOverWidget
                    || _playerData.ActiveSettlement != null)
                {
                    return;
                }
                else
                {
                    armyOrdermouseMode.Target = _playerData.HoveredCell;
                    armyOrdermouseMode.TargetValid = armyOrdermouseMode.ArmyAction.IsValidFor(armyOrdermouseMode.Army, _playerData.HoveredCell);

                    if(armyOrdermouseMode.PathCalculator.Destination != _playerData.HoveredCell)
                    {
                        armyOrdermouseMode.PathCalculator.SetDestination(_playerData.HoveredCell, armyOrdermouseMode.ArmyAction.PathAdjacent);
                    }

                    if (inputState.MouseReleased && armyOrdermouseMode.TargetValid && armyOrdermouseMode.PathCalculator.PathSuccessful)
                    {
                        armyOrdermouseMode.Army.CurrentOrder = armyOrdermouseMode.ArmyAction
                            .BuildOrderFor(armyOrdermouseMode.Army, armyOrdermouseMode.PathCalculator.Destination);

                        _playerData.MouseMode = null;
                    }
                }

                armyOrdermouseMode.PathCalculator.Update();
            }
            // Order shortcuts
            else if (_playerData.SelectedArmy != null 
                && !inputState.MouseOverWidget 
                && inputState.MouseInWindow 
                && !_playerData.SelectedArmy.IsLocked
                && _playerData.SelectedArmy.Owner == _playerData.PlayerTribe
                && _playerData.MouseMode == null)
            {
                if(inputState.MouseRightPressed)
                {
                    _moveShortcutArmy = _playerData.SelectedArmy;
                    _moveShortcutStartPos = inputState.MousePosition.ToVector2();
                    _moveShortcutStartTime = DateTime.Now;
                }
                else if (inputState.MouseRightReleased)
                {
                    var dragDistance = inputState.MousePosition.ToVector2() - _moveShortcutStartPos;
                    var holdTime = DateTime.Now - _moveShortcutStartTime;

                    if(_playerData.SelectedArmy == _moveShortcutArmy
                        && dragDistance.Length() < 10
                        && holdTime.TotalSeconds < 0.5)
                    {
                        var quickAction = new[]
                            {
                                _moveShortcutArmy.AvailableActions
                                    .Where(a => a is ArmyDisembarkAction)
                                    .FirstOrDefault(),
                                _moveShortcutArmy.AvailableActions
                                    .Where(a => a is ArmyRaidAction)
                                    .FirstOrDefault(),
                                _moveShortcutArmy.AvailableActions
                                    .Where(a => a is ArmyMergeAction)
                                    .FirstOrDefault(),
                                _moveShortcutArmy.AvailableActions
                                    .Where(a => a is ArmyApproachAction)
                                    .FirstOrDefault(),
                                _moveShortcutArmy.AvailableActions
                                    .Where(a => a is ArmyInteractAction)
                                    .FirstOrDefault(),
                                _moveShortcutArmy.AvailableActions
                                    .Where(a => a is ArmyMoveAction)
                                    .FirstOrDefault()
                            }
                            .Where(a=> a!= null)
                            .Where(a => a.IsValidForQuickAction(_moveShortcutArmy, _playerData.HoveredCell))
                            .FirstOrDefault();

                        if (quickAction != null)
                        {
                            _moveShortcutArmy.CurrentOrder = quickAction.BuildOrderFor(_moveShortcutArmy, _playerData.HoveredCell);
                        }
                    }
                }
            }
        }

        public void DrawWorld(
            IDrawingService drawingService, 
            IEnumerable<Cell> cellsInView,
            ViewMode viewMode)
        {
            if (_playerData.MouseMode is ArmyOrderMouseMode armyOrdermouseMode)
            {
                if(armyOrdermouseMode.TargetValid)
                {
                    foreach(var cell in armyOrdermouseMode.PathCalculator.Path)
                    {
                        if (cell.Position.Plane == _playerData.CurrentPlane)
                        {
                            _gameWorldDrawer.DrawPathdot(
                                drawingService,
                                cell.Position,
                                viewMode);
                        }
                    }
                }
                else if(armyOrdermouseMode.PathCalculator.Destination != null)
                {
                    _gameWorldDrawer.DrawRedX(
                            drawingService,
                            armyOrdermouseMode.PathCalculator.Destination.Position,
                            viewMode);
                }
            }
        }

        public void DrawWorldAsMap(
            IDrawingService drawingService, 
            IEnumerable<Region> regionsInView)
        {
            if (_playerData.MouseMode is ArmyOrderMouseMode armyOrdermouseMode)
            {
                if (armyOrdermouseMode.TargetValid)
                {
                    foreach (var cell in armyOrdermouseMode.PathCalculator.Path)
                    {
                        _gameWorldDrawer.DrawPathdot(
                            drawingService,
                            cell.Position,
                            ViewMode.Summary);
                    }
                }
                else if (armyOrdermouseMode.PathCalculator.Destination != null)
                {
                    _gameWorldDrawer.DrawRedX(
                            drawingService,
                            armyOrdermouseMode.PathCalculator.Destination.Position,
                            ViewMode.Summary);
                }
            }
        }

        public void DrawTiles(IDrawingService drawingService, IEnumerable<Tile> tilesInView)
        {
        }
    }
}
