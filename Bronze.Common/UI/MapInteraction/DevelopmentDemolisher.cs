﻿using System.Collections.Generic;
using Bronze.Common.Data.World.CellDevelopments;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI;

namespace Bronze.Common.UI.MapInteraction
{
    public class DevelopmentDemolisher : IMapInteractionComponent
    {
        private readonly IPlayerDataTracker _playerData;
        private readonly IGameWorldDrawer _gameWorldDrawer;
        private readonly IAudioService _audioService;

        public DevelopmentDemolisher(
            IPlayerDataTracker playerData,
            IGameWorldDrawer gameWorldDrawer,
            IAudioService audioService)
        {
            _playerData = playerData;
            _gameWorldDrawer = gameWorldDrawer;
            _audioService = audioService;
        }

        public void Update(InputState inputState, float elapsedSeconds)
        {
            if (_playerData.MouseMode is DemolishingDevelopmentMouseMode demolishingDevelopmentMouseMode)
            {
                if (_playerData.ActiveSettlement != null
                    || _playerData.ActiveRegion?.Settlement == null
                    || _playerData.ActiveRegion?.Settlement?.Owner != _playerData.PlayerTribe)
                {
                    _playerData.MouseMode = null;
                }

                if (!inputState.MouseInWindow
                    || inputState.MouseOverWidget
                    || _playerData.ActiveSettlement != null)
                {
                    return;
                }
                else
                {
                    if (inputState.MouseDown)
                    {
                        if (_playerData.HoveredCell.Region.Owner == _playerData.PlayerTribe 
                            && _playerData.HoveredCell.Development != null
                            && _playerData.HoveredCell.Development.CanBeDestroyed)
                        {
                            _playerData.HoveredCell.Development = null;

                            _audioService.PlaySound(UiSounds.Demolish);
                        }
                    }
                }
            }
        }

        public void DrawWorld(
            IDrawingService drawingService, 
            IEnumerable<Cell> cellsInView,
            ViewMode viewMode)
        {
            if (_playerData.MouseMode is DemolishingDevelopmentMouseMode demolishingDevelopmentMouseMode 
                && _playerData.HoveredCell != null
                && _playerData.HoveredCell.Region.Owner == _playerData.PlayerTribe 
                && _playerData.HoveredCell.Development != null
                && !(_playerData.HoveredCell.Development is DistrictDevelopment
                    || _playerData.HoveredCell.Development is VillageDevelopment))
            {
                _gameWorldDrawer
                    .DrawRedX(
                        drawingService,
                        _playerData.HoveredCell.Position,
                        viewMode);
            }
        }


        public void DrawTiles(IDrawingService drawingService, IEnumerable<Tile> tilesInView)
        {
        }

        public void DrawWorldAsMap(IDrawingService drawingService, IEnumerable<Region> regionsInView)
        {
        }
    }
}
