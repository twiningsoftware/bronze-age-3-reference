﻿using System.Collections.Generic;
using System.Linq;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI;

namespace Bronze.Common.UI.MapInteraction
{
    public class CultRiteTargeter : IMapInteractionComponent
    {
        private readonly IPlayerDataTracker _playerData;
        private readonly IGameWorldDrawer _gameWorldDrawer;
        private readonly IAudioService _audioService;

        public CultRiteTargeter(
            IPlayerDataTracker playerData,
            IGameWorldDrawer gameWorldDrawer,
            IAudioService audioService)
        {
            _playerData = playerData;
            _gameWorldDrawer = gameWorldDrawer;
            _audioService = audioService;
        }

        public void Update(InputState inputState, float elapsedSeconds)
        {
            if (_playerData.MouseMode is CultRiteTargetingMouseMode riteTargeterMouseMode)
            {
                riteTargeterMouseMode.PlacementValid = false;
                
                if (!inputState.MouseInWindow
                    || inputState.MouseOverWidget)
                {
                    return;
                }
                else
                {
                    riteTargeterMouseMode.PlacementValid = riteTargeterMouseMode.CultRite.IsValidFor(riteTargeterMouseMode.ForSettlement, _playerData.HoveredCell);

                    if (inputState.MouseDown)
                    {
                        if (riteTargeterMouseMode.PlacementValid)
                        {
                            riteTargeterMouseMode.CultRite.Enact(riteTargeterMouseMode.ForSettlement, _playerData.HoveredCell);

                            _playerData.MouseMode = null;
                        }
                    }
                }
            }
        }

        public void DrawWorld(
            IDrawingService drawingService, 
            IEnumerable<Cell> cellsInView,
            ViewMode viewMode)
        {
            if (_playerData.MouseMode is CultRiteTargetingMouseMode riteTargeterMouseMode && _playerData.HoveredCell != null)
            {
                if (riteTargeterMouseMode.PlacementValid)
                {
                    _gameWorldDrawer.DrawCellIcon(
                            drawingService,
                            _playerData.HoveredCell.Position,
                            viewMode,
                            riteTargeterMouseMode.CultRite.DetailedTargetingIcon,
                            riteTargeterMouseMode.CultRite.SummaryTargetingIcon);
                }
                else
                {
                    _gameWorldDrawer.DrawRedX(
                        drawingService,
                        _playerData.HoveredCell.Position,
                        viewMode);
                }
            }
        }

        public void DrawTiles(IDrawingService drawingService, IEnumerable<Tile> tilesInView)
        {
        }
    }
}
