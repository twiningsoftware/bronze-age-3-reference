﻿using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.UI.MapInteraction
{
    public class LogiPathDrawer : IMapInteractionComponent
    {
        private readonly IPlayerDataTracker _playerData;
        private readonly IGameWorldDrawer _gameWorldDrawer;
        private readonly TileAnimationLayer _deliveryAnimationLayer;

        public LogiPathDrawer(
            IPlayerDataTracker playerData,
            IGameWorldDrawer gameWorldDrawer)
        {
            _playerData = playerData;
            _gameWorldDrawer = gameWorldDrawer;
            _deliveryAnimationLayer = new TileAnimationLayer
            {
                ImageKey = "ui_tile_lines",
                TileLayout = TileLayout.Adjacency,
                DrawLayer = 0.4f
            };
        }

        public void DrawWorld(IDrawingService drawingService, IEnumerable<Cell> cellsInView, ViewMode viewMode)
        {
        }

        public void DrawWorldAsMap(IDrawingService drawingService, IEnumerable<Region> regionsInView)
        {
        }

        public void DrawTiles(IDrawingService drawingService, IEnumerable<Tile> tilesInView)
        {
            if (_playerData.ActiveSettlement != null)
            {
                var activeStructure = _playerData.SelectedStructure ?? _playerData.HoveredTile?.Structure;

                foreach(var hauler in _playerData.ActiveSettlement.LogiHaulers)
                {
                    if(hauler.PositionOnPath < hauler.Path.Length && hauler.ActiveMovementType != null)
                    {
                        var fromVec = hauler.Path[hauler.PositionOnPath].Position.ToVector();
                        var toVec = hauler.Path[hauler.PositionOnPath + 1].Position.ToVector();
                        
                        var worldPos = ((fromVec * (1 - (float)hauler.ProgressToNext)) + (toVec * (float)hauler.ProgressToNext));

                        var facing = Util.GetFacing(fromVec, toVec);

                        var animation = hauler.HaulerInfo.Animations
                            .Where(a => hauler.ActiveMovementType.AnimationNames.Contains(a.Name))
                            .FirstOrDefault();

                        if (animation != null)
                        {
                            animation.Draw(
                                drawingService,
                                worldPos * Constants.TILE_SIZE_PIXELS,
                                hauler.AnimationTime,
                                _playerData.ActiveSettlement.Owner.PrimaryColor,
                                facing,
                                0);

                            if(activeStructure == hauler.From || activeStructure == hauler.To)
                            {
                                drawingService.DrawSlicedImage(
                                    UiImages.DragSelectBox,
                                    Rectangle.AroundCenter(worldPos * Constants.TILE_SIZE_PIXELS, animation.Width, animation.Height),
                                    10,
                                    true);
                            }
                        }
                    }
                }

                if(activeStructure != null)
                {
                    var adjacencies = new bool[8];

                    var orthoFaces = new Facing[]
                    {
                        Facing.East,
                        Facing.North,
                        Facing.South,
                        Facing.West
                    };

                    foreach(var deliveryRoute in activeStructure.DeliveryRoutes)
                    {
                        for (var i = 0; i < deliveryRoute.Path.Length; i++)
                        {
                            var tile = deliveryRoute.Path[i];

                            foreach(var facing in orthoFaces)
                            {
                                adjacencies[(int)facing] = (i > 0 && deliveryRoute.Path[i - 1] == tile.GetNeighbor(facing))
                                    || (i < deliveryRoute.Path.Length - 1 && deliveryRoute.Path[i + 1] == tile.GetNeighbor(facing));
                            }
                            
                            _deliveryAnimationLayer.DrawTile(
                                drawingService,
                                BronzeColor.None,
                                tile,
                                adjacencies);
                        }
                    }

                    if (activeStructure.DeliveryArea != null)
                    {
                        foreach (var tile in activeStructure.DeliveryArea)
                        {
                            drawingService.DrawImage(
                                "ui_tile_mask",
                                tile.Position.ToVector() * Constants.TILE_SIZE_PIXELS,
                                5,
                                true);
                        }
                    }
                }
            }
        }

        public void Update(InputState inputState, float elapsedSeconds)
        {
        }
        
        private void ClearState()
        {
        }
    }
}
