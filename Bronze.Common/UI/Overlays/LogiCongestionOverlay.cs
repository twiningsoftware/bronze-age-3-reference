﻿using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using System.Collections.Generic;

namespace Bronze.Common.UI.Overlays
{
    public class LogiCongestionOverlay : IOverlay
    {
        private readonly IPlayerDataTracker _playerData;

        public string Name => "Congestion";
        public bool IsAvailble => _playerData.ActiveSettlement != null;
        public bool IsActive { get; set; }

        public LogiCongestionOverlay(IPlayerDataTracker playerData)
        {
            _playerData = playerData;
            IsActive = false;
        }

        public void DrawWorld(IDrawingService drawingService, IEnumerable<Cell> cellsInView, ViewMode viewMode)
        {
        }

        public void DrawWorldAsMap(IDrawingService drawingService, IEnumerable<Region> regionsInView)
        {
        }

        public void DrawTiles(IDrawingService drawingService, IEnumerable<Tile> tilesInView)
        {
            if(_playerData.ActiveSettlement == null)
            {
                return;
            }

            if(IsActive)
            {
                foreach(var actor in _playerData.ActiveSettlement.EconomicActors)
                {
                    DrawPathsForActor(drawingService, actor);
                }
            }
            else if(_playerData.SelectedStructure != null)
            {
                DrawPathsForActor(drawingService, _playerData.SelectedStructure);
            }
        }

        private void DrawPathsForActor(IDrawingService drawingService, IEconomicActor actor)
        {
            // TODO outgoing paths instead
            //foreach (var path in actor.IncomingPaths)
            //{
            //    for (var i = 0; i < path.Tiles.Length; i++)
            //    {
            //        var image = string.Empty;
            //        if (path.ThroughputByTile[i] > 0.8)
            //        {
            //            image = "ui_logi_overlay_low";
            //        }
            //        else if (path.ThroughputByTile[i] > 0.6)
            //        {
            //            image = "ui_logi_overlay_medium";
            //        }
            //        else
            //        {
            //            image = "ui_logi_overlay_high";
            //        }

            //        drawingService.DrawImage(
            //            image,
            //            path.Tiles[i].Position.ToVector() * Constants.TILE_SIZE_PIXELS + new System.Numerics.Vector2(0.5f),
            //            10f,
            //            true);
            //    }
            //}
        }

        public void Update(InputState inputState, float elapsedSeconds)
        {
        }
    }
}
