﻿using Bronze.Contracts.Data;
using Bronze.Contracts.UI;
using System;

namespace Bronze.Common.UI
{
    public class DemolishingStructureMouseMode : IMouseMode
    {
        private Action _cancelAction;

        public DemolishingStructureMouseMode(Action cancelAction)
        {
            _cancelAction = cancelAction;
        }

        public void OnCancel()
        {
            _cancelAction();
        }

        public bool IsValidFor(ViewMode viewMode)
        {
            return viewMode == ViewMode.Settlement;
        }
    }
}
