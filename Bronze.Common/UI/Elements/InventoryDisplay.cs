﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;

namespace Bronze.Common.UI.Elements
{
    public class InventoryDisplay : AbstractUiElement
    {
        private readonly int _itemsPerRow;
        private readonly ItemInventory _inventory;
        private StackGroup _internalStack;
        private IEnumerable<Item> _lastItems;
        private bool _needsRelayout;

        public override bool NeedsRelayout => _internalStack.NeedsRelayout || _needsRelayout;

        public InventoryDisplay(ItemInventory inventory, int itemsPerRow)
        {
            _inventory = inventory;
            _itemsPerRow = itemsPerRow;
            _internalStack = new StackGroup
            {
                Orientation = Orientation.Vertical
            };
            _lastItems = Enumerable.Empty<Item>();
            _needsRelayout = true;

            var newItems = _inventory.ItemsPresent
                .Where(i => _inventory.QuantityOf(i) >= 1)
                .ToArray();

            RebuildContent(newItems);
        }

        public override void Update(InputState inputState, IAudioService audioService, float elapsedSeconds)
        {
            base.Update(inputState, audioService, elapsedSeconds);

            var newItems = _inventory.ItemsPresent
                .Where(i => _inventory.QuantityOf(i) >= 1)
                .ToArray();

            if(newItems.Intersect(_lastItems).Count() != newItems.Union(_lastItems).Count())
            {
                RebuildContent(newItems);
            }

            _internalStack.Update(inputState, audioService, elapsedSeconds);
        }

        private void RebuildContent(Item[] newItems)
        {
            _lastItems = newItems;
            _needsRelayout = true;
            _internalStack.Children.Clear();

            for (var i = 0; i < newItems.Length; i += _itemsPerRow)
            {
                _internalStack.Children.Add(
                    new StackGroup
                    {
                        Orientation = Orientation.Horizontal,
                        Children = newItems.Skip(i).Take(_itemsPerRow)
                            .SelectMany(item => new IUiElement[]
                            {
                                    new IconText(item.IconKey, "", tooltip: item.Name + "\n" + item.Description)
                                    {
                                        ContentUpdater = () => Math.Floor(_inventory.QuantityOf(item)).ToString("N0")
                                    },
                                    new Spacer(3, 0)
                            })
                            .ToList()
                    });
            }
        }

        public override void DoLayout(IDrawingService drawingService, Rectangle parentBounds)
        {
            if(!_internalStack.NeedsRelayout)
            {
                _needsRelayout = false;
            }

            _internalStack.DoLayout(drawingService, parentBounds);

            Bounds = _internalStack.Bounds;
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            _internalStack.Draw(drawingService, layer);
        }
    }
}
