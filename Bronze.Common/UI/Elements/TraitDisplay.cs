﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.InjectableServices;
using Bronze.UI.Data;
using Bronze.UI.Elements;

namespace Bronze.Common.UI.Elements
{
    public class TraitDisplay : AbstractUiElement
    {
        private Func<IEnumerable<Trait>> _getTraits;
        private StackGroup _internalStack;
        private IEnumerable<Trait> _lastTraits;
        private bool _needsRelayout;

        public override bool NeedsRelayout => _internalStack.NeedsRelayout || _needsRelayout;

        public TraitDisplay(Func<IEnumerable<Trait>> getTraits)
        {
            _getTraits = getTraits;
            _internalStack = new StackGroup
            {
                Orientation = Orientation.Horizontal
            };
            _lastTraits = Enumerable.Empty<Trait>();
            _needsRelayout = true;

            UpdateTraits();
        }

        public override void Update(InputState inputState, IAudioService audioService, float elapsedSeconds)
        {
            base.Update(inputState, audioService, elapsedSeconds);

            UpdateTraits();

            _internalStack.Update(inputState, audioService, elapsedSeconds);
        }

        public override void DoLayout(IDrawingService drawingService, Rectangle parentBounds)
        {
            if(!_internalStack.NeedsRelayout)
            {
                _needsRelayout = false;
            }

            _internalStack.DoLayout(drawingService, parentBounds);

            Bounds = _internalStack.Bounds;
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            _internalStack.Draw(drawingService, layer);
        }

        private void UpdateTraits()
        {
            var newTraits = _getTraits().Where(t => !t.Hidden).ToArray();

            if (newTraits.Intersect(_lastTraits).Count() != newTraits.Union(_lastTraits).Count())
            {
                _lastTraits = newTraits;
                _needsRelayout = true;
                _internalStack.Children.Clear();
                foreach (var trait in newTraits)
                {
                    _internalStack.Children
                        .Add(new IconText(trait.IconKey, "", tooltip: trait.Name + "\n" + trait.Description));
                    _internalStack.Children.Add(new Spacer(3, 0));
                }
            }
        }
    }
}
