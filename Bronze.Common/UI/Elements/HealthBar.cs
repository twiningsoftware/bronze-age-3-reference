﻿using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;
using Bronze.UI.Elements;
using System;
using System.Numerics;

namespace Bronze.Common.UI.Elements
{
    public class HealthBar : AbstractUiElement
    {
        public override bool NeedsRelayout => false;

        private int _width;
        private int _height;

        private Func<float> _getPercentFunc;

        public Func<float, BronzeColor> PercentToColor { get; set; }

        public HealthBar(Func<float> getPercentFunc, int width, int height = 4)
        {
            _getPercentFunc = getPercentFunc;
            _width = width;
            _height = height;

            PercentToColor = percent => Util.PercentToColor(percent);
        }
        
        public override void DoLayout(IDrawingService drawingService, Rectangle parentBounds)
        {
            Bounds = new Rectangle(
                parentBounds.X,
                parentBounds.Y,
                _width,
                _height);
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            var percent = _getPercentFunc();

            var barWidth = Bounds.Width * percent;

            var barColor = PercentToColor(percent);

            var from = new Vector2(
                Bounds.Left,
                Bounds.Bottom - _height / 2 );

            var to = from + new Vector2(barWidth, 0);

            drawingService
                .DrawLine(
                    from,
                    from + new Vector2(_width, 0),
                    _height,
                    layer + 1,
                    BronzeColor.Grey,
                    false);

            drawingService
                .DrawLine(
                    from,
                    to,
                    _height,
                    layer + 2,
                    barColor,
                    false);
        }
    }
}
