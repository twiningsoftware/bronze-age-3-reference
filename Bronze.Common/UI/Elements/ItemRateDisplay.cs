﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;

namespace Bronze.Common.UI.Elements
{
    public class ItemRateDisplay : AbstractUiElement
    {
        private Func<Dictionary<Item, double>> _getRatesFunc;
        private StackGroup _internalStack;
        private IEnumerable<Item> _lastItems;
        private bool _needsRelayout;

        public override bool NeedsRelayout => _internalStack.NeedsRelayout || _needsRelayout;

        public ItemRateDisplay(Func<Dictionary<Item, double>> getRatesFunc)
        {
            _getRatesFunc = getRatesFunc;
            
            _internalStack = new StackGroup
            {
                Orientation = Orientation.Vertical
            };
            _lastItems = Enumerable.Empty<Item>();
            _needsRelayout = true;
        }

        public override void Update(InputState inputState, IAudioService audioService, float elapsedSeconds)
        {
            base.Update(inputState, audioService, elapsedSeconds);

            var rates = _getRatesFunc();

            var newItems = rates.Keys.ToArray();

            if(newItems.Intersect(_lastItems).Count() != newItems.Union(_lastItems).Count())
            {
                _lastItems = newItems;
                _needsRelayout = true;
                _internalStack.Children.Clear();
                var perRow = 6;
                for (var i = 0; i < newItems.Length; i += perRow)
                {
                    _internalStack.Children
                        .Add(new StackGroup
                        {
                            Orientation = Orientation.Horizontal,
                            Children = newItems.Skip(i).Take(perRow)
                                .SelectMany(item => new IUiElement[]
                                {
                                    new IconText(item.IconKey, "", tooltip: item.Name + "\n" + item.Description)
                                    {
                                        ContentUpdater = () =>
                                        {
                                            var r = _getRatesFunc();

                                            if(r.ContainsKey(item))
                                            {
                                                if(r[item] > 0)
                                                {
                                                    return "+" + r[item].ToString("N0");
                                                }
                                                else
                                                {
                                                    return r[item].ToString("N0");
                                                }
                                            }

                                            return string.Empty;
                                        },
                                        ColorUpdater = () =>
                                        {
                                            var r = _getRatesFunc();

                                            if(r.ContainsKey(item) && r[item] < 0)
                                            {
                                                return BronzeColor.Red;
                                            }

                                            return BronzeColor.Green;
                                        }
                                    },
                                    new Spacer(5, 0)
                                })
                                .ToList()
                        });
                }
            }

            _internalStack.Update(inputState, audioService, elapsedSeconds);
        }

        public override void DoLayout(IDrawingService drawingService, Rectangle parentBounds)
        {
            if(!_internalStack.NeedsRelayout)
            {
                _needsRelayout = false;
            }

            _internalStack.DoLayout(drawingService, parentBounds);

            Bounds = _internalStack.Bounds;
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            _internalStack.Draw(drawingService, layer);
        }
    }
}
