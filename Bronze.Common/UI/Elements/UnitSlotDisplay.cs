﻿using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using System.Collections.Generic;

namespace Bronze.Common.UI.Elements
{
    public class UnitSlotDisplay : UiElementWithTooltip
    {
        public static readonly int ExpectedWidth = 48; // 48 px wide with 3 px margin 
        public static readonly int ExpectedHeight = 72;

        public override bool NeedsRelayout => base.NeedsRelayout || _stack.NeedsRelayout;
        
        private readonly StackGroup _stack;
        
        public UnitSlotDisplay(IUnitHelper unitHelper, Army army, IUnit unit)
        {
            _stack = new StackGroup
            {
                Orientation = Orientation.Vertical,
                HorizontalContentAlignment = HorizontalAlignment.Middle,
                Children = new List<IUiElement>
                {
                    new HealthBar(() => (float)unit.HealthPercent, width: 48),
                    new HealthBar(() => (float)unit.Morale, width: 48)
                    {
                        PercentToColor = percent => BronzeColor.Blue
                    },
                    new HealthBar(() => (float)unit.SuppliesPercent, width: 48)
                    {
                        PercentToColor = percent => BronzeColor.Orange
                    },
                    new Image(unit.PortraitKey, tooltip: unit.Name)
                    {
                        ImageUpdater = () => unit.PortraitKey,
                        Colorization = army.Owner.PrimaryColor
                    },
                    new Image(unit.Category.IconKey, tooltip: unit.Category.Name)
                }
            };
        }

        public UnitSlotDisplay(Tribe owner, IUnit unit)
        {
            _stack = new StackGroup
            {
                Orientation = Orientation.Vertical,
                HorizontalContentAlignment = HorizontalAlignment.Middle,
                Children = new List<IUiElement>
                {
                    new HealthBar(() => (float)unit.HealthPercent, width: 48),
                    new HealthBar(() => (float)unit.SuppliesPercent, width: 48)
                    {
                        PercentToColor = percent => BronzeColor.Orange
                    },
                    new Image(unit.PortraitKey, tooltip: unit.Name)
                    {
                        ImageUpdater = () => unit.PortraitKey,
                        Colorization = owner.PrimaryColor
                    },
                    new Image(unit.Category.IconKey, tooltip: unit.Category.Name)
                }
            };
        }

        public UnitSlotDisplay(Tribe owner, IUnitType unitType, UnitEquipmentSet equipmentSet)
        {
            _stack = new StackGroup
            {
                Orientation = Orientation.Vertical,
                HorizontalContentAlignment = HorizontalAlignment.Middle,
                Children = new List<IUiElement>
                {
                    new HealthBar(() => 0, width: 48),
                    new HealthBar(() => 0, width: 48)
                    {
                        PercentToColor = percent => BronzeColor.Orange
                    },
                    new Image(equipmentSet.PortraitKey, tooltip: unitType.Name)
                    {
                        Colorization = owner.PrimaryColor
                    },
                    new Image(unitType.Category.IconKey, tooltip: unitType.Category.Name)
                }
            };
        }
        
        public override void Update(
            InputState inputState, 
            IAudioService audioService, 
            float elapsedSeconds)
        {
            base.Update(inputState, audioService, elapsedSeconds);
            _stack.Update(inputState, audioService, elapsedSeconds);
        }

        public override void DoLayout(IDrawingService drawingService, Rectangle parentBounds)
        {
            _stack.DoLayout(drawingService, parentBounds);
            base.DoLayout(drawingService, parentBounds);
            Bounds = _stack.Bounds;
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            base.Draw(drawingService, layer);

            _stack.Draw(drawingService, layer);
        }
    }
}
