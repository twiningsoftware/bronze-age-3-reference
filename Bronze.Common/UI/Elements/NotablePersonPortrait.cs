﻿using Bronze.Contracts.Data;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.UI;
using Bronze.UI.Elements;
using System;

namespace Bronze.Common.UI.Elements
{
    public class NotablePersonPortrait : AbstractUiElement
    {
        public override bool NeedsRelayout => false;

        private readonly Func<NotablePerson> _getPerson;
        
        public NotablePersonPortrait(Func<NotablePerson> getPerson)
        {
            _getPerson = getPerson;
        }
        
        public override void DoLayout(
            IDrawingService drawingService, 
            Rectangle parentBounds)
        {
            Bounds = new Rectangle(
                parentBounds.X,
                parentBounds.Y,
                82,
                93);
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            var person = _getPerson();

            drawingService.DrawImage(
                UiImages.PORTRAIT_BORDER,
                Bounds,
                layer,
                BronzeColor.None,
                false);

            if (person != null)
            {
                foreach (var placement in person.PortraitParts)
                {
                    var key = placement.Part.GetKeyFor(person);

                    drawingService.DrawImage(
                        key,
                        Bounds,
                        layer + (float)placement.Part.Layer + 0.1f,
                        person.Owner.PrimaryColor,
                        false);
                }
            }
            else
            {
                drawingService.DrawImage(
                    UiImages.PORTRAIT_NULL,
                    Bounds,
                    layer + 0.1f,
                    BronzeColor.None,
                    false);
            }
        }
    }
}
