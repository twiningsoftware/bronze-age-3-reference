﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;

namespace Bronze.Common.UI.Elements
{
    public class BuffDisplay : AbstractUiElement
    {
        private Func<IEnumerable<Buff>> _getBuffs;
        private StackGroup _internalStack;
        private IEnumerable<Buff> _lastBuffs;
        private bool _needsRelayout;

        public override bool NeedsRelayout => _internalStack.NeedsRelayout || _needsRelayout;

        public BuffDisplay(Func<IEnumerable<Buff>> getBuffs)
        {
            _getBuffs = getBuffs;
            _internalStack = new StackGroup
            {
                Orientation = Orientation.Horizontal
            };
            _lastBuffs = Enumerable.Empty<Buff>();
            _needsRelayout = true;

            UpdateBuffs();
        }

        public override void Update(InputState inputState, IAudioService audioService, float elapsedSeconds)
        {
            base.Update(inputState, audioService, elapsedSeconds);

            UpdateBuffs();

            _internalStack.Update(inputState, audioService, elapsedSeconds);
        }

        public override void DoLayout(IDrawingService drawingService, Rectangle parentBounds)
        {
            if(!_internalStack.NeedsRelayout)
            {
                _needsRelayout = false;
            }

            _internalStack.DoLayout(drawingService, parentBounds);

            Bounds = _internalStack.Bounds;
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            _internalStack.Draw(drawingService, layer);
        }

        private void UpdateBuffs()
        {
            var newBuffs = _getBuffs().ToArray();

            if (newBuffs.Intersect(_lastBuffs).Count() != newBuffs.Union(_lastBuffs).Count())
            {
                _lastBuffs = newBuffs;
                _needsRelayout = true;
                _internalStack.Children.Clear();
                foreach (var buff in newBuffs)
                {
                    _internalStack.Children
                        .Add(new IconText(buff.IconKey, string.Empty)
                        {
                            Tooltip = new SlicedBackground(
                                new StackGroup
                                {
                                    Orientation = Orientation.Vertical,
                                    Children = new List<IUiElement>
                                    {
                                        new Label(buff.Name),
                                        new Label(buff.Description, BronzeColor.Grey),
                                        new Label(buff.ExpireMonth > 0 ? $"Expires {Util.FriendlyDateDisplay(buff.ExpireMonth)}" : string.Empty, BronzeColor.Grey),
                                    }
                                    .Concat(buff.AppliedBonuses.Keys
                                        .Select(bt => new IconText(bt.IconKey, buff.AppliedBonuses[bt].ToString("P0"), tooltip: bt.Name)
                                        {
                                            ContentUpdater = () => buff.AppliedBonuses[bt].ToString("P0"),
                                            DisplayCondition = () => buff.AppliedBonuses[bt] != 0
                                        }))
                                    .Concat(buff.AppliedInfluence.Keys
                                        .Select(c => new IconText(c.IconKey, buff.AppliedInfluence[c].ToString("F0"), tooltip: c.Name)
                                        {
                                            ContentUpdater = () => buff.AppliedInfluence[c].ToString("F0"),
                                            DisplayCondition = () => buff.AppliedInfluence[c] != 0
                                        }))
                                    .ToList()
                                })
                        });
                    _internalStack.Children.Add(new Spacer(3, 0));
                }
            }
        }
    }
}
