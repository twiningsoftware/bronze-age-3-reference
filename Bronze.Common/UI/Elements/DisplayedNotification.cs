﻿using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using System.Collections.Generic;
using System.Numerics;

namespace Bronze.Common.UI.Elements
{
    public class DisplayedNotification : UiElementWithTooltip
    {
        private bool _firstPosSet;
        private Vector2 _desiredPos;
        private Vector2 _actualPos;
        private bool _isHovered;
        private bool _needsRelayout;
        private double _flickerTimer;

        private readonly INotificationsSystem _notificationsSystem;
        private readonly IWorldManager _worldManager;
        private readonly BronzeColor _colorization;

        public Notification Notification { get; }
        public override bool NeedsRelayout => base.NeedsRelayout || _needsRelayout;

        public DisplayedNotification(
            Notification notification,
            INotificationsSystem notificationsSystem,
            IWorldManager worldManager,
            BronzeColor colorization)
        {
            Notification = notification;
            _notificationsSystem = notificationsSystem;
            _worldManager = worldManager;
            _colorization = colorization;
            _needsRelayout = true;

            Tooltip = new SlicedBackground(new StackGroup
            {
                Width = Width.Fixed(200),
                Orientation = Orientation.Vertical,
                Children = new List<IUiElement>
                {
                    new Text(notification.SummaryTitle),
                    new Text(notification.SummaryBody),
                    new Text("Expires in ...", BronzeColor.Grey)
                    {
                        ContentUpdater = () =>
                        {
                            if(notification.ExpireDate > 0)
                            {
                                return $"Expires in {Util.FriendlyTimeDisplay(notification.ExpireDate - worldManager.Month)}.";
                            }

                            return string.Empty;
                        }
                    }
                }
            });
        }
        
        public override void Update(
            InputState inputState, 
            IAudioService audioService, 
            float elapsedSeconds)
        {
            base.Update(inputState, audioService, elapsedSeconds);

            var delta = _desiredPos - _actualPos;

            var len = delta.Length();

            if (len > 0)
            {
                if(len > elapsedSeconds * 80)
                {
                    _actualPos += delta / len * elapsedSeconds * 80;
                }
                else
                {
                    _actualPos = _desiredPos;
                }

                Bounds = new Rectangle(
                    (int)_actualPos.X,
                    (int)_actualPos.Y,
                    48,
                    48);
            }

            _isHovered = Bounds.Contains(inputState.MousePosition);

            if(inputState.MouseReleased && _isHovered)
            {
                _notificationsSystem.ShowNotification(Notification);
            }
            else if (inputState.MouseRightReleased && _isHovered)
            {
                if(Notification.QuickDismissable)
                {
                    Notification.IsDismissed = true;
                }
            }

            if(Notification.ExpireDate - _worldManager.Month < 1 && Notification.ExpireDate > 0)
            {
                _flickerTimer += elapsedSeconds;

                if(_flickerTimer > 1.0)
                {
                    _flickerTimer = -0.5;
                }
            }
        }

        public override void DoLayout(
            IDrawingService drawingService, 
            Rectangle parentBounds)
        {
            base.DoLayout(drawingService, parentBounds);

            _desiredPos = new Vector2(
                parentBounds.X,
                parentBounds.Y);

            if(!_firstPosSet)
            {
                _firstPosSet = true;
                _actualPos = new Vector2(_desiredPos.X, -48);

                Bounds = new Rectangle(
                    (int)_actualPos.X,
                    (int)_actualPos.Y,
                    48,
                    48);
            }
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            base.Draw(drawingService, layer);

            if (_isHovered)
            {
                drawingService.DrawImage(
                    "ui_notification_frame_highlight",
                    Bounds,
                    layer,
                    false);
            }
            else
            {
                if (_flickerTimer >= 0)
                {
                    drawingService.DrawImage(
                        "ui_notification_frame_normal",
                        Bounds,
                        layer,
                        false);
                }
                else
                {
                    drawingService.DrawImage(
                        "ui_notification_frame_fade",
                        Bounds,
                        layer,
                        false);
                }
            }

            drawingService.DrawImage(
                Notification.Icon,
                _colorization,
                Bounds.Center.ToVector2(),
                layer + 1,
                false);
        }
    }
}
