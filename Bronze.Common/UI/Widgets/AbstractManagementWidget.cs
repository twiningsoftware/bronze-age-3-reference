﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bronze.Common.UI.Elements;
using Bronze.Common.UI.Modals;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Modals;
using Bronze.UI.Widgets;

namespace Bronze.Common.UI.Widgets
{
    public  abstract class AbstractManagementWidget : UiWindowWidget
    {
        private const int MIN_WIDTH = 250;
        private const int MIN_HEIGHT = 300;

        protected override int WidgetWidth => ContentStack.Bounds.Width;
        protected override int WidgetHeight => ContentStack.Bounds.Height;
        protected override HorizontalAlignment HorizontalAlignment => HorizontalAlignment.Right;
        protected override VerticalAlignment VerticalAlignment => VerticalAlignment.Bottom;

        private readonly IPlayerDataTracker _playerData;
        private readonly IDialogManager _dialogManager;
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IEmpireNotificationBuilder _empireNotificationHandler;
        private readonly IWorldManager _worldManager;
        private readonly INotificationsSystem _notificationsSystem;
        
        protected StackGroup ContentStack { get; private set; }

        private bool _needsRelayout;

        public override bool NeedsRelayout => ContentStack.NeedsRelayout || _needsRelayout || true;

        public override bool IsActive => !(_playerData.MouseMode is ArmyOrderMouseMode) && GetActiveSettlement() != null;

        protected Settlement ActiveSettlement { get; private set; }

        protected TabData[] Tabs { get; set; }
        protected TabData ActiveTab { get; set; }

        protected Caste ActiveConstructionCaste { get; set; }

        protected StackGroup _header;
        protected StackGroup _tabBody;

        protected Action Refresh { get; set; }
        protected bool _needToRefresh;
        public Action _updateFunction;

        public AbstractManagementWidget(
            IPlayerDataTracker playerData,
            IDialogManager dialogManager,
            IGamedataTracker gamedataTracker,
            IEmpireNotificationBuilder empireNotificationHandler,
            IWorldManager worldManager,
            INotificationsSystem notificationsSystem)
        {
            _playerData = playerData;
            _dialogManager = dialogManager;
            _gamedataTracker = gamedataTracker;
            _empireNotificationHandler = empireNotificationHandler;
            _worldManager = worldManager;
            _notificationsSystem = notificationsSystem;
            _needsRelayout = false;

            ContentStack = new StackGroup
            {
                Orientation = Orientation.Vertical,
                Children = new List<IUiElement>()
            };

            _tabBody = new StackGroup
            {
                Orientation = Orientation.Vertical
            };

            _header = new StackGroup
            {
                Orientation = Orientation.Vertical
            };

            Tabs = BuildTabs().ToArray();

            Refresh = () => { };

            _playerData.PlayerTribe.OnItemDiscovered += CallRefresh;
        }

        private void CallRefresh(Item item)
        {
            if (IsActive)
            {
                Refresh?.Invoke();
            }
            else
            {
                _needToRefresh = true;
            }
        }

        protected virtual IEnumerable<TabData> BuildTabs()
        {
            return new[]
            {
                new TabData
                {
                    IconKey = "settlement_management_tab_overview",
                    Name = "Overview",
                    Populate = () => SetTabOverview()
                },
                new TabData
                {
                    IconKey = "settlement_management_tab_civics",
                    Name = "Civics",
                    Populate = () => SetTabCivics()
                },
                new TabData
                {
                    IconKey = "settlement_management_tab_cults",
                    Name = "Cults",
                    Populate = () => SetTabCults()
                },
                new TabData
                {
                    IconKey = "settlement_management_tab_units",
                    Name = "Military",
                    Populate = () => SetTabMilitary()
                },
                new TabData
                {
                    IconKey = "settlement_management_tab_construction",
                    Name = "Construction",
                    Populate = () => SetTabConstruction(ActiveSettlement.Race.Castes.First())
                }
            };
        }

        public abstract Settlement GetActiveSettlement();

        public override void Update(
            InputState inputState,
            IAudioService audioService,
            float elapsedSeconds)
        {
            if(_updateFunction != null)
            {
                _updateFunction();
            }

            if(_needToRefresh)
            {
                _needToRefresh = false;
                Refresh?.Invoke();
            }

            var newSettlement = GetActiveSettlement();

            if(newSettlement != ActiveSettlement)
            {
                ActiveTab = Tabs[0];
                _needsRelayout = true;
                ContentStack.Children.Clear();
                ActiveSettlement = newSettlement;
                _playerData.MouseMode = null;

                if (ActiveSettlement != null)
                {
                    Tabs[0].Populate();

                    BuildHeader();
                    
                    ContentStack.Children.Add(new Spacer(MIN_WIDTH, 10));
                    ContentStack.Children.Add(
                        new StackGroup
                        {
                            Orientation = Orientation.Horizontal,
                            Children = new List<IUiElement>
                            {
                                new Spacer(5, 0),
                                _header,
                                new Spacer(5, 0)
                            }
                        });
                    ContentStack.Children.Add(new Spacer(0, 5));
                    ContentStack.Children.Add(
                        new StackGroup
                        {
                            Orientation = Orientation.Horizontal,
                            Children = new List<IUiElement>
                            {
                                new Spacer(5, MIN_HEIGHT),
                                new StackGroup
                                {
                                    Orientation = Orientation.Vertical,
                                    Children = Tabs
                                        .Select(t => new IconButton(
                                            t.IconKey, 
                                            string.Empty, 
                                            () =>
                                            {
                                                ActiveTab = t;
                                                _needsRelayout = true;
                                                t.Populate();
                                            }, 
                                            tooltip: t.Name,
                                            highlightCheck: () => ActiveTab == t))
                                        .ToList<IUiElement>()
                                },
                                new Spacer(10, 0),
                                _tabBody,
                                new Spacer(5, MIN_HEIGHT),
                            }
                        });
                    ContentStack.Children.Add(new Spacer(MIN_WIDTH, 10));
                }
            }

            base.Update(inputState, audioService, elapsedSeconds);
            
            ContentStack.Update(inputState, audioService, elapsedSeconds);
        }
        
        public override void DoLayout(IDrawingService drawingService, Rectangle parentBounds)
        {
            _needsRelayout = ContentStack.NeedsRelayout;
            
            base.DoLayout(drawingService, parentBounds);

            ContentStack.DoLayout(drawingService, Bounds);
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            base.Draw(drawingService, layer);
            ContentStack.Draw(drawingService, layer + LAYER_STEP);
        }

        private void BuildHeader()
        {
            var topRow = new StackGroup
            {
                Orientation = Orientation.Horizontal,
                VerticalContentAlignment = VerticalAlignment.Middle,
                Children = new List<IUiElement>
                {
                    new TextInput(
                        () => ActiveSettlement.Name,
                        val => ActiveSettlement.Name = val,
                        150),
                    new Spacer(10, 0)
                }
            };

            topRow.Children.Add(new IconButton(
                "ui_become_capital",
                string.Empty,
                () =>
                {
                    _dialogManager.PushModal<ConfirmModal, ConfirmModalContext>(new ConfirmModalContext
                    {
                        Title = "Change Capital to " + ActiveSettlement.Name,
                        Body = "You can only change capitals once every 3 years.",
                        YesAction = () =>
                        {
                            _playerData.PlayerTribe.LastCapitalChangeMonth = _worldManager.Month;
                            _playerData.PlayerTribe.Capital = ActiveSettlement;
                            _notificationsSystem.AddNotification(_empireNotificationHandler
                                .BuildCapitalChanged(_playerData.PlayerTribe, _playerData.PlayerTribe.Capital));
                        }
                    });
                },
                disableCheck: () => _playerData.PlayerTribe.LastCapitalChangeMonth + 36 > _worldManager.Month
                                || _playerData.PlayerTribe.Capital == ActiveSettlement)
            {
                Tooltip = new SlicedBackground(new Text(" ")
                {
                    Width = Width.Fixed(200),
                    ContentUpdater = () =>
                    {
                        if (ActiveSettlement == _playerData.PlayerTribe.Capital)
                        {
                            return $"{ActiveSettlement.Name} is your capital.";
                        }
                        if (_playerData.PlayerTribe.LastCapitalChangeMonth + 40 > _worldManager.Month)
                        {
                            return "You cannot change capitals until " + Util.FriendlyDateDisplay(_playerData.PlayerTribe.LastCapitalChangeMonth + 40);
                        }

                        return $"Make {ActiveSettlement.Name} your capital.";
                    }
                })
            });

            var popRow = new List<IUiElement>();
            foreach (var caste in ActiveSettlement.PopulationByCaste.Select(kvp => kvp.Key))
            {
                popRow.Add(new IconText(caste.WorkerIcon, string.Empty, tooltip: "Available " + caste.Name + " workers.")
                {
                    ContentUpdater = () => Math.Max(0, ActiveSettlement.PopulationByCaste[caste] - ActiveSettlement.JobsByCaste[caste]).ToString()
                });
                popRow.Add(new Spacer(10, 0));
            }

            _header.Children.Clear();
            _header.Children.Add(topRow);
            _header.Children.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new IconText("ui_status_under_siege", "Under Siege", BronzeColor.Red, tooltip: "The settlement is under siege.")
                    {
                        DisplayCondition = () => ActiveSettlement.IsUnderSiege
                    },
                    new IconText("ui_status_road_connection_north", string.Empty, tooltip: "No road connection to the north.")
                    {
                        DisplayCondition = () => !ActiveSettlement.RoadConnections[(int)Facing.North]
                    },
                    new IconText("ui_status_road_connection_south", string.Empty, tooltip: "No road connection to the south.")
                    {
                        DisplayCondition = () => !ActiveSettlement.RoadConnections[(int)Facing.South]
                    },
                    new IconText("ui_status_road_connection_east", string.Empty, tooltip: "No road connection to the east.")
                    {
                        DisplayCondition = () => !ActiveSettlement.RoadConnections[(int)Facing.East]
                    },
                    new IconText("ui_status_road_connection_west", string.Empty, tooltip: "No road connection to the west.")
                    {
                        DisplayCondition = () => !ActiveSettlement.RoadConnections[(int)Facing.West]
                    }
                }
            });
            _header.Children.Add(new Spacer(0, 5));
            _header.Children.Add(new TraitDisplay(() => ActiveSettlement.Traits));
            _header.Children.Add(new BuffDisplay(() => ActiveSettlement.Buffs));
            _header.Children.Add(new Spacer(0, 5));
            _header.Children.Add(new ItemRateDisplay(
                () => _gamedataTracker.Items
                .Where(i => ActiveSettlement.ItemAvailability[i])
                .ToDictionary(i => i, i => ActiveSettlement.NetItemProduction[i])));
            _header.Children.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = popRow
            });
        }

        protected virtual void SetTabOverview()
        {
            Refresh = () => { };
            _playerData.MouseMode = null;
            _updateFunction = null;
            
            _tabBody.Children.Clear();
            _tabBody.Children.Add(
                new StackGroup
                {
                    Orientation = Orientation.Horizontal,
                    Children = new List<IUiElement>
                    {
                        new ContainerButton(
                            new NotablePersonPortrait(() => ActiveSettlement.Governor),
                            () => _dialogManager.PushModal<NotablePersonDetailsModal, NotablePerson>(ActiveSettlement.Governor),
                            disableCheck: () => ActiveSettlement.Governor == null)
                        {
                            Tooltip = new SlicedBackground(
                                new StackGroup
                                {
                                    Orientation = Orientation.Vertical,
                                    Children  = new List<IUiElement>
                                    {
                                        new Label("")
                                        {
                                            ContentUpdater = () => ActiveSettlement.Governor?.Name ?? string.Empty
                                        },
                                        UiBuilder.BuildStatDisplay(() => ActiveSettlement.Governor, NotablePersonStatType.Valor),
                                        UiBuilder.BuildStatDisplay(() => ActiveSettlement.Governor, NotablePersonStatType.Wit),
                                        UiBuilder.BuildStatDisplay(() => ActiveSettlement.Governor, NotablePersonStatType.Charisma)
                                    }
                                })
                        },
                        new Spacer(5, 0),
                        new StackGroup
                        {
                            Orientation = Orientation.Vertical,
                            Children = new List<IUiElement>
                            {
                                new TextButton(
                                    "Change",
                                    () =>
                                    {
                                        var personPickerContext = new NotablePersonPickerModalContext
                                        {
                                            Title = "Pick a Governor for " + ActiveSettlement.Name,
                                            Options = _playerData.PlayerTribe.NotablePeople
                                                        .Where(np => !np.IsDead && np.IsMature)
                                                        .ToArray(),
                                            CurrentChoice = ActiveSettlement.Governor,
                                            Callback = pick =>
                                            {
                                                pick.AssignTo(ActiveSettlement);
                                            },
                                            ValidCheck = np => null
                                        };

                                        _dialogManager.PushModal<NotablePersonPickerModal, NotablePersonPickerModalContext>(personPickerContext);
                                    }),
                                new IconText(ActiveSettlement.Race.Castes.Where(c => c.IsDefault).First().IconKey, "", tooltip: "Total settlement population.")
                                {
                                    ContentUpdater = () => ActiveSettlement.Population.Count.ToString(),
                                },
                                new IconText(UiImages.ICON_EMPIRE_AUTHORITY, "", tooltip: "The governor's ability to control the population.")
                                {
                                    ContentUpdater = () => ActiveSettlement.TotalAuthority.ToString(),
                                    Tooltip = new SlicedBackground(new Text("authority")
                                    {
                                        Width = Width.Fixed(200),
                                        ContentUpdater = () =>
                                        {
                                            string result;
                                            if(ActiveSettlement.Governor != null)
                                            {
                                                result = $"{ActiveSettlement.GovernorAuthority} from governor";
                                            }
                                            else
                                            {
                                                result = $"{ActiveSettlement.Owner.AuthorityPerSettlement} from ruler";
                                            }

                                            result += $"\n{ActiveSettlement.StructureAuthority} from structures";

                                            return result;
                                        }
                                    })
                                }
                            }
                        }
                    }
                });
            _tabBody.Children.Add(new Spacer(0, 10));
            _tabBody.Children.Add(UiBuilder.BuildPopDisplay(ActiveSettlement));
            _tabBody.Children.Add(new Spacer(0, 10));
            _tabBody.Children.Add(new TextButton("Trade Routes",
                () =>
                {
                    _dialogManager.PushModal<SettlementTradeModal, Settlement>(ActiveSettlement);
                }));
        }

        protected virtual void SetTabCivics()
        {
            Refresh = () => { };
            _playerData.MouseMode = null;
            _updateFunction = null;

            _tabBody.Children.Clear();
            _tabBody.Children.Add(new Label("Policies"));
            _tabBody.Children.Add(new Spacer(0, 10));
            _tabBody.Children.Add(new Label("Tax Rate"));
            _tabBody.Children.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new TextButton("5%",
                        () => ActiveSettlement.TaxRate = 0.05,
                        highlightCheck: () => ActiveSettlement.TaxRate == 0.05,
                        tooltip: "A low tax rate, with a low happiness penalty."),
                    new Spacer(10, 0),
                    new TextButton("10%",
                        () => ActiveSettlement.TaxRate = 0.10,
                        highlightCheck: () => ActiveSettlement.TaxRate == 0.10,
                        tooltip: "A moderate tax rate, with a moderate happiness penalty."),
                    new Spacer(10, 0),
                    new TextButton("15%",
                        () => ActiveSettlement.TaxRate = 0.15,
                        highlightCheck: () => ActiveSettlement.TaxRate == 0.15,
                        tooltip: "A high tax rate, with a high happiness penalty."),
                    new Spacer(10, 0),
                    new TextButton("20%",
                        () => ActiveSettlement.TaxRate = 0.20,
                        highlightCheck: () => ActiveSettlement.TaxRate == 0.20,
                        tooltip: "A severe tax rate, with a severe happiness penalty.")
                }
            });
            _tabBody.Children.Add(new Spacer(0, 10));
            _tabBody.Children.Add(new TextButton(
                "Abandon",
                () =>
                {
                    _dialogManager.PushModal<ConfirmModal, ConfirmModalContext>(
                        new ConfirmModalContext
                        {
                            Title = "Abandon Settlement?",
                            Body = "Abandoning the settlement will reduce any structures to ruins, and scatter the population.",
                            YesAction = () =>
                            {
                                ActiveSettlement.Owner.Abandon(ActiveSettlement);
                            }
                        });
                },
                tooltip: "Abandon this settlement?"));
        }

        protected virtual void SetTabCults()
        {
            var cultsToShow = 4;

            var cultList = new Cult[cultsToShow];
            var cultCount = new int[cultsToShow];
            var cultInfluence = new double[cultsToShow];
            var otherCount = 0;
            
            var cultGroup = new StackGroup
            {
                Orientation = Orientation.Vertical,
                Children = new List<IUiElement>
                {
                    BuildCultRow(0, cultList, cultCount, cultInfluence),
                    BuildCultRiteRow(0, cultList),
                    BuildCultRow(1, cultList, cultCount, cultInfluence),
                    BuildCultRiteRow(1, cultList),
                    BuildCultRow(2, cultList, cultCount, cultInfluence),
                    BuildCultRiteRow(2, cultList),
                    BuildCultRow(3, cultList, cultCount, cultInfluence),
                    BuildCultRiteRow(3, cultList),
                    new StackGroup
                    {
                        Orientation = Orientation.Horizontal,
                        Children = new List<IUiElement>
                        {
                            new Image("cult_none", tooltip: "Other"),
                            new Spacer(10, 0),
                            new Text(string.Empty)
                            {
                                ContentUpdater = () => otherCount.ToString()
                            }
                        }
                    }
                }
            };
            
            _updateFunction = () =>
            {
                var cultsInOrder = _gamedataTracker.Cults
                    .OrderByDescending(c => ActiveSettlement.CultMembership[c].Count)
                    .ToArray();

                otherCount = ActiveSettlement.Population.Count;
                for(var i = 0; i < cultsToShow; i++)
                {
                    var iButtonRow = i * 2;
                    var iRiteRow = i * 2 + 1;

                    if (iButtonRow < cultsInOrder.Length && ActiveSettlement.CultMembership[cultsInOrder[i]].Count > 0)
                    {
                        cultList[i] = cultsInOrder[i];
                        cultCount[i] = ActiveSettlement.CultMembership[cultsInOrder[i]].Count;
                        cultInfluence[i] = ActiveSettlement.InfluenceByCult[cultsInOrder[i]];
                        otherCount -= ActiveSettlement.CultMembership[cultsInOrder[i]].Count;

                        ((cultGroup.Children[iButtonRow] as StackGroup).Children[0] as Image).Tooltip = UiBuilder.BuildTooltipFor(
                            cultsInOrder[i],
                             ActiveSettlement.CultMembership[cultsInOrder[i]].Count / (double)ActiveSettlement.Population.Count);

                        for(var j = 0; j < cultsInOrder[i].Rites.Count; j++)
                        {
                            ((cultGroup.Children[iRiteRow] as StackGroup).Children[j] as IconButton).Tooltip 
                                = new SlicedBackground(cultsInOrder[i].Rites[j].BuildTooltip(ActiveSettlement));
                        }
                    }
                    else
                    {
                        cultList[i] = null;
                        cultCount[i] = 0;
                        cultInfluence[i] = 0;

                        ((cultGroup.Children[iButtonRow] as StackGroup).Children[0] as Image).Tooltip = null;

                        foreach (var button in (cultGroup.Children[iRiteRow] as StackGroup).Children.OfType<IconButton>())
                        {
                            button.Tooltip = null;
                        }
                    }
                }
            };
            
            Refresh = () => { };
            _playerData.MouseMode = null;

            _tabBody.Children.Clear();
            _tabBody.Children.Add(new Label("Cults"));
            _tabBody.Children.Add(new Spacer(0, 10));
            _tabBody.Children.Add(cultGroup);
            _tabBody.Children.Add(new Label("")
            {
                ContentUpdater = () =>
                {
                    if(ActiveSettlement.RiteCooldown > 0)
                    {
                        return $"Next rite in {Util.FriendlyTimeDisplay(ActiveSettlement.RiteCooldown)}";
                    }

                    return string.Empty;
                }
            });
        }

        private IUiElement BuildCultRow(int i, Cult[] cultList, int[] cultCount, double[] cultInfluence)
        {
            return new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                        {
                            new Image(string.Empty)
                            {
                                ImageUpdater = () => cultList[i]?.IconKey ?? string.Empty
                            },
                            new Spacer(10, 0),
                            new IconText(string.Empty, string.Empty, tooltip: "Members")
                            {
                                IconUpdater = () => cultCount[i] > 0 ? "ui_icon_person" : string.Empty,
                                ContentUpdater = () => cultCount[i] > 0 ? cultCount[i].ToString() : string.Empty
                            },
                            new Spacer(20, 0),
                            new IconText(string.Empty, "0")
                            {
                                IconUpdater = () => cultCount[i] > 0 ? "icon_overview_production" : string.Empty,
                                ContentUpdater = () => cultList[i] != null ? cultInfluence[i].ToString("F2") : string.Empty,
                                Tooltip = new SlicedBackground(
                                    new Text("No influence sources.")
                                    {
                                        Width = Width.Fixed(400),
                                        ContentUpdater = () =>
                                        {
                                            if(cultList[i] != null && ActiveSettlement.InfluenceSourcesByCult[cultList[i]] != null)
                                            {
                                                return "Influence:\n" + string.Join(
                                                    "\n",
                                                    ActiveSettlement.InfluenceSourcesByCult[cultList[i]]
                                                        .Select(t => $"{t.Item2.ToString("F2")} {t.Item1}"));
                                            }

                                            return "No influence sources.";
                                        }
                                    })
                            }
                        }
            };
        }

        private IUiElement BuildCultRiteRow(int i, Cult[] cultList)
        {
            var maxRiteCount = _gamedataTracker.Cults
                .Max(c => c.Rites.Count);

            var row = new StackGroup
            {
                Orientation = Orientation.Horizontal
            };

            for(var j = 0; j < maxRiteCount; j++)
            {
                var k = j;

                row.Children.Add(new IconButton(string.Empty, string.Empty,
                    () =>
                    {
                        if (cultList[i].Rites.Count > k)
                        {
                            cultList[i].Rites[k].Enact(ActiveSettlement);
                        }
                    },
                    disableCheck: () => ActiveSettlement.RiteCooldown > 0 || (cultList[i]?.Rites?.Count() ?? 0) <= k || !cultList[i].Rites[k].RequirementsMet(ActiveSettlement))
                {
                    ImageUpdater = () => cultList[i]?.Rites?.Count() > k ? cultList[i].Rites[k].IconKey : string.Empty,
                    HideCheck = () => (cultList[i]?.Rites?.Count() ?? 0) <= k,
                });
            };

            return row;
        }

        protected virtual void SetTabMilitary()
        {
            Refresh = () => { };
            _playerData.MouseMode = null;
            _updateFunction = null;
            _tabBody.Children.Clear();
            _tabBody.Children.Add(new Label("Military"));
            _tabBody.Children.Add(new Spacer(0, 10));

            _tabBody.Children.Add(new Label("Available Recruits"));
            foreach(var category in _gamedataTracker.RecruitmentSlotCategories)
            {
                if(ActiveSettlement.RecruitmentSlots.ContainsKey(category))
                {
                    _tabBody.Children.Add(new IconText(string.Empty, $"{category.Name}: {(int)ActiveSettlement.RecruitmentSlots[category]}")
                    {
                        ContentUpdater = () => $"{category.Name}: {(int)ActiveSettlement.RecruitmentSlots[category]} / {ActiveSettlement.RecruitmentSlotsMax[category]}",
                        DisplayCondition = () => ActiveSettlement.RecruitmentSlotsMax[category] > 0,
                        Tooltip = new SlicedBackground(new IconText(string.Empty, "At limit.")
                        {
                            ContentUpdater = ()=>
                            {
                                if (ActiveSettlement.RecruitmentSlots[category] < ActiveSettlement.RecruitmentSlotsMax[category])
                                {
                                    var raw = ActiveSettlement.RecruitmentSlots[category];
                                    var whole = (int)raw;
                                    var remainer = raw - whole;

                                    return $"More in {Util.FriendlyTimeDisplay((1 - remainer) * category.RegenTimeMonths)}.";
                                }

                                return $"At limit.";
                            }
                        })
                    });
                }
            }

            _tabBody.Children.Add(new TextButton(
                "Recruit Army",
                () =>
                {
                    _dialogManager.PushModal<RecruitmentModal, RecruitmentModalContext>(
                        new RecruitmentModalContext
                        {
                            Settlement = ActiveSettlement
                        });
                },
                disableCheck: () => ActiveSettlement.RecruitmentSlots.All(kvp => kvp.Value < 1)));

            _tabBody.Children.Add(new Spacer(0, 10));
        }

        protected abstract void SetTabConstruction(Caste caste);
    }
}
