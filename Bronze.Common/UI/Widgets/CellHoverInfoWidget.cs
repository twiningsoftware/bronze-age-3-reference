﻿using System.Collections.Generic;
using System.Linq;
using Bronze.Common.Data.World.Units;
using Bronze.Common.UI.Elements;
using Bronze.Common.UI.Modals;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Diplomacy;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Widgets;

namespace Bronze.Common.UI.Widgets
{
    public class CellHoverInfoWidget : UiWindowWidget, IGameplayWidget, IPreviewWidget
    {
        protected override int WidgetWidth => _contentStack.Bounds.Width;
        protected override int WidgetHeight => _contentStack.Bounds.Height;
        protected override HorizontalAlignment HorizontalAlignment => HorizontalAlignment.Left;
        protected override VerticalAlignment VerticalAlignment => VerticalAlignment.Bottom;

        private readonly IDialogManager _dialogManager;
        private readonly IPlayerDataTracker _playerData;
        private readonly IWorldManager _worldManager;
        private readonly ILosTracker _losTracker;
        private readonly IGamedataTracker _gamedataTracker;
        private readonly StackGroup _contentStack;

        private bool _needsRelayout;
        private Cell _activeCell;
        private int _activeCellStateHash;

        public override bool NeedsRelayout => _contentStack.NeedsRelayout || _needsRelayout;
        public override bool IsActive => _playerData.ActiveSettlement == null 
            && !(_playerData.MouseMode is StructurePlacementMouseMode 
                || _playerData.MouseMode is DevelopmentPlacementMouseMode
                || _playerData.MouseMode is CultRiteTargetingMouseMode);

        public CellHoverInfoWidget(
            IDialogManager dialogManager,
            IPlayerDataTracker playerData,
            IWorldManager worldManager,
            ILosTracker losTracker,
            IGamedataTracker gamedataTracker)
        {
            _dialogManager = dialogManager;
            _playerData = playerData;
            _worldManager = worldManager;
            _losTracker = losTracker;
            _gamedataTracker = gamedataTracker;

            _contentStack = new StackGroup
            {
                Orientation = Orientation.Horizontal
            };
        }

        public override void Update(
            InputState inputState,
            IAudioService audioService,
            float elapsedSeconds)
        {
            base.Update(inputState, audioService, elapsedSeconds);

            var newActiveCell = _playerData.SelectedCell ?? _playerData.HoveredCell;

            // If hovering over a UI element, keep showing the same cell infomation
            // this should prevent strobing when hovering cells and the mouse moves over the cell hover info widget
            if (newActiveCell == null)
            {
                newActiveCell = _activeCell;
            }

            var newStateHash = CalculateCellStateHash(newActiveCell);

            if (_activeCellStateHash != newStateHash)
            {
                _activeCell = newActiveCell;
                _activeCellStateHash = newStateHash;

                RebuildContent();
            }

            _contentStack.Update(inputState, audioService, elapsedSeconds);
        }

        public override void DoLayout(IDrawingService drawingService, Rectangle parentBounds)
        {
            _needsRelayout = _contentStack.NeedsRelayout;

            base.DoLayout(drawingService, parentBounds);
            _contentStack.DoLayout(drawingService, Bounds);
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            base.Draw(drawingService, layer);
            _contentStack.Draw(drawingService, layer + LAYER_STEP);
        }

        private int CalculateCellStateHash(Cell cell)
        {
            if (cell == null)
            {
                return 0;
            }

            var toHash = cell.Biome.Name;

            toHash += cell.Position.ToString();

            if (cell.Region.Owner != null)
            {
                toHash += cell.Region.Owner.Name;
            }

            if (cell.Feature != null)
            {
                toHash += cell.Feature.Name;
            }

            if (cell.Development != null)
            {
                toHash += cell.Development.Name;
                
                if (cell.Development.UnderConstruction)
                {
                    toHash += "construction";
                }
                if (cell.Development.IsUpgrading)
                {
                    toHash += "upgrading";
                }

                toHash += cell.Development.FactoryState.ToString();
            }

            if (_playerData.PlayerTribe.IsExplored(cell))
            {
                toHash += "exp";
            }
            else
            {
                toHash += "unexp";
            }

            toHash += cell.WorldActors
                .OfType<Army>()
                .Where(a => a.CurrentOrder is ArmyCampOrder aco && aco.IsCamped)
                .Count();

            return toHash.GetHashCode();
        }

        private void RebuildContent()
        {
            _needsRelayout = true;
            _contentStack.Children.Clear();

            if (_activeCell != null)
            {
                var mainColumn = new StackGroup
                {
                    Orientation = Orientation.Vertical,
                    Children = new List<IUiElement>()
                };

                _contentStack.Children = new List<IUiElement>
                {
                    new Spacer(10, 200),
                    mainColumn,
                    new Spacer(10, 200)
                };

                if (_playerData.PlayerTribe.IsExplored(_activeCell))
                {
                    BuildContentForExploredCell(mainColumn);
                }
                else
                {
                    mainColumn.Children.Add(new Label("Unexplored"));
                }
            }
        }

        private void BuildContentForExploredCell(StackGroup mainColumn)
        {
            var playerOwns = _activeCell.Region.Settlement?.Owner == _playerData.PlayerTribe;
            var playerSees = _playerData.PlayerTribe.IsVisible(_activeCell);

            mainColumn.Children = new List<IUiElement>
            {
                new Spacer(200, 10),
                new TraitDisplay(() => _activeCell?.Traits ?? Enumerable.Empty<Trait>()),
                new Label(_activeCell.Region.Name),
                new Label("")
                {
                    ContentUpdater = () =>
                    {
                        if(_activeCell.Region.Settlement != null)
                        {
                            return $"{_activeCell.Region.Settlement.Name} ({_activeCell.Region.Settlement.Owner.Name})";
                        }

                        return "Unowned";
                    }
                }
            };

            if (_activeCell.Feature != null)
            {
                mainColumn.Children.Add(new Label($"{_activeCell.Feature?.Name} ({_activeCell.Biome.Name})"));
            }
            else
            {
                mainColumn.Children.Add(new Label(_activeCell.Biome.Name));
            }

            if (_activeCell.Development != null)
            {
                mainColumn.Children.Add(UiBuilder.BuildEconomicActorHoverInfo(_gamedataTracker, _activeCell.Development, playerOwns, playerSees));
            }

            var campedArmy = _activeCell.WorldActors
                .OfType<Army>()
                .Where(a => a.CurrentOrder is ArmyCampOrder aco && aco.IsCamped)
                .FirstOrDefault();
            if(campedArmy != null && campedArmy.CurrentOrder is ArmyCampOrder campOrder)
            {
                mainColumn.Children.Add(UiBuilder.BuildEconomicActorHoverInfo(_gamedataTracker, campOrder.CampedArmy, playerOwns, playerSees));
            }
            
            mainColumn.Children.Add(new TextButton("Details",
                () =>
                {
                    _dialogManager.PushModal<CellDetailsModal, Cell>(_activeCell);
                }));

            if(_activeCell.Owner != null 
                && _activeCell.Owner != _playerData.PlayerTribe
                && _playerData.PlayerTribe.HasKnowledgeOf(_activeCell.Owner))
            {
                mainColumn.Children.Add(new TextButton("Talk",
                    () => _dialogManager.PushModal<DiplomacyModal, DiplomacyModalContext>(
                        new DiplomacyModalContext
                        {
                            OtherTribe = _activeCell.Owner,
                            InitialAction = DiploConstants.ACTION_TYPE_GREETING,
                            CloseCallback = () => { }
                        })
                    ));
            }

            mainColumn.Children.Add(new Spacer(200, 10));
        }
    }
}
