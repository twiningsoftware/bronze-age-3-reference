﻿using System.Collections.Generic;
using System.Linq;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;

namespace Bronze.Common.UI.Widgets
{
    public class RegionManagementWidget : AbstractManagementWidget, IGameplayWidget
    {
        private readonly IPlayerDataTracker _playerData;

        private Cell _hoveredCell;
        
        public RegionManagementWidget(
            IPlayerDataTracker playerData,
            IDialogManager dialogManager,
            IGamedataTracker gamedataTracker,
            IEmpireNotificationBuilder empireNotificationHandler,
            IWorldManager worldManager,
            INotificationsSystem notificationsSystem)
            :base(playerData, dialogManager, gamedataTracker, empireNotificationHandler, worldManager, notificationsSystem)
        {
            _playerData = playerData;
        }

        public override void Update(
            InputState inputState, 
            IAudioService audioService, 
            float elapsedSeconds)
        {
            base.Update(inputState, audioService, elapsedSeconds);

            if(_hoveredCell != _playerData.HoveredCell)
            {
                Refresh?.Invoke();
                _hoveredCell = _playerData.HoveredCell;
            }
        }

        public override Settlement GetActiveSettlement()
        {
            if(_playerData.ActiveSettlement == null
                && _playerData.ActiveRegion?.Settlement?.Owner == _playerData.PlayerTribe)
            {
                return _playerData.ActiveRegion.Settlement;
            }

            return null;
        }

        protected override void SetTabConstruction(Caste caste)
        {
            Refresh = () => SetTabConstruction(caste);

            _playerData.MouseMode = null;
            _tabBody.Children.Clear();
            _tabBody.Children.Add(new Label("Construction"));
            _tabBody.Children.Add(new Spacer(0, 5));
            _tabBody.Children.Add(new Spacer(0, 5));

            var developmentOptions = ActiveSettlement.Race.AvailableCellDevelopments
                .Where(d => !d.UpgradeOnly)
                .Where(d => !d.IsActivity)
                .Where(d => d.CouldBePlaced(ActiveSettlement))
                .ToArray();

            var developmentButtons = new IconButton(
                    UiImages.TileInvalid,
                    string.Empty,
                    () =>
                    {
                        SetTabDemolish();
                    },
                    tooltip: "Demolish developments.")
                .Yield()
                .Concat(developmentOptions
                    .Select(d => new IconButton(
                        d.IconKey,
                        string.Empty,
                        () =>
                        {
                            SetTabDevelopment(d);
                        },
                        tooltip: d.Name)))
                .ToArray();

            _tabBody.Children.Add(new Spacer(0, 5));

            for (var i = 0; i < developmentButtons.Length; i += 4)
            {
                _tabBody.Children.Add(new StackGroup
                {
                    Orientation = Orientation.Horizontal,
                    Children = developmentButtons
                        .Skip(i)
                        .Take(4)
                        .ToList<IUiElement>()
                });
            }
        }

        private void SetTabDevelopment(ICellDevelopmentType developmentType)
        {
            Refresh = () => SetTabDevelopment(developmentType);

            _playerData.MouseMode = new DevelopmentPlacementMouseMode(
                developmentType, 
                ActiveSettlement,
                () => SetTabConstruction(null));

            _tabBody.Children.Clear();
            _tabBody.Children.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                VerticalContentAlignment = VerticalAlignment.Middle,
                Children = new List<IUiElement>
                {
                    new Label(developmentType.Name),
                    new Spacer(10, 0),
                    new TextButton("X",
                    () =>
                    {
                        SetTabConstruction(null);
                    }),
                }
            });
            _tabBody.Children.Add(new Spacer(0, 10));
            _tabBody.Children.Add(developmentType.BuildTypeInfoPanel(250, withName: false, cell: _playerData.HoveredCell));
        }

        private void SetTabDemolish()
        {
            Refresh = () => { };

            _playerData.MouseMode = new DemolishingDevelopmentMouseMode(() => SetTabConstruction(null));

            _tabBody.Children.Clear();
            _tabBody.Children.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                VerticalContentAlignment = VerticalAlignment.Middle,
                Children = new List<IUiElement>
                {
                    new Label("Demolishing"),
                    new Spacer(10, 0),
                    new TextButton("X",
                    () =>
                    {
                        SetTabConstruction(null);
                    }),
                }
            });
            _tabBody.Children.Add(new Spacer(0, 5));

            _tabBody.Children.Add(new Text("Click to demolish a cell development any stored items will be lost.")
            {
                Width = Width.Fixed(180)
            });
        }
    }
}
