﻿using System.Collections.Generic;
using System.Linq;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Widgets;

namespace Bronze.Common.UI.Widgets
{
    public class IssueArmyOrderWidget : UiWindowWidget, IGameplayWidget
    {
        private const int MIN_WIDTH = 200;
        private const int MIN_HEIGHT = 250;

        protected override int WidgetWidth => ContentStack.Bounds.Width;
        protected override int WidgetHeight => ContentStack.Bounds.Height;
        protected override HorizontalAlignment HorizontalAlignment => HorizontalAlignment.Right;
        protected override VerticalAlignment VerticalAlignment => VerticalAlignment.Bottom;
        protected StackGroup ContentStack { get; private set; }
        
        public override bool NeedsRelayout => ContentStack.NeedsRelayout || _needsRelayout;

        public override bool IsActive => _playerData.MouseMode is ArmyOrderMouseMode;

        private readonly IPlayerDataTracker _playerData;
        private bool _needsRelayout;
        private Cell _hoveredCell;
        
        public IssueArmyOrderWidget(
            IPlayerDataTracker playerData)
        {
            _playerData = playerData;
            ContentStack = new StackGroup
            {
                Orientation = Orientation.Horizontal
            };
        }

        public override void Update(InputState inputState, IAudioService audioService, float elapsedSeconds)
        {
            base.Update(inputState, audioService, elapsedSeconds);

            if(_hoveredCell != _playerData.HoveredCell)
            {
                _hoveredCell = _playerData.HoveredCell;

                RebuildContent();
            }
        }

        public override void DoLayout(IDrawingService drawingService, Rectangle parentBounds)
        {
            _needsRelayout = ContentStack.NeedsRelayout;

            base.DoLayout(drawingService, parentBounds);

            ContentStack.DoLayout(drawingService, Bounds);
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            base.Draw(drawingService, layer);
            ContentStack.Draw(drawingService, layer + LAYER_STEP);
        }

        private void RebuildContent()
        {
            var orderMouseMode = _playerData.MouseMode as ArmyOrderMouseMode;
            
            _needsRelayout = true;
            ContentStack.Children.Clear();

            if(orderMouseMode == null)
            {
                return;
            }
            
            var contents = new List<IUiElement>
            {
                new Spacer(MIN_WIDTH, 10),
                new StackGroup
                {
                    Orientation = Orientation.Horizontal,
                    VerticalContentAlignment = VerticalAlignment.Middle,
                    Width = Width.Fixed(MIN_WIDTH),
                    Children = new List<IUiElement>
                    {
                        new Label(orderMouseMode.ArmyAction.Name),
                        new Spacer(20, 0),
                        new TextButton("X",
                        () =>
                        {
                            _playerData.MouseMode = null;
                        }),
                    }
                },
                new Spacer(0, 10),
                new Text(orderMouseMode.ArmyAction.Description),
                new Spacer(0, 10),
                orderMouseMode.ArmyAction.BuildDescriptionStack()
            };

            var messages = orderMouseMode.ArmyAction.GetValidationMessagesFor(orderMouseMode.Army, orderMouseMode.Target);
            if (messages.Any())
            {
                contents.Add(new Spacer(0, 10));
                contents.AddRange(messages
                    .Select(s => new Text(s, BronzeColor.Yellow)));
            }

            contents.Add(new Spacer(0, 10));

            ContentStack.Children.Add(new Spacer(5, MIN_HEIGHT));
            ContentStack.Children.Add(new StackGroup
            {
                Orientation = Orientation.Vertical,
                Width = Width.Fixed(MIN_WIDTH),
                Children = contents
            });
            ContentStack.Children.Add(new Spacer(5, MIN_HEIGHT));
        }
    }
}
