﻿using System.Linq;
using System.Numerics;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Widgets;

namespace Bronze.Common.UI.Widgets
{
    public class CellActivitiesPreviewWidget : UiWindowWidget, IGameplayWidget
    {
        protected override int WidgetWidth => 0;
        protected override int WidgetHeight => 0;
        protected override HorizontalAlignment HorizontalAlignment => HorizontalAlignment.Left;
        protected override VerticalAlignment VerticalAlignment => VerticalAlignment.Top;
        
        private readonly IPlayerDataTracker _playerData;
        private readonly IGameWorldDrawer _gameWorldDrawer;
        private readonly Border _outerBorder;
        private readonly StackGroup _contentStack;
        
        private Cell _activeCell;
        private int _activeCellStateHash;

        public override bool NeedsRelayout => true;
        public override bool IsActive => _playerData.ActiveSettlement == null 
            && ! (_playerData.MouseMode is DevelopmentPlacementMouseMode) 
            && _playerData.HoveredCell != null
            && _playerData.HoveredCell != _playerData.SelectedCell
            && _playerData.HoveredCell.Region?.Owner == _playerData.PlayerTribe;

        public CellActivitiesPreviewWidget(
            IPlayerDataTracker playerData,
            IGameWorldDrawer gameWorldDrawer)
        {
            _playerData = playerData;
            _gameWorldDrawer = gameWorldDrawer;

            _contentStack = new StackGroup
            {
                Orientation = Orientation.Horizontal
            };
            _outerBorder = new Border(_contentStack, margin: 3);
        }

        public override void Update(
            InputState inputState,
            IAudioService audioService,
            float elapsedSeconds)
        {
            base.Update(inputState, audioService, elapsedSeconds);

            var newActiveCell = _playerData.HoveredCell;
            
            var newStateHash = CalculateCellStateHash(newActiveCell);

            if (_activeCellStateHash != newStateHash)
            {
                _activeCell = newActiveCell;
                _activeCellStateHash = newStateHash;

                RebuildContent();
            }

            _outerBorder.Update(inputState, audioService, elapsedSeconds);
        }

        public override void DoLayout(IDrawingService drawingService, Rectangle parentBounds)
        {
            var x = 0;
            var y = 0;
            
            if (_playerData.HoveredCell != null)
            {
                Vector2 cellPos;
                float toEdgeOffset;

                if(_playerData.ViewMode == ViewMode.Detailed)
                {
                    cellPos = _gameWorldDrawer.TransformToScreen(_playerData.HoveredCell.Position.ToVector() * Constants.CELL_SIZE_PIXELS);
                    toEdgeOffset = Constants.CELL_SIZE_PIXELS * 0.5f;
                }
                else
                {
                    cellPos = _gameWorldDrawer.TransformToScreen(_playerData.HoveredCell.Position.ToVector() * Constants.CELL_SIZE_PIXELS);
                    toEdgeOffset = Constants.CELL_SIZE_SUMMARY_PIXELS * 1.5f;
                }
                
                var viewDelta = cellPos + new Vector2(-toEdgeOffset, -toEdgeOffset);
                
                x = (int)viewDelta.X;
                y = (int)viewDelta.Y;
            }

            parentBounds = new Rectangle(
                parentBounds.X + x,
                parentBounds.Y + y,
                parentBounds.Width,
                parentBounds.Height);
            
            base.DoLayout(drawingService, parentBounds);
            _outerBorder.DoLayout(drawingService, Bounds);
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            // Don't call base, so we don't draw the border
            if (_contentStack.Children.Any())
            {
                _outerBorder.Draw(drawingService, layer + LAYER_STEP);
            }
        }

        private int CalculateCellStateHash(Cell cell)
        {
            if (cell == null)
            {
                return 0;
            }

            var toHash = cell.Biome.Name;

            toHash += cell.Position.ToString();

            if (cell.Region.Owner != null)
            {
                toHash += cell.Region.Owner.Name;
            }

            if (cell.Feature != null)
            {
                toHash += cell.Feature.Name;
            }

            if (cell.Development != null)
            {
                toHash += cell.Development.Name;

                if (cell.Development.UnderConstruction)
                {
                    toHash += "construction";
                }
            }

            return toHash.GetHashCode();
        }

        private void RebuildContent()
        {
            _contentStack.Children.Clear();
            
            if (_activeCell != null)
            {
                var activityOptions = (_activeCell.Region.Settlement?.Race?.AvailableCellDevelopments ?? Enumerable.Empty<ICellDevelopmentType>())
                    .Where(d => d.IsActivity)
                    .Where(d => !d.UpgradeOnly)
                    .Where(d => d.CanBePlaced(_playerData.PlayerTribe, _activeCell))
                    .ToArray();

                _contentStack.Children
                    .AddRange(activityOptions
                        .Select(d => new Image(d.IconKey)));
            }
        }
    }
}
