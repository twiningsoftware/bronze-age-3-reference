﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bronze.Common.Data.World.CellDevelopments;
using Bronze.Common.UI.Elements;
using Bronze.Common.UI.Modals;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Widgets;

namespace Bronze.Common.UI.Widgets
{
    public class EmpireOverviewWidget : UiWindowWidget, IGameplayWidget, IPreviewWidget
    {
        protected override int WidgetWidth => _contentStack.Bounds.Width;
        protected override int WidgetHeight => _contentStack.Bounds.Height;
        protected override HorizontalAlignment HorizontalAlignment => HorizontalAlignment.Left;
        protected override VerticalAlignment VerticalAlignment => VerticalAlignment.Top;

        private readonly IDialogManager _dialogManager;
        private readonly IPlayerDataTracker _playerData;
        private readonly IWorldManager _worldManager;
        private readonly IUserSettings _userSettings;
        private readonly StackGroup _contentStack;
        private bool _needsRelayout;

        public override bool NeedsRelayout => _contentStack.NeedsRelayout || _needsRelayout;
        public override bool IsActive => _playerData.PlayerTribe != null;

        public EmpireOverviewWidget(
            IDialogManager dialogManager,
            IPlayerDataTracker playerData,
            IWorldManager worldManager,
            IUserSettings userSettings)
        {
            _dialogManager = dialogManager;
            _playerData = playerData;
            _worldManager = worldManager;
            _userSettings = userSettings;
            _needsRelayout = false;
            
            _contentStack = new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new Spacer(10, 0),
                    new StackGroup
                    {
                        Orientation = Orientation.Vertical,
                        Children = new List<IUiElement>
                        {
                            new Spacer(0, 10),
                            new Label("")
                            {
                                ContentUpdater = () => _playerData.PlayerTribe?.Name ?? string.Empty
                            },
                            new TraitDisplay(() => _playerData.PlayerTribe?.Traits ?? Enumerable.Empty<Trait>()),
                            new BuffDisplay(() => _playerData.PlayerTribe?.Buffs ?? Enumerable.Empty<Buff>()),
                            new Spacer(0, 5),
                            new StackGroup
                            {
                                Orientation = Orientation.Horizontal,
                                Children = new List<IUiElement>
                                {
                                    new ContainerButton(
                                        new NotablePersonPortrait(() => _playerData.PlayerTribe.Ruler),
                                        () =>
                                        {
                                            _dialogManager.PushModal<NotablePersonDetailsModal, NotablePerson>(_playerData.PlayerTribe.Ruler);
                                        })
                                    {
                                        Tooltip = new SlicedBackground(
                                            new StackGroup
                                            {
                                                Orientation = Orientation.Vertical,
                                                Children  = new List<IUiElement>
                                                {
                                                    new Label("")
                                                    {
                                                        ContentUpdater = () => _playerData.PlayerTribe?.Ruler?.Name ?? string.Empty
                                                    },
                                                    UiBuilder.BuildStatDisplay(() => _playerData.PlayerTribe.Ruler, NotablePersonStatType.Valor),
                                                    UiBuilder.BuildStatDisplay(() => _playerData.PlayerTribe.Ruler, NotablePersonStatType.Wit),
                                                    UiBuilder.BuildStatDisplay(() => _playerData.PlayerTribe.Ruler, NotablePersonStatType.Charisma)
                                                }
                                            })
                                    },
                                    new Spacer(10, 0),
                                    UiBuilder.BuildMinimapManagementButtons(_dialogManager, _playerData.PlayerTribe),
                                }
                            },
                            new Spacer(0, 5),
                            new TraitDisplay(() => _playerData.PlayerTribe?.Traits ?? Enumerable.Empty<Trait>()),
                            new Spacer(0, 5),
                            new StackGroup
                            {
                                Orientation = Orientation.Horizontal,
                                Children = new List<IUiElement>
                                {
                                    new IconText(UiImages.ICON_EMPIRE_SETTLEMENTS, "", tooltip: "How many settlements you control.")
                                    {
                                        ContentUpdater = () => _playerData.PlayerTribe.Settlements.Count.ToString()
                                    },
                                    new Spacer(10, 0),
                                    new IconText(UiImages.ICON_EMPIRE_AUTHORITY, "", tooltip: "Your ability to control your population. This is divided up amongst your settlements without governors.")
                                    {
                                        ContentUpdater = () => _playerData.PlayerTribe.Authority.ToString()
                                    },
                                    new Spacer(10, 0),
                                    new IconText(_playerData.PlayerTribe.Race.Castes.Where(c => c.IsDefault).First().IconKey, "", tooltip: "Your total population.")
                                    {
                                        ContentUpdater = () => _playerData.PlayerTribe.Settlements.Sum(s=>s.Population.Count).ToString()
                                    },
                                }
                            },
                            new Spacer(0, 5),
                            new StackGroup
                            {
                                Orientation = Orientation.Horizontal,
                                Children = new List<IUiElement>
                                {
                                    new IconButton(
                                        "ui_button_outline_none",
                                        string.Empty,
                                        () => _userSettings.OutlinerMode = OutlinerMode.None,
                                        highlightCheck: () => _userSettings.OutlinerMode == OutlinerMode.None,
                                        tooltip: "Hide outliner."),
                                    new Spacer(5, 0),
                                    new IconButton(
                                        "ui_button_outline_settlements",
                                        string.Empty,
                                        () => _userSettings.OutlinerMode = OutlinerMode.Settlement,
                                        highlightCheck: () => _userSettings.OutlinerMode == OutlinerMode.Settlement,
                                        tooltip: "List settlements."),

                                    new Spacer(5, 0),
                                    new IconButton(
                                        "ui_button_outline_armies",
                                        string.Empty,
                                        () => _userSettings.OutlinerMode = OutlinerMode.Army,
                                        highlightCheck: () => _userSettings.OutlinerMode == OutlinerMode.Army,
                                        tooltip: "List armies."),
                                }
                            },
                            new Spacer(0, 10),
                        }
                    },
                    new Spacer(10, 0)
                }
            };
        }

        
        
        public override void Update(
            InputState inputState,
            IAudioService audioService,
            float elapsedSeconds)
        {
            base.Update(inputState, audioService, elapsedSeconds);
            
            _contentStack.Update(inputState, audioService, elapsedSeconds);
        }
        
        public override void DoLayout(IDrawingService drawingService, Rectangle parentBounds)
        {
            parentBounds = new Rectangle(
                parentBounds.X,
                parentBounds.Y + 36,
                parentBounds.Width,
                parentBounds.Height);

            _needsRelayout = _contentStack.NeedsRelayout;
            
            base.DoLayout(drawingService, parentBounds);

            _contentStack.DoLayout(drawingService, Bounds);
            Bounds = _contentStack.Bounds;
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            base.Draw(drawingService, layer);
            _contentStack.Draw(drawingService, layer + LAYER_STEP);
        }
    }
}
