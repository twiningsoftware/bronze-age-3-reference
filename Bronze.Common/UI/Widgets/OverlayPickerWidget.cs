﻿using System.Collections.Generic;
using System.Linq;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Widgets;

namespace Bronze.Common.UI.Widgets
{
    public class OverlayPickerWidget : UiWindowWidget, IGameplayWidget
    {
        protected override int WidgetWidth => _contentStack.Bounds.Width;
        protected override int WidgetHeight => _contentStack.Bounds.Height;
        protected override HorizontalAlignment HorizontalAlignment => HorizontalAlignment.Left;
        protected override VerticalAlignment VerticalAlignment => VerticalAlignment.Top;
        
        private readonly IUserSettings _userSettings;
        private readonly IOverlay[] _overlays;
        private readonly StackGroup _contentStack;

        private bool _needsRelayout;

        public override bool NeedsRelayout => _contentStack.NeedsRelayout || _needsRelayout;

        public override bool IsActive => _userSettings.ShowOverlaysMenu;

        public OverlayPickerWidget(
            IUserSettings userSettings,
            IEnumerable<IOverlay> overlays)
        {
            _userSettings = userSettings;
            _overlays = overlays.ToArray();
            
            _needsRelayout = false;

            _contentStack = new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new Spacer(10, 0),
                    new StackGroup
                    {
                        Orientation = Orientation.Vertical,
                        Children = new Spacer(0, 10).Yield()
                            .OfType<IUiElement>()
                            .Concat(_overlays
                                .Select(x=> new TextButton(
                                    x.Name,
                                    () => x.IsActive = !x.IsActive,
                                    highlightCheck: () => x.IsActive)))
                            .Concat(new Spacer(0, 10).Yield())
                            .ToList()
                    },
                    new Spacer(10, 0)
                }
            };
        }

        public override void Update(
            InputState inputState,
            IAudioService audioService,
            float elapsedSeconds)
        {
            base.Update(inputState, audioService, elapsedSeconds);
            
            _contentStack.Update(inputState, audioService, elapsedSeconds);
        }
        
        public override void DoLayout(IDrawingService drawingService, Rectangle parentBounds)
        {
            parentBounds = new Rectangle(
                parentBounds.X + 157,
                parentBounds.Y + 36,
                parentBounds.Width,
                parentBounds.Height);

            _needsRelayout = _contentStack.NeedsRelayout;
            
            base.DoLayout(drawingService, parentBounds);

            _contentStack.DoLayout(drawingService, Bounds);
            Bounds = _contentStack.Bounds;
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            base.Draw(drawingService, layer);
            _contentStack.Draw(drawingService, layer + LAYER_STEP);
        }
    }
}
