﻿using System.Collections.Generic;
using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Widgets;

namespace Bronze.Common.UI.Widgets
{
    public class PlaneSwitcherWidget : UiWindowWidget, IPreviewWidget, IGameplayWidget
    {
        protected override int WidgetWidth => _contentStack.Bounds.Width;
        protected override int WidgetHeight => _contentStack.Bounds.Height;
        protected override HorizontalAlignment HorizontalAlignment => HorizontalAlignment.Right;
        protected override VerticalAlignment VerticalAlignment => VerticalAlignment.Top;
        
        private readonly IPlayerDataTracker _playerData;
        private readonly IWorldManager _worldManager;
        private readonly StackGroup _contentStack;
        
        public override bool NeedsRelayout => true;
        public override bool IsActive => _playerData.ActiveSettlement == null;

        public PlaneSwitcherWidget(
            IPlayerDataTracker playerData,
            IWorldManager worldManager)
        {
            _playerData = playerData;
            _worldManager = worldManager;

            _contentStack = new StackGroup
            {
                Orientation = Orientation.Vertical,
                Children = new List<IUiElement>
                {
                    new Spacer(0,5),
                    new StackGroup
                    {
                        Orientation = Orientation.Horizontal,
                        VerticalContentAlignment = VerticalAlignment.Middle,
                        Children = new List<IUiElement>
                        {
                            new Spacer(10,0),
                            new TextButton("<",
                                () =>
                                {
                                    _playerData.CurrentPlane -= 1;
                                },
                                disableCheck: () => _worldManager.GetPlane(_playerData.CurrentPlane - 1) == null),
                            new Spacer(5, 0),
                            new Label("")
                            {
                                ContentUpdater = () => _worldManager.GetPlane(_playerData.CurrentPlane)?.Name ?? string.Empty
                            },
                            new Spacer(5, 0),
                            new TextButton(">",
                                () =>
                                {
                                    _playerData.CurrentPlane += 1;
                                },
                                disableCheck: () => _worldManager.GetPlane(_playerData.CurrentPlane + 1) == null),
                            new Spacer(10, 0),
                        }
                    },
                    new Spacer(0,5),
                }
            };
        }

        public override void Update(
            InputState inputState,
            IAudioService audioService,
            float elapsedSeconds)
        {
            base.Update(inputState, audioService, elapsedSeconds);
            
            _contentStack.Update(inputState, audioService, elapsedSeconds);
        }
        
        public override void DoLayout(IDrawingService drawingService, Rectangle parentBounds)
        {
            parentBounds = new Rectangle(
                parentBounds.X - 300,
                parentBounds.Y,
                parentBounds.Width,
                parentBounds.Height);

            base.DoLayout(drawingService, parentBounds);

            _contentStack.DoLayout(drawingService, Bounds);

            Bounds = _contentStack.Bounds;
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            base.Draw(drawingService, layer);
            _contentStack.Draw(drawingService, layer + LAYER_STEP);
        }
    }
}
