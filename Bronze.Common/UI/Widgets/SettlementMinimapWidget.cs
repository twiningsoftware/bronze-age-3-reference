﻿using System.Collections.Generic;
using System.Linq;
using Bronze.Common.Gameplay.UiElements;
using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Widgets;

namespace Bronze.Common.UI.Widgets
{
    public class SettlementMinimapWidget : UiWindowWidget, IGameplayWidget, IPreviewWidget
    {
        protected override int WidgetWidth => _contentStack.Bounds.Width;
        protected override int WidgetHeight => _contentStack.Bounds.Height;
        protected override HorizontalAlignment HorizontalAlignment => HorizontalAlignment.Right;
        protected override VerticalAlignment VerticalAlignment => VerticalAlignment.Top;
        
        private readonly IPlayerDataTracker _playerData;
        private readonly IDialogManager _dialogManager;
        private readonly StackGroup _contentStack;

        private bool _needsRelayout;

        private bool _showMinimap;

        public override bool NeedsRelayout => _contentStack.NeedsRelayout || _needsRelayout;

        public override bool IsActive => _playerData.ViewMode == ViewMode.Settlement;

        public SettlementMinimapWidget(
            IPlayerDataTracker playerData,
            IDialogManager dialogManager)
        {
            _playerData = playerData;
            _dialogManager = dialogManager;
            _needsRelayout = false;
            _showMinimap = true;

            _contentStack = new StackGroup
            {
                Orientation = Orientation.Vertical,
                Children = new List<IUiElement>
                {
                    new Spacer(0, 5),
                    new StackGroup
                    {
                        VerticalContentAlignment = VerticalAlignment.Middle,
                        Orientation = Orientation.Horizontal,
                        Children = new List<IUiElement>
                        {
                            new Spacer(10, 0),
                            new ToLeftSizePadder(new Label(string.Empty)
                                {
                                    ContentUpdater = () => _playerData.ActiveSettlement?.Name
                                },
                                minWidth: 150),
                            new TextButton("Leave",
                                () =>
                                {
                                    _playerData.LookAt(_playerData.ActiveSettlement.Districts.First().Cell.Position);
                                    _playerData.ViewMode = ViewMode.Detailed;
                                    _playerData.ActiveSettlement = null;
                                }),
                            new IconButton(
                                "ui_button_minimap",
                                string.Empty,
                                () =>
                                {
                                    _showMinimap = !_showMinimap;
                                    _needsRelayout = true;
                                },
                                highlightCheck: () => _showMinimap),
                            new Spacer(5, 0),
                        }
                    },
                    new Spacer(250, 0),
                    new StackGroup
                    {
                        Orientation = Orientation.Horizontal,
                        Children = new List<IUiElement>
                        {
                            new Spacer(5, 0),
                            new SettlementMinimapElement(_playerData, _dialogManager)
                            {
                                Width = Width.Fixed(240),
                                Height = Height.Fixed(240),
                                HideCondition = () => !_showMinimap
                            },
                            new Spacer(5, 0),
                        },
                    },
                    new Spacer(0, 5),
                }
            };
        }

        public override void Update(
            InputState inputState,
            IAudioService audioService,
            float elapsedSeconds)
        {
            base.Update(inputState, audioService, elapsedSeconds);
            
            _contentStack.Update(inputState, audioService, elapsedSeconds);
        }
        
        public override void DoLayout(IDrawingService drawingService, Rectangle parentBounds)
        {
            parentBounds = new Rectangle(
                parentBounds.X,
                parentBounds.Y + 36,
                parentBounds.Width,
                parentBounds.Height);

            _needsRelayout = _contentStack.NeedsRelayout;
            
            base.DoLayout(drawingService, parentBounds);

            _contentStack.DoLayout(drawingService, Bounds);
            Bounds = _contentStack.Bounds;
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            base.Draw(drawingService, layer);
            _contentStack.Draw(drawingService, layer + LAYER_STEP);
        }
    }
}
