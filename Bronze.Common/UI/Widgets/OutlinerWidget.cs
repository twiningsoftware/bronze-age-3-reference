﻿using System.Collections.Generic;
using System.Linq;
using Bronze.Common.Data.World.CellDevelopments;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Widgets;

namespace Bronze.Common.UI.Widgets
{
    public class OutlinerWidget : UiWindowWidget, IGameplayWidget, IPreviewWidget
    {
        protected override int WidgetWidth => _contentStack.Bounds.Width;
        protected override int WidgetHeight => _contentStack.Bounds.Height;
        protected override HorizontalAlignment HorizontalAlignment => HorizontalAlignment.Left;
        protected override VerticalAlignment VerticalAlignment => VerticalAlignment.Top;

        private readonly IPlayerDataTracker _playerData;
        private readonly IUserSettings _userSettings;
        private readonly StackGroup _contentStack;

        private bool _needsRelayout;
        private OutlinerMode _outlinerMode;
        private int _elementCount;

        public override bool NeedsRelayout => _contentStack.NeedsRelayout || _needsRelayout;
        public override bool IsActive => _userSettings.OutlinerMode != OutlinerMode.None;

        public OutlinerWidget(
            IPlayerDataTracker playerData,
            IUserSettings userSettings)
        {
            _playerData = playerData;
            _userSettings = userSettings;
            _needsRelayout = false;

            _contentStack = new StackGroup
            {
                Orientation = Orientation.Horizontal,
            };
        }

        public override void Update(
            InputState inputState,
            IAudioService audioService,
            float elapsedSeconds)
        {
            base.Update(inputState, audioService, elapsedSeconds);

            if (_outlinerMode != _userSettings.OutlinerMode
                || (_outlinerMode == OutlinerMode.Army && _elementCount != _playerData.PlayerTribe.Armies.Count)
                || (_outlinerMode == OutlinerMode.Settlement && _elementCount != _playerData.PlayerTribe.Settlements.Count))
            {
                RebuildContent();
            }

            _contentStack.Update(inputState, audioService, elapsedSeconds);
        }

        public override void DoLayout(IDrawingService drawingService, Rectangle parentBounds)
        {
            parentBounds = new Rectangle(
                parentBounds.X,
                parentBounds.Y + 275,
                parentBounds.Width,
                parentBounds.Height);

            _needsRelayout = _contentStack.NeedsRelayout;

            base.DoLayout(drawingService, parentBounds);

            _contentStack.DoLayout(drawingService, Bounds);
            Bounds = _contentStack.Bounds;
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            base.Draw(drawingService, layer);
            _contentStack.Draw(drawingService, layer + LAYER_STEP);
        }

        private void RebuildContent()
        {
            _needsRelayout = true;
            _outlinerMode = _userSettings.OutlinerMode;
            _contentStack.Children.Clear();

            if (_outlinerMode != OutlinerMode.None)
            {
                IUiElement table;

                if (_outlinerMode == OutlinerMode.Army)
                {
                    _elementCount = _playerData.PlayerTribe.Armies.Count;

                    table = new ScrollableList(
                        10,
                        _playerData.PlayerTribe.Armies.Select(a => BuildRow(a)).ToArray());
                }
                else
                {
                    _elementCount = _playerData.PlayerTribe.Settlements.Count;

                    table = new ScrollableList(
                        10,
                        _playerData.PlayerTribe.Settlements.Select(s => BuildRow(s)).ToArray());
                }

                _contentStack.Children = new List<IUiElement>
                {
                    new Spacer(10, 50),
                    new StackGroup
                    {
                        Orientation = Orientation.Vertical,
                        Children = new List<IUiElement>
                        {
                            new Spacer(170, 10),
                            table,
                            new Spacer(0, 10)
                        }
                    },
                    new Spacer(10, 0)
                };
            }
        }

        private IUiElement BuildRow(Army a)
        {
            return new StackGroup
            {
                Orientation = Orientation.Vertical,
                Children = new List<IUiElement>
                {
                    new StackGroup
                    {
                        Orientation = Orientation.Horizontal,
                        Children = new List<IUiElement>
                        {
                            new SizePadder(new Label(a.Name), minWidth: 150)
                            {
                                HorizontalAlignment = HorizontalAlignment.Left
                            },
                            new Spacer(10, 0),
                            new IconButton(
                                UiImages.IconView,
                                string.Empty,
                                () =>
                                {
                                    _playerData.LookAt(a.Cell.Position);
                                },
                                tooltip: "View the army.")
                        }
                    },
                    new StackGroup
                    {
                        Orientation = Orientation.Horizontal,
                        Children = new List<IUiElement>
                        {
                            new IconText("ui_stat_melee", a.StrengthEstimate.ToString("F0"), tooltip: "Total strength.")
                            {
                                ContentUpdater = () => a.StrengthEstimate.ToString("F0")
                            },
                            new Image("ui_status_battle", "In battle")
                            {
                                VisibilityCheck = () => a.SkirmishingWith.Any()
                            }
                        }
                    }
                }
            };
        }

        private IUiElement BuildRow(Settlement s)
        {
            return new StackGroup
            {
                Orientation = Orientation.Vertical,
                Children = new List<IUiElement>
                {
                    new StackGroup
                    {
                        Orientation = Orientation.Horizontal,
                        Children = new List<IUiElement>
                        {
                            new SizePadder(new Label(s.Name), minWidth: 150)
                            {
                                HorizontalAlignment = HorizontalAlignment.Left
                            },
                            new Spacer(10, 0),
                            new IconButton(
                                UiImages.IconView,
                                string.Empty,
                                () =>
                                {
                                    _playerData.LookAt(s.EconomicActors.Where(ea => ea is DistrictDevelopment || ea is VillageDevelopment).OfType<ICellDevelopment>().FirstOrDefault()?.Cell?.Position ?? s.Region.Center);
                                },
                                tooltip: "View the settlement.")
                        }
                    },
                    new StackGroup
                    {
                        Orientation = Orientation.Horizontal,
                        Children = new List<IUiElement>
                        {
                            new IconText(s.Race.Castes.First(c => c.IsDefault).IconKey, s.Population.Count.ToString(), tooltip: "Total population.")
                            {
                                ContentUpdater = () => s.Population.Count.ToString()
                            },
                            new Spacer(5, 0),
                            new IconText(UiImages.TINY_POP_HAPPINESS, 1.ToString("P0"), tooltip: "Average satisfaction.")
                            {
                                ContentUpdater = () => s.AverageSatisfaction.ToString("P0")
                            },
                            new Image("ui_status_battle", "Under siege")
                            {
                                VisibilityCheck = () => s.IsUnderSiege
                            }
                        }
                    }
                }
            };
        }
    }
}
