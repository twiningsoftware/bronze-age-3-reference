﻿using System.Linq;
using System.Numerics;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Widgets;

namespace Bronze.Common.UI.Widgets
{
    public class StructureRecipieSwitchWidget : UiWindowWidget, IGameplayWidget
    {
        protected override int WidgetWidth => _contentStack.Bounds.Width;
        protected override int WidgetHeight => _contentStack.Bounds.Height;
        protected override HorizontalAlignment HorizontalAlignment => HorizontalAlignment.Left;
        protected override VerticalAlignment VerticalAlignment => VerticalAlignment.Top;
        
        private readonly IPlayerDataTracker _playerData;
        private readonly IGameWorldDrawer _gameWorldDrawer;
        private readonly StackGroup _contentStack;
        
        private IStructure _activeStructure;
        private int _structureStateHash;
        private bool _hovered;

        public override bool NeedsRelayout => true;
        public override bool IsActive => _playerData.ViewMode == ViewMode.Settlement
            && !(_playerData.MouseMode is StructurePlacementMouseMode)
            && !(_playerData.MouseMode is DemolishingStructureMouseMode);

        public StructureRecipieSwitchWidget(
            IPlayerDataTracker playerData,
            IGameWorldDrawer gameWorldDrawer)
        {
            _playerData = playerData;
            _gameWorldDrawer = gameWorldDrawer;
            
            _contentStack = new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Margin = 5
            };
        }

        public override void Update(
            InputState inputState,
            IAudioService audioService,
            float elapsedSeconds)
        {
            base.Update(inputState, audioService, elapsedSeconds);
            
            _hovered = Bounds.Contains(inputState.MousePosition) || _hovered && inputState.MouseOverWidget;
            
            var newActiveStructure = GetActiveStructure();
            
            var newStateHash = CalculateCellStateHash(newActiveStructure);

            if (_structureStateHash != newStateHash)
            {
                _activeStructure = newActiveStructure;
                _structureStateHash = newStateHash;

                RebuildContent();
            }
            
            _contentStack.Update(inputState, audioService, elapsedSeconds);
        }

        public override void DoLayout(IDrawingService drawingService, Rectangle parentBounds)
        {
            var x = 0;
            var y = 0;
            
            if (_activeStructure != null)
            {
                var ulPos = _gameWorldDrawer.TransformToScreen(_activeStructure.UlTile.Position.ToVector() * Constants.TILE_SIZE_PIXELS);
                
                var viewDelta = ulPos 
                    + new Vector2(
                        -Constants.TILE_SIZE_PIXELS / 2,
                        (_activeStructure.TilesHigh + -0.5f) * Constants.TILE_SIZE_PIXELS - WidgetHeight);
                
                x = (int)viewDelta.X;
                y = (int)viewDelta.Y;
            }

            parentBounds = new Rectangle(
                parentBounds.X + x,
                parentBounds.Y + y,
                parentBounds.Width,
                parentBounds.Height);
            
            base.DoLayout(drawingService, parentBounds);
            _contentStack.DoLayout(drawingService, Bounds);
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            if(_contentStack.Children.Any() && Bounds.Width > 0)
            {
                base.Draw(drawingService, layer);
                _contentStack.Draw(drawingService, layer + LAYER_STEP);
            }
        }

        private IStructure GetActiveStructure()
        {
            return _playerData.SelectedStructure ?? _playerData.HoveredTile?.Structure ?? _activeStructure;
        }

        private int CalculateCellStateHash(IStructure structure)
        {
            if (structure == null)
            {
                return 0;
            }

            var toHash = string.Empty;

            toHash += structure.UlTile.Position.ToString();
            toHash += structure.TypeId;
            
            if (structure.UnderConstruction)
            {
                toHash += "construction";
            }
            if(structure.IsUpgrading)
            {
                toHash += "upgrading";
            }
                
            toHash += structure.ActiveRecipie?.Name ?? string.Empty;
            
            if(structure == _playerData.SelectedStructure)
            {
                toHash += "selected";
            }

            if(_hovered)
            {
                toHash += "hovered";
            }

            return toHash.GetHashCode();
        }

        private void RebuildContent()
        {
            _contentStack.Children.Clear();
            
            if (_activeStructure != null)
            {
                var validRecipies = _activeStructure.AvailableRecipies
                    .Where(r => r.ShowToPlayer(_activeStructure))
                    .ToArray();

                if((validRecipies.Length > 1 || _activeStructure.ActiveRecipie == null)
                    && validRecipies.Any())
                { 
                    if(!_hovered
                        && _activeStructure != _playerData.SelectedStructure
                        && _activeStructure.ActiveRecipie != null)
                    {
                        _contentStack.Children.Add(
                            new Image(_activeStructure.ActiveRecipie.IconKey));
                    }
                    else if(_hovered || _activeStructure == _playerData.SelectedStructure)
                    {
                        _contentStack.Children.Add(
                            UiBuilder.BuildRecipieButtonRow(validRecipies, _activeStructure));
                    }
                }
            }
        }
    }
}
