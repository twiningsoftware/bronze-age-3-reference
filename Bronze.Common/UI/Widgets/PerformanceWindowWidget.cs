﻿using System.Collections.Generic;
using System.Linq;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Widgets;

namespace Bronze.Common.UI.Widgets
{
    public class PerformanceWindowWidget : UiWindowWidget, IGameplayWidget, IPreviewWidget
    {
        private const int MARGIN = 20;

        protected override int WidgetWidth => _outerStack.Bounds.Width;
        protected override int WidgetHeight => _outerStack.Bounds.Height;
        protected override HorizontalAlignment HorizontalAlignment => HorizontalAlignment.Left;
        protected override VerticalAlignment VerticalAlignment => VerticalAlignment.Top;

        private readonly IPerformanceTracker _performanceTracker;
        private readonly StackGroup _outerStack;
        private readonly IEnumerable<AbstractUiElement> _contents;
        private readonly Text[] _scriptPerformanceLabels;
        private readonly Text[] _scriptPerformanceValues;
        private readonly Text[] _updatePerformanceLabels;
        private readonly Text[] _updatePerformanceValues;
        private readonly Text[] _renderPerformanceLabels;
        private readonly Text[] _renderPerformanceValues;
        private readonly Text[] _calculationPerformanceLabels;
        private readonly Text[] _calculationPerformanceValues;

        public override bool NeedsRelayout => _outerStack.NeedsRelayout || _needsRelayout;
        public override bool IsActive => true;

        private bool _isActive;
        private bool _needsRelayout;

        public PerformanceWindowWidget(IPerformanceTracker performanceTracker)
        {
            _performanceTracker = performanceTracker;
            _needsRelayout = true;

            _outerStack = new StackGroup
            {
                HorizontalContentAlignment = HorizontalAlignment.Middle,
                Orientation = Orientation.Vertical
            };

            _scriptPerformanceLabels = Enumerable.Range(0, 10)
                .Select(i => new Text("")
                    {
                        Width = Width.Fixed(1000)
                    })
                .ToArray();
            _scriptPerformanceValues = Enumerable.Range(0, 10)
                .Select(i => new Text("")
                {
                    Width = Width.Fixed(1000)
                })
                .ToArray();

            _updatePerformanceLabels = Enumerable.Range(0, 10)
                .Select(i => new Text("")
                {
                    Width = Width.Fixed(1000)
                })
                .ToArray();
            _updatePerformanceValues = Enumerable.Range(0, 10)
                .Select(i => new Text("")
                {
                    Width = Width.Fixed(1000)
                })
                .ToArray();

            _renderPerformanceLabels = Enumerable.Range(0, 10)
                .Select(i => new Text("")
                {
                    Width = Width.Fixed(1000)
                })
                .ToArray();
            _renderPerformanceValues = Enumerable.Range(0, 10)
                .Select(i => new Text("")
                {
                    Width = Width.Fixed(1000)
                })
                .ToArray();

            _calculationPerformanceLabels = Enumerable.Range(0, 10)
                .Select(i => new Text("")
                {
                    Width = Width.Fixed(1000)
                })
                .ToArray();
            _calculationPerformanceValues = Enumerable.Range(0, 10)
                .Select(i => new Text("")
                {
                    Width = Width.Fixed(1000)
                })
                .ToArray();

            _contents = new AbstractUiElement[]
            {
                new Spacer(MARGIN,MARGIN),
                new Label("PERFORMANCE"),
                new StackGroup
                {
                    Orientation = Orientation.Horizontal,
                    Children = new List<IUiElement>
                    {
                        new Spacer(MARGIN,MARGIN),
                        new StackGroup
                        {
                            Orientation = Orientation.Vertical,
                            Children = new List<IUiElement>
                            {
                                new Spacer(100, 0),
                                new Label("")
                                {
                                    ContentUpdater = () => $"FPS: {_performanceTracker.FPS}"
                                },
                                new Label("")
                                {
                                    ContentUpdater = () => $"Update: {_performanceTracker.UpdateTime} ms"
                                },
                                new Label("")
                                {
                                    ContentUpdater = () => $"Layout: {_performanceTracker.LayoutTime} ms"
                                },
                                new Label("")
                                {
                                    ContentUpdater = () => $"Render: {_performanceTracker.RenderTime} ms"
                                }
                            }
                        },
                        new Spacer(10,10),
                        new StackGroup
                        {
                            HorizontalContentAlignment = HorizontalAlignment.Middle,
                            Orientation = Orientation.Vertical,
                            Children = new List<IUiElement>
                            {
                                new Label("UPDATES"),
                                new StackGroup
                                {
                                    Orientation = Orientation.Horizontal,
                                    Children = new List<IUiElement>
                                    {
                                        new StackGroup
                                        {
                                            HorizontalContentAlignment = HorizontalAlignment.Right,
                                            Orientation = Orientation.Vertical,
                                            Children = _updatePerformanceLabels
                                                .ToList<IUiElement>()
                                        },
                                        new StackGroup
                                        {
                                            HorizontalContentAlignment = HorizontalAlignment.Right,
                                            Orientation = Orientation.Vertical,
                                            Children = _updatePerformanceValues
                                                .AsEnumerable<IUiElement>()
                                                .Concat(new Spacer(80,0).Yield())
                                                .ToList()
                                        }
                                    }
                                },
                            }
                        },
                        new Spacer(10,10),
                        new StackGroup
                        {
                            HorizontalContentAlignment = HorizontalAlignment.Middle,
                            Orientation = Orientation.Vertical,
                            Children = new List<IUiElement>
                            {
                                new Label("RENDERING"),
                                new StackGroup
                                {
                                    Orientation = Orientation.Horizontal,
                                    Children = new List<IUiElement>
                                    {
                                        new StackGroup
                                        {
                                            HorizontalContentAlignment = HorizontalAlignment.Right,
                                            Orientation = Orientation.Vertical,
                                            Children = _renderPerformanceLabels
                                                .ToList<IUiElement>()
                                        },
                                        new StackGroup
                                        {
                                            HorizontalContentAlignment = HorizontalAlignment.Right,
                                            Orientation = Orientation.Vertical,
                                            Children = _renderPerformanceValues
                                                .AsEnumerable<IUiElement>()
                                                .Concat(new Spacer(80,0).Yield())
                                                .ToList()
                                        }
                                    }
                                },
                            }
                        },
                        new Spacer(10,10),
                        new StackGroup
                        {
                            HorizontalContentAlignment = HorizontalAlignment.Middle,
                            Orientation = Orientation.Vertical,
                            Children = new List<IUiElement>
                            {
                                new Label("CALCULATIONS"),
                                new StackGroup
                                {
                                    Orientation = Orientation.Horizontal,
                                    Children = new List<IUiElement>
                                    {
                                        new StackGroup
                                        {
                                            HorizontalContentAlignment = HorizontalAlignment.Right,
                                            Orientation = Orientation.Vertical,
                                            Children = _calculationPerformanceLabels
                                                .ToList<IUiElement>()
                                        },
                                        new StackGroup
                                        {
                                            HorizontalContentAlignment = HorizontalAlignment.Right,
                                            Orientation = Orientation.Vertical,
                                            Children = _calculationPerformanceValues
                                                .AsEnumerable<IUiElement>()
                                                .Concat(new Spacer(80,0).Yield())
                                                .ToList()
                                        }
                                    }
                                },
                            }
                        },
                        new Spacer(10,10),
                        new StackGroup
                        {
                            HorizontalContentAlignment = HorizontalAlignment.Middle,
                            Orientation = Orientation.Vertical,
                            Children = new List<IUiElement>
                            {
                                new Label("SCRIPTS"),
                                new StackGroup
                                {
                                    Orientation = Orientation.Horizontal,
                                    Children = new List<IUiElement>
                                    {
                                        new StackGroup
                                        {
                                            HorizontalContentAlignment = HorizontalAlignment.Right,
                                            Orientation = Orientation.Vertical,
                                            Children = _scriptPerformanceLabels
                                                .ToList<IUiElement>()
                                        },
                                        new StackGroup
                                        {
                                            HorizontalContentAlignment = HorizontalAlignment.Right,
                                            Orientation = Orientation.Vertical,
                                            Children = _scriptPerformanceValues
                                                .AsEnumerable<IUiElement>()
                                                .Concat(new Spacer(80,0).Yield())
                                                .ToList()
                                        }
                                    }
                                },
                            }
                        },
                        new Spacer(MARGIN,MARGIN),
                    }
                },
                new Spacer(MARGIN,MARGIN)
            };
        }
        
        public override void Update(
            InputState inputState,
            IAudioService audioService,
            float elapsedSeconds)
        {
            base.Update(inputState, audioService, elapsedSeconds);

            if (_isActive && inputState.JustPressed(KeyCode.F2))
            {
                _isActive = false;
                _needsRelayout = true;
                _outerStack.Children.Clear();
                audioService.PlaySound(UiSounds.Click);
            }
            else if (!_isActive && inputState.JustPressed(KeyCode.F2))
            {
                audioService.PlaySound(UiSounds.Click);
                _needsRelayout = true;
                _isActive = true;
                _outerStack.Children.AddRange(_contents);
            }

            if(_isActive)
            {
                UpdatePerformanceTexts();
            }

            _outerStack.Update(inputState, audioService, elapsedSeconds);
        }
        
        public override void DoLayout(IDrawingService drawingService, Rectangle parentBounds)
        {
            parentBounds = new Rectangle(
                parentBounds.X + 0,
                parentBounds.Y + 35,
                parentBounds.Width,
                parentBounds.Height);

            base.DoLayout(drawingService, parentBounds);

            // Want to do an extra layout, after the content stack is done with layout
            _needsRelayout = false || _outerStack.NeedsRelayout;

            _outerStack.DoLayout(drawingService, Bounds);
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            base.Draw(drawingService, layer + 1000);

            _outerStack.Draw(drawingService, layer + LAYER_STEP + 1000);
        }

        private void UpdatePerformanceTexts()
        {
            var slowScripts = _performanceTracker
                .GetPoorlyPerformingScripts(_scriptPerformanceLabels.Length)
                .ToArray();
            
            for (var i = 0; i < slowScripts.Length && i < _scriptPerformanceLabels.Length; i++)
            {
                _scriptPerformanceLabels[i].Content = slowScripts[i].Item1;
                _scriptPerformanceValues[i].Content = $"{slowScripts[i].Item2:N0} t";
            }

            var slowUpdates = _performanceTracker
                .GetPoorlyPerformingUpdates(_updatePerformanceLabels.Length)
                .ToArray();
            
            for (var i = 0; i < slowUpdates.Length && i < _updatePerformanceLabels.Length; i++)
            {
                _updatePerformanceLabels[i].Content = slowUpdates[i].Item1;
                _updatePerformanceValues[i].Content = $"{slowUpdates[i].Item2:N0} t";
            }

            var slowRenders = _performanceTracker
                .GetPoorlyPerformingRendering(_renderPerformanceLabels.Length)
                .ToArray();

            for (var i = 0; i < slowRenders.Length && i < _renderPerformanceLabels.Length; i++)
            {
                _renderPerformanceLabels[i].Content = slowRenders[i].Item1;
                _renderPerformanceValues[i].Content = $"{slowRenders[i].Item2:N0} t";
            }

            var slowCalculations = _performanceTracker
                .GetPoorlyPerformingCalculations(_calculationPerformanceLabels.Length)
                .ToArray();

            for (var i = 0; i < slowCalculations.Length && i < _calculationPerformanceLabels.Length; i++)
            {
                _calculationPerformanceLabels[i].Content = slowCalculations[i].Item1;
                _calculationPerformanceValues[i].Content = $"{slowCalculations[i].Item2:N0} t";
            }
        }
    }
}
