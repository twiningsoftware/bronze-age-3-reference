﻿using System.Collections.Generic;
using System.Linq;
using Bronze.Contracts;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;

namespace Bronze.Common.UI.Widgets
{
    public class SettlementManagementWidget : AbstractManagementWidget, IGameplayWidget
    {
        private readonly IPlayerDataTracker _playerData;
        
        public SettlementManagementWidget(
            IPlayerDataTracker playerData,
            IDialogManager dialogManager,
            IGamedataTracker gamedataTracker,
            IEmpireNotificationBuilder empireNotificationHandler,
            IWorldManager worldManager,
            INotificationsSystem notificationsSystem)
            :base(playerData, dialogManager, gamedataTracker, empireNotificationHandler, worldManager, notificationsSystem)
        {
            _playerData = playerData;
        }

        public override Settlement GetActiveSettlement()
        {
            return _playerData.ActiveSettlement;
        }

        protected override void SetTabConstruction(Caste caste)
        {
            Refresh = () => SetTabConstruction(caste);

            ActiveConstructionCaste = caste;
            _playerData.MouseMode = null;

            var structuresForCaste = ActiveSettlement.Race.AvailableStructures
                .Where(s => !s.UpgradeOnly)
                .Where(s => s.OperationWorkers.Caste == caste)
                .Where(s => s.CouldBePlaced(ActiveSettlement))
                .ToArray();
            
            _tabBody.Children.Clear();

            _tabBody.Children.Add(new Label("Construction"));
            _tabBody.Children.Add(new Spacer(0, 5));
            _tabBody.Children.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = _playerData.ActiveSettlement.Race.Castes
                    .SelectMany(c => new IUiElement[]
                    {
                        new IconButton(
                            c.BigIconKey, 
                            string.Empty,
                            () => SetTabConstruction(c),
                            tooltip: c.Name + " Structures",
                            highlightCheck: () => ActiveConstructionCaste == c),
                        new Spacer(5, 0)
                    })
                    .ToList()
            });
            _tabBody.Children.Add(new Spacer(0, 5));

            var structureButtons = new IconButton(
                    UiImages.TileInvalid,
                    string.Empty,
                    () =>
                    {
                        SetTabDemolish();
                    },
                    tooltip: "Demolish structures.")
                .Yield()
                .Concat(structuresForCaste
                    .Select(s => new IconButton(
                        s.IconKey,
                        string.Empty,
                        () =>
                        {
                            SetTabStructure(s);
                        },
                        tooltip: s.Name)))
                .ToArray();

            _tabBody.Children.Add(new Spacer(0, 5));

            for(var i = 0; i < structureButtons.Length; i += 4)
            {
                _tabBody.Children.Add(new StackGroup
                {
                    Orientation = Orientation.Horizontal,
                    Children = structureButtons
                        .Skip(i)
                        .Take(4)
                        .ToList<IUiElement>()
                });
            }
        }

        private void SetTabStructure(IStructureType structureType)
        {
            Refresh = () => { };

            _playerData.MouseMode = new StructurePlacementMouseMode(
                structureType, 
                () => SetTabConstruction(ActiveConstructionCaste));

            _tabBody.Children.Clear();
            _tabBody.Children.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                VerticalContentAlignment = VerticalAlignment.Middle,
                Children = new List<IUiElement>
                {
                    new Label(structureType.Name),
                    new Spacer(10, 0),
                    new TextButton("X",
                    () =>
                    {
                        SetTabConstruction(ActiveConstructionCaste);
                    }),
                }
            });
            _tabBody.Children.Add(new Spacer(0, 10));

            _tabBody.Children.Add(structureType.BuildTypeInfoPanel(250, withName: false));
        }

        private void SetTabDemolish()
        {
            Refresh = () => { };
            
            _playerData.MouseMode = new DemolishingStructureMouseMode(
                () => SetTabConstruction(ActiveConstructionCaste));

            _tabBody.Children.Clear();
            _tabBody.Children.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                VerticalContentAlignment = VerticalAlignment.Middle,
                Children = new List<IUiElement>
                {
                    new Label("Demolishing"),
                    new Spacer(10, 0),
                    new TextButton("X",
                    () =>
                    {
                        SetTabConstruction(ActiveConstructionCaste);
                    }),
                }
            });
            _tabBody.Children.Add(new Spacer(0, 5));

            _tabBody.Children.Add(new Text("Click or drag a box to demolish structures. Any stored items will be lost.")
            {
                Width = Width.Fixed(180)
            });
        }
    }
}
