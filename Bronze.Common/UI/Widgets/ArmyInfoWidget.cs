﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bronze.Common.Data.Game.Units;
using Bronze.Common.Data.World.CellDevelopments;
using Bronze.Common.UI.Elements;
using Bronze.Common.UI.Modals;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Diplomacy;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Widgets;

namespace Bronze.Common.UI.Widgets
{
    public class ArmyInfoWidget : UiWindowWidget, IGameplayWidget, IPreviewWidget
    {
        protected override int WidgetWidth => _contentStack.Bounds.Width;
        protected override int WidgetHeight => _contentStack.Bounds.Height;
        protected override HorizontalAlignment HorizontalAlignment => HorizontalAlignment.Middle;
        protected override VerticalAlignment VerticalAlignment => VerticalAlignment.Bottom;

        private readonly IDialogManager _dialogManager;
        private readonly IPlayerDataTracker _playerData;
        private readonly IWorldManager _worldManager;
        private readonly IUnitHelper _unitHelper;
        private readonly StackGroup _contentStack;

        private bool _needsRelayout;
        private long _currentStateHash;
        private Army _activeArmy;
        
        public override bool NeedsRelayout => _contentStack.NeedsRelayout || _needsRelayout;
        public override bool IsActive => _playerData.SelectedArmy != null;

        public ArmyInfoWidget(
            IDialogManager dialogManager,
            IPlayerDataTracker playerData,
            IWorldManager worldManager,
            IUnitHelper unitHelper)
        {
            _dialogManager = dialogManager;
            _playerData = playerData;
            _worldManager = worldManager;
            _unitHelper = unitHelper;

            _contentStack = new StackGroup
            {
                Orientation = Orientation.Vertical,
                HorizontalContentAlignment = HorizontalAlignment.Middle
            };
        }

        public override void Update(
            InputState inputState,
            IAudioService audioService,
            float elapsedSeconds)
        {
            base.Update(inputState, audioService, elapsedSeconds);

            var newActiveSettlement = _playerData.ActiveSettlement ?? _playerData.ActiveRegion?.Settlement;
            var newActiveArmy = _playerData.SelectedArmy;

            if (newActiveArmy != _activeArmy)
            {
                _activeArmy = newActiveArmy;

                RebuildContent();
            }
            else
            {
                var newStateHash = CalculateCellStateHash();

                if (_currentStateHash != newStateHash)
                {
                    RebuildContent();
                }
            }

            _contentStack.Update(inputState, audioService, elapsedSeconds);
        }

        public override void DoLayout(IDrawingService drawingService, Rectangle parentBounds)
        {
            _needsRelayout = _contentStack.NeedsRelayout;

            base.DoLayout(drawingService, parentBounds);
            _contentStack.DoLayout(drawingService, Bounds);
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            base.Draw(drawingService, layer);
            _contentStack.Draw(drawingService, layer + LAYER_STEP);
        }

        private long CalculateCellStateHash()
        {
            if (_activeArmy != null)
            {
                return _activeArmy.Id.GetHashCode()
                    + _activeArmy.Units.Select(u => (long)u.Name.GetHashCode()).Sum()
                    + _activeArmy.Cell.Position.ToString().GetHashCode()
                    + _activeArmy.General?.Name?.GetHashCode() ?? 0
                    + _activeArmy.TransportType?.Id?.GetHashCode() ?? 0
                    + _activeArmy.Units.Count;
            }
            
            return 0;
        }

        private void RebuildContent()
        {
            _currentStateHash = CalculateCellStateHash();
            
            _needsRelayout = true;
            _contentStack.Children.Clear();

            if (_activeArmy == null)
            {
                return;
            }

            _contentStack.Children.Add(new Spacer(0, 5));
            var mainRow = new StackGroup
            {
                Orientation = Orientation.Horizontal
            };
            _contentStack.Children.Add(mainRow);
            _contentStack.Children.Add(new Spacer(0, 5));

            var slots = new List<IUiElement>();
            
            if (_activeArmy != null)
            {
                var leftDisplay = new StackGroup
                {
                    Orientation = Orientation.Vertical,
                    Children = new List<IUiElement>
                    {
                        new ContainerButton(
                            new SmallNotablePersonPortrait(() => _activeArmy.General),
                            () =>
                            {
                                _dialogManager.PushModal<NotablePersonPickerModal, NotablePersonPickerModalContext>(
                                    new NotablePersonPickerModalContext
                                    {
                                        Title = "General for " + _activeArmy.Name,
                                        CurrentChoice = _activeArmy.General,
                                        Callback = (newGeneral) => newGeneral.AssignTo(_activeArmy),
                                        Options = _activeArmy.Owner.NotablePeople
                                                .Where(np => !np.IsDead && np.IsMature)
                                                .ToArray(),
                                        ValidCheck = np => null
                                    });
                            },
                            disableCheck: () => _activeArmy.IsLocked || _activeArmy.Owner != _playerData.PlayerTribe)
                        {
                            Tooltip = new SlicedBackground(new StackGroup
                            {
                                Orientation = Orientation.Vertical,
                                Children = new List<IUiElement>
                                {
                                    new Label(_activeArmy.General?.Name ?? "Nobody"),
                                    UiBuilder.BuildStatDisplay(_activeArmy.General, NotablePersonStatType.Valor),
                                    UiBuilder.BuildStatDisplay(_activeArmy.General, NotablePersonStatType.Wit),
                                    UiBuilder.BuildStatDisplay(_activeArmy.General, NotablePersonStatType.Charisma)
                                }
                            })
                        },
                        new Spacer(0, 5)
                    }
                };
                
                if(_activeArmy.TransportType != null)
                {
                    leftDisplay.Children.Add(UiBuilder.BuildDisplayFor(_activeArmy.TransportType));
                }

                slots.Add(leftDisplay);

                slots.Add(new Spacer(5, 0));
                
                slots.AddRange(_activeArmy.Units
                    .Select(u => new ContainerButton(
                        new UnitSlotDisplay(_unitHelper, _activeArmy, u),
                        () =>
                        {
                            _dialogManager.PushModal<UnitInfoModal, UnitInfoModalContext>(
                                new UnitInfoModalContext
                                {
                                    Army = _activeArmy,
                                    Unit = u
                                });
                        })));
            }
            
            for(var i = _activeArmy.Units.Count; i < Constants.UNITS_PER_ARMY; i++)
            {
                slots.Add(new Spacer(UnitSlotDisplay.ExpectedWidth, UnitSlotDisplay.ExpectedHeight));
            }

            var buttonColumnColumns = new StackGroup
            {
                Orientation = Orientation.Horizontal
            };

            var leftColumn = new StackGroup
            {
                Orientation = Orientation.Vertical
            };

            var topRow = new StackGroup
            {
                Orientation = Orientation.Horizontal
            };

            mainRow.Children.Add(new Spacer(5, 0));
            mainRow.Children.Add(leftColumn);
            mainRow.Children.Add(new Spacer(5, 0));
            mainRow.Children.Add(buttonColumnColumns);
            mainRow.Children.Add(new Spacer(5, 0));

            leftColumn.Children.Add(topRow);

            topRow.Children.Add(new Spacer(10, 0));

            if(_activeArmy.Owner == _playerData.PlayerTribe)
            {
                topRow.Children.Add(new TextInput(
                    () => _activeArmy.Name,
                    val => _activeArmy.Name = val,
                    200));
            }
            else
            {
                topRow.Children.Add(new Text(_activeArmy.Name)
                {
                    Width = Width.Fixed(200)
                });
            }
            
            topRow.Children.AddRange(new IUiElement[]
            {
                new Spacer(10, 0),
                new ContainerButton(
                    new Image(UiImages.STANCE_AGRESSIVE),
                    () => _activeArmy.Stance = ArmyStance.Agressive,
                    disableCheck: () => _activeArmy.Owner != _playerData.PlayerTribe,
                    highlightCheck: () => _activeArmy.Stance == ArmyStance.Agressive,
                    tooltip: "Agressively approach enemy armies."),
                new ContainerButton(
                    new Image(UiImages.STANCE_DEFENSIVE),
                    () => _activeArmy.Stance = ArmyStance.Defensive,
                    disableCheck: () => _activeArmy.Owner != _playerData.PlayerTribe,
                    highlightCheck: () => _activeArmy.Stance == ArmyStance.Defensive,
                    tooltip: "Ignore enemy armies."),
                new ContainerButton(
                    new Image(UiImages.STANCE_EVASIVE),
                    () => _activeArmy.Stance = ArmyStance.Evasive,
                    disableCheck: () => _activeArmy.Owner != _playerData.PlayerTribe,
                    highlightCheck: () => _activeArmy.Stance == ArmyStance.Evasive,
                    tooltip: "Avoid enemy armies."),
                new ContainerButton(
                    new Image(UiImages.STANCE_HARASSING),
                    () => _activeArmy.Stance = ArmyStance.Harassing,
                    disableCheck: () => _activeArmy.Owner != _playerData.PlayerTribe,
                    highlightCheck: () => _activeArmy.Stance == ArmyStance.Harassing,
                    tooltip: "Keep at skirmish range with enemy armies."),
                new Spacer(20, 0),
                new Label(string.Empty, BronzeColor.Grey)
                {
                    ContentUpdater = () => _activeArmy.GetStatusDescription()
                },
                new Spacer(10, 0),
                new IconButton(
                    UiImages.IconView,
                    string.Empty,
                    () =>
                    {
                        _playerData.LookAt(_activeArmy.Cell.Position);
                    },
                    tooltip: "Zoom to the army.")
            });
            
            leftColumn.Children.Add(new Spacer(5, 0));
            leftColumn.Children.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = slots
            });

            if (_activeArmy.Owner == _playerData.PlayerTribe)
            {
                var allButtons = new List<IUiElement>();

                var localSettlement = _activeArmy.Cell.Region.Settlement;

                buttonColumnColumns.Children.Add(new Spacer(0, 5));

                Func<IEnumerable<Cell>> getSpawns = () => _activeArmy.Cell.OrthoNeighbors
                    .Where(c => _activeArmy.CanPath(c))
                    .ToArray();

                allButtons.Add(new IconButton(
                    UiImages.BUTTON_SPLIT_ARMY,
                        string.Empty,
                        () =>
                        {
                            _dialogManager.PushModal<ArmySplitModal, ArmySplitModalContext>(
                                new ArmySplitModalContext
                                {
                                    SourceArmy = _activeArmy,
                                    PotentialSpawns = getSpawns()
                                });
                        },
                        disableCheck: () => _activeArmy.Units.Count < 1 || !getSpawns().Any() || _activeArmy.IsLocked,
                        tooltip: "Split the army into two."));

                allButtons.Add(new IconButton(
                        UiImages.BUTTON_RECRUIT,
                        string.Empty,
                        () =>
                        {
                            _dialogManager.PushModal<RecruitmentModal, RecruitmentModalContext>(
                                new RecruitmentModalContext
                                {
                                    DestinationArmy = _activeArmy,
                                    Settlement = _activeArmy.Cell.Region.Settlement,
                                });
                        },
                        disableCheck: () => _activeArmy.Cell.Region.Settlement == null || _activeArmy.Cell.Region.Settlement.Owner != _playerData.PlayerTribe || _activeArmy.IsLocked,
                        tooltip: "Recruit units"));

                allButtons.AddRange(_activeArmy.AvailableActions
                    .Where(a => !(a is ArmyMergeAction) && !(a is ArmyApproachAction) && !(a is ArmySkirmishAction))
                    .Select(armyAction => new IconButton(
                        armyAction.Icon,
                        string.Empty,
                        () =>
                        {
                            _playerData.MouseMode = new ArmyOrderMouseMode(_playerData.SelectedArmy, armyAction, () => { }, _worldManager);
                        },
                        highlightCheck: () => _playerData.MouseMode is ArmyOrderMouseMode aomm && aomm.ArmyAction == armyAction,
                        tooltip: armyAction.Name,
                        disableCheck: () => _activeArmy.IsLocked)));

                var buttonsPerColumn = 3;
                for (var i = 0; i < allButtons.Count; i += buttonsPerColumn)
                {
                    var columnItems = new List<IUiElement>();
                    columnItems.Add(new Spacer(0, 5));
                    columnItems.AddRange(allButtons.Skip(i).Take(buttonsPerColumn));
                    columnItems.Add(new Spacer(0, 5));

                    buttonColumnColumns.Children.Add(new StackGroup
                    {
                        Orientation = Orientation.Vertical,
                        Children = columnItems
                    });
                }
            }
        }
    }
}
