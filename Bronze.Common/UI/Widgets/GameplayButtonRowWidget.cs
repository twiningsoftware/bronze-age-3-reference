﻿using System.Collections.Generic;
using Bronze.Common.UI.Modals;
using Bronze.Contracts.Data;
using Bronze.Contracts.Help;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Widgets;

namespace Bronze.Common.UI.Widgets
{
    public class GameplayButtonRowWidget : UiWindowWidget, IGameplayWidget
    {
        protected override int WidgetWidth => _contentStack.Bounds.Width;
        protected override int WidgetHeight => _contentStack.Bounds.Height;
        protected override HorizontalAlignment HorizontalAlignment => HorizontalAlignment.Left;
        protected override VerticalAlignment VerticalAlignment => VerticalAlignment.Top;

        private readonly IDialogManager _dialogManager;
        private readonly IPlayerDataTracker _playerData;
        private readonly IUserSettings _userSettings;
        private readonly StackGroup _contentStack;

        public override bool NeedsRelayout => _contentStack.NeedsRelayout;

        public override bool IsActive => true;

        public GameplayButtonRowWidget(
            IDialogManager dialogManager,
            IPlayerDataTracker playerData,
            IUserSettings userSettings)
        {
            _dialogManager = dialogManager;;
            _playerData = playerData;
            _userSettings = userSettings;
            
            _contentStack = new StackGroup
            {
                Orientation = Orientation.Horizontal,
                VerticalContentAlignment = VerticalAlignment.Middle,
                Children = new List<IUiElement>
                {
                    new Spacer(10, 36),
                    new TextButton("Menu (Esc)",
                        () =>
                        {
                            _dialogManager.PushModal<EscapeMenuModal>();
                        },
                        KeyCode.Escape),
                    new Spacer(10, 36),
                    new TextButton("Help",
                        () =>
                        {
                            _dialogManager.PushModal<HelpModal, HelpModalContext>(
                                new HelpModalContext());
                        },
                        KeyCode.F1,
                        disableCheck: () => true),
                    new Spacer(10, 36),
                    new TextButton(
                        "Overlays",
                        () => _userSettings.ShowOverlaysMenu = !_userSettings.ShowOverlaysMenu,
                        highlightCheck: () => _userSettings.ShowOverlaysMenu),
                    new Spacer(10, 36),
                }
            };
        }

        public override void Update(
            InputState inputState,
            IAudioService audioService,
            float elapsedSeconds)
        {
            base.Update(inputState, audioService, elapsedSeconds);
            
            _contentStack.Update(inputState, audioService, elapsedSeconds);
        }
        
        public override void DoLayout(IDrawingService drawingService, Rectangle parentBounds)
        {
            base.DoLayout(drawingService, parentBounds);

            _contentStack.DoLayout(drawingService, Bounds);

            Bounds = _contentStack.Bounds;
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            base.Draw(drawingService, layer);
            _contentStack.Draw(drawingService, layer + LAYER_STEP);
        }
    }
}
