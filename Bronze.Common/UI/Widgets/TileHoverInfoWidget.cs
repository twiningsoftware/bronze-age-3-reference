﻿using System.Collections.Generic;
using System.Linq;
using Bronze.Common.UI.Elements;
using Bronze.Common.UI.Modals;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Widgets;

namespace Bronze.Common.UI.Widgets
{
    public class TileHoverInfoWidget : UiWindowWidget, IGameplayWidget, IPreviewWidget
    {
        protected override int WidgetWidth => _contentStack.Bounds.Width;
        protected override int WidgetHeight => _contentStack.Bounds.Height;
        protected override HorizontalAlignment HorizontalAlignment => HorizontalAlignment.Left;
        protected override VerticalAlignment VerticalAlignment => VerticalAlignment.Bottom;

        private readonly IDialogManager _dialogManager;
        private readonly IPlayerDataTracker _playerData;
        private readonly IWorldManager _worldManager;
        private readonly IGamedataTracker _gamedataTracker;
        private readonly StackGroup _contentStack;

        private bool _needsRelayout;
        private Tile _activeTile;
        private int _activeTileStateHash;

        public override bool NeedsRelayout => _contentStack.NeedsRelayout || _needsRelayout;
        public override bool IsActive => _playerData.ActiveSettlement != null && !(_playerData.MouseMode is StructurePlacementMouseMode || _playerData.MouseMode is DevelopmentPlacementMouseMode);

        public TileHoverInfoWidget(
            IDialogManager dialogManager,
            IPlayerDataTracker playerData,
            IWorldManager worldManager,
            IGamedataTracker gamedataTracker)
        {
            _dialogManager = dialogManager;
            _playerData = playerData;
            _worldManager = worldManager;
            _gamedataTracker = gamedataTracker;

            _contentStack = new StackGroup
            {
                Orientation = Orientation.Horizontal,
            };
        }

        public override void Update(
            InputState inputState,
            IAudioService audioService,
            float elapsedSeconds)
        {
            base.Update(inputState, audioService, elapsedSeconds);

            var newActiveTile = _playerData.SelectedStructure?.UlTile ?? _playerData.HoveredTile;

            // If hovering over a UI element, keep showing the same tile infomation
            // this should prevent strobing when hovering tiles and the mouse moves over the tile hover info widget
            if (newActiveTile == null)
            {
                newActiveTile = _activeTile;
            }

            var newStateHash = CalculateTileStateHash(newActiveTile);

            if (_activeTileStateHash != newStateHash)
            {
                _activeTile = newActiveTile;
                _activeTileStateHash = newStateHash;

                RebuildContent();
            }

            _contentStack.Update(inputState, audioService, elapsedSeconds);
        }

        public override void DoLayout(IDrawingService drawingService, Rectangle parentBounds)
        {
            _needsRelayout = _contentStack.NeedsRelayout;

            base.DoLayout(drawingService, parentBounds);
            _contentStack.DoLayout(drawingService, Bounds);
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            base.Draw(drawingService, layer);
            _contentStack.Draw(drawingService, layer + LAYER_STEP);
        }

        private int CalculateTileStateHash(Tile tile)
        {
            if (tile == null)
            {
                return 0;
            }

            var toHash = tile.Terrain.Name;

            toHash += tile.Position.ToString();

            if (tile.Structure != null)
            {
                toHash += tile.Structure.Name;

                if (tile.Structure.UnderConstruction)
                {
                    toHash += "construction";
                }

                if (tile.Structure.IsUpgrading)
                {
                    toHash += "upgrading";
                }

                toHash += tile.Structure.FactoryState.ToString();
            }

            return toHash.GetHashCode();
        }

        private void RebuildContent()
        {
            _needsRelayout = true;
            _contentStack.Children.Clear();

            if (_activeTile != null)
            {
                var mainColumn = new StackGroup
                {
                    Orientation = Orientation.Vertical,
                    Children = new List<IUiElement>()
                };

                _contentStack.Children = new List<IUiElement>
                {
                    new Spacer(10, 150),
                    mainColumn,
                    new Spacer(10, 150)
                };

                mainColumn.Children = new List<IUiElement>
                {
                    new Spacer(200, 10),
                    new TraitDisplay(() => _activeTile?.Traits ?? Enumerable.Empty<Trait>()),
                    new Label(_activeTile.Terrain.Name),
                };

                if (_activeTile.Structure != null)
                {
                    mainColumn.Children.Add(UiBuilder.BuildEconomicActorHoverInfo(_gamedataTracker, _activeTile.Structure, true, true));

                    if(_activeTile.Structure.CanManuallyUpgrade)
                    {
                        mainColumn.Children.Add(new IconButton(
                            KnownImages.IconUpgrade,
                            "Upgrade",
                            () =>
                            {
                                _dialogManager.PushModal<ConstructionInfoModal, ConstructionInfoModalContext>(
                                    new ConstructionInfoModalContext
                                    {
                                        Tile = _activeTile.Structure.UlTile,
                                        StructureTypes = _activeTile.Structure.ManuallyUpgradesTo
                                    });
                            }));
                    }
                    else if (_activeTile.Structure.IsUpgrading && !(_activeTile.Structure is IHousingProvider))
                    {
                        mainColumn.Children.Add(new IconButton(
                            KnownImages.IconIsUpgrading,
                            "Cancel Upgrading",
                            () =>
                            {
                                _activeTile.Structure.CancelUpgrading();
                            }));
                    }
                }



                mainColumn.Children.Add(new TextButton("Details",
                    () =>
                    {
                        _dialogManager.PushModal<TileDetailsModal, Tile>(_activeTile.Structure.UlTile);
                    }));
                
                mainColumn.Children.Add(new Spacer(200, 10));
            }
        }
    }
}
