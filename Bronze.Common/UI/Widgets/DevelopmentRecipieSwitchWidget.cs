﻿using System.Linq;
using System.Numerics;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Widgets;

namespace Bronze.Common.UI.Widgets
{
    public class DevelopmentRecipieSwitchWidget : UiWindowWidget, IGameplayWidget
    {
        protected override int WidgetWidth => _contentStack.Bounds.Width;
        protected override int WidgetHeight => _contentStack.Bounds.Height;
        protected override HorizontalAlignment HorizontalAlignment => HorizontalAlignment.Left;
        protected override VerticalAlignment VerticalAlignment => VerticalAlignment.Top;
        
        private readonly IPlayerDataTracker _playerData;
        private readonly IGameWorldDrawer _gameWorldDrawer;
        private readonly StackGroup _contentStack;
        
        private Cell _activeCell;
        private int _activeCellStateHash;
        private bool _hovered;

        public override bool NeedsRelayout => true;
        public override bool IsActive => _playerData.ViewMode == ViewMode.Detailed
            && ! (_playerData.MouseMode is DevelopmentPlacementMouseMode)
            && !(_playerData.MouseMode is DemolishingDevelopmentMouseMode)
            && (_playerData.SelectedCell ?? _playerData.HoveredCell ?? _activeCell)?.Region?.Owner == _playerData.PlayerTribe;

        public DevelopmentRecipieSwitchWidget(
            IPlayerDataTracker playerData,
            IGameWorldDrawer gameWorldDrawer)
        {
            _playerData = playerData;
            _gameWorldDrawer = gameWorldDrawer;

            

            _contentStack = new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Margin = 5
            };
        }

        public override void Update(
            InputState inputState,
            IAudioService audioService,
            float elapsedSeconds)
        {
            base.Update(inputState, audioService, elapsedSeconds);
            
            _hovered = Bounds.Contains(inputState.MousePosition) || _hovered && inputState.MouseOverWidget;
            
            var newActiveCell = GetActiveCell();
            
            var newStateHash = CalculateCellStateHash(newActiveCell);

            if (_activeCellStateHash != newStateHash)
            {
                _activeCell = newActiveCell;
                _activeCellStateHash = newStateHash;

                RebuildContent();
            }
            
            _contentStack.Update(inputState, audioService, elapsedSeconds);
        }

        public override void DoLayout(IDrawingService drawingService, Rectangle parentBounds)
        {
            var x = 0;
            var y = 0;
            
            if (_activeCell != null && _activeCell == GetActiveCell())
            {
                var cellPos = _gameWorldDrawer.TransformToScreen(_activeCell.Position.ToVector() * Constants.CELL_SIZE_PIXELS);
                
                var viewDelta = cellPos + new Vector2(-Constants.CELL_SIZE_PIXELS / 2, Constants.CELL_SIZE_PIXELS / 2 - WidgetHeight);
                
                x = (int)viewDelta.X;
                y = (int)viewDelta.Y;
            }

            parentBounds = new Rectangle(
                parentBounds.X + x,
                parentBounds.Y + y,
                parentBounds.Width,
                parentBounds.Height);
            
            base.DoLayout(drawingService, parentBounds);
            _contentStack.DoLayout(drawingService, Bounds);
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            if(_contentStack.Children.Any() && Bounds.Width > 0)
            {
                base.Draw(drawingService, layer);
                _contentStack.Draw(drawingService, layer + LAYER_STEP);
            }
        }

        private Cell GetActiveCell()
        {
            return _playerData.SelectedCell ?? _playerData.HoveredCell ?? _activeCell;
        }

        private int CalculateCellStateHash(Cell cell)
        {
            if (cell == null)
            {
                return 0;
            }

            var toHash = cell.Biome.Name;

            toHash += cell.Position.ToString();

            if (cell.Region.Owner != null)
            {
                toHash += cell.Region.Owner.Name;
            }

            if (cell.Feature != null)
            {
                toHash += cell.Feature.Name;
            }

            if (cell.Development != null)
            {
                toHash += cell.Development.Name;

                if (cell.Development.UnderConstruction)
                {
                    toHash += "construction";
                }
                if(cell.Development.IsUpgrading)
                {
                    toHash += "upgrading";
                }
                
                toHash += cell.Development.ActiveRecipie?.Name ?? string.Empty;
            }

            if(cell == _playerData.SelectedCell)
            {
                toHash += "selected";
            }

            if(_hovered)
            {
                toHash += "hovered";
            }

            return toHash.GetHashCode();
        }

        private void RebuildContent()
        {
            _contentStack.Children.Clear();
            
            if (_activeCell != null && _activeCell.Development != null)
            {
                var validRecipies = _activeCell.Development.AvailableRecipies
                    .Where(r => r.ShowToPlayer(_activeCell.Development))
                    .ToArray();

                if((validRecipies.Length > 1 || _activeCell.Development.ActiveRecipie == null)
                    && validRecipies.Any())
                { 
                    if(!_hovered
                        && _activeCell != _playerData.SelectedCell
                        && _activeCell.Development.ActiveRecipie != null)
                    {
                        _contentStack.Children.Add(
                            new Image(_activeCell.Development.ActiveRecipie.IconKey));
                    }
                    else if(_hovered || _activeCell == _playerData.SelectedCell)
                    {
                        _contentStack.Children.Add(
                            UiBuilder.BuildRecipieButtonRow(validRecipies, _activeCell.Development));
                    }
                }
            }
        }
    }
}
