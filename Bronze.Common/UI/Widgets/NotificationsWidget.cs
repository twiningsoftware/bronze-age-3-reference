﻿using Bronze.UI.Data;
using Bronze.UI.Elements;
using System.Linq;
using Bronze.Common.UI.Elements;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.Data;
using Bronze.UI.Widgets;
using Bronze.Contracts.UI;

namespace Bronze.Common.UI.Widgets
{
    public class NotificationsWidget : UiWindowWidget, IGameplayWidget
    {
        protected override int WidgetWidth => _contentStack.Bounds.Width;
        protected override int WidgetHeight => _contentStack.Bounds.Height;
        protected override HorizontalAlignment HorizontalAlignment => HorizontalAlignment.Left;
        protected override VerticalAlignment VerticalAlignment => VerticalAlignment.Top;

        private readonly IWorldManager _worldManager;
        private readonly INotificationsSystem _notificationsSystem;
        private readonly IPlayerDataTracker _playerDataTracker;
        private readonly StackGroup _contentStack;

        public override bool NeedsRelayout => _contentStack.NeedsRelayout;

        public override bool IsActive => true;

        public NotificationsWidget(
            IWorldManager worldManager,
            INotificationsSystem notificationsSystem,
            IPlayerDataTracker playerDataTracker)
        {
            _worldManager = worldManager;
            _notificationsSystem = notificationsSystem;
            _playerDataTracker = playerDataTracker;
            
            _contentStack = new StackGroup
            {
                Height = Height.Fixed(48),
                Orientation = Orientation.Horizontal,
                VerticalContentAlignment = VerticalAlignment.Middle
            };
        }

        public override void Update(
            InputState inputState,
            IAudioService audioService,
            float elapsedSeconds)
        {
            base.Update(inputState, audioService, elapsedSeconds);
            
            _contentStack.Update(inputState, audioService, elapsedSeconds);

            var currentNotifications = _contentStack.Children
                .OfType<DisplayedNotification>()
                .Select(n => n.Notification)
                .ToArray();
            
            foreach(var notification in _notificationsSystem.GetActiveNotifications())
            {
                if(!currentNotifications.Contains(notification))
                {
                    _contentStack.Children.Add(new DisplayedNotification(notification, _notificationsSystem, _worldManager, _playerDataTracker.PlayerTribe.PrimaryColor));
                }
            }

            _contentStack.Children.RemoveAll(n => (n is DisplayedNotification dn) && dn.Notification.IsDismissed);
        }
        
        public override void DoLayout(IDrawingService drawingService, Rectangle parentBounds)
        {
            parentBounds = new Rectangle(
                parentBounds.X + 245,
                parentBounds.Y,
                parentBounds.Width,
                parentBounds.Height);

            base.DoLayout(drawingService, parentBounds);

            _contentStack.DoLayout(drawingService, Bounds);

            Bounds = _contentStack.Bounds;
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            _contentStack.Draw(drawingService, layer + LAYER_STEP);
        }
    }
}
