﻿using System.Linq;
using System.Numerics;
using Bronze.Common.UI.Modals;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Widgets;

namespace Bronze.Common.UI.Widgets
{
    public class CellActivitiesWidget : UiWindowWidget, IGameplayWidget
    {
        protected override int WidgetWidth => _contentStack.Bounds.Width;
        protected override int WidgetHeight => _contentStack.Bounds.Height;
        protected override HorizontalAlignment HorizontalAlignment => HorizontalAlignment.Left;
        protected override VerticalAlignment VerticalAlignment => VerticalAlignment.Top;

        private readonly IDialogManager _dialogManager;
        private readonly IPlayerDataTracker _playerData;
        private readonly IGameWorldDrawer _gameWorldDrawer;
        private readonly StackGroup _contentStack;
        
        private Cell _activeCell;
        private int _activeCellStateHash;

        public override bool NeedsRelayout => true;
        public override bool IsActive => _playerData.ActiveSettlement == null 
            && ! (_playerData.MouseMode is DevelopmentPlacementMouseMode) 
            && _playerData.SelectedCell != null
            && _playerData.SelectedCell.Region?.Owner == _playerData.PlayerTribe;

        public CellActivitiesWidget(
            IDialogManager dialogManager,
            IPlayerDataTracker playerData,
            IGameWorldDrawer gameWorldDrawer)
        {
            _dialogManager = dialogManager;
            _playerData = playerData;
            _gameWorldDrawer = gameWorldDrawer;

            _contentStack = new StackGroup
            {
                Orientation = Orientation.Vertical
            };
        }

        public override void Update(
            InputState inputState,
            IAudioService audioService,
            float elapsedSeconds)
        {
            base.Update(inputState, audioService, elapsedSeconds);

            var newActiveCell = _playerData.SelectedCell;
            
            var newStateHash = CalculateCellStateHash(newActiveCell);

            if (_activeCellStateHash != newStateHash)
            {
                _activeCell = newActiveCell;
                _activeCellStateHash = newStateHash;

                RebuildContent();
            }
            
            _contentStack.Update(inputState, audioService, elapsedSeconds);
        }

        public override void DoLayout(IDrawingService drawingService, Rectangle parentBounds)
        {
            var x = 0;
            var y = 0;

            if (_playerData.SelectedCell != null)
            {
                Vector2 cellPos;
                float toEdgeOffset;

                if (_playerData.ViewMode == ViewMode.Detailed)
                {
                    cellPos = _gameWorldDrawer.TransformToScreen(_playerData.SelectedCell.Position.ToVector() * Constants.CELL_SIZE_PIXELS);
                    toEdgeOffset = Constants.CELL_SIZE_PIXELS * 0.5f;
                }
                else
                {
                    cellPos = _gameWorldDrawer.TransformToScreen(_playerData.SelectedCell.Position.ToVector() * Constants.CELL_SIZE_PIXELS);
                    toEdgeOffset = Constants.CELL_SIZE_SUMMARY_PIXELS * 0.5f;
                }
                
                var viewDelta = cellPos + new Vector2(toEdgeOffset, -toEdgeOffset);
                
                x = (int)viewDelta.X;
                y = (int)viewDelta.Y;
            }

            parentBounds = new Rectangle(
                parentBounds.X + x,
                parentBounds.Y + y,
                parentBounds.Width,
                parentBounds.Height);
            
            base.DoLayout(drawingService, parentBounds);
            _contentStack.DoLayout(drawingService, Bounds);
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            base.Draw(drawingService, layer);
            _contentStack.Draw(drawingService, layer + LAYER_STEP);
        }

        private int CalculateCellStateHash(Cell cell)
        {
            if (cell == null)
            {
                return 0;
            }

            var toHash = cell.Biome.Name;

            toHash += cell.Position.ToString();

            if (cell.Region.Owner != null)
            {
                toHash += cell.Region.Owner.Name;
            }

            if (cell.Feature != null)
            {
                toHash += cell.Feature.Name;
            }

            if (cell.Development != null)
            {
                toHash += cell.Development.Name;

                if (cell.Development.UnderConstruction)
                {
                    toHash += "construction";
                }
                if(cell.Development.IsUpgrading)
                {
                    toHash += "upgrading";
                }
            }

            return toHash.GetHashCode();
        }

        private void RebuildContent()
        {
            _contentStack.Children.Clear();
            
            if (_activeCell != null)
            {
                var activityOptions = (_activeCell.Region.Settlement?.Race?.AvailableCellDevelopments ?? Enumerable.Empty<ICellDevelopmentType>())
                    .Where(d => d.IsActivity)
                    .Where(d => !d.UpgradeOnly)
                    .Where(d => d.CanBePlaced(_playerData.PlayerTribe, _activeCell))
                    .ToArray();

                _contentStack.Children
                    .AddRange(activityOptions
                        .Select(d => new IconButton(
                            d.IconKey,
                            d.Name,
                            () =>
                            {
                                _dialogManager.PushModal<ConstructionInfoModal, ConstructionInfoModalContext>(
                                    new ConstructionInfoModalContext
                                    {
                                        Cell = _activeCell,
                                        CellDevelopmentTypes = d.Yield()
                                    });
                            })));

                if(_activeCell.Development != null)
                {
                    if(_activeCell.Development.CanManuallyUpgrade)
                    {
                        _contentStack.Children.Add(new IconButton(
                            KnownImages.IconUpgrade,
                            "Upgrade",
                            () =>
                            {
                                _dialogManager.PushModal<ConstructionInfoModal, ConstructionInfoModalContext>(
                                new ConstructionInfoModalContext
                                {
                                    Cell = _activeCell,
                                    CellDevelopmentTypes = _activeCell.Development.ManuallyUpgradesTo
                                });
                            }));
                    }
                    if(_activeCell.Development.CanManuallyDowngrade)
                    {
                        _contentStack.Children.Add(new IconButton(
                            KnownImages.IconDowngrade,
                            "Downgrade",
                            () =>
                            {
                                _dialogManager.PushModal<ConstructionInfoModal, ConstructionInfoModalContext>(
                                new ConstructionInfoModalContext
                                {
                                    Cell = _activeCell,
                                    CellDevelopmentTypes = _activeCell.Development.ManuallyDowngradesTo.Yield()
                                });
                            }));
                    }
                }
            }
        }
    }
}
