﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Widgets;

namespace Bronze.Common.UI.Widgets
{
    public class ConsoleWindowWidget : UiWindowWidget, IGameplayWidget, IPreviewWidget
    {
        private const int MARGIN = 20;

        protected override int WidgetWidth => _outerStack.Bounds.Width;
        protected override int WidgetHeight => _outerStack.Bounds.Height;
        protected override HorizontalAlignment HorizontalAlignment => HorizontalAlignment.Left;
        protected override VerticalAlignment VerticalAlignment => VerticalAlignment.Top;

        private readonly IConsoleCommand[] _consoleCommands;
        private readonly ILogger _logger;
        private readonly IGameInterface _gameInterface;
        private readonly StackGroup _outerStack;
        private readonly StackGroup _contents;

        private readonly List<string> _commandHistory;
        private int _commandHistoryIndex;

        public override bool NeedsRelayout => true;
        public override bool IsActive => true;

        private bool _isActive;
        private bool _needsRelayout;

        private string _consoleText;

        private TextInput _textInput;
        private Text[] _history; 

        public ConsoleWindowWidget(
            ILogger logger,
            IConsoleCommand[] consoleCommands,
            IGameInterface gameInterface)
        {
            _logger = logger;
            _consoleCommands = consoleCommands.ToArray();
            _gameInterface = gameInterface;

            _commandHistory = new List<string>();
                 
            _needsRelayout = true;
            
            _outerStack = new StackGroup
            {
                Orientation = Orientation.Horizontal
            };

            _textInput = new TextInput(() => _consoleText, val => _consoleText = val, 400);
            _textInput.GetHintText = GetHintText;
            _textInput.OnKeyTyped += TextInput_OnKeyTyped;

            _history = Enumerable.Range(0, 15)
                .Select(i => new Text(string.Empty)
                {
                    WordWrap = false
                })
                .ToArray();

            _contents = new StackGroup
            {
                HorizontalContentAlignment = HorizontalAlignment.Middle,
                Orientation = Orientation.Vertical,
                Children = new List<IUiElement>
                {
                    new Spacer(0, 5),
                    new Label("CONSOLE"),
                    new StackGroup
                    {
                        Width = Width.Relative(1),
                        HorizontalContentAlignment = HorizontalAlignment.Left,
                        Children = _history.ToList<IUiElement>()
                    },
                    _textInput,
                    new Spacer(0, 3),
                }
            };
        }
        
        public override void Update(
            InputState inputState,
            IAudioService audioService,
            float elapsedSeconds)
        {
            base.Update(inputState, audioService, elapsedSeconds);

            var togglePressed = inputState.JustPressed(KeyCode.OemTilde) || inputState.JustPressed(KeyCode.F3);

            if (_isActive && togglePressed)
            {
                _isActive = false;
                _needsRelayout = true;
                _outerStack.Children.Clear();
                audioService.PlaySound(UiSounds.Click);

                if(inputState.FocusedElement == _textInput)
                {
                    inputState.FocusedElement = null;
                }
            }
            else if (!_isActive && togglePressed)
            {
                audioService.PlaySound(UiSounds.Click);
                _needsRelayout = true;
                _isActive = true;
                _outerStack.Children.Add(new Spacer(5, 0));
                _outerStack.Children.Add(_contents);
                _outerStack.Children.Add(new Spacer(5, 0));
                inputState.FocusedElement = _textInput;
            }

            if(_isActive)
            {
                UpdateHistory();
            }

            _outerStack.Update(inputState, audioService, elapsedSeconds);
        }
        
        public override void DoLayout(IDrawingService drawingService, Rectangle parentBounds)
        {
            parentBounds = new Rectangle(
                parentBounds.X + 0,
                parentBounds.Y + 35,
                parentBounds.Width,
                parentBounds.Height);

            base.DoLayout(drawingService, parentBounds);

            // Want to do an extra layout, after the content stack is done with layout
            _needsRelayout = false || _outerStack.NeedsRelayout;

            _outerStack.DoLayout(drawingService, Bounds);
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            base.Draw(drawingService, layer + 2000);

            _outerStack.Draw(drawingService, layer + LAYER_STEP + 2000);
        }

        private void UpdateHistory()
        {
            var logHistory = _logger.GetHistory(_history.Length);

            for(var i = 0; i < _history.Length; i++)
            {
                var dstIndex = _history.Length - 1 - i;
                
                if(i < logHistory.Length)
                {
                    _history[dstIndex].Content = logHistory[i].Message;

                    if (logHistory[i].LogLevel == LogLevel.ERROR)
                    {
                        _history[dstIndex].Color = BronzeColor.Red;
                    }
                    else if (logHistory[i].LogLevel == LogLevel.CONSOLE)
                    {
                        _history[dstIndex].Color = BronzeColor.White;
                    }
                    else
                    {
                        _history[dstIndex].Color = BronzeColor.Grey;
                    }
                }
                else
                {
                    _history[dstIndex].Content = " ";
                }
            }
        }

        private string GetHintText()
        {
            if(_consoleText.Any())
            {
                var tokens = _consoleText.Split(' ')
                    .Where(s => !string.IsNullOrWhiteSpace(s))
                    .ToArray();

                if (tokens.Length < 2)
                {
                    if ("help".StartsWith(_consoleText))
                    {
                        return "help".Substring(_consoleText.Length);
                    }

                    foreach (var command in _consoleCommands)
                    {
                        if (command.CommandText.StartsWith(_consoleText))
                        {
                            return command.CommandText.Substring(_consoleText.Length);
                        }
                    }
                }
                else
                {
                    var command = _consoleCommands
                        .Where(c => c.CommandText == tokens[0])
                        .FirstOrDefault();

                    if(command != null)
                    {
                        return command.GetHint(tokens.Skip(1).ToArray());
                    }
                    else if (tokens[0] == "help" && tokens.Length > 1)
                    {
                        foreach (var secondCommand in _consoleCommands)
                        {
                            if (secondCommand.CommandText.StartsWith(tokens[1]))
                            {
                                return secondCommand.CommandText.Substring(tokens[1].Length);
                            }
                        }
                    }

                }
            }
            
            return string.Empty;
        }

        private bool TextInput_OnKeyTyped(KeyCode keyCode)
        {
            try
            {
                switch (keyCode)
                {
                    case KeyCode.Enter:
                        ExecuteCommand(_consoleText);
                        _consoleText = string.Empty;
                        return true;
                    case KeyCode.Tab:
                        _consoleText += GetHintText() + " ";
                        return true;
                    case KeyCode.Up:
                        if (_commandHistoryIndex > 0 && _commandHistory.Any())
                        {
                            _commandHistoryIndex -= 1;
                            _consoleText = _commandHistory[_commandHistoryIndex];
                        }
                        return true;
                    case KeyCode.Down:
                        if (_commandHistoryIndex < _commandHistory.Count)
                        {
                            _commandHistoryIndex += 1;

                            if (_commandHistoryIndex < _commandHistory.Count)
                            {
                                _consoleText = _commandHistory[_commandHistoryIndex];
                            }
                            else
                            {
                                _consoleText = string.Empty;
                            }
                        }
                        return true;
                }
            }
            catch(Exception ex)
            {
                _gameInterface.OnException(ex);
            }


            return false;
        }

        private void ExecuteCommand(string consoleText)
        {
            _commandHistory.Add(consoleText);
            _commandHistoryIndex = _commandHistory.Count;
            
            if (consoleText.Any())
            {
                var tokens = consoleText.Split(' ')
                    .Where(s => !string.IsNullOrWhiteSpace(s))
                    .ToArray();

                if (tokens[0] == "help")
                {
                    DoHelpCommand(tokens.Skip(1).ToArray());
                }
                else
                {
                    var command = _consoleCommands
                        .Where(c => c.CommandText == tokens[0])
                        .FirstOrDefault();

                    if (command != null)
                    {
                        command.Execute(tokens.Skip(1).ToArray());
                    }
                    else
                    {
                        _logger.Console("Unknown command: " + tokens[0]);
                    }
                }
            }
        }

        public void DoHelpCommand(string[] parameters)
        {
            if (parameters.Any())
            {
                var command = _consoleCommands
                    .Where(c => c.CommandText == parameters[0])
                    .FirstOrDefault();

                if (command != null)
                {
                    foreach (var line in command.DetailedHelp)
                    {
                        _logger.Console(line);
                    }
                }
                else if(parameters[0] == "help")
                {
                    _logger.Console("----------");
                    _logger.Console("help");
                    _logger.Console("Display help information for the console.");
                    _logger.Console("\"help\" show a list of possible commands");
                    _logger.Console("\"help command_name\" show detailed help for a command");
                    _logger.Console("----------");
                }
                else
                {
                    _logger.Console("Unknown command: " + command);
                }
            }
            else
            {
                _logger.Console("help - show this list, or \"help command_name\"");
                foreach (var command in _consoleCommands)
                {
                    _logger.Console(command.QuickHelp);
                }
            }
        }
    }
}
