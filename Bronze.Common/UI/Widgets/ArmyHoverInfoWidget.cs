﻿using System.Linq;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Widgets;

namespace Bronze.Common.UI.Widgets
{
    public class ArmyHoverInfoWidget : UiWindowWidget, IGameplayWidget, IPreviewWidget
    {
        protected override int WidgetWidth => 0;
        protected override int WidgetHeight => 0;
        protected override HorizontalAlignment HorizontalAlignment => HorizontalAlignment.Left;
        protected override VerticalAlignment VerticalAlignment => VerticalAlignment.Top;
        
        private readonly IPlayerDataTracker _playerData;
        private readonly IGameWorldDrawer _gameWorldDrawer;
        private readonly Border _outerBorder;
        private readonly StackGroup _contentStack;
        
        private long _activeStateHash;

        public override bool NeedsRelayout => true;
        public override bool IsActive => _playerData.ActiveSettlement == null 
            && _playerData.HoveredArmy != null
            && _playerData.PlayerTribe.IsVisible(_playerData.HoveredArmy.Cell);

        public ArmyHoverInfoWidget(
            IPlayerDataTracker playerData,
            IGameWorldDrawer gameWorldDrawer)
        {
            _playerData = playerData;
            _gameWorldDrawer = gameWorldDrawer;

            _contentStack = new StackGroup
            {
                Orientation = Orientation.Vertical
            };
            _outerBorder = new Border(_contentStack, margin: 3);
        }

        public override void Update(
            InputState inputState,
            IAudioService audioService,
            float elapsedSeconds)
        {
            base.Update(inputState, audioService, elapsedSeconds);
            
            var newStateHash = CalculateStateHash(_playerData.HoveredArmy);

            if (_activeStateHash != newStateHash)
            {
                _activeStateHash = newStateHash;

                RebuildContent();
            }

            _outerBorder.Update(inputState, audioService, elapsedSeconds);
        }

        public override void DoLayout(IDrawingService drawingService, Rectangle parentBounds)
        {
            var x = 0;
            var y = 0;

            if (_playerData.HoveredArmy != null)
            {
                var army = _playerData.HoveredArmy;
                
                var cellPos = _gameWorldDrawer.TransformToScreen(army.Position * Constants.CELL_SIZE_PIXELS);

                var toEdgeOffset = Constants.CELL_SIZE_PIXELS * 0.5f;

                var viewDelta = cellPos + new System.Numerics.Vector2(-toEdgeOffset, toEdgeOffset - 16);
                
                x = (int)viewDelta.X;
                y = (int)viewDelta.Y;
            }

            parentBounds = new Rectangle(
                parentBounds.X + x,
                parentBounds.Y + y,
                parentBounds.Width,
                parentBounds.Height);
            
            base.DoLayout(drawingService, parentBounds);
            _outerBorder.DoLayout(drawingService, Bounds);
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            // Don't call base, so we don't draw the border
            if (_contentStack.Children.Any())
            {
                _outerBorder.Draw(drawingService, layer + LAYER_STEP);
            }
        }

        private long CalculateStateHash(Army army)
        {
            if (army == null)
            {
                return 0;
            }

            var hash = 0L;

            hash += army.CurrentOrder.GetHashCode();
            hash += army.GetHashCode();

            return hash;
        }

        private void RebuildContent()
        {
            _contentStack.Children.Clear();

            var army = _playerData.HoveredArmy;
            
            if (army != null)
            {
                _contentStack.Children.Add(new Label(army.Name));
                _contentStack.Children.Add(new Label(string.Empty, BronzeColor.Grey)
                {
                    ContentUpdater = () => army.GetStatusDescription()
                });
                _contentStack.Children.Add(new Label(string.Empty, BronzeColor.Yellow)
                {
                    ContentUpdater = () => army.Units.Any(u=>u.SufferingAttrition) ? "Suffering Attrition" : string.Empty
                });

                // TODO health and unit information?

                // TODO combat summary?
            }
        }
    }
}
