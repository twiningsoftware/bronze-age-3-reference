﻿using System.Linq;
using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Widgets;

namespace Bronze.Common.UI.Widgets
{
    public class GamePausedIndicator : UiWindowWidget, IGameplayWidget, IPreviewWidget
    {
        private readonly IUserSettings _userSettings;

        protected override int WidgetWidth => _contentStack.Bounds.Width;
        protected override int WidgetHeight => _contentStack.Bounds.Height;
        protected override HorizontalAlignment HorizontalAlignment => HorizontalAlignment.Middle;
        protected override VerticalAlignment VerticalAlignment => VerticalAlignment.Top;

        private readonly StackGroup _contentStack;
        private bool _needsRelayout;

        public override bool NeedsRelayout => _contentStack.NeedsRelayout || _needsRelayout;
        public override bool IsActive => true;

        public GamePausedIndicator(IUserSettings userSettings)
        {
            _userSettings = userSettings;
            _needsRelayout = true;

            _contentStack = new StackGroup
            {
                HorizontalContentAlignment = HorizontalAlignment.Middle,
                VerticalContentAlignment = VerticalAlignment.Middle,
                Orientation = Orientation.Vertical
            };
        }

        public override void Update(
            InputState inputState,
            IAudioService audioService,
            float elapsedSeconds)
        {
            base.Update(inputState, audioService, elapsedSeconds);

            if (_contentStack.Children.Any() && !_userSettings.SimulationPaused)
            {
                _contentStack.Children.Clear();
            }
            else if (!_contentStack.Children.Any() && _userSettings.SimulationPaused)
            {
                BuildContent();
            }

            _contentStack.Update(inputState, audioService, elapsedSeconds);
        }

        public override void DoLayout(IDrawingService drawingService, Rectangle parentBounds)
        {
            base.DoLayout(drawingService, parentBounds);

            // Want to do an extra layout, after the content stack is done with layout
            _needsRelayout = false || _contentStack.NeedsRelayout;

            _contentStack.DoLayout(drawingService, Bounds);
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            if (!_needsRelayout)
            {
                base.Draw(drawingService, layer);

                _contentStack.Draw(drawingService, layer + 1);
            }
        }

        private void BuildContent()
        {
            _contentStack.Children.Add(new Spacer(100, 10));
            _contentStack.Children.Add(new Label("PAUSED")
            {
                Scale = 1.25f,
            });
            _contentStack.Children.Add(new Spacer(100, 10));
        }
    }
}
