﻿using System.Collections.Generic;
using Bronze.Common.Gameplay.UiElements;
using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Widgets;

namespace Bronze.Common.UI.Widgets
{
    public class TimeControlWidget : UiWindowWidget, IGameplayWidget, IPreviewWidget
    {
        protected override int WidgetWidth => _contentStack.Bounds.Width;
        protected override int WidgetHeight => _contentStack.Bounds.Height;
        protected override HorizontalAlignment HorizontalAlignment => HorizontalAlignment.Right;
        protected override VerticalAlignment VerticalAlignment => VerticalAlignment.Top;

        
        private readonly IWorldManager _worldManager;
        private readonly IUserSettings _userSettings;
        private readonly StackGroup _contentStack;
        private readonly IPlayerDataTracker _playerData;

        private bool _needsRelayout;

        public override bool NeedsRelayout => _contentStack.NeedsRelayout || _needsRelayout;

        public override bool IsActive => true;

        public TimeControlWidget(
            IWorldManager worldManager,
            IUserSettings userSettings,
            IPlayerDataTracker playerData)
        {
            _worldManager = worldManager;
            _userSettings = userSettings;
            _playerData = playerData;
            _needsRelayout = false;

            _contentStack = new StackGroup
            {
                Orientation = Orientation.Horizontal,
                VerticalContentAlignment = VerticalAlignment.Middle,
                Children = new List<IUiElement>
                {
                    new Spacer(10, 36),
                    new Label("")
                    {
                        ContentUpdater = () => _worldManager.DateString
                    },
                    new Spacer(10,0),
                    new SpeedControlWidget(_userSettings),
                    new Spacer(10, 36)
                }
            };
        }

        public override void Update(
            InputState inputState,
            IAudioService audioService,
            float elapsedSeconds)
        {
            base.Update(inputState, audioService, elapsedSeconds);
            
            _contentStack.Update(inputState, audioService, elapsedSeconds);
        }
        
        public override void DoLayout(IDrawingService drawingService, Rectangle parentBounds)
        {
            _needsRelayout = _contentStack.NeedsRelayout;
            
            base.DoLayout(drawingService, parentBounds);

            _contentStack.DoLayout(drawingService, Bounds);

            Bounds = _contentStack.Bounds;
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            base.Draw(drawingService, layer);
            _contentStack.Draw(drawingService, layer + LAYER_STEP);
        }
    }
}
