﻿using System.Collections.Generic;
using System.Linq;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Widgets;

namespace Bronze.Common.UI.Widgets
{
    public class PlacementMessagesWidget : UiWindowWidget, IGameplayWidget
    {
        protected override int WidgetWidth => _contentStack.Bounds.Width;
        protected override int WidgetHeight => _contentStack.Bounds.Height;
        protected override HorizontalAlignment HorizontalAlignment => HorizontalAlignment.Left;
        protected override VerticalAlignment VerticalAlignment => VerticalAlignment.Bottom;

        private readonly IPlayerDataTracker _playerData;
        private readonly StackGroup _contentStack;

        private bool _needsRelayout;
        private long _stateHash;

        public override bool NeedsRelayout => _contentStack.NeedsRelayout || _needsRelayout;
        public override bool IsActive => _playerData.MouseMode is StructurePlacementMouseMode 
            || _playerData.MouseMode is DevelopmentPlacementMouseMode
            || _playerData.MouseMode is CultRiteTargetingMouseMode;

        public PlacementMessagesWidget(IPlayerDataTracker playerData)
        {
            _playerData = playerData;

            _contentStack = new StackGroup
            {
                Orientation = Orientation.Horizontal
            };
        }

        public override void Update(
            InputState inputState,
            IAudioService audioService,
            float elapsedSeconds)
        {
            base.Update(inputState, audioService, elapsedSeconds);

            var messages = GetPlacementMessages();

            var newStateHash = messages.Select(s => (long)s.GetHashCode()).Sum();

            if (_stateHash != newStateHash)
            {
                _stateHash = newStateHash;

                RebuildContent(messages);
            }

            _contentStack.Update(inputState, audioService, elapsedSeconds);
        }

        private IEnumerable<string> GetPlacementMessages()
        {
            if(_playerData.MouseMode is StructurePlacementMouseMode structurePlacement
                && _playerData.HoveredTile != null)
            {
                return structurePlacement.StructureType.GetPlacementMessages(_playerData.HoveredTile);
            }
            else if (_playerData.MouseMode is DevelopmentPlacementMouseMode developmentPlacement
                && _playerData.PlayerTribe != null && _playerData.HoveredCell != null)
            {
                var messages = developmentPlacement.DevelopmentType.GetPlacementMessages(_playerData.PlayerTribe, _playerData.HoveredCell);

                if(!developmentPlacement.PlacementValid
                    && developmentPlacement.ForSettlement != null
                    && _playerData.HoveredCell.Region.Settlement != developmentPlacement.ForSettlement)
                {
                    messages = messages.Concat("Cannot place in another region.".Yield()).ToArray();
                }

                return messages;
            }
            else if (_playerData.MouseMode is CultRiteTargetingMouseMode riteTargetingMouseMode)
            {
                return riteTargetingMouseMode.CultRite.GetErrorMessageFor(riteTargetingMouseMode.ForSettlement, _playerData.HoveredCell).Yield();
            }

            return Enumerable.Empty<string>();
        }

        public override void DoLayout(IDrawingService drawingService, Rectangle parentBounds)
        {
            _needsRelayout = _contentStack.NeedsRelayout;

            base.DoLayout(drawingService, parentBounds);
            _contentStack.DoLayout(drawingService, Bounds);
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            base.Draw(drawingService, layer);
            _contentStack.Draw(drawingService, layer + LAYER_STEP);
        }

        private void RebuildContent(IEnumerable<string> messages)
        {
            _needsRelayout = true;
            _contentStack.Children.Clear();

            var mainColumn = new StackGroup
            {
                Orientation = Orientation.Vertical,
                Width = Width.Fixed(200),
                Children = new List<IUiElement>()
            };

            _contentStack.Children = new List<IUiElement>
                {
                    new Spacer(10, 200),
                    mainColumn,
                    new Spacer(10, 200)
                };

            mainColumn.Children.Add(new Spacer(200, 20));

            if (messages.Any())
            {
                mainColumn.Children.AddRange(messages
                    .Select(m => new Text(m, BronzeColor.Red)));
            }

            mainColumn.Children.Add(new Spacer(200, 20));
        }
    }
}
