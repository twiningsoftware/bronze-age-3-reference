﻿using System;

namespace Bronze.Common.UI
{
    public class TabData
    {
        public string IconKey { get; set; }
        public string Name { get; set; }
        public Action Populate { get; set; }
    }
}
