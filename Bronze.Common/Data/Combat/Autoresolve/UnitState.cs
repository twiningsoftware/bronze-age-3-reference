﻿using Bronze.Contracts;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.Data.Combat.Autoresolve
{
    public class UnitState
    {
        public IUnit Unit { get; private set; }
        public Army Army { get; private set; }
        public double Position { get; set; }
        public bool IsAttacker { get; private set; }
        public bool IsDead { get; set; }
        public bool IsRouting => Unit.Morale <= 0;
        public List<IndividualState> Individuals { get; private set; }
        public bool MadeMeleeAttack { get; set; }
        public bool MadeRangedAttack { get; set; }
        public bool WasAttacked { get; set; }

        public UnitState(IUnit unit, Army army, int position, bool isAttacker)
        {
            Unit = unit;
            Army = army;
            Position = position;
            IsAttacker = isAttacker;
            
            Individuals = new List<IndividualState>();
            var healthPool = unit.Health;
            while (healthPool > 0)
            {
                var health = Math.Min(healthPool, unit.Equipment.HealthPerIndividual);
                healthPool -= health;

                Individuals.Add(new IndividualState(
                    this,
                    health));
            }
        }

        public void StepSimulation(
            IEnumerable<UnitState> unitStates,
            Cell location, 
            Random random, 
            ICombatHelper combatHelper,
            double timeStep, 
            double enemyFrontLine, 
            UnitState[] enemies,
            IEnumerable<UnitState> enemyLeadUnits,
            bool isLeadUnit)
        {
            var maxRange = 0;
            if (Individuals.Any())
            {
                maxRange = Individuals.Max(i => i.MaxRange);
            }

            if (IsRouting && IsAttacker)
            {
                Position -= timeStep * Unit.GetMovementSpeed(location) * Constants.TILE_SIZE_PIXELS * Constants.COMBAT_MOVE_MOD;
            }
            else if (IsRouting && !IsAttacker)
            {
                Position += timeStep * Unit.GetMovementSpeed(location) * Constants.TILE_SIZE_PIXELS * Constants.COMBAT_MOVE_MOD;
            }
            else
            {
                if (Math.Abs(Position - enemyFrontLine) <= maxRange)
                {
                    var meleeTargets = enemyLeadUnits
                        .GroupBy(u => Math.Abs(Position - u.Position))
                        .OrderBy(g => g.Key)
                        .FirstOrDefault();

                    var rangedTargets = enemies
                        .GroupBy(u => Math.Abs(Position - u.Position))
                        .OrderBy(g => g.Key)
                        .FirstOrDefault();

                    UnitState meleeTarget = null;
                    if (meleeTargets != null && meleeTargets.Any())
                    {
                        meleeTarget = random.Choose(meleeTargets);
                    }

                    UnitState rangedTarget = null;
                    if (rangedTargets != null && rangedTargets.Any())
                    {
                        rangedTarget = random.Choose(rangedTargets);
                    }

                    var i = 0;
                    foreach (var individual in Individuals)
                    {
                        individual.StepSimulation(
                            combatHelper,
                            random,
                            timeStep,
                            meleeTarget,
                            rangedTarget,
                            i <= 4,
                            isLeadUnit);

                        i += 1;
                    }
                }
                else
                {
                    if (IsAttacker && Position < enemyFrontLine)
                    {
                        Position += timeStep * Unit.GetMovementSpeed(location) * Constants.TILE_SIZE_PIXELS * Constants.COMBAT_MOVE_MOD;
                    }
                    else if (!IsAttacker && Position > enemyFrontLine)
                    {
                        Position -= timeStep * Unit.GetMovementSpeed(location) * Constants.TILE_SIZE_PIXELS * Constants.COMBAT_MOVE_MOD;
                    }
                }

                if (IsAttacker)
                {
                    Position = Math.Min(Position, enemyFrontLine);
                }
                else
                {
                    Position = Math.Max(Position, enemyFrontLine);
                }
            }
        }

        public void ApplyDamage(Random random, double damage, bool isMelee)
        {
            var potentialRecipients = Enumerable.Empty<IndividualState>();

            if(isMelee)
            {
                potentialRecipients = Individuals.Take(4);
            }
            else
            {
                potentialRecipients = Individuals;
            }

            if(Individuals.Any())
            {
                var recipient = random.Choose(potentialRecipients);

                recipient.Health -= damage;

                if(recipient.Health <= 0)
                {
                    Individuals.Remove(recipient);

                    IsDead = !Individuals.Any();

                    if(Individuals.Any())
                    {
                        var moraleShock = 1.0 / (Individuals.Count + 1) * 2;

                        ApplyMoraleShock(moraleShock);
                    }
                }
            }
        }

        public void ApplyMoraleShock(double moraleShock)
        {
            var reduction = Util.Clamp(
                    Unit.ResolveSkill * Unit.MoraleMod / 10.0 
                    + (Army.General?.GetBonusMoraleShockReduction() ?? 0.0), 
                0, 0.9);

            var damage = (1 - reduction) * moraleShock;

            Unit.Morale -= damage;
        }
    }
}
