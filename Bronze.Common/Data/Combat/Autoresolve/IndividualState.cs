﻿using Bronze.Contracts;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.Data.Combat.Autoresolve
{
    public class IndividualState
    {
        public IEnumerable<AttackState> Attacks { get; private set; }
        private UnitState _unit;
        public double Health { get; set; }

        public int MaxRange => 0.Yield().Concat(
            Attacks
                .Where(a => a.IsMelee || a.Ammo > 0)
                .OrderByDescending(a => a.Attack.Damage)
                .Take(1)
                .Select(a => a.Attack.Range))
            .Max();

        public IndividualState(UnitState unit, double health)
        {
            _unit = unit;
            Health = health;

            Attacks = unit.Unit.MeleeAttacks.Select(a => new AttackState(a, true))
                .Concat(unit.Unit.RangedAttacks.Select(a => new AttackState(a, false)))
                .ToArray();
        }

        public void StepSimulation(
            ICombatHelper combatHelper,
            Random random, 
            double timeStep, 
            UnitState meleeTarget,
            UnitState rangedTarget,
            bool inFrontRank,
            bool couldMelee)
        {
            if (_unit.Unit.Morale > 0)
            {
                foreach (var attack in Attacks)
                {
                    attack.Cooldown -= timeStep;
                }

                if(inFrontRank)
                {
                    var meleeRange = Constants.TILE_SIZE_PIXELS.Yield()
                        .Concat(Attacks.Where(a => a.IsMelee).Select(a => a.Attack.Range))
                        .Max();

                    var inMelee = meleeTarget != null
                        && couldMelee
                        && Math.Abs(_unit.Position - meleeTarget.Position) <= meleeRange;

                    AttackState chosenAttack = null;
                    UnitState chosenTarget = null;

                    if (inMelee)
                    {
                        var targetDist = Math.Abs(_unit.Position - meleeTarget.Position);

                        var meleeAttacks = Attacks
                            .Where(a => a.IsMelee)
                            .Where(a => a.Cooldown <= 0)
                            .Where(a => a.Attack.Range >= targetDist)
                            .ToArray();

                        if (meleeAttacks.Any())
                        {
                            chosenAttack = random.Choose(meleeAttacks);
                            chosenTarget = meleeTarget;
                        }
                    }
                    else
                    {
                        var targetDist = Math.Abs(_unit.Position - meleeTarget.Position);

                        var rangedAttacks = Attacks
                            .Where(a => !a.IsMelee)
                            .Where(a => a.Cooldown <= 0)
                            .Where(a => a.Attack.Range >= targetDist)
                            .Where(a => a.Ammo > 0)
                            .ToArray();

                        if (rangedAttacks.Any())
                        {
                            chosenAttack = random.Choose(rangedAttacks);
                            chosenTarget = rangedTarget;
                        }
                    }

                    if(chosenAttack != null && chosenTarget != null)
                    {
                        chosenAttack.Cooldown = chosenAttack.Attack.Cooldown;
                        
                        chosenTarget.WasAttacked = true;

                        if (!chosenAttack.IsMelee)
                        {
                            _unit.MadeRangedAttack = true;

                            chosenAttack.Ammo -= 1;

                            var damage = combatHelper.ResolveRangedAttack(
                                random,
                                (int)Math.Round(chosenAttack.Attack.Skill * _unit.Unit.MoraleMod),
                                (int)Math.Round(chosenTarget.Unit.BlockSkill * chosenTarget.Unit.MoraleMod),
                                chosenAttack.Attack.Damage,
                                chosenTarget.Unit.Armor,
                                chosenAttack.Attack.ArmorPenetration);

                            chosenTarget.ApplyDamage(random, damage, chosenAttack.IsMelee);
                        }
                        else
                        {
                            _unit.MadeMeleeAttack = true;

                            var damage = combatHelper.ResolveMeleeAttack(
                                random,
                                (int)Math.Round(chosenAttack.Attack.Skill * _unit.Unit.MoraleMod),
                                (int)Math.Round(chosenTarget.Unit.DefenseSkill * chosenTarget.Unit.MoraleMod),
                                (int)Math.Round(chosenTarget.Unit.BlockSkill * chosenTarget.Unit.MoraleMod),
                                chosenAttack.Attack.Damage,
                                chosenTarget.Unit.Armor,
                                chosenAttack.Attack.ArmorPenetration);

                            chosenTarget.ApplyDamage(random, damage, chosenAttack.IsMelee);
                        }
                    }
                }
            }
        }
    }
}
