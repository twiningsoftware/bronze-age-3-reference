﻿using Bronze.Contracts.Data.Gamedata;

namespace Bronze.Common.Data.Combat.Autoresolve
{
    public class AttackState
    {
        public AttackInfo Attack { get; private set; }
        public double Cooldown { get; set; }
        public bool IsMelee { get; private set; }
        public int Ammo { get; set; }

        public AttackState(AttackInfo attack, bool isMelee)
        {
            Attack = attack;
            Cooldown = 0;
            IsMelee = isMelee;

            if (!isMelee)
            {
                Ammo = attack.Ammo;
            }
        }
    }
}
