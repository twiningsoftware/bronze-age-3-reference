﻿using System;
using System.Xml.Linq;
using Bronze.Common.UI;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.Data.Game.Rites
{
    public abstract class AbstractTargetCultRite : AbstractCultRite, ITargetedCultRite
    {
        private readonly IPlayerDataTracker _playerData;
        private Random _random;

        protected bool OnlyAdjacent { get; private set; }
        public string DetailedTargetingIcon { get; private set; }
        public string SummaryTargetingIcon { get; private set; }

        public AbstractTargetCultRite(
            IPlayerDataTracker playerData)
        {
            _playerData = playerData;
            _random = new Random();
        }

        public override void LoadFrom(
            IGamedataTracker gamedataTracker, 
            IXmlReaderUtil xmlReaderUtil, 
            XElement riteElement, 
            Cult forCult)
        {
            base.LoadFrom(gamedataTracker, xmlReaderUtil, riteElement, forCult);

            OnlyAdjacent = xmlReaderUtil.AttributeAsBool(riteElement, "only_adjacent");

            DetailedTargetingIcon = xmlReaderUtil.AttributeValue(riteElement, "detailed_targeting");
            SummaryTargetingIcon = xmlReaderUtil.AttributeValue(riteElement, "summary_targeting");
        }

        public override void Enact(Settlement settlement)
        {
            _playerData.MouseMode = new CultRiteTargetingMouseMode(
                this,
                settlement,
                () => _playerData.MouseMode = null);
        }

        public abstract bool IsValidFor(Settlement settlement, Cell target);

        public abstract string GetErrorMessageFor(Settlement settlement, Cell target);

        public abstract void Enact(Settlement settlement, Cell target);
    }
}
