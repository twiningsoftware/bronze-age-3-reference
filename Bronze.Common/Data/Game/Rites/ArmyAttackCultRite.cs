﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Bronze.Common.UI;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Elements;

namespace Bronze.Common.Data.Game.Rites
{
    public class ArmyAttackCultRite : AbstractTargetCultRite
    {
        private readonly IWorldManager _worldManager;
        private readonly IGamedataTracker _gamedataTracker;
        private readonly INotificationsSystem _notificationsSystem;
        private readonly IGenericNotificationBuilder _genericNotificationBuilder;
        private readonly IPlayerDataTracker _playerData;
        private readonly IUnitHelper _unitHelper;
        private readonly ICombatHelper _combatHelper;
        private Random _random;

        private Cult _forCult;
        private string _notificationDescription;
        private string _notificationIcon;
        private int _attacks;
        private AttackInfo _attackInfo;
        
        public ArmyAttackCultRite(
            IWorldManager worldManager,
            IGamedataTracker gamedataTracker,
            INotificationsSystem notificationsSystem,
            IGenericNotificationBuilder genericNotificationBuilder,
            IPlayerDataTracker playerData,
            IUnitHelper unitHelper,
            ICombatHelper combatHelper)
            :base(playerData)
        {
            _worldManager = worldManager;
            _gamedataTracker = gamedataTracker;
            _notificationsSystem = notificationsSystem;
            _genericNotificationBuilder = genericNotificationBuilder;
            _playerData = playerData;
            _unitHelper = unitHelper;
            _combatHelper = combatHelper;
            _random = new Random();
        }

        public override void LoadFrom(
            IGamedataTracker gamedataTracker,
            IXmlReaderUtil xmlReaderUtil,
            XElement riteElement,
            Cult forCult)
        {
            base.LoadFrom(gamedataTracker, xmlReaderUtil, riteElement, forCult);

            _forCult = forCult;

            _attacks = xmlReaderUtil.AttributeAsInt(riteElement, "attacks");

            _attackInfo = new AttackInfo
            {
                ArmorPenetration = xmlReaderUtil.AttributeAsInt(riteElement, "armor_penetration"),
                Damage = xmlReaderUtil.AttributeAsInt(riteElement, "damage"),
                Skill = xmlReaderUtil.AttributeAsInt(riteElement, "skill")
            };
            
            _notificationDescription = xmlReaderUtil.Element(riteElement, "notification_description").Value.Trim();

            _notificationIcon = xmlReaderUtil.AttributeValue(riteElement, "notification_icon");
        }

        public override IStackUiElement BuildTooltip(Settlement settlement)
        {
            var stack = base.BuildTooltip(settlement);

            stack.Children.Add(new Label($"Hits army with {_attacks} Attacks"));

            stack.Children.Add(UiBuilder.BuildDisplayForRangedAttack(_attackInfo));
            
            stack.Children.Add(new Label("requirements", BronzeColor.Red)
            {
                ContentUpdater = () => string.Join("\n", Requirements
                    .Select(r => r.GetErrorMessageFor(settlement))
                    .Where(s => !string.IsNullOrWhiteSpace(s)))
            });

            return stack;
        }

        public override bool IsValidFor(Settlement settlement, Cell target)
        {
            return target.WorldActors
                .OfType<Army>()
                .Where(a => settlement.Owner != a.Owner)
                .FirstOrDefault() != null
                && (!OnlyAdjacent || settlement.Region.Neighbors.Contains(target.Region) || settlement.Region == target.Region);
        }

        public override string GetErrorMessageFor(Settlement settlement, Cell target)
        {
            if(target == null 
                || target.WorldActors
                    .OfType<Army>()
                    .Where(a => settlement.Owner != a.Owner)
                    .FirstOrDefault() == null)
            {
                return "Target an army.";
            }
            
            if (OnlyAdjacent && (!settlement.Region.Neighbors.Contains(target.Region) || settlement.Region == target.Region))
            {
                return "Can only target neighboring regions.";
            }
            
            return string.Empty;
        }

        public override void Enact(Settlement sourceSettlement, Cell target)
        {
            var targetArmy = target.WorldActors
                .OfType<Army>()
                .Where(a => sourceSettlement.Owner != a.Owner)
                .FirstOrDefault();

            if (targetArmy == null)
            {
                return;
            }

            sourceSettlement.RiteCooldown = Cooldown;

            for(var i = 0; i < _attacks && targetArmy.Units.Any(); i++)
            {
                var targetUnit = _random.Choose(targetArmy.Units);

                if (targetUnit.Health > 0)
                {
                    var damage = _combatHelper.ResolveRangedAttack(
                        _random,
                        _attackInfo.Skill,
                        targetUnit.BlockSkill,
                        _attackInfo.Damage,
                        targetUnit.Armor,
                        _attackInfo.ArmorPenetration);

                    targetUnit.Health -= damage;
                }
                    
                if (targetUnit.Health <= 0)
                {
                    _unitHelper.RemoveUnit(targetArmy, targetUnit, removedViolently: true);
                }
            }
            
            if (sourceSettlement.Owner != _playerData.PlayerTribe && targetArmy.Owner == _playerData.PlayerTribe)
            {
                _notificationsSystem.AddNotification(_genericNotificationBuilder.BuildArmyNotification(
                    _notificationIcon,
                    "Foul Sorcery!",
                    $"Evil sorcery affecting {targetArmy.Name}.",
                    "Foul Sorcery!",
                    _notificationDescription,
                    targetArmy));
            }
        }
    }
}
