﻿using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Bronze.Common.Data.Game.Rites
{
    public abstract class AbstractCultRite : ICultRite
    {
        public string Id { get; private set; }
        public string Name { get; private set; }
        public string Description { get; private set; }
        public string IconKey { get; private set; }
        public double Cooldown { get; private set; }

        public List<ICultRiteRequirement> Requirements { get; private set; }
        
        public AbstractCultRite()
        {
            Requirements = new List<ICultRiteRequirement>();
        }

        public abstract void Enact(Settlement settlement);

        public virtual void LoadFrom(
            IGamedataTracker gamedataTracker, 
            IXmlReaderUtil xmlReaderUtil, 
            XElement riteElement,
            Cult forCult)
        {
            Id = xmlReaderUtil.AttributeValue(riteElement, "id");
            Name = xmlReaderUtil.AttributeValue(riteElement, "name");
            IconKey = xmlReaderUtil.AttributeValue(riteElement, "icon");
            Description = xmlReaderUtil.Element(riteElement, "description").Value.Trim();
            Cooldown = xmlReaderUtil.AttributeAsDouble(riteElement, "cooldown");

            foreach(var requirementElement in riteElement.Elements("requirement"))
            {
                var type = xmlReaderUtil.AttributeValue(requirementElement, "type");
                switch(type)
                {
                    case "membership_perc":
                        Requirements.Add(new MembershipPercentRequirement(forCult, xmlReaderUtil.AttributeAsDouble(requirementElement, "value")));
                        break;
                    case "membership_count":
                        Requirements.Add(new MembershipCountRequirement(forCult, xmlReaderUtil.AttributeAsInt(requirementElement, "value")));
                        break;
                }
            }
        }
        
        private class MembershipPercentRequirement : ICultRiteRequirement
        {
            private Cult _cult;
            private double _percent;

            public MembershipPercentRequirement(Cult cult, double percent)
            {
                _cult = cult;
                _percent = percent;
            }

            public string GetErrorMessageFor(Settlement settlement)
            {
                if(!IsValidFor(settlement))
                {
                    return $"Cult membership must be at least {_percent.ToString("P0")}";
                }

                return string.Empty;
            }

            public bool IsValidFor(Settlement settlement)
            {
                var members = settlement.CultMembership[_cult]?.Count ?? 0;

                return members / (double)settlement.Population.Count >= _percent;
            }
        }

        private class MembershipCountRequirement : ICultRiteRequirement
        {
            private Cult _cult;
            private int _count;

            public MembershipCountRequirement(Cult cult, int count)
            {
                _cult = cult;
                _count = count;
            }

            public string GetErrorMessageFor(Settlement settlement)
            {
                if (!IsValidFor(settlement))
                {
                    return $"Cult membership must be at least {_count}";
                }

                return string.Empty;
            }

            public bool IsValidFor(Settlement settlement)
            {
                return (settlement.CultMembership[_cult]?.Count ?? 0) >= _count;
            }
        }

        public virtual IStackUiElement BuildTooltip(Settlement settlement)
        {
            return new StackGroup
            {
                Orientation = Orientation.Vertical,
                Children = new List<IUiElement>
                {
                    new Label(Name),
                    new Text(Description, BronzeColor.Grey)
                    {
                        Width = Width.Fixed(200)
                    }
                }
            };
        }

        public bool RequirementsMet(Settlement settlement)
        {
            return Requirements.All(r => r.IsValidFor(settlement));
        }
    }
}
