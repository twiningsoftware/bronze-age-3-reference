﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Elements;

namespace Bronze.Common.Data.Game.Rites
{
    public class SettlementTargetBonusCultRite : AbstractTargetCultRite
    {
        private readonly IWorldManager _worldManager;
        private readonly IGamedataTracker _gamedataTracker;
        private readonly INotificationsSystem _notificationsSystem;
        private readonly IGenericNotificationBuilder _genericNotificationBuilder;
        private readonly IPlayerDataTracker _playerData;
        private Random _random;

        private Cult _forCult;
        private string _buffIcon;
        private double _buffDuration;
        private Dictionary<BonusType, double> _buffBonuses;
        private Dictionary<Cult, double> _buffInfluence;
        private bool _isHostile;
        private string _notificationDescription;
        private string _notificationIcon;

        public SettlementTargetBonusCultRite(
            IWorldManager worldManager,
            IGamedataTracker gamedataTracker,
            INotificationsSystem notificationsSystem,
            IGenericNotificationBuilder genericNotificationBuilder,
            IPlayerDataTracker playerData)
            :base(playerData)
        {
            _worldManager = worldManager;
            _gamedataTracker = gamedataTracker;
            _notificationsSystem = notificationsSystem;
            _genericNotificationBuilder = genericNotificationBuilder;
            _playerData = playerData;
            _random = new Random();
        }

        public override void LoadFrom(
            IGamedataTracker gamedataTracker,
            IXmlReaderUtil xmlReaderUtil,
            XElement riteElement,
            Cult forCult)
        {
            base.LoadFrom(gamedataTracker, xmlReaderUtil, riteElement, forCult);

            _forCult = forCult;
            
            _buffDuration = xmlReaderUtil.AttributeAsDouble(riteElement, "duration");

            _buffIcon = xmlReaderUtil.AttributeValue(riteElement, "buff_icon");

            _buffBonuses = riteElement.Elements("buff_bonus")
                .ToDictionary(
                e => xmlReaderUtil.ObjectReferenceLookup(
                    e,
                    "bonus",
                    gamedataTracker.BonusTypes,
                    b => b.Id),
                e => xmlReaderUtil.AttributeAsDouble(e, "value"));

            _buffInfluence = riteElement.Elements("buff_influence")
                .ToDictionary(
                e => xmlReaderUtil.ObjectReferenceLookup(
                    e,
                    "cult",
                    gamedataTracker.Cults,
                    c => c.Id),
                e => xmlReaderUtil.AttributeAsDouble(e, "value"));

            _isHostile = xmlReaderUtil.AttributeAsBool(riteElement, "is_hostile");

            _notificationDescription = xmlReaderUtil.Element(riteElement, "notification_description").Value.Trim();

            _notificationIcon = xmlReaderUtil.AttributeValue(riteElement, "notification_icon");
        }

        public override IStackUiElement BuildTooltip(Settlement settlement)
        {
            var stack = base.BuildTooltip(settlement);

            stack.Children.Add(new Label($"For {Util.FriendlyTimeDisplay(_buffDuration)}"));
            
            stack.Children.AddRange(
                _buffBonuses
                    .Select(kvp => new IconText(
                        kvp.Key.IconKey,

                        kvp.Value.ToString("P0"),
                        tooltip: kvp.Key.Name)));

            stack.Children.AddRange(
                _buffInfluence
                    .Select(kvp => new IconText(
                        kvp.Key.IconKey,
                        Util.PrependPositive(kvp.Value),
                        tooltip: kvp.Key.Name)));

            stack.Children.Add(new Label("requirements", BronzeColor.Red)
            {
                ContentUpdater = () => string.Join("\n", Requirements
                    .Select(r => r.GetErrorMessageFor(settlement))
                    .Where(s => !string.IsNullOrWhiteSpace(s)))
            });

            return stack;
        }

        public override bool IsValidFor(Settlement settlement, Cell target)
        {
            return target.Region.Owner != null
                && (!OnlyAdjacent || settlement.Region.Neighbors.Contains(target.Region) || settlement.Region == target.Region);
        }

        public override string GetErrorMessageFor(Settlement settlement, Cell target)
        {
            if(target.Region.Owner == null)
            {
                return "Target a settlement.";
            }

            if (OnlyAdjacent && (!settlement.Region.Neighbors.Contains(target.Region) || settlement.Region == target.Region))
            {
                return "Can only target neighboring regions.";
            }
            
            return string.Empty;
        }

        public override void Enact(Settlement sourceSettlement, Cell target)
        {
            var targetSettlement = target.Region.Settlement;

            if (targetSettlement == null)
            {
                return;
            }

            sourceSettlement.RiteCooldown = Cooldown;
            
            var buff = new Buff
            {
                Id = Id + "_buff_" + targetSettlement.Id,
                ExpireMonth = _worldManager.Month + _buffDuration,
                IconKey = _buffIcon,
                Name = Name
            };

            foreach (var kvp in _buffBonuses)
            {
                buff.AppliedBonuses[kvp.Key] = kvp.Value;
            }

            foreach (var kvp in _buffInfluence)
            {
                buff.AppliedInfluence[kvp.Key] = kvp.Value;
            }

            targetSettlement.AddBuff(buff);

            if (sourceSettlement.Owner != _playerData.PlayerTribe && targetSettlement.Owner == _playerData.PlayerTribe)
            {
                if (_isHostile)
                {
                    _notificationsSystem.AddNotification(_genericNotificationBuilder.BuildBuffNotification(
                        _notificationIcon,
                        "Foul Sorcery!",
                        $"Our wise men have detected evil sorcery affecting {targetSettlement.Name}.",
                        "Foul Sorcery!",
                        _notificationDescription,
                        targetSettlement,
                        buff));
                }
                else
                {
                    _notificationsSystem.AddNotification(_genericNotificationBuilder.BuildBuffNotification(
                        _notificationIcon,
                        "A Blessing",
                        $"Our wise men have detected a blessing affecting {targetSettlement.Name}.",
                        "A Blessing",
                        _notificationDescription,
                        targetSettlement,
                        buff));
                }
            }
        }
    }
}
