﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Elements;

namespace Bronze.Common.Data.Game.Rites
{
    public class BonusCultRite : AbstractCultRite
    {
        private string _buffIcon;
        private double _buffDuration;
        private Dictionary<BonusType, double> _buffBonuses;
        private Dictionary<Cult, double> _buffInfluence;
        private readonly IWorldManager _worldManager;

        public BonusCultRite(IWorldManager worldManager)
        {
            _worldManager = worldManager;
        }

        public override void LoadFrom(
            IGamedataTracker gamedataTracker, 
            IXmlReaderUtil xmlReaderUtil, 
            XElement riteElement, 
            Cult forCult)
        {
            base.LoadFrom(gamedataTracker, xmlReaderUtil, riteElement, forCult);

            _buffDuration = xmlReaderUtil.AttributeAsDouble(riteElement, "duration");

            _buffIcon = xmlReaderUtil.AttributeValue(riteElement, "buff_icon");

            _buffBonuses = riteElement.Elements("buff_bonus")
                .ToDictionary(
                e => xmlReaderUtil.ObjectReferenceLookup(
                    e,
                    "bonus",
                    gamedataTracker.BonusTypes,
                    b => b.Id),
                e => xmlReaderUtil.AttributeAsDouble(e, "value"));

            _buffInfluence = riteElement.Elements("buff_influence")
                .ToDictionary(
                e => xmlReaderUtil.ObjectReferenceLookup(
                    e,
                    "cult",
                    gamedataTracker.Cults,
                    c => c.Id),
                e => xmlReaderUtil.AttributeAsDouble(e, "value"));
        }

        public override void Enact(Settlement settlement)
        {
            settlement.RiteCooldown = Cooldown;

            var buff = new Buff
            {
                Id = Id + "_buff",
                ExpireMonth = _worldManager.Month + _buffDuration,
                IconKey = _buffIcon,
                Name = Name
            };

            foreach(var kvp in _buffBonuses)
            {
                buff.AppliedBonuses[kvp.Key] = kvp.Value;
            }

            foreach (var kvp in _buffInfluence)
            {
                buff.AppliedInfluence[kvp.Key] = kvp.Value;
            }
            
            settlement.AddBuff(buff);
        }

        public override IStackUiElement BuildTooltip(Settlement settlement)
        {
            var stack = base.BuildTooltip(settlement);

            stack.Children.Add(new Label($"For {Util.FriendlyTimeDisplay(_buffDuration)}"));

            
            stack.Children.AddRange(
                _buffBonuses
                    .Select(kvp => new IconText(
                        kvp.Key.IconKey,
                        
                        kvp.Value.ToString("P0"),
                        tooltip: kvp.Key.Name)));

            stack.Children.AddRange(
                _buffInfluence
                    .Select(kvp => new IconText(
                        kvp.Key.IconKey,
                        Util.PrependPositive(kvp.Value),
                        tooltip: kvp.Key.Name)));

            stack.Children.Add(new Label("requirements", BronzeColor.Red)
            {
                ContentUpdater = () => string.Join("\n", Requirements
                    .Select(r => r.GetErrorMessageFor(settlement))
                    .Where(s => !string.IsNullOrWhiteSpace(s)))
            });

            return stack;
        }
    }
}
