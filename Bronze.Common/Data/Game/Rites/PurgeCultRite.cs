﻿using System;
using System.Linq;
using System.Xml.Linq;
using Bronze.Common.Data.World.CellDevelopments;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Elements;

namespace Bronze.Common.Data.Game.Rites
{
    public class PurgeCultRite : AbstractCultRite
    {
        private readonly IWorldManager _worldManager;
        private readonly IGamedataTracker _gamedataTracker;
        private readonly INotificationsSystem _notificationsSystem;
        private readonly IGenericNotificationBuilder _genericNotificationBuilder;
        private readonly IPlayerDataTracker _playerData;
        private Random _random;

        private Cult _forCult;
        private int _purgeCount;
        private Cult[] _cultsToPurge;
        private string _notificationIcon;
        private BonusType _strifeBonus;
        private string _buffIcon;

        public PurgeCultRite(
            IWorldManager worldManager,
            IGamedataTracker gamedataTracker,
            INotificationsSystem notificationsSystem,
            IGenericNotificationBuilder genericNotificationBuilder,
            IPlayerDataTracker playerData)
        {
            _worldManager = worldManager;
            _gamedataTracker = gamedataTracker;
            _notificationsSystem = notificationsSystem;
            _genericNotificationBuilder = genericNotificationBuilder;
            _playerData = playerData;
            _random = new Random();
        }

        public override void LoadFrom(
            IGamedataTracker gamedataTracker, 
            IXmlReaderUtil xmlReaderUtil, 
            XElement riteElement, 
            Cult forCult)
        {
            base.LoadFrom(gamedataTracker, xmlReaderUtil, riteElement, forCult);

            _forCult = forCult;

            _purgeCount = xmlReaderUtil.AttributeAsInt(riteElement, "purge_count");

            _notificationIcon = xmlReaderUtil.AttributeValue(riteElement, "notification_icon");

            _cultsToPurge = _gamedataTracker.Cults
                .Where(c => forCult.CultRelations[c] < 0)
                .ToArray();

            _strifeBonus = xmlReaderUtil.ObjectReferenceLookup(
                riteElement,
                "strife_bonus",
                _gamedataTracker.BonusTypes,
                b => b.Id);

            _buffIcon = xmlReaderUtil.AttributeValue(riteElement, "buff_icon");
        }

        public override void Enact(Settlement settlement)
        {
            settlement.RiteCooldown = Cooldown;

            var targets = settlement.Population
                .Where(p => _cultsToPurge.Contains(p.CultMembership))
                .ToList();

            var count = 0;
            while(targets.Any() && count < _purgeCount)
            {
                var victim = _random.Choose(targets);

                targets.Remove(victim);
                count += 1;
                settlement.Population.Remove(victim);
                settlement.SetCalculationFlag("pop_removed");
            }

            if(settlement.Owner == _playerData.PlayerTribe)
            {
                var settlementLocation = settlement.EconomicActors
                    .OfType<ICellDevelopment>()
                    .Where(cd => cd is DistrictDevelopment || cd is VillageDevelopment)
                    .Select(cd => cd.Cell.Position)
                    .FirstOrDefault();

                var settlementBuff = new Buff
                {
                    Id = Id + "buff",
                    ExpireMonth = _worldManager.Month + 6,
                    IconKey = _buffIcon,
                    Name = "Purges",
                    Description = "Recent purges have upset the populous.",
                };
                settlementBuff.AppliedBonuses[_strifeBonus] = -1 * (count + 1); ;

                settlement.AddBuff(settlementBuff);

                if(count > 0)
                {
                    _notificationsSystem.AddNotification(_genericNotificationBuilder
                        .BuildSimpleNotification(
                            _notificationIcon,
                            $"Purge in {settlement.Name}",
                            $"Purge claims {count} lives in {settlement.Name}.",
                            $"Purge in {settlement.Name}",
                            $"A purge lead by the {_forCult.Name} claims {count} lives in {settlement.Name}.",
                            settlementLocation));
                }
                else
                {
                    _notificationsSystem.AddNotification(_genericNotificationBuilder
                        .BuildSimpleNotification(
                            _notificationIcon,
                            $"Purge in {settlement.Name}",
                            $"Attempted purge in {settlement.Name}.",
                            $"Purge in {settlement.Name}",
                            $"A purge lead by the {_forCult.Name} swept through {settlement.Name}, but failed to find any victims.",
                            settlementLocation));
                }
            }
        }

        public override IStackUiElement BuildTooltip(Settlement settlement)
        {
            var stack = base.BuildTooltip(settlement);

            stack.Children.Add(new Label($"Target Cults"));
            var perRow = 7;
            var purgeOptions = _cultsToPurge
                .Where(c => settlement.CultMembership[c].Count > 0)
                .ToArray();

            for(var i = 0; i < purgeOptions.Length; i += perRow)
            {
                stack.Children.Add(new StackGroup
                {
                    Orientation = Bronze.UI.Data.Orientation.Horizontal,
                    Children = purgeOptions.Skip(i).Take(perRow)
                        .Select(c => new IconText(c.IconKey, string.Empty, tooltip: c.Name))
                        .ToList<IUiElement>()
                });
            }

            stack.Children.Add(new Label("requirements", BronzeColor.Red)
            {
                ContentUpdater = () => string.Join("\n", Requirements
                    .Select(r => r.GetErrorMessageFor(settlement))
                    .Where(s => !string.IsNullOrWhiteSpace(s)))
            });

            return stack;
        }
    }
}
