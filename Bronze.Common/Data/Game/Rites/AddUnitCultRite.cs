﻿using System.Linq;
using System.Xml.Linq;
using Bronze.Common.Data.World.CellDevelopments;
using Bronze.Common.UI;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Elements;

namespace Bronze.Common.Data.Game.Rites
{
    public class AddUnitCultRite : AbstractCultRite
    {
        private IUnitType _unitType;
        private UnitEquipmentSet _equipmentSet;

        private readonly IUnitHelper _unitHelper;
        
        public AddUnitCultRite(
            IUnitHelper unitHelper)
        {
            _unitHelper = unitHelper;
        }

        public override void LoadFrom(
            IGamedataTracker gamedataTracker, 
            IXmlReaderUtil xmlReaderUtil, 
            XElement riteElement, 
            Cult forCult)
        {
            base.LoadFrom(gamedataTracker, xmlReaderUtil, riteElement, forCult);

            _unitType = xmlReaderUtil.ObjectReferenceLookup(
                riteElement,
                "unit_type",
                gamedataTracker.UnitTypes,
                u => u.Id);

            _equipmentSet = xmlReaderUtil.ObjectReferenceLookup(
                riteElement,
                "equipment_set",
                _unitType.EquipmentSets,
                es => es.Level.ToString());
        }

        public override void Enact(Settlement settlement)
        {
            settlement.RiteCooldown = Cooldown;

            var settlementPosition = settlement.EconomicActors
                .OfType<ICellDevelopment>()
                .Where(cd => cd is DistrictDevelopment || cd is VillageDevelopment)
                .Select(cd => cd.Cell.Position)
                .FirstOrDefault();

            var spawnPos = settlement.Region.Cells
                .Where(c => _equipmentSet.CanPath(c))
                .OrderBy(c => c.Position.DistanceSq(settlementPosition))
                .FirstOrDefault();

            if(spawnPos != null)
            {
                var army = _unitHelper.CreateArmy(settlement.Owner, spawnPos);

                army.Units.Add(_unitType.CreateUnit(_equipmentSet));
            }
        }

        public override IStackUiElement BuildTooltip(Settlement settlement)
        {
            var stack = base.BuildTooltip(settlement);

            stack.Children.Add(UiBuilder.BuildDisplayFor(_unitType, _equipmentSet, settlement.Owner.PrimaryColor));

            
            stack.Children.Add(new Label("requirements", BronzeColor.Red)
            {
                ContentUpdater = () => string.Join("\n", Requirements
                    .Select(r => r.GetErrorMessageFor(settlement))
                    .Where(s => !string.IsNullOrWhiteSpace(s)))
            });

            return stack;
        }
    }
}
