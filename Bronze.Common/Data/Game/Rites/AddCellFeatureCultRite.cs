﻿using System;
using System.Linq;
using System.Xml.Linq;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Elements;

namespace Bronze.Common.Data.Game.Rites
{
    public class AddCellFeatureCultRite : AbstractCultRite
    {
        private string _notificationIcon;
        private ICellFeature[] _featureOptions;
        private Trait[] _requiredTraits;
        private Trait[] _forbiddenTraits;
        private Cult _forCult;

        private readonly INotificationsSystem _notificationsSystem;
        private readonly IGenericNotificationBuilder _genericNotificationBuilder;
        private readonly IPlayerDataTracker _playerData;

        public AddCellFeatureCultRite(
            INotificationsSystem notificationsSystem,
            IGenericNotificationBuilder genericNotificationBuilder,
            IPlayerDataTracker playerData)
        {
            _notificationsSystem = notificationsSystem;
            _genericNotificationBuilder = genericNotificationBuilder;
            _playerData = playerData;
        }

        public override void LoadFrom(
            IGamedataTracker gamedataTracker, 
            IXmlReaderUtil xmlReaderUtil, 
            XElement riteElement, 
            Cult forCult)
        {
            base.LoadFrom(gamedataTracker, xmlReaderUtil, riteElement, forCult);

            _featureOptions = riteElement
                .Elements("option")
                .Select(e => xmlReaderUtil.ObjectReferenceLookup(
                    e,
                    "cell_feature",
                    gamedataTracker.CellFeatures,
                    cf => cf.Id))
                .ToArray();

            _requiredTraits = riteElement
                .Elements("required")
                .Select(e => xmlReaderUtil.ObjectReferenceLookup(
                    e,
                    "trait",
                    gamedataTracker.Traits,
                    t => t.Id))
                .ToArray();

            _forbiddenTraits = riteElement
                .Elements("forbidden")
                .Select(e => xmlReaderUtil.ObjectReferenceLookup(
                    e,
                    "trait",
                    gamedataTracker.Traits,
                    t => t.Id))
                .ToArray();

            _notificationIcon = xmlReaderUtil.AttributeValue(riteElement, "notification_icon");

            _forCult = forCult;
        }

        public override void Enact(Settlement settlement)
        {
            settlement.RiteCooldown = Cooldown;

            var cellOptions = settlement.Region.Cells
                .Where(c => c.Feature == null)
                .Where(c => c.Traits.ContainsAll(_requiredTraits))
                .Where(c => !c.Traits.ContainsAny(_forbiddenTraits))
                .ToArray();

            if (cellOptions.Any())
            {
                var rnd = new Random();
                var cell = rnd.Choose(cellOptions);
                var feature = rnd.Choose(_featureOptions);

                cell.Feature = feature;

                if (_playerData.PlayerTribe == settlement.Owner)
                {
                    _notificationsSystem.AddNotification(_genericNotificationBuilder
                        .BuildLookAtNotification(
                            _notificationIcon,
                            $"{feature.Name} discovered!",
                            cell.Position));
                }
            }
            else
            {
                if (_playerData.PlayerTribe == settlement.Owner)
                {
                    _notificationsSystem.AddNotification(_genericNotificationBuilder
                        .BuildSimpleNotification(
                            _notificationIcon,
                            $"{Name} Failed!",
                            "A cult rite has failed.",
                            $"{Name} Failed!",
                            "The wise men say that the gods cannot help us.",
                            null));
                }
            }
        }

        public override IStackUiElement BuildTooltip(Settlement settlement)
        {
            var stack = base.BuildTooltip(settlement);
            
            stack.Children.Add(new Label("requirements", BronzeColor.Red)
            {
                ContentUpdater = () => string.Join("\n", Requirements
                    .Select(r => r.GetErrorMessageFor(settlement))
                    .Where(s => !string.IsNullOrWhiteSpace(s)))
            });

            return stack;
        }
    }
}
