﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Bronze.Common.Data.World.Units;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Elements;

namespace Bronze.Common.Data.Game.Units
{
    public class ArmyRecoverAction : IArmyAction
    {
        public string Icon => "ui_action_interact";
        public string Name => "Recover";
        public string Description => "Post battle recovery.";
        public bool PathAdjacent => true;
        
        private readonly IWorldManager _worldManager;
        private readonly IBattleHelper _battleHelper;

        public ArmyRecoverAction(
            IWorldManager worldManager,
            IBattleHelper battleHelper)
        {
            _worldManager = worldManager;
            _battleHelper = battleHelper;
        }
        
        public IUiElement BuildDescriptionStack()
        {
            return new Label("Recovering");
        }

        public bool IsValidFor(Army army, Cell target)
        {
            return !GetValidationMessagesFor(army, target).Any();
        }

        public bool CouldBeValidFor(Tribe tribe, Cell target)
        {
            return true;
        }

        public bool IsValidForQuickAction(Army army, Cell target)
        {
            return IsValidFor(army, target);
        }

        public IEnumerable<string> GetValidationMessagesFor(Army army, Cell target)
        {
            return Enumerable.Empty<string>();
        }
        
        public void LoadFrom(IXmlReaderUtil xmlReaderUtil, Race race, XElement actionElement)
        {
        }

        public IArmyOrder BuildOrderFor(Army army, Cell target)
        {
            //var targetArmy = target.WorldActors.OfType<Army>()
            //    .Where(a => a.Owner != army.Owner)
            //    .FirstOrDefault();

            //return new ArmyRecoverOrder(army, false, army.CurrentOrder, duration);
            throw new NotImplementedException();
        }

        public IArmyOrder DeserializeOrderFrom(
            IGamedataTracker gamedataTracker, 
            IEnumerable<Region> regions, 
            Tribe tribe, 
            Army army, 
            SerializedObject orderRoot)
        {
            return ArmyRecoverOrder.DeserializeFrom(regions, army, orderRoot, _worldManager, gamedataTracker, _battleHelper);
        }
    }
}
