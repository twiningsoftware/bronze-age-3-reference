﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Bronze.Common.Data.World.Units;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Elements;

namespace Bronze.Common.Data.Game.Units
{
    public class ArmySettleAction : IArmyAction
    {
        public string Icon => "ui_action_settle";
        public string Name => "Settle";
        public string Description => "Settle down to found a new settlement, the part of the army will become the starting population.";
        public bool PathAdjacent => false;

        public Race SettlementRace { get; private set; }
        public Caste StartingPopCaste { get; private set; }
        public int StartingPopCount { get; private set; }
        public double SettleMonths { get; private set; }

        public ICellDevelopmentType CellDevelopmentType { get; private set; }

        private readonly IUnitHelper _unitHelper;
        private readonly INameSource _nameSource;

        public ArmySettleAction(
            IUnitHelper unitHelper,
            INameSource nameSource)
        {
            _unitHelper = unitHelper;
            _nameSource = nameSource;
        }
        
        public IUiElement BuildDescriptionStack()
        {
            return new StackGroup
            {
                Orientation = Bronze.UI.Data.Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new Label(SettlementRace.Name + " Settlement"),
                    new Spacer(10, 0),
                    new IconText(StartingPopCaste.IconKey, StartingPopCount.ToString(), tooltip: $"Starting population: {StartingPopCount} {StartingPopCaste.NamePlural}"),
                }
            };
        }

        public IEnumerable<string> GetValidationMessagesFor(Army army, Cell target)
        {
            return CellDevelopmentType.GetPlacementMessages(army.Owner, target);
        }

        public bool IsValidFor(Army army, Cell target)
        {
            return !GetValidationMessagesFor(army, target).Any();
        }

        public bool CouldBeValidFor(Tribe tribe, Cell target)
        {
            return !CellDevelopmentType.GetPlacementMessages(tribe, target).Any();
        }

        public bool IsValidForQuickAction(Army army, Cell target)
        {
            return IsValidFor(army, target);
        }

        public void LoadFrom(IXmlReaderUtil xmlReaderUtil, Race race, XElement actionElement)
        {
            CellDevelopmentType = xmlReaderUtil.ObjectReferenceLookup(
                actionElement,
                "development",
                race.AvailableCellDevelopments,
                d => d.Id);

            StartingPopCaste = xmlReaderUtil.ObjectReferenceLookup(
                actionElement,
                "pop_caste",
                race.Castes,
                d => d.Id);

            StartingPopCount = xmlReaderUtil.AttributeAsInt(actionElement, "pop_count");

            SettleMonths = xmlReaderUtil.AttributeAsDouble(actionElement, "months_to_settle");

            SettlementRace = race;
        }

        public IArmyOrder BuildOrderFor(Army army, Cell target)
        {
            return new SettleVillageOrder(army, target, this, _unitHelper, _nameSource);
        }

        public IArmyOrder DeserializeOrderFrom(
            IGamedataTracker gamedataTracker, 
            IEnumerable<Region> regions, 
            Tribe tribe, 
            Army army, 
            SerializedObject orderRoot)
        {
            return SettleVillageOrder.DeserializeFrom(
                _unitHelper,
                _nameSource,
                regions,
                army,
                this,
                orderRoot);
        }
    }
}
