﻿using Bronze.Common.Data.World.Units;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System.Linq;

namespace Bronze.Common.Data.Game.Units
{
    public class SimpleUnitType : AbstractUnitType
    {
        private readonly IInjectionProvider _injectionProvider;

        public override bool CanBeTrained => true;

        public SimpleUnitType(IInjectionProvider injectionProvider)
        {
            _injectionProvider = injectionProvider;
        }
        
        public override IUnit CreateUnit(UnitEquipmentSet equipmentSet)
        {
            return _injectionProvider
                .Build<SimpleUnit>()
                .Initialize(this, equipmentSet);
        }

        public override IUnit DeserializeFrom(
            IGamedataTracker gamedataTracker,
            SerializedObject unitRoot)
        {
            var equipment = unitRoot
                .GetOptionalObjectReference(
                    "equipment_level",
                    EquipmentSets,
                    es => es.Level.ToString())
                    ?? EquipmentSets.First();

            var unit = _injectionProvider
                .Build<SimpleUnit>()
                .Initialize(this, equipment);

            unit.DeserializeFrom(gamedataTracker, unitRoot);

            return unit;
        }
    }
}
