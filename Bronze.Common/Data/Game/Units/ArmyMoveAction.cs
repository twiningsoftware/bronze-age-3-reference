﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Bronze.Common.Data.World.Units;
using Bronze.Contracts;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Elements;

namespace Bronze.Common.Data.Game.Units
{
    public class ArmyMoveAction : IArmyAction
    {
        public string Icon => "ui_action_move";
        public string Name => "Move";
        public string Description => "Move to a new position.";
        public bool PathAdjacent => false;

        public IUiElement BuildDescriptionStack()
        {
            return new StackGroup
            {
            };
        }

        public bool IsValidFor(Army army, Cell target)
        {
            return army.CanPath(target);
        }

        public bool CouldBeValidFor(Tribe tribe, Cell target)
        {
            return true;
        }

        public bool IsValidForQuickAction(Army army, Cell target)
        {
            return IsValidFor(army, target);
        }

        public IEnumerable<string> GetValidationMessagesFor(Army army, Cell target)
        {
            if(!army.CanPath(target))
            {
                return "Army cannot travel there.".Yield();
            }

            return Enumerable.Empty<string>();
        }
        
        public void LoadFrom(IXmlReaderUtil xmlReaderUtil, Race race, XElement actionElement)
        {
        }

        public IArmyOrder BuildOrderFor(Army army, Cell target)
        {
            return new ArmyMoveOrder(army, target);
        }

        public IArmyOrder DeserializeOrderFrom(
            IGamedataTracker gamedataTracker, 
            IEnumerable<Region> regions, 
            Tribe tribe, 
            Army army, 
            SerializedObject orderRoot)
        {
            return ArmyMoveOrder.DeserializeFrom(regions, army, orderRoot);
        }
    }
}
