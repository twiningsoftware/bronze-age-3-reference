﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Bronze.Common.Data.World.Units;
using Bronze.Contracts;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Elements;

namespace Bronze.Common.Data.Game.Units
{
    public class ArmyApproachAction : IArmyAction
    {
        public string Icon => "ui_action_interact";
        public string Name => "Approach";
        public string Description => "Approach another army.";
        public bool PathAdjacent => true;
        
        private readonly IWorldManager _worldManager;

        public ArmyApproachAction(
            IWorldManager worldManager)
        {
            _worldManager = worldManager;
        }
        
        public IUiElement BuildDescriptionStack()
        {
            return new Label("Approaching");
        }

        public bool IsValidFor(Army army, Cell target)
        {
            return !GetValidationMessagesFor(army, target).Any();
        }

        public bool CouldBeValidFor(Tribe tribe, Cell target)
        {
            return true;
        }

        public bool IsValidForQuickAction(Army army, Cell target)
        {
            return IsValidFor(army, target);
        }

        public IEnumerable<string> GetValidationMessagesFor(Army army, Cell target)
        {
            var targetArmy = target.WorldActors.OfType<Army>()
                .Where(a => a.Owner != army.Owner)
                .FirstOrDefault();

            if(targetArmy == null)
            {
                return "No army to approach.".Yield();
            }
            
            if (!target.OrthoNeighbors.Any(n => army.CanPath(n)))
            {
                return "Army cannot travel there.".Yield();
            }
            
            return Enumerable.Empty<string>();
        }
        
        public void LoadFrom(IXmlReaderUtil xmlReaderUtil, Race race, XElement actionElement)
        {
        }

        public IArmyOrder BuildOrderFor(Army army, Cell target)
        {
            var targetArmy = target.WorldActors.OfType<Army>()
                .Where(a => a.Owner != army.Owner)
                .FirstOrDefault();

            return new ArmyApproachOrder(army, targetArmy, _worldManager);
        }

        public IArmyOrder DeserializeOrderFrom(
            IGamedataTracker gamedataTracker, 
            IEnumerable<Region> regions, 
            Tribe tribe, 
            Army army, 
            SerializedObject orderRoot)
        {
            return ArmyApproachOrder.DeserializeFrom(regions, army, orderRoot, _worldManager);
        }
    }
}
