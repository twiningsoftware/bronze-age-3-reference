﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.Exceptions;
using Bronze.Contracts.InjectableServices;
using System.Linq;
using System.Xml.Linq;

namespace Bronze.Common.Data.Game.Units
{
    public abstract class AbstractUnitType : IUnitType 
    {
        public string Id { get; private set; }
        public UnitCategory Category { get; private set; }
        public string Name { get; private set; }
        public string Description { get; private set; }
        public UnitEquipmentSet[] EquipmentSets { get; private set; }
        public abstract bool CanBeTrained { get; }
        public RecruitmentSlotCategory RecruitmentCategory { get; private set; }

        public abstract IUnit CreateUnit(UnitEquipmentSet equipmentSet);

        public abstract IUnit DeserializeFrom(
            IGamedataTracker gamedataTracker,
            SerializedObject unitRoot);

        public void LoadFrom(
            IGamedataTracker gamedataTracker, 
            IXmlReaderUtil xmlReaderUtil,
            IInjectionProvider injectionProvider,
            XElement element)
        {
            var forRace = xmlReaderUtil.ObjectReferenceLookup(
                element,
                "race",
                gamedataTracker.Races,
                r => r.Id);

            forRace.AvailableUnits.Add(this);

            LoadFrom(gamedataTracker, xmlReaderUtil, injectionProvider, element, forRace);
        }

        protected virtual void LoadFrom(
            IGamedataTracker gamedataTracker, 
            IXmlReaderUtil xmlReaderUtil,
            IInjectionProvider injectionProvider,
            XElement element, 
            Race forRace)
        {
            Id = xmlReaderUtil.AttributeValue(element, "id");
            Name = xmlReaderUtil.AttributeValue(element, "name");
            
            Category = xmlReaderUtil.ObjectReferenceLookup(
                element,
                "category",
                gamedataTracker.UnitCategories,
                c => c.Id);

            RecruitmentCategory = xmlReaderUtil.ObjectReferenceLookup(
                element,
                "recruitment_category",
                gamedataTracker.RecruitmentSlotCategories,
                c => c.Id);


            Description = xmlReaderUtil.Element(element, "description").Value.Trim();
            
            EquipmentSets = element
                .Elements("equipment")
                .Select(e => LoadEquipmentSet(gamedataTracker, xmlReaderUtil, injectionProvider, e, forRace))
                .OrderBy(es => es.Level)
                .ToArray();

            if(!EquipmentSets.Any())
            {
                throw DataLoadException.MiscError(element, "Unit types must have at least one 'equipment' element.");
            }
        }

        private UnitEquipmentSet LoadEquipmentSet(
            IGamedataTracker gamedataTracker, 
            IXmlReaderUtil xmlReaderUtil, 
            IInjectionProvider injectionProvider,
            XElement element,
            Race forRace)
        {
            var equipment = new UnitEquipmentSet
            {
                Level = xmlReaderUtil.AttributeAsInt(element, "level"),
                Name = xmlReaderUtil.AttributeValue(element, "name"),
                IconKey = xmlReaderUtil.AttributeValue(element, "icon"),
                PortraitKey = xmlReaderUtil.AttributeValue(element, "portrait"),
                Description = xmlReaderUtil.Element(element, "description").Value.Trim(),
                VisionRange = xmlReaderUtil.AttributeAsInt(xmlReaderUtil.Element(element, "vision"), "range"),
                AvailableActions = new IArmyAction[]
                    {
                        new ArmyMoveAction(),
                        injectionProvider.BuildNamed<IArmyAction>("ArmyInteractAction"),
                        injectionProvider.BuildNamed<IArmyAction>("ArmyApproachAction"),
                        injectionProvider.BuildNamed<IArmyAction>("ArmyMergeAction"),
                        injectionProvider.BuildNamed<IArmyAction>("ArmySkirmishAction")
                    }
                    .Concat(element
                        .Elements("action")
                        .Select(e => LoadActionElement(xmlReaderUtil, injectionProvider, forRace, e)))
                    .ToArray(),
                RecruitmentCost = xmlReaderUtil.Element(element, "recruitment")
                    .Elements("equipment")
                    .Select(e => new ItemQuantity(
                        xmlReaderUtil.ObjectReferenceLookup(
                            e,
                            "item",
                            gamedataTracker.Items,
                            i => i.Name),
                        xmlReaderUtil.AttributeAsInt(e, "amount")))
                    .ToArray()
            };

            LoadMovementElement(
                gamedataTracker,
                xmlReaderUtil,
                equipment,
                xmlReaderUtil.Element(element, "movement"));

            LoadCombatElement(
                gamedataTracker,
                xmlReaderUtil,
                equipment,
                xmlReaderUtil.Element(element, "combat"));

            LoadIndividualsElement(
                gamedataTracker,
                xmlReaderUtil,
                equipment,
                xmlReaderUtil.Element(element, "individuals"));
            
            LoadUpkeepElement(
                gamedataTracker,
                xmlReaderUtil,
                equipment,
                xmlReaderUtil.Element(element, "upkeep"));

            return equipment;
        }

        protected virtual void LoadMovementElement(
            IGamedataTracker gamedataTracker,
            IXmlReaderUtil xmlReaderUtil,
            UnitEquipmentSet equipmentSet,
            XElement movementElement)
        {
            equipmentSet.MovementModes = movementElement
                .Elements("movement_mode")
                .Select(e => new MovementMode
                {
                    MovementType = xmlReaderUtil
                        .ObjectReferenceLookup(
                            e,
                            "type",
                            gamedataTracker.MovementTypes,
                            x => x.Id),
                    Speed = xmlReaderUtil.AttributeAsDouble(e, "speed")
                })
                .ToArray();
        }

        protected virtual void LoadCombatElement(
            IGamedataTracker gamedataTracker, 
            IXmlReaderUtil xmlReaderUtil,
            UnitEquipmentSet equipmentSet,
            XElement combatElement)
        {
            equipmentSet.IndividualCount = xmlReaderUtil.AttributeAsInt(
                combatElement,
                "individuals");

            equipmentSet.HealthPerIndividual = xmlReaderUtil.AttributeAsInt(
                combatElement,
                "health");

            equipmentSet.Armor = xmlReaderUtil.AttributeAsInt(
                combatElement,
                "armor");

            equipmentSet.StrengthEstimate = xmlReaderUtil.AttributeAsDouble(
                combatElement,
                "strength_estimate");

            equipmentSet.SkirmishSkill = xmlReaderUtil.AttributeAsInt(
                combatElement,
                "skirmish");

            equipmentSet.BlockSkill = xmlReaderUtil.AttributeAsInt(
                combatElement,
                "block");

            equipmentSet.DefenseSkill = xmlReaderUtil.AttributeAsInt(
                combatElement,
                "defense");

            equipmentSet.ResolveSkill = xmlReaderUtil.AttributeAsInt(
                combatElement,
                "resolve");

            equipmentSet.MeleeAttacks = combatElement
                .Elements("melee")
                .Select(e => LoadAttackInfo(xmlReaderUtil, e, true))
                .ToArray();

            equipmentSet.RangedAttacks = combatElement
                .Elements("ranged")
                .Select(e => LoadAttackInfo(xmlReaderUtil, e, false))
                .ToArray();
        }

        private AttackInfo LoadAttackInfo(
            IXmlReaderUtil xmlReaderUtil, 
            XElement element,
            bool isMelee)
        {
            var attack = new AttackInfo
            {
                AnimationName = xmlReaderUtil.AttributeValue(element, "animation"),
                Skill = xmlReaderUtil.AttributeAsInt(element, "skill"),
                Damage = xmlReaderUtil.AttributeAsInt(element, "damage"),
                Cooldown = xmlReaderUtil.AttributeAsDouble(element, "cooldown"),
                Range = xmlReaderUtil.AttributeAsInt(element, "range"),
                ArmorPenetration = xmlReaderUtil.AttributeAsInt(element, "armor_penetration"),
            };

            if(!isMelee)
            {
                attack.Ammo = xmlReaderUtil.AttributeAsInt(element, "ammo");
            }

            return attack;
        }

        protected virtual void LoadIndividualsElement(
            IGamedataTracker gamedataTracker, 
            IXmlReaderUtil xmlReaderUtil,
            UnitEquipmentSet equipmentSet,
            XElement individualsElement)
        {
            equipmentSet.IndividualAnimations = individualsElement
                .Elements("animation")
                .Select(e => xmlReaderUtil.LoadIndividualAnimation(e))
                .ToArray();
        }
        
        protected virtual void LoadUpkeepElement(
            IGamedataTracker gamedataTracker, 
            IXmlReaderUtil xmlReaderUtil,
            UnitEquipmentSet equipmentSet,
            XElement upkeepElement)
        {
            equipmentSet.SupplyCapacity = xmlReaderUtil.AttributeAsInt(upkeepElement, "supply_capacity");

            equipmentSet.UpkeepNeeds = upkeepElement
                .Elements("input")
                .Select(x => xmlReaderUtil.LoadItemRate(x))
                .ToArray();
        }

        protected virtual IArmyAction LoadActionElement(
            IXmlReaderUtil xmlReaderUtil,
            IInjectionProvider injectionProvider,
            Race race,
            XElement actionElement)
        {
            var typeName = xmlReaderUtil.AttributeValue(actionElement, "type");

            var actionType = injectionProvider.BuildNamed<IArmyAction>(typeName);

            actionType.LoadFrom(xmlReaderUtil, race, actionElement);

            return actionType;
        }

        public void PostLink(
            IGamedataTracker gamedataTracker, 
            IXmlReaderUtil xmlReaderUtil, 
            XElement element)
        {
        }
    }
}
