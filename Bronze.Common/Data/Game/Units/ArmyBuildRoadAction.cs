﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Bronze.Common.Data.World.Units;
using Bronze.Contracts;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Elements;

namespace Bronze.Common.Data.Game.Units
{
    public class ArmyBuildRoadAction : IArmyAction
    {
        public string Icon => "ui_action_roads";
        public string Name => "Build Roads";
        public string Description => "Travel along a path, building roads as the army moves.";
        public bool PathAdjacent => false;

        public IEnumerable<RoadOption> RoadOptions { get; private set; }

        public ArmyBuildRoadAction()
        {
        }
        
        public IUiElement BuildDescriptionStack()
        {
            return new StackGroup
            {
                Orientation = Bronze.UI.Data.Orientation.Horizontal
            };
        }

        public bool IsValidFor(Army army, Cell target)
        {
            return army.CanPath(target);
        }

        public bool CouldBeValidFor(Tribe tribe, Cell target)
        {
            return true;
        }

        public bool IsValidForQuickAction(Army army, Cell target)
        {
            return IsValidFor(army, target);
        }

        public IEnumerable<string> GetValidationMessagesFor(Army army, Cell target)
        {
            if (!army.CanPath(target))
            {
                return "Army cannot travel there.".Yield();
            }

            return Enumerable.Empty<string>();
        }

        public void LoadFrom(IXmlReaderUtil xmlReaderUtil, Race race, XElement actionElement)
        {
            RoadOptions = actionElement.Elements("option")
                .Select(e => new RoadOption
                {
                    BuildMonths = xmlReaderUtil.AttributeAsDouble(e, "build_months"),
                    CellDevelopmentType = xmlReaderUtil.ObjectReferenceLookup(
                        e,
                        "development",
                        race.AvailableCellDevelopments,
                        d => d.Id)
                })
                .ToArray();
        }

        public IArmyOrder BuildOrderFor(Army army, Cell target)
        {
            return new BuildRoadOrder(army, target, this);
        }

        public IArmyOrder DeserializeOrderFrom(
            IGamedataTracker gamedataTracker, 
            IEnumerable<Region> regions, 
            Tribe tribe, 
            Army army, 
            SerializedObject orderRoot)
        {
            return BuildRoadOrder.DeserializeFrom(
                regions,
                army,
                this,
                orderRoot);
        }
        
        public class RoadOption
        {
            public ICellDevelopmentType CellDevelopmentType { get; set; }
            public double BuildMonths { get; set; }

        }
    }
}
