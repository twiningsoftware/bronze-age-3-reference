﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Bronze.Common.Data.World.Units;
using Bronze.Contracts;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Elements;

namespace Bronze.Common.Data.Game.Units
{
    public class ArmyEatCorpseAction : IArmyAction
    {
        public string Icon => "ui_action_eatcorpse";
        public string Name => "Eat Corpses";
        public string Description => "Eat corpses to heal and resupply.";
        public bool PathAdjacent => false;
        
        public ArmyEatCorpseAction()
        {
        }
        
        public IUiElement BuildDescriptionStack()
        {
            return new StackGroup
            {
            };
        }

        public bool IsValidFor(Army army, Cell target)
        {
            return !GetValidationMessagesFor(army, target).Any();
        }

        public bool CouldBeValidFor(Tribe tribe, Cell target)
        {
            return target.WorldActors.OfType<WorldCorpse>().Any();
        }

        public bool IsValidForQuickAction(Army army, Cell target)
        {
            return IsValidFor(army, target);
        }

        public IEnumerable<string> GetValidationMessagesFor(Army army, Cell target)
        {
            if(!target.WorldActors.OfType<WorldCorpse>().Any())
            {
                yield return "No corpses to eat.";
            }
            
            if(!army.CanPath(target))
            {
                yield return "Army cannot travel there.";
            }
        }
        
        public void LoadFrom(IXmlReaderUtil xmlReaderUtil, Race race, XElement actionElement)
        {
        }

        public IArmyOrder BuildOrderFor(Army army, Cell target)
        {
            return new ArmyEatCorpseOrder(army, target);
        }

        public IArmyOrder DeserializeOrderFrom(
            IGamedataTracker gamedataTracker, 
            IEnumerable<Region> regions, 
            Tribe tribe, 
            Army army, 
            SerializedObject orderRoot)
        {
            return ArmyEatCorpseOrder.DeserializeFrom(regions, army, orderRoot);
        }
    }
}
