﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Bronze.Common.Data.World.Units;
using Bronze.Contracts;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Elements;

namespace Bronze.Common.Data.Game.Units
{
    public class ArmySkirmishAction : IArmyAction
    {
        public string Icon => "ui_action_interact";
        public string Name => "Skrimish";
        public string Description => "Skirmish with another army.";
        public bool PathAdjacent => true;
        
        private readonly IWorldManager _worldManager;
        private readonly IBattleHelper _battleHelper;

        public ArmySkirmishAction(
            IWorldManager worldManager,
            IBattleHelper battleHelper)
        {
            _worldManager = worldManager;
            _battleHelper = battleHelper;
        }
        
        public IUiElement BuildDescriptionStack()
        {
            return new Label("Skirmishing");
        }

        public bool IsValidFor(Army army, Cell target)
        {
            return !GetValidationMessagesFor(army, target).Any();
        }

        public bool CouldBeValidFor(Tribe tribe, Cell target)
        {
            return true;
        }

        public bool IsValidForQuickAction(Army army, Cell target)
        {
            return IsValidFor(army, target);
        }

        public IEnumerable<string> GetValidationMessagesFor(Army army, Cell target)
        {
            var targetArmy = target.WorldActors.OfType<Army>()
                .Where(a => a.Owner != army.Owner)
                .FirstOrDefault();

            if(targetArmy == null)
            {
                return "No army to approach.".Yield();
            }
            
            if (!target.OrthoNeighbors.Any(n => army.CanPath(n)))
            {
                return "Army cannot travel there.".Yield();
            }

            if(!army.Owner.IsHostileTo(target.Owner))
            {
                return "Not hostile to target army.".Yield();
            }
            
            return Enumerable.Empty<string>();
        }
        
        public void LoadFrom(IXmlReaderUtil xmlReaderUtil, Race race, XElement actionElement)
        {
        }

        public IArmyOrder BuildOrderFor(Army army, Cell target)
        {
            var targetArmy = target.WorldActors.OfType<Army>()
                .Where(a => a.Owner != army.Owner)
                .FirstOrDefault();

            return new ArmySkirmishOrder(army, targetArmy, _worldManager, army.CurrentOrder, _battleHelper);
        }

        public IArmyOrder DeserializeOrderFrom(
            IGamedataTracker gamedataTracker, 
            IEnumerable<Region> regions, 
            Tribe tribe, 
            Army army, 
            SerializedObject orderRoot)
        {
            return ArmySkirmishOrder.DeserializeFrom(regions, army, orderRoot, _worldManager, gamedataTracker, _battleHelper);
        }
    }
}
