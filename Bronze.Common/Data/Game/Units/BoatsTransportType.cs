﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Bronze.Contracts;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.Data.Game.Units
{
    public class BoatsTransportType : ITransportType
    {
        public string Id { get; private set; }
        public string Icon { get; private set; }
        public string Name { get; private set; }
        public bool ShowSingleTransport { get; private set; }

        public IndividualAnimation[] IndividualAnimations { get; private set; }
        public MovementMode[] MovementModes { get; private set; }
        public IEnumerable<IArmyAction> AvailableActions { get; private set; }

        private UnitCategory[] _acceptableCategories;
        private TransportDisembarkBehavior _disembarkBehavior;

        public bool CanTransport(IUnitType unitType)
        {
            return _acceptableCategories.Contains(unitType.Category);
        }

        public bool CanTransport(IUnit unit)
        {
            return _acceptableCategories.Contains(unit.Category);
        }

        public virtual bool CanPath(Cell cell)
        {
            return MovementModes
                .Select(mm => mm.MovementType)
                .Where(mm => cell.Traits.ContainsAll(mm.RequiredTraits))
                .Where(mm => !cell.Traits.ContainsAny(mm.PreventedBy))
                .Any();

        }

        public double GetMovementSpeed(Cell cell)
        {
            var movementMode = MovementModes
                .Where(mm => cell.Traits.ContainsAll(mm.MovementType.RequiredTraits))
                .Where(mm => !cell.Traits.ContainsAny(mm.MovementType.PreventedBy))
                .FirstOrDefault();

            return movementMode?.GetMovementSpeedFor(cell.Traits) ?? 0;
        }

        public void OnEmbark(Army army)
        {
        }

        public void OnDisembark(Army army)
        {
            switch(_disembarkBehavior.DisembarkType)
            {
                case TransportDisembarkType.ToDevelopment:
                    if(_disembarkBehavior.ReplacementDevelopment.CanBePlaced(army.Owner, army.Cell))
                    {
                        _disembarkBehavior.ReplacementDevelopment.Place(army.Owner, army.Cell, true);
                    }
                    break;
                case TransportDisembarkType.Nothing:
                    // Nothing to do
                    break;
            }
        }
        
        public void LoadFrom(
            IGamedataTracker gamedataTracker, 
            IXmlReaderUtil xmlReaderUtil, 
            IInjectionProvider injectionProvider, 
            XElement element)
        {
            Id = xmlReaderUtil.AttributeValue(element, "id");

            Icon = xmlReaderUtil.AttributeValue(element, "icon");
            Name = xmlReaderUtil.AttributeValue(element, "name");
            ShowSingleTransport = xmlReaderUtil.AttributeAsBool(element, "show_single_transport");

            _acceptableCategories = element
                .Elements("accepted_category")
                .Select(e => xmlReaderUtil.ObjectReferenceLookup(
                    e,
                    "id",
                    gamedataTracker.UnitCategories,
                    c => c.Id))
                .ToArray();

            LoadMovementElement(
                gamedataTracker,
                xmlReaderUtil,
                xmlReaderUtil.Element(element, "movement"));
            
            LoadAnimationsElement(
                gamedataTracker,
                xmlReaderUtil,
                xmlReaderUtil.Element(element, "animations"));

            AvailableActions = new IArmyAction[]
                {
                    new ArmyMoveAction(),
                    new ArmyDisembarkAction(),
                    injectionProvider.BuildNamed<IArmyAction>("ArmyInteractAction")
                }
                .Concat(element
                    .Elements("action")
                    .Select(e => LoadActionElement(xmlReaderUtil, injectionProvider, e)))
                .ToArray();
        }
        
        protected virtual void LoadMovementElement(
            IGamedataTracker gamedataTracker,
            IXmlReaderUtil xmlReaderUtil,
            XElement movementElement)
        {
            MovementModes = movementElement
                .Elements("movement_mode")
                .Select(e => new MovementMode
                {
                    MovementType = xmlReaderUtil
                        .ObjectReferenceLookup(
                            e,
                            "type",
                            gamedataTracker.MovementTypes,
                            x => x.Id),
                    Speed = xmlReaderUtil.AttributeAsDouble(e, "speed")
                })
                .ToArray();
        }
        
        protected virtual void LoadAnimationsElement(
            IGamedataTracker gamedataTracker,
            IXmlReaderUtil xmlReaderUtil,
            XElement individualsElement)
        {
            IndividualAnimations = individualsElement
                .Elements("animation")
                .Select(e => xmlReaderUtil.LoadIndividualAnimation(e))
                .ToArray();
        }

        public void PostLink(
            IGamedataTracker gamedataTracker,
            IXmlReaderUtil xmlReaderUtil,
            XElement element)
        {
            LoadDisembarkElement(
                gamedataTracker,
                xmlReaderUtil,
                xmlReaderUtil.Element(element, "disembark"));
        }

        protected virtual void LoadDisembarkElement(
            IGamedataTracker gamedataTracker,
            IXmlReaderUtil xmlReaderUtil,
            XElement disembarkElement)
        {
            _disembarkBehavior = new TransportDisembarkBehavior
            {
                DisembarkType = xmlReaderUtil.AttributeAsEnum<TransportDisembarkType>(
                    disembarkElement,
                    "type")
            };

            if (_disembarkBehavior.DisembarkType == TransportDisembarkType.ToDevelopment)
            {
                _disembarkBehavior.ReplacementDevelopment = xmlReaderUtil.ObjectReferenceLookup(
                    disembarkElement,
                    "development",
                    gamedataTracker.CellDevelopments,
                    f => f.Id);
            }
        }

        protected virtual IArmyAction LoadActionElement(
            IXmlReaderUtil xmlReaderUtil,
            IInjectionProvider injectionProvider,
            XElement actionElement)
        {
            var typeName = xmlReaderUtil.AttributeValue(actionElement, "type");

            var actionType = injectionProvider.BuildNamed<IArmyAction>(typeName);

            actionType.LoadFrom(xmlReaderUtil, null, actionElement);

            return actionType;
        }
    }
}
