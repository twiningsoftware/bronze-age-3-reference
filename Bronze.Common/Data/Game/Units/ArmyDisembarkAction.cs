﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Bronze.Common.Data.World.Units;
using Bronze.Contracts;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Elements;

namespace Bronze.Common.Data.Game.Units
{
    public class ArmyDisembarkAction : IArmyAction
    {
        public string Icon => "ui_action_disembark";
        public string Name => "Disembark";
        public string Description => "Leave the transports.";
        public bool PathAdjacent => true;

        public IUiElement BuildDescriptionStack()
        {
            return new StackGroup
            {
            };
        }

        public bool IsValidFor(Army army, Cell target)
        {
            return !GetValidationMessagesFor(army, target).Any();
        }

        public bool CouldBeValidFor(Tribe tribe, Cell target)
        {
            return true;
        }

        public bool IsValidForQuickAction(Army army, Cell target)
        {
            return !GetValidationMessagesFor(army, target).Any()
                && !army.CanPath(target);
        }

        public IEnumerable<string> GetValidationMessagesFor(Army army, Cell target)
        {
            if (!target.OrthoNeighbors.Any(n => army.CanPath(n)))
            {
                return "Army cannot travel there.".Yield();
            }
            
            if (!army.CouldPathWithoutTransport(target))
            {
                return "Army cannot disembark there.".Yield();
            }

            if (army.TransportType == null)
            {
                return "Army does not have a transport.".Yield();
            }
            
            return Enumerable.Empty<string>();
        }
        
        public void LoadFrom(IXmlReaderUtil xmlReaderUtil, Race race, XElement actionElement)
        {
        }

        public IArmyOrder BuildOrderFor(Army army, Cell target)
        {
            return new ArmyDisembarkOrder(army, target);
        }

        public IArmyOrder DeserializeOrderFrom(
            IGamedataTracker gamedataTracker, 
            IEnumerable<Region> regions, 
            Tribe tribe, 
            Army army, 
            SerializedObject orderRoot)
        {
            return ArmyDisembarkOrder.DeserializeFrom(regions, army, orderRoot);
        }
    }
}
