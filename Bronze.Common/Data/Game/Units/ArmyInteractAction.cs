﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Bronze.Common.Data.World.Units;
using Bronze.Contracts;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Elements;

namespace Bronze.Common.Data.Game.Units
{
    public class ArmyInteractAction : IArmyAction
    {
        public string Icon => "ui_action_interact";
        public string Name => "Interact";
        public string Description => "Interact with something in the world.";
        public bool PathAdjacent => true;

        private string _descriptionText;
        
        private readonly IWarHelper _warHelper;
        private readonly IBattleHelper _battleHelper;

        public ArmyInteractAction(
            IWarHelper warHelper,
            IBattleHelper battleHelper)
        {
            _warHelper = warHelper;
            _battleHelper = battleHelper;

            _descriptionText = string.Empty;
        }
        
        public IUiElement BuildDescriptionStack()
        {
            return new Label(_descriptionText)
            {
                ContentUpdater = () => _descriptionText
            };
        }

        public bool IsValidFor(Army army, Cell target)
        {
            return !GetValidationMessagesFor(army, target).Any();
        }

        public bool CouldBeValidFor(Tribe tribe, Cell target)
        {
            return true;
        }

        public bool IsValidForQuickAction(Army army, Cell target)
        {
            return IsValidFor(army, target);
        }

        public IEnumerable<string> GetValidationMessagesFor(Army army, Cell target)
        {
            _descriptionText = string.Empty;
            
            var targetInteractable = target.Development as IArmyInteractable;
            var siegeTarget = target.Development;

            if (siegeTarget != null && siegeTarget.Settlement?.Owner != null && siegeTarget.CanBeSieged && _warHelper.AreAtWar(army.Owner, siegeTarget.Settlement?.Owner))
            {
                if (!siegeTarget.Cell.OrthoNeighbors.Any(n => army.CanPath(n)))
                {
                    return "Army cannot travel there.".Yield();
                }
            }
            else if(targetInteractable != null)
            {
                if (targetInteractable.InteractAdjacent && !target.OrthoNeighbors.Any(n => army.CanPath(n)))
                {
                    return "Army cannot travel there.".Yield();
                }
                else if (!targetInteractable.InteractAdjacent && !army.CanPath(target))
                {
                    return "Army cannot travel there.".Yield();
                }

                if(!targetInteractable.InteractWithForeign && target.Region.Owner != army.Owner)
                {
                    return "Army cannot interact with that.".Yield();
                }
                
                if (target.Owner == army.Owner)
                {
                    _descriptionText = targetInteractable.InteractVerb;
                }
                else
                {
                    _descriptionText = "Approaching";
                }
            }
            else
            {
                return "Nothing to interact with.".Yield();
            }
            
            return Enumerable.Empty<string>();
        }
        
        public void LoadFrom(IXmlReaderUtil xmlReaderUtil, Race race, XElement actionElement)
        {
        }

        public IArmyOrder BuildOrderFor(Army army, Cell target)
        {


            return new ArmyInteractOrder(army, target, _battleHelper);
        }

        public IArmyOrder DeserializeOrderFrom(
            IGamedataTracker gamedataTracker, 
            IEnumerable<Region> regions, 
            Tribe tribe, 
            Army army, 
            SerializedObject orderRoot)
        {
            return ArmyInteractOrder.DeserializeFrom(regions, army, orderRoot, _battleHelper);
        }
    }
}
