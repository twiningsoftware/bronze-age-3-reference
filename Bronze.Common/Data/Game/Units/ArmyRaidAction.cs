﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Bronze.Common.Data.World.Units;
using Bronze.Contracts;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Elements;

namespace Bronze.Common.Data.Game.Units
{
    public class ArmyRaidAction : IArmyAction
    {
        public string Icon => "ui_action_raid";
        public string Name => "Raid";
        public string Description => "Raid and destroy enemy developments.";
        public bool PathAdjacent => false;

        private readonly IWarHelper _warHelper;

        public ArmyRaidAction(IWarHelper warHelper)
        {
            _warHelper = warHelper;
        }
        
        public IUiElement BuildDescriptionStack()
        {
            return new StackGroup
            {
            };
        }

        public bool IsValidFor(Army army, Cell target)
        {
            return !GetValidationMessagesFor(army, target).Any();
        }

        public bool CouldBeValidFor(Tribe tribe, Cell target)
        {
            return target.Region.Owner != null
                && target.Development != null
                && tribe.IsHostileTo(target.Region.Owner);
        }

        public bool IsValidForQuickAction(Army army, Cell target)
        {
            return IsValidFor(army, target);
        }

        public IEnumerable<string> GetValidationMessagesFor(Army army, Cell target)
        {
            if(target.Region?.Settlement == null)
            {
                return "Region is not claimed.".Yield();
            }

            if (!army.Owner.IsHostileTo(target.Region.Settlement.Owner))
            {
                return $"We are not at war with the {target.Region.Settlement.Owner.Name}.".Yield();
            }
            

            if (target.Development == null)
            {
                return "No development to raid.".Yield();
            }

            if (!target.Development.CanBeRaided)
            {
                return $"Cannot raid {target.Development.Name}".Yield();
            }

            if (target.Development == null)
            {
                return "No development to raid.".Yield();
            }

            if(!army.CanPath(target))
            {
                return "Army cannot travel there.".Yield();
            }

            return Enumerable.Empty<string>();
        }
        
        public void LoadFrom(IXmlReaderUtil xmlReaderUtil, Race race, XElement actionElement)
        {
        }

        public IArmyOrder BuildOrderFor(Army army, Cell target)
        {
            return new ArmyRaidOrder(army, target, _warHelper);
        }

        public IArmyOrder DeserializeOrderFrom(
            IGamedataTracker gamedataTracker, 
            IEnumerable<Region> regions, 
            Tribe tribe, 
            Army army, 
            SerializedObject orderRoot)
        {
            return ArmyRaidOrder.DeserializeFrom(regions, army, orderRoot, _warHelper);
        }
    }
}
