﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Bronze.Common.Data.World.Units;
using Bronze.Contracts;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Elements;

namespace Bronze.Common.Data.Game.Units
{
    public class ArmyCampAction : IArmyAction
    {
        public string Icon => "ui_action_camp";
        public string Name => "Camp";
        public string Description => "Camp the army to replentish supplies.";
        public bool PathAdjacent => false;

        public IUiElement BuildDescriptionStack()
        {
            return new StackGroup
            {
            };
        }

        public bool IsValidFor(Army army, Cell target)
        {
            return !GetValidationMessagesFor(army, target).Any();
        }

        public bool CouldBeValidFor(Tribe tribe, Cell target)
        {
            return true;
        }

        public bool IsValidForQuickAction(Army army, Cell target)
        {
            return IsValidFor(army, target);
        }

        public IEnumerable<string> GetValidationMessagesFor(Army army, Cell target)
        {
            if(!army.CanPath(target) || target == null)
            {
                return "Army cannot travel there.".Yield();
            }

            if (target.WorldActors.OfType<Army>().Any(a => a != army && a.CurrentOrder != null && a.CurrentOrder is ArmyCampOrder campOrder && campOrder.IsCamped))
            {
                return "There is already an army camped there.".Yield();
            }

            return Enumerable.Empty<string>();
        }
        
        public void LoadFrom(IXmlReaderUtil xmlReaderUtil, Race race, XElement actionElement)
        {
        }

        public IArmyOrder BuildOrderFor(Army army, Cell target)
        {
            return new ArmyCampOrder(army, target);
        }

        public IArmyOrder DeserializeOrderFrom(
            IGamedataTracker gamedataTracker, 
            IEnumerable<Region> regions, 
            Tribe tribe, 
            Army army, 
            SerializedObject orderRoot)
        {
            return ArmyCampOrder.DeserializeFrom(regions, army, orderRoot);
        }
    }
}
