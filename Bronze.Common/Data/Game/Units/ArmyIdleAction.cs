﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Bronze.Common.Data.World.Units;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Elements;

namespace Bronze.Common.Data.Game.Units
{
    public class ArmyIdleAction : IArmyAction
    {
        public string Icon => string.Empty;
        public string ButtonIcon => string.Empty;
        public string Name => string.Empty;
        public string Description => string.Empty;
        public bool PathAdjacent => false;

        public IUiElement BuildDescriptionStack()
        {
            return new StackGroup();
        }

        public IEnumerable<string> GetValidationMessagesFor(Army army, Cell target)
        {
            return Enumerable.Empty<string>();
        }

        public bool IsValidFor(Army army, Cell target)
        {
            return true;
        }

        public bool CouldBeValidFor(Tribe tribe, Cell target)
        {
            return true;
        }

        public bool IsValidForQuickAction(Army army, Cell target)
        {
            return IsValidFor(army, target);
        }

        public IArmyOrder BuildOrderFor(Army army, Cell target)
        {
            return new IdleOrder();
        }

        public IArmyOrder DeserializeOrderFrom(
            IGamedataTracker gamedataTracker, 
            IEnumerable<Region> regions, 
            Tribe tribe, 
            Army army, 
            SerializedObject orderRoot)
        {
            return new IdleOrder();
        }
        
        public void LoadFrom(IXmlReaderUtil xmlReaderUtil, Race race, XElement actionElement)
        {
        }
    }
}
