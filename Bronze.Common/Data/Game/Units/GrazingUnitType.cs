﻿using Bronze.Common.Data.World.Units;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Bronze.Common.Data.Game.Units
{
    public class GrazingUnitType : AbstractUnitType
    {
        private readonly IInjectionProvider _injectionProvider;

        public override bool CanBeTrained => true;

        public ItemRate[] GrazingProduction { get; set; }
        public double GrazingRecovery { get; set; }
        public double GrazingExhaustion { get; set; }
        public Trait[] GrazingRequirements { get; set; }
        public Trait[] GrazingForbidden { get; set; }
        public bool GrazeInRegion { get; set; }
        public HaulerInfo[] LogiSourceHaulers { get; set; }
        public IEnumerable<BonusType> BonusedBy { get; set; }

        public GrazingUnitType(IInjectionProvider injectionProvider)
        {
            _injectionProvider = injectionProvider;
        }
        
        public override IUnit CreateUnit(UnitEquipmentSet equipmentSet)
        {
            return _injectionProvider
                .Build<GrazingUnit>()
                .Initialize(this, equipmentSet);
        }

        public override IUnit DeserializeFrom(
            IGamedataTracker gamedataTracker,
            SerializedObject unitRoot)
        {
            var equipment = unitRoot
                .GetOptionalObjectReference(
                    "equipment_level",
                    EquipmentSets,
                    es => es.Level.ToString())
                    ?? EquipmentSets.First();

            var unit = _injectionProvider
                .Build<GrazingUnit>()
                .Initialize(this, equipment);

            unit.DeserializeFrom(gamedataTracker, unitRoot);

            return unit;
        }

        protected override void LoadFrom(
            IGamedataTracker gamedataTracker, 
            IXmlReaderUtil xmlReaderUtil, 
            IInjectionProvider injectionProvider, 
            XElement element, 
            Race forRace)
        {
            base.LoadFrom(gamedataTracker, xmlReaderUtil, injectionProvider, element, forRace);

            var grazingElement = xmlReaderUtil.Element(element, "grazing");

            LoadGrazingElement(gamedataTracker, xmlReaderUtil, grazingElement);
        }

        private void LoadGrazingElement(
            IGamedataTracker gamedataTracker, 
            IXmlReaderUtil xmlReaderUtil, 
            XElement grazingElement)
        {
            GrazingProduction = grazingElement
                .Elements("production")
                .Select(e => xmlReaderUtil.LoadItemRate(e))
                .ToArray();

            LogiSourceHaulers = grazingElement
                .Elements("hauler")
                .Select(e => xmlReaderUtil.ObjectReferenceLookup(
                    e,
                    "id",
                    gamedataTracker.HaulerInfos,
                    h => h.Id))
                .ToArray();

            GrazingRecovery = xmlReaderUtil.AttributeAsDouble(grazingElement, "recovery");
            GrazingExhaustion = xmlReaderUtil.AttributeAsDouble(grazingElement, "exhaustion");
            GrazeInRegion = xmlReaderUtil.AttributeAsBool(grazingElement, "stay_in_region");

            GrazingRequirements = grazingElement
                .Elements("requires")
                .Select(e => xmlReaderUtil.OptionalObjectReferenceLookup(
                    e,
                    "trait",
                    gamedataTracker.Traits,
                    t => t.Id))
                .Where(t => t != null)
                .ToArray();

            GrazingForbidden = grazingElement
                .Elements("requires")
                .Select(e => xmlReaderUtil.OptionalObjectReferenceLookup(
                    e,
                    "not_trait",
                    gamedataTracker.Traits,
                    t => t.Id))
                .Where(t => t != null)
                .ToArray();

            BonusedBy = grazingElement
                    .Elements("bonused_by")
                    .Select(e => xmlReaderUtil.ObjectReferenceLookup(
                        e,
                        "bonus",
                        gamedataTracker.BonusTypes,
                        b => b.Id))
                    .ToArray();
        }
    }
}
