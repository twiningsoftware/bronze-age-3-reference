﻿using System.Collections.Generic;
using System.Xml.Linq;
using Bronze.Common.Data.World.CellDevelopments;
using Bronze.Common.UI;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;

namespace Bronze.Common.Data.Game.CellDevelopments
{
    public class TraderDevelopmentType : AbstractCellDevelopmentType, ITradeActorType
    {
        private readonly IInjectionProvider _injectionProvider;

        public MovementType TradeType { get; private set; }
        public double TradeCapacity { get; private set; }
        public TradeHaulerInfo TradeHauler { get; private set; }

        public TraderDevelopmentType(IInjectionProvider injectionProvider)
        {
            _injectionProvider = injectionProvider;
        }
        
        public override void Place(Tribe tribe, Cell cell, bool instantBuild)
        {
            cell.Development = _injectionProvider
                .Build<TraderDevelopment>()
                .Initialize(cell, this, instantBuild);
        }

        public override ICellDevelopment DeserializeFrom(
            IGamedataTracker gamedataTracker,
            Cell cell,
            SerializedObject developmentRoot)
        {
            var development = _injectionProvider
                .Build<TraderDevelopment>()
                .Initialize(cell, this, instantBuild: false);

            development.DeserializeFrom(gamedataTracker, developmentRoot);

            return development;
        }

        protected override void LoadFrom(IGamedataTracker gamedataTracker, IXmlReaderUtil xmlReaderUtil, XElement element, Race race)
        {
            base.LoadFrom(gamedataTracker, xmlReaderUtil, element, race);

            var tradeElement = xmlReaderUtil.Element(element, "trade");
            LoadTradeElement(gamedataTracker, xmlReaderUtil, tradeElement);
        }

        protected virtual void LoadTradeElement(
            IGamedataTracker gamedataTracker,
            IXmlReaderUtil xmlReaderUtil,
            XElement tradeElement)
        {
            TradeType = xmlReaderUtil.ObjectReferenceLookup(
                tradeElement,
                "movement_type",
                gamedataTracker.MovementTypes,
                m => m.Id);

            TradeCapacity = xmlReaderUtil.AttributeAsDouble(tradeElement, "capacity");

            TradeHauler = xmlReaderUtil.LoadTradeHaulerInfo(xmlReaderUtil.Element(tradeElement, "trade_hauler"));
        }

        public override IStackUiElement BuildTypeInfoPanel(int width, bool withName, Cell cell)
        {
            var stackGroup = base.BuildTypeInfoPanel(width, withName, cell);

            stackGroup.Children.AddRange(new AbstractUiElement[]
            {
                new Spacer(0, 5),
                new StackGroup
                {
                    Orientation = Orientation.Horizontal,
                    Children = new List<IUiElement>
                    {
                        new Label("Trade: "),
                        UiBuilder.BuildIconFor(TradeType),
                        new Label(TradeCapacity.ToString()),
                    }
                }
            });

            return stackGroup;
        }
    }
}
