﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Bronze.Common.Data.World.CellDevelopments;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.Data.Game.CellDevelopments
{
    public class IrrigationDevelopmentType : AbstractCellDevelopmentType
    {
        public IEnumerable<Trait> CarriedTraits { get; set; }
        public Trait SourceTrait { get; set; }

        private readonly IInjectionProvider _injectionProvider;

        public IrrigationDevelopmentType(IInjectionProvider injectionProvider)
        {
            _injectionProvider = injectionProvider;
        }
        
        public override void Place(Tribe tribe, Cell cell, bool instantBuild)
        {
            cell.Development = _injectionProvider
                .Build<IrrigationDevelopment>()
                .Initialize(cell, this, instantBuild);
        }

        public override ICellDevelopment DeserializeFrom(
            IGamedataTracker gamedataTracker,
            Cell cell,
            SerializedObject developmentRoot)
        {
            var development = _injectionProvider
                .Build<IrrigationDevelopment>()
                .Initialize(cell, this, instantBuild: false);

            development.DeserializeFrom(gamedataTracker, developmentRoot);

            return development;
        }

        protected override void LoadOperationElement(
            IGamedataTracker gamedataTracker,
            IXmlReaderUtil xmlReaderUtil, 
            XElement operationElement, 
            Race race)
        {
            base.LoadOperationElement(gamedataTracker, xmlReaderUtil, operationElement, race);

            var irrigationElement = xmlReaderUtil.Element(operationElement, "irrigation");

            SourceTrait = xmlReaderUtil.ObjectReferenceLookup(
                irrigationElement,
                "source_trait",
                gamedataTracker.Traits,
                t => t.Id);

            CarriedTraits = irrigationElement
                .Elements("carried")
                .Select(e => xmlReaderUtil.ObjectReferenceLookup(
                    e,
                    "trait",
                    gamedataTracker.Traits,
                    t => t.Id))
                .ToArray();
            
        }
    }
}
