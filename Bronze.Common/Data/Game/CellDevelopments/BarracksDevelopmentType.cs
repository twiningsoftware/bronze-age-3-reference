﻿using Bronze.Common.Data.World.CellDevelopments;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Bronze.Common.Data.Game.CellDevelopments
{
    public class BarracksDevelopmentType : AbstractCellDevelopmentType, IRecruitmentSlotProviderType
    {
        private readonly IInjectionProvider _injectionProvider;
        private readonly IGamedataTracker _gamedataTracker;

        public RecruitmentSlotCategory RecruitmentCategory { get; set; }
        public int ProvidedRecruitmentSlots { get; set; }
        public IntIndexableLookup<Item, bool> ItemsAllowed { get; private set; }

        public BarracksDevelopmentType(
            IInjectionProvider injectionProvider,
            IGamedataTracker gamedataTracker)
        {
            _injectionProvider = injectionProvider;
            _gamedataTracker = gamedataTracker;
        }
        
        public override void Place(Tribe tribe, Cell cell, bool instantBuild)
        {
            cell.Development = _injectionProvider
                .Build<BarracksDevelopment>()
                .Initialize(cell, this, instantBuild);
        }

        public override ICellDevelopment DeserializeFrom(
            IGamedataTracker gamedataTracker,
            Cell cell,
            SerializedObject developmentRoot)
        {
            var development = _injectionProvider
                .Build<BarracksDevelopment>()
                .Initialize(cell, this, instantBuild: false);

            development.DeserializeFrom(gamedataTracker, developmentRoot);

            return development;
        }

        public override IStackUiElement BuildTypeInfoPanel(int width, bool withName, Cell cell)
        {
            var stackGroup = base.BuildTypeInfoPanel(width, withName, cell);
            
            stackGroup.Children.Add(new Spacer(0, 10));
            stackGroup.Children.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new Label($"Provides {ProvidedRecruitmentSlots} {RecruitmentCategory.Name} recruits.")
                }
            });

            return stackGroup;
        }

        protected override void LoadFrom(IGamedataTracker gamedataTracker, IXmlReaderUtil xmlReaderUtil, XElement element, Race race)
        {
            base.LoadFrom(gamedataTracker, xmlReaderUtil, element, race);

            var recruitmentElement = xmlReaderUtil.Element(element, "recruitment");
            
            LoadRecruitmentElement(gamedataTracker, xmlReaderUtil, recruitmentElement);
        }

        protected virtual void LoadRecruitmentElement(IGamedataTracker gamedataTracker, IXmlReaderUtil xmlReaderUtil, XElement recruitmentElement)
        {
            RecruitmentCategory = xmlReaderUtil
                .ObjectReferenceLookup(
                    recruitmentElement,
                    "category",
                    _gamedataTracker.RecruitmentSlotCategories,
                    c => c.Id);

            ProvidedRecruitmentSlots = xmlReaderUtil.AttributeAsInt(recruitmentElement, "slots");

            var itemList = recruitmentElement
                .Elements("armory")
                .Select(e => xmlReaderUtil.ObjectReferenceLookup(
                    e,
                    "item",
                    gamedataTracker.Items,
                    i => i.Name))
                .ToArray();

            ItemsAllowed = new IntIndexableLookup<Item, bool>();
            foreach (var item in gamedataTracker.Items)
            {
                ItemsAllowed[item] = itemList.Contains(item);
            }
        }
    }
}
