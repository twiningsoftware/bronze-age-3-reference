﻿using Bronze.Common.Data.World.CellDevelopments;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System.Linq;
using System.Xml.Linq;

namespace Bronze.Common.Data.Game.CellDevelopments
{
    public class CreepAttackableDevelopmentType : AbstractCellDevelopmentType
    {
        public Race DefenderRace { get; private set; }
        public IUnitType DefenderUnit { get; private set; }
        public int DefenderCount { get; private set; }

        private readonly IInjectionProvider _injectionProvider;

        public CreepAttackableDevelopmentType(IInjectionProvider injectionProvider)
        {
            _injectionProvider = injectionProvider;
        }
        
        public override void PostLoad(
            IGamedataTracker gamedataTracker, 
            IXmlReaderUtil xmlReaderUtil, 
            XElement element)
        {
            base.PostLoad(gamedataTracker, xmlReaderUtil, element);
        
            LoadDefendersElement(
                gamedataTracker,
                xmlReaderUtil,
                xmlReaderUtil.Element(element, "defenders"));
        }

        protected virtual void LoadDefendersElement(
            IGamedataTracker gamedataTracker,
            IXmlReaderUtil xmlReaderUtil,
            XElement defendersElement)
        {
            DefenderRace = xmlReaderUtil.ObjectReferenceLookup(
                defendersElement,
                "race",
                gamedataTracker.Races,
                r => r.Id);

            DefenderUnit = xmlReaderUtil.ObjectReferenceLookup(
                defendersElement,
                "unit_type",
                gamedataTracker.UnitTypes,
                ut => ut.Id);

            DefenderCount = xmlReaderUtil.AttributeAsInt(defendersElement, "count");
        }

        public override void Place(Tribe tribe, Cell cell, bool instantBuild)
        {
            cell.Development = _injectionProvider
                .Build<CreepAttackableDevelopment>()
                .Initialize(cell, this, instantBuild);
        }

        public override ICellDevelopment DeserializeFrom(
            IGamedataTracker gamedataTracker,
            Cell cell,
            SerializedObject developmentRoot)
        {
            var development = _injectionProvider
                .Build<CreepAttackableDevelopment>()
                .Initialize(cell, this, instantBuild: false);

            development.DeserializeFrom(gamedataTracker, developmentRoot);

            return development;
        }
    }
}
