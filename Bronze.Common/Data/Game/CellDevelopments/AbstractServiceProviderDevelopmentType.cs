﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Bronze.Common.UI;
using Bronze.Contracts;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Priority_Queue;

namespace Bronze.Common.Data.Game.CellDevelopments
{
    public abstract class AbstractServiceProviderDevelopmentType : AbstractCellDevelopmentType, IServiceProviderDevelopmentType
    {
        public ServiceType ServiceType { get; private set; }
        public double ServiceRange { get; private set; }
        public IEnumerable<Trait> ServiceTransmissionTraits { get; private set; }

        protected override void LoadOperationElement(
            IGamedataTracker gamedataTracker,
            IXmlReaderUtil xmlReaderUtil,
            XElement operationElement,
            Race race)
        {
            base.LoadOperationElement(gamedataTracker, xmlReaderUtil, operationElement, race);

            var serviceElement = xmlReaderUtil.Element(operationElement, "service");

            LoadServiceElement(
                gamedataTracker,
                xmlReaderUtil,
                serviceElement,
                race);
        }

        protected virtual void LoadServiceElement(
            IGamedataTracker gamedataTracker,
            IXmlReaderUtil xmlReaderUtil,
            XElement serviceElement,
            Race race)
        {
            ServiceType = xmlReaderUtil.ObjectReferenceLookup(
                serviceElement,
                "type",
                gamedataTracker.ServiceTypes,
                s => s.Name);

            ServiceRange = xmlReaderUtil.AttributeAsInt(serviceElement, "range");

            ServiceTransmissionTraits = serviceElement
                .Elements("transmission")
                .Select(x => xmlReaderUtil.ObjectReferenceLookup(
                    x,
                    "trait",
                    gamedataTracker.Traits,
                    t => t.Id))
                .ToArray();
        }

        public IEnumerable<Cell> CalculateServiceArea(Cell cell)
        {
            var open = new SimplePriorityQueue<Cell, double>();
            var distTo = new Dictionary<Cell, double>();
            var closed = new HashSet<Cell>();

            open.Enqueue(cell, 0);
            distTo.Add(cell, 0);
            
            while (open.Any())
            {
                var current = open.Dequeue();

                closed.Add(current);

                var cost = 1;// TODO change when adding logi to cells

                if (distTo[current] + cost <= ServiceRange)
                {
                    var neighborsToExpandTo = current.Neighbors
                        .Where(c => !closed.Contains(c))
                        .Where(c => c.Region == cell.Region)
                        .Where(c => c.Traits.ContainsAll(ServiceTransmissionTraits))
                        .ToArray();

                    foreach (var neighbor in neighborsToExpandTo)
                    {
                        var dist = distTo[current] + cost;

                        if (distTo.ContainsKey(neighbor))
                        {
                            if (dist < distTo[neighbor])
                            {
                                distTo[neighbor] = dist;
                                open.UpdatePriority(neighbor, dist);
                            }
                        }
                        else
                        {
                            open.Enqueue(neighbor, dist);
                            distTo.Add(neighbor, dist);
                        }
                    }
                }
            }

            return closed.ToArray();
        }

        public override IStackUiElement BuildTypeInfoPanel(int width, bool withName, Cell cell)
        {
            var stack = base.BuildTypeInfoPanel(width, withName, cell);
            stack.Children.Add(UiBuilder.BuildServiceProvidedTypeRow(ServiceType, ServiceRange));
            return stack;
        }

        public abstract bool CanService(IEconomicActor economicActor);

        public abstract bool CanService(IEconomicActorType economicActorType);
    }
}
