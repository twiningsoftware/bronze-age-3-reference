﻿using System.Collections.Generic;
using System.Xml.Linq;
using Bronze.Common.Data.World.CellDevelopments;
using Bronze.Common.UI;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;

namespace Bronze.Common.Data.Game.CellDevelopments
{
    public class TransportAnchorageDevelopmentType : AbstractCellDevelopmentType
    {
        private readonly IInjectionProvider _injectionProvider;

        public ITransportType TransportType { get; private set; }
        
        public TransportAnchorageDevelopmentType(IInjectionProvider injectionProvider)
        {
            _injectionProvider = injectionProvider;
        }
        
        public override void Place(Tribe tribe, Cell cell, bool instantBuild)
        {
            cell.Development = _injectionProvider
                .Build<TransportAnchorageDevelopment>()
                .Initialize(cell, this, instantBuild);
        }

        public override ICellDevelopment DeserializeFrom(
            IGamedataTracker gamedataTracker,
            Cell cell,
            SerializedObject developmentRoot)
        {
            var development = _injectionProvider
                .Build<TransportAnchorageDevelopment>()
                .Initialize(cell, this, instantBuild: false);

            development.DeserializeFrom(gamedataTracker, developmentRoot);

            return development;
        }

        protected override void LoadFrom(IGamedataTracker gamedataTracker, IXmlReaderUtil xmlReaderUtil, XElement element, Race race)
        {
            base.LoadFrom(gamedataTracker, xmlReaderUtil, element, race);

            LoadTransportSourceElement(
                gamedataTracker,
                xmlReaderUtil,
                xmlReaderUtil.Element(element, "transport_source"));
        }

        private void LoadTransportSourceElement(
            IGamedataTracker gamedataTracker, 
            IXmlReaderUtil xmlReaderUtil, 
            XElement transportElement)
        {
            TransportType = xmlReaderUtil.ObjectReferenceLookup(
                transportElement,
                "transport_type",
                gamedataTracker.TransportTypes,
                tt => tt.Id);
        }

        public override IStackUiElement BuildTypeInfoPanel(int width, bool withName, Cell cell)
        {
            var stackGroup = base.BuildTypeInfoPanel(width, withName, cell);

            stackGroup.Children.AddRange(new IUiElement[]
            {
                new Spacer(0, 5),
                new StackGroup
                {
                    Orientation = Orientation.Horizontal,
                    Children = new List<IUiElement>
                    {
                        new Label("Provides Transport: "),
                        UiBuilder.BuildDisplayFor(TransportType)
                    }
                }
            });

            return stackGroup;
        }
    }
}
