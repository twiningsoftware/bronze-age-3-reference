﻿using Bronze.Common.Data.World.CellDevelopments;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System.Linq;
using System.Xml.Linq;

namespace Bronze.Common.Data.Game.CellDevelopments
{
    public class CreepCampDevelopmentType : AbstractCellDevelopmentType
    {
        public double PacificationMonths { get; set; }
        public RecruitmentSlotCategory RecruitmentCategory { get; set; }
        public int ProvidedRecruitmentSlots { get; set; }
        public IUnitType[] MercenaryUnitTypes { get; set; }
        public string DialogContext { get; private set; }
        public double BanditSpawnTimeMin { get; private set; }
        public double BanditSpawnTimeMax { get; private set; }
        public IUnitType BanditUnit { get; private set; }
        public Race BanditRace { get; private set; }
        public int BanditCountMin { get; private set; }
        public int BanditCountMax { get; private set; }
        public PacificationOption[] PacificationOptions { get; private set; }

        private readonly IInjectionProvider _injectionProvider;

        public CreepCampDevelopmentType(IInjectionProvider injectionProvider)
        {
            _injectionProvider = injectionProvider;
        }

        protected override void LoadFrom(
            IGamedataTracker gamedataTracker, 
            IXmlReaderUtil xmlReaderUtil, 
            XElement element, 
            Race race)
        {
            base.LoadFrom(gamedataTracker, xmlReaderUtil, element, race);

            
        }


        protected virtual void LoadPacificationElement(
            IGamedataTracker gamedataTracker,
            IXmlReaderUtil xmlReaderUtil,
            XElement pacificationElement)
        {
            PacificationMonths = xmlReaderUtil.AttributeAsDouble(pacificationElement, "months_to_pacify");

            PacificationOptions = pacificationElement.Elements("pacification_option")
                .Select(e => new PacificationOption
                {
                    Id = xmlReaderUtil.AttributeValue(e, "id"),
                    Inputs = e.Elements("input")
                        .Select(x => xmlReaderUtil.LoadItemRate(x))
                        .ToArray()
                })
                .ToArray();

            DialogContext = xmlReaderUtil.AttributeValue(pacificationElement, "dialog_context");

            var mercenaryElement = xmlReaderUtil.Element(pacificationElement, "mercenaries");

            RecruitmentCategory = xmlReaderUtil
                .ObjectReferenceLookup(
                    mercenaryElement,
                    "category",
                    gamedataTracker.RecruitmentSlotCategories,
                    c => c.Id);

            ProvidedRecruitmentSlots = xmlReaderUtil.AttributeAsInt(mercenaryElement, "slots");

            MercenaryUnitTypes = mercenaryElement
                .Elements("available_unit")
                .Select(e => xmlReaderUtil.ObjectReferenceLookup(
                    e,
                    "unit_id",
                    gamedataTracker.UnitTypes,
                    u => u.Id))
                .ToArray();
        }

        public override void PostLoad(
            IGamedataTracker gamedataTracker, 
            IXmlReaderUtil xmlReaderUtil, 
            XElement element)
        {
            base.PostLoad(gamedataTracker, xmlReaderUtil, element);

            LoadPacificationElement(
                gamedataTracker,
                xmlReaderUtil,
                xmlReaderUtil.Element(element, "pacification"));

            LoadBanditsElement(
                gamedataTracker,
                xmlReaderUtil,
                xmlReaderUtil.Element(element, "bandits"));
        }

        protected virtual void LoadBanditsElement(
            IGamedataTracker gamedataTracker,
            IXmlReaderUtil xmlReaderUtil,
            XElement banditsElement)
        {
            BanditRace = xmlReaderUtil.ObjectReferenceLookup(
                banditsElement,
                "race",
                gamedataTracker.Races,
                r => r.Id);
            BanditSpawnTimeMin = xmlReaderUtil.AttributeAsDouble(banditsElement, "spawn_months_min");
            BanditSpawnTimeMax = xmlReaderUtil.AttributeAsDouble(banditsElement, "spawn_months_max");
            BanditUnit = xmlReaderUtil.ObjectReferenceLookup(
                banditsElement,
                "unit_type",
                gamedataTracker.UnitTypes,
                ut => ut.Id);
            BanditCountMin = xmlReaderUtil.AttributeAsInt(banditsElement, "count_min");
            BanditCountMax = xmlReaderUtil.AttributeAsInt(banditsElement, "count_max");
        }

        public override void Place(Tribe tribe, Cell cell, bool instantBuild)
        {
            cell.Development = _injectionProvider
                .Build<CreepCampDevelopment>()
                .Initialize(cell, this, instantBuild);
        }

        public override ICellDevelopment DeserializeFrom(
            IGamedataTracker gamedataTracker,
            Cell cell,
            SerializedObject developmentRoot)
        {
            var development = _injectionProvider
                .Build<CreepCampDevelopment>()
                .Initialize(cell, this, instantBuild: false);

            development.DeserializeFrom(gamedataTracker, developmentRoot);

            return development;
        }

        public class PacificationOption
        {
            public string Id { get; set; }
            public ItemRate[] Inputs { get; set; }
        }
    }
}
