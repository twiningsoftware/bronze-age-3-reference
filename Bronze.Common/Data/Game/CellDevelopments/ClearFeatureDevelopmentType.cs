﻿using Bronze.Common.Data.World.CellDevelopments;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.Data.Game.CellDevelopments
{
    public class ClearFeatureDevelopmentType : AbstractCellDevelopmentType
    {
        private readonly IInjectionProvider _injectionProvider;

        public ClearFeatureDevelopmentType(IInjectionProvider injectionProvider)
        {
            _injectionProvider = injectionProvider;
        }
        
        public override void Place(Tribe tribe, Cell cell, bool instantBuild)
        {
            if (instantBuild)
            {
                cell.Feature = null;
            }
            else
            {
                cell.Development = _injectionProvider
                    .Build<ClearFeatureDevelopment>()
                    .Initialize(cell, this, instantBuild);
            }
        }

        public override ICellDevelopment DeserializeFrom(
            IGamedataTracker gamedataTracker, 
            Cell cell, 
            SerializedObject developmentRoot)
        {
            var development = _injectionProvider
                .Build<ClearFeatureDevelopment>()
                .Initialize(cell, this, instantBuild: false);

            development.DeserializeFrom(gamedataTracker, developmentRoot);

            return development;
        }
    }
}
