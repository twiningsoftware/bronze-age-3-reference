﻿using System.Linq;
using System.Xml.Linq;
using Bronze.Common.Data.World.CellDevelopments;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.Exceptions;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.Data.Game.CellDevelopments
{
    public class TradeRecieverDevelopmentType : AbstractCellDevelopmentType, ITradeRecieverType
    {
        private readonly IInjectionProvider _injectionProvider;

        public TradeRecieverDevelopmentType(IInjectionProvider injectionProvider)
        {
            _injectionProvider = injectionProvider;
        }

        public override ICellDevelopment DeserializeFrom(
            IGamedataTracker gamedataTracker, 
            Cell cell, 
            SerializedObject developmentRoot)
        {
            var development = _injectionProvider
                .Build<TradeRecieverDevelopment>()
                .Initialize(cell, this, instantBuild: false);

            development.DeserializeFrom(gamedataTracker, developmentRoot);

            return development;
        }

        public override void Place(
            Tribe tribe, 
            Cell cell, 
            bool instantBuild)
        {
            cell.Development = _injectionProvider
                .Build<TradeRecieverDevelopment>()
                .Initialize(cell, this, instantBuild);
        }

        protected override void LoadFrom(IGamedataTracker gamedataTracker, IXmlReaderUtil xmlReaderUtil, XElement element, Race race)
        {
            base.LoadFrom(gamedataTracker, xmlReaderUtil, element, race);

            if (!LogiSourceHaulers.Any())
            {
                throw DataLoadException.MiscError(element, "Trade Reciever has no logi source haulers, it will not be able to.");
            }
        }

        public override void PostLink(IGamedataTracker gamedataTracker, IXmlReaderUtil xmlReaderUtil, XElement element)
        {
            base.PostLink(gamedataTracker, xmlReaderUtil, element);
        }
    }
}
