﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Bronze.Common.UI;
using Bronze.Common.UI.Elements;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.Exceptions;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;

namespace Bronze.Common.Data.Game.CellDevelopments
{
    public abstract class AbstractCellDevelopmentType : ICellDevelopmentType
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string IconKey { get; set; }
        public string Description { get; set; }
        public string[] AdjacencyGroups { get; set; }
        public IEnumerable<Trait> Traits { get; set; }
        public IEnumerable<Trait> NotTraits { get; set; }
        public WorkerNeed ConstructionWorkers { get; set; }
        public WorkerNeed OperationWorkers { get; set; }
        public double ConstructionMonths { get; set; }
        public IEnumerable<CellAnimationLayer> DetailLayers { get; private set; }
        public IEnumerable<CellAnimationLayer> SummaryLayers { get; private set; }
        public IEnumerable<CellAnimationLayer> ConstructionDetailLayers { get; private set; }
        public IEnumerable<CellAnimationLayer> ConstructionSummaryLayers { get; private set; }

        public int InventoryCapacity { get; set; }
        public IEnumerable<Recipie> Recipies { get; set; }
        public HaulerInfo[] LogiSourceHaulers { get; set; }
        public bool IsActivity { get; set; }
        public int VisionRange { get; private set; }
        public IEnumerable<ICellDevelopmentType> ManuallyUpgradesTo { get; private set; }
        public ICellDevelopmentType ManuallyDowngradesTo { get; private set; }
        public bool UpgradeOnly { get; private set; }
        public IEnumerable<ItemRate> UpkeepInputs { get; set; }
        public Trait[] TraitsToClear { get; private set; }
        public DevelopmentAbandonmentBehavior AbandonmentBehavior { get; private set; }
        public DevelopmentAbandonmentBehavior RaidedBehavior { get; private set; }
        public IEnumerable<ItemQuantity> ConstructionCost { get; set; }
        public IntIndexableLookup<Item, double> ConstructionCostLookup { get; set; }

        public MinimapColor MinimapColor { get; private set; }

        private List<CellDevelopmentRestriction> _cellDevelopmentRestrictions;
        
        public bool CanBePlaced(Tribe tribe, Cell cell)
        {
            return cell != null 
                && GetPlacementMessages(tribe, cell).Length == 0;
        }

        public bool CouldBePlacedWithoutFeature(Tribe tribe, Cell cell)
        {
            if (cell == null)
            {
                return false;
            }

            var t = _cellDevelopmentRestrictions
                .Select(x => x.CouldBeValidWithoutFeature(tribe, cell, this))
                .Where(x => x != null)
                .ToArray();

            return !_cellDevelopmentRestrictions
                .Select(x => x.CouldBeValidWithoutFeature(tribe, cell, this))
                .Where(x => x != null)
                .Any();
        }

        public string[] GetPlacementMessages(Tribe tribe, Cell cell)
        {
            if(cell == null)
            {
                return new string[0];
            }

            return _cellDevelopmentRestrictions
                .Select(x => x.IsValid(tribe, cell, this))
                .Where(x => x != null)
                .ToArray();
        }

        public bool CouldBePlaced(Settlement settlement)
        {
            return _cellDevelopmentRestrictions
                .All(x => x.IsValidChoice(settlement.Owner, this));
        }

        public abstract void Place(Tribe tribe, Cell cell, bool instantBuild);

        public virtual IStackUiElement BuildTypeInfoPanel(int width, bool withName, Cell cell)
        {
            var stackGroup = new StackGroup
            {
                Orientation = Orientation.Vertical,
                Width = Width.Fixed(width)
            };

            if(withName)
            {
                stackGroup.Children.Add(new StackGroup
                {
                    Orientation = Orientation.Horizontal,
                    VerticalContentAlignment = VerticalAlignment.Middle,
                    Children = new List<IUiElement>
                        {
                            new Label(Name)
                        }
                });
                stackGroup.Children.Add(new Spacer(0, 10));
            }

            stackGroup.Children.AddRange(new AbstractUiElement[]
            {
                new TraitDisplay(() => Traits),
                new Spacer(0, 5),
                new Text(Description)
                {
                    Width = Width.Fixed(width)
                },
                UiBuilder.BuildConstructionInfoPanel(this, cell),
                UiBuilder.BuildEconomicInfoPanel(this)
            });

            return stackGroup;
        }

        public void LoadFrom(
            IGamedataTracker gamedataTracker,
            IXmlReaderUtil xmlReaderUtil,
            XElement element)
        {
            var forRace = xmlReaderUtil.ObjectReferenceLookup(
                element,
                "race",
                gamedataTracker.Races,
                r => r.Id);

            forRace.AvailableCellDevelopments.Add(this);

            LoadFrom(gamedataTracker, xmlReaderUtil, element, forRace);
        }

        public virtual void PostLoad(
            IGamedataTracker gamedataTracker, 
            IXmlReaderUtil xmlReaderUtil, 
            XElement element)
        {
        }

        protected virtual void LoadFrom(
            IGamedataTracker gamedataTracker, 
            IXmlReaderUtil xmlReaderUtil, 
            XElement element,
            Race race)
        {
            Id = xmlReaderUtil.AttributeValue(element, "id");
            Name = xmlReaderUtil.AttributeValue(element, "name");
            IconKey = xmlReaderUtil.AttributeValue(element, "icon");
            Description = xmlReaderUtil.SanitizeTextForFont(xmlReaderUtil.Element(element, "description").Value);
            UpgradeOnly = xmlReaderUtil.AttributeAsBool(element, "upgrade_only");
            MinimapColor = xmlReaderUtil.AttributeAsEnum<MinimapColor>(element, "minimap_color");
            AdjacencyGroups = element.Elements("adjacency_group")
                .Select(e => xmlReaderUtil.AttributeValue(e, "group"))
                .ToArray();
            DetailLayers = element
                .Elements("detailed_sprite")
                .Select(e => xmlReaderUtil.LoadCellSpriteLayer(e))
                .Concat(element
                    .Elements("detailed_animation")
                    .Select(e => xmlReaderUtil.LoadCellAnimationLayer(e)))
                .ToArray();
            SummaryLayers = element
                .Elements("summary_sprite")
                .Select(e => xmlReaderUtil.LoadCellSpriteLayer(e))
                .Concat(element
                    .Elements("summary_animation")
                    .Select(e => xmlReaderUtil.LoadCellAnimationLayer(e)))
                .ToArray();
            Traits = element
                .Elements("trait")
                .Select(e => xmlReaderUtil.ObjectReferenceLookup(e,
                    "trait",
                    gamedataTracker.Traits,
                    t => t.Id))
                .ToArray();
            NotTraits = element
                .Elements("not_trait")
                .Select(e => xmlReaderUtil.ObjectReferenceLookup(e,
                    "trait",
                    gamedataTracker.Traits,
                    t => t.Id))
                .ToArray();
            IsActivity = xmlReaderUtil.AttributeAsBool(element, "is_activity");
            _cellDevelopmentRestrictions = element
                .Elements("placement_restriction")
                .Select(e => CellDevelopmentRestriction.Load(gamedataTracker, xmlReaderUtil, e))
                .Concat(CellDevelopmentRestriction.ItemKnowledgeRestriction().Yield())
                .ToList();

            LoadConstructionElement(gamedataTracker, xmlReaderUtil, xmlReaderUtil.Element(element, "construction"), race);

            LoadOperationElement(gamedataTracker, xmlReaderUtil, xmlReaderUtil.Element(element, "operation"), race);

            LoadLogiElement(gamedataTracker, xmlReaderUtil, xmlReaderUtil.Element(element, "logi"));

            LoadAbandonmentElement(gamedataTracker, xmlReaderUtil, xmlReaderUtil.Element(element, "abandonment"));

            LoadRaidedElement(gamedataTracker, xmlReaderUtil, xmlReaderUtil.Element(element, "raided"));

            VisionRange = xmlReaderUtil.AttributeAsInt(xmlReaderUtil.Element(element, "vision"), "range");

            if (Recipies.Any() && !LogiSourceHaulers.Any())
            {
                throw DataLoadException.MiscError(element, "Cell Development type has a recipie, but no logi source haulers. Cell Development will not be able to deliver items.");
            }
        }
        
        protected virtual void LoadConstructionElement(
            IGamedataTracker gamedataTracker, 
            IXmlReaderUtil xmlReaderUtil, 
            XElement constructionElement, 
            Race race)
        {
            ConstructionWorkers = xmlReaderUtil.LoadWorkerNeed(constructionElement, race);
            ConstructionMonths = xmlReaderUtil.AttributeAsDouble(constructionElement, "months");
            ConstructionDetailLayers = constructionElement
                .Elements("detailed_sprite")
                .Select(e => xmlReaderUtil.LoadCellSpriteLayer(e))
                .Concat(constructionElement
                    .Elements("detailed_animation")
                    .Select(e => xmlReaderUtil.LoadCellAnimationLayer(e)))
                .ToArray();
            ConstructionSummaryLayers = constructionElement
                .Elements("summary_sprite")
                .Select(e => xmlReaderUtil.LoadCellSpriteLayer(e))
                .Concat(constructionElement
                    .Elements("summary_animation")
                    .Select(e => xmlReaderUtil.LoadCellAnimationLayer(e)))
                .ToArray();
            ConstructionCost = constructionElement
                .Elements("input")
                .Select(e => xmlReaderUtil.LoadItemRate(e).ToQuantity(ConstructionMonths))
                .ToArray();
            ConstructionCostLookup = new IntIndexableLookup<Item, double>();
            foreach (var input in ConstructionCost)
            {
                ConstructionCostLookup[input.Item] += input.Quantity;
            }
            TraitsToClear = constructionElement
                .Elements("clears_feature")
                .Select(x => xmlReaderUtil.ObjectReferenceLookup(
                    x,
                    "trait",
                    gamedataTracker.Traits,
                    t => t.Id))
                .ToArray();
        }

        protected virtual void LoadOperationElement(IGamedataTracker gamedataTracker, IXmlReaderUtil xmlReaderUtil, XElement operationElement, Race race)
        {
            OperationWorkers = xmlReaderUtil.LoadWorkerNeed(operationElement, race);

            var inventoryElement = xmlReaderUtil.ElementOrNull(operationElement, "inventory");

            if (inventoryElement != null)
            {
                InventoryCapacity = xmlReaderUtil.AttributeAsInt(inventoryElement, "capacity");
            }

            Recipies = operationElement.Elements("recipe")
                .Select(x => xmlReaderUtil.ObjectReferenceLookup(
                    x,
                    "id",
                    gamedataTracker.Recipies,
                    r => r.Id))
                .ToArray();

            UpkeepInputs = operationElement.Elements("input")
                .Select(x => xmlReaderUtil.LoadItemRate(x))
                .ToArray();
        }

        protected virtual void LoadLogiElement(IGamedataTracker gamedataTracker, IXmlReaderUtil xmlReaderUtil, XElement logiElement)
        {
            var source = xmlReaderUtil.ElementOrNull(logiElement, "source");
            if (source != null)
            {
                LogiSourceHaulers = source
                    .Elements("hauler")
                    .Select(e => xmlReaderUtil.ObjectReferenceLookup(
                        e,
                        "id",
                        gamedataTracker.HaulerInfos,
                        h => h.Id))
                    .ToArray();
            }
            else
            {
                LogiSourceHaulers = new HaulerInfo[0];
            }
        }

        private void LoadAbandonmentElement(
            IGamedataTracker gamedataTracker,
            IXmlReaderUtil xmlReaderUtil,
            XElement abandonmentElement)
        {
            AbandonmentBehavior = new DevelopmentAbandonmentBehavior
            {
                AbandonmentType = xmlReaderUtil.AttributeAsEnum<AbandonmentType>(
                    abandonmentElement,
                    "type")
            };
            
            if(AbandonmentBehavior.AbandonmentType == AbandonmentType.ToFeature)
            {
                AbandonmentBehavior.ReplacementFeature = xmlReaderUtil.ObjectReferenceLookup(
                    abandonmentElement,
                    "feature",
                    gamedataTracker.CellFeatures,
                    f => f.Id);  
            }
        }

        private void LoadRaidedElement(
            IGamedataTracker gamedataTracker,
            IXmlReaderUtil xmlReaderUtil,
            XElement abandonmentElement)
        {
            RaidedBehavior = new DevelopmentAbandonmentBehavior
            {
                AbandonmentType = xmlReaderUtil.AttributeAsEnum<AbandonmentType>(
                    abandonmentElement,
                    "type")
            };

            if (RaidedBehavior.AbandonmentType == AbandonmentType.ToFeature)
            {
                RaidedBehavior.ReplacementFeature = xmlReaderUtil.ObjectReferenceLookup(
                    abandonmentElement,
                    "feature",
                    gamedataTracker.CellFeatures,
                    f => f.Id);
            }
        }

        public virtual void PostLink(
            IGamedataTracker gamedataTracker, 
            IXmlReaderUtil xmlReaderUtil, 
            XElement element)
        {
            var upgradesTo = new List<ICellDevelopmentType>();

            foreach(var upgradeElement in element.Elements("upgrades"))
            {
                var toType = xmlReaderUtil.ObjectReferenceLookup(
                    upgradeElement,
                    "to",
                    gamedataTracker.CellDevelopments,
                    cd => cd.Id);
                
                upgradesTo.Add(toType);
            }

            ManuallyUpgradesTo = upgradesTo.ToArray();
            
            var downgradeElement = xmlReaderUtil.ElementOrNull(element, "downgrades");
            if (downgradeElement != null)
            {
                ManuallyDowngradesTo = xmlReaderUtil.ObjectReferenceLookup(
                    downgradeElement,
                    "to",
                    gamedataTracker.CellDevelopments,
                    cd => cd.Id);
            }
        }

        public abstract ICellDevelopment DeserializeFrom(
            IGamedataTracker gamedataTracker, 
            Cell cell,
            SerializedObject developmentRoot);
    }
}
