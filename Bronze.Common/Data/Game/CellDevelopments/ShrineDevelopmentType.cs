﻿using System.Xml.Linq;
using Bronze.Common.Data.World.CellDevelopments;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.Data.Game.CellDevelopments
{
    public class ShrineDevelopmentType : AbstractCellDevelopmentType
    {
        public Cult Cult { get; set; }
        public double InfluencePerPop { get; set; }
        public int BonusAuthority { get; set; }

        private readonly IInjectionProvider _injectionProvider;

        public ShrineDevelopmentType(IInjectionProvider injectionProvider)
        {
            _injectionProvider = injectionProvider;
        }
        
        public override void Place(Tribe tribe, Cell cell, bool instantBuild)
        {
            cell.Development = _injectionProvider
                .Build<ShrineDevelopment>()
                .Initialize(cell, this, instantBuild);
        }

        public override ICellDevelopment DeserializeFrom(
            IGamedataTracker gamedataTracker,
            Cell cell,
            SerializedObject developmentRoot)
        {
            var development = _injectionProvider
                .Build<ShrineDevelopment>()
                .Initialize(cell, this, instantBuild: false);

            development.DeserializeFrom(gamedataTracker, developmentRoot);

            return development;
        }

        protected override void LoadOperationElement(
            IGamedataTracker gamedataTracker, 
            IXmlReaderUtil xmlReaderUtil, 
            XElement operationElement, 
            Race race)
        {
            base.LoadOperationElement(gamedataTracker, xmlReaderUtil, operationElement, race);

            var cultInfluenceElement = xmlReaderUtil
                .Element(operationElement, "cult_influence");

            Cult = xmlReaderUtil.ObjectReferenceLookup(
                cultInfluenceElement,
                "cult",
                gamedataTracker.Cults,
                c => c.Id);

            InfluencePerPop = xmlReaderUtil.AttributeAsDouble(
                cultInfluenceElement,
                "influence_per_pop");

            var bonusAdminElement = xmlReaderUtil.ElementOrNull(
                operationElement,
                "bonus_administration");

            if (bonusAdminElement != null)
            {
                BonusAuthority = xmlReaderUtil.AttributeAsInt(
                    bonusAdminElement,
                    "administration");
            }
        }
    }
}
