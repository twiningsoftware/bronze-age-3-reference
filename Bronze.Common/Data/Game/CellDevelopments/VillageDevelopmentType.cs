﻿using Bronze.Common.Data.World.CellDevelopments;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Bronze.Common.Data.Game.CellDevelopments
{
    public class VillageDevelopmentType : AbstractCellDevelopmentType
    {
        public Caste SupportedCaste { get; private set; }
        public int SupportedPops { get; private set; }
        public IEnumerable<IPopNeed> MaintenanceNeeds { get; private set; }
        public IEnumerable<IPopNeed> UpgradeNeeds { get; private set; }
        public IEnumerable<IPopNeed> BonusNeeds { get; private set; }

        private readonly INameSource _nameSource;
        private readonly IInjectionProvider _injectionProvider;
        
        public VillageDevelopmentType(
            INameSource nameSource,
            IInjectionProvider injectionProvider)
        {
            _nameSource = nameSource;
            _injectionProvider = injectionProvider;
        }
        
        public override void Place(Tribe tribe, Cell cell, bool instantBuild)
        {
            if (cell.Region.Settlement == null)
            {
                cell.Region.Settlement = new Settlement(tribe.Race, tribe)
                {
                    Name = _nameSource.MakeSettlementName(tribe.Race, cell),
                    Region = cell.Region
                };
                tribe.Settlements.Add(cell.Region.Settlement);
            }

             cell.Development = _injectionProvider
                .Build<VillageDevelopment>()
                .Initialize(cell, this, instantBuild);
        }

        public override ICellDevelopment DeserializeFrom(
            IGamedataTracker gamedataTracker,
            Cell cell,
            SerializedObject developmentRoot)
        {
            var development = _injectionProvider
                .Build<VillageDevelopment>()
                .Initialize(cell, this, instantBuild: false);

            development.DeserializeFrom(gamedataTracker, developmentRoot);

            return development;
        }

        protected override void LoadFrom(
            IGamedataTracker gamedataTracker, 
            IXmlReaderUtil xmlReaderUtil, 
            XElement element,
            Race race)
        {
            base.LoadFrom(gamedataTracker, xmlReaderUtil, element, race);

            var housingElement = xmlReaderUtil.Element(element, "housing");

            LoadHousingElement(gamedataTracker, xmlReaderUtil, housingElement, race);
        }

        protected virtual void LoadHousingElement(
            IGamedataTracker gamedataTracker, 
            IXmlReaderUtil xmlReaderUtil, 
            XElement housingElement, 
            Race race)
        {
            MaintenanceNeeds = housingElement
                .Elements("maintenance_need")
                .Select(x => xmlReaderUtil.LoadPopNeed(x))
                .ToArray();

            UpgradeNeeds = housingElement
                .Elements("upgrade_need")
                .Select(x => xmlReaderUtil.LoadPopNeed(x))
                .ToArray();

            BonusNeeds = housingElement
                .Elements("bonus_need")
                .Select(x => xmlReaderUtil.LoadPopNeed(x))
                .ToArray();

            SupportedCaste = xmlReaderUtil.ObjectReferenceLookup(
                housingElement,
                "caste",
                race.Castes,
                c => c.Id);

            SupportedPops = xmlReaderUtil.AttributeAsInt(housingElement, "capacity");
        }
    }
}
