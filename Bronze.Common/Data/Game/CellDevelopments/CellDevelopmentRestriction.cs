﻿using System;
using System.Linq;
using System.Xml.Linq;
using Bronze.Common.Data.World.CellDevelopments;
using Bronze.Contracts;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.Data.Game.CellDevelopments
{
    public abstract class CellDevelopmentRestriction
    {
        public abstract string IsValid(
            Tribe tribe, 
            Cell cell,
            ICellDevelopmentType cellDevelopmentType);

        public abstract string CouldBeValidWithoutFeature(
            Tribe tribe,
            Cell cell,
            ICellDevelopmentType cellDevelopmentType);

        public abstract bool IsValidChoice(
            Tribe tribe,
            ICellDevelopmentType cellDevelopmentType);
        
        public static CellDevelopmentRestriction Load(
            IGamedataTracker gamedataTracker,
            IXmlReaderUtil xmlReaderUtil, 
            XElement element)
        {
            var type = xmlReaderUtil.AttributeValue(element, "type");

            switch(type)
            {
                case "cell_has_trait":
                    return new CellHasTraitRestriction(
                            xmlReaderUtil.ObjectReferenceLookup(
                                    element,
                                    "trait",
                                    gamedataTracker.Traits,
                                    t => t.Id));
                case "cell_not_has_trait":
                    return new CellNotHasTraitRestriction(
                            xmlReaderUtil.ObjectReferenceLookup(
                                    element,
                                    "trait",
                                    gamedataTracker.Traits,
                                    t => t.Id));
                case "region_unowned":
                    return new RegionUnownedRestriction();
                case "region_owned":
                    return new RegionOwnedRestriction();
                case "cell_no_development":
                    return new CellNoDevelopmentRestriction();
                case "settlement_adjacent":
                    return new CellSettlementAdjacentDevelopmentRestriction();
                case "one_at_a_time":
                    return new OneAtATimeDevelopmentRestriction(
                        xmlReaderUtil.ObjectReferenceLookup(
                                    element,
                                    "trait",
                                    gamedataTracker.Traits,
                                    t => t.Id),
                        xmlReaderUtil.AttributeValue(element, "description"));
                case "neighbor_not_has_trait":
                    return new NeighborNotHasTraitRestriction(
                            xmlReaderUtil.ObjectReferenceLookup(
                                    element,
                                    "trait",
                                    gamedataTracker.Traits,
                                    t => t.Id));
                case "neighbor_has_trait":
                    return new NeighborHasTraitRestriction(
                            xmlReaderUtil.ObjectReferenceLookup(
                                    element,
                                    "trait",
                                    gamedataTracker.Traits,
                                    t => t.Id));
                case "any_recipie_valid":
                    return new AnyRecipieValidRestriction();
                case "unique_by_id":
                    return new UniqueByIdRestriction();
                case "unique_by_id_with_neighbors":
                    return new UniqueByIdWithNeighborsRestriction();
                case "unique_by_trait":
                    return new UniqueByTraitRestriction(
                            xmlReaderUtil.ObjectReferenceLookup(
                                    element,
                                    "trait",
                                    gamedataTracker.Traits,
                                    t => t.Id));
                default:
                    throw new Exception("Unknown cell development placement restriction type " + type);
            }

        }
        
        public static CellDevelopmentRestriction ItemKnowledgeRestriction()
        {
            return new ItemKnowledgeDevelopmentRestriction();
        }
        
        private class CellHasTraitRestriction : CellDevelopmentRestriction
        {
            private Trait _trait;

            public CellHasTraitRestriction(Trait trait)
            {
                _trait = trait;
            }
            
            public override string IsValid(
                Tribe tribe, 
                Cell cell, 
                ICellDevelopmentType cellDevelopmentType)
            {
                if(cell.Traits.Contains(_trait))
                {
                    return null;
                }

                return "Must have " + _trait.Name;
            }

            public override string CouldBeValidWithoutFeature(
                Tribe tribe,
                Cell cell,
                ICellDevelopmentType cellDevelopmentType)
            {
                if(cell.Feature == null)
                {
                    return IsValid(tribe, cell, cellDevelopmentType);
                }

                var notTraits = cell.Biome.NotTraits
                        .Concat(cell.Development?.NotTraits ?? Enumerable.Empty<Trait>())
                        .ToArray();

                var traits = cell.Biome.Traits
                    .Concat(cell.Region?.Settlement?.Traits ?? Enumerable.Empty<Trait>())
                    .Concat(cell.Development?.Traits ?? Enumerable.Empty<Trait>())
                    .Except(notTraits)
                    .ToArray();

                if (traits.Contains(_trait))
                {
                    return null;
                }

                return "Must have " + _trait.Name;
            }

            public override bool IsValidChoice(Tribe tribe, ICellDevelopmentType cellDevelopmentType)
            {
                return true;
            }
        }

        private class CellNotHasTraitRestriction : CellDevelopmentRestriction
        {
            private Trait _trait;

            public CellNotHasTraitRestriction(Trait trait)
            {
                _trait = trait;
            }

            public override string CouldBeValidWithoutFeature(
                Tribe tribe, 
                Cell cell, 
                ICellDevelopmentType cellDevelopmentType)
            {
                if(cell.Feature == null)
                {
                    return IsValid(tribe, cell, cellDevelopmentType);
                }

                var notTraits = cell.Biome.NotTraits
                        .Concat(cell.Development?.NotTraits ?? Enumerable.Empty<Trait>())
                        .ToArray();

                var traits = cell.Biome.Traits
                    .Concat(cell.Region?.Settlement?.Traits ?? Enumerable.Empty<Trait>())
                    .Concat(cell.Development?.Traits ?? Enumerable.Empty<Trait>())
                    .Except(notTraits)
                    .ToArray();

                if (!traits.Contains(_trait))
                {
                    return null;
                }

                return "Cannot have " + _trait.Name;
            }

            public override string IsValid(
                Tribe tribe,
                Cell cell,
                ICellDevelopmentType cellDevelopmentType)
            {
                if(!cell.Traits.Contains(_trait))
                {
                    return null;
                }

                return "Cannot have " + _trait.Name;
            }

            public override bool IsValidChoice(Tribe tribe, ICellDevelopmentType cellDevelopmentType)
            {
                return true;
            }
        }

        private class RegionUnownedRestriction : CellDevelopmentRestriction
        {
            public override string CouldBeValidWithoutFeature(
                Tribe tribe, 
                Cell cell, 
                ICellDevelopmentType cellDevelopmentType)
            {
                return IsValid(tribe, cell, cellDevelopmentType);
            }

            public override string IsValid(
                Tribe tribe,
                Cell cell,
                ICellDevelopmentType cellDevelopmentType)
            {
                if(cell.Region.Owner == null)
                {
                    return null;
                }

                return "Region must be unclaimed";
            }
            
            public override bool IsValidChoice(Tribe tribe, ICellDevelopmentType cellDevelopmentType)
            {
                return true;
            }
        }

        private class RegionOwnedRestriction : CellDevelopmentRestriction
        {
            public override string CouldBeValidWithoutFeature(
                Tribe tribe,
                Cell cell,
                ICellDevelopmentType cellDevelopmentType)
            {
                return IsValid(tribe, cell, cellDevelopmentType);
            }

            public override string IsValid(
                Tribe tribe,
                Cell cell,
                ICellDevelopmentType cellDevelopmentType)
            {
                if(cell.Region.Owner == tribe)
                {
                    return null;
                }

                return "Region must be claimed.";
            }

            public override bool IsValidChoice(Tribe tribe, ICellDevelopmentType cellDevelopmentType)
            {
                return true;
            }
        }

        private class RegionOwnedOrVacantRestriction : CellDevelopmentRestriction
        {
            public override string CouldBeValidWithoutFeature(
                Tribe tribe,
                Cell cell,
                ICellDevelopmentType cellDevelopmentType)
            {
                return IsValid(tribe, cell, cellDevelopmentType);
            }

            public override string IsValid(
                Tribe tribe,
                Cell cell,
                ICellDevelopmentType cellDevelopmentType)
            {
                if (cell.Region.Owner == tribe || cell.Region.Owner == null)
                {
                    return null;
                }

                return "Region must be no be claimed by another tribe.";
            }

            public override bool IsValidChoice(Tribe tribe, ICellDevelopmentType cellDevelopmentType)
            {
                return true;
            }
        }

        private class CellNoDevelopmentRestriction : CellDevelopmentRestriction
        {
            public override string CouldBeValidWithoutFeature(
                Tribe tribe,
                Cell cell,
                ICellDevelopmentType cellDevelopmentType)
            {
                return IsValid(tribe, cell, cellDevelopmentType);
            }

            public override string IsValid(
                Tribe tribe,
                Cell cell,
                ICellDevelopmentType cellDevelopmentType)
            {
                if(cell.Development == null)
                {
                    return null;
                }

                return "Cell is already developed.";
            }

            public override bool IsValidChoice(Tribe tribe, ICellDevelopmentType cellDevelopmentType)
            {
                return true;
            }
        }

        private class CellSettlementAdjacentDevelopmentRestriction : CellDevelopmentRestriction
        {
            public override string CouldBeValidWithoutFeature(
                Tribe tribe,
                Cell cell,
                ICellDevelopmentType cellDevelopmentType)
            {
                return IsValid(tribe, cell, cellDevelopmentType);
            }

            public override string IsValid(
                Tribe tribe,
                Cell cell,
                ICellDevelopmentType cellDevelopmentType)
            {
                if (cell.OrthoNeighbors
                    .Where(c => c.Region == cell.Region)
                    .Where(c => c.Development is DistrictDevelopment)
                    .Any())
                {
                    return null;
                }

                return "Cell must be adjacent to a settlement district.";
            }

            public override bool IsValidChoice(Tribe tribe, ICellDevelopmentType cellDevelopmentType)
            {
                return true;
            }
        }

        private class OneAtATimeDevelopmentRestriction : CellDevelopmentRestriction
        {
            private Trait _trait;
            private string _description;
            
            public OneAtATimeDevelopmentRestriction(Trait trait, string description)
            {
                _trait = trait;
                _description = description;
            }

            public override string CouldBeValidWithoutFeature(
                Tribe tribe,
                Cell cell,
                ICellDevelopmentType cellDevelopmentType)
            {
                return IsValid(tribe, cell, cellDevelopmentType);
            }

            public override string IsValid(
                Tribe tribe,
                Cell cell,
                ICellDevelopmentType cellDevelopmentType)
            {
                var isMatchingConstruction = cell.Region.Cells
                    .Select(c => c.Development)
                    .Where(d => d != null)
                    .Where(d => d.UnderConstruction)
                    .Where(d => d.Traits.Contains(_trait))
                    .Any();
                
                if (isMatchingConstruction)
                {
                    return "There is already a " + _description + " under construction.";
                }

                return null;
            }

            public override bool IsValidChoice(Tribe tribe, ICellDevelopmentType cellDevelopmentType)
            {
                return true;
            }
        }

        private class NeighborNotHasTraitRestriction : CellDevelopmentRestriction
        {
            private Trait _trait;

            public NeighborNotHasTraitRestriction(Trait trait)
            {
                _trait = trait;
            }

            public override string CouldBeValidWithoutFeature(
                Tribe tribe,
                Cell cell,
                ICellDevelopmentType cellDevelopmentType)
            {
                return IsValid(tribe, cell, cellDevelopmentType);
            }

            public override string IsValid(
                Tribe tribe,
                Cell cell,
                ICellDevelopmentType cellDevelopmentType)
            {
                if (!cell.OrthoNeighbors.Any(n => n.TraitsAfterConstruction.Contains(_trait)))
                {
                    return null;
                }

                return "Cannot have neighbor with " + _trait.Name;
            }

            public override bool IsValidChoice(Tribe tribe, ICellDevelopmentType cellDevelopmentType)
            {
                return true;
            }
        }

        private class NeighborHasTraitRestriction : CellDevelopmentRestriction
        {
            private Trait _trait;

            public NeighborHasTraitRestriction(Trait trait)
            {
                _trait = trait;
            }

            public override string CouldBeValidWithoutFeature(
                Tribe tribe,
                Cell cell,
                ICellDevelopmentType cellDevelopmentType)
            {
                return IsValid(tribe, cell, cellDevelopmentType);
            }

            public override string IsValid(
                Tribe tribe,
                Cell cell,
                ICellDevelopmentType cellDevelopmentType)
            {
                if (cell.OrthoNeighbors.Any(n => n.TraitsAfterConstruction.Contains(_trait)))
                {
                    return null;
                }

                return "Needs neighbor with " + _trait.Name;
            }

            public override bool IsValidChoice(Tribe tribe, ICellDevelopmentType cellDevelopmentType)
            {
                return true;
            }
        }

        private class AnyRecipieValidRestriction : CellDevelopmentRestriction
        {
            public override string CouldBeValidWithoutFeature(
                Tribe tribe,
                Cell cell,
                ICellDevelopmentType cellDevelopmentType)
            {
                return IsValid(tribe, cell, cellDevelopmentType);
            }

            public override string IsValid(
                Tribe tribe,
                Cell cell,
                ICellDevelopmentType cellDevelopmentType)
            {
                var traits = cell.Traits.Concat(tribe.Traits);

                var isValid = cellDevelopmentType
                    .Recipies
                    .Any(r => traits.ContainsAll(r.RequiredTraits));

                if(!isValid)
                {
                    return "Cannot operate here.";
                }

                return null;
            }

            public override bool IsValidChoice(Tribe tribe, ICellDevelopmentType cellDevelopmentType)
            {
                return true;
            }
        }

        private class ItemKnowledgeDevelopmentRestriction : CellDevelopmentRestriction
        {
            public override string CouldBeValidWithoutFeature(
                Tribe tribe,
                Cell cell,
                ICellDevelopmentType cellDevelopmentType)
            {
                return IsValid(tribe, cell, cellDevelopmentType);
            }

            public override string IsValid(
                Tribe tribe,
                Cell cell,
                ICellDevelopmentType cellDevelopmentType)
            {
                return null;
            }

            public override bool IsValidChoice(Tribe tribe, ICellDevelopmentType cellDevelopmentType)
            {
                foreach (var item in cellDevelopmentType.ConstructionCost.Select(ir => ir.Item))
                {
                    if (!tribe.KnownItems[item])
                    {
                        return false;
                    }
                }

                return true;
            }
        }

        private class UniqueByIdRestriction : CellDevelopmentRestriction
        {
            public override string CouldBeValidWithoutFeature(
                Tribe tribe,
                Cell cell,
                ICellDevelopmentType cellDevelopmentType)
            {
                return IsValid(tribe, cell, cellDevelopmentType);
            }

            public override string IsValid(
                Tribe tribe,
                Cell cell,
                ICellDevelopmentType cellDevelopmentType)
            {
                var isValid = !cell.Region.Cells
                    .Where(c => c.Development != null && c.Development.TypeId == cellDevelopmentType.Id)
                    .Any();

                if (!isValid)
                {
                    return "Limit of one per region.";
                }

                return null;
            }

            public override bool IsValidChoice(Tribe tribe, ICellDevelopmentType cellDevelopmentType)
            {
                return true;
            }
        }

        private class UniqueByIdWithNeighborsRestriction : CellDevelopmentRestriction
        {
            public override string CouldBeValidWithoutFeature(
                Tribe tribe,
                Cell cell,
                ICellDevelopmentType cellDevelopmentType)
            {
                return IsValid(tribe, cell, cellDevelopmentType);
            }

            public override string IsValid(
                Tribe tribe,
                Cell cell,
                ICellDevelopmentType cellDevelopmentType)
            {
                var isValid = !cell.Region.Cells
                    .Concat(cell.Region.Neighbors.SelectMany(r => r.Cells))
                    .Where(c => c.Development != null && c.Development.TypeId == cellDevelopmentType.Id)
                    .Any();

                if (!isValid)
                {
                    return "Limit of one per region or neighboring regions.";
                }

                return null;
            }

            public override bool IsValidChoice(Tribe tribe, ICellDevelopmentType cellDevelopmentType)
            {
                return true;
            }
        }

        private class UniqueByTraitRestriction : CellDevelopmentRestriction
        {
            private Trait _trait;

            public UniqueByTraitRestriction(Trait trait)
            {
                _trait = trait;
            }

            public override string CouldBeValidWithoutFeature(
                Tribe tribe,
                Cell cell,
                ICellDevelopmentType cellDevelopmentType)
            {
                return IsValid(tribe, cell, cellDevelopmentType);
            }

            public override string IsValid(
                Tribe tribe,
                Cell cell,
                ICellDevelopmentType cellDevelopmentType)
            {
                var isValid = !cell.Region.Cells
                    .Where(c => c.Development != null && c.Development.Traits.Contains(_trait))
                    .Any();

                if (!isValid)
                {
                    return $"Limit of one development per region with the trait {_trait.Name}.";
                }

                return null;
            }

            public override bool IsValidChoice(Tribe tribe, ICellDevelopmentType cellDevelopmentType)
            {
                return true;
            }
        }
    }
}
