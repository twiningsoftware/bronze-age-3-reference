﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bronze.Common.Data.World.CellDevelopments;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.Data.Game.CellDevelopments
{
    public class DistrictDevelopmentType : AbstractCellDevelopmentType
    {
        private readonly INameSource _nameSource;
        private readonly IInjectionProvider _injectionProvider;
        
        public DistrictDevelopmentType(
            INameSource nameSource,
            IInjectionProvider injectionProvider)
        {
            _nameSource = nameSource;
            _injectionProvider = injectionProvider;
        }

        public override void Place(Tribe tribe, Cell cell, bool instantBuild)
        {
            if (cell.Region.Settlement == null)
            {
                cell.Region.Settlement = new Settlement(tribe.Race, tribe)
                {
                    Name = _nameSource.MakeSettlementName(tribe.Race, cell),
                    Region = cell.Region
                };
                tribe.Settlements.Add(cell.Region.Settlement);
            }

             cell.Development = _injectionProvider
                .Build<DistrictDevelopment>()
                .Initialize(cell, this, instantBuild);
        }

        public override ICellDevelopment DeserializeFrom(
            IGamedataTracker gamedataTracker,
            Cell cell,
            SerializedObject developmentRoot)
        {
            var development = _injectionProvider
                .Build<DistrictDevelopment>()
                .Initialize(cell, this, instantBuild: false);

            development.DeserializeFrom(gamedataTracker, developmentRoot);

            return development;
        }
    }
}
