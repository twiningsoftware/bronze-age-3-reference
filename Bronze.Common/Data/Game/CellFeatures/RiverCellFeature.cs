﻿using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System.Linq;
using System.Xml.Linq;

namespace Bronze.Common.Data.Game.CellFeatures
{
    public class RiverCellFeature : AbstractCellFeature
    {
        private RiverTerrain[] _riverTerrain;

        private string[] _riverAdjacenies;

        public override void ApplyGeneration(IWorldManager worldManager, District district, Tile[,] tiles)
        {
            var xNoiseGen = new SimplexNoiseGenerator(worldManager.Seed.GetHashCode() + 13);
            var yNoiseGen = new SimplexNoiseGenerator(worldManager.Seed.GetHashCode() + 97);

            var orderedTerrain = _riverTerrain
                .OrderBy(t => t.Threshold)
                .ToArray();

            for (var x = 0; x < TilePosition.TILES_PER_CELL; x++)
            {
                for (var y = 0; y < TilePosition.TILES_PER_CELL; y++)
                {
                    var tilePosition = new TilePosition(district.Cell, x, y);
                    float ul, ur, ll, lr;
                    float a, b;

                    var mutatedX = x + xNoiseGen.CoherentNoise(
                        tilePosition.ToVector().X,
                        tilePosition.ToVector().Y,
                        district.Cell.Position.Plane * 1000,
                        octaves: 1,
                        multiplier: 20,
                        amplitude: 25);

                    var mutatedY = y + yNoiseGen.CoherentNoise(
                        tilePosition.ToVector().X,
                        tilePosition.ToVector().Y,
                        district.Cell.Position.Plane * 1000,
                        octaves: 1,
                        multiplier: 20,
                        amplitude: 25);

                    var halfDistrict = TilePosition.TILES_PER_CELL / 2f;

                    if (mutatedX < TilePosition.TILES_PER_CELL / 2)
                    {
                        if (mutatedY < TilePosition.TILES_PER_CELL / 2)
                        {
                            a = mutatedX / halfDistrict + 0f;
                            b = mutatedY / halfDistrict + 0f;
                            ul = 1;
                            ur = (district.Cell.GetNeighbor(Facing.North) ?? district.Cell).AdjacencyGroups.ContainsAny(_riverAdjacenies) ? 0 : 1;
                            ll = (district.Cell.GetNeighbor(Facing.West) ?? district.Cell).AdjacencyGroups.ContainsAny(_riverAdjacenies) ? 0 : 1;
                            lr = district.Cell.AdjacencyGroups.ContainsAny(_riverAdjacenies) ? 0 : 1;
                        }
                        else
                        {
                            a = mutatedX / halfDistrict + 0f;
                            b = mutatedY / halfDistrict - 1f;
                            ul = (district.Cell.GetNeighbor(Facing.West) ?? district.Cell).AdjacencyGroups.ContainsAny(_riverAdjacenies) ? 0 : 1;
                            ur = district.Cell.AdjacencyGroups.ContainsAny(_riverAdjacenies) ? 0 : 1;
                            ll = 1;
                            lr = (district.Cell.GetNeighbor(Facing.South) ?? district.Cell).AdjacencyGroups.ContainsAny(_riverAdjacenies) ? 0 : 1;
                        }
                    }
                    else
                    {
                        if (mutatedY < TilePosition.TILES_PER_CELL / 2)
                        {
                            a = mutatedX / halfDistrict - 1f;
                            b = mutatedY / halfDistrict + 0f;
                            ul = (district.Cell.GetNeighbor(Facing.North) ?? district.Cell).AdjacencyGroups.ContainsAny(_riverAdjacenies) ? 0 : 1;
                            ur = 1;
                            ll = district.Cell.AdjacencyGroups.ContainsAny(_riverAdjacenies) ? 0 : 1;
                            lr = (district.Cell.GetNeighbor(Facing.East) ?? district.Cell).AdjacencyGroups.ContainsAny(_riverAdjacenies) ? 0 : 1;
                        }
                        else
                        {
                            a = mutatedX / halfDistrict - 1f;
                            b = mutatedY / halfDistrict - 1f;
                            ul = district.Cell.AdjacencyGroups.ContainsAny(_riverAdjacenies) ? 0 : 1;
                            ur = (district.Cell.GetNeighbor(Facing.East) ?? district.Cell).AdjacencyGroups.ContainsAny(_riverAdjacenies) ? 0 : 1;
                            ll = (district.Cell.GetNeighbor(Facing.South) ?? district.Cell).AdjacencyGroups.ContainsAny(_riverAdjacenies) ? 0 : 1;
                            lr = 1;
                        }
                    }

                    var value = GenUtils.Bilerp(ul, ur, ll, lr, a, b);

                    var terrain = orderedTerrain
                       .Where(t => value < t.Threshold)
                       .Select(t => t.Terrain)
                       .FirstOrDefault();

                    var wouldOverride = orderedTerrain
                        .Where(t => t.Terrain == tiles[x, y].Terrain)
                        .Where(t => value > t.Threshold)
                        .Any();

                    if (terrain != null && !wouldOverride)
                    {
                        tiles[x, y].Terrain = terrain;
                        tiles[x, y].Decoration = null;
                    }
                }
            }
        }

        public override void LoadFrom(IGamedataTracker gamedataTracker, IXmlReaderUtil xmlReaderUtil, XElement element)
        {
            base.LoadFrom(gamedataTracker, xmlReaderUtil, element);

            _riverAdjacenies = element
                .Elements("adjacency_group")
                .Select(e => xmlReaderUtil.AttributeValue(e, "group"))
                .ToArray();
                
            _riverTerrain = element.Elements("river_terrain")
                .Select(x => new RiverTerrain
                {
                    Threshold = xmlReaderUtil.AttributeAsDouble(x, "threshold"),
                    Terrain = xmlReaderUtil.ObjectReferenceLookup(x,
                        "terrain",
                        gamedataTracker.Terrain,
                        t => t.Id)
                })
                .ToArray();
        }

        private class RiverTerrain
        {
            public double Threshold { get; set; }
            public Terrain Terrain { get; set; }
        }
    }
}
