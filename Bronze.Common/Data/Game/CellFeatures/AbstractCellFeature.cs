﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Bronze.Common.UI.Elements;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;

namespace Bronze.Common.Data.Game.CellFeatures
{
    public abstract class AbstractCellFeature : ICellFeature
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string[] AdjacencyGroups { get; set; }

        public IEnumerable<Trait> Traits { get; set; }

        public IEnumerable<Trait> NotTraits { get; set; }

        public IEnumerable<CellAnimationLayer> DetailLayers { get; private set; }
        public IEnumerable<CellAnimationLayer> SummaryLayers { get; private set; }

        public DistrictGenerationLayer[] DistrictGenerationLayers { get; set; }

        public MinimapColor MinimapColor { get; private set; }

        public abstract void ApplyGeneration(IWorldManager worldManager, District district, Tile[,] tiles);

        public virtual void LoadFrom(IGamedataTracker gamedataTracker, IXmlReaderUtil xmlReaderUtil, XElement element)
        {
            Id = xmlReaderUtil.AttributeValue(element, "id");
            Name = xmlReaderUtil.AttributeValue(element, "name");
            MinimapColor = xmlReaderUtil.AttributeAsEnum<MinimapColor>(element, "minimap_color");
            AdjacencyGroups = element.Elements("adjacency_group")
                .Select(e => xmlReaderUtil.AttributeValue(e, "group"))
                .ToArray();
            DetailLayers = element
                .Elements("detailed_sprite")
                .Select(e => xmlReaderUtil.LoadCellSpriteLayer(e))
                .Concat(element
                    .Elements("detailed_animation")
                    .Select(e => xmlReaderUtil.LoadCellAnimationLayer(e)))
                .ToArray();
            SummaryLayers = element
                .Elements("summary_sprite")
                .Select(e => xmlReaderUtil.LoadCellSpriteLayer(e))
                .Concat(element
                    .Elements("summary_animation")
                    .Select(e => xmlReaderUtil.LoadCellAnimationLayer(e)))
                .ToArray();
            Traits = element
                .Elements("trait")
                .Select(e => xmlReaderUtil.ObjectReferenceLookup(e,
                    "trait",
                    gamedataTracker.Traits,
                    t => t.Id))
                .ToArray();
            NotTraits = element
                .Elements("not_trait")
                .Select(e => xmlReaderUtil.ObjectReferenceLookup(e,
                    "trait",
                    gamedataTracker.Traits,
                    t => t.Id));
            DistrictGenerationLayers = element
                    .Elements("district_generation_layer")
                    .Select(e => xmlReaderUtil.LoadDistrictGenerationLayer(e))
                    .ToArray();
        }

        public virtual IUiElement BuildDetailColumn()
        {
            return new StackGroup
            {
                Orientation = Orientation.Vertical,
                Children = new List<IUiElement>
                {
                    new Label(Name),
                    new Spacer(0, 5),
                    new TraitDisplay(() => Traits)
                }
            };
        }
    }
}
