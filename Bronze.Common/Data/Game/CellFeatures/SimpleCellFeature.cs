﻿using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.Data.Game.CellFeatures
{
    public class SimpleCellFeature : AbstractCellFeature
    {
        public override void ApplyGeneration(IWorldManager worldManager, District district, Tile[,] tiles)
        {
        }
    }
}
