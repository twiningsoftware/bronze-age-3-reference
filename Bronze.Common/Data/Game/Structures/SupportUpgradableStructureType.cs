﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Bronze.Common.Data.World.Structures;
using Bronze.Common.UI;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Elements;

namespace Bronze.Common.Data.Game.Structures
{
    public class SupportUpgradableStructureType : AbstractStructureType
    {
        public ServiceType ServiceNeeded { get; private set; }
        public SupportUpgradableStructureType UpgradesTo { get; private set; }
        public SupportUpgradableStructureType DowngradesTo { get; private set; }
        public double DowngradeMonths { get; private set; }
        
        private readonly IInjectionProvider _injectionProvider;

        public SupportUpgradableStructureType(IInjectionProvider injectionProvider)
        {
            _injectionProvider = injectionProvider;
        }

        public override IStackUiElement BuildTypeInfoPanel(int width, bool withName)
        {
            var stack = base.BuildTypeInfoPanel(width, withName);

            if(UpgradesTo != null)
            {
                stack.Children.Add(new StackGroup
                {
                    Orientation = Bronze.UI.Data.Orientation.Horizontal,
                    Children = new List<IUiElement>
                    {
                        new Text($"Upgrades to {UpgradesTo.Name} with "),
                        UiBuilder.BuildIconFor(ServiceNeeded)
                    }
                });
            }
            if(DowngradesTo != null)
            {
                stack.Children.Add(new StackGroup
                {
                    Orientation = Bronze.UI.Data.Orientation.Horizontal,
                    Children = new List<IUiElement>
                    {
                        new Text($"Downgrades to {DowngradesTo.Name} without "),
                        UiBuilder.BuildIconFor(ServiceNeeded)
                    }
                });
            }

            return stack;
        }

        public override void PostLink(
            IGamedataTracker gamedataTracker, 
            IXmlReaderUtil xmlReaderUtil, 
            XElement element)
        {
            base.PostLink(gamedataTracker, xmlReaderUtil, element);

            var operationElement = xmlReaderUtil.Element(element, "operation");

            var upgradeElement = xmlReaderUtil.Element(operationElement, "support_upgrade");

            ServiceNeeded = xmlReaderUtil.ObjectReferenceLookup(
                upgradeElement, 
                "service_needed",
                gamedataTracker.ServiceTypes,
                s => s.Name);

            UpgradesTo = xmlReaderUtil.OptionalObjectReferenceLookup(
                upgradeElement,
                "upgrades_to",
                gamedataTracker.StructureTypes.OfType<SupportUpgradableStructureType>(),
                s => s.Id);

            DowngradesTo = xmlReaderUtil.OptionalObjectReferenceLookup(
                upgradeElement,
                "downgrades_to",
                gamedataTracker.StructureTypes.OfType<SupportUpgradableStructureType>(),
                s => s.Id);

            if(DowngradesTo != null)
            {
                DowngradeMonths = xmlReaderUtil.AttributeAsDouble(upgradeElement, "downgrade_months");
            }
        }

        protected override IStructure CreateStructureInstance(Tile ulTile)
        {
            return _injectionProvider.Build<SupportUpgradableStructure>()
                .Initialize(ulTile, this);
        }

        public override IStructure DeserializeFrom(
            IGamedataTracker gamedataTracker,
            Settlement settlement,
            Cell cell,
            Tile[,] tiles,
            SerializedObject structureRoot)
        {
            var ulTilePos = TilePosition.DeserializeFrom(
                cell,
                structureRoot.GetChild("ultile_position"));

            var ulTile = tiles[ulTilePos.X, ulTilePos.Y];

            var structure = _injectionProvider
                .Build<SupportUpgradableStructure>()
                .Initialize(ulTile, this);

            structure.DeserializeFrom(gamedataTracker, settlement, structureRoot);

            return structure;
        }
    }
}
