﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Bronze.Common.Data.World.Structures;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;

namespace Bronze.Common.Data.Game.Structures
{
    public class BarracksStructureType : AbstractStructureType, IRecruitmentSlotProviderType
    {
        private readonly IInjectionProvider _injectionProvider;
        
        public RecruitmentSlotCategory RecruitmentCategory { get; private set; }
        public int ProvidedRecruitmentSlots { get; private set; }
        public IntIndexableLookup<Item, bool> ItemsAllowed { get; private set; }

        public BarracksStructureType(
            IInjectionProvider injectionProvider)
        {
            _injectionProvider = injectionProvider;
        }

        protected override IStructure CreateStructureInstance(Tile ulTile)
        {
            return _injectionProvider.Build<BarracksStructure>()
                .Initialize(ulTile, this);
        }
        
        public override IStackUiElement BuildTypeInfoPanel(int width, bool withName)
        {
            var stackGroup = base.BuildTypeInfoPanel(width, withName);

            stackGroup.Children.Add(new Spacer(0, 10));
            stackGroup.Children.Add(new StackGroup
            {
                Orientation = Orientation.Vertical,
                Children = new List<IUiElement>
                {
                    new Label($"Provides {ProvidedRecruitmentSlots} {RecruitmentCategory.Name} recruits."),
                    new Label($"Provides an armory stockpile of {InventoryCapacity}")
                }
            });

            return stackGroup;
        }

        protected override void LoadFrom(IGamedataTracker gamedataTracker, IXmlReaderUtil xmlReaderUtil, XElement element, Race race)
        {
            base.LoadFrom(gamedataTracker, xmlReaderUtil, element, race);

            var recruitmentElement = xmlReaderUtil.Element(element, "recruitment");

            LoadRecruitmentElement(gamedataTracker, xmlReaderUtil, recruitmentElement);
        }


        protected virtual void LoadRecruitmentElement(IGamedataTracker gamedataTracker, IXmlReaderUtil xmlReaderUtil, XElement recruitmentElement)
        {
            RecruitmentCategory = xmlReaderUtil
                .ObjectReferenceLookup(
                    recruitmentElement,
                    "category",
                    gamedataTracker.RecruitmentSlotCategories,
                    c => c.Id);
            
            ProvidedRecruitmentSlots = xmlReaderUtil.AttributeAsInt(recruitmentElement, "slots");

            var itemList = recruitmentElement
                .Elements("armory")
                .Select(e => xmlReaderUtil.ObjectReferenceLookup(
                    e,
                    "item",
                    gamedataTracker.Items,
                    i => i.Name))
                .ToArray();

            ItemsAllowed = new IntIndexableLookup<Item, bool>();
            foreach (var item in gamedataTracker.Items)
            {
                ItemsAllowed[item] = itemList.Contains(item);
            }
        }

        public override IStructure DeserializeFrom(
            IGamedataTracker gamedataTracker,
            Settlement settlement,
            Cell cell,
            Tile[,] tiles,
            SerializedObject structureRoot)
        {
            var ulTilePos = TilePosition.DeserializeFrom(
                cell,
                structureRoot.GetChild("ultile_position"));

            var ulTile = tiles[ulTilePos.X, ulTilePos.Y];

            var structure = _injectionProvider
                .Build<BarracksStructure>()
                .Initialize(ulTile, this);

            structure.DeserializeFrom(gamedataTracker, settlement, structureRoot);

            return structure;
        }
    }
}