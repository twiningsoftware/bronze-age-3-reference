﻿using Bronze.Common.Data.World.Structures;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Bronze.Common.Data.Game.Structures
{
    public class DistributorStructureType : AbstractServiceProviderStructureType
    {
        private readonly IInjectionProvider _injectionProvider;
        
        public int DistributionMaxItems { get; private set; }
        public IEnumerable<Item> InitialDistribution { get; private set; }
        
        public DistributorStructureType(IInjectionProvider injectionProvider)
        {
            _injectionProvider = injectionProvider;
        }

        protected override void LoadServiceElement(
            IGamedataTracker gamedataTracker, 
            IXmlReaderUtil xmlReaderUtil, 
            XElement serviceElement,
            Race race)
        {
            base.LoadServiceElement(gamedataTracker, xmlReaderUtil, serviceElement, race);
            
            DistributionMaxItems = xmlReaderUtil.AttributeAsInt(serviceElement, "max_items");
            
            InitialDistribution = serviceElement
                .Elements("item")
                .Select(e => xmlReaderUtil.ObjectReferenceLookup(
                    e,
                    "name",
                    gamedataTracker.Items,
                    i => i.Name))
                .ToArray();
        }

        protected override IStructure CreateStructureInstance(Tile ulTile)
        {
            return _injectionProvider.Build<DistributorStructure>()
                .Initialize(ulTile, this);
        }
        
        public override IStructure DeserializeFrom(
            IGamedataTracker gamedataTracker,
            Settlement settlement,
            Cell cell,
            Tile[,] tiles,
            SerializedObject structureRoot)
        {
            var ulTilePos = TilePosition.DeserializeFrom(
                cell,
                structureRoot.GetChild("ultile_position"));

            var ulTile = tiles[ulTilePos.X, ulTilePos.Y];

            var structure = _injectionProvider
                .Build<DistributorStructure>()
                .Initialize(ulTile, this);

            structure.DeserializeFrom(gamedataTracker, settlement, structureRoot);

            return structure;
        }

        public override bool CanService(IEconomicActor economicActor)
        {
            return economicActor is IHousingProvider;
        }

        public override bool CanService(IEconomicActorType economicActorType)
        {
            return economicActorType is HousingStructureType;
        }
    }
}
