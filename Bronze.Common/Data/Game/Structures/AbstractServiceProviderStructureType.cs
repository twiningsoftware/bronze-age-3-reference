﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Bronze.Common.UI;
using Bronze.Contracts;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Priority_Queue;

namespace Bronze.Common.Data.Game.Structures
{
    public abstract class AbstractServiceProviderStructureType : AbstractStructureType, IServiceProviderStructureType
    {
        public ServiceType ServiceType { get; private set; }
        public double ServiceRange { get; private set; }
        public IEnumerable<Trait> ServiceTransmissionTraits { get; private set; }

        protected override void LoadOperationElement(
            IGamedataTracker gamedataTracker,
            IXmlReaderUtil xmlReaderUtil,
            XElement operationElement,
            Race race)
        {
            base.LoadOperationElement(gamedataTracker, xmlReaderUtil, operationElement, race);

            var serviceElement = xmlReaderUtil.Element(operationElement, "service");

            LoadServiceElement(
                gamedataTracker,
                xmlReaderUtil,
                serviceElement,
                race);
        }

        protected virtual void LoadServiceElement(
            IGamedataTracker gamedataTracker,
            IXmlReaderUtil xmlReaderUtil,
            XElement serviceElement,
            Race race)
        {
            ServiceType = xmlReaderUtil.ObjectReferenceLookup(
                serviceElement,
                "type",
                gamedataTracker.ServiceTypes,
                s => s.Name);

            ServiceRange = xmlReaderUtil.AttributeAsInt(serviceElement, "range");

            ServiceTransmissionTraits = serviceElement
                .Elements("transmission")
                .Select(x => xmlReaderUtil.ObjectReferenceLookup(
                    x,
                    "trait",
                    gamedataTracker.Traits,
                    t => t.Id))
                .ToArray();
        }

        public IEnumerable<Tile> CalculateServiceArea(Tile ulTile)
        {
            var open = new SimplePriorityQueue<Tile, double>();
            var distTo = new Dictionary<Tile, double>();
            var closed = new HashSet<Tile>();

            var footprint = new List<Tile>();
            for (var i = 0; i < TilesWide; i++)
            {
                for (var j = 0; j < TilesHigh; j++)
                {
                    var tile = ulTile.District.Settlement
                        .GetTile(ulTile.Position.Relative(i, j));

                    if (tile != null)
                    {
                        footprint.Add(tile);
                        open.Enqueue(tile, 0);
                        distTo.Add(tile, 0);
                    }
                }
            }

            while (open.Any())
            {
                var current = open.Dequeue();

                closed.Add(current);

                var cost = current.LogiDistance;

                if (distTo[current] + cost <= ServiceRange)
                {
                    var neighborsToExpandTo = current.OrthoNeighbors
                        .Where(t => !closed.Contains(t))
                        .Where(t => t.Traits.ContainsAll(ServiceTransmissionTraits))
                        .ToArray();

                    foreach (var neighbor in neighborsToExpandTo)
                    {
                        var dist = distTo[current] + cost;

                        if (distTo.ContainsKey(neighbor))
                        {
                            if (dist < distTo[neighbor])
                            {
                                distTo[neighbor] = dist;
                                open.UpdatePriority(neighbor, dist);
                            }
                        }
                        else
                        {
                            open.Enqueue(neighbor, dist);
                            distTo.Add(neighbor, dist);
                        }
                    }
                }
            }

            return closed.ToArray();
        }

        public override IStackUiElement BuildTypeInfoPanel(int width, bool withName)
        {
            var stack = base.BuildTypeInfoPanel(width, withName);
            stack.Children.Add(UiBuilder.BuildServiceProvidedTypeRow(ServiceType, ServiceRange));
            return stack;
        }
        public abstract bool CanService(IEconomicActor economicActor);

        public abstract bool CanService(IEconomicActorType economicActorType);
    }
}
