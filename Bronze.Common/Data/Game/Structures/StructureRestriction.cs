﻿using System;
using System.Linq;
using System.Xml.Linq;
using Bronze.Common.Data.World.Structures;
using Bronze.Contracts;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.Data.Game.Structures
{
    public abstract class StructureRestriction
    {
        public abstract string IsValid(Tile ulTile, IStructureType structureType);

        public abstract bool IsValidChoice(Settlement settlement, IStructureType structureType);

        public static StructureRestriction Load(
            IGamedataTracker gamedataTracker,
            IXmlReaderUtil xmlReaderUtil, 
            XElement element)
        {
            var type = xmlReaderUtil.AttributeValue(element, "type");

            switch(type)
            {
                case "has_trait":
                    return new HasTraitStructureRestriction(
                            xmlReaderUtil.ObjectReferenceLookup(
                                    element,
                                    "trait",
                                    gamedataTracker.Traits,
                                    t => t.Id));
                case "not_has_trait":
                    return new NotHasTraitStructureRestriction(
                            xmlReaderUtil.ObjectReferenceLookup(
                                    element,
                                    "trait",
                                    gamedataTracker.Traits,
                                    t => t.Id));
                case "neighbor_has_trait":
                    return new NeighborHasTraitStructureRestriction(
                            xmlReaderUtil.ObjectReferenceLookup(
                                    element,
                                    "trait",
                                    gamedataTracker.Traits,
                                    t => t.Id));
                case "no_structure":
                    return new NoStructureStructureRestriction();
                default:
                    throw new Exception("Unknown structure placement restriction type " + type);
            }
        }
        
        public static StructureRestriction ItemKnowledgeRestriction()
        {
            return new ItemKnowledgeStructureRestriction();
        }

        private class HasTraitStructureRestriction : StructureRestriction
        {
            private Trait _trait;

            public HasTraitStructureRestriction(Trait trait)
            {
                _trait = trait;
            }

            public override string IsValid(
                Tile ulTile,
                IStructureType structureType)
            {
                for (var i = 0; i < structureType.TilesWide; i++)
                {
                    for (var j = 0; j < structureType.TilesHigh; j++)
                    {
                        var tile = ulTile?.District?.Settlement
                            ?.GetTile(ulTile.Position.Relative(i, j));

                        if(tile == null || !tile.Terrain.Traits.Contains(_trait))
                        {
                            return "Must have " + _trait.Name;
                        }
                    }
                }

                return null;
            }

            public override bool IsValidChoice(
                Settlement settlement, 
                IStructureType structureType)
            {
                return true;
            }
        }
        
        private class NotHasTraitStructureRestriction : StructureRestriction
        {
            private Trait _trait;

            public NotHasTraitStructureRestriction(Trait trait)
            {
                _trait = trait;
            }

            public override string IsValid(
                Tile ulTile,
                IStructureType structureType)
            {
                for (var i = 0; i < structureType.TilesWide; i++)
                {
                    for (var j = 0; j < structureType.TilesHigh; j++)
                    {
                        var tile = ulTile?.District?.Settlement
                            ?.GetTile(ulTile.Position.Relative(i, j));

                        if (tile == null || tile.Terrain.Traits.Contains(_trait))
                        {
                            return "Cannot have " + _trait.Name;
                        }
                    }
                }

                return null;
            }

            public override bool IsValidChoice(
                Settlement settlement,
                IStructureType structureType)
            {
                return true;
            }
        }

        private class NeighborHasTraitStructureRestriction : StructureRestriction
        {
            private Trait _trait;

            public NeighborHasTraitStructureRestriction(Trait trait)
            {
                _trait = trait;
            }

            public override string IsValid(
                Tile ulTile,
                IStructureType structureType)
            {
                for (var i = 0; i < structureType.TilesWide; i++)
                {
                    for (var j = 0; j < structureType.TilesHigh; j++)
                    {
                        var tile = ulTile?.District?.Settlement
                            ?.GetTile(ulTile.Position.Relative(i, j));

                        if (tile == null)
                        {
                            return "Must be next to  " + _trait.Name;
                        }

                        if (tile.OrthoNeighbors.Any(n => n.Traits.Contains(_trait)))
                        {
                            return null;
                        }
                    }
                }

                return "Must be next to " + _trait.Name;
            }

            public override bool IsValidChoice(
                Settlement settlement,
                IStructureType structureType)
            {
                return true;
            }
        }

        private class NoStructureStructureRestriction : StructureRestriction
        {
            public NoStructureStructureRestriction()
            {
            }

            public override string IsValid(
                Tile ulTile,
                IStructureType structureType)
            {
                for (var i = 0; i < structureType.TilesWide; i++)
                {
                    for (var j = 0; j < structureType.TilesHigh; j++)
                    {
                        var tile = ulTile?.District?.Settlement
                            ?.GetTile(ulTile.Position.Relative(i, j));


                        // If doing a road over road placement, don't restrict it
                        if (tile != null 
                            && tile.Structure != null
                            && !(structureType is SimpleServiceProviderStructure && tile.Structure is SimpleServiceProviderStructure))
                        {
                            return "Occupied by structure.";
                        }
                    }
                }

                return null;
            }

            public override bool IsValidChoice(
                Settlement settlement,
                IStructureType structureType)
            {
                return true;
            }
        }
        
        private class ItemKnowledgeStructureRestriction : StructureRestriction
        {
            public ItemKnowledgeStructureRestriction()
            {
            }

            public override string IsValid(
                Tile ulTile,
                IStructureType structureType)
            {
                return null;
            }

            public override bool IsValidChoice(
                Settlement settlement,
                IStructureType structureType)
            {
                foreach (var item in structureType.ConstructionCost.Select(ir => ir.Item))
                {
                    if (!settlement.Owner.KnownItems[item])
                    {
                        return false;
                    }
                }

                return true;
            }
        }
    }
}
