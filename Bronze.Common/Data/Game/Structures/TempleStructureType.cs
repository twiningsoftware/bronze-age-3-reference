﻿using System.Xml.Linq;
using Bronze.Common.Data.World.Structures;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.Data.Game.Structures
{
    public class TempleStructureType : AbstractServiceProviderStructureType
    {
        public Cult Cult { get; set; }
        public double InfluencePerPop { get; set; }
        public int BonusAuthority { get; set; }
        
        private readonly IInjectionProvider _injectionProvider;
        
        public TempleStructureType(IInjectionProvider injectionProvider)
        {
            _injectionProvider = injectionProvider;
        }
        
        protected override IStructure CreateStructureInstance(Tile ulTile)
        {
            return _injectionProvider.Build<TempleStructure>()
                .Initialize(ulTile, this);
        }

        protected override void LoadOperationElement(IGamedataTracker gamedataTracker, IXmlReaderUtil xmlReaderUtil, XElement operationElement, Race race)
        {
            base.LoadOperationElement(gamedataTracker, xmlReaderUtil, operationElement, race);

            var cultInfluenceElement = xmlReaderUtil
                .Element(operationElement, "cult_influence");

            Cult = xmlReaderUtil.ObjectReferenceLookup(
                cultInfluenceElement,
                "cult",
                gamedataTracker.Cults,
                c => c.Id);

            InfluencePerPop = xmlReaderUtil.AttributeAsDouble(
                cultInfluenceElement,
                "influence_per_pop");

            var bonusAuthorityElement = xmlReaderUtil.ElementOrNull(
                operationElement,
                "bonus_authority");

            if (bonusAuthorityElement != null)
            {
                BonusAuthority = xmlReaderUtil.AttributeAsInt(
                    bonusAuthorityElement,
                    "authority");
            }
        }

        public override IStructure DeserializeFrom(
            IGamedataTracker gamedataTracker,
            Settlement settlement,
            Cell cell,
            Tile[,] tiles, 
            SerializedObject structureRoot)
        {
            var ulTilePos = TilePosition.DeserializeFrom(
                cell,
                structureRoot.GetChild("ultile_position"));

            var ulTile = tiles[ulTilePos.X, ulTilePos.Y];

            var structure = _injectionProvider
                .Build<TempleStructure>()
                .Initialize(ulTile, this);

            structure.DeserializeFrom(gamedataTracker, settlement, structureRoot);

            return structure;
        }

        public override bool CanService(IEconomicActor economicActor)
        {
            return economicActor is IHousingProvider;
        }

        public override bool CanService(IEconomicActorType economicActorType)
        {
            return economicActorType is HousingStructureType;
        }
    }
}
