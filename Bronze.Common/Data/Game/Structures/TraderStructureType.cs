﻿using System.Collections.Generic;
using System.Xml.Linq;
using Bronze.Common.Data.World.Structures;
using Bronze.Common.UI;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;

namespace Bronze.Common.Data.Game.Structures
{
    public class TraderStructureType : AbstractStructureType
    {
        private readonly IInjectionProvider _injectionProvider;

        public MovementType TradeType { get; private set; }
        public double TradeCapacity { get; private set; }
        public TradeHaulerInfo TradeHauler { get; private set; }

        public TraderStructureType(IInjectionProvider injectionProvider)
        {
            _injectionProvider = injectionProvider;
        }
        
        protected override IStructure CreateStructureInstance(Tile ulTile)
        {
            return _injectionProvider.Build<TraderStructure>()
                .Initialize(ulTile, this);
        }

        public override IStructure DeserializeFrom(
            IGamedataTracker gamedataTracker,
            Settlement settlement,
            Cell cell,
            Tile[,] tiles, 
            SerializedObject structureRoot)
        {
            var ulTilePos = TilePosition.DeserializeFrom(
                cell,
                structureRoot.GetChild("ultile_position"));

            var ulTile = tiles[ulTilePos.X, ulTilePos.Y];

            var structure = _injectionProvider
                .Build<TraderStructure>()
                .Initialize(ulTile, this);

            structure.DeserializeFrom(gamedataTracker, settlement, structureRoot);

            return structure;
        }

        protected override void LoadFrom(
            IGamedataTracker gamedataTracker,
            IXmlReaderUtil xmlReaderUtil,
            XElement element,
            Race race)
        {
            base.LoadFrom(gamedataTracker, xmlReaderUtil, element, race);

            var tradeElement = xmlReaderUtil.Element(element, "trade");
            LoadTradeElement(gamedataTracker, xmlReaderUtil, tradeElement);
        }

        protected virtual void LoadTradeElement(
            IGamedataTracker gamedataTracker, 
            IXmlReaderUtil xmlReaderUtil, 
            XElement tradeElement)
        {
            TradeType = xmlReaderUtil.ObjectReferenceLookup(
                tradeElement,
                "movement_type",
                gamedataTracker.MovementTypes,
                m => m.Id);

            TradeCapacity = xmlReaderUtil.AttributeAsDouble(tradeElement, "capacity");

            TradeHauler = xmlReaderUtil.LoadTradeHaulerInfo(xmlReaderUtil.Element(tradeElement, "trade_hauler"));
        }

        public override IStackUiElement BuildTypeInfoPanel(int width, bool withName)
        {
            var stackGroup = base.BuildTypeInfoPanel(width, withName);

            stackGroup.Children.AddRange(new AbstractUiElement[]
            {
                new Spacer(0, 5),
                new StackGroup
                {
                    Orientation = Orientation.Horizontal,
                    Children = new List<IUiElement>
                    {
                        new Label("Trade: "),
                        UiBuilder.BuildIconFor(TradeType),
                        new Label(TradeCapacity.ToString()),
                    }
                }
            });

            return stackGroup;
        }
    }
}
