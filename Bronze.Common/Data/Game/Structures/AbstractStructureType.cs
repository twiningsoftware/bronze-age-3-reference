﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Bronze.Common.UI;
using Bronze.Common.UI.Elements;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.Exceptions;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;

namespace Bronze.Common.Data.Game.Structures
{
    public abstract class AbstractStructureType : IStructureType
    {
        public string Id { get; set; }
        public string IconKey { get; set; }
        public string Name { get; set; }
        public bool UpgradeOnly { get; set; }
        public string Description { get; set; }
        public string[] AdjacencyGroups { get; set; }
        public IEnumerable<ITileDisplay> AnimationLayers { get; set; }
        public int TilesWide { get; set; }
        public int TilesHigh { get; set; }
        public StructurePlacementType PlacementType { get; set; }
        public WorkerNeed ConstructionWorkers { get; set; }
        public WorkerNeed OperationWorkers { get; set; }
        public double ConstructionMonths { get; set; }
        public IEnumerable<ItemQuantity> ConstructionCost { get; set; }
        public IntIndexableLookup<Item, double> ConstructionCostLookup { get; set; }
        public IEnumerable<ITileDisplay> ConstructionAnimationLayers { get; set; }
        public int InventoryCapacity { get; set; }
        public IEnumerable<Recipie> Recipies { get; set;  }
        public double LogiDistance { get; set; }
        public HaulerInfo[] LogiSourceHaulers { get; set; }
        public IEnumerable<Trait> Traits { get; set; }
        public IEnumerable<Trait> NotTraits { get; set; }
        public IEnumerable<ItemRate> UpkeepInputs { get; set; }
        public MinimapColor MinimapColor { get; private set; }
        public IEnumerable<IStructureType> ManuallyUpgradesTo { get; private set; }
        public IStructureType ManuallyDowngradesTo { get; private set; }

        private List<StructureRestriction> _restrictions;

        public bool CanBePlaced(Tile ulTile)
        {
            return GetPlacementMessages(ulTile).Length == 0;
        }

        public string[] GetPlacementMessages(Tile ulTile)
        {
            return _restrictions
                .Select(x => x.IsValid(ulTile, this))
                .Where(x => x != null)
                .ToArray();
        }

        public bool CouldBePlaced(Settlement settlement)
        {
            return _restrictions
                .All(x => x.IsValidChoice(settlement, this));
        }

        public virtual IStructure Place(Tile ulTile)
        {
            if(ulTile.Structure != null)
            {
                if (ulTile.Structure.TypeId == Id
                    && ulTile.Structure.UlTile == ulTile)
                {
                    // We're doing a draw-over upgrade, and placing the same type 
                    // of structure over itself, can safely abort
                    return ulTile.Structure;
                }
                else
                {
                    ulTile.District.RemoveStructure(ulTile.Structure);
                }
            }

            var structure = CreateStructureInstance(ulTile);
            
            ulTile.District.AddStructure(structure);

            foreach(var tile in structure.Footprint)
            {
                tile.Decoration = null;
            }
            
            return structure;
        }

        public virtual IStackUiElement BuildTypeInfoPanel(int width, bool withName)
        {
            var stackGroup = new StackGroup
            {
                Orientation = Orientation.Vertical,
                Width = Width.Fixed(width)
            };

            if(withName)
            {
                stackGroup.Children.Add(new StackGroup
                {
                    Orientation = Orientation.Horizontal,
                    VerticalContentAlignment = VerticalAlignment.Middle,
                    Children = new List<IUiElement>
                        {
                            new Label(Name)
                        }
                });

                stackGroup.Children.Add(new Spacer(0, 10));
            }

            stackGroup.Children.AddRange(new IUiElement[]
            {
                new TraitDisplay(() => Traits),
                new Spacer(0, 5),
                new Text(Description)
                {
                    Width = Width.Fixed(width)
                },
                UiBuilder.BuildConstructionInfoPanel(this),
                UiBuilder.BuildEconomicInfoPanel(this)
            });

            return stackGroup;
        }

        public void LoadFrom(
            IGamedataTracker gamedataTracker, 
            IXmlReaderUtil xmlReaderUtil, 
            XElement element)
        {
            var forRace = xmlReaderUtil.ObjectReferenceLookup(
                element,
                "race",
                gamedataTracker.Races,
                r => r.Id);

            forRace.AvailableStructures.Add(this);

            LoadFrom(gamedataTracker, xmlReaderUtil, element, forRace);
        }

        protected virtual void LoadFrom(
            IGamedataTracker gamedataTracker, 
            IXmlReaderUtil xmlReaderUtil, 
            XElement element, 
            Race race)
        {
            Id = xmlReaderUtil.AttributeValue(element, "id");
            IconKey = xmlReaderUtil.AttributeValue(element, "icon");
            Name = xmlReaderUtil.AttributeValue(element, "name");
            UpgradeOnly = xmlReaderUtil.AttributeAsBool(element, "upgrade_only");
            Description = xmlReaderUtil.SanitizeTextForFont(xmlReaderUtil.Element(element, "description").Value);
            PlacementType = xmlReaderUtil.AttributeAsEnum<StructurePlacementType>(element, "placement_type");
            TilesWide = xmlReaderUtil.AttributeAsInt(element, "tiles_wide");
            TilesHigh = xmlReaderUtil.AttributeAsInt(element, "tiles_high");
            MinimapColor = xmlReaderUtil.AttributeAsEnum<MinimapColor>(element, "minimap_color");
            AdjacencyGroups = element.Elements("adjacency_group")
                .Select(e => xmlReaderUtil.AttributeValue(e, "group"))
                .ToArray();
            AnimationLayers = xmlReaderUtil.LoadTileDisplays(element);
            Traits = element
                .Elements("trait")
                .Select(e => xmlReaderUtil.ObjectReferenceLookup(e,
                    "trait",
                    gamedataTracker.Traits,
                    t => t.Id))
                .ToArray();
            NotTraits = element
                .Elements("not_trait")
                .Select(e => xmlReaderUtil.ObjectReferenceLookup(e,
                    "trait",
                    gamedataTracker.Traits,
                    t => t.Id))
                .ToArray();

            _restrictions = element
                .Elements("placement_restriction")
                .Select(e => StructureRestriction.Load(gamedataTracker, xmlReaderUtil, e))
                .Concat(StructureRestriction.ItemKnowledgeRestriction().Yield())
                .ToList();

            LoadConstructionElement(xmlReaderUtil, xmlReaderUtil.Element(element, "construction"), race);

            LoadOperationElement(gamedataTracker, xmlReaderUtil, xmlReaderUtil.Element(element, "operation"), race);

            LoadLogiElement(gamedataTracker, xmlReaderUtil, xmlReaderUtil.Element(element, "logi"));
             
            if(Recipies.Any() && !LogiSourceHaulers.Any())
            {
                throw DataLoadException.MiscError(element, "Structure type has a recipie, but no logi source haulers. Structure will not be able to deliver items.");
            }
        }

        protected virtual void LoadConstructionElement(IXmlReaderUtil xmlReaderUtil, XElement constructionElement, Race race)
        {
            ConstructionWorkers = xmlReaderUtil.LoadWorkerNeed(constructionElement, race);
            ConstructionMonths = xmlReaderUtil.AttributeAsDouble(constructionElement, "months");
            ConstructionAnimationLayers = xmlReaderUtil.LoadTileDisplays(constructionElement);
            ConstructionCost = constructionElement
                .Elements("input")
                .Select(e => xmlReaderUtil.LoadItemRate(e).ToQuantity(ConstructionMonths))
                .ToArray();
            ConstructionCostLookup = new IntIndexableLookup<Item, double>();
            foreach(var input in ConstructionCost)
            {
                ConstructionCostLookup[input.Item] += input.Quantity;
            }
        }

        protected virtual void LoadOperationElement(IGamedataTracker gamedataTracker, IXmlReaderUtil xmlReaderUtil, XElement operationElement, Race race)
        {
            OperationWorkers = xmlReaderUtil.LoadWorkerNeed(operationElement, race);

            var inventoryElement = xmlReaderUtil.ElementOrNull(operationElement, "inventory");

            if(inventoryElement != null)
            {
                InventoryCapacity = xmlReaderUtil.AttributeAsInt(inventoryElement, "capacity");
            }

            Recipies = operationElement.Elements("recipe")
                .Select(x => xmlReaderUtil.ObjectReferenceLookup(
                    x,
                    "id",
                    gamedataTracker.Recipies,
                    r => r.Id))
                .ToArray();

            UpkeepInputs = operationElement.Elements("input")
                .Select(x => xmlReaderUtil.LoadItemRate(x))
                .ToArray();
        }

        protected virtual void LoadLogiElement(IGamedataTracker gamedataTracker, IXmlReaderUtil xmlReaderUtil, XElement logiElement)
        {
            LogiDistance = xmlReaderUtil.AttributeAsDouble(logiElement, "distance");

            var source = xmlReaderUtil.ElementOrNull(logiElement, "source");
            if (source != null)
            {
                LogiSourceHaulers = source
                    .Elements("hauler")
                    .Select(e => xmlReaderUtil.ObjectReferenceLookup(
                        e,
                        "id",
                        gamedataTracker.HaulerInfos,
                        h => h.Id))
                    .ToArray();
            }
            else
            {
                LogiSourceHaulers = new HaulerInfo[0];
            }
        }

        public virtual void PostLink(IGamedataTracker gamedataTracker, IXmlReaderUtil xmlReaderUtil, XElement element)
        {
            var upgradesTo = new List<IStructureType>();

            foreach (var upgradeElement in element.Elements("upgrades"))
            {
                var toType = xmlReaderUtil.ObjectReferenceLookup(
                    upgradeElement,
                    "to",
                    gamedataTracker.StructureTypes,
                    cd => cd.Id);

                upgradesTo.Add(toType);
            }

            ManuallyUpgradesTo = upgradesTo.ToArray();

            var downgradeElement = xmlReaderUtil.ElementOrNull(element, "downgrades");
            if (downgradeElement != null)
            {
                ManuallyDowngradesTo = xmlReaderUtil.ObjectReferenceLookup(
                    downgradeElement,
                    "to",
                    gamedataTracker.StructureTypes,
                    cd => cd.Id);
            }
        }

        protected abstract IStructure CreateStructureInstance(Tile ulTile);

        public abstract IStructure DeserializeFrom(
            IGamedataTracker gamedataTracker,
            Settlement settlement,
            Cell cell,
            Tile[,] tiles,
            SerializedObject structureRoot);
    }
}
