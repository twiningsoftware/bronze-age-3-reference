﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Bronze.Common.Data.World.Structures;
using Bronze.Common.UI;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;

namespace Bronze.Common.Data.Game.Structures
{
    public class HousingStructureType : AbstractStructureType
    {
        private readonly IInjectionProvider _injectionProvider;

        public Caste SupportedCaste { get; private set; }
        public int SupportedPops { get; private set; }
        public double ProspertyPerPop { get; private set; }
        public IEnumerable<IPopNeed> MaintenanceNeeds { get; private set; }
        public IEnumerable<IPopNeed> UpgradeNeeds { get; private set; }
        public IEnumerable<IPopNeed> BonusNeeds { get; private set; }
        public HousingStructureType UpgradesTo { get; private set; }
        public double UpgradeTime { get; private set; }
        public HousingStructureType DowngradesTo { get; private set; }
        public double DowngradeTime { get; private set; }

        public HousingStructureType(IInjectionProvider injectionProvider)
        {
            _injectionProvider = injectionProvider;
        }

        protected override IStructure CreateStructureInstance(Tile ulTile)
        {
            return _injectionProvider.Build<HousingStructure>()
                .Initialize(ulTile, this);
        }
        
        public override IStackUiElement BuildTypeInfoPanel(int width, bool withName)
        {
            var stackGroup = base.BuildTypeInfoPanel(width, withName);

            stackGroup.Children.Add(new Spacer(0, 10));
            stackGroup.Children.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new IconText(SupportedCaste.IconKey, SupportedPops.ToString(), tooltip: $"Provides {SupportedPops} housing for {SupportedCaste.NamePlural}."),
                    new Spacer(10, 0),
                    new IconText(UiImages.SMALL_PROSPERITY, ProspertyPerPop.ToString(), tooltip: "Generated prosperity per resident.")
                }
            });
            stackGroup.Children.Add(new Spacer(0, 10));

            if (MaintenanceNeeds.Any())
            {
                stackGroup.Children.Add(new Label("Needs"));
                foreach (var popneed in MaintenanceNeeds)
                {
                    stackGroup.Children.Add(UiBuilder.BuildDisplayFor(popneed));
                }
                stackGroup.Children.Add(new Spacer(0, 10));
            }
            if (UpgradesTo != null)
            {
                stackGroup.Children.Add(new Label($"Upgrades To {UpgradesTo.Name} with"));
                foreach (var popneed in UpgradeNeeds)
                {
                    stackGroup.Children.Add(UiBuilder.BuildDisplayFor(popneed));
                }
                stackGroup.Children.Add(new Spacer(0, 10));
            }
            if(BonusNeeds.Any())
            {
                stackGroup.Children.Add(new Label("Luxuries"));
                foreach (var popneed in BonusNeeds)
                {
                    stackGroup.Children.Add(UiBuilder.BuildDisplayFor(popneed));
                }
                stackGroup.Children.Add(new Spacer(0, 10));
            }

            return stackGroup;
        }

        protected override void LoadFrom(
            IGamedataTracker gamedataTracker, 
            IXmlReaderUtil xmlReaderUtil, 
            XElement element, 
            Race race)
        {
            base.LoadFrom(gamedataTracker, xmlReaderUtil, element, race);

            var housingElement = xmlReaderUtil.Element(element, "housing");

            LoadHousingElement(gamedataTracker, xmlReaderUtil, housingElement, race);
        }

        protected virtual void LoadHousingElement(IGamedataTracker gamedataTracker, IXmlReaderUtil xmlReaderUtil, XElement housingElement, Race race)
        {
            MaintenanceNeeds = housingElement
                .Elements("maintenance_need")
                .Select(x => xmlReaderUtil.LoadPopNeed(x))
                .ToArray();

            UpgradeNeeds = housingElement
                .Elements("upgrade_need")
                .Select(x => xmlReaderUtil.LoadPopNeed(x))
                .ToArray();

            BonusNeeds = housingElement
                .Elements("bonus_need")
                .Select(x => xmlReaderUtil.LoadPopNeed(x))
                .ToArray();

            SupportedCaste = xmlReaderUtil.ObjectReferenceLookup(
                housingElement,
                "caste",
                race.Castes,
                c => c.Id);

            SupportedPops = xmlReaderUtil.AttributeAsInt(housingElement, "capacity");

            ProspertyPerPop = xmlReaderUtil.AttributeAsDouble(housingElement, "prosperity_per_pop");
        }

        public override void PostLink(
            IGamedataTracker gamedataTracker, 
            IXmlReaderUtil xmlReaderUtil, 
            XElement element)
        {
            base.PostLink(gamedataTracker, xmlReaderUtil, element);

            var housingElement = xmlReaderUtil.Element(element, "housing");

            var upgradeElement = xmlReaderUtil.ElementOrNull(housingElement, "upgrade");
            if(upgradeElement != null)
            {
                UpgradesTo = xmlReaderUtil.OptionalObjectReferenceLookup(
                    upgradeElement,
                    "to",
                    gamedataTracker.StructureTypes.OfType<HousingStructureType>(),
                    s => s.Id);

                UpgradeTime = xmlReaderUtil.AttributeAsDouble(upgradeElement, "time");
            }

            var downgradeElement = xmlReaderUtil.ElementOrNull(housingElement, "downgrade");
            if (downgradeElement != null)
            {
                DowngradesTo = xmlReaderUtil.OptionalObjectReferenceLookup(
                    downgradeElement,
                    "to",
                    gamedataTracker.StructureTypes.OfType<HousingStructureType>(),
                    s => s.Id);

                DowngradeTime = xmlReaderUtil.AttributeAsDouble(downgradeElement, "time");
            }
        }

        public override IStructure DeserializeFrom(
            IGamedataTracker gamedataTracker,
            Settlement settlement,
            Cell cell,
            Tile[,] tiles,
            SerializedObject structureRoot)
        {
            var ulTilePos = TilePosition.DeserializeFrom(
                cell,
                structureRoot.GetChild("ultile_position"));

            var ulTile = tiles[ulTilePos.X, ulTilePos.Y];

            var structure = _injectionProvider
                .Build<HousingStructure>()
                .Initialize(ulTile, this);

            structure.DeserializeFrom(gamedataTracker, settlement, structureRoot);

            return structure;
        }
    }
}