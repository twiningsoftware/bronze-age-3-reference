﻿using System.Xml.Linq;
using Bronze.Common.Data.World.Structures;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.Data.Game.Structures
{
    public class TaxCollectorStructureType : AbstractServiceProviderStructureType
    {
        private readonly IInjectionProvider _injectionProvider;

        public Item ProducedItem { get; private set; }
        
        public TaxCollectorStructureType(IInjectionProvider injectionProvider)
        {
            _injectionProvider = injectionProvider;
        }

        protected override void LoadServiceElement(
            IGamedataTracker gamedataTracker, 
            IXmlReaderUtil xmlReaderUtil, 
            XElement serviceElement, 
            Race race)
        {
            base.LoadServiceElement(gamedataTracker, xmlReaderUtil, serviceElement, race);

            ProducedItem = xmlReaderUtil.ObjectReferenceLookup(
                serviceElement,
                "produced_item",
                gamedataTracker.Items,
                i => i.Name);
        }

        protected override IStructure CreateStructureInstance(Tile ulTile)
        {
            return _injectionProvider.Build<TaxCollectorStructure>()
                .Initialize(ulTile, this);
        }
        
        public override IStructure DeserializeFrom(
            IGamedataTracker gamedataTracker,
            Settlement settlement,
            Cell cell,
            Tile[,] tiles, 
            SerializedObject structureRoot)
        {
            var ulTilePos = TilePosition.DeserializeFrom(
                cell,
                structureRoot.GetChild("ultile_position"));

            var ulTile = tiles[ulTilePos.X, ulTilePos.Y];

            var structure = _injectionProvider
                .Build<TaxCollectorStructure>()
                .Initialize(ulTile, this);

            structure.DeserializeFrom(gamedataTracker, settlement, structureRoot);

            return structure;
        }

        public override bool CanService(IEconomicActor economicActor)
        {
            return true;
        }

        public override bool CanService(IEconomicActorType economicActorType)
        {
            return true;
        }
    }
}
