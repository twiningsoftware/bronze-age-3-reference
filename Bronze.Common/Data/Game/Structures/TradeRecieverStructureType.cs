﻿using System.Linq;
using System.Xml.Linq;
using Bronze.Common.Data.World.Structures;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.Exceptions;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.Data.Game.Structures
{
    public class TradeRecieverStructureType : AbstractStructureType
    {
        private readonly IInjectionProvider _injectionProvider;
        
        public TradeRecieverStructureType(IInjectionProvider injectionProvider)
        {
            _injectionProvider = injectionProvider;
        }
        
        protected override IStructure CreateStructureInstance(Tile ulTile)
        {
            return _injectionProvider.Build<TradeRecieverStructure>()
                .Initialize(ulTile, this);
        }

        public override IStructure DeserializeFrom(
            IGamedataTracker gamedataTracker,
            Settlement settlement,
            Cell cell,
            Tile[,] tiles, 
            SerializedObject structureRoot)
        {
            var ulTilePos = TilePosition.DeserializeFrom(
                cell,
                structureRoot.GetChild("ultile_position"));

            var ulTile = tiles[ulTilePos.X, ulTilePos.Y];

            var structure = _injectionProvider
                .Build<TradeRecieverStructure>()
                .Initialize(ulTile, this);

            structure.DeserializeFrom(gamedataTracker, settlement, structureRoot);

            return structure;
        }

        protected override void LoadFrom(IGamedataTracker gamedataTracker, IXmlReaderUtil xmlReaderUtil, XElement element, Race race)
        {
            base.LoadFrom(gamedataTracker, xmlReaderUtil, element, race);

            if (!LogiSourceHaulers.Any())
            {
                throw DataLoadException.MiscError(element, "Trade Reciever has no logi source haulers, it will not be able to.");
            }
        }
    }
}
