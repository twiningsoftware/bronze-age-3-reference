﻿using Bronze.Common.Data.World.Structures;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.Data.Game.Structures
{
    public class SimpleStructureType : AbstractStructureType
    {
        private readonly IInjectionProvider _injectionProvider;

        public SimpleStructureType(IInjectionProvider injectionProvider)
        {
            _injectionProvider = injectionProvider;
        }
        
        protected override IStructure CreateStructureInstance(Tile ulTile)
        {
            return _injectionProvider.Build<SimpleStructure>()
                .Initialize(ulTile, this);
        }

        public override IStructure DeserializeFrom(
            IGamedataTracker gamedataTracker,
            Settlement settlement,
            Cell cell,
            Tile[,] tiles, 
            SerializedObject structureRoot)
        {
            var ulTilePos = TilePosition.DeserializeFrom(
                cell,
                structureRoot.GetChild("ultile_position"));

            var ulTile = tiles[ulTilePos.X, ulTilePos.Y];

            var structure = _injectionProvider
                .Build<SimpleStructure>()
                .Initialize(ulTile, this);

            structure.DeserializeFrom(gamedataTracker, settlement, structureRoot);

            return structure;
        }
    }
}
