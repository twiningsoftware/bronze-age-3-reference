﻿using Bronze.Common.Data.World.Structures;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System.Linq;
using System.Xml.Linq;

namespace Bronze.Common.Data.Game.Structures
{
    public class ItemStorageStructureType : AbstractStructureType, IItemStorageProviderType
    {
        private readonly IInjectionProvider _injectionProvider;
        
        public IntIndexableLookup<Item, bool> ItemsAllowed { get; private set; }
        
        public ItemStorageStructureType(IInjectionProvider injectionProvider)
        {
            _injectionProvider = injectionProvider;
        }

        protected override void LoadOperationElement(
            IGamedataTracker gamedataTracker, 
            IXmlReaderUtil xmlReaderUtil, 
            XElement operationElement, 
            Race race)
        {
            base.LoadOperationElement(gamedataTracker, xmlReaderUtil, operationElement, race);

            var storageElement = xmlReaderUtil.Element(operationElement, "storage");

            LoadStorageElement(
                gamedataTracker,
                xmlReaderUtil,
                storageElement,
                race);
        }

        protected virtual void LoadStorageElement(
            IGamedataTracker gamedataTracker,
            IXmlReaderUtil xmlReaderUtil,
            XElement storageElement,
            Race race)
        {
            var itemList = storageElement
                .Elements("item")
                .Select(e => xmlReaderUtil.ObjectReferenceLookup(
                    e,
                    "name",
                    gamedataTracker.Items,
                    i => i.Name))
                .ToArray();

            var isWhitelist = xmlReaderUtil.AttributeAsBool(storageElement, "is_whitelist");
            
            ItemsAllowed = new IntIndexableLookup<Item, bool>();
            foreach(var item in gamedataTracker.Items)
            {
                if (isWhitelist)
                {
                    ItemsAllowed[item] = itemList.Contains(item);
                }
                else
                {
                    ItemsAllowed[item] = !itemList.Contains(item);
                }
            }
        }
        
        protected override IStructure CreateStructureInstance(Tile ulTile)
        {
            return _injectionProvider.Build<ItemStorageStructure>()
                .Initialize(ulTile, this);
        }
        
        public override IStructure DeserializeFrom(
            IGamedataTracker gamedataTracker,
            Settlement settlement,
            Cell cell,
            Tile[,] tiles,
            SerializedObject structureRoot)
        {
            var ulTilePos = TilePosition.DeserializeFrom(
                cell,
                structureRoot.GetChild("ultile_position"));

            var ulTile = tiles[ulTilePos.X, ulTilePos.Y];

            var structure = _injectionProvider
                .Build<ItemStorageStructure>()
                .Initialize(ulTile, this);

            structure.DeserializeFrom(gamedataTracker, settlement, structureRoot);

            return structure;
        }
    }
}
