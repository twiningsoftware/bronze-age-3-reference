﻿using Bronze.Contracts.Data.AI;

namespace Bronze.Common.Data.Game.AI
{
    public class AbstractBuildingDecision : IBuildingDecision
    {
        public string Id { get; }
    }
}
