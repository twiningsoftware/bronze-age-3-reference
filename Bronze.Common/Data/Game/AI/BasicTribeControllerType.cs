﻿using Bronze.Common.Data.World.AI;
using Bronze.Contracts.Data.AI;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;

namespace Bronze.Common.Data.Game.AI
{
    public class BasicTribeControllerType : AbstractTribeControllerType
    {
        private readonly IInjectionProvider _injectionProvider;

        public BasicTribeControllerType(IInjectionProvider injectionProvider)
            : base(injectionProvider)
        {
            _injectionProvider = injectionProvider;
        }

        public override ITribeController BuildController(Tribe tribe, Random random, double worldHostility, double month)
        {
            var controller = _injectionProvider.Build<BasicTribeController>();

            controller.Seed(this, tribe, random, worldHostility, month);

            return controller;
        }

        public override ITribeController DeserializeFrom(IEnumerable<Region> regions, SerializedObject controllerRoot, Tribe tribe)
        {
            var controller = _injectionProvider
                .Build<BasicTribeController>();

            controller.DeserializeAndInit(this, tribe, regions, controllerRoot);

            return controller;
        }
    }
}
