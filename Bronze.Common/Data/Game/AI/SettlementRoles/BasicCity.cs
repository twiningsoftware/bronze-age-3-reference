﻿using Bronze.Common.Data.Game.CellDevelopments;
using Bronze.Common.Data.Game.Structures;
using Bronze.Common.Data.World.CellDevelopments;
using Bronze.Contracts;
using Bronze.Contracts.Data.AI;
using Bronze.Contracts.Data.Diplomacy;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Xml.Linq;

namespace Bronze.Common.Data.Game.AI.SettlementRoles
{
    public class BasicCity : ISettlementRole
    {
        private static readonly string RoleName = "BasicCity";

        private readonly IGamedataTracker _gamedataTracker;
        private readonly IXmlReaderUtil _xmlReaderUtil;
        private readonly ITradeManager _tradeManager;
        private readonly IAiSpy _aiSpy;
        private readonly IUnitHelper _unitHelper;
        private readonly ICityPrefabPlacer _cityPrefabPlacer;
        
        private Item[] _priorityItems;
        private Trait[] _traitsToClear;
        private Item[] _bonusItems;
        private Trait[] _traitsToPreserve;
        private Dictionary<Caste, int> _popTarget;
        private ICellDevelopmentType[] _pacificationTargets;

        private ItemImportTarget[] _itemImportTargets;

        public BasicCity(
            IGamedataTracker gamedataTracker,
            IXmlReaderUtil xmlReaderUtil,
            ITradeManager tradeManager,
            IAiSpy aiSpy,
            IUnitHelper unitHelper,
            ICityPrefabPlacer cityPrefabPlacer)
        {
            _gamedataTracker = gamedataTracker;
            _xmlReaderUtil = xmlReaderUtil;
            _tradeManager = tradeManager;
            _aiSpy = aiSpy;
            _unitHelper = unitHelper;
            _cityPrefabPlacer = cityPrefabPlacer;
        }

        public void Assign(Settlement settlement, SerializedObject newState)
        {
            newState.Set("role", RoleName);
        }

        public bool CanApplyTo(Settlement settlement)
        {
            return settlement.Districts.Any();
        }

        public bool IsFor(SerializedObject serializedObject)
        {
            return serializedObject.GetOptionalString("role") == RoleName;
        }

        public void Seed(Tribe tribe, SerializedObject tribeState, Settlement settlement, SerializedObject settlementState)
        {
        }

        public IEnumerable<Action> RunFor(
            Tribe tribe,
            Settlement settlement, 
            SerializedObject settlementState, 
            StrategicTarget tribeStrategicTarget)
        {
            var results = new List<Action>();

            // Haven't run the first calculation yet, skip processing
            if (settlement.EconomicActors.Length == 0)
            {
                return results;
            }

            var currentlyBuilding = settlement.EconomicActors
                .Where(e => e.UnderConstruction && e.WorkerSupply > 0)
                .Any();

            currentlyBuilding = currentlyBuilding || _cityPrefabPlacer.IsPendingPacement(settlement);

            var settlementCenter = GetSettlementCenter(settlement);


            var idealNetProduction = _gamedataTracker.Items.ToDictionary(i => i, i => 0.0);

            var availablePopByCaste = new Dictionary<Caste, int>();
            foreach(var caste in settlement.Race.Castes)
            {
                if(settlement.PopulationByCaste.ContainsKey(caste))
                {
                    availablePopByCaste[caste] = settlement.PopulationByCaste[caste] - settlement.JobsByCaste[caste];
                }
            }
            
            foreach (var actor in settlement.EconomicActors)
            {
                foreach (var itemRate in actor.ExpectedOutputs)
                {
                    var workerBonus = 1.0;
                    
                    if(actor.WorkerPercent > 0)
                    {
                        workerBonus = 1 / actor.WorkerPercent;
                    }

                    idealNetProduction[itemRate.Item] += itemRate.PerMonth * workerBonus;
                }
                foreach (var itemRate in actor.NeededInputs)
                {
                    idealNetProduction[itemRate.Item] -= itemRate.PerMonth;
                }

                if(actor.UnderConstruction && actor.ActiveRecipie != null)
                {
                    foreach(var itemRate in actor.ActiveRecipie.Inputs)
                    {
                        idealNetProduction[itemRate.Item] -= itemRate.PerMonth;
                    }
                    foreach (var itemRate in actor.ActiveRecipie.Outputs)
                    {
                        idealNetProduction[itemRate.Item] += itemRate.PerMonth;
                    }
                }
            }

            var tradeCapacity = settlement.EconomicActors
                .OfType<ITradeActor>()
                .GroupBy(ta => ta.TradeType)
                .ToDictionary(g => g.Key, g => g.Where(x => x.TradeRoute == null).Count());

            var idealRecruitmentSlots = _gamedataTracker.RecruitmentSlotCategories
                .ToDictionary(rs => rs, rs => 0);

            foreach(var provider in settlement.EconomicActors.OfType<IRecruitmentSlotProvider>())
            {
                idealRecruitmentSlots[provider.RecruitmentCategory] += provider.MaximumProvidedRecruitmentSlots;
            }
            
            using (var settlementSpy = _aiSpy.StartSettlement(RoleName, settlement, idealNetProduction, availablePopByCaste, tradeCapacity))
            {
                if (!currentlyBuilding)
                {
                    using (var choiceSpy = settlementSpy.StartChoice("SolveDeficits"))
                    {
                        currentlyBuilding = BuildToSolveDeficits(
                            settlement,
                            idealNetProduction,
                            availablePopByCaste,
                            settlementCenter,
                            choiceSpy,
                            results);
                    }
                }

                if (!currentlyBuilding)
                {
                    using (var choiceSpy = settlementSpy.StartChoice("Housing"))
                    {
                        currentlyBuilding = BuildForHousing(
                            settlement,
                            idealNetProduction,
                            availablePopByCaste,
                            tradeCapacity,
                            settlementCenter,
                            choiceSpy,
                            results);
                    }
                }

                if (!currentlyBuilding)
                {
                    using (var choiceSpy = settlementSpy.StartChoice("SolveDeficitsSettlement"))
                    {
                        currentlyBuilding = SolveDeficitsSettlement(
                            settlement,
                            idealNetProduction,
                            availablePopByCaste,
                            tradeCapacity,
                            settlementCenter,
                            choiceSpy,
                            results);
                    }
                }

                if (!currentlyBuilding)
                {
                    using (var choiceSpy = settlementSpy.StartChoice("Trade"))
                    {
                        currentlyBuilding = BuildForTrade(
                            settlement,
                            idealNetProduction,
                            availablePopByCaste,
                            tradeCapacity,
                            settlementCenter,
                            choiceSpy,
                            results);
                    }
                }

                if (!currentlyBuilding)
                {
                    using (var choiceSpy = settlementSpy.StartChoice("TradeReciever"))
                    {
                        currentlyBuilding = BuildForTradeReciever(
                            settlement,
                            idealNetProduction,
                            availablePopByCaste,
                            tradeCapacity,
                            settlementCenter,
                            choiceSpy,
                            results);
                    }
                }

                if (!currentlyBuilding)
                {
                    using (var choiceSpy = settlementSpy.StartChoice("BonusProduction"))
                    {
                        currentlyBuilding = BuildForBonusProduction(
                            settlement,
                            idealNetProduction,
                            availablePopByCaste,
                            settlementCenter,
                            choiceSpy,
                            results);
                    }
                }

                if (!currentlyBuilding)
                {
                    using (var choiceSpy = settlementSpy.StartChoice("Barracks"))
                    {
                        currentlyBuilding = BuildForBarracks(
                            settlement,
                            idealNetProduction,
                            availablePopByCaste,
                            settlementCenter,
                            choiceSpy,
                            results,
                            idealRecruitmentSlots);
                    }
                }

                if (!currentlyBuilding)
                {
                    using (var choiceSpy = settlementSpy.StartChoice("BarracksSettlement"))
                    {
                        currentlyBuilding = BuildForBarracksSettlement(
                            settlement,
                            idealNetProduction,
                            availablePopByCaste,
                            settlementCenter,
                            choiceSpy,
                            results,
                            idealRecruitmentSlots);
                    }
                }
            }

            if(settlement.AuthorityFraction < 0.8 && settlement.Governor == null)
            {
                results.Add(() =>
                {
                    var governor = settlement.Owner.NotablePeople
                        .Where(np => np.CurrentAssignment == string.Empty && np.IsMature && !np.IsDead)
                        .OrderByDescending(np => np.GetAuthority())
                        .FirstOrDefault();

                    if(governor != null)
                    {
                        governor.AssignTo(settlement);
                    }
                });
            }

            foreach (var camp in settlement.EconomicActors.OfType<CreepCampDevelopment>())
            {
                if (!camp.IsBeingPacified
                    && _pacificationTargets.Any(dt => dt.Id == camp.TypeId))
                {
                    var pacificationOption = camp.PacificationOptions
                        .Where(po => po.Inputs.All(ir => settlement.ItemAvailability[ir.Item]))
                        .OrderByDescending(po => po.Inputs.Sum(ir => idealNetProduction[ir.Item]))
                        .FirstOrDefault();

                    if (pacificationOption != null)
                    {
                        camp.SelectedPacificationOption = pacificationOption;
                    }
                }
            }

            return results;
        }
        
        private bool BuildToSolveDeficits(
            Settlement settlement,
            Dictionary<Item, double> idealNetProduction,
            Dictionary<Caste, int> availablePopByCaste,
            Vector2 settlementCenter,
            IChoiceAiSpy choiceSpy,
            List<Action> results)
        {
            foreach (var item in idealNetProduction.Keys.OrderByDescending(i => _priorityItems.Contains(i)))
            {
                // Produce to fill needs
                if (idealNetProduction[item] < 0)
                {
                    var developmentOptions = settlement.Race.AvailableCellDevelopments
                        .Where(d => !d.UpgradeOnly)
                        .Where(d => availablePopByCaste[d.ConstructionWorkers.Caste] >= d.ConstructionWorkers.Need)
                        .Select(d => Tuple.Create(d, d.Recipies.FirstOrDefault(r => r.Outputs.Any(o => o.Item == item))))
                        .Where(t => t.Item2 != null)
                        .ToArray();

                    choiceSpy.Options(item.Name, developmentOptions.Select(d => d.Item1));

                    var constructionOptions = developmentOptions
                        .Select(t => Tuple.Create(
                            t.Item1,
                            t.Item2,
                            settlement.Region.Cells
                               .Where(c => t.Item1.CanBePlaced(settlement.Owner, c))
                               .Where(c => c.Traits.ContainsAll(t.Item2.RequiredTraits))
                               .Where(c => !c.Traits.ContainsAny(_traitsToPreserve.Except(t.Item2.RequiredTraits)))
                               .OrderByDescending(c => c.Traits.Intersect(t.Item2.BoostingTraits).Count())
                               .ThenBy(c => (c.Position.ToVector() - settlementCenter).Length())
                               .FirstOrDefault()))
                        .Where(t => t.Item3 != null)
                        .ToArray();

                    if (constructionOptions.Any())
                    {
                        var constructionChoice = constructionOptions.First();

                        results.Add(() =>
                        {
                            constructionChoice.Item1.Place(settlement.Owner, constructionChoice.Item3, false);

                            constructionChoice.Item3.Development.ActiveRecipie = constructionChoice.Item2;
                        });

                        choiceSpy.Pick(constructionChoice.Item1, constructionChoice.Item2);

                        return true;
                    }
                    else if (developmentOptions.Any())
                    {
                        // have a development to place, but no place to put it, try clearing land

                        var placedClearance = BuildForClearance(
                            settlement,
                            settlementCenter,
                            choiceSpy,
                            availablePopByCaste,
                            developmentOptions,
                            results);

                        if (placedClearance)
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        private bool BuildForBonusProduction(
            Settlement settlement,
            Dictionary<Item, double> idealNetProduction,
            Dictionary<Caste, int> availablePopByCaste,
            Vector2 settlementCenter,
            IChoiceAiSpy choiceSpy,
            List<Action> results)
        {
            foreach (var item in idealNetProduction.Keys.Where(i => idealNetProduction[i] < 1).OrderByDescending(i => _bonusItems.Contains(i)))
            {
                var developmentOptions = settlement.Race.AvailableCellDevelopments
                    .Where(d => !d.UpgradeOnly)
                    .Where(d => availablePopByCaste[d.ConstructionWorkers.Caste] >= d.ConstructionWorkers.Need)
                    .Select(d => Tuple.Create(d, d.Recipies.FirstOrDefault(r => r.Outputs.Any(o => o.Item == item))))
                    .Where(t => t.Item2 != null)
                    .ToArray();

                choiceSpy.Options(item.Name, developmentOptions.Select(t => t.Item1));

                var constructionOptions = developmentOptions
                    .Select(t => Tuple.Create(
                        t.Item1,
                        t.Item2,
                        settlement.Region.Cells
                            .Where(c => t.Item1.CanBePlaced(settlement.Owner, c))
                            .Where(c => c.Traits.ContainsAll(t.Item2.RequiredTraits))
                            .Where(c => t.Item1.ConstructionCost.All(i => settlement.ItemAvailability[i.Item]))
                            .Where(c => !c.Traits.ContainsAny(_traitsToPreserve.Except(t.Item2.RequiredTraits)))
                            .OrderByDescending(c => c.Traits.Intersect(t.Item2.BoostingTraits).Count())
                            .ThenBy(c => (c.Position.ToVector() - settlementCenter).Length())
                            .FirstOrDefault()))
                    .Where(t => t.Item3 != null)
                    .ToArray();

                if (constructionOptions.Any())
                {
                    var constructionChoice = constructionOptions.First();

                    results.Add(() =>
                    {
                        constructionChoice.Item1.Place(settlement.Owner, constructionChoice.Item3, false);

                        constructionChoice.Item3.Development.ActiveRecipie = constructionChoice.Item2;
                    });

                    choiceSpy.Pick(constructionChoice.Item1, constructionChoice.Item2);

                    return true;
                }
                else if (developmentOptions.Any())
                {
                    var placedClearance = BuildForClearance(
                        settlement,
                        settlementCenter,
                        choiceSpy,
                        availablePopByCaste,
                        developmentOptions,
                        results);

                    if (placedClearance)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private bool BuildForBarracks(
            Settlement settlement,
            Dictionary<Item, double> idealNetProduction,
            Dictionary<Caste, int> availablePopByCaste,
            Vector2 settlementCenter,
            IChoiceAiSpy choiceSpy,
            List<Action> results,
            Dictionary<RecruitmentSlotCategory, int> idealRecruitmentSlots)
        {
            var targetSlots = 2;
            if (settlement.Owner.Controller.IsAggressive)
            {
                targetSlots = 4;
            }

            var recruitmentCategories = _gamedataTracker.RecruitmentSlotCategories;

            choiceSpy.Log("Max Recruits Slots: " + string.Join(", ", recruitmentCategories.Select(c => $"{c.Name}: {idealRecruitmentSlots[c]}")));
            choiceSpy.Log($"Target Slots: {targetSlots}");



            var developmentOptions = settlement.Race.AvailableCellDevelopments
                .Where(d => !d.UpgradeOnly)
                .Where(d => availablePopByCaste[d.ConstructionWorkers.Caste] >= d.ConstructionWorkers.Need)
                .Where(d => d is IRecruitmentSlotProviderType rspt && idealRecruitmentSlots[rspt.RecruitmentCategory] < targetSlots)
                .ToArray();

            choiceSpy.Options(null, developmentOptions);

            var constructionOptions = developmentOptions
                .Select(d => Tuple.Create(
                    d,
                    settlement.Region.Cells
                        .Where(c => d.CanBePlaced(settlement.Owner, c))
                        .Where(c => d.ConstructionCost.All(i => settlement.ItemAvailability[i.Item]))
                        .Where(c => !c.Traits.ContainsAny(_traitsToPreserve))
                        .OrderBy(c => (c.Position.ToVector() - settlementCenter).Length())
                        .FirstOrDefault()))
                .Where(t => t.Item2 != null)
                .ToArray();

            if (constructionOptions.Any())
            {
                var constructionChoice = constructionOptions.First();

                results.Add(() =>
                {
                    constructionChoice.Item1.Place(settlement.Owner, constructionChoice.Item2, false);
                });

                choiceSpy.Pick(constructionChoice.Item1);

                return true;
            }
            else if (developmentOptions.Any())
            {
                var placedClearance = BuildForClearance(
                    settlement,
                    settlementCenter,
                    choiceSpy,
                    availablePopByCaste,
                    developmentOptions,
                    results);

                if (placedClearance)
                {
                    return true;
                }
            }

            return false;
        }

        private bool BuildForTrade(
            Settlement settlement,
            Dictionary<Item, double> idealNetProduction,
            Dictionary<Caste, int> availablePopByCaste,
            Dictionary<MovementType, int> tradeCapacity,
            Vector2 settlementCenter,
            IChoiceAiSpy choiceSpy,
            List<Action> results)
        {
            var tradeDevelopmentOptions = settlement.Race.AvailableCellDevelopments
                .Where(d => !d.UpgradeOnly)
                .OfType<ITradeActorType>()
                .Where(t => !tradeCapacity.ContainsKey(t.TradeType) || tradeCapacity[t.TradeType] < 1)
                .OfType<ICellDevelopmentType>()
                .ToArray();

            choiceSpy.Options(null, tradeDevelopmentOptions);

            var constructionOptions = tradeDevelopmentOptions
                .Select(t => Tuple.Create(
                        t,
                        settlement.Region.Cells
                            .Where(c => t.CanBePlaced(settlement.Owner, c))
                            .Where(c => t.ConstructionCost.All(i => settlement.ItemAvailability[i.Item]))
                            .OrderBy(c => c.Traits.Intersect(_traitsToPreserve).Count())
                            .Where(c => !c.Traits.ContainsAny(_traitsToPreserve))
                            .OrderBy(c => (c.Position.ToVector() - settlementCenter).Length())
                            .FirstOrDefault()))
                .Where(t => t.Item2 != null)
                .ToArray();

            if (constructionOptions.Any())
            {
                var constructionChoice = constructionOptions.First();

                results.Add(() =>
                {
                    constructionChoice.Item1.Place(settlement.Owner, constructionChoice.Item2, false);
                });

                choiceSpy.Pick(constructionChoice.Item1);

                return true;
            }
            else if (tradeDevelopmentOptions.Any())
            {
                var placedClearance = BuildForClearance(
                    settlement,
                    settlementCenter,
                    choiceSpy,
                    availablePopByCaste,
                    tradeDevelopmentOptions,
                    results);

                if (placedClearance)
                {
                    return true;
                }
            }

            return false;
        }

        private bool BuildForTradeReciever(
            Settlement settlement,
            Dictionary<Item, double> idealNetProduction,
            Dictionary<Caste, int> availablePopByCaste,
            Dictionary<MovementType, int> tradeCapacity,
            Vector2 settlementCenter,
            IChoiceAiSpy choiceSpy,
            List<Action> results)
        {
            if(settlement.EconomicActors.OfType<ITradeReciever>().Any())
            {
                return false;
            }

            var tradeRecieverDevelopmentOptions = settlement.Race.AvailableCellDevelopments
                .Where(d => !d.UpgradeOnly)
                .OfType<ITradeRecieverType>()
                .OfType<ICellDevelopmentType>()
                .ToArray();

            choiceSpy.Options(null, tradeRecieverDevelopmentOptions);

            var constructionOptions = tradeRecieverDevelopmentOptions
                .Select(t => Tuple.Create(
                        t,
                        settlement.Region.Cells
                            .Where(c => t.CanBePlaced(settlement.Owner, c))
                            .Where(c => t.ConstructionCost.All(i => settlement.ItemAvailability[i.Item]))
                            .OrderBy(c => c.Traits.Intersect(_traitsToPreserve).Count())
                            .Where(c => !c.Traits.ContainsAny(_traitsToPreserve))
                            .OrderBy(c => (c.Position.ToVector() - settlementCenter).Length())
                            .FirstOrDefault()))
                .Where(t => t.Item2 != null)
                .ToArray();

            if (constructionOptions.Any())
            {
                var constructionChoice = constructionOptions.First();

                results.Add(() =>
                {
                    constructionChoice.Item1.Place(settlement.Owner, constructionChoice.Item2, false);
                });

                choiceSpy.Pick(constructionChoice.Item1);

                return true;
            }
            else if (tradeRecieverDevelopmentOptions.Any())
            {
                var placedClearance = BuildForClearance(
                    settlement,
                    settlementCenter,
                    choiceSpy,
                    availablePopByCaste,
                    tradeRecieverDevelopmentOptions,
                    results);

                if (placedClearance)
                {
                    return true;
                }
            }

            return false;
        }

        private bool BuildForClearance(
            Settlement settlement,
            Vector2 settlementCenter,
            IChoiceAiSpy choiceSpy,
            Dictionary<Caste, int> availablePopByCaste,
            Tuple<ICellDevelopmentType, Recipie>[] cellDevelopmentsToTryToPlace,
            List<Action> results)
        {
            var placementPossibilities = cellDevelopmentsToTryToPlace
                .Select(t => Tuple.Create(
                    t.Item1,
                    t.Item2,
                    settlement.Region.Cells
                        .Where(c => c.Feature != null)
                        .Where(c => c.Feature.Traits.ContainsAny(_traitsToClear))
                        .Where(c => t.Item1.CouldBePlacedWithoutFeature(settlement.Owner, c))
                        .Where(c => c.Traits.ContainsAll(t.Item2.RequiredTraits))
                        .Where(c => t.Item1.ConstructionCost.All(i => settlement.ItemAvailability[i.Item]))
                        .Where(c => !c.Traits.ContainsAny(_traitsToPreserve))
                        .OrderByDescending(c => c.Traits.Intersect(t.Item2.BoostingTraits).Count())
                        .ThenBy(c => (c.Position.ToVector() - settlementCenter).Length())
                        .FirstOrDefault()))
                .Where(t => t.Item3 != null)
                .ToArray();

            var clearanceOption = placementPossibilities
                .Select(t => Tuple.Create(
                    t.Item3,
                    t.Item1,
                    settlement.Race.AvailableCellDevelopments
                            .OfType<ClearFeatureDevelopmentType>()
                            .Where(d => availablePopByCaste[d.ConstructionWorkers.Caste] >= d.ConstructionWorkers.Need)
                            .Where(d => d.CanBePlaced(settlement.Owner, t.Item3))
                            .FirstOrDefault()))
                .Where(t => t.Item3 != null)
                .FirstOrDefault();


            if (clearanceOption != null)
            {
                results.Add(() =>
                {
                    clearanceOption.Item3.Place(settlement.Owner, clearanceOption.Item1, false);
                });

                choiceSpy.PickForClearing(clearanceOption.Item3, clearanceOption.Item2);

                return true;
            }

            return false;
        }

        private bool BuildForClearance(
            Settlement settlement,
            Vector2 settlementCenter,
            IChoiceAiSpy choiceSpy,
            Dictionary<Caste, int> availablePopByCaste,
            ICellDevelopmentType[] cellDevelopmentsToTryToPlace,
            List<Action> results)
        {
            var placementPossibilities = cellDevelopmentsToTryToPlace
                .Select(d => Tuple.Create(
                    d,
                    settlement.Region.Cells
                        .Where(c => c.Feature != null)
                        .Where(c => c.Feature.Traits.ContainsAny(_traitsToClear))
                        .Where(c => d.CouldBePlacedWithoutFeature(settlement.Owner, c))
                        .Where(c => d.ConstructionCost.All(i => settlement.ItemAvailability[i.Item]))
                        .Where(c => !c.Traits.ContainsAny(_traitsToPreserve))
                        .OrderBy(c => (c.Position.ToVector() - settlementCenter).Length())
                        .FirstOrDefault()))
                .Where(t => t.Item2 != null)
                .ToArray();

            var clearanceOption = placementPossibilities
                .Select(t => Tuple.Create(
                    t.Item2,
                    t.Item1,
                    settlement.Race.AvailableCellDevelopments
                            .OfType<ClearFeatureDevelopmentType>()
                            .Where(d => availablePopByCaste[d.ConstructionWorkers.Caste] >= d.ConstructionWorkers.Need)
                            .Where(d => d.CanBePlaced(settlement.Owner, t.Item2))
                            .FirstOrDefault()))
                .FirstOrDefault();


            if (clearanceOption != null)
            {
                results.Add(() =>
                {
                    clearanceOption.Item3.Place(settlement.Owner, clearanceOption.Item1, false);
                });

                choiceSpy.PickForClearing(clearanceOption.Item3, clearanceOption.Item2);

                return true;
            }

            return false;
        }


        private bool BuildForHousing(
            Settlement settlement,
            Dictionary<Item, double> idealNetProduction,
            Dictionary<Caste, int> availablePopByCaste,
            Dictionary<MovementType, int> tradeCapacity,
            Vector2 settlementCenter,
            IChoiceAiSpy choiceSpy,
            List<Action> results)
        {
            var castesNeedingHousing = settlement.HousingByCaste.Keys
                .Where(c => settlement.PopulationByCaste[c] > 1
                    && settlement.HousingByCaste[c] <= settlement.PopulationByCaste[c]
                    && ((availablePopByCaste[c] < 2 && settlement.HousingByCaste[c] <= settlement.PopulationByCaste[c] + 2)
                        || (_popTarget.ContainsKey(c) && settlement.PopulationByCaste[c] < _popTarget[c])))
                .ToArray();

            choiceSpy.Log($"Castes needing housing: {string.Join(", ", castesNeedingHousing.Select(c => c.Id))}");

            foreach (var caste in castesNeedingHousing)
            {
                var prefabOptions = settlement.Race.CityPrefabs
                    .Where(p => !p.IsCityStarter)
                    .Where(p => !p.RequiredItemAccess.Any() || p.RequiredItemAccess.All(i => settlement.ItemAvailability[i]))
                    .Where(p => p.StructurePlacements.Any(sp => sp.Structure is HousingStructureType hst && hst.SupportedCaste == caste))
                    .OrderByDescending(p => p.StructurePlacements
                        .Select(sp => sp.Structure)
                        .OfType<HousingStructureType>()
                        .Where(s => s.SupportedCaste == caste)
                        .Sum(s => s.SupportedPops))
                    .ToArray();

                choiceSpy.Options(null, prefabOptions);

                if (prefabOptions.Any())
                {
                    _cityPrefabPlacer.QueueForPlacement(
                        settlement, 
                        prefabOptions, 
                        false,
                        settlement.Race.CityPrefabs
                            .Where(p => !p.IsCityStarter)
                            .Where(p => p.StructurePlacements.Any(sp => sp.Structure is IItemStorageProviderType))
                            .ToArray());

                    choiceSpy.Pick(prefabOptions);

                    return true;
                }
            }

            return false;
        }
        
        private bool SolveDeficitsSettlement(
            Settlement settlement,
            Dictionary<Item, double> idealNetProduction,
            Dictionary<Caste, int> availablePopByCaste,
            Dictionary<MovementType, int> tradeCapacity,
            Vector2 settlementCenter,
            IChoiceAiSpy choiceSpy,
            List<Action> results)
        {
            foreach (var item in idealNetProduction.Keys.OrderByDescending(i => _priorityItems.Contains(i)))
            {
                // Produce to fill needs
                if (idealNetProduction[item] < 0)
                {
                    var prefabOptions = settlement.Race.CityPrefabs
                        .Where(p => !p.IsCityStarter)
                        .Where(p => !p.RequiredItemAccess.Any() || p.RequiredItemAccess.All(i => settlement.ItemAvailability[i]))
                        .Select(p => Tuple.Create(p, p.StructurePlacements
                            .Where(sp => sp.Recipie != null && sp.Recipie.Outputs.Any(o => o.Item == item))
                            .Select(sp => sp.Recipie)
                            .ToArray()))
                        .Where(t => t.Item2.Any())
                        .OrderBy(o => o.Item2.Sum(r => r.Outputs.Where(ir => ir.Item == item).Sum(ir => ir.PerMonth)))
                        .Select(t => t.Item1)
                        .ToArray();
                    
                    choiceSpy.Options(item.Name, prefabOptions);
                    
                    if (prefabOptions.Any())
                    {
                        choiceSpy.Pick(prefabOptions);

                        _cityPrefabPlacer.QueueForPlacement(
                            settlement, 
                            prefabOptions, 
                            false,
                            settlement.Race.CityPrefabs
                                .Where(p => !p.IsCityStarter)
                                .Where(p => p.StructurePlacements.Any(sp => sp.Structure is IItemStorageProviderType))
                                .ToArray());

                        return true;
                    }
                }
            }

            return false;
        }

        private bool BuildForBarracksSettlement(
            Settlement settlement,
            Dictionary<Item, double> idealNetProduction,
            Dictionary<Caste, int> availablePopByCaste,
            Vector2 settlementCenter,
            IChoiceAiSpy choiceSpy,
            List<Action> results,
            Dictionary<RecruitmentSlotCategory, int> idealRecruitmentSlots)
        {
            var targetSlots = 2;
            if (settlement.Owner.Controller.IsAggressive)
            {
                targetSlots = 4;
            }

            var recruitmentCategories = _gamedataTracker.RecruitmentSlotCategories;

            choiceSpy.Log("Max Recruits Slots: " + string.Join(", ", recruitmentCategories.Select(c => $"{c.Name}: {idealRecruitmentSlots[c]}")));
            choiceSpy.Log($"Target Slots: {targetSlots}");

            var prefabOptions = settlement.Race.CityPrefabs
                .Where(p => !p.IsCityStarter)
                .Where(p => !p.RequiredItemAccess.Any() || p.RequiredItemAccess.All(i => settlement.ItemAvailability[i]))
                .Where(p=>p.StructurePlacements.Any(sp=>sp.Structure is IRecruitmentSlotProviderType rspt && idealRecruitmentSlots[rspt.RecruitmentCategory] < targetSlots))
                .ToArray();
            
            if (prefabOptions.Any())
            {
                choiceSpy.Pick(prefabOptions);

                _cityPrefabPlacer.QueueForPlacement(
                    settlement, 
                    prefabOptions, 
                    false,
                    settlement.Race.CityPrefabs
                        .Where(p => !p.IsCityStarter)
                        .Where(p => p.StructurePlacements.Any(sp => sp.Structure is IItemStorageProviderType))
                        .ToArray());

                return true;
            }
            
            return false;
        }



        private Vector2 GetSettlementCenter(Settlement settlement)
        {
            var center = settlement.Region.BoundingBox.Center.ToVector2();

            var cityCenter = settlement.EconomicActors
                .OfType<ICellDevelopment>()
                .Where(a => a is VillageDevelopment || a is DistrictDevelopment)
                .FirstOrDefault();

            if (cityCenter != null)
            {
                center = cityCenter.Cell.Position.ToVector();
            }

            return center; 
        }

        public void LoadFrom(XElement element)
        {
            _priorityItems = element
                .Elements("production_priority")
                .Select(e => _xmlReaderUtil.ObjectReferenceLookup(
                    e,
                    "item",
                    _gamedataTracker.Items,
                    i => i.Name))
                .ToArray();

            _bonusItems = element
                .Elements("production_bonus")
                .Select(e => _xmlReaderUtil.ObjectReferenceLookup(
                    e,
                    "item",
                    _gamedataTracker.Items,
                    i => i.Name))
                .ToArray();

            _traitsToClear = element
                .Elements("land_clearing")
                .Select(e => _xmlReaderUtil.ObjectReferenceLookup(e, "trait", _gamedataTracker.Traits, t => t.Id))
                .ToArray();

            _traitsToPreserve = element
                .Elements("preserve")
                .Select(e => _xmlReaderUtil.ObjectReferenceLookup(e, "trait", _gamedataTracker.Traits, t => t.Id))
                .ToArray();

            _popTarget = element
                .Elements("pop_target")
                .ToDictionary(
                    e => _xmlReaderUtil.ObjectReferenceLookup(e, "caste", _gamedataTracker.Races.SelectMany(r => r.Castes), c => c.Id),
                    e => _xmlReaderUtil.AttributeAsInt(e, "count"));

            _pacificationTargets = element
                .Elements("pacification_target")
                .Select(e => _xmlReaderUtil.ObjectReferenceLookup(
                    e,
                    "development_id",
                    _gamedataTracker.CellDevelopments,
                    d => d.Id))
                .ToArray();

            _itemImportTargets = element
                .Elements("import_item")
                .Select(e => new ItemImportTarget
                {
                    Item = _xmlReaderUtil.ObjectReferenceLookup(
                        e,
                        "item",
                        _gamedataTracker.Items,
                        i => i.Name),
                    TargetSurplus = _xmlReaderUtil.AttributeAsInt(e, "target_surplus"),
                    Value = _xmlReaderUtil.AttributeAsInt(e, "value")
                })
                .ToArray();
        }

        public IEnumerable<ItemImportTarget> GetItemImportTargets()
        {
            return _itemImportTargets;
        }
    }
}
