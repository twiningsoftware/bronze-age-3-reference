﻿using Bronze.Common.Data.Game.CellDevelopments;
using Bronze.Common.Data.World.CellDevelopments;
using Bronze.Contracts;
using Bronze.Contracts.Data.AI;
using Bronze.Contracts.Data.Diplomacy;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Xml.Linq;

namespace Bronze.Common.Data.Game.AI.SettlementRoles
{
    public class SubsistanceFarming : ISettlementRole
    {
        private static readonly string RoleName = "SubsistanceFarming";

        private readonly IGamedataTracker _gamedataTracker;
        private readonly IXmlReaderUtil _xmlReaderUtil;
        private readonly ITradeManager _tradeManager;
        private readonly IAiSpy _aiSpy;
        private readonly IUnitHelper _unitHelper;
        
        private Item[] _priorityItems;
        private Trait[] _traitsToClear;
        private Item[] _bonusItems;
        private Trait[] _traitsToPreserve;
        private ICellDevelopmentType[] _pacificationTargets;
        private ItemImportTarget[] _itemImportTargets;
        private Cult[] _targetCults;

        public SubsistanceFarming(
            IGamedataTracker gamedataTracker,
            IXmlReaderUtil xmlReaderUtil,
            ITradeManager tradeManager,
            IAiSpy aiSpy,
            IUnitHelper unitHelper)
        {
            _gamedataTracker = gamedataTracker;
            _xmlReaderUtil = xmlReaderUtil;
            _tradeManager = tradeManager;
            _aiSpy = aiSpy;
            _unitHelper = unitHelper;
        }

        public void Assign(Settlement settlement, SerializedObject newState)
        {
            newState.Set("role", RoleName);
        }

        public bool CanApplyTo(Settlement settlement)
        {
            return true;
        }

        public bool IsFor(SerializedObject serializedObject)
        {
            return serializedObject.GetOptionalString("role") == RoleName;
        }

        public void Seed(Tribe tribe, SerializedObject tribeState, Settlement settlement, SerializedObject settlementState)
        {
        }

        public IEnumerable<Action> RunFor(
            Tribe tribe,
            Settlement settlement, 
            SerializedObject settlementState, 
            StrategicTarget tribeStrategicTarget)
        {
            var results = new List<Action>();

            // Haven't run the first calculation yet, skip processing
            if (settlement.EconomicActors.Length == 0)
            {
                return results;
            }

            var currentlyBuilding = settlement.EconomicActors
                .Where(e => e.UnderConstruction)
                .Any();

            var settlementCenter = GetSettlementCenter(settlement);


            var idealNetProduction = _gamedataTracker.Items.ToDictionary(i => i, i => 0.0);

            var availablePopByCaste = new Dictionary<Caste, int>();
            foreach(var caste in settlement.Race.Castes)
            {
                if(settlement.PopulationByCaste.ContainsKey(caste))
                {
                    availablePopByCaste[caste] = settlement.PopulationByCaste[caste] - settlement.JobsByCaste[caste];
                }
            }
            
            foreach (var actor in settlement.EconomicActors)
            {
                foreach (var itemRate in actor.ExpectedOutputs)
                {
                    idealNetProduction[itemRate.Item] += itemRate.PerMonth;
                }
                foreach (var itemRate in actor.NeededInputs)
                {
                    idealNetProduction[itemRate.Item] -= itemRate.PerMonth;
                }
            }

            var tradeCapacity = settlement.EconomicActors
                .OfType<ITradeActor>()
                .GroupBy(ta => ta.TradeType)
                .ToDictionary(g => g.Key, g => g.Where(x => x.TradeRoute == null).Count());

            using (var settlementSpy = _aiSpy.StartSettlement(RoleName, settlement, idealNetProduction, availablePopByCaste, tradeCapacity))
            {
                
                if (!currentlyBuilding)
                {
                    if (!currentlyBuilding)
                    {
                        using (var choiceSpy = settlementSpy.StartChoice("VillageUpgrade"))
                        {
                            currentlyBuilding = BuildToUpgradeVillage(
                                settlement,
                                idealNetProduction,
                                availablePopByCaste,
                                settlementCenter,
                                choiceSpy,
                                results);
                        }
                    }

                    if (!currentlyBuilding)
                    {
                        using (var choiceSpy = settlementSpy.StartChoice("SolveDeficits"))
                        {
                            currentlyBuilding = BuildToSolveDeficits(
                                settlement,
                                idealNetProduction,
                                availablePopByCaste,
                                settlementCenter,
                                choiceSpy,
                                results);
                        }
                    }

                    if (!currentlyBuilding)
                    {
                        using (var choiceSpy = settlementSpy.StartChoice("Trade"))
                        {
                            currentlyBuilding = BuildForTrade(
                                settlement,
                                idealNetProduction,
                                availablePopByCaste,
                                tradeCapacity,
                                settlementCenter,
                                choiceSpy,
                                results);
                        }
                    }

                    if (!currentlyBuilding)
                    {
                        using (var choiceSpy = settlementSpy.StartChoice("BonusProduction"))
                        {
                            currentlyBuilding = BuildForBonusProduction(
                                settlement,
                                idealNetProduction,
                                availablePopByCaste,
                                settlementCenter,
                                choiceSpy,
                                results);
                        }
                    }

                    if (!currentlyBuilding)
                    {
                        using (var choiceSpy = settlementSpy.StartChoice("Barracks"))
                        {
                            currentlyBuilding = BuildForBarracks(
                                settlement,
                                idealNetProduction,
                                availablePopByCaste,
                                settlementCenter,
                                choiceSpy,
                                results);
                        }
                    }

                    if (!currentlyBuilding)
                    {
                        using (var choiceSpy = settlementSpy.StartChoice("Shrine"))
                        {
                            currentlyBuilding = BuildForShrine(
                                settlement,
                                idealNetProduction,
                                availablePopByCaste,
                                settlementCenter,
                                choiceSpy,
                                results);
                        }
                    }
                }
            }

            foreach(var camp in settlement.EconomicActors.OfType<CreepCampDevelopment>())
            {
                if (!camp.IsBeingPacified
                    && _pacificationTargets.Any(dt => dt.Id == camp.TypeId))
                {
                    var pacificationOption = camp.PacificationOptions
                        .Where(po => po.Inputs.All(ir => settlement.ItemAvailability[ir.Item]))
                        .OrderByDescending(po => po.Inputs.Sum(ir => idealNetProduction[ir.Item]))
                        .FirstOrDefault();

                    if (pacificationOption != null)
                    {
                        camp.SelectedPacificationOption = pacificationOption;
                    }
                }
            }
            
            return results;
        }

        private bool BuildToUpgradeVillage(
            Settlement settlement,
            Dictionary<Item, double> idealNetProduction,
            Dictionary<Caste, int> availablePopByCaste,
            Vector2 settlementCenter,
            IChoiceAiSpy choiceSpy,
            List<Action> results)
        {
            var village = settlement.EconomicActors
                .OfType<VillageDevelopment>()
                .FirstOrDefault();

            if(village != null && !village.IsUpgrading)
            {
                var developmentOptions = village.ManuallyUpgradesTo
                    .Where(d => d.ConstructionCost.All(iq => settlement.ItemAvailability[iq.Item]))
                    .Where(d => availablePopByCaste[d.ConstructionWorkers.Caste] >= d.ConstructionWorkers.Need)
                    .Where(d => d is VillageDevelopmentType)
                    .ToArray();

                choiceSpy.Options("Upgrade", developmentOptions);

                if (developmentOptions.Any())
                {
                    var constructionChoice = developmentOptions.First();

                    results.Add(() =>
                    {
                        village.UpgradeTo(constructionChoice);
                    });

                    choiceSpy.Pick(constructionChoice);

                    return true;
                }
            }
            
            return false;
        }

        private bool BuildToSolveDeficits(
            Settlement settlement,
            Dictionary<Item, double> idealNetProduction,
            Dictionary<Caste, int> availablePopByCaste,
            Vector2 settlementCenter,
            IChoiceAiSpy choiceSpy,
            List<Action> results)
        {
            foreach (var item in idealNetProduction.Keys.OrderByDescending(i => _priorityItems.Contains(i)))
            {
                // Produce to fill needs
                if (idealNetProduction[item] < 0)
                {
                    var developmentOptions = settlement.Race.AvailableCellDevelopments
                        .Where(d => !d.UpgradeOnly)
                        .Where(d => availablePopByCaste[d.ConstructionWorkers.Caste] >= d.ConstructionWorkers.Need)
                        .Select(d => Tuple.Create(d, d.Recipies.FirstOrDefault(r => r.Outputs.Any(o => o.Item == item))))
                        .Where(t => t.Item2 != null)
                        .ToArray();

                    choiceSpy.Options(item.Name, developmentOptions.Select(d => d.Item1));

                    var constructionOptions = developmentOptions
                        .Select(t => Tuple.Create(
                            t.Item1,
                            t.Item2,
                            settlement.Region.Cells
                               .Where(c => t.Item1.CanBePlaced(settlement.Owner, c))
                               .Where(c => c.Traits.ContainsAll(t.Item2.RequiredTraits))
                               .Where(c => !c.Traits.ContainsAny(_traitsToPreserve.Except(t.Item2.RequiredTraits)))
                               .OrderByDescending(c => c.Traits.Intersect(t.Item2.BoostingTraits).Count())
                               .ThenBy(c => (c.Position.ToVector() - settlementCenter).Length())
                               .FirstOrDefault()))
                        .Where(t => t.Item3 != null)
                        .ToArray();

                    if (constructionOptions.Any())
                    {
                        var constructionChoice = constructionOptions.First();

                        results.Add(() =>
                        {
                            constructionChoice.Item1.Place(settlement.Owner, constructionChoice.Item3, false);

                            constructionChoice.Item3.Development.ActiveRecipie = constructionChoice.Item2;
                        });

                        choiceSpy.Pick(constructionChoice.Item1, constructionChoice.Item2);

                        return true;
                    }
                    else if (developmentOptions.Any())
                    {
                        // have a development to place, but no place to put it, try clearing land

                        var placedClearance = BuildForClearance(
                            settlement,
                            settlementCenter,
                            choiceSpy,
                            availablePopByCaste,
                            developmentOptions,
                            results);

                        if (placedClearance)
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        private bool BuildForBonusProduction(
            Settlement settlement,
            Dictionary<Item, double> idealNetProduction,
            Dictionary<Caste, int> availablePopByCaste,
            Vector2 settlementCenter,
            IChoiceAiSpy choiceSpy,
            List<Action> results)
        {
            foreach (var item in idealNetProduction.Keys.Where(i => idealNetProduction[i] < 1).OrderByDescending(i => _bonusItems.Contains(i)))
            {
                var developmentOptions = settlement.Race.AvailableCellDevelopments
                    .Where(d => !d.UpgradeOnly)
                    .Where(d => availablePopByCaste[d.ConstructionWorkers.Caste] >= d.ConstructionWorkers.Need)
                    .Select(d => Tuple.Create(d, d.Recipies.FirstOrDefault(r => r.Outputs.Any(o => o.Item == item))))
                    .Where(t => t.Item2 != null)
                    .ToArray();

                choiceSpy.Options(item.Name, developmentOptions.Select(t => t.Item1));

                var constructionOptions = developmentOptions
                    .Select(t => Tuple.Create(
                        t.Item1,
                        t.Item2,
                        settlement.Region.Cells
                            .Where(c => t.Item1.CanBePlaced(settlement.Owner, c))
                            .Where(c => c.Traits.ContainsAll(t.Item2.RequiredTraits))
                            .Where(c => t.Item1.ConstructionCost.All(i => settlement.ItemAvailability[i.Item]))
                            .Where(c => !c.Traits.ContainsAny(_traitsToPreserve.Except(t.Item2.RequiredTraits)))
                            .OrderByDescending(c => c.Traits.Intersect(t.Item2.BoostingTraits).Count())
                            .ThenBy(c => (c.Position.ToVector() - settlementCenter).Length())
                            .FirstOrDefault()))
                    .Where(t => t.Item3 != null)
                    .ToArray();

                if (constructionOptions.Any())
                {
                    var constructionChoice = constructionOptions.First();

                    results.Add(() =>
                    {
                        constructionChoice.Item1.Place(settlement.Owner, constructionChoice.Item3, false);

                        constructionChoice.Item3.Development.ActiveRecipie = constructionChoice.Item2;
                    });

                    choiceSpy.Pick(constructionChoice.Item1, constructionChoice.Item2);

                    return true;
                }
                else if (developmentOptions.Any())
                {
                    var placedClearance = BuildForClearance(
                        settlement,
                        settlementCenter,
                        choiceSpy,
                        availablePopByCaste,
                        developmentOptions,
                        results);

                    if (placedClearance)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private bool BuildForBarracks(
            Settlement settlement,
            Dictionary<Item, double> idealNetProduction,
            Dictionary<Caste, int> availablePopByCaste,
            Vector2 settlementCenter,
            IChoiceAiSpy choiceSpy,
            List<Action> results)
        {
            var targetSlots = 2;
            if (settlement.Owner.Controller.IsAggressive)
            {
                targetSlots = 4;
            }

            var recruitmentCategories = _gamedataTracker.RecruitmentSlotCategories;

            choiceSpy.Log("Max Recruits Slots: " + string.Join(", ", recruitmentCategories.Select(c => $"{c.Name}: {settlement.RecruitmentSlotsMax[c]}")));
            choiceSpy.Log($"Target Slots: {targetSlots}");



            var developmentOptions = settlement.Race.AvailableCellDevelopments
                .Where(d => !d.UpgradeOnly)
                .Where(d => availablePopByCaste[d.ConstructionWorkers.Caste] >= d.ConstructionWorkers.Need)
                .Where(d => d is IRecruitmentSlotProviderType rspt && settlement.RecruitmentSlotsMax[rspt.RecruitmentCategory] < targetSlots)
                .ToArray();

            choiceSpy.Options(null, developmentOptions);

            var constructionOptions = developmentOptions
                .Select(d => Tuple.Create(
                    d,
                    settlement.Region.Cells
                        .Where(c => d.CanBePlaced(settlement.Owner, c))
                        .Where(c => d.ConstructionCost.All(i => settlement.ItemAvailability[i.Item]))
                        .Where(c => !c.Traits.ContainsAny(_traitsToPreserve))
                        .OrderBy(c => (c.Position.ToVector() - settlementCenter).Length())
                        .FirstOrDefault()))
                .Where(t => t.Item2 != null)
                .ToArray();

            if (constructionOptions.Any())
            {
                var constructionChoice = constructionOptions.First();

                results.Add(() =>
                {
                    constructionChoice.Item1.Place(settlement.Owner, constructionChoice.Item2, false);
                });

                choiceSpy.Pick(constructionChoice.Item1);

                return true;
            }
            else if (developmentOptions.Any())
            {
                var placedClearance = BuildForClearance(
                    settlement,
                    settlementCenter,
                    choiceSpy,
                    availablePopByCaste,
                    developmentOptions,
                    results);

                if (placedClearance)
                {
                    return true;
                }
            }

            return false;
        }

        private bool BuildForTrade(
            Settlement settlement,
            Dictionary<Item, double> idealNetProduction,
            Dictionary<Caste, int> availablePopByCaste,
            Dictionary<MovementType, int> tradeCapacity,
            Vector2 settlementCenter,
            IChoiceAiSpy choiceSpy,
            List<Action> results)
        {
            var tradeDevelopmentOptions = settlement.Race.AvailableCellDevelopments
                .Where(d => !d.UpgradeOnly)
                .OfType<ITradeActorType>()
                .Where(t => !tradeCapacity.ContainsKey(t.TradeType) || tradeCapacity[t.TradeType] < 1)
                .OfType<ICellDevelopmentType>()
                .ToArray();

            choiceSpy.Options(null, tradeDevelopmentOptions);

            var constructionOptions = tradeDevelopmentOptions
                .Select(t => Tuple.Create(
                        t,
                        settlement.Region.Cells
                            .Where(c => t.CanBePlaced(settlement.Owner, c))
                            .Where(c => t.ConstructionCost.All(i => settlement.ItemAvailability[i.Item]))
                            .OrderBy(c => c.Traits.Intersect(_traitsToPreserve).Count())
                            .Where(c => !c.Traits.ContainsAny(_traitsToPreserve))
                            .OrderBy(c => (c.Position.ToVector() - settlementCenter).Length())
                            .FirstOrDefault()))
                .Where(t => t.Item2 != null)
                .ToArray();

            if (constructionOptions.Any())
            {
                var constructionChoice = constructionOptions.First();

                results.Add(() =>
                {
                    constructionChoice.Item1.Place(settlement.Owner, constructionChoice.Item2, false);
                });

                choiceSpy.Pick(constructionChoice.Item1);

                return true;
            }
            else if (tradeDevelopmentOptions.Any())
            {
                var placedClearance = BuildForClearance(
                    settlement,
                    settlementCenter,
                    choiceSpy,
                    availablePopByCaste,
                    tradeDevelopmentOptions,
                    results);

                if (placedClearance)
                {
                    return true;
                }
            }

            return false;
        }

        private bool BuildForShrine(
            Settlement settlement,
            Dictionary<Item, double> idealNetProduction,
            Dictionary<Caste, int> availablePopByCaste,
            Vector2 settlementCenter,
            IChoiceAiSpy choiceSpy,
            List<Action> results)
        {
            if (settlement.EconomicActors.Any(ea => ea is ShrineDevelopment))
            {
                return false;
            }
            
            var developmentOptions = settlement.Race.AvailableCellDevelopments
                .Where(d => !d.UpgradeOnly)
                .Where(d => availablePopByCaste[d.ConstructionWorkers.Caste] >= d.ConstructionWorkers.Need)
                .Where(d => d is ShrineDevelopmentType sdt && _targetCults.Contains(sdt.Cult))
                .ToArray();
            
            choiceSpy.Options(null, developmentOptions);

            new Random().Shuffle(developmentOptions);

            var constructionOptions = developmentOptions
                .Select(d => Tuple.Create(
                    d,
                    settlement.Region.Cells
                        .Where(c => d.CanBePlaced(settlement.Owner, c))
                        .Where(c => d.ConstructionCost.All(i => settlement.ItemAvailability[i.Item]))
                        .Where(c => !c.Traits.ContainsAny(_traitsToPreserve))
                        .OrderBy(c => (c.Position.ToVector() - settlementCenter).Length())
                        .FirstOrDefault()))
                .Where(t => t.Item2 != null)
                .ToArray();

            if (constructionOptions.Any())
            {
                var constructionChoice = constructionOptions.First();

                results.Add(() =>
                {
                    constructionChoice.Item1.Place(settlement.Owner, constructionChoice.Item2, false);
                });

                choiceSpy.Pick(constructionChoice.Item1);

                return true;
            }
            else if (developmentOptions.Any())
            {
                var placedClearance = BuildForClearance(
                    settlement,
                    settlementCenter,
                    choiceSpy,
                    availablePopByCaste,
                    developmentOptions,
                    results);

                if (placedClearance)
                {
                    return true;
                }
            }

            return false;
        }

        private bool BuildForClearance(
            Settlement settlement,
            Vector2 settlementCenter,
            IChoiceAiSpy choiceSpy,
            Dictionary<Caste, int> availablePopByCaste,
            Tuple<ICellDevelopmentType, Recipie>[] cellDevelopmentsToTryToPlace,
            List<Action> results)
        {
            var placementPossibilities = cellDevelopmentsToTryToPlace
                .Select(t => Tuple.Create(
                    t.Item1,
                    t.Item2,
                    settlement.Region.Cells
                        .Where(c => c.Feature != null)
                        .Where(c => c.Feature.Traits.ContainsAny(_traitsToClear))
                        .Where(c => t.Item1.CouldBePlacedWithoutFeature(settlement.Owner, c))
                        .Where(c => c.Traits.ContainsAll(t.Item2.RequiredTraits))
                        .Where(c => t.Item1.ConstructionCost.All(i => settlement.ItemAvailability[i.Item]))
                        .Where(c => !c.Traits.ContainsAny(_traitsToPreserve))
                        .OrderByDescending(c => c.Traits.Intersect(t.Item2.BoostingTraits).Count())
                        .ThenBy(c => (c.Position.ToVector() - settlementCenter).Length())
                        .FirstOrDefault()))
                .Where(t => t.Item3 != null)
                .ToArray();

            var clearanceOption = placementPossibilities
                .Select(t => Tuple.Create(
                    t.Item3,
                    t.Item1,
                    settlement.Race.AvailableCellDevelopments
                            .OfType<ClearFeatureDevelopmentType>()
                            .Where(d => availablePopByCaste[d.ConstructionWorkers.Caste] >= d.ConstructionWorkers.Need)
                            .Where(d => d.CanBePlaced(settlement.Owner, t.Item3))
                            .FirstOrDefault()))
                .Where(t => t.Item3 != null)
                .FirstOrDefault();


            if (clearanceOption != null)
            {
                results.Add(() =>
                {
                    clearanceOption.Item3.Place(settlement.Owner, clearanceOption.Item1, false);
                });

                choiceSpy.PickForClearing(clearanceOption.Item3, clearanceOption.Item2);

                return true;
            }

            return false;
        }

        private bool BuildForClearance(
            Settlement settlement,
            Vector2 settlementCenter,
            IChoiceAiSpy choiceSpy,
            Dictionary<Caste, int> availablePopByCaste,
            ICellDevelopmentType[] cellDevelopmentsToTryToPlace,
            List<Action> results)
        {
            var placementPossibilities = cellDevelopmentsToTryToPlace
                .Select(d => Tuple.Create(
                    d,
                    settlement.Region.Cells
                        .Where(c => c.Feature != null)
                        .Where(c => c.Feature.Traits.ContainsAny(_traitsToClear))
                        .Where(c => d.CouldBePlacedWithoutFeature(settlement.Owner, c))
                        .Where(c => d.ConstructionCost.All(i => settlement.ItemAvailability[i.Item]))
                        .Where(c => !c.Traits.ContainsAny(_traitsToPreserve))
                        .OrderBy(c => (c.Position.ToVector() - settlementCenter).Length())
                        .FirstOrDefault()))
                .Where(t => t.Item2 != null)
                .ToArray();

            var clearanceOption = placementPossibilities
                .Select(t => Tuple.Create(
                    t.Item2,
                    t.Item1,
                    settlement.Race.AvailableCellDevelopments
                            .OfType<ClearFeatureDevelopmentType>()
                            .Where(d => availablePopByCaste[d.ConstructionWorkers.Caste] >= d.ConstructionWorkers.Need)
                            .Where(d => d.CanBePlaced(settlement.Owner, t.Item2))
                            .FirstOrDefault()))
                .Where(t => t.Item3 != null)
                .FirstOrDefault();


            if (clearanceOption != null)
            {
                results.Add(() =>
                {
                    clearanceOption.Item3.Place(settlement.Owner, clearanceOption.Item1, false);
                });

                choiceSpy.PickForClearing(clearanceOption.Item3, clearanceOption.Item2);

                return true;
            }

            return false;
        }

        private Vector2 GetSettlementCenter(Settlement settlement)
        {
            var center = settlement.Region.BoundingBox.Center.ToVector2();

            var cityCenter = settlement.EconomicActors
                .OfType<ICellDevelopment>()
                .Where(a => a is VillageDevelopment || a is DistrictDevelopment)
                .FirstOrDefault();

            if (cityCenter != null)
            {
                center = cityCenter.Cell.Position.ToVector();
            }

            return center;
        }

        public void LoadFrom(XElement element)
        {
            _priorityItems = element
                .Elements("production_priority")
                .Select(e => _xmlReaderUtil.ObjectReferenceLookup(
                    e,
                    "item",
                    _gamedataTracker.Items,
                    i => i.Name))
                .ToArray();

            _bonusItems = element
                .Elements("production_bonus")
                .Select(e => _xmlReaderUtil.ObjectReferenceLookup(
                    e,
                    "item",
                    _gamedataTracker.Items,
                    i => i.Name))
                .ToArray();

            _traitsToClear = element
                .Elements("land_clearing")
                .Select(e => _xmlReaderUtil.ObjectReferenceLookup(e, "trait", _gamedataTracker.Traits, t => t.Id))
                .ToArray();

            _traitsToPreserve = element
                .Elements("preserve")
                .Select(e => _xmlReaderUtil.ObjectReferenceLookup(e, "trait", _gamedataTracker.Traits, t => t.Id))
                .ToArray();

            _pacificationTargets = element
                .Elements("pacification_target")
                .Select(e => _xmlReaderUtil.ObjectReferenceLookup(
                    e,
                    "development_id",
                    _gamedataTracker.CellDevelopments,
                    d => d.Id))
                .ToArray();

            _itemImportTargets = element
                .Elements("import_item")
                .Select(e => new ItemImportTarget
                {
                    Item = _xmlReaderUtil.ObjectReferenceLookup(
                        e,
                        "item",
                        _gamedataTracker.Items,
                        i => i.Name),
                    TargetSurplus = _xmlReaderUtil.AttributeAsInt(e, "target_surplus"),
                    Value = _xmlReaderUtil.AttributeAsInt(e, "value")
                })
                .ToArray();
            
            _targetCults = element.Elements("target_cult")
                .Select(e => _xmlReaderUtil.ObjectReferenceLookup(
                    e,
                    "cult",
                    _gamedataTracker.Cults,
                    c => c.Id))
                .ToArray();
        }

        public IEnumerable<ItemImportTarget> GetItemImportTargets()
        {
            return _itemImportTargets;
        }
    }
}
