﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Bronze.Contracts.Data.AI;
using Bronze.Contracts.Data.Diplomacy;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.Data.Game.AI
{
    public abstract class AbstractTribeControllerType : ITribeControllerType
    {
        public string Id { get; private set; }

        public float Aggression { get; private set; }
        public int MaxTerritory { get; private set; }

        public Dictionary<Trait, int> ExpansionGoalScores { get; private set; }
        public Dictionary<Trait, int> ExpansionGoalNeighborScores { get; private set; }
        public Dictionary<Trait, int> SettlementPlacementGoalScores { get; private set; }
        public Dictionary<Trait, int> SettlementPlacementNeighborScores { get; private set; }
        public ISettlementRole[] SettlementRoles { get; private set; }
        public Trait WalkableTrait { get; private set; }

        private readonly IInjectionProvider _injectionProvider;

        public AbstractTribeControllerType(IInjectionProvider injectionProvider)
        {
            _injectionProvider = injectionProvider;

            ExpansionGoalScores = new Dictionary<Trait, int>();
            ExpansionGoalNeighborScores = new Dictionary<Trait, int>();
            SettlementPlacementGoalScores = new Dictionary<Trait, int>();
            SettlementPlacementNeighborScores = new Dictionary<Trait, int>();
        }
        
        public void LoadFrom(
            IGamedataTracker gamedataTracker, 
            IXmlReaderUtil xmlReaderUtil,
            XElement element)
        {
            Id = xmlReaderUtil.AttributeValue(element, "id");

            Aggression = xmlReaderUtil.AttributeAsFloat(element, "aggression");
            MaxTerritory = xmlReaderUtil.AttributeAsInt(element, "max_territory");
            WalkableTrait = xmlReaderUtil.ObjectReferenceLookup(
                element,
                "walkable_trait_id",
                gamedataTracker.Traits,
                t => t.Id);
            
            foreach (var expansionGoalElement in element.Elements("expansion_goal"))
            {
                if (expansionGoalElement.Attribute("trait") != null)
                {
                    var trait = xmlReaderUtil.ObjectReferenceLookup(
                        expansionGoalElement,
                        "trait",
                        gamedataTracker.Traits,
                        t => t.Id);

                    var score = xmlReaderUtil.AttributeAsInt(expansionGoalElement, "score");

                    if (ExpansionGoalScores.ContainsKey(trait))
                    {
                        ExpansionGoalScores[trait] += score;
                    }
                    else
                    {
                        ExpansionGoalScores.Add(trait, score);
                    }
                }
                else if (expansionGoalElement.Attribute("neighbor_trait") != null)
                {
                    var trait = xmlReaderUtil.ObjectReferenceLookup(
                        expansionGoalElement,
                        "neighbor_trait",
                        gamedataTracker.Traits,
                        t => t.Id);

                    var score = xmlReaderUtil.AttributeAsInt(expansionGoalElement, "score");

                    if (ExpansionGoalNeighborScores.ContainsKey(trait))
                    {
                        ExpansionGoalNeighborScores[trait] += score;
                    }
                    else
                    {
                        ExpansionGoalNeighborScores.Add(trait, score);
                    }
                }
            }

            foreach (var settlementPlacementGoalElement in element.Elements("settlement_placement"))
            {
                if (settlementPlacementGoalElement.Attribute("trait") != null)
                {
                    var trait = xmlReaderUtil.ObjectReferenceLookup(
                        settlementPlacementGoalElement,
                        "trait",
                        gamedataTracker.Traits,
                        t => t.Id);

                    var score = xmlReaderUtil.AttributeAsInt(settlementPlacementGoalElement, "score");

                    if (SettlementPlacementGoalScores.ContainsKey(trait))
                    {
                        SettlementPlacementGoalScores[trait] += score;
                    }
                    else
                    {
                        SettlementPlacementGoalScores.Add(trait, score);
                    }
                }
                else if (settlementPlacementGoalElement.Attribute("neighbor_trait") != null)
                {
                    var trait = xmlReaderUtil.ObjectReferenceLookup(
                        settlementPlacementGoalElement,
                        "neighbor_trait",
                        gamedataTracker.Traits,
                        t => t.Id);

                    var score = xmlReaderUtil.AttributeAsInt(settlementPlacementGoalElement, "score");

                    if (SettlementPlacementNeighborScores.ContainsKey(trait))
                    {
                        SettlementPlacementNeighborScores[trait] += score;
                    }
                    else
                    {
                        SettlementPlacementNeighborScores.Add(trait, score);
                    }
                }
            }

            SettlementRoles = element
                .Elements("settlement_role")
                .OrderBy(x => xmlReaderUtil.AttributeAsInt(x, "priority"))
                .Select(x => LoadSettlementRole(xmlReaderUtil, x))
                .ToArray();
        }

        private ISettlementRole LoadSettlementRole(
            IXmlReaderUtil xmlReaderUtil,
            XElement element)
        {
            var type = xmlReaderUtil.AttributeValue(element, "type");

            var role = _injectionProvider.BuildNamed<ISettlementRole>(type);

            role.LoadFrom(element);

            return role;
        }

        public abstract ITribeController BuildController(Tribe tribe, Random random, double worldHostility, double month);

        public abstract ITribeController DeserializeFrom(IEnumerable<Region> regions, SerializedObject controllerRoot, Tribe tribe);
    }
}
