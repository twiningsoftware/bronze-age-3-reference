﻿using Bronze.Contracts.Data.AI;
using Bronze.Contracts.Data.Diplomacy;
using Bronze.Contracts.Data.World;

namespace Bronze.Common.Data.Game.AI.Messaging
{
    public class EndWarInterAiMessage : IInterAiMessage
    {
        public Tribe From { get; set; }
        public Tribe To { get; set; }
        public War War { get; set; }
    }
}
