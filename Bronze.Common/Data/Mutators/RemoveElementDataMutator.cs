﻿using Bronze.Contracts.Data.Gamedata;
using System.Xml.Linq;
using System.Xml.XPath;

namespace Bronze.Common.Mutators
{
    public class RemoveElementDataMutator : IDataMutator
    {
        private string _xpath;
        
        public RemoveElementDataMutator(
            string xpath)
        {
            _xpath = xpath;
        }

        public void Mutate(XDocument dataDoc)
        {
            var element = dataDoc.XPathSelectElement(_xpath);

            if(element != null)
            {
                element.Remove();
            }
        }
    }
}
