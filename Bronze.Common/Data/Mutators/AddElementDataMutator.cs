﻿using Bronze.Contracts.Data.Gamedata;
using System.Xml.Linq;
using System.Xml.XPath;

namespace Bronze.Common.Mutators
{
    public class AddElementDataMutator : IDataMutator
    {
        private string _xpath;
        private string _childXml;

        public AddElementDataMutator(
            string xpath, 
            string childXml)
        {
            _xpath = xpath;
            _childXml = childXml;
        }

        public void Mutate(XDocument dataDoc)
        {
            var element = dataDoc.XPathSelectElement(_xpath);

            if (element != null)
            {
                var childXml = XDocument.Parse(_childXml);

                element.Add(childXml.Root);
            }
        }
    }
}
