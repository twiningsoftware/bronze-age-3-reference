﻿using Bronze.Contracts.Data.Gamedata;
using System.Xml.Linq;
using System.Xml.XPath;

namespace Bronze.Common.Mutators
{
    public class SetAttributeDataMutator : IDataMutator
    {
        private string _xpath;
        private string _attributeName;
        private string _attributeValue;

        public SetAttributeDataMutator(
            string xpath, 
            string attributeName, 
            string attributeValue)
        {
            _xpath = xpath;
            _attributeName = attributeName;
            _attributeValue = attributeValue;
        }

        public void Mutate(XDocument dataDoc)
        {
            var element = dataDoc.XPathSelectElement(_xpath);

            if (element != null)
            {
                element.SetAttributeValue(_attributeName, _attributeValue);
            }
        }
    }
}
