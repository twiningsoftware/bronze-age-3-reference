﻿using Bronze.Common.Data.Game.CellDevelopments;
using Bronze.Contracts.Data.Dungeons;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;

namespace Bronze.Common.Data.Dungeons
{
    public class DungeonDevelopmentType : AbstractCellDevelopmentType
    {
        private readonly IInjectionProvider _injectionProvider;

        public DungeonDefinition DungeonDefinition { get; set; }

        public DungeonDevelopmentType(IInjectionProvider injectionProvider)
        {
            _injectionProvider = injectionProvider;
        }

        public override void Place(Tribe tribe, Cell cell, bool instantBuild)
        {
            var random = new Random(cell.Position.GetHashCode() + cell.Region.Id);

            var development = _injectionProvider
                .Build<DungeonDevelopment>()
                .Initialize(cell, this, instantBuild)
                as DungeonDevelopment;

            cell.Development = development;

            development.BuildDungeon(random);
        }

        public override ICellDevelopment DeserializeFrom(
            IGamedataTracker gamedataTracker,
            Cell cell,
            SerializedObject developmentRoot)
        {
            var development = _injectionProvider
                .Build<DungeonDevelopment>()
                .Initialize(cell, this, instantBuild: false);

            development.DeserializeFrom(gamedataTracker, developmentRoot);

            return development;
        }
    }
}
