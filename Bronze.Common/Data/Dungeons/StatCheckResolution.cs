﻿using Bronze.Contracts.Data.Dungeons;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;
using System.Linq;
using System.Xml.Linq;

namespace Bronze.Common.Data.Dungeons
{
    /// <summary>
    /// A room resolution that has two options, success or failure, based on the character's stats.
    /// The evaluation is based on a dice roll, a simulated d20 roll + the appropriate stat, compared 
    /// to the target number.
    /// </summary>
    public class StatCheckResolution : IRoomResolution
    {
        public string Text { get; private set; }
        public NotablePersonStatType Stat { get; private set; }
        public double Target { get; private set; }

        public ResolutionData SuccessResolution { get; private set; }
        public ResolutionData FailureResolution { get; private set; }
        public IfState[] IfStates { get; private set; }

        public IRoomResolution LoadFrom(IInjectionProvider injectionProvider, IXmlReaderUtil xmlreaderUtil, XElement element, DungeonDefinition dungeonDefinition)
        {
            Text = xmlreaderUtil.AttributeValue(element, "text");
            Stat = xmlreaderUtil.AttributeAsEnum<NotablePersonStatType>(element, "stat");
            Target = xmlreaderUtil.AttributeAsDouble(element, "target");

            IfStates = element
                .Elements("if_state")
                .Select(e => new IfState
                {
                    State = xmlreaderUtil.AttributeValue(e, "state"),
                    Value = xmlreaderUtil.AttributeValue(e, "value")
                })
                .ToArray();

            SuccessResolution = ResolutionData.LoadFrom(
                injectionProvider,
                xmlreaderUtil, 
                xmlreaderUtil.Element(element, "success"), 
                dungeonDefinition);

            FailureResolution = ResolutionData.LoadFrom(
                injectionProvider,
                xmlreaderUtil, 
                xmlreaderUtil.Element(element, "failure"),
                dungeonDefinition);

            return this;
        }

        public ResolutionData GetResolution(
            Random random,
            Cell dungeonLocation,
            GeneratedDungeon dungeon,
            NotablePerson character,
            IUnit bodyguard)
        {
            if (character.GetStat(Stat) + random.Next(1, 21) >= Target)
            {
                return SuccessResolution;
            }
            else
            {
                return FailureResolution;
            }
        }
    }
}