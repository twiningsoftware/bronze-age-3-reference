﻿using Bronze.Common.Data.World.CellDevelopments;
using Bronze.Common.UI.Modals;
using Bronze.Contracts.Data.Dungeons;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;

namespace Bronze.Common.Data.Dungeons
{
    public class DungeonDevelopment : AbstractCellDevelopment<DungeonDevelopmentType>, IArmyInteractable
    {
        public string InteractVerb => "Exploring";
        public bool InteractAdjacent => true;
        public bool InteractWithForeign => true;
        public override bool CanBeSieged => false;
        public override bool CanBeDestroyed => false;
        public bool IsExplored { get; set; }

        private readonly IPlayerDataTracker _playerData;
        private readonly IDialogManager _dialogManager;
        private readonly IDungeonDataManager _dungeonDataManager;

        private GeneratedDungeon _generatedDungeon;
        
        public DungeonDevelopment(
            IDialogManager dialogManager, 
            IGamedataTracker gamedataTracker,
            IPlayerDataTracker playerData,
            IDungeonDataManager dungeonDataManager,
            ISimulationEventBus simulationEventBus)
            : base(dialogManager, gamedataTracker, simulationEventBus)
        {
            _playerData = playerData;
            _dialogManager = dialogManager;
            _dungeonDataManager = dungeonDataManager;
        }

        public override string Name
        {
            get
            {
                if(IsExplored)
                {
                    return $"{base.Name} (explored)";
                }

                return base.Name;
            }
        }

        public void BuildDungeon(Random random)
        {
            _generatedDungeon = new GeneratedDungeon(random.Next(), BaseType.DungeonDefinition);
        }
        
        public void DoInteraction(Army interactingArmy)
        {
            if (interactingArmy.Owner == _playerData.PlayerTribe
                && _generatedDungeon != null)
            {
                IsExplored = true;

                _dialogManager.PushModal<RunDungeonDialog, RunDungeonDialogContext>(
                    new RunDungeonDialogContext
                    {
                        GeneratedDungeon = _generatedDungeon,
                        Character = interactingArmy.General,
                        Army = interactingArmy,
                        DungeonLocation = Cell
                    });
            }
        }

        public override void SerializeTo(SerializedObject developmentRoot)
        {
            base.SerializeTo(developmentRoot);

            _generatedDungeon.SerializeTo(developmentRoot.CreateChild("generated_dungeon"));

            developmentRoot.Set("is_explored", IsExplored);
        }

        public override void DeserializeFrom(
            IGamedataTracker gamedataTracker, 
            SerializedObject developmentRoot)
        {
            base.DeserializeFrom(gamedataTracker, developmentRoot);

            _generatedDungeon = GeneratedDungeon.DeserializeFrom(
                _dungeonDataManager,
                developmentRoot.GetChild("generated_dungeon"));

            IsExplored = developmentRoot.GetBool("is_explored");
        }
    }
}
