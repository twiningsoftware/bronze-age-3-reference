﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Dungeons;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Elements;

namespace Bronze.Common.Data.Dungeons.ResolutionActions
{
    /// <summary>
    /// This is a resolution action that adds or removes health from the bodyguard unit.
    /// </summary>
    public class BodyguardHealthResolutionAction : IResolutionAction
    {
        private double _healthPerc;
        
        public IResolutionAction LoadFrom(
            IXmlReaderUtil xmlreaderUtil, 
            XElement element, 
            DungeonDefinition dungeonDefinition)
        {
            _healthPerc = xmlreaderUtil.AttributeAsDouble(element, "value");
            
            return this;
        }

        public IEnumerable<IUiElement> Apply(
            Random random,
            Cell dungeonLocation,
            GeneratedDungeon dungeon, 
            NotablePerson character, 
            IUnit bodyguard)
        {
            var newHealth = Util.Clamp(bodyguard.HealthPercent + _healthPerc, 0, 1) * bodyguard.MaxHealth;

            var deltaHealth = newHealth - bodyguard.Health;

            bodyguard.Health = newHealth;

            if((int)deltaHealth < 0)
            {
                return new Label($"{bodyguard.Name} loses {Math.Abs((int)deltaHealth)} health.", BronzeColor.Red).Yield();
            }
            else if ((int)deltaHealth > 0)
            {
                return new Label($"{bodyguard.Name} gains {Math.Abs((int)deltaHealth)} health.", BronzeColor.Green).Yield();
            }
            
            return Enumerable.Empty<IUiElement>();
        }
    }
}
