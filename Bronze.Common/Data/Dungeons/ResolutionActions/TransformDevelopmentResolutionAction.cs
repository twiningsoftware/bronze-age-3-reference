﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Bronze.Contracts.Data.Dungeons;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;

namespace Bronze.Common.Data.Dungeons.ResolutionActions
{
    /// <summary>
    /// A resolution action that changes the dungeon cell development into a different cell development or cell feature.
    /// </summary>
    public class TransformDevelopmentResolutionAction : IResolutionAction
    {
        private ICellDevelopmentType _cellDevelopmentType;
        private ICellFeature _cellFeature;
        private readonly IGamedataTracker _gamedataTracker;

        public TransformDevelopmentResolutionAction(IGamedataTracker gamedataTracker)
        {
            _gamedataTracker = gamedataTracker;
        }
        
        public IResolutionAction LoadFrom(
            IXmlReaderUtil xmlreaderUtil, 
            XElement element, 
            DungeonDefinition dungeonDefinition)
        {
            _cellDevelopmentType = xmlreaderUtil.OptionalObjectReferenceLookup(
                element,
                "cell_development",
                _gamedataTracker.CellDevelopments,
                cd => cd.Id);

            _cellFeature = xmlreaderUtil.OptionalObjectReferenceLookup(
                element,
                "cell_feature",
                _gamedataTracker.CellFeatures,
                cf => cf.Id);

            return this;
        }

        public IEnumerable<IUiElement> Apply(
            Random random,
            Cell dungeonLocation,
            GeneratedDungeon dungeon, 
            NotablePerson character, 
            IUnit bodyguard)
        {
            dungeonLocation.Feature = _cellFeature;
            if(_cellDevelopmentType != null)
            {
                _cellDevelopmentType.Place(
                    null,
                    dungeonLocation,
                    true);
            }
            else
            {
                dungeonLocation.Development = null;
            }
            
            return Enumerable.Empty<IUiElement>();
        }
    }
}
