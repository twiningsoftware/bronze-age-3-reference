﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Dungeons;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;

namespace Bronze.Common.Data.Dungeons.ResolutionActions
{
    /// <summary>
    /// This is a resolution action that gives the player a free unit, either into their current army, or a new one.
    /// </summary>
    public class UnitRewardResolutionAction : IResolutionAction
    {
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IWorldManager _worldManager;
        private IUnitType _unitType;
        private UnitEquipmentSet _equipmentSet;
        
        public UnitRewardResolutionAction(
            IGamedataTracker gamedataTracker,
            IWorldManager worldManager)
        {
            _gamedataTracker = gamedataTracker;
            _worldManager = worldManager;
        }
        
        public IResolutionAction LoadFrom(
            IXmlReaderUtil xmlreaderUtil, 
            XElement element, 
            DungeonDefinition dungeonDefinition)
        {
            _unitType = xmlreaderUtil.ObjectReferenceLookup(
                element,
                "unit_type",
                _gamedataTracker.Races.SelectMany(r => r.AvailableUnits).ToArray(),
                ut => ut.Id);

            _equipmentSet = xmlreaderUtil.ObjectReferenceLookup(
                element,
                "equipment_set",
                _unitType.EquipmentSets,
                es => es.Level.ToString());
            
            return this;
        }

        public IEnumerable<IUiElement> Apply(
            Random random,
            Cell dungeonLocation,
            GeneratedDungeon dungeon, 
            NotablePerson character, 
            IUnit bodyguard)
        {
            var army = character.GeneralOf;

            if(army.IsFull)
            {
                army = new Army(character.Owner, character.GeneralOf.Cell, _worldManager);
            }

            army.Units.Add(_unitType.CreateUnit(_equipmentSet));

            if (!string.IsNullOrWhiteSpace(_equipmentSet.Name))
            {
                return new StackGroup
                {
                    Orientation = Orientation.Horizontal,
                    Children = new List<IUiElement>
                    {
                        new Label($"You recieve a {_unitType.Name} with {_equipmentSet.Name}.", BronzeColor.Yellow)
                    }
                }.Yield();
            }

            return new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new Label($"You recieve a {_unitType.Name}.", BronzeColor.Yellow)
                }
            }.Yield();
        }
    }
}
