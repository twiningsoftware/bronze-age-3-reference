﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Bronze.Contracts;
using Bronze.Contracts.Data.Dungeons;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;

namespace Bronze.Common.Data.Dungeons.ResolutionActions
{
    /// <summary>
    /// This is a resolution action that randomly picks one of serveral other resolution actions.
    /// </summary>
    public class PickOptionResolutionAction : IResolutionAction
    {
        private readonly IInjectionProvider _injectionProvider;
        private IResolutionAction[] _options;

        public PickOptionResolutionAction(
            IInjectionProvider injectionProvider)
        {
            _injectionProvider = injectionProvider;
        }
        
        public IResolutionAction LoadFrom(
            IXmlReaderUtil xmlReaderUtil, 
            XElement element, 
            DungeonDefinition dungeonDefinition)
        {
            _options = element.Elements("action")
                .Select(e => _injectionProvider
                    .BuildNamed<IResolutionAction>(xmlReaderUtil.AttributeValue(e, "type"))
                    .LoadFrom(xmlReaderUtil, e, dungeonDefinition))
                .ToArray();
            
            return this;
        }

        public IEnumerable<IUiElement> Apply(
            Random random,
            Cell dungeonLocation,
            GeneratedDungeon dungeon, 
            NotablePerson character, 
            IUnit bodyguard)
        {
            var results = new List<IUiElement>();

            if(_options.Length > 0)
            {
                results.AddRange(random.Choose(_options).Apply(random, dungeonLocation, dungeon, character, bodyguard));
            }
            
            return results;
        }
    }
}
