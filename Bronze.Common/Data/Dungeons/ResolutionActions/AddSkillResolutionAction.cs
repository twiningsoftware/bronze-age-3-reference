﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Bronze.Common.UI;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Dungeons;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;

namespace Bronze.Common.Data.Dungeons.ResolutionActions
{
    /// <summary>
    /// This is a resolution action that gives the character a new skill, or increases the level of an existing skill by 1.
    /// </summary>
    public class AddSkillResolutionAction : IResolutionAction
    {
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IWorldManager _worldManager;
        private NotablePersonSkill _skill;
        
        public AddSkillResolutionAction(
            IGamedataTracker gamedataTracker,
            IWorldManager worldManager)
        {
            _gamedataTracker = gamedataTracker;
            _worldManager = worldManager;
        }
        
        public IResolutionAction LoadFrom(
            IXmlReaderUtil xmlreaderUtil, 
            XElement element, 
            DungeonDefinition dungeonDefinition)
        {
            _skill = xmlreaderUtil.ObjectReferenceLookup(
                element,
                "skill_id",
                _gamedataTracker.Skills,
                s => s.Id);
            
            return this;
        }

        public IEnumerable<IUiElement> Apply(
            Random random,
            Cell dungeonLocation,
            GeneratedDungeon dungeon, 
            NotablePerson character, 
            IUnit bodyguard)
        {
            var skillPlacement = character.Skills
                    .Where(sp => sp.Skill == _skill)
                    .FirstOrDefault();

            if (skillPlacement == null || skillPlacement.Level.Level < _skill.Levels.Max(l => l.Level))
            {
                if(skillPlacement == null)
                {
                    skillPlacement = new NotablePersonSkillPlacement
                    {
                        Skill = _skill,
                        Level = _skill.Levels.Where(l => l.Level == 0).First()
                    };
                    character.Skills.Add(skillPlacement);
                }
                else
                {
                    skillPlacement.Level = skillPlacement.Skill.Levels.Where(l => l.Level == skillPlacement.Level.Level + 1).First();
                }

                return new StackGroup
                {
                    Orientation = Orientation.Horizontal,
                    Children = new List<IUiElement>
                    {
                        new Label($"{character.Name} gains ", BronzeColor.Yellow),
                        UiBuilder.BuildDisplayFor(skillPlacement)
                    }
                }.Yield();
            }
            
            return Enumerable.Empty<IUiElement>();
        }
    }
}
