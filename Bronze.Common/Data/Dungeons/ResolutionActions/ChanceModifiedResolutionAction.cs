﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Bronze.Contracts.Data.Dungeons;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;

namespace Bronze.Common.Data.Dungeons.ResolutionActions
{
    /// <summary>
    /// This is a resolution action that has a chance of appling one or more other resolution actions.
    /// </summary>
    public class ChanceModifiedResolutionAction : IResolutionAction
    {
        private readonly IInjectionProvider _injectionProvider;
        private IResolutionAction[] _subActions;
        private double _chance;

        public ChanceModifiedResolutionAction(
            IInjectionProvider injectionProvider)
        {
            _injectionProvider = injectionProvider;
        }
        
        public IResolutionAction LoadFrom(
            IXmlReaderUtil xmlReaderUtil, 
            XElement element, 
            DungeonDefinition dungeonDefinition)
        {
            _chance = xmlReaderUtil.AttributeAsDouble(element, "chance");

            _subActions = element.Elements("action")
                .Select(e => _injectionProvider
                    .BuildNamed<IResolutionAction>(xmlReaderUtil.AttributeValue(e, "type"))
                    .LoadFrom(xmlReaderUtil, e, dungeonDefinition))
                .ToArray();
            
            return this;
        }

        public IEnumerable<IUiElement> Apply(
            Random random,
            Cell dungeonLocation,
            GeneratedDungeon dungeon, 
            NotablePerson character, 
            IUnit bodyguard)
        {
            var results = new List<IUiElement>();
            
            if (random.NextDouble() <= _chance)
            {
                foreach(var action in _subActions)
                {
                    results.AddRange(action.Apply(random, dungeonLocation, dungeon, character, bodyguard));
                }
            }

            return results;
        }
    }
}
