﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Bronze.Contracts.Data.Dungeons;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;

namespace Bronze.Common.Data.Dungeons.ResolutionActions
{
    /// <summary>
    /// A resolution action that leaves the dungeon.
    /// </summary>
    public class TransitionExitResolutionAction : IResolutionAction
    {
        private readonly IDialogManager _dialogManager;

        public TransitionExitResolutionAction(IDialogManager dialogManager)
        {
            _dialogManager = dialogManager;
        }

        public IResolutionAction LoadFrom(
            IXmlReaderUtil xmlreaderUtil, 
            XElement element, 
            DungeonDefinition dungeonDefinition)
        {
            return this;
        }

        public IEnumerable<IUiElement> Apply(
            Random random,
            Cell dungeonLocation,
            GeneratedDungeon dungeon, 
            NotablePerson character, 
            IUnit bodyguard)
        {
            _dialogManager.PopModal();

            return Enumerable.Empty<IUiElement>();
        }
    }
}
