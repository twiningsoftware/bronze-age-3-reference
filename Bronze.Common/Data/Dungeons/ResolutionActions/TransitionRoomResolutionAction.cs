﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Bronze.Contracts.Data.Dungeons;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;

namespace Bronze.Common.Data.Dungeons.ResolutionActions
{
    /// <summary>
    /// A resolution action that moves the character to a different room.
    /// </summary>
    public class TransitionRoomResolutionAction : IResolutionAction
    {
        private DungeonRoomDefinition _destination;
        private readonly INameSource _nameSource;

        public TransitionRoomResolutionAction(INameSource nameSource)
        {
            _nameSource = nameSource;
        }

        public IResolutionAction LoadFrom(
            IXmlReaderUtil xmlreaderUtil, 
            XElement element, 
            DungeonDefinition dungeonDefinition)
        {
            _destination = xmlreaderUtil.ObjectReferenceLookup(
                element,
                "room_id",
                dungeonDefinition.RoomDefinitions,
                rt => rt.Id);

            return this;
        }

        public IEnumerable<IUiElement> Apply(
            Random random,
            Cell dungeonLocation,
            GeneratedDungeon dungeon, 
            NotablePerson character, 
            IUnit bodyguard)
        {

            dungeon.CurrentRoom = dungeon.Rooms
                .Where(r => r.Id == _destination.Id)
                .First();

            return Enumerable.Empty<IUiElement>();
        }
    }
}
