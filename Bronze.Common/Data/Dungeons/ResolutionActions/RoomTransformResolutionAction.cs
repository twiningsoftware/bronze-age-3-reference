﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Bronze.Contracts.Data.Dungeons;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;

namespace Bronze.Common.Data.Dungeons.ResolutionActions
{
    /// <summary>
    /// A resolution action that changes the state of the current dungeon room.
    /// </summary>
    public class RoomTransformResolutionAction : IResolutionAction
    {
        private DungeonRoomType _newRoomType;
        
        public IResolutionAction LoadFrom(
            IXmlReaderUtil xmlreaderUtil, 
            XElement element, 
            DungeonDefinition dungeonDefinition)
        {
            _newRoomType = xmlreaderUtil.ObjectReferenceLookup(
                element,
                "room_type_id",
                dungeonDefinition.RoomTypes,
                rt => rt.Id);

            return this;
        }

        public IEnumerable<IUiElement> Apply(
            Random random,
            Cell dungeonLocation,
            GeneratedDungeon dungeon, 
            NotablePerson character, 
            IUnit bodyguard)
        {
            dungeon.CurrentRoom.RoomType = _newRoomType;

            return Enumerable.Empty<IUiElement>();
        }
    }
}
