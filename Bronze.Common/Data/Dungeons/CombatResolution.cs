﻿using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Dungeons;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;
using System.Linq;
using System.Xml.Linq;

namespace Bronze.Common.Data.Dungeons
{
    /// <summary>
    /// A room resolution that has two options, success or failure, based on simulated combat between the
    /// player's bodyguard, and a unit in the room.
    /// </summary>
    public class CombatResolution : IRoomResolution
    {
        public string Text { get; private set; }
        public IUnitType DefendingUnitType { get; private set; }
        public UnitEquipmentSet DefendingUnitEquipment { get; private set; }
        public string BackgroundImage { get; private set; }

        public ResolutionData SuccessResolution { get; private set; }
        public ResolutionData FailureResolution { get; private set; }
        public IfState[] IfStates { get; private set; }

        private readonly IGamedataTracker _gamedataTracker;
        private readonly ICombatAutoresolver _combatAutoresolver;
        private readonly IWorldManager _worldManager;
        
        public CombatResolution(
            IGamedataTracker gamedataTracker,
            ICombatAutoresolver combatAutoresolver,
            IWorldManager worldManager)
        {
            _gamedataTracker = gamedataTracker;
            _combatAutoresolver = combatAutoresolver;
            _worldManager = worldManager;
        }

        public IRoomResolution LoadFrom(
            IInjectionProvider injectionProvider, 
            IXmlReaderUtil xmlreaderUtil, 
            XElement element, 
            DungeonDefinition dungeonDefinition)
        {
            Text = xmlreaderUtil.AttributeValue(element, "text");
            
            DefendingUnitType = xmlreaderUtil.ObjectReferenceLookup(
                element,
                "defender_type",
                _gamedataTracker.UnitTypes,
                ut => ut.Id);

            DefendingUnitEquipment = xmlreaderUtil.ObjectReferenceLookup(
                element,
                "defender_equipment",
                DefendingUnitType.EquipmentSets,
                es => es.Level.ToString());

            BackgroundImage = xmlreaderUtil.AttributeValue(
                element,
                "background_image");
            
            IfStates = element
                .Elements("if_state")
                .Select(e => new IfState
                {
                    State = xmlreaderUtil.AttributeValue(e, "state"),
                    Value = xmlreaderUtil.AttributeValue(e, "value")
                })
                .ToArray();

            SuccessResolution = ResolutionData.LoadFrom(
                injectionProvider,
                xmlreaderUtil, 
                xmlreaderUtil.Element(element, "success"), 
                dungeonDefinition);

            FailureResolution = ResolutionData.LoadFrom(
                injectionProvider,
                xmlreaderUtil, 
                xmlreaderUtil.Element(element, "failure"),
                dungeonDefinition);

            return this;
        }

        public ResolutionData GetResolution(
            Random random,
            Cell dungeonLocation,
            GeneratedDungeon dungeon,
            NotablePerson character,
            IUnit bodyguard)
        {
            var defender = DefendingUnitType.CreateUnit(DefendingUnitEquipment);

            var defenderTribe = new Tribe
            {
                PrimaryColor = BronzeColor.Red,
                SecondaryColor = BronzeColor.Black,
                IsBandits = true
            };

            var defendingArmy = new Army(defenderTribe, dungeonLocation, _worldManager);
            defendingArmy.Units.Add(defender);

            var attackingArmy = new Army(character.Owner, dungeonLocation, _worldManager);
            attackingArmy.Units.Add(bodyguard);
            attackingArmy.General = character;

            var battleResult = _combatAutoresolver.AutoResolveCombat(
                dungeonLocation,
                attackingArmy.Yield(),
                defendingArmy.Yield(),
                null);

            defendingArmy.Cell.WorldActors.RemoveAll(a => a == defendingArmy);
            defendingArmy.Cell.Region.WorldActors.RemoveAll(a => a == defendingArmy);
            attackingArmy.Cell.WorldActors.RemoveAll(a => a == attackingArmy);
            attackingArmy.Cell.Region.WorldActors.RemoveAll(a => a == attackingArmy);

            battleResult.BackgroundImage = BackgroundImage;

            if(battleResult.AttackerWon)
            {
                SuccessResolution.BattleResult = battleResult;

                return SuccessResolution;
            }
            else
            {
                FailureResolution.BattleResult = battleResult;

                return FailureResolution;
            }
        }
    }
}