﻿using Bronze.Contracts.Data.Dungeons;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;
using System.Linq;
using System.Xml.Linq;

namespace Bronze.Common.Data.Dungeons
{
    /// <summary>
    /// A room resolution that automatically succeeds.
    /// </summary>
    public class AutoPassResolution : IRoomResolution
    {
        public string Text { get; private set; }

        public ResolutionData Resolution { get; private set; }
        public IfState[] IfStates { get; private set; }

        public IRoomResolution LoadFrom(IInjectionProvider injectionProvider, IXmlReaderUtil xmlreaderUtil, XElement element, DungeonDefinition dungeonDefinition)
        {
            Text = xmlreaderUtil.AttributeValue(element, "text");

            Resolution = ResolutionData.LoadFrom(injectionProvider, xmlreaderUtil, element, dungeonDefinition);

            IfStates = element
                .Elements("if_state")
                .Select(e => new IfState
                {
                    State = xmlreaderUtil.AttributeValue(e, "state"),
                    Value = xmlreaderUtil.AttributeValue(e, "value")
                })
                .ToArray();

            return this;
        }

        public ResolutionData GetResolution(
            Random random,
            Cell dungeonLocation,
            GeneratedDungeon dungeon,
            NotablePerson character,
            IUnit bodyguard)
        {
            return Resolution;
        }
    }
}