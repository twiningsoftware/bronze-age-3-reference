﻿using Bronze.Common.Data.World.Units;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.Data.World
{
    public class CampedArmy : IEconomicActor
    {
        public Army Army { get; }
        
        public string Id => Army.Id;
        public string TypeId => string.Empty;
        public string Name => Army.Name;
        public WorkerNeed WorkerNeed { get; }
        public int WorkerSupply { get; set; }
        public double WorkerPercent => 1;
        public double UpkeepPercent { get; set; }
        public bool IsRemoved => false;
        public Settlement Settlement => Army.Cell.Region.Settlement;
        public IEnumerable<Trait> NetTraits => Enumerable.Empty<Trait>();
        public bool UnderConstruction { get; set; }
        public double ConstructionProgress { get; set; }
        public double ConstructionMonths => 0.001;
        public bool IsUpgrading => false;
        public double UpgradeProgress { get; set; }
        public double UpgradeMonths => 1;
        public ItemInventory Inventory { get; }
        public int InventoryCapacity => 10;
        public IEnumerable<Recipie> AvailableRecipies => Enumerable.Empty<Recipie>();
        public Recipie ActiveRecipie { get; set; }
        public FactoryState FactoryState { get; set; }
        public List<Item> MissingItems { get; }
        public IntIndexableLookup<Item, double> PerItemSatisfaction { get; }
        public List<DeliveryRoute> DeliveryRoutes { get; private set; }
        public List<IServiceProviderActor> ServiceProviders { get; }
        public IEnumerable<ItemRate> UpgradeInputs => Enumerable.Empty<ItemRate>();
        public IEnumerable<ItemRate> UpkeepInputs => Enumerable.Empty<ItemRate>();
        public bool IsCellLevel => true;
        public double ProductionCheckWait { get; set; }
        public double ItemTransmissionWait { get; set; }
        public double ProductionProgress { get; set; }
        public IntIndexableLookup<Item, bool> ItemsProduced { get; }
        public IntIndexableLookup<Item, bool> ItemsConsumed { get; }
        public IntIndexableLookup<Item, double> IncomingItems { get; }
        public List<LogiHauler> IncomingHaulers { get; }
        public List<LogiHauler> OutgoingHaulers { get; }
        public IEnumerable<Tile> DeliveryArea { get; set; }
        public IntIndexableLookup<BonusType, double> CurrentBonuses { get; }

        private int _unitCount;

        public CampedArmy(Army army)
        {
            Army = army;
            Inventory = new ItemInventory();
            DeliveryRoutes = new List<DeliveryRoute>();
            ServiceProviders = new List<IServiceProviderActor>();
            MissingItems = new List<Item>();
            PerItemSatisfaction = new IntIndexableLookup<Item, double>();
            ItemsProduced = new IntIndexableLookup<Item, bool>();
            ItemsConsumed = new IntIndexableLookup<Item, bool>();
            IncomingHaulers = new List<LogiHauler>();
            OutgoingHaulers = new List<LogiHauler>();
            IncomingItems = new IntIndexableLookup<Item, double>();
            CurrentBonuses = new IntIndexableLookup<BonusType, double>();

            _unitCount = army.Units.Count;

            WorkerNeed = new WorkerNeed
            {
                Caste = army.Owner.Race.Castes.FirstOrDefault(c => c.IsDefault),
                Need = 0
            };

            if(army.CurrentOrder is ArmyCampOrder campOrder)
            {
                campOrder.CampedArmy = this;
            }
        }

        public IEnumerable<ItemRate> NeededInputs
        {
            get
            {
                return Army.Units
                    .SelectMany(u => u.UpkeepNeeds)
                    .GroupBy(ir => ir.Item)
                    .Select(g => new ItemRate(g.Key, g.Sum(x => x.PerMonth)))
                    .ToArray();
            }
        }

        public IEnumerable<ItemRate> ExpectedOutputs
        {
            get
            {
                if (Army.Cell.Owner == Army.Owner)
                {
                    return Army.Units
                        .OfType<IGrazingUnit>()
                        .SelectMany(u => u.GetBonusedGrazingProduction(CurrentBonuses))
                        .GroupBy(ir => ir.Item)
                        .Select(g => new ItemRate(g.Key, g.Sum(x => x.PerMonth)))
                        .ToArray();
                }

                return Enumerable.Empty<ItemRate>();
            }
        }

        public IEnumerable<HaulerInfo> LogiSourceHaulers
        {
            get
            {
                return Army.Units
                    .OfType<IGrazingUnit>()
                    .SelectMany(u => u.LogiSourceHaulers)
                    .Distinct()
                    .ToArray();
            }
        }
        
        public IEnumerable<ItemQuantity> ConstructionCost => Enumerable.Empty<ItemQuantity>();
        public IEnumerable<ItemQuantity> UpgradeCost => Enumerable.Empty<ItemQuantity>();
        
        public IStackUiElement BuildHoverInfoColumn(bool playerOwns, bool playerSees)
        {
            return new StackGroup
            {
                Orientation = Orientation.Vertical
            };
        }

        public Tile[] CalculateLogiDoors(IEnumerable<MovementType> movementTypes)
        {
            var nearestDistrict = Army.Cell.Region?.Settlement?.Districts
                ?.Where(d => d.IsGenerated)
                ?.OrderBy(d => d.Cell.Position.DistanceSq(Army.Cell.Position))
                ?.FirstOrDefault();

            var tiles = new List<Tile>();

            if (nearestDistrict != null)
            {
                var facing = Util.GetFacing(nearestDistrict.Cell.Position.ToVector(), Army.Cell.Position.ToVector());

                for (var i = 0; i < TilePosition.TILES_PER_CELL; i++)
                {
                    for (var j = 0; j < TilePosition.TILES_PER_CELL; j++)
                    {
                        var tile = nearestDistrict.Tiles[i, j];
                        if (tile.GetNeighbor(facing) == null 
                            && movementTypes.Any(mt => tile.Traits.ContainsAll(mt.RequiredTraits))
                            && !movementTypes.Any(mt => tile.Traits.ContainsAny(mt.PreventedBy)))
                        {
                            tiles.Add(tile);
                        }
                    }
                }
            }

            return tiles.ToArray();
        }

        public void DoUpdate(double deltaMonths)
        {
            if (deltaMonths > 0)
            {
                if(_unitCount != Army.Units.Count)
                {
                    _unitCount = Army.Units.Count;

                    if(Settlement != null)
                    {
                        Settlement.SetCalculationFlag("camped army units changed");
                    }
                }

                var supplyRates = new List<double>();

                foreach (var unit in Army.Units)
                {
                    if (unit.CanResupply)
                    {
                        var supplyRate = Util.PullInputs(this, unit.UpkeepNeeds, deltaMonths) * Constants.RESUPPLY_RATE;

                        unit.Supplies = Math.Min(unit.MaxSupplies, unit.Supplies + supplyRate);

                        supplyRates.Add(supplyRate);
                        
                        if (unit.HealthPercent < 1)
                        {
                            unit.Health = unit.MaxHealth * Math.Min(1, unit.HealthPercent + supplyRate * Constants.HEAL_RATE);
                        }
                    }
                }
                
                if(Army.CurrentOrder is ArmyCampOrder campOrder && supplyRates.Any())
                {
                    campOrder.UpkeepPercent = supplyRates.Average() / deltaMonths / Constants.RESUPPLY_RATE;
                }
            }
        }

        public void FinishUpgrading()
        {
        }

        public void OnConstructionFinished()
        {
        }

        public double NeedOf(Item item)
        {
            return InventoryCapacity - Inventory.QuantityOf(item) + IncomingItems[item];
        }

        public void OnProductionStarted()
        {
        }
    }
}
