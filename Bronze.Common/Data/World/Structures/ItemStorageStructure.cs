﻿using Bronze.Common.Data.Game.Structures;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using System.Linq;

namespace Bronze.Common.Data.World.Structures
{
    public class ItemStorageStructure : AbstractStructure<ItemStorageStructureType>, IItemStorageProvider
    {
        private readonly IGamedataTracker _gamedataTracker;
        private IntIndexableLookup<Item, bool> _itemsStored;
        
        public ItemStorageStructure(
            IDialogManager dialogManager,
            IGamedataTracker gamedataTracker)
            : base(dialogManager, gamedataTracker)
        {
            _gamedataTracker = gamedataTracker;
            _itemsStored = new IntIndexableLookup<Item, bool>();
        }
        
        public override AbstractStructure<ItemStorageStructureType> Initialize(Tile ulTile, ItemStorageStructureType baseType)
        {
            base.Initialize(ulTile, baseType);

            foreach (var item in _gamedataTracker.Items)
            {
                _itemsStored[item] = BaseType.ItemsAllowed[item];
            }

            return this;
        }
        
        public override IStackUiElement BuildDetailColumn()
        {
            var stackGroup = base.BuildDetailColumn();

            stackGroup.Children.Add(new Spacer(0, 10));
            stackGroup.Children.Add(new Label("Stored Items"));
            stackGroup.Children.Add(new Spacer(0, 5));

            const int itemsPerRow = 6;

            var knownItems = _gamedataTracker.Items
                .Where(i => Settlement.Owner.KnownItems[i])
                .Where(i => BaseType.ItemsAllowed[i])
                .ToArray();

            for (var i = 0; i < knownItems.Length; i += itemsPerRow)
            {
                stackGroup.Children.Add(new StackGroup
                {
                    Orientation = Orientation.Horizontal,
                    Children = knownItems
                        .Skip(i)
                        .Take(itemsPerRow)
                        .Select(item => new IconButton(
                            item.IconKeyBig,
                            string.Empty,
                            () =>
                            {
                                if (_itemsStored[item])
                                {
                                    _itemsStored[item] = false;
                                }
                                else
                                {
                                    _itemsStored[item] = true;
                                }

                                UlTile.District.Settlement.SetCalculationFlag("storage items changed");
                            },
                            tooltip: item.Name,
                            disableCheck: () => !BaseType.ItemsAllowed[item],
                            highlightCheck: () => _itemsStored[item]))
                        .ToList<IUiElement>()
                });
            }

            return stackGroup;
        }

        public override void SerializeTo(SerializedObject structureRoot)
        {
            base.SerializeTo(structureRoot);

            foreach (var item in _gamedataTracker.Items)
            {
                if (_itemsStored[item])
                {
                    structureRoot.CreateChild("item_stored")
                        .Set("name", item.Name);
                }
            }
        }

        public override void DeserializeFrom(
            IGamedataTracker gamedataTracker,
            Settlement settlement,
            SerializedObject structureRoot)
        {
            base.DeserializeFrom(gamedataTracker, settlement, structureRoot);

            foreach(var item in _gamedataTracker.Items)
            {
                _itemsStored[item] = false;
            }
            
            foreach (var itemRoot in structureRoot.GetChildren("item_stored"))
            {
                var item = itemRoot.GetObjectReference(
                    "name",
                    gamedataTracker.Items,
                    i => i.Name);

                _itemsStored[item] = true;
            }
        }

        public bool CanStore(Item item)
        {
            return _itemsStored[item] && !UnderConstruction;
        }
    }
}
