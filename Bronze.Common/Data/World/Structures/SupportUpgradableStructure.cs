﻿using Bronze.Common.Data.Game.Structures;
using Bronze.Common.UI;
using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.Data.World.Structures
{
    public class SupportUpgradableStructure : AbstractStructure<SupportUpgradableStructureType>
    {
        public SupportUpgradableStructureType UpgradesTo => BaseType.UpgradesTo;

        public double DowngradeTimer { get; set; }
        public bool ServiceProvided { get; set; }

        public SupportUpgradableStructure(IDialogManager dialogManager, IGamedataTracker gamedataTracker)
            : base(dialogManager, gamedataTracker)
        {
        }

        public override void DoUpdate(double deltaMonths)
        {
            base.DoUpdate(deltaMonths);
            
            ServiceProvided = ServiceProviders
                .Where(s => s.ServicePercent > 0)
                .Any(s => s.ServiceType == BaseType.ServiceNeeded);

            if (!ServiceProvided && BaseType.DowngradesTo != null)
            {
                DowngradeTimer += deltaMonths;

                if(DowngradeTimer >= BaseType.DowngradeMonths)
                {
                    ChangeTo(BaseType.DowngradesTo);
                }
            }
            else
            {
                DowngradeTimer = 0;
            }
        }

        public override IStackUiElement BuildHoverInfoColumn(bool playerOwns, bool playerSees)
        {
            var stack = base.BuildHoverInfoColumn(playerOwns, playerSees);

            stack.Children.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new Label("Needs ")
                    {
                        ColorUpdater = () =>
                        {
                            if(ServiceProvided)
                            {
                                return BronzeColor.Green;
                            }
                            else if(BaseType.DowngradesTo != null)
                            {
                                return BronzeColor.Red;
                            }

                            return BronzeColor.Orange;
                        }
                    },
                    UiBuilder.BuildIconFor(BaseType.ServiceNeeded)
                }
            });
            
            return stack;
        }

        public override IStackUiElement BuildDetailColumn()
        {
            var stack = base.BuildDetailColumn();

            stack.Children.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new Label("Needs ")
                    {
                        ColorUpdater = () =>
                        {
                            if(ServiceProvided)
                            {
                                return BronzeColor.Green;
                            }
                            else if(BaseType.DowngradesTo != null)
                            {
                                return BronzeColor.Red;
                            }

                            return BronzeColor.Orange;
                        }
                    },
                    UiBuilder.BuildIconFor(BaseType.ServiceNeeded)
                }
            });

            return stack;
        }

        public void ChangeTo(SupportUpgradableStructureType newType)
        {
            BaseType = newType;

            ConstructionProgress = newType.ConstructionMonths;
            UnderConstruction = false;
            Settlement.SetCalculationFlag("upgradable structure changed");
            CalculateLayers();
        }
    }
}
