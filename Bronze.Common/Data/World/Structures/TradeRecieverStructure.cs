﻿using Bronze.Common.Data.Game.Structures;
using Bronze.Common.UI;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.Trade;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Elements;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.Data.World.Structures
{
    public class TradeRecieverStructure : AbstractStructure<TradeRecieverStructureType>, ITradeReciever
    {
        public IEnumerable<TradeCellExit> CellExits => ToBorderPaths.SelectMany(x => x.Value).Select(x => x.CellExit);
        public List<Tuple<TradeRoute, TraderToDistrictBorderPath>> ActiveToBorderPaths { get; private set; }
        public Dictionary<MovementType, List<TraderToDistrictBorderPath>> ToBorderPaths { get; }

        public List<TradeRoute> TradeRoutes { get; }

        private readonly ITradeManager _tradeManager;

        public TradeRecieverStructure(
            IDialogManager dialogManager,
            ITradeManager tradeManager,
            IGamedataTracker gamedataTracker)
            : base(dialogManager, gamedataTracker)
        {
            _tradeManager = tradeManager;
            ToBorderPaths = new Dictionary<MovementType, List<TraderToDistrictBorderPath>>();
            ActiveToBorderPaths = new List<Tuple<TradeRoute, TraderToDistrictBorderPath>>();
            TradeRoutes = new List<TradeRoute>();
        }
        
        public override IEnumerable<ItemRate> ExpectedOutputs
        {
            get
            {
                if (!UnderConstruction)
                {
                    return base.ExpectedOutputs
                        .Concat(TradeRoutes
                            .Where(t => t.RouteValid)
                            .SelectMany(t => t.Exports))
                            .ToArray();
                }

                return base.ExpectedOutputs;
            }
        }
        
        public void DetermineActiveToBorderPaths()
        {
            ActiveToBorderPaths.Clear();

            foreach (var tradeRoute in TradeRoutes.Where(r => r.RouteValid && r.PathCalculator.Path.Any()))
            { 
                var lastCell = tradeRoute.PathCalculator.Path.Last();
                TraderToDistrictBorderPath toBorderPath = null;

                if (tradeRoute.PathCalculator.Path.Count > 1 && ToBorderPaths.Any())
                {
                    var secondToLastCell = tradeRoute.PathCalculator.Path[tradeRoute.PathCalculator.Path.Count - 2];

                    toBorderPath = ToBorderPaths[tradeRoute.MovementType]
                        .Where(tbp => tbp.CellExit.Cell == secondToLastCell)
                        .Where(tbp => tbp.CellExit.FromCell == lastCell)
                        .FirstOrDefault();

                    if (toBorderPath != null)
                    {
                        ActiveToBorderPaths.Add(Tuple.Create(tradeRoute, toBorderPath));
                    }
                }

                if (toBorderPath == null)
                {
                    toBorderPath = ToBorderPaths[tradeRoute.MovementType]
                        .Where(tbp => (tbp.CellExit.FromCell ?? tbp.CellExit.Cell) == lastCell)
                        .FirstOrDefault();
                    
                    if (toBorderPath != null)
                    {
                        ActiveToBorderPaths.Add(Tuple.Create(tradeRoute, toBorderPath));
                    }
                }
            }
        }
        
        public override IStackUiElement BuildDetailColumn()
        {
            var stackGroup = base.BuildDetailColumn();

            if (!UnderConstruction)
            {
                stackGroup.Children.Add(new Spacer(0, 10));

                if (TradeRoutes.Any())
                {
                    var importSettlements = TradeRoutes
                        .Select(t => t.FromSettlement)
                        .Distinct()
                        .ToArray();

                    if (importSettlements.Length == 1)
                    {
                        stackGroup.Children.Add(new Label($"Importing from {importSettlements.First().Name}"));
                    }
                    else
                    {
                        stackGroup.Children.Add(new Label($"Importing from {importSettlements.Length} settlements"));
                    }

                    stackGroup.Children.Add(UiBuilder.BuildDisplayFor(TradeRoutes.SelectMany(t => t.Exports), 5));
                }
                else
                {
                    stackGroup.Children.Add(new Label($"Not trading"));
                }
                
                if (!ToBorderPaths.Any())
                {
                    stackGroup.Children.Add(new Label($"No path to settlement border", BronzeColor.Yellow));
                }
            }

            return stackGroup;
        }

        public override IStackUiElement BuildHoverInfoColumn(bool playerOwns, bool playerSees)
        {
            var stackGroup = base.BuildHoverInfoColumn(playerOwns, playerSees);

            if (!UnderConstruction && !ToBorderPaths.Any())
            {
                stackGroup.Children.Add(new Label($"No path to settlement border", BronzeColor.Yellow));
            }

            return stackGroup;
        }

        public void OnTradePathChanged()
        {
            DetermineActiveToBorderPaths();
        }

        public override void OnUpgradeFinished(IStructure newStructure)
        {
            base.OnUpgradeFinished(newStructure);
            
            if (newStructure is ITradeReciever newTradeReciever)
            {
                foreach (var tradeRoute in TradeRoutes.ToArray())
                {
                    _tradeManager.ReparentTradeRoute(this, newTradeReciever, tradeRoute);
                }
            }
        }

        public void OnTradeRoutesChanged()
        {
            DetermineActiveToBorderPaths();
        }
    }
}
