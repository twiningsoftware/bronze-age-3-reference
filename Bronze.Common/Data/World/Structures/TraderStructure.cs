﻿using Bronze.Common.Data.Game.Structures;
using Bronze.Common.UI;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.Trade;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Elements;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.Data.World.Structures
{
    public class TraderStructure : AbstractStructure<TraderStructureType>, ITradeActor
    {
        public MovementType TradeType => BaseType.TradeType;
        public double TradeCapacity => BaseType.TradeCapacity;
        public TradeHaulerInfo TradeHauler => BaseType.TradeHauler;
        public IEnumerable<TradeCellExit> CellExits => ToBorderPaths.Select(x => x.CellExit);
        public TraderToDistrictBorderPath ActiveToBorderPath { get; private set; }
        public List<TraderToDistrictBorderPath> ToBorderPaths { get; }

        private TradeRoute _tradeRoute;

        private readonly ITradeManager _tradeManager;

        public TraderStructure(
            IDialogManager dialogManager,
            ITradeManager tradeManager,
            IGamedataTracker gamedataTracker)
            : base(dialogManager, gamedataTracker)
        {
            _tradeManager = tradeManager;
            ToBorderPaths = new List<TraderToDistrictBorderPath>();
        }

        public override IEnumerable<ItemRate> NeededInputs
        {
            get
            {
                if (!UnderConstruction
                    && TradeRoute != null
                    && TradeRoute.FromActor == this
                    && TradeRoute.PathCalculator.PathSuccessful)
                {
                    return base.NeededInputs
                        .Concat(TradeRoute.Exports)
                        .ToArray();
                }

                return base.NeededInputs;
            }
        }

        public override IEnumerable<ItemRate> ExpectedOutputs
        {
            get
            {
                if (!UnderConstruction
                    && TradeRoute != null
                    && TradeRoute.ToActor == this
                    && TradeRoute.PathCalculator.PathSuccessful)
                {
                    return base.ExpectedOutputs
                        .Concat(TradeRoute.Exports)
                        .ToArray();
                }

                return base.ExpectedOutputs;
            }
        }

        public TradeRoute TradeRoute
        {
            get => _tradeRoute;
            set
            {
                _tradeRoute = value;

                DetermineActiveToBorderPath();
            }
        }

        public void DetermineActiveToBorderPath()
        {
            if (TradeRoute != null && TradeRoute.PathCalculator.Path.Any())
            {
                if (TradeRoute.FromActor == this)
                {
                    var firstCell = TradeRoute.PathCalculator.Path.First();

                    if (TradeRoute.PathCalculator.Path.Count > 1)
                    {
                        var secondCell = TradeRoute.PathCalculator.Path[1];

                        var exitUsed = CellExits
                            .Where(ce => ce.Cell == secondCell)
                            .Where(ce => ce.FromCell == firstCell)
                            .FirstOrDefault();

                        if (exitUsed != null)
                        {
                            ActiveToBorderPath = ToBorderPaths
                                .Where(tbp => tbp.CellExit == exitUsed)
                                .FirstOrDefault();
                        }
                    }

                    if (ActiveToBorderPath == null)
                    {
                        ActiveToBorderPath = ToBorderPaths
                            .Where(tbp => (tbp.CellExit.FromCell ?? tbp.CellExit.Cell) == firstCell)
                            .FirstOrDefault();
                    }
                }
                else
                {
                    ActiveToBorderPath = null;

                    var lastCell = TradeRoute.PathCalculator.Path.Last();

                    if (TradeRoute.PathCalculator.Path.Count > 1)
                    {
                        var secondToLastCell = TradeRoute.PathCalculator.Path[TradeRoute.PathCalculator.Path.Count - 2];

                        var exitUsed = CellExits
                            .Where(ce => ce.Cell == secondToLastCell)
                            .Where(ce => ce.FromCell == lastCell)
                            .FirstOrDefault();

                        if (exitUsed != null)
                        {
                            ActiveToBorderPath = ToBorderPaths
                                .Where(tbp => tbp.CellExit == exitUsed)
                                .FirstOrDefault();
                        }
                    }

                    if (ActiveToBorderPath == null)
                    {
                        ActiveToBorderPath = ToBorderPaths
                            .Where(tbp => (tbp.CellExit.FromCell ?? tbp.CellExit.Cell) == lastCell)
                            .FirstOrDefault();
                    }
                }
            }
            else
            {
                ActiveToBorderPath = null;
            }
        }
        
        public override IStackUiElement BuildDetailColumn()
        {
            var stackGroup = base.BuildDetailColumn();

            if (!UnderConstruction)
            {
                stackGroup.Children.Add(new Spacer(0, 10));

                if (TradeRoute != null)
                {
                    if (TradeRoute.FromActor == this)
                    {
                        stackGroup.Children.Add(new Label($"Exporting to {TradeRoute.ToSettlement.Name}"));
                        stackGroup.Children.Add(UiBuilder.BuildDisplayFor(TradeRoute.Exports, 5));
                    }
                    else
                    {
                        stackGroup.Children.Add(new Label($"Importing from {TradeRoute.FromSettlement.Name}"));
                        stackGroup.Children.Add(UiBuilder.BuildDisplayFor(TradeRoute.Exports, 5));
                    }
                }
                else
                {
                    stackGroup.Children.Add(new Label($"Not trading"));
                }

                if (!ToBorderPaths.Any())
                {
                    stackGroup.Children.Add(new Label($"No path to settlement border", BronzeColor.Yellow));
                }
            }

            return stackGroup;
        }

        public override IStackUiElement BuildHoverInfoColumn(bool playerOwns, bool playerSees)
        {
            var stackGroup = base.BuildHoverInfoColumn(playerOwns, playerSees);

            if (!UnderConstruction && !ToBorderPaths.Any())
            {
                stackGroup.Children.Add(new Label($"No path to settlement border", BronzeColor.Yellow));
            }

            return stackGroup;
        }

        public void OnTradePathChanged()
        {
            DetermineActiveToBorderPath();
        }

        public override void OnUpgradeFinished(IStructure newStructure)
        {
            base.OnUpgradeFinished(newStructure);

            if (_tradeRoute != null && newStructure is ITradeActor newTradeActor)
            {
                _tradeManager.ReparentTradeRoute(this, newTradeActor, _tradeRoute);
            }
        }
    }
}
