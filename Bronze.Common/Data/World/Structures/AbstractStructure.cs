﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bronze.Common.UI;
using Bronze.Common.UI.Elements;
using Bronze.Common.UI.Modals;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;

namespace Bronze.Common.Data.World.Structures
{
    public abstract class AbstractStructure<TStructureType> : IStructure where TStructureType : IStructureType
    {
        public string Id { get; private set; }
        public string TypeId => BaseType.Id;
        public Tile UlTile { get; private set; }
        public TilePosition Center { get; private set; }
        public string[] AdjacencyGroups => BaseType.AdjacencyGroups;
        public int TilesWide => BaseType.TilesWide;
        public int TilesHigh => BaseType.TilesHigh;
        public IEnumerable<ITileDisplay> AnimationLayers { get; private set; }
        public IEnumerable<Trait> Traits => BaseType.Traits;
        public IEnumerable<Trait> NotTraits => BaseType.NotTraits;
        public IEnumerable<Tile> Footprint { get; private set; }
        public WorkerNeed WorkerNeed => UnderConstruction ? BaseType.ConstructionWorkers : BaseType.OperationWorkers;
        public int WorkerSupply { get; set; }
        public bool UnderConstruction { get; set; }
        public double ConstructionProgress { get; set; }
        public double ConstructionMonths => BaseType.ConstructionMonths;
        public bool IsUpgrading => UpgradingTo != null;
        public double UpgradeProgress { get; set; }
        public double UpgradeMonths => UpgradingTo?.ConstructionMonths ?? 0;
        public double WorkerPercent => WorkerNeed.Need > 0 ? WorkerSupply / (double)WorkerNeed.Need : 1;
        public FactoryState FactoryState { get; set; }
        public List<Item> MissingItems { get; }
        public IntIndexableLookup<Item, double> PerItemSatisfaction { get; }
        public IEnumerable<ItemRate> UpkeepInputs => BaseType.UpkeepInputs;
        public MinimapColor MinimapColor => BaseType.MinimapColor;
        public double UpkeepPercent { get; set; }
        public bool IsCellLevel => false;
        public double ProductionCheckWait { get; set; }
        public double ItemTransmissionWait { get; set; }
        public double ProductionProgress { get; set; }
        public IntIndexableLookup<Item, bool> ItemsProduced { get; }
        public IntIndexableLookup<Item, bool> ItemsConsumed { get; }
        public IntIndexableLookup<Item, double> IncomingItems { get; }
        public IEnumerable<ItemQuantity> ConstructionCost => BaseType.ConstructionCost;
        public IEnumerable<ItemQuantity> UpgradeCost => UpgradingTo?.ConstructionCost ?? Enumerable.Empty<ItemQuantity>();
        private FactoryState _lastFactoryState;
        public IEnumerable<Tile> DeliveryArea { get; set; }
        public IntIndexableLookup<BonusType, double> CurrentBonuses { get; }

        public string Name => BaseType.Name;

        public bool IsRemoved { get; set; }

        public IEnumerable<Recipie> AvailableRecipies => BaseType.Recipies;

        public List<DeliveryRoute> DeliveryRoutes { get; private set; }

        public ItemInventory Inventory { get; private set; }

        public int InventoryCapacity => BaseType.InventoryCapacity;

        public double LogiDistance => BaseType.LogiDistance;

        public IEnumerable<HaulerInfo> LogiSourceHaulers => BaseType.LogiSourceHaulers;

        public virtual double Prosperity => 0;

        public Settlement Settlement => UlTile.District.Settlement;

        public IEnumerable<IStructureType> ManuallyUpgradesTo => BaseType.ManuallyUpgradesTo;
        public bool CanManuallyUpgrade => !UnderConstruction && !IsUpgrading && ManuallyUpgradesTo.Any(u => u.CanBePlaced(UlTile));
        public bool CanManuallyDowngrade => !UnderConstruction && !IsUpgrading && ManuallyDowngradesTo != null;
        public IStructureType ManuallyDowngradesTo => BaseType.ManuallyDowngradesTo;
        public IStructureType UpgradingTo { get; private set; }
        
        public List<IServiceProviderActor> ServiceProviders { get; }
        public List<LogiHauler> IncomingHaulers { get; }
        public List<LogiHauler> OutgoingHaulers { get; }

        private TStructureType _baseType;
        private Recipie _recipie;

        private readonly IDialogManager _dialogManager;
        private readonly IGamedataTracker _gamedataTracker;

        protected AbstractStructure(
            IDialogManager dialogManager,
            IGamedataTracker gamedataTracker)
        {
            _dialogManager = dialogManager;
            _gamedataTracker = gamedataTracker;
            Id = Guid.NewGuid().ToString();
            UpkeepPercent = 1;
            ServiceProviders = new List<IServiceProviderActor>();
            MissingItems = new List<Item>();
            PerItemSatisfaction = new IntIndexableLookup<Item, double>();
            ItemsProduced = new IntIndexableLookup<Item, bool>();
            ItemsConsumed = new IntIndexableLookup<Item, bool>();
            IncomingItems = new IntIndexableLookup<Item, double>();
            IncomingHaulers = new List<LogiHauler>();
            OutgoingHaulers = new List<LogiHauler>();
            DeliveryArea = Enumerable.Empty<Tile>();
            CurrentBonuses = new IntIndexableLookup<BonusType, double>();
        }

        protected TStructureType BaseType
        {
            get => _baseType;
            set
            {
                Initialize(UlTile, value);
            }
        }

        public Recipie ActiveRecipie
        {
            get => _recipie;
            set
            {
                if (_recipie != value)
                {
                    ProductionCheckWait = 0;
                    ProductionProgress = 0;
                }

                _recipie = value;
                if (UlTile?.District?.Settlement != null)
                {
                    UlTile.District.Settlement.SetCalculationFlag("recipie changed");
                }

                CalculateLayers();
            }
        }

        public virtual IEnumerable<ItemRate> NeededInputs
        {
            get
            {
                IEnumerable<ItemRate> inputs;

                if (!UnderConstruction)
                {
                    inputs = UpkeepInputs;

                    if (ActiveRecipie != null)
                    {
                        inputs = inputs.Concat(ActiveRecipie.Inputs);
                    }
                }
                else
                {
                        inputs = Enumerable.Empty<ItemRate>();
                }

                return inputs;
            }
        }

        public virtual IEnumerable<ItemRate> ExpectedOutputs
        {
            get
            {
                if (!UnderConstruction && ActiveRecipie != null)
                {
                    return ActiveRecipie.BoostedOutputs(this, UlTile.Traits)
                        .Select(ir => new ItemRate(ir.Item, ir.PerMonth * WorkerPercent))
                        .ToArray();
                }

                return Enumerable.Empty<ItemRate>();
            }
        }

        public IEnumerable<Trait> NetTraits => UlTile.Traits;
        
        public virtual AbstractStructure<TStructureType> Initialize(Tile ulTile, TStructureType baseType)
        {
            UlTile = ulTile;
            Center = ulTile.Position.Relative(
                (int)(baseType.TilesWide / 2f - 0.5f),
                (int)(baseType.TilesHigh / 2f - 0.5f));
            _baseType = baseType;
            UnderConstruction = true;
            Inventory = new ItemInventory();
            DeliveryRoutes = new List<DeliveryRoute>();
            
            Footprint = Util.CalculateFootprint(ulTile, baseType).ToArray();

            foreach(var tile in Footprint)
            {
                tile.Structure = this;
                tile.CalculateTraits();
            }

            ActiveRecipie = AvailableRecipies
                .Where(r => r.IsValid(this))
                .FirstOrDefault();

            if (UlTile?.District?.Settlement != null)
            {
                UlTile.District.Settlement.SetCalculationFlag("structure added");
            }

            CalculateLayers();

            return this;
        }

        public virtual void OnConstructionFinished()
        {
            CalculateLayers();
        }

        public virtual void OnProductionStarted()
        {
            CalculateLayers();
        }

        public virtual void OnUpgradeFinished(IStructure newStructure)
        {
        }

        public virtual void DoUpdate(double deltaMonths)
        {
            if(FactoryState != _lastFactoryState)
            {
                _lastFactoryState = FactoryState;
                CalculateLayers();
            }
        }

        public Tile[] CalculateLogiDoors(IEnumerable<MovementType> movementTypes)
        {
            return Footprint.ToArray();
        }
        
        public virtual IStackUiElement BuildDetailColumn()
        {
            var stack = new StackGroup
            {
                Orientation = Orientation.Vertical,
                Width = Width.Fixed(250),
                Children = new List<IUiElement>
                {
                    new Label(Name),
                    new Spacer(0, 5),
                    new TraitDisplay(() => Traits),
                    new Spacer(0, 10)
                }
            };

            if (UnderConstruction)
            {
                if (ConstructionMonths > 0)
                {
                    stack.Children.Add(new Label($"Under Construction: {(int)(ConstructionProgress / ConstructionMonths * 100)}%"));
                }
                else
                {
                    stack.Children.Add(new Label($"Under Construction"));
                }

                if (NeededInputs.Any())
                {
                    stack.Children.Add(new Spacer(0, 5));
                    stack.Children.Add(UiBuilder.BuildDisplayFor(NeededInputs.GroupBy(ir => ir.Item).Select(g => new ItemRate(g.Key, g.Sum(x => x.PerMonth * -1)))));
                }

                stack.Children.Add(new Spacer(0, 5));
                stack.Children.Add(new TextButton(
                    "Cancel",
                    () =>
                    {
                        UlTile.District.RemoveStructure(this);
                        _dialogManager.PopModal();
                    },
                    KeyCode.C));
            }

            if (!UnderConstruction && !IsUpgrading)
            {
                if (ManuallyUpgradesTo.Any())
                {
                    stack.Children.Add(new TextButton(
                        "Upgrade",
                        () =>
                        {
                            _dialogManager.PushModal<ConstructionInfoModal, ConstructionInfoModalContext>(
                                new ConstructionInfoModalContext
                                {
                                    Tile = UlTile,
                                    StructureTypes = ManuallyUpgradesTo
                                });
                        },
                        KeyCode.U,
                        disableCheck: () => !CanManuallyUpgrade));
                }

                if (ManuallyDowngradesTo != null)
                {
                    stack.Children.Add(new TextButton(
                        "Downgrade",
                        () =>
                        {
                            _dialogManager.PushModal<ConstructionInfoModal, ConstructionInfoModalContext>(
                                new ConstructionInfoModalContext
                                {
                                    Tile = UlTile,
                                    StructureTypes = ManuallyDowngradesTo.Yield()
                                });
                        },
                        KeyCode.D,
                        disableCheck: () => !CanManuallyDowngrade));
                }
            }
            else if (IsUpgrading)
            {
                if (UpgradingTo.ConstructionMonths > 0)
                {
                    stack.Children.Add(new Label("")
                    {
                        ContentUpdater = () => $"Upgrading {(int)(100 * UpgradeProgress / UpgradingTo.ConstructionMonths)}%"
                    });
                }
                else
                {
                    stack.Children.Add(new Label("Upgrading"));
                }

                stack.Children.Add(new Spacer(0, 5));
                stack.Children.Add(new TextButton(
                    "Cancel",
                    () =>
                    {
                        CancelUpgrading();
                        _dialogManager.PopModal();
                    },
                    KeyCode.C));
            }

            if (UpkeepInputs.Any())
            {
                stack.Children.Add(new Spacer(0, 10));
                stack.Children.Add(UiBuilder.BuildUpkeepRow(UpkeepInputs));
            }

            if (!UnderConstruction && AvailableRecipies.Any())
            {
                stack.Children.Add(new Spacer(0, 10));
                stack.Children.Add(new Label($"   Production"));
                if (ActiveRecipie != null)
                {
                    stack.Children.Add(new Label($"{ActiveRecipie.Verb}: {ProductionProgress.ToString("P0")}"));
                    stack.Children.Add(UiBuilder.BuildDisplayFor(
                        ActiveRecipie.Inputs.Select(y => new ItemRate(y.Item, -1 * y.PerMonth)).Concat(ActiveRecipie.Outputs)));
                    stack.Children.Add(new Spacer(0, 5));
                    stack.Children.Add(UiBuilder.BuildFactoryStateDisplay(_gamedataTracker, this));
                    stack.Children.Add(new Spacer(0, 5));
                }
                stack.Children.Add(UiBuilder.BuildRecipieButtonRow(AvailableRecipies, this));
                stack.Children.Add(new Spacer(0, 5));
            }

            if (WorkerNeed.Need > 0)
            {
                stack.Children.Add(new IconText(WorkerNeed.Caste.IconKey, $"Workers {WorkerSupply} / {WorkerNeed.Need}", tooltip: $"Supply of needed {WorkerNeed.Caste.Name} workers."));
            }

            if (Inventory.ItemsPresent.Any())
            {
                stack.Children.Add(new Label($"   Inventory"));
                stack.Children.Add(new InventoryDisplay(Inventory, 6));
            }

            if(Prosperity > 0)
            {
                stack.Children.Add(new Spacer(0, 10));
                stack.Children.Add(new IconText(UiImages.SMALL_PROSPERITY, Prosperity.ToString("F"), tooltip: "Generated prosperity."));
            }

            if (BaseType.LogiSourceHaulers.Any())
            {
                stack.Children.Add(new Spacer(0, 10));
                stack.Children.AddRange(
                    BaseType.LogiSourceHaulers
                        .Select(x => UiBuilder.BuildHaulerinfoPanel(x)));
            }

            return stack;
        }

        public virtual IStackUiElement BuildHoverInfoColumn(bool playerOwns, bool playerSees)
        {
            return new StackGroup
            {
                Orientation = Orientation.Vertical
            };
        }

        public virtual void OnRemoval()
        {
            IsRemoved = true;

            foreach(var tile in Footprint)
            {
                tile.CalculateTraits();
            }
        }

        public virtual void SerializeTo(SerializedObject structureRoot)
        {
            structureRoot.Set("id", Id);
            UlTile.Position.SerializeTo(structureRoot.CreateChild("ultile_position"));
            structureRoot.Set("type_id", BaseType.Id);
            structureRoot.Set("worker_supply", WorkerSupply);
            structureRoot.Set("under_construction", UnderConstruction);
            structureRoot.Set("construction_progress", ConstructionProgress);
            structureRoot.Set("active_recipie_id", ActiveRecipie?.Id);
            structureRoot.Set("upgrade_progress", UpgradeProgress);
            structureRoot.Set("upgrading_to_id", UpgradingTo?.Id);
            Inventory.SerializeTo(structureRoot.CreateChild("inventory"));
            structureRoot.Set("production_check_wait", ProductionCheckWait);
            structureRoot.Set("item_transmission_wait", ItemTransmissionWait);
            structureRoot.Set("production_progress", ProductionProgress);
        }

        public virtual void DeserializeFrom(
            IGamedataTracker gamedataTracker,
            Settlement settlement,
            SerializedObject structureRoot)
        {
            Id = structureRoot.GetString("id");
            WorkerSupply = structureRoot.GetInt("worker_supply");
            UnderConstruction = structureRoot.GetBool("under_construction");
            ConstructionProgress = structureRoot.GetDouble("construction_progress");
            var activeRecipieId = structureRoot.GetOptionalString("active_recipie_id");
            ActiveRecipie = AvailableRecipies
                .Where(r => r.Id == activeRecipieId)
                .FirstOrDefault();
            Inventory.DeserializeFrom(gamedataTracker, structureRoot.GetChild("inventory"));

            UpgradeProgress = structureRoot.GetOptionalDouble("upgrade_progress");
            UpgradingTo = structureRoot.GetOptionalObjectReference(
                "upgrading_to_id",
                gamedataTracker.StructureTypes,
                cd => cd.Id);

            ProductionCheckWait = structureRoot.GetOptionalDouble("production_check_wait");
            ItemTransmissionWait = structureRoot.GetOptionalDouble("item_transmission_wait");
            ProductionProgress = structureRoot.GetOptionalDouble("production_progress");

            foreach (var tile in Footprint)
            {
                tile.CalculateTraits();
            }
        }

        public override string ToString()
        {
            return Name + " @ " + UlTile?.Position;
        }

        public void FinishUpgrading()
        {
            if (UpgradingTo != null)
            {
                UpgradingTo.Place(UlTile);

                var newStructure = UlTile.Structure;

                newStructure.ConstructionProgress = newStructure.ConstructionMonths;
                newStructure.UnderConstruction = false;
                Inventory.TransferTo(newStructure.Inventory);

                if (ActiveRecipie == null
                    && newStructure.AvailableRecipies.Contains(ActiveRecipie))
                {
                    newStructure.ActiveRecipie = ActiveRecipie;
                }

                OnUpgradeFinished(newStructure);
                newStructure.OnConstructionFinished();
            }

            UpgradingTo = null;
            UpgradeProgress = 0;
        }

        public void UpgradeTo(IStructureType structureType)
        {
            UpgradingTo = structureType;
            UpgradeProgress = 0;
            Settlement.SetCalculationFlag("upgrade start");
        }

        public void CancelUpgrading()
        {
            UpgradingTo = null;
            UpgradeProgress = 0;
            Settlement.SetCalculationFlag("upgrade cancel");
        }

        protected virtual IEnumerable<string> GetAnimationNames()
        {
            var animationNames = Constants.IDLE_ANIMATIONS;

            if (ActiveRecipie != null && ProductionProgress > 0)
            {
                animationNames = ActiveRecipie.AnimationNames;
            }

            return animationNames;
        }

        public void CalculateLayers()
        {
            var animationNames = GetAnimationNames();
            
            AnimationLayers = (UnderConstruction ? BaseType.ConstructionAnimationLayers : BaseType.AnimationLayers)
                .Where(l => string.IsNullOrWhiteSpace(l.AnimationName) || animationNames.Contains(l.AnimationName))
                .ToArray();
        }

        public double NeedOf(Item item)
        {
            var supply = Inventory.QuantityOf(item) + IncomingItems[item];

            if (UnderConstruction)
            {
                if (ConstructionProgress <= 0)
                {
                    return BaseType.ConstructionCostLookup[item] - supply;
                }

                return 0;
            }
            else if (IsUpgrading && UpgradeCost.Any(iq => iq.Item == item))
            {
                if (UpgradeProgress <= 0)
                {
                    return UpgradingTo.ConstructionCostLookup[item] - supply;
                }

                return 0;
            }
            else
            {
                var capacity = InventoryCapacity;

                return capacity - supply;
            }
        }
    }
}