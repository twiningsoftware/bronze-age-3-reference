﻿using Bronze.Common.Data.Game.Structures;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.Data.World.Structures
{
    public class DistributorStructure : AbstractServiceProviderStructure<DistributorStructureType>
    {
        private readonly IGamedataTracker _gamedataTracker;
        private List<Item> _itemsDistributed;
        private ItemRate[] _itemNeeds;

        private double[] _itemNeedLookup;
        
        public DistributorStructure(
            IDialogManager dialogManager,
            IGamedataTracker gamedataTracker)
            : base(dialogManager, gamedataTracker)
        {
            _gamedataTracker = gamedataTracker;
            _itemsDistributed = new List<Item>();
            _itemNeeds = new ItemRate[0];
            _itemNeedLookup = new double[_gamedataTracker.Items.Max(i => i.LookupIndex) + 1];
        }

        public override IEnumerable<ItemRate> NeededInputs
        {
            get
            {
                if (!UnderConstruction)
                {
                    return base.NeededInputs.Concat(_itemNeeds);
                }

                return base.NeededInputs;
            }
        }
        
        public override AbstractStructure<DistributorStructureType> Initialize(Tile ulTile, DistributorStructureType baseType)
        {
            base.Initialize(ulTile, baseType);

            _itemsDistributed = baseType.InitialDistribution
                .Take(baseType.DistributionMaxItems)
                .ToList();
            
            return this;
        }

        public override void DoUpdate(double deltaMonths)
        {
            base.DoUpdate(deltaMonths);
            
            var newNeedsLookup = new double[_itemNeedLookup.Length];

            foreach(var item in _itemsDistributed)
            {
                foreach (var house in ActorsServed.OfType<IHousingProvider>())
                {
                    newNeedsLookup[item.LookupIndex] += house.NeedPerMonthOf(item);
                }
            }

            var needsChanged = false;

            for(var i = 0; i < _itemNeedLookup.Length && !needsChanged; i++)
            {
                needsChanged = needsChanged || _itemNeedLookup[i] != newNeedsLookup[i];
            }

            if (needsChanged)
            {
                _itemNeedLookup = newNeedsLookup;

                _itemNeeds = _itemsDistributed
                    .Where(i => newNeedsLookup[i.LookupIndex] > 0)
                    .Select(i => new ItemRate(i, newNeedsLookup[i.LookupIndex]))
                    .ToArray();

                UlTile.District.Settlement.SetCalculationFlag("distributor needs changed");
            }
        }

        public override IStackUiElement BuildDetailColumn()
        {
            var stackGroup = base.BuildDetailColumn();

            stackGroup.Children.Add(new Spacer(0, 10));
            stackGroup.Children.Add(new Label("Distributed Items"));
            stackGroup.Children.Add(new Spacer(0, 5));

            const int itemsPerRow = 6;

            var knownItems = _gamedataTracker.Items
                .Where(i => Settlement.Owner.KnownItems[i])
                .ToArray();

            for(var i = 0; i < knownItems.Length; i += itemsPerRow)
            {
                stackGroup.Children.Add(new StackGroup
                {
                    Orientation = Orientation.Horizontal,
                    Children = knownItems
                        .Skip(i)
                        .Take(itemsPerRow)
                        .Select(item => new IconButton(
                            item.IconKeyBig,
                            string.Empty,
                            () =>
                            {
                                if (_itemsDistributed.Contains(item))
                                {
                                    _itemsDistributed.Remove(item);
                                }
                                else
                                {
                                    _itemsDistributed.Add(item);
                                }

                                UlTile.District.Settlement.SetCalculationFlag("distributor items changed");
                            },
                            tooltip: item.Name,
                            disableCheck: () => !_itemsDistributed.Contains(item) && _itemsDistributed.Count >= BaseType.DistributionMaxItems,
                            highlightCheck: () => _itemsDistributed.Contains(item)))
                        .ToList<IUiElement>()
                });
            }

            return stackGroup;
        }

        public override void SerializeTo(SerializedObject structureRoot)
        {
            base.SerializeTo(structureRoot);

            foreach(var item in _itemsDistributed)
            {
                structureRoot.CreateChild("item_distributed")
                    .Set("name", item.Name);
            }
        }

        public override void DeserializeFrom(
            IGamedataTracker gamedataTracker, 
            Settlement settlement,
            SerializedObject structureRoot)
        {
            base.DeserializeFrom(gamedataTracker, settlement, structureRoot);
            _itemsDistributed.Clear();

            foreach (var itemRoot in structureRoot.GetChildren("item_distributed"))
            {
                _itemsDistributed.Add(itemRoot.GetObjectReference(
                    "name",
                    gamedataTracker.Items,
                    i => i.Name));
            }
        }
        
        public override void AddServicedActor(IEconomicActor actorToService)
        {
            base.AddServicedActor(actorToService);

            if (actorToService is IHousingProvider house)
            {
                house.Distributor = this;
            }
        }

        public override void ClearServicedActors()
        {
            foreach (var house in ActorsServed.OfType<IHousingProvider>())
            {
                house.Distributor = null;
            }

            base.ClearServicedActors();
        }
    }
}
