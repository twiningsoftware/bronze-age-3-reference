﻿using Bronze.Common.Data.Game.Structures;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.Data.World.Structures
{
    public class TaxCollectorStructure : AbstractServiceProviderStructure<TaxCollectorStructureType>
    {
        public double ProductionRate { get; set; }
        
        public TaxCollectorStructure(
            IDialogManager dialogManager,
            IGamedataTracker gamedataTracker)
            : base(dialogManager, gamedataTracker)
        {
        }

        public override IEnumerable<ItemRate> ExpectedOutputs
        {
            get
            {
                if(!UnderConstruction)
                {
                    return new ItemRate(BaseType.ProducedItem, ProductionRate).Yield();
                }
                else
                {
                    return base.ExpectedOutputs;
                }
            }
        }
        
        public override void DoUpdate(double deltaMonths)
        {
            base.DoUpdate(deltaMonths);

            ProductionRate = ActorsServed.OfType<IStructure>().Sum(s => s.Prosperity) * UlTile.District.Settlement.TaxRate * WorkerPercent;

            var qtyToAdd = Math.Min(
                InventoryCapacity - Inventory.QuantityOf(BaseType.ProducedItem),
                deltaMonths * ProductionRate);

            Inventory.Add(BaseType.ProducedItem, qtyToAdd);
            Settlement.LastMonthProduction[BaseType.ProducedItem] += deltaMonths * ProductionRate;

            foreach (var house in ActorsServed.OfType<IHousingProvider>())
            {
                house.EffectiveTaxRate = UlTile.District.Settlement.TaxRate;
            }
        }
        
        public override IStackUiElement BuildDetailColumn()
        {
            var stackGroup = base.BuildDetailColumn();

            stackGroup.Children.Add(new Spacer(0, 10));
            stackGroup.Children.Add(new Label("Tax Collection"));
            stackGroup.Children.Add(new Spacer(0, 5));
            stackGroup.Children.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new IconText(
                        string.Empty,
                        (UlTile.District.Settlement.TaxRate * 100) + "%",
                        tooltip: "Settlement tax rate."),
                    new Spacer(10, 0),
                    new IconText(
                        UiImages.TINY_POP_HOUSING,
                        ActorsServed.Count.ToString(),
                        tooltip: "Collecting taxes from " +  ActorsServed.Count + " structures."),
                    new Spacer(10, 0),
                    new IconText(
                        BaseType.ProducedItem.IconKey,
                        "+" + Math.Round(ProductionRate, 1).ToString(),
                        BronzeColor.Green,
                        tooltip: BaseType.ProducedItem.Name + " produced per month."),
                }
            });
            
            return stackGroup;
        }
        
        public override void ClearServicedActors()
        {
            foreach (var house in ActorsServed.OfType<IHousingProvider>())
            {
                house.EffectiveTaxRate = 0;
            }

            base.ClearServicedActors();
        }
    }
}
