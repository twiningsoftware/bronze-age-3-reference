﻿using Bronze.Common.Data.Game.Structures;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.Data.World.Structures
{
    public class TempleStructure : AbstractServiceProviderStructure<TempleStructureType>, ICultInfluenceServiceProvider, IAuthorityProvider
    {
        public Cult Cult => BaseType.Cult;
        public double InfluencePerPop => BaseType.InfluencePerPop;
        public int AuthorityProvided => BaseType.BonusAuthority;

        public TempleStructure(
            IDialogManager dialogManager,
            IGamedataTracker gamedataTracker)
            : base(dialogManager, gamedataTracker)
        {
        }
    }
}
