﻿using Bronze.Common.Data.Game.Structures;
using Bronze.Common.UI;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using System;
using System.Collections.Generic;

namespace Bronze.Common.Data.World.Structures
{
    public abstract class AbstractServiceProviderStructure<TStructureType>
        : AbstractStructure<TStructureType>, IServiceProviderStructure where TStructureType : AbstractServiceProviderStructureType
    {
        public List<Tile> TilesServed { get; }
        public ServiceType ServiceType => BaseType.ServiceType;
        public List<IEconomicActor> ActorsServed { get; }
        public double ServiceRange => BaseType.ServiceRange;
        public double ServicePercent => Math.Min(UpkeepPercent, WorkerPercent);
        public IEnumerable<Trait> ServiceTransmissionTraits => BaseType.ServiceTransmissionTraits;

        protected AbstractServiceProviderStructure(
            IDialogManager dialogManager,
            IGamedataTracker gamedataTracker)
            : base(dialogManager, gamedataTracker)
        {
            TilesServed = new List<Tile>();
            ActorsServed = new List<IEconomicActor>();
        }

        public virtual IEnumerable<Tile> CalculateServiceArea()
        {
            return BaseType.CalculateServiceArea(UlTile);
        }
        
        public virtual void AddServicedActor(IEconomicActor actorToService)
        {
            actorToService.ServiceProviders.Add(this);
            ActorsServed.Add(actorToService);
        }
        
        public virtual bool CanService(IEconomicActor economicActor)
        {
            return BaseType.CanService(economicActor);
        }

        public virtual bool CanService(IEconomicActorType economicActorType)
        {
            return BaseType.CanService(economicActorType);
        }

        public virtual void ClearServicedActors()
        {
            foreach(var actor in ActorsServed)
            {
                actor.ServiceProviders.RemoveAll(s => s == this);
            }
            ActorsServed.Clear();
        }

        public override void OnRemoval()
        {
            base.OnRemoval();
            ClearServicedActors();
        }

        public override IStackUiElement BuildDetailColumn()
        {
            var stack = base.BuildDetailColumn();
            stack.Children.Add(UiBuilder.BuildServiceProvidedDetailRow(ServiceType, ServiceRange, () => ServicePercent));
            return stack;
        }

        public override IStackUiElement BuildHoverInfoColumn(bool playerOwns, bool playerSees)
        {
            var stack = base.BuildHoverInfoColumn(playerOwns, playerSees);
            if (playerOwns)
            {
                stack.Children.Add(UiBuilder.BuildServiceProvidedSummaryRow(ServiceType, () => ServicePercent));
            }
            return stack;
        }
    }
}
