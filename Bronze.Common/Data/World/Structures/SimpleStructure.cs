﻿using Bronze.Common.Data.Game.Structures;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.Data.World.Structures
{
    public class SimpleStructure : AbstractStructure<SimpleStructureType>
    {
        public SimpleStructure(IDialogManager dialogManager, IGamedataTracker gamedataTracker)
            : base(dialogManager, gamedataTracker)
        {
        }
    }
}
