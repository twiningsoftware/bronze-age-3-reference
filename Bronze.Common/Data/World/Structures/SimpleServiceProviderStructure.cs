﻿using Bronze.Common.Data.Game.Structures;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.Data.World.Structures
{
    public class SimpleServiceProviderStructure : AbstractServiceProviderStructure<SimpleServiceProviderStructureType>
    {
        public SimpleServiceProviderStructure(
            IDialogManager dialogManager,
            IGamedataTracker gamedataTracker)
            : base(dialogManager, gamedataTracker)
        {
        }
    }
}
