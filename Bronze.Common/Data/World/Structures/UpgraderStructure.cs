﻿using Bronze.Common.Data.Game.Structures;
using Bronze.Contracts;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Elements;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.Data.World.Structures
{
    public class UpgraderStructure : AbstractServiceProviderStructure<UpgraderStructureType>
    {
        public double UpgradeCounter { get; set; }
        public SupportUpgradableStructure StructureBeingUpgraded { get; set; }
        
        public UpgraderStructure(
            IDialogManager dialogManager,
            IGamedataTracker gamedataTracker)
            : base(dialogManager, gamedataTracker)
        {
        }

        public override IStackUiElement BuildDetailColumn()
        {
            var stackGroup = base.BuildDetailColumn();

            stackGroup.Children.Add(new Spacer(0, 10));

            stackGroup.Children.Add(new Label("Upgrading")
            {
                ContentUpdater = () =>
                {
                    if (StructureBeingUpgraded != null)
                    {
                        return $"Upgrading {StructureBeingUpgraded.Name} {(int)(100 * UpgradeCounter / BaseType.MonthsToUpgrade)}%";
                    }

                    return string.Empty;
                }
            });

            return stackGroup;
        }

        public override IStackUiElement BuildHoverInfoColumn(bool playerOwns, bool playerSees)
        {
            var stackGroup = base.BuildHoverInfoColumn(playerOwns, playerSees);

            if (playerOwns)
            {
                stackGroup.Children.Add(new Label("Upgrading")
                {
                    ContentUpdater = () =>
                    {
                        if (StructureBeingUpgraded != null)
                        {
                            return $"Upgrading {StructureBeingUpgraded.Name} {(int)(100 * UpgradeCounter / BaseType.MonthsToUpgrade)}%";
                        }

                        return string.Empty;
                    }
                });
            }

            return stackGroup;
        }

        public override void DoUpdate(double deltaMonths)
        {
            base.DoUpdate(deltaMonths);

            var wasUpgrading = StructureBeingUpgraded != null;

            if (!UnderConstruction)
            {
                StructureBeingUpgraded = ActorsServed
                    .OfType<SupportUpgradableStructure>()
                    .Where(s => s.UpgradesTo != null)
                    .FirstOrDefault();

                if(wasUpgrading != (StructureBeingUpgraded != null))
                {
                    CalculateLayers();
                }

                if (StructureBeingUpgraded != null)
                {
                    UpgradeCounter += deltaMonths * ServicePercent;

                    if (UpgradeCounter >= BaseType.MonthsToUpgrade)
                    {
                        UpgradeCounter = 0;
                        StructureBeingUpgraded.ChangeTo(StructureBeingUpgraded.UpgradesTo);
                    }
                }
                else
                {
                    UpgradeCounter = 0;
                }
            }
            else
            {
                StructureBeingUpgraded = null;
                UpgradeCounter = 0;
            }
        }

        public override void AddServicedActor(IEconomicActor actorToService)
        {
            base.AddServicedActor(actorToService);

            var sorted = ActorsServed.OfType<IStructure>()
                .OrderBy(x => x.Center.DistanceSq(Center))
                .ToArray();

            ActorsServed.Clear();
            ActorsServed.AddRange(sorted);
        }

        public override void SerializeTo(SerializedObject structureRoot)
        {
            base.SerializeTo(structureRoot);

            structureRoot.Set("upgrade_counter", UpgradeCounter);
        }

        public override void DeserializeFrom(IGamedataTracker gamedataTracker, Settlement settlement, SerializedObject structureRoot)
        {
            base.DeserializeFrom(gamedataTracker, settlement, structureRoot);

            UpgradeCounter = structureRoot.GetOptionalDouble("upgrade_counter");
        }

        protected override IEnumerable<string> GetAnimationNames()
        {
            var names = base.GetAnimationNames();

            if(StructureBeingUpgraded != null)
            {
                names = names.Concat("upgrading".Yield());
            }

            return names;
        }
    }
}
