﻿using Bronze.Common.Data.Game.Structures;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Elements;
using System.Linq;

namespace Bronze.Common.Data.World.Structures
{
    public class AdministratorStructure : AbstractServiceProviderStructure<AdministratorStructureType>, IAuthorityProvider
    {
        private int _populationServed;

        public int AuthorityProvided => (int)(_populationServed * BaseType.AuthorityPerPop * UpkeepPercent);

        public AdministratorStructure(
            IDialogManager dialogManager,
            IGamedataTracker gamedataTracker)
            : base(dialogManager, gamedataTracker)
        {
        }
        
        public override IStackUiElement BuildDetailColumn()
        {
            var stackGroup = base.BuildDetailColumn();

            stackGroup.Children.Add(new Spacer(0, 10));
            stackGroup.Children.Add(new Label("Authority"));
            stackGroup.Children.Add(new Label($"Produces {AuthorityProvided} authority."));
            
            return stackGroup;
        }

        public override void AddServicedActor(IEconomicActor actorToService)
        {
            base.AddServicedActor(actorToService);

            _populationServed = ActorsServed
                .OfType<IHousingProvider>()
                .Select(h => h.Residents.Count)
                .Sum();
        }
    }
}
