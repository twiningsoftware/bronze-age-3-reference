﻿using Bronze.Common.Data.Game;
using Bronze.Common.Data.Game.Structures;
using Bronze.Common.UI;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.Data.World.Structures
{
    public class HousingStructure : AbstractStructure<HousingStructureType>, IUpgradableHousing
    {
        public Caste SupportedCaste => BaseType.SupportedCaste;
        public List<Pop> Residents { get; }
        public int ProvidedHousing => BaseType.SupportedPops;
        public IEconomicActor Distributor { get; set; }
        public IEnumerable<PopNeedState> PopNeeds { get; private set; }
        public override double Prosperity => _prosperity;
        public double EffectiveTaxRate { get; set; }
        public double AverageSatisfaction { get; private set; }
        public IntIndexableLookup<Cult, double> InfluencePerPop { get; private set; }
        private readonly IGamedataTracker _gamedataTracker;

        private double[] _itemNeedsPerPopPerMonth;

        private double _upgradeCounter;
        private double _prosperity;

        public HousingStructure(
            IDialogManager dialogManager,
            IGamedataTracker gamedataTracker)
            : base(dialogManager, gamedataTracker)
        {
            Residents = new List<Pop>();
            PopNeeds = new PopNeedState[0];
            AverageSatisfaction = 1;
            InfluencePerPop = new IntIndexableLookup<Cult, double>();
            _gamedataTracker = gamedataTracker;

            _itemNeedsPerPopPerMonth = new double[gamedataTracker.Items.Max(i => i.LookupIndex) + 1];
        }

        public int UpgradePercent
        {
            get
            {
                if (BaseType.UpgradeTime > 0)
                {
                    return (int)(Math.Max(0, _upgradeCounter / BaseType.UpgradeTime) * 100);
                }

                return 0;
            }
        }

        public int DowngradePercent
        {
            get
            {
                if (BaseType.UpgradeTime > 0)
                {
                    return (int)(Math.Max(0, -1 * _upgradeCounter / BaseType.DowngradeTime) * 100);
                }

                return 0;
            }
        }

        public override AbstractStructure<HousingStructureType> Initialize(Tile ulTile, HousingStructureType baseType)
        {
            base.Initialize(ulTile, baseType);

            _itemNeedsPerPopPerMonth = new double[_itemNeedsPerPopPerMonth.Length];

            PopNeeds = BaseType.MaintenanceNeeds
                .Select(n => new PopNeedState(n, false, false))
                .Concat(BaseType.UpgradeNeeds
                    .Select(n => new PopNeedState(n, true, false)))
                .Concat(BaseType.BonusNeeds
                    .Select(n => new PopNeedState(n, false, true)))
                .ToArray();

            foreach(var itemNeed in BaseType.MaintenanceNeeds.Concat(BaseType.UpgradeNeeds).Concat(BaseType.BonusNeeds).OfType<ItemPopNeed>())
            {
                _itemNeedsPerPopPerMonth[itemNeed.Item.LookupIndex] += itemNeed.PerMonthPerPop;
            }

            return this;
        }

        public override void DoUpdate(double deltaMonths)
        {
            base.DoUpdate(deltaMonths);

            var maintenenceNeedsMet = true;
            var upgradeNeedsMet = true;

            // Determine need satisfaction
            foreach (var popneed in PopNeeds)
            {
                popneed.Residents = Residents.Count;
                popneed.Update(this, deltaMonths);
            
                if (popneed.IsUpgradeNeed)
                {
                    upgradeNeedsMet = upgradeNeedsMet && popneed.Satisfaction >= 1;
                }
                else
                {
                    maintenenceNeedsMet = maintenenceNeedsMet && popneed.Satisfaction >= 0.5;
                }
            }

            var taxMod = 1 - EffectiveTaxRate;

            var maintenenceNeeds = PopNeeds
                .Where(n => !n.IsUpgradeNeed && !n.IsBonusNeed)
                .ToArray();

            var bonusNeeds = PopNeeds
                .Where(n => n.IsBonusNeed)
                .ToArray();

            var growthBonus = 1.0;
            foreach(var bonusType in SupportedCaste.GrowthBonusedBy)
            {
                growthBonus += CurrentBonuses[bonusType];
            }

            var bonusFactor = 1.0;
            if (bonusNeeds.Any())
            {
                bonusFactor += bonusNeeds.Average(n => n.Satisfaction) / 3;
            }

            var satisfaction = 0.0;
            if (!maintenenceNeeds.Any())
            {
                satisfaction = 1 * taxMod * bonusFactor;
            }
            else
            {
                satisfaction = maintenenceNeeds.Average(n => n.Satisfaction) * taxMod * bonusFactor;
            }

            if(Settlement.Governor != null)
            {
                satisfaction += Settlement.Governor.GetBonusSatisfaction();
            }

            foreach (var bonusType in SupportedCaste.SatisfactionBonusedBy)
            {
                satisfaction += CurrentBonuses[bonusType];
            }

            _prosperity = BaseType.ProspertyPerPop * Residents.Count + PopNeeds.Sum(pn => pn.BonusProsperity);

            // Handle upgrading and downgrading
            if (UnderConstruction)
            {
                _upgradeCounter = 0;
            }
            else
            {
                if (!maintenenceNeedsMet && AverageSatisfaction < 0.6) // count down and downgrade
                {
                    _upgradeCounter -= deltaMonths;

                    if (BaseType.DowngradesTo != null)
                    {
                        if (_upgradeCounter <= -1 * BaseType.DowngradeTime)
                        {
                            ChangeTo(BaseType.DowngradesTo);
                            _upgradeCounter = 0;
                        }
                    }
                    else
                    {
                        _upgradeCounter = Math.Max(0, _upgradeCounter);
                    }
                }
                else
                {
                    // count up and upgrade
                    if (upgradeNeedsMet && BaseType.UpgradesTo != null && Residents.Count == ProvidedHousing)
                    {
                        _upgradeCounter += deltaMonths;

                        if (_upgradeCounter >= BaseType.UpgradeTime)
                        {
                            ChangeTo(BaseType.UpgradesTo);
                            _upgradeCounter = 0;
                        }
                    }
                    else if (_upgradeCounter < 0) // normalize up to 0
                    {
                        _upgradeCounter = Math.Min(0, _upgradeCounter + deltaMonths);
                    }
                    else if (_upgradeCounter > 0) // normalize down to 0
                    {
                        _upgradeCounter = Math.Max(0, _upgradeCounter - deltaMonths);
                    }
                }
            }

            foreach (var pop in Residents)
            {
                pop.AddSatisfaction(deltaMonths, satisfaction);
                pop.GrowthBonus = growthBonus;
            }

            if(Residents.Any())
            {
                AverageSatisfaction = Residents.Select(r => r.Satisfaction).Average();
            }
            else
            {
                AverageSatisfaction = 1;
            }
        }

        private void ChangeTo(HousingStructureType newType)
        {
            BaseType = newType;

            ConstructionProgress = newType.ConstructionMonths;
            UnderConstruction = false;
            Settlement.SetCalculationFlag("upgradable structure changed");
            CalculateLayers();
        }

        public double NeedPerMonthOf(Item item)
        {
            return _itemNeedsPerPopPerMonth[item.LookupIndex] * Residents.Count;
        }

        public override IStackUiElement BuildDetailColumn()
        {
            var stackGroup = base.BuildDetailColumn();

            if (!UnderConstruction)
            {
                stackGroup.Children.Add(new Label("Housing"));
                stackGroup.Children.Add(new IconText(SupportedCaste.IconKey, $"{Residents.Count} / {ProvidedHousing}", tooltip: $"{SupportedCaste.Name} housing provided."));
                stackGroup.Children.Add(new Spacer(0, 10));
                stackGroup.Children.Add(new Label("")
                {
                    ContentUpdater = () =>
                    {
                        if (Residents.Any())
                        {
                            return $"Satisfaction {(int)(AverageSatisfaction * 100)}%";
                        }

                        return string.Empty;
                    }
                });

                var taxColor = BronzeColor.Green;
                if (EffectiveTaxRate >= 0.1)
                {
                    taxColor = BronzeColor.Yellow;
                }
                else if (EffectiveTaxRate >= 0.2)
                {
                    taxColor = BronzeColor.Red;
                }

                stackGroup.Children.Add(new StackGroup
                {
                    Orientation = Orientation.Horizontal,
                    Children = new List<IUiElement>
                    {
                        new Label("Taxed At"),
                        new Spacer(10, 0),
                        new Label((int)(EffectiveTaxRate * 100) + "%", taxColor),
                    }
                });


                stackGroup.Children.Add(new Label("")
                {
                    ContentUpdater = () =>
                    {
                        if (UpgradePercent > 0)
                        {
                            return $"Upgrading {UpgradePercent}%";
                        }
                        else if (DowngradePercent > 0)
                        {
                            return $"Downgrading {DowngradePercent}%";
                        }

                        return string.Empty;
                    },
                    ColorUpdater = () =>
                    {
                        if (UpgradePercent > 0)
                        {
                            return BronzeColor.Green;
                        }

                        return BronzeColor.Red;
                    }
                });

                stackGroup.Children.Add(new Spacer(0, 10));

                if (BaseType.MaintenanceNeeds.Any())
                {
                    stackGroup.Children.Add(new Label("Needs"));

                    foreach (var popneed in PopNeeds.Where(n => !n.IsUpgradeNeed && !n.IsBonusNeed))
                    {
                        stackGroup.Children.Add(UiBuilder.BuildDisplayFor(this, popneed));
                    }

                    stackGroup.Children.Add(new Spacer(0, 10));
                }


                if (BaseType.UpgradesTo != null)
                {
                    stackGroup.Children.Add(new Label($"Upgrades To {BaseType.UpgradesTo.Name} with"));
                    foreach (var popneed in PopNeeds.Where(n => n.IsUpgradeNeed))
                    {
                        stackGroup.Children.Add(UiBuilder.BuildDisplayFor(this, popneed));
                    }
                    stackGroup.Children.Add(new Spacer(0, 10));
                }

                if (BaseType.BonusNeeds.Any())
                {
                    stackGroup.Children.Add(new Label("Luxuries"));

                    foreach (var popneed in PopNeeds.Where(n => n.IsBonusNeed))
                    {
                        stackGroup.Children.Add(UiBuilder.BuildDisplayFor(this, popneed));
                    }

                    stackGroup.Children.Add(new Spacer(0, 10));
                }

                stackGroup.Children.Add(new Label("Cults"));
                var cultBody = new StackGroup
                {
                    Orientation = Orientation.Vertical
                };
                stackGroup.Children.Add(cultBody);
                foreach (var cult in _gamedataTracker.Cults)
                {
                    var members = Residents
                        .Where(p => p.CultMembership == cult)
                        .Count();
                    var influence = InfluencePerPop[cult];

                    if(members > 0 || influence > 0)
                    {
                        cultBody.Children.Add(new StackGroup
                        {
                            Orientation = Orientation.Horizontal,
                            Children = new List<IUiElement>
                            {
                                new Image(cult.IconKey, cult.Name),
                                new Spacer(10, 0),
                                new SizePadder(new IconText(string.Empty, members.ToString(), tooltip: cult.Name + " Members"), 25)
                                {
                                    HorizontalAlignment = HorizontalAlignment.Right
                                },
                                new Spacer(10, 0),
                                new SizePadder(new IconText(string.Empty, influence.ToString("P0"), tooltip: cult.Name + " Influence"), 25)
                                {
                                    HorizontalAlignment = HorizontalAlignment.Right
                                }
                            }
                        });

                    }
                }
            }

            return stackGroup;
        }

        public override IStackUiElement BuildHoverInfoColumn(bool playerOwns, bool playerSees)
        {
            var stackGroup = base.BuildHoverInfoColumn(playerOwns, playerSees);

            if (!UnderConstruction)
            {
                stackGroup.Children.AddRange(new List<IUiElement>
                {
                    new IconText(SupportedCaste.IconKey, $"{Residents.Count} / {ProvidedHousing}", tooltip: $"{SupportedCaste.Name} housing provided."),
                    new Spacer(0, 5),
                    new Label("")
                    {
                        ContentUpdater = () =>
                        {
                            return $"Satisfaction {(int)(AverageSatisfaction * 100)}%";
                        }
                    },
                    new Label("")
                    {
                        ContentUpdater = () =>
                        {
                            if (UpgradePercent > 0)
                            {
                                return $"Upgrading {UpgradePercent}%";
                            }
                            else if (DowngradePercent > 0)
                            {
                                return $"Downgrading {DowngradePercent}%";
                            }

                            return string.Empty;
                        }
                    },
                });
                var maintNeeds = PopNeeds.Where(n => !n.IsUpgradeNeed && !n.IsBonusNeed).ToArray();
                var upgradeNeeds = PopNeeds.Where(n => n.IsUpgradeNeed).ToArray();
                var bonusNeeds = PopNeeds.Where(n => n.IsBonusNeed).ToArray();

                stackGroup.Children.Add(new Label("Needs"));
                foreach (var popneed in maintNeeds)
                {
                    stackGroup.Children.Add(UiBuilder.BuildDisplayFor(this, popneed));
                }

                if(upgradeNeeds.Any())
                {
                    stackGroup.Children.Add(new Label("To Upgrade"));
                    foreach (var popneed in upgradeNeeds)
                    {
                        stackGroup.Children.Add(UiBuilder.BuildDisplayFor(this, popneed));
                    }
                }

                if (bonusNeeds.Any())
                {
                    stackGroup.Children.Add(new Label("Luxuries"));
                    foreach (var popneed in bonusNeeds)
                    {
                        stackGroup.Children.Add(UiBuilder.BuildDisplayFor(this, popneed));
                    }
                }
            }

            return stackGroup;
        }

        public override void OnRemoval()
        {
            base.OnRemoval();

            foreach (var pop in Residents)
            {
                pop.Home = null;
            }
        }

        public override void SerializeTo(SerializedObject structureRoot)
        {
            base.SerializeTo(structureRoot);

            foreach(var popneed in PopNeeds)
            {
                popneed.SerializeTo(structureRoot.CreateChild("need_state"));
            }
        }

        public override void DeserializeFrom(
            IGamedataTracker gamedataTracker, 
            Settlement settlement, 
            SerializedObject structureRoot)
        {
            base.DeserializeFrom(gamedataTracker, settlement, structureRoot);

            var needRoots = structureRoot.GetChildren("need_state").ToArray();
            var needs = PopNeeds.ToArray();

            for(var i = 0; i < needRoots.Length && i < PopNeeds.Count(); i++)
            {
                needs[i].DeserializeFrom(needRoots[i]);
            }
        }
    }
}
