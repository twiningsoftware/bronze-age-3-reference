﻿using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using System.Diagnostics;
using System.Linq;

namespace Bronze.Common.Data.World.Units
{
    public abstract class AbstractArmyOrder : IArmyOrder
    {
        public abstract string ActionTypeName { get; }

        public abstract bool IsIdle { get; }

        public abstract string GetDescription();

        public virtual void OnOrderChanged(Army army, IArmyOrder newOrder)
        {
        }

        public virtual void SerializeTo(SerializedObject orderRoot)
        {
            orderRoot.Set("action_type_name", ActionTypeName);
        }

        public abstract void SetAnimationFor(Army army, IUnit unit);

        public virtual void Simulate(Army army, double deltaMonths, AggregatePerformanceTracker aggregatePerformanceTracker)
        {
            var stopwatch = Stopwatch.StartNew();
            army.CellPathCalculator.Update();
            aggregatePerformanceTracker.AddValue("army_path_calculations", stopwatch.ElapsedTicks);
            stopwatch.Restart();
            

            if (army.CellPathCalculator.Path.Any()
                && army.CellPathCalculator.Path.Peek() == army.Cell)
            {
                var nextCell = army.CellPathCalculator.Path.Peek();
                var atPathEnd = army.CellPathCalculator.Path.Count < 2;

                if ((atPathEnd && (nextCell.Position.ToVector() - army.Position).Length() < 0.05)
                    || (!atPathEnd && (nextCell.Position.ToVector() - army.Position).Length() < 0.40))
                {
                    army.CellPathCalculator.Path.Dequeue();
                }
            }

            if(army.CellPathCalculator.Path.Any() && deltaMonths > 0)
            {
                if (army.CellPathCalculator.Path.Peek().Position.Plane != army.Cell.Position.Plane)
                {
                    army.Cell = army.CellPathCalculator.Path.Peek();
                }
                else
                {
                    army.MoveToward(deltaMonths, army.CellPathCalculator.Path.Peek().Position.ToVector());
                }
            }

            aggregatePerformanceTracker.AddValue("army_moving", stopwatch.ElapsedTicks);
            stopwatch.Restart();

            foreach (var unit in army.Units)
            {
                unit.AnimationTime += deltaMonths / Constants.MONTHS_PER_SECOND;
            }
        }
    }
}
