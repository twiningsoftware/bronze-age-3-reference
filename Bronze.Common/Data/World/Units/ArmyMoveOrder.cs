﻿using System.Collections.Generic;
using System.Linq;
using Bronze.Common.Data.Game.Units;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;

namespace Bronze.Common.Data.World.Units
{
    public class ArmyMoveOrder : AbstractArmyOrder
    {
        private Army _army;
        private Cell _target;

        public override string ActionTypeName => typeof(ArmyMoveAction).Name;

        public override bool IsIdle => false;

        public ArmyMoveOrder(
            Army army,
            Cell target)
        {
            _army = army;
            _target = target;
        }

        public override void Simulate(Army army, double deltaMonths, AggregatePerformanceTracker aggregatePerformanceTracker)
        {
            army.CellPathCalculator.SetDestination(_target, false);

            base.Simulate(army, deltaMonths, aggregatePerformanceTracker);

            if(army.Cell == _target && !army.CellPathCalculator.Path.Any())
            {
                army.CurrentOrder = new IdleOrder();
            }
        }

        public override void SerializeTo(SerializedObject orderRoot)
        {
            base.SerializeTo(orderRoot);

            _target.Position.SerializeTo(orderRoot.CreateChild("target_position"));
        }
        
        public static IArmyOrder DeserializeFrom(
            IEnumerable<Region> regions,
            Army army,
            SerializedObject orderRoot)
        {
            var targetPos = CellPosition.DeserializeFrom(orderRoot.GetChild("target_position"));

            var target = regions.SelectMany(r => r.Cells)
                .Where(c => c.Position == targetPos)
                .FirstOrDefault();

            if(target != null)
            {
                return new ArmyMoveOrder(army, target);
            }

            return new IdleOrder();
        }

        public override void SetAnimationFor(Army army, IUnit unit)
        {
            unit.SetAnimationForMovement(army, army.Cell.Traits);
        }

        public override string GetDescription()
        {
            return "Moving";
        }
    }
}
