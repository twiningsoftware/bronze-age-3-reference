﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Bronze.Common.Data.Game.Units;
using Bronze.Common.Data.World.CellDevelopments;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;

namespace Bronze.Common.Data.World.Units
{
    public class ArmyCampOrder : AbstractArmyOrder
    {
        private Army _army;
        private Cell _target;

        public bool IsCamped { get; private set; }
        public bool IsGarrisoned => IsCamped 
            && _army.Cell.Region?.Settlement?.Owner == _army.Owner 
            && (_army.Cell.Development is VillageDevelopment || _army.Cell.Development is DistrictDevelopment);

        public override bool IsIdle => true;

        public override string ActionTypeName => typeof(ArmyCampAction).Name;

        public double UpkeepPercent { get; set; }

        public CampedArmy CampedArmy { get; set; }

        public ArmyCampOrder(
            Army army,
            Cell target)
        {
            _army = army;
            _target = target;
        }

        public override void Simulate(Army army, double deltaMonths, AggregatePerformanceTracker aggregatePerformanceTracker)
        {
            army.CellPathCalculator.SetDestination(_target, false);

            base.Simulate(army, deltaMonths, aggregatePerformanceTracker);

            if(army.Cell.Owner != army.Owner)
            {
                UpkeepPercent = 0;
            }

            if(army.Cell == _target)
            {
                if (!army.CellPathCalculator.Path.Any() && !IsCamped)
                {
                    IsCamped = true;
                    var localSettlement = army.Cell.Region.Settlement;

                    if(localSettlement != null && localSettlement.Owner == army.Owner)
                    {
                        localSettlement.SetCalculationFlag("army camped");
                    }
                }

                if(army.Cell.WorldActors.OfType<Army>().Any(a => a != army && a.CurrentOrder is ArmyCampOrder campOrder && campOrder.IsCamped))
                {
                    army.CurrentOrder = new IdleOrder();
                }
            }
        }

        public override void OnOrderChanged(Army army, IArmyOrder newOrder)
        {
            base.OnOrderChanged(army, newOrder);
            
            if (IsCamped)
            {
                var localSettlement = army.Cell.Region.Settlement;

                if (localSettlement != null && localSettlement.Owner == army.Owner)
                {
                    localSettlement.SetCalculationFlag("army decamped");
                }
            }

            IsCamped = false;
        }

        public override void SerializeTo(SerializedObject orderRoot)
        {
            base.SerializeTo(orderRoot);

            _target.Position.SerializeTo(orderRoot.CreateChild("target_position"));
            orderRoot.Set("is_camped", IsCamped);
        }
        
        public override void SetAnimationFor(Army army, IUnit unit)
        {
            if (army.Cell == _target && IsCamped)
            {
                unit.SetAnimation(army, "camping".Yield());
            }
            else
            {
                unit.SetAnimationForMovement(army, army.Cell.Traits);
            }
        }

        public override string GetDescription()
        {
            if (_army.Cell != _target)
            {
                return "Moving to camp";
            }
            else if (IsGarrisoned)
            {
                if (UpkeepPercent > 0)
                {
                    return $"Garrisoned (Supply Rate {(int)(100 * UpkeepPercent)}%)";
                }
                else
                {
                    return "Garrisoned";
                }
            }
            else
            {
                if (UpkeepPercent > 0)
                {
                    return $"Camping (Supply Rate {(int)(100 * UpkeepPercent)}%)";
                }
                else
                {
                    return "Camping";
                }
            }
        }

        public static IArmyOrder DeserializeFrom(
            IEnumerable<Region> regions,
            Army army,
            SerializedObject orderRoot)
        {
            var targetPos = CellPosition.DeserializeFrom(orderRoot.GetChild("target_position"));

            var target = regions.SelectMany(r => r.Cells)
                .Where(c => c.Position == targetPos)
                .FirstOrDefault();

            if(target != null)
            {
                return new ArmyCampOrder(army, target)
                {
                    IsCamped = orderRoot.GetBool("is_camped")
                };
            }

            return new IdleOrder();
        }
    }
}
