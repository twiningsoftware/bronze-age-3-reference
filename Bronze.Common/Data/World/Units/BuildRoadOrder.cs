﻿using System.Collections.Generic;
using System.Linq;
using Bronze.Common.Data.Game.Units;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;

namespace Bronze.Common.Data.World.Units
{
    public class BuildRoadOrder : AbstractArmyOrder
    {
        private Army _army;
        private Cell _target;
        private ArmyBuildRoadAction _action;

        private ICellDevelopmentType _currentlyBuilding;
        private double _currentBuildCounter;
        private double _currentBuildMonths;

        public override string ActionTypeName => typeof(ArmyBuildRoadAction).Name;

        public override bool IsIdle => false;

        public BuildRoadOrder(
            Army army,
            Cell target,
            ArmyBuildRoadAction action)
        {
            _army = army;
            _target = target;
            _action = action;
        }

        public override void Simulate(Army army, double deltaMonths, AggregatePerformanceTracker aggregatePerformanceTracker)
        {
            base.Simulate(army, deltaMonths, aggregatePerformanceTracker);

            if (_currentlyBuilding != null)
            {
                if (_currentlyBuilding.CanBePlaced(army.Owner, army.Cell))
                {
                    _currentBuildCounter += deltaMonths;

                    if (_currentBuildCounter > _currentBuildMonths)
                    {
                        _currentlyBuilding.Place(army.Owner, army.Cell, instantBuild: true);

                        _currentlyBuilding = null;
                        _currentBuildCounter = 0;
                        _currentBuildMonths = 0;
                        army.CellPathCalculator.SetDestination(_target, false);
                    }
                }
                else
                {
                    _currentlyBuilding = null;
                    _currentBuildCounter = 0;
                    _currentBuildMonths = 0;
                    army.CellPathCalculator.SetDestination(_target, false);
                }
            }
            else
            {
                var buildOption = _action.RoadOptions
                    .Where(o => o.CellDevelopmentType.CanBePlaced(army.Owner, army.Cell))
                    .OrderBy(o => o.BuildMonths)
                    .FirstOrDefault();

                if (buildOption != null)
                {
                    _currentBuildCounter = 0;
                    _currentBuildMonths = buildOption.BuildMonths;
                    _currentlyBuilding = buildOption.CellDevelopmentType;
                }
            }
            
            if(_currentlyBuilding == null)
            {
                army.CellPathCalculator.SetDestination(_target, false);
            }
            else
            {
                army.CellPathCalculator.SetDestination(army.Cell, false);
            }
            
            if ((army.Cell == _target && _currentlyBuilding == null)
                || !army.AvailableActions.Any(a => a is ArmyBuildRoadAction))
            {
                army.CurrentOrder = new IdleOrder();
            }
        }

        public override void SetAnimationFor(Army army, IUnit unit)
        {
            if(_currentlyBuilding != null)
            {
                unit.SetAnimation(army, "building".Yield());
            }
            else
            {
                unit.SetAnimationForMovement(army, army.Cell.Traits);
            }
        }

        public override string GetDescription()
        {
            if(_currentlyBuilding == null)
            {
                return "Building Roads";
            }
            else
            {
                return $"Building {_currentlyBuilding.Name} {(int)(_currentBuildCounter / _currentBuildMonths * 100)}%";
            }
        }

        public override void SerializeTo(SerializedObject orderRoot)
        {
            base.SerializeTo(orderRoot);

            _target.Position.SerializeTo(orderRoot.CreateChild("target_position"));

            if (_currentlyBuilding != null)
            {
                orderRoot.Set("currently_building_id", _currentlyBuilding.Id);
                orderRoot.Set("current_build_counter", _currentBuildCounter);
                orderRoot.Set("current_build_months", _currentBuildMonths);
            }
        }

        public static IArmyOrder DeserializeFrom(
            IEnumerable<Region> regions,
            Army army,
            ArmyBuildRoadAction action,
            SerializedObject orderRoot)
        {
            var targetPos = CellPosition.DeserializeFrom(orderRoot.GetChild("target_position"));

            var target = regions.SelectMany(r => r.Cells)
                .Where(c => c.Position == targetPos)
                .FirstOrDefault();

            if(target != null)
            {
                var order = new BuildRoadOrder(army, target, action);

                order._currentlyBuilding = orderRoot.GetOptionalObjectReference(
                    "currently_building_id",
                    army.Owner.Race.AvailableCellDevelopments,
                    d => d.Id);

                order._currentBuildCounter = orderRoot.GetOptionalDouble("current_build_counter");

                order._currentBuildMonths = orderRoot.GetOptionalDouble("current_build_months");

                return order;
            }

            return new IdleOrder();
        }
    }
}
