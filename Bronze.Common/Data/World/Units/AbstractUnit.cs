﻿using Bronze.Contracts;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace Bronze.Common.Data.World.Units
{
    public abstract class AbstractUnit<TUnitType> : IUnit where TUnitType : IUnitType
    {
        public string IconKey => Equipment.IconKey;
        public string PortraitKey => Equipment.PortraitKey;
        public UnitCategory Category => BaseType.Category;
        public string Name => BaseType.Name;
        public string Description => BaseType.Description;
        public int VisionRange => Equipment.VisionRange;
        public MovementMode[] MovementModes => Equipment.MovementModes;
        public double Health { get; set; }
        public int MaxHealth => Equipment.HealthPerIndividual * Equipment.IndividualCount;
        public double Morale { get; set; }
        public double HealthPercent => Health / MaxHealth;
        public int Armor => Equipment.Armor;
        public int IndividualCount => Equipment.IndividualCount;
        public IndividualAnimation[] IndividualAnimations => Equipment.IndividualAnimations;
        public Vector2? LastDrawPosition { get; set; }

        protected TUnitType BaseType { get; private set; }
        public IndividualAnimation CurrentAnimation { get; set; }
        public double AnimationTime { get; set; }
        
        public ItemRate[] UpkeepNeeds => Equipment.UpkeepNeeds;
        
        public IEnumerable<IArmyAction> AvailableActions => Equipment.AvailableActions;

        public bool SufferingAttrition { get; private set; }
        public double Supplies { get; set; }
        public double MaxSupplies => Equipment.SupplyCapacity;
        public double SuppliesPercent => Supplies / MaxSupplies;
        public IUnitType UnitType => BaseType;
        public double StrengthEstimate => HealthPercent * Equipment.StrengthEstimate * MoraleMod;
        public int SkirmishSkill => Equipment.SkirmishSkill;
        public int BlockSkill => Equipment.BlockSkill;
        public int DefenseSkill => Equipment.DefenseSkill;
        public AttackInfo[] MeleeAttacks => Equipment.MeleeAttacks;
        public AttackInfo[] RangedAttacks => Equipment.RangedAttacks;
        public int ResolveSkill => Equipment.ResolveSkill;
        public abstract bool CanResupply { get; }
        private bool _inInstantAnimation;

        public double MoraleMod => Util.Clamp(0.5 + Morale / 2, 0, 2);

        private UnitEquipmentSet _equipmentSet;

        public UnitEquipmentSet Equipment
        {
            get => _equipmentSet;
            set
            {
                if (_equipmentSet != null)
                {
                    // Max health might change, keep the percent the same
                    var healthPerc = HealthPercent;
                    _equipmentSet = value;
                    Health = healthPerc * MaxHealth;
                }
                else
                {
                    _equipmentSet = value;
                }
            }
        }

        public RecruitmentSlotCategory RecruitmentCategory => BaseType.RecruitmentCategory;

        public virtual AbstractUnit<TUnitType> Initialize(TUnitType baseType, UnitEquipmentSet equipmentSet)
        {
            BaseType = baseType;
            Equipment = equipmentSet;
            Health = MaxHealth;
            Supplies = MaxSupplies;
            Morale = 1;

            AnimationTime = 0;
            CurrentAnimation = IndividualAnimations
                .Where(ia => ia.Name == Constants.ANIMATION_IDLE)
                .FirstOrDefault();

            return this;
        }

        public virtual void SerializeTo(SerializedObject unitRoot)
        {
            unitRoot.Set("type_id", BaseType.Id);
            unitRoot.Set("health", Health);
            unitRoot.Set("animation_name", CurrentAnimation?.Name);
            unitRoot.Set("animation_time", AnimationTime);
            unitRoot.Set("supplies", Supplies);
            unitRoot.Set("equipment_level", Equipment.Level);
            unitRoot.Set("morale", Morale);
        }

        public virtual void DeserializeFrom(
            IGamedataTracker gamedataTracker, 
            SerializedObject unitRoot)
        {
            Health = unitRoot.GetDouble("health");
            Supplies = unitRoot.GetDouble("supplies");
            AnimationTime = unitRoot.GetDouble("animation_time");
            Morale = unitRoot.GetDouble("morale");
            
            // Setting equipment is done before this call, so it can be passed into initialize
        }
        
        public virtual bool CanPath(Cell cell)
        {
            return Equipment.CanPath(cell);
        }

        public virtual bool CanPath(Tile tile)
        {
            return Equipment.CanPath(tile);
        }

        public virtual double GetMovementSpeed(Cell cell)
        {
            return Equipment.GetMovementSpeed(cell);
        }

        public void SetInstantAnimation(Army army, IEnumerable<string> animationNames)
        {
            CurrentAnimation = null;
            _inInstantAnimation = true;
            SetAnimation(army, animationNames);
        }

        public void SetAnimation(Army army, IEnumerable<string> animationNames)
        {
            if (CurrentAnimation == null || (!animationNames.Contains(CurrentAnimation.Name) && !_inInstantAnimation))
            {
                AnimationTime = 0;
                CurrentAnimation = (army.TransportType?.IndividualAnimations  ?? IndividualAnimations)
                    .Where(a => animationNames.Contains(a.Name))
                    .FirstOrDefault()
                    ?? (army.TransportType?.IndividualAnimations ?? IndividualAnimations)
                    .Where(a => a.Name == Constants.ANIMATION_IDLE)
                    .FirstOrDefault();
            }
        }

        public void SetAnimationForMovement(Army army, IEnumerable<Trait> terrainTraits)
        {
            var animationNames = (army.TransportType?.MovementModes ?? MovementModes)
                .Where(mm => terrainTraits.ContainsAll(mm.MovementType.RequiredTraits))
                .Where(mm => !terrainTraits.ContainsAny(mm.MovementType.PreventedBy))
                .OrderByDescending(mm => mm.GetMovementSpeedFor(terrainTraits))
                .SelectMany(mm => mm.MovementType.AnimationNames)
                .ToArray();

            SetAnimation(army, animationNames);
        }
        
        public virtual void Update(double elapsedMonths, Army army)
        {
            if (elapsedMonths > 0)
            {
                Supplies = Math.Max(0, Supplies - elapsedMonths);

                SufferingAttrition = Supplies <= 0;

                if (SufferingAttrition)
                {
                    Health = MaxHealth * (HealthPercent - Constants.ATTRITION_RATE * elapsedMonths);
                }

                if (_inInstantAnimation && (CurrentAnimation == null || CurrentAnimation.LoopTime <= AnimationTime))
                {
                    _inInstantAnimation = false;
                    CurrentAnimation = null;
                }
            }
        }
        
        public virtual void OnDisbanded()
        {
        }
    }
}
