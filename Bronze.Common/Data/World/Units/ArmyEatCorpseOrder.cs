﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bronze.Common.Data.Game.Units;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;

namespace Bronze.Common.Data.World.Units
{
    public class ArmyEatCorpseOrder : AbstractArmyOrder
    {
        private Army _army;
        private Cell _target;
        private Random _random;

        public bool IsEating { get; private set; }
        
        public override bool IsIdle => false;

        public override string ActionTypeName => typeof(ArmyEatCorpseAction).Name;
        
        public ArmyEatCorpseOrder(
            Army army,
            Cell target)
        {
            _army = army;
            _target = target;
            _random = new Random();
        }

        public override void Simulate(Army army, double deltaMonths, AggregatePerformanceTracker aggregatePerformanceTracker)
        {
            army.CellPathCalculator.SetDestination(_target, false);

            base.Simulate(army, deltaMonths, aggregatePerformanceTracker);
            
            if(army.Cell == _target)
            {
                if (!army.CellPathCalculator.Path.Any() && !IsEating)
                {
                    IsEating = true;
                }

                if(IsEating)
                {
                    var eaters = army.Units
                        .Where(u => u.AvailableActions.Any(a => a is ArmyEatCorpseAction))
                        .ToArray();

                    var corpses = _target.WorldActors.OfType<WorldCorpse>().ToArray();

                    if(eaters.Any() && corpses.Any())
                    {
                        var eatTarget = corpses.First();

                        var eatAmount = deltaMonths * eaters.Count() * 5;

                        eatTarget.DecayTime += eatAmount;

                        foreach(var eater in eaters)
                        {
                            eater.Supplies = Math.Min(eater.MaxSupplies, eater.Supplies + deltaMonths * 3);
                            eater.Health = Math.Min(1, eater.HealthPercent + deltaMonths) * eater.MaxHealth;
                        }

                        if (!army.IsFull && eaters.All(e => e.SuppliesPercent > 0.95))
                        {
                            var toClone = _random.Choose(eaters);
                            var clone = toClone.UnitType.CreateUnit(toClone.Equipment);
                            clone.Supplies = 0.5;
                            army.Units.Add(clone);
                        }
                    }
                    else
                    {
                        army.CurrentOrder = new IdleOrder();
                    }
                }
            }
        }

        public override void OnOrderChanged(Army army, IArmyOrder newOrder)
        {
            base.OnOrderChanged(army, newOrder);
            
            IsEating = false;
        }

        public override void SerializeTo(SerializedObject orderRoot)
        {
            base.SerializeTo(orderRoot);

            _target.Position.SerializeTo(orderRoot.CreateChild("target_position"));
            orderRoot.Set("is_eating", IsEating);
        }
        
        public override void SetAnimationFor(Army army, IUnit unit)
        {
            if (army.Cell == _target && IsEating)
            {
                unit.SetAnimation(army, "eating".Yield());
            }
            else
            {
                unit.SetAnimationForMovement(army, army.Cell.Traits);
            }
        }

        public override string GetDescription()
        {
            if(IsEating)
            {
                return "Eating";
            }
            else
            {
                return "Moving to eat";
            }
        }

        public static IArmyOrder DeserializeFrom(
            IEnumerable<Region> regions,
            Army army,
            SerializedObject orderRoot)
        {
            var targetPos = CellPosition.DeserializeFrom(orderRoot.GetChild("target_position"));

            var target = regions.SelectMany(r => r.Cells)
                .Where(c => c.Position == targetPos)
                .FirstOrDefault();

            if(target != null)
            {
                return new ArmyEatCorpseOrder(army, target)
                {
                    IsEating = orderRoot.GetBool("is_eating")
                };
            }

            return new IdleOrder();
        }
    }
}
