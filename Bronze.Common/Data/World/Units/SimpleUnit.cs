﻿using Bronze.Common.Data.Game.Units;

namespace Bronze.Common.Data.World.Units
{
    public class SimpleUnit : AbstractUnit<SimpleUnitType>
    {
        public override bool CanResupply => true;
    }
}
