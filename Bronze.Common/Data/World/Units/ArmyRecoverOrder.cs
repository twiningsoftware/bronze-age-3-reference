﻿using System.Collections.Generic;
using System.Linq;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.Data.World.Units
{
    public class ArmyRecoverOrder : AbstractArmyOrder
    {
        public bool IsRouting { get; set; }
        public Cell Target { get; set; }
        public IArmyOrder FallbackOrder { get; private set; }
        public double TotalTime { get; private set; }
        public double TimeRemaining { get; private set; }

        public override string ActionTypeName => typeof(ArmyRecoverOrder).Name;

        public override bool IsIdle => false;

        public ArmyRecoverOrder(
            Army army,
            bool isRouting,
            IArmyOrder fallbackOrder,
            double totalTime)
        {
            FallbackOrder = fallbackOrder;
            IsRouting = isRouting;
            Target = army.Cell;
            TotalTime = totalTime;
            TimeRemaining = totalTime;
        }

        public override void Simulate(Army army, double deltaMonths, AggregatePerformanceTracker aggregatePerformanceTracker)
        {
            TimeRemaining -= deltaMonths;
            army.IsRouting = IsRouting;

            if (IsRouting)
            {
                army.CellPathCalculator.SetDestination(Target, false);
            }
            else
            {
                army.CellPathCalculator.ClearPathing();
            }

            base.Simulate(army, deltaMonths, aggregatePerformanceTracker);

            if(!army.IsLocked)
            {
                army.CurrentOrder = FallbackOrder;
            }
        }

        public override void SerializeTo(SerializedObject orderRoot)
        {
            base.SerializeTo(orderRoot);

            orderRoot.Set("is_routing", IsRouting);
            Target.Position.SerializeTo(orderRoot.CreateChild("target_position"));
            orderRoot.Set("total_time", TotalTime);
            orderRoot.Set("time_remaining", TimeRemaining);
        }
        
        public static IArmyOrder DeserializeFrom(
            IEnumerable<Region> regions,
            Army army,
            SerializedObject orderRoot,
            IWorldManager worldManager,
            IGamedataTracker gamedataTracker,
            IBattleHelper battleHelper)
        {
            var fallbackOrder = (IArmyOrder)new NullOrder();

            var targetPos = CellPosition.DeserializeFrom(orderRoot.GetChild("target_position"));

            var target = regions.SelectMany(r => r.Cells)
                .Where(c => c.Position == targetPos)
                .FirstOrDefault();
            
            var fallbackOrderRoot = orderRoot.GetOptionalChild("fallback_order");
            if (fallbackOrderRoot != null)
            {
                var actionTypeName = fallbackOrderRoot.GetOptionalString("action_type_name");

                var action = army.AvailableActions
                    .Where(a => a.GetType().Name == actionTypeName)
                    .FirstOrDefault();

                if (action != null)
                {
                    fallbackOrder = action.DeserializeOrderFrom(gamedataTracker, regions, army.Owner, army, fallbackOrderRoot);
                }
            }

            var totalTime = orderRoot.GetDouble("total_time");
            var remainingTime = orderRoot.GetDouble("time_remaining");

            var order = new ArmyRecoverOrder(
                army, 
                orderRoot.GetBool("is_routing"), 
                fallbackOrder,
                totalTime);

            order.TimeRemaining = remainingTime;

            if (target != null)
            {
                order.Target = target;
            }

            return order;
        }

        public override void SetAnimationFor(Army army, IUnit unit)
        {
            if (IsRouting)
            {
                unit.SetAnimationForMovement(army, army.Cell.Traits);
            }
            else
            {
                unit.SetAnimation(army, Constants.ANIMATION_IDLE.Yield());
            }
        }

        public override string GetDescription()
        {
            var progress = 1 - (TimeRemaining / TotalTime);
            if (IsRouting)
            {
                return $"Routing {progress.ToString("P0")}";
            }
            else
            {
                return $"Recovering {progress.ToString("P0")}";
            }
        }

        public override void OnOrderChanged(Army army, IArmyOrder newOrder)
        {
            base.OnOrderChanged(army, newOrder);

            army.IsRouting = false;
        }
    }
}
