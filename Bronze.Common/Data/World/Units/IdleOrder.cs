﻿using Bronze.Common.Data.Game.Units;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.World;
using System.Linq;

namespace Bronze.Common.Data.World.Units
{
    public class IdleOrder : AbstractArmyOrder
    {
        public override string ActionTypeName => typeof(ArmyIdleAction).Name;

        public override bool IsIdle => true;

        private double _campTimer;

        public IdleOrder()
        {
            _campTimer = 0.1;
        }

        public override string GetDescription()
        {
            return "Waiting";
        }

        public override void SetAnimationFor(Army army, IUnit unit)
        {
            unit.SetAnimation(army, Constants.ANIMATION_IDLE.Yield());
        }

        public override void Simulate(Army army, double deltaMonths, AggregatePerformanceTracker aggregatePerformanceTracker)
        {
            army.CellPathCalculator.ClearPathing();

            base.Simulate(army, deltaMonths, aggregatePerformanceTracker);

            _campTimer -= deltaMonths;

            if(_campTimer <= 0 && !army.IsLocked)
            {
                var campAction = army.AvailableActions
                    .OfType<ArmyCampAction>()
                    .FirstOrDefault();

                if(campAction != null)
                {
                    var targetCell = army.Cell.Yield()
                        .Concat(army.Cell.OrthoNeighbors)
                        .Where(c => campAction.IsValidFor(army, c))
                        .FirstOrDefault();

                    if (targetCell != null)
                    {
                        army.CurrentOrder = campAction.BuildOrderFor(army, targetCell);
                    }
                }
            }
        }
    }
}
