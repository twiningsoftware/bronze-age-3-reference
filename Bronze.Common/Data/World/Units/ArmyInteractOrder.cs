﻿using System.Collections.Generic;
using System.Linq;
using Bronze.Common.Data.Game.Units;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.Data.World.Units
{
    public class ArmyInteractOrder : AbstractArmyOrder
    {
        private readonly IBattleHelper _battleHelper;

        private Army _army;
        private Cell _targetCell;

        public Settlement SiegedSettlement { get; set; }
        
        public override string ActionTypeName => typeof(ArmyInteractAction).Name;

        public override bool IsIdle => false;

        public ArmyInteractOrder(
            Army army,
            Cell target,
            IBattleHelper battleHelper)
        {
            _army = army;
            _targetCell = target;
            _battleHelper = battleHelper;
        }

        public override void Simulate(Army army, double deltaMonths, AggregatePerformanceTracker aggregatePerformanceTracker)
        {
            var targetInteractable = _targetCell.Development as IArmyInteractable;
            var siegeTarget = _targetCell.Development;
            if(targetInteractable != null)
            {
                army.CellPathCalculator.SetDestination(_targetCell, targetInteractable.InteractAdjacent);
            }
            else if (siegeTarget != null)
            {
                army.CellPathCalculator.SetDestination(_targetCell, pathAdjacent: true);
            }
            else
            {
                army.CurrentOrder = new IdleOrder();
                return;
            }
            
            base.Simulate(army, deltaMonths, aggregatePerformanceTracker);

            if (deltaMonths > 0)
            {
                if (siegeTarget != null && siegeTarget.Settlement?.Owner != null && siegeTarget.CanBeSieged && army.Owner.IsHostileTo(siegeTarget.Settlement?.Owner))
                {
                    if (army.Cell.OrthoNeighbors.Any(n => n == siegeTarget.Cell))
                    {
                        army.CellPathCalculator.ClearPathing();

                        SiegedSettlement = siegeTarget.Settlement;

                        if (!siegeTarget.Settlement.SiegedBy.Contains(army))
                        {
                            siegeTarget.Settlement.SetCalculationFlag("under siege");
                            siegeTarget.Settlement.SiegedBy.Add(army);

                        }

                        return;
                    }
                }
                else if (targetInteractable != null)
                {
                    if (!targetInteractable.InteractWithForeign && targetInteractable.Cell.Region.Owner != army.Owner)
                    {
                        army.CurrentOrder = new IdleOrder();
                        return;
                    }

                    if (targetInteractable.InteractAdjacent && army.Cell.OrthoNeighbors.Any(n => n == targetInteractable.Cell))
                    {
                        targetInteractable.DoInteraction(_army);
                        army.CurrentOrder = new IdleOrder();
                        return;
                    }
                    else if (!targetInteractable.InteractAdjacent && army.Cell == targetInteractable.Cell)
                    {
                        targetInteractable.DoInteraction(_army);
                        army.CurrentOrder = new IdleOrder();
                        return;
                    }
                }
                else
                {
                    army.CurrentOrder = new IdleOrder();
                    return;
                }
            }
        }

        public override void SerializeTo(SerializedObject orderRoot)
        {
            base.SerializeTo(orderRoot);
            
            _targetCell.Position.SerializeTo(orderRoot.CreateChild("target_position"));
        }

        public override void OnOrderChanged(Army army, IArmyOrder newOrder)
        {
            base.OnOrderChanged(army, newOrder);

            if(SiegedSettlement != null)
            {
                SiegedSettlement.SiegedBy.Remove(army);
                SiegedSettlement.SetCalculationFlag("sieger leaving");
                SiegedSettlement = null;
            }
        }

        public static IArmyOrder DeserializeFrom(
            IEnumerable<Region> regions,
            Army army,
            SerializedObject orderRoot,
            IBattleHelper battleHelper)
        {
            var positionRoot = orderRoot.GetOptionalChild("target_position");

            if (positionRoot != null)
            {
                var targetPos = CellPosition.DeserializeFrom(positionRoot);

                var targetCell = regions.SelectMany(r => r.Cells)
                    .Where(c => c.Position == targetPos)
                    .FirstOrDefault();

                if (targetCell != null)
                {
                    return new ArmyInteractOrder(army, targetCell, battleHelper);
                }
            }

            return new IdleOrder();
        }

        public override void SetAnimationFor(Army army, IUnit unit)
        {
            if (SiegedSettlement == null)
            {
                unit.SetAnimationForMovement(army, army.Cell.Traits);
            }
            else
            {
                unit.SetAnimation(army, "skirmishing".Yield());
            }
        }

        public override string GetDescription()
        {
            var targetInteractable = _targetCell.Development as IArmyInteractable;
            var siegeTarget = _targetCell.Development;

            if(SiegedSettlement != null)
            {
                return "Sieging " + SiegedSettlement.Name;
            }
            if (siegeTarget != null && siegeTarget.Settlement?.Owner != null && siegeTarget.CanBeSieged && _army.Owner.IsHostileTo(siegeTarget.Settlement?.Owner))
            {
                return "Moving to Siege";
            }
            else if (targetInteractable != null)
            {
                return targetInteractable.InteractVerb;
            }

            return "Moving";
        }
    }
}
