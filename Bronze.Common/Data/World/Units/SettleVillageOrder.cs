﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bronze.Common.Data.Game.Units;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.Data.World.Units
{
    public class SettleVillageOrder : AbstractArmyOrder
    {
        private Army _army;
        private Cell _target;
        private ArmySettleAction _action;
        private readonly IUnitHelper _unitHelper;
        private readonly INameSource _nameSource;

        public override string ActionTypeName => typeof(ArmySettleAction).Name;

        public override bool IsIdle => false;

        private double _settleCounter;

        public SettleVillageOrder(
            Army army,
            Cell target,
            ArmySettleAction action,
            IUnitHelper unitHelper,
            INameSource nameSource)
        {
            _army = army;
            _target = target;
            _action = action;
            _unitHelper = unitHelper;
            _nameSource = nameSource;
            _settleCounter = 0;
        }

        public override void Simulate(Army army, double deltaMonths, AggregatePerformanceTracker aggregatePerformanceTracker)
        {
            army.CellPathCalculator.SetDestination(_target, false);

            base.Simulate(army, deltaMonths, aggregatePerformanceTracker);

            if (army.Cell == _target)
            {
                _settleCounter += deltaMonths;

                if (_settleCounter >= _action.SettleMonths)
                {
                    DoSettling(army);
                }
            }

            if (!_action.CellDevelopmentType.CanBePlaced(army.Owner, _target)
                || !army.AvailableActions.Any(a => a is ArmySettleAction))
            {
                army.CurrentOrder = new IdleOrder();
            }
        }

        public override void SerializeTo(SerializedObject orderRoot)
        {
            base.SerializeTo(orderRoot);

            _target.Position.SerializeTo(orderRoot.CreateChild("target_position"));
        }

        private void DoSettling(Army army)
        {
            if(_action.CellDevelopmentType.CanBePlaced(army.Owner, _target))
            {
                _target.Region.Settlement = new Settlement(_action.SettlementRace, army.Owner)
                {
                    Name = _nameSource.MakeSettlementName(_action.SettlementRace, _target),
                    Region = _target.Region
                };
                army.Owner.Settlements.Add(_target.Region.Settlement);

                _action.CellDevelopmentType.Place(army.Owner, army.Cell, instantBuild: true);

                var random = new Random();

                if (army.Cell.Region.Settlement != null)
                {
                    for (var i = 0; i < _action.StartingPopCount; i++)
                    {
                        Cult cultMembership = null;

                        if(_action.StartingPopCaste.CultLikelyhoods.Any())
                        {
                            cultMembership = random.ChooseByLikelyhood(_action.StartingPopCaste.CultLikelyhoods);
                        }
                        
                        army.Cell.Region.Settlement.Population.Add(new Pop(
                            _action.SettlementRace, 
                            _action.StartingPopCaste,
                            cultMembership));
                    }
                }

                var settlingUnit = army.Units
                    .Where(u => u.AvailableActions.Contains(_action))
                    .FirstOrDefault();

                if (settlingUnit != null)
                {
                    _unitHelper.RemoveUnit(army, settlingUnit, removedViolently: false);
                }
            }

            army.CurrentOrder = new IdleOrder();
        }

        public override void SetAnimationFor(Army army, IUnit unit)
        {
            if (_army.Cell == _target)
            {
                unit.SetAnimation(army, "building".Yield());
            }
            else
            {
                unit.SetAnimationForMovement(army, army.Cell.Traits);
            }
        }

        public override string GetDescription()
        {
            if (_army.Cell != _target)
            {
                return "Moving to settle";
            }
            else
            {
                return $"Settling: {(int)(_settleCounter / _action.SettleMonths * 100)}%";
            }
        }

        public static IArmyOrder DeserializeFrom(
            IUnitHelper unitHelper,
            INameSource nameSource,
            IEnumerable<Region> regions,
            Army army,
            ArmySettleAction action,
            SerializedObject orderRoot)
        {
            var targetPos = CellPosition.DeserializeFrom(orderRoot.GetChild("target_position"));

            var target = regions.SelectMany(r => r.Cells)
                .Where(c => c.Position == targetPos)
                .FirstOrDefault();

            if(target != null)
            {
                return new SettleVillageOrder(army, target, action, unitHelper, nameSource);
            }

            return new IdleOrder();
        }
    }
}
