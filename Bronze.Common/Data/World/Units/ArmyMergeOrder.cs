﻿using System.Collections.Generic;
using System.Linq;
using Bronze.Common.Data.Game.Units;
using Bronze.Common.UI.Modals;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.Data.World.Units
{
    public class ArmyMergeOrder : AbstractArmyOrder
    {
        private readonly IWorldManager _worldManager;
        private readonly IDialogManager _dialogManager;

        private Army _army;
        private Army _targetArmy;
        private string _targetArmyId;

        public override string ActionTypeName => typeof(ArmyMergeAction).Name;

        public override bool IsIdle => false;

        public ArmyMergeOrder(
            Army army,
            Army targetArmy,
            IWorldManager worldManager,
            IDialogManager dialogManager)
        {
            _army = army;
            _targetArmy = targetArmy;
            _worldManager = worldManager;
            _dialogManager = dialogManager;
            _targetArmyId = _targetArmy?.Id;
        }

        public override void Simulate(Army army, double deltaMonths, AggregatePerformanceTracker aggregatePerformanceTracker)
        {
            // After deserializing, will need to get the actual target army.
            if (_targetArmyId != null && _targetArmy == null)
            {
                _targetArmy = _worldManager.Tribes
                    .SelectMany(t => t.Armies)
                    .Where(a => a.Id == _targetArmyId)
                    .FirstOrDefault();

                if (_targetArmy == null)
                {
                    army.CurrentOrder = new IdleOrder();
                    return;
                }
            }

            if (!_targetArmy.Units.Any())
            {
                army.CurrentOrder = new IdleOrder();
                return;
            }

            army.CellPathCalculator.SetDestination(_targetArmy);

            base.Simulate(army, deltaMonths, aggregatePerformanceTracker);

            if (army.Cell.OrthoNeighbors.Any(n => n == _targetArmy.Cell)
                || army.Cell == _targetArmy.Cell)
            {
                if (army.Cell.OrthoNeighbors.Any(n => n == _targetArmy.Cell))
                {
                    army.CurrentOrder = new IdleOrder();

                    if (_targetArmy.Owner == army.Owner
                        && !_targetArmy.IsLocked)
                    {
                        _dialogManager.PushModal<ArmyMergeModal, ArmyMergeModalContext>(
                             new ArmyMergeModalContext
                             {
                                 FromArmy = army,
                                 ToArmy = _targetArmy
                             });
                    }
                }
            }
        }

        public override void SerializeTo(SerializedObject orderRoot)
        {
            base.SerializeTo(orderRoot);
            
            orderRoot.Set("target_army_id", _targetArmyId);
        }
        
        public static IArmyOrder DeserializeFrom(
            IEnumerable<Region> regions,
            Army army,
            SerializedObject orderRoot,
            IWorldManager worldManager,
            IDialogManager dialogManager)
        {
            var order = new ArmyMergeOrder(army, null, worldManager, dialogManager);

            order._targetArmyId = orderRoot.GetString("target_army_id");

            return order;
        }

        public override void SetAnimationFor(Army army, IUnit unit)
        {
            unit.SetAnimationForMovement(army, army.Cell.Traits);
        }

        public override string GetDescription()
        {
            return "Merging";
        }
    }
}
