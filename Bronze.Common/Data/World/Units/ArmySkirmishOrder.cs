﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Bronze.Common.Data.Game.Units;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.Data.World.Units
{
    public class ArmySkirmishOrder : AbstractArmyOrder
    {
        private readonly IWorldManager _worldManager;
        private readonly IBattleHelper _battleHelper;

        private Army _army;
        public Army TargetArmy { get; private set; }
        private string _targetArmyId;
        public IArmyOrder FallbackOrder { get; private set; }
        private double _skirmishAttackCooldown;

        public override string ActionTypeName => typeof(ArmySkirmishAction).Name;

        public override bool IsIdle => false;

        public ArmySkirmishOrder(
            Army army,
            Army targetArmy,
            IWorldManager worldManager,
            IArmyOrder fallbackOrder,
            IBattleHelper battleHelper)
        {
            _army = army;
            TargetArmy = targetArmy;
            _worldManager = worldManager;
            _targetArmyId = TargetArmy?.Id;
            FallbackOrder = fallbackOrder;
            _battleHelper = battleHelper;
        }

        public override void Simulate(Army army, double deltaMonths, AggregatePerformanceTracker aggregatePerformanceTracker)
        {
            army.CellPathCalculator.ClearPathing();

            var stopwatch = Stopwatch.StartNew();

            // After deserializing, will need to get the actual target army.
            if(_targetArmyId != null && TargetArmy == null)
            {
                TargetArmy = _worldManager.Tribes
                    .SelectMany(t => t.Armies)
                    .Where(a => a.Id == _targetArmyId)
                    .FirstOrDefault();

                if(TargetArmy == null)
                {
                    army.CurrentOrder = FallbackOrder;
                    return;
                }
            }

            if (!TargetArmy.Units.Any() || !army.SkirmishingWith.Any() || !army.Owner.IsHostileTo(TargetArmy.Owner))
            {
                army.CurrentOrder = FallbackOrder;
                return;
            }

            var distTo = (army.Position - TargetArmy.Position).Length();

            if(distTo > Constants.DIST_SKIRMISH_MAX)
            {
                army.CurrentOrder = FallbackOrder;
                return;
            }

            aggregatePerformanceTracker.AddValue("army_skirmish_prelogic", stopwatch.ElapsedTicks);
            stopwatch.Restart();

            if(army.Stance == ArmyStance.Agressive || (army.Stance == ArmyStance.Harassing && distTo > Constants.DIST_HARASSMENT_MAX))
            {
                if (distTo > Constants.DIST_COMBAT)
                {
                    army.MoveToward(deltaMonths, TargetArmy.Position);
                }
            }
            else if(army.Stance == ArmyStance.Evasive || (army.Stance == ArmyStance.Harassing && distTo < Constants.DIST_HARASSMENT_MIN))
            {
                var fleeCell = army.Cell.Yield()
                    .Concat(army.Cell.Neighbors)
                    .Where(c => army.CanPath(c))
                    .OrderByDescending(c => (c.Position.ToVector() - TargetArmy.Position).LengthSquared())
                    .ThenBy(c => (c.Position.ToVector() - army.Position).LengthSquared())
                    .FirstOrDefault();

                if (fleeCell != null)
                {
                    army.MoveToward(deltaMonths, fleeCell.Position.ToVector());
                }
            }

            army.Facing = Util.GetFacing(army.Position, TargetArmy.Position);

            aggregatePerformanceTracker.AddValue("army_skirmish_movement", stopwatch.ElapsedTicks);
            stopwatch.Restart();

            base.Simulate(army, deltaMonths, aggregatePerformanceTracker);

            _skirmishAttackCooldown -= deltaMonths;

            if(_skirmishAttackCooldown <= 0)
            {
                _skirmishAttackCooldown = 0.1;
                
                _battleHelper.DoSkirmishing(army, TargetArmy);

                aggregatePerformanceTracker.AddValue("army_skirmish_attack", stopwatch.ElapsedTicks);
            }
        }

        public override void SerializeTo(SerializedObject orderRoot)
        {
            base.SerializeTo(orderRoot);
            
            orderRoot.Set("target_army_id", _targetArmyId);
            orderRoot.Set("skirmish_attack_cooldown", _skirmishAttackCooldown);
        }
        
        public static IArmyOrder DeserializeFrom(
            IEnumerable<Region> regions,
            Army army,
            SerializedObject orderRoot,
            IWorldManager worldManager,
            IGamedataTracker gamedataTracker,
            IBattleHelper battleHelper)
        {
            var fallbackOrder = (IArmyOrder)new NullOrder();

            var fallbackOrderRoot = orderRoot.GetOptionalChild("fallback_order");
            if (fallbackOrderRoot != null)
            {
                var actionTypeName = fallbackOrderRoot.GetOptionalString("action_type_name");

                var action = army.AvailableActions
                    .Where(a => a.GetType().Name == actionTypeName)
                    .FirstOrDefault();

                if (action != null)
                {
                    fallbackOrder = action.DeserializeOrderFrom(gamedataTracker, regions, army.Owner, army, fallbackOrderRoot);
                }
            }

            var order = new ArmySkirmishOrder(army, null, worldManager, fallbackOrder, battleHelper);

            order._targetArmyId = orderRoot.GetString("target_army_id");
            order._skirmishAttackCooldown = orderRoot.GetDouble("skirmish_attack_cooldown");

            return order;
        }

        public override void SetAnimationFor(Army army, IUnit unit)
        {
            unit.SetAnimationForMovement(army, army.Cell.Traits);
        }

        public override string GetDescription()
        {
            return "Skirmishing";
        }
    }
}
