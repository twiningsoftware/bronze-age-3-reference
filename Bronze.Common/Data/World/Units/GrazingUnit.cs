﻿using Bronze.Common.Data.Game.Units;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.Data.World.Units
{
    public class GrazingUnit : AbstractUnit<GrazingUnitType>, IGrazingUnit
    {
        public override bool CanResupply => true;
        
        public ItemRate[] GrazingProduction => BaseType.GrazingProduction;
        public double GrazingRecovery => BaseType.GrazingRecovery;
        public double GrazingExhaustion => BaseType.GrazingExhaustion;
        public Trait[] GrazingRequirements => BaseType.GrazingRequirements;
        public Trait[] GrazingForbidden => BaseType.GrazingForbidden;
        public IEnumerable<BonusType> BonusedBy => BaseType.BonusedBy;
        public bool GrazeInRegion => BaseType.GrazeInRegion;
        public HaulerInfo[] LogiSourceHaulers => BaseType.LogiSourceHaulers;

        private readonly IGrazingManager _grazingManager;
        private readonly ILosTracker _losTracker;
        private readonly Random _random;

        public GrazingUnit(
            IGrazingManager grazingManager,
            ILosTracker losTracker)
        {
            _grazingManager = grazingManager;
            _losTracker = losTracker;
            _random = new Random();
        }

        public override void Update(double elapsedMonths, Army army)
        {
            base.Update(elapsedMonths, army);

            if(army.CurrentOrder is ArmyCampOrder campOrder && campOrder.IsCamped)
            {
                if (_grazingManager.CanGraze(army.Cell, this))
                {
                    _grazingManager.DoGrazing(army, this, elapsedMonths);
                }
                else
                {
                    var armyCampAction = army.AvailableActions
                        .OfType<ArmyCampAction>()
                        .FirstOrDefault();

                    if (armyCampAction != null)
                    {
                        var armyLos = army.Cell.OrthoNeighbors;

                        if(GrazeInRegion)
                        {
                            armyLos = army.Cell.Region.Cells;
                        }

                        var cellsToMoveTo = armyLos
                            .Where(c => _grazingManager.CanGraze(c, this))
                            .Where(c => armyCampAction.IsValidFor(army, c))
                            .Where(c => c.Region == army.Cell.Region || !GrazeInRegion)
                            .ToArray();

                        _random.Shuffle(cellsToMoveTo);
                        
                        if (cellsToMoveTo.Any())
                        {
                            army.CurrentOrder = armyCampAction.BuildOrderFor(
                                army, cellsToMoveTo.OrderBy(c => c.Position.DistanceSq(army.Cell.Position)).First());
                        }
                        else
                        {
                            cellsToMoveTo = armyLos
                                .Where(c => _grazingManager.CanGraze(c, this))
                                .Where(c => army.CanPath(c))
                                .Where(c => c.Region == army.Cell.Region || !GrazeInRegion)
                                .ToArray();

                            if (cellsToMoveTo.Any())
                            {
                                army.CurrentOrder = new ArmyMoveOrder(army, _random.Choose(cellsToMoveTo));
                            }
                        }
                    }
                }
            }
        }

        public IEnumerable<ItemRate> GetBonusedGrazingProduction(IntIndexableLookup<BonusType, double> currentBonuses)
        {
            var boostFactor = 1.0;

            foreach (var bonusType in BonusedBy)
            {
                boostFactor += currentBonuses[bonusType];
            }

            if(boostFactor == 1)
            {
                return GrazingProduction;
            }

            return GrazingProduction
                .Select(ir => new ItemRate(ir.Item, ir.PerMonth * boostFactor));
        }
    }
}
