﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bronze.Common.Data.Game.Units;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.Data.World.Units
{
    public class ArmyRaidOrder : AbstractArmyOrder
    {
        private readonly IWarHelper _warHelper;
        private Army _army;
        private Cell _target;

        public bool IsCamped { get; private set; }
        public override bool IsIdle => false;
        public override string ActionTypeName => typeof(ArmyRaidAction).Name;
        public double UpkeepPercent { get; set; }

        public ArmyRaidOrder(
            Army army,
            Cell target,
            IWarHelper warHelper)
        {
            _army = army;
            _target = target;
            _warHelper = warHelper;
        }

        public override void Simulate(Army army, double deltaMonths, AggregatePerformanceTracker aggregatePerformanceTracker)
        {
            army.CellPathCalculator.SetDestination(_target, false);

            base.Simulate(army, deltaMonths, aggregatePerformanceTracker);
            
            if(_target.Development == null
                || _target.Region.Settlement == null
                || !army.Owner.IsHostileTo(_target.Region.Settlement.Owner))
            {
                army.CurrentOrder = new IdleOrder();
            }
            else if(army.Cell == _target)
            {
                _target.Development.Devastation += deltaMonths * Constants.DEVELOPMENT_DEVASTATION_DAMAGE;

                foreach(var unit in army.Units)
                {
                    unit.Supplies = Math.Min(unit.MaxSupplies, unit.Supplies + deltaMonths * 2);
                }

                if(_target.Development.Devastation >= 1)
                {
                    var war = army.Owner.CurrentWars
                        .Where(w => w.Defender == _target.Region.Settlement?.Owner || w.Aggressor == _target.Region.Settlement?.Owner)
                        .FirstOrDefault();
                        
                    if(war != null)
                    {
                        _warHelper.RecordRaid(army, _target, war);
                    }

                    _target.Development.WasRaided();
                }
            }
        }
        
        public override void SerializeTo(SerializedObject orderRoot)
        {
            base.SerializeTo(orderRoot);

            _target.Position.SerializeTo(orderRoot.CreateChild("target_position"));
            orderRoot.Set("is_camped", IsCamped);
        }
        
        public override void SetAnimationFor(Army army, IUnit unit)
        {
            if (army.Cell == _target)
            {
                unit.SetAnimation(army, "melee".Yield());
            }
            else
            {
                unit.SetAnimationForMovement(army, army.Cell.Traits);
            }
        }

        public override string GetDescription()
        {
            if (_army.Cell != _target)
            {
                return "Moving to raid";
            }
            else if(_target.Development != null)
            {
                return $"Raiding {(int)(_target.Development.Devastation * 100)}%";
            }
            else
            {
                return $"Raiding";
            }
        }

        public static IArmyOrder DeserializeFrom(
            IEnumerable<Region> regions,
            Army army,
            SerializedObject orderRoot,
            IWarHelper warHelper)
        {
            var targetPos = CellPosition.DeserializeFrom(orderRoot.GetChild("target_position"));

            var target = regions.SelectMany(r => r.Cells)
                .Where(c => c.Position == targetPos)
                .FirstOrDefault();

            if(target != null)
            {
                return new ArmyRaidOrder(army, target, warHelper);
            }

            return new IdleOrder();
        }
    }
}
