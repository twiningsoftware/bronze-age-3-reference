﻿using Bronze.Contracts.Data.World;
using System.Collections.Generic;

namespace Bronze.Common.Data.World.CellDevelopments
{
    public interface IServiceProviderDevelopment : ICellDevelopment, IServiceProviderActor
    {
        List<Cell> CellsServed { get; }

        IEnumerable<Cell> CalculateServiceArea();
    }
}
