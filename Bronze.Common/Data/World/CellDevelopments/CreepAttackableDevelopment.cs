﻿using Bronze.Common.Data.Game.CellDevelopments;
using Bronze.Common.InjectableServices;
using Bronze.Common.UI.Modals;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Elements;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.Data.World.CellDevelopments
{
    public class CreepAttackableDevelopment : AbstractCellDevelopment<CreepAttackableDevelopmentType>, IArmyInteractable
    {
        public override bool CanBeSieged => false;
        public override bool CanBeDestroyed => false;

        public string InteractVerb => "Attacking";
        public bool InteractAdjacent => false;
        public bool InteractWithForeign => true;
        
        public double DefenderStrength { get; set; }

        public override int VisionRange => BaseType.VisionRange;
        
        public IUnitType DefenderUnit => BaseType.DefenderUnit;
        public Race DefenderRace => BaseType.DefenderRace;
        public int DefenderCount => BaseType.DefenderCount;

        private readonly IDialogManager _dialogManager;
        private readonly ICreepBanditManager _creepBanditManager;

        public CreepAttackableDevelopment(
            IDialogManager dialogManager, 
            IGamedataTracker gamedataTracker,
            ICreepBanditManager creepBanditManager,
            ISimulationEventBus simulationEventBus)
            : base(dialogManager, gamedataTracker, simulationEventBus)
        {
            _dialogManager = dialogManager;
            _creepBanditManager = creepBanditManager;
        }
        
        public override AbstractCellDevelopment<CreepAttackableDevelopmentType> Initialize(
            Cell cell,
            CreepAttackableDevelopmentType baseType,
            bool instantBuild)
        {
            base.Initialize(cell, baseType, instantBuild);
            DefenderStrength = 1;

            return this;
        }
        
        public override void DoUpdate(double deltaMonths)
        {
            base.DoUpdate(deltaMonths);

            DefenderStrength = Math.Min(1, DefenderStrength + deltaMonths / 3);
        }

        public override IStackUiElement BuildDetailColumn(bool playerOwned, bool playerSees)
        {
            var stackGroup = base.BuildDetailColumn(playerOwned, playerSees);

            stackGroup.Children.Add(new Label("Hostile", BronzeColor.Red));

            return stackGroup;
        }

        public override IStackUiElement BuildHoverInfoColumn(bool playerOwns, bool playerSees)
        {
            var stackGroup = base.BuildHoverInfoColumn(playerOwns, playerSees);

            stackGroup.Children.Add(new Label("Hostile", BronzeColor.Red));
            
            return stackGroup;
        }
        
        public void DoInteraction(Army interactingArmy)
        {
            _creepBanditManager.HandleArmyAttack(this, interactingArmy);
        }

        public override void SerializeTo(SerializedObject developmentRoot)
        {
            base.SerializeTo(developmentRoot);
            
            developmentRoot.Set("defender_strength", DefenderStrength);
        }

        public override void DeserializeFrom(
            IGamedataTracker gamedataTracker, 
            SerializedObject developmentRoot)
        {
            base.DeserializeFrom(gamedataTracker, developmentRoot);
            
            DefenderStrength = developmentRoot.GetDouble("defender_strength");
        }
    }
}
