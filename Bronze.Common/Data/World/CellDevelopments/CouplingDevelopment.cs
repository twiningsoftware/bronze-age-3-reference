﻿using Bronze.Common.Data.Game.CellDevelopments;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Elements;
using System.Linq;

namespace Bronze.Common.Data.World.CellDevelopments
{
    public class CouplingDevelopment : AbstractCellDevelopment<CouplingDevelopmentType>, IArmyInteractable
    {
        public override bool CanBeSieged => false;
        public override bool CanBeDestroyed => false;

        public string InteractVerb => "Entering";
        public bool InteractAdjacent => true;
        public bool InteractWithForeign => true;

        public CellPosition DestinationPos { get; set; }

        private readonly IWorldManager _worldManager;
        private readonly IPlayerDataTracker _playerData;

        public CouplingDevelopment(
            IDialogManager dialogManager, 
            IGamedataTracker gamedataTracker,
            IWorldManager worldManager,
            IPlayerDataTracker playerData,
            ISimulationEventBus simulationEventBus)
            : base(dialogManager, gamedataTracker, simulationEventBus)
        {
            _worldManager = worldManager;
            _playerData = playerData;
        }

        public void DoInteraction(Army interactingArmy)
        {
            var destination = _worldManager.GetCell(DestinationPos);

            interactingArmy.Owner.WorldExplored[destination.Position.Plane, destination.Position.X, destination.Position.Y] = true;

            if (destination != null && !interactingArmy.CanPath(destination))
            {
                destination = destination.OrthoNeighbors
                    .Where(c => interactingArmy.CanPath(c))
                    .FirstOrDefault();
            }

            if (destination != null)
            {
                if(interactingArmy.Owner == _playerData.PlayerTribe
                    && _playerData.CurrentPlane == interactingArmy.Cell.Position.Plane
                    && _playerData.ViewMode != ViewMode.Settlement
                    && (_playerData.ViewCenter - interactingArmy.Position * Constants.CELL_SIZE_PIXELS).Length() < 800)
                {
                    _playerData.LookAt(DestinationPos);
                }

                interactingArmy.MoveTo(destination);
            }
        }

        public override IStackUiElement BuildHoverInfoColumn(bool playerOwns, bool playerSees)
        {
            var stack = base.BuildHoverInfoColumn(playerOwns, playerSees);

            var destination = _worldManager.GetCell(DestinationPos);

            if (destination != null)
            {
                stack.Children.Add(new TextButton(
                    "View Destination",
                    () =>
                    {
                        _playerData.LookAt(DestinationPos);
                        _playerData.SelectedCell = destination;
                    }));
            }

            return stack;
        }

        public override IStackUiElement BuildDetailColumn(bool playerOwned, bool playerSees)
        {
            var stack = base.BuildDetailColumn(playerOwned, playerSees);

            stack.Children.Add(new TextButton(
                "View Destination",
                () => _playerData.LookAt(DestinationPos)));

            return stack;
        }

        public override void SerializeTo(SerializedObject developmentRoot)
        {
            base.SerializeTo(developmentRoot);

            DestinationPos.SerializeTo(developmentRoot.CreateChild("destination_pos"));
        }

        public override void DeserializeFrom(IGamedataTracker gamedataTracker, SerializedObject developmentRoot)
        {
            base.DeserializeFrom(gamedataTracker, developmentRoot);

            DestinationPos = CellPosition.DeserializeFrom(developmentRoot.GetChild("destination_pos"));
        }
    }
}
