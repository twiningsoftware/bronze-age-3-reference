﻿using System;
using System.Collections.Generic;
using Bronze.Common.Data.Game.CellDevelopments;
using Bronze.Common.UI;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;

namespace Bronze.Common.Data.World.CellDevelopments
{
    public abstract class AbstractServiceProviderDevelopment<TDevelopmentType>
        : AbstractCellDevelopment<TDevelopmentType>, IServiceProviderDevelopment where TDevelopmentType : AbstractServiceProviderDevelopmentType
    {
        public List<Cell> CellsServed { get; }
        public ServiceType ServiceType => BaseType.ServiceType;
        public List<IEconomicActor> ActorsServed { get; }
        public double ServiceRange => BaseType.ServiceRange;
        public double ServicePercent => Math.Min(UpkeepPercent, WorkerPercent);
        public IEnumerable<Trait> ServiceTransmissionTraits => BaseType.ServiceTransmissionTraits;

        protected AbstractServiceProviderDevelopment(
            IDialogManager dialogManager,
            IGamedataTracker gamedataTracker,
            ISimulationEventBus simulationEventBus)
            : base(dialogManager, gamedataTracker, simulationEventBus)
        {
            CellsServed = new List<Cell>();
            ActorsServed = new List<IEconomicActor>();
        }
        
        public virtual void AddServicedActor(IEconomicActor actorToService)
        {
            ActorsServed.Add(actorToService);
            actorToService.ServiceProviders.Add(this);
        }

        public virtual IEnumerable<Cell> CalculateServiceArea()
        {
            // We're not just setting CellsServed here becuase this calculation will happen in a 
            // calculator, and will be applied at a later time.
            return BaseType.CalculateServiceArea(Cell);
        }

        public virtual bool CanService(IEconomicActor economicActor)
        {
            return BaseType.CanService(economicActor);
        }

        public virtual bool CanService(IEconomicActorType economicActorType)
        {
            return BaseType.CanService(economicActorType);
        }

        public virtual void ClearServicedActors()
        {
            foreach (var actor in ActorsServed)
            {
                actor.ServiceProviders.RemoveAll(s => s == this);
            }
            ActorsServed.Clear();
        }

        public override void OnRemoval()
        {
            base.OnRemoval();
            ClearServicedActors();
        }

        public override IStackUiElement BuildDetailColumn(bool playerOwned, bool playerSees)
        {
            var stack = base.BuildDetailColumn(playerOwned, playerSees);
            if (playerSees)
            {
                stack.Children.Add(UiBuilder.BuildServiceProvidedDetailRow(ServiceType, ServiceRange, () => ServicePercent));
            }
            return stack;
        }

        public override IStackUiElement BuildHoverInfoColumn(bool playerOwns, bool playerSees)
        {
            var stack = base.BuildHoverInfoColumn(playerOwns, playerSees);
            if (playerSees)
            {
                stack.Children.Add(UiBuilder.BuildServiceProvidedSummaryRow(ServiceType, () => ServicePercent));
            }
            return stack;
        }
    }
}
