﻿using System.Collections.Generic;
using System.Linq;
using Bronze.Common.Data.Game;
using Bronze.Common.Data.Game.CellDevelopments;
using Bronze.Common.UI;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.Trade;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Elements;

namespace Bronze.Common.Data.World.CellDevelopments
{
    public class VillageDevelopment : AbstractCellDevelopment<VillageDevelopmentType>, IHousingProvider, ITradeReciever
    {
        public IEconomicActor Distributor { get; set; }
        public double EffectiveTaxRate { get; set; }
        public List<Pop> Residents { get; }
        public Caste SupportedCaste => BaseType.SupportedCaste;
        public int ProvidedHousing => BaseType.SupportedPops;
        public IEnumerable<PopNeedState> PopNeeds { get; private set; }
        public IEnumerable<TradeCellExit> CellExits => _cellExits;
        public double AverageSatisfaction { get; private set; }        
        public List<TradeRoute> TradeRoutes { get; set; }
        public override bool CanBeSieged => true;
        public override bool CanBeDestroyed => false;
        public IntIndexableLookup<Cult, double> InfluencePerPop { get; private set; }

        private readonly IPlayerDataTracker _playerData;
        private readonly IDialogManager _dialogManager;
        private readonly IGamedataTracker _gamedataTracker;
        private readonly ITradeManager _tradeManager;
        
        private ItemRate[] _itemNeeds;
        private double[] _itemNeedLookup;

        private TradeCellExit[] _cellExits;

        public VillageDevelopment(
            IDialogManager dialogManager,
            IPlayerDataTracker playerData,
            IGamedataTracker gamedataTracker,
            ISimulationEventBus simulationEventBus,
            ITradeManager tradeManager)
            : base(dialogManager, gamedataTracker, simulationEventBus)
        {
            _dialogManager = dialogManager;
            _playerData = playerData;
            _gamedataTracker = gamedataTracker;
            _tradeManager = tradeManager;

            Residents = new List<Pop>();
            PopNeeds = new PopNeedState[0];
            AverageSatisfaction = 1;
            TradeRoutes = new List<TradeRoute>();
            _cellExits = new TradeCellExit[0];

            _itemNeeds = new ItemRate[0];
            _itemNeedLookup = new double[_gamedataTracker.Items.Max(i => i.LookupIndex) + 1];
            InfluencePerPop = new IntIndexableLookup<Cult, double>();
        }

        public override AbstractCellDevelopment<VillageDevelopmentType> Initialize(
            Cell cell, 
            VillageDevelopmentType baseType,
            bool instantBuild)
        {
            base.Initialize(cell, baseType, instantBuild);
            
            PopNeeds = BaseType.MaintenanceNeeds
                .Select(n => new PopNeedState(n, false, false))
                .ToArray();
            
            return this;
        }
        
        public override void DoUpdate(double deltaMonths)
        {
            base.DoUpdate(deltaMonths);

            if (!_cellExits.Any())
            {
                _cellExits = new TradeCellExit(Cell, null).Yield()
                    .Concat(Cell.OrthoNeighbors
                        .Select(x => new TradeCellExit(x, Cell)))
                    .ToArray();
            }
            
            var newNeedsLookup = new double[_itemNeedLookup.Length];

            foreach(var need in PopNeeds)
            {
                if(need.Need is ItemPopNeed itemNeed)
                {
                    newNeedsLookup[itemNeed.Item.LookupIndex] += need.Residents * itemNeed.PerMonthPerPop;
                }
            }
            
            var needsChanged = false;

            for (var i = 0; i < _itemNeedLookup.Length && !needsChanged; i++)
            {
                needsChanged = needsChanged || _itemNeedLookup[i] != newNeedsLookup[i];
            }

            if (needsChanged)
            {
                _itemNeedLookup = newNeedsLookup;

                _itemNeeds = _gamedataTracker.Items
                    .Where(i => newNeedsLookup[i.LookupIndex] > 0)
                    .Select(i => new ItemRate(i, newNeedsLookup[i.LookupIndex]))
                    .ToArray();

                Cell.Region.Settlement.SetCalculationFlag("village needs changed");
            }
            
            foreach (var popneed in PopNeeds)
            {
                popneed.Residents = Residents.Count;
                popneed.Update(this, deltaMonths);
            }
            
            var taxMod = 1 - EffectiveTaxRate;

            var maintenenceNeeds = PopNeeds
                .Where(n => !n.IsUpgradeNeed && !n.IsBonusNeed)
                .ToArray();

            var bonusNeeds = PopNeeds
                .Where(n => n.IsBonusNeed)
                .ToArray();
            
            var bonusFactor = 1.0;
            if (bonusNeeds.Any())
            {
                bonusFactor += bonusNeeds.Average(n => n.Satisfaction);
            }

            var growthBonus = 1.0;
            foreach (var bonusType in SupportedCaste.GrowthBonusedBy)
            {
                growthBonus += CurrentBonuses[bonusType];
            }

            var satisfaction = 0.0;
            if (!maintenenceNeeds.Any())
            {
                satisfaction = 1 * taxMod * bonusFactor;
            }
            else
            {
                satisfaction = maintenenceNeeds.Average(n => n.Satisfaction) * taxMod * bonusFactor;
            }

            if (Settlement.Governor != null)
            {
                satisfaction += Settlement.Governor.GetBonusSatisfaction();
            }

            foreach (var bonusType in SupportedCaste.SatisfactionBonusedBy)
            {
                satisfaction += CurrentBonuses[bonusType];
            }

            foreach (var pop in Residents)
            {
                pop.AddSatisfaction(deltaMonths, satisfaction);
                pop.GrowthBonus = growthBonus;
            }

            if (Residents.Any())
            {
                AverageSatisfaction = Residents.Select(r => r.Satisfaction).Average();
            }
            else
            {
                AverageSatisfaction = 1;
            }
        }

        public override IEnumerable<ItemRate> ExpectedOutputs
        {
            get
            {
                var expectedOutputs = base.ExpectedOutputs;

                if(!UnderConstruction)
                {
                    expectedOutputs = expectedOutputs.Concat(TradeRoutes.SelectMany(r => r.Exports));
                }

                return expectedOutputs;
            }
        }

        public override IEnumerable<ItemRate> NeededInputs
        {
            get
            {
                var neededInputs = base.NeededInputs;
                
                if(!UnderConstruction)
                {
                    neededInputs = neededInputs
                        .Concat(_itemNeeds);
                }

                return neededInputs;
            }
        }
        
        public double NeedPerMonthOf(Item item)
        {
            return _itemNeedLookup[item.LookupIndex];
        }

        public override IStackUiElement BuildDetailColumn(bool playerOwned, bool playerSees)
        {
            var stackGroup = base.BuildDetailColumn(playerOwned, playerSees);

            if (playerSees)
            {
                if (!UnderConstruction)
                {
                    stackGroup.Children.Add(new Label("Housing"));
                    stackGroup.Children.Add(new IconText(SupportedCaste.IconKey, $"{Residents.Count} / {ProvidedHousing}", tooltip: $"{SupportedCaste.Name} housing provided."));
                    stackGroup.Children.Add(new Spacer(0, 10));
                    stackGroup.Children.Add(new Label("")
                    {
                        ContentUpdater = () =>
                        {
                            return $"Satisfaction {(int)(AverageSatisfaction * 100)}%";
                        }
                    });

                    stackGroup.Children.Add(new Spacer(0, 10));

                    if (BaseType.MaintenanceNeeds.Any())
                    {
                        stackGroup.Children.Add(new Label("Needs"));

                        foreach (var popneed in PopNeeds.Where(n => !n.IsUpgradeNeed))
                        {
                            stackGroup.Children.Add(UiBuilder.BuildDisplayFor(this, popneed));
                        }
                    }

                    stackGroup.Children.Add(new Spacer(0, 10));

                    if (TradeRoutes.Any())
                    {
                        var importSettlements = TradeRoutes
                            .Select(t => t.FromSettlement)
                            .Distinct()
                            .ToArray();

                        if(importSettlements.Length == 1)
                        {
                            stackGroup.Children.Add(new Label($"Importing from {importSettlements.First().Name}"));
                        }
                        else
                        {
                            stackGroup.Children.Add(new Label($"Importing from {importSettlements.Length} settlements"));
                        }

                        stackGroup.Children.Add(UiBuilder.BuildDisplayFor(TradeRoutes.SelectMany(t => t.Exports), 5));
                    }
                    else
                    {
                        stackGroup.Children.Add(new Label($"Not trading"));
                    }
                }
            }

            return stackGroup;
        }
        
        public override IStackUiElement BuildHoverInfoColumn(bool playerOwns, bool playerSees)
        {
            var stack = base.BuildHoverInfoColumn(playerOwns, playerSees);

            if (!UnderConstruction && playerSees && Settlement != null)
            {
                stack.Children.Add(new Label("Under Siege", BronzeColor.Red, () => Settlement.IsUnderSiege));

                stack.Children.AddRange(new List<IUiElement>
                    {
                        new IconText(SupportedCaste.IconKey, "/", tooltip: $"{SupportedCaste.Name} housing provided.")
                        {
                            ContentUpdater = () => $"{Residents.Count} / {ProvidedHousing}"
                        },
                        new Spacer(0, 5),
                        new Label("")
                        {
                            ContentUpdater = () =>
                            {
                                return $"Satisfaction {(int)(AverageSatisfaction * 100)}%";
                            }
                        }
                    });
            }
            
            return stack;
        }

        public override void OnRemoval()
        {
            base.OnRemoval();

            foreach (var pop in Residents)
            {
                pop.Home = null;
            }
        }

        public void OnTradePathChanged()
        {
        }

        public void OnTradeRoutesChanged()
        {
        }
        
        public override bool AllowsAccess(Army army)
        {
            return army.Owner == Settlement?.Owner;
        }

        public override void OnUpgradeFinished(ICellDevelopment newDevelopment)
        {
            base.OnUpgradeFinished(newDevelopment);

            if (newDevelopment is ITradeReciever newTradeReciever)
            {
                foreach (var tradeRoute in TradeRoutes.ToArray())
                {
                    _tradeManager.ReparentTradeRoute(this, newTradeReciever, tradeRoute);
                }
            }
        }
    }
}
