﻿using Bronze.Common.Data.Game.CellDevelopments;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.Data.World.CellDevelopments
{
    public class BarracksDevelopment : AbstractCellDevelopment<BarracksDevelopmentType>, IRecruitmentSlotProvider, IArmoryStorageProvider
    {
        private readonly IGamedataTracker _gamedataTracker;
        private IntIndexableLookup<Item, bool> _itemsStored;

        public RecruitmentSlotCategory RecruitmentCategory => BaseType.RecruitmentCategory;
        public int ProvidedRecruitmentSlots => UnderConstruction ? 0 : (int)(BaseType.ProvidedRecruitmentSlots * UpkeepPercent * WorkerPercent);
        public int MaximumProvidedRecruitmentSlots => BaseType.ProvidedRecruitmentSlots;
        public override bool CanBeSieged => false;

        public BarracksDevelopment(
            IDialogManager dialogManager, 
            IGamedataTracker gamedataTracker,
            ISimulationEventBus simulationEventBus)
            : base(dialogManager, gamedataTracker, simulationEventBus)
        {
            _gamedataTracker = gamedataTracker;
            _itemsStored = new IntIndexableLookup<Item, bool>();
        }

        public override IEnumerable<ItemRate> NeededInputs
        {
            get
            {
                var inputs = base.NeededInputs;

                if (!UnderConstruction)
                {
                    inputs = inputs.Concat(_gamedataTracker.Items
                        .Where(i => _itemsStored[i])
                        .Select(i => new ItemRate(i, 1)))
                        .ToArray();
                }

                return inputs;
            }
        }
        
        public override AbstractCellDevelopment<BarracksDevelopmentType> Initialize(Cell cell, BarracksDevelopmentType baseType, bool instantBuild)
        {
            base.Initialize(cell, baseType, instantBuild);

            foreach (var item in _gamedataTracker.Items)
            {
                _itemsStored[item] = BaseType.ItemsAllowed[item];
            }

            return this;
        }

        public override IStackUiElement BuildDetailColumn(bool playerOwned, bool playerSees)
        {
            var stackGroup = base.BuildDetailColumn(playerOwned, playerSees);

            if (!UnderConstruction)
            {
                stackGroup.Children.Add(new Label($"Providing {ProvidedRecruitmentSlots} {RecruitmentCategory.Name} recruits."));

                stackGroup.Children.Add(new Spacer(0, 10));
                stackGroup.Children.Add(new Label("Stored Items"));
                stackGroup.Children.Add(new Spacer(0, 5));

                const int itemsPerRow = 6;

                var knownItems = _gamedataTracker.Items
                    .Where(i => Settlement.Owner.KnownItems[i])
                    .Where(i => BaseType.ItemsAllowed[i])
                    .ToArray();

                for (var i = 0; i < knownItems.Length; i += itemsPerRow)
                {
                    stackGroup.Children.Add(new StackGroup
                    {
                        Orientation = Orientation.Horizontal,
                        Children = knownItems
                            .Skip(i)
                            .Take(itemsPerRow)
                            .Select(item => new IconButton(
                                item.IconKeyBig,
                                string.Empty,
                                () =>
                                {
                                    if (_itemsStored[item])
                                    {
                                        _itemsStored[item] = false;
                                    }
                                    else
                                    {
                                        _itemsStored[item] = true;
                                    }

                                    Settlement.SetCalculationFlag("storage items changed");
                                },
                                tooltip: item.Name,
                                disableCheck: () => !BaseType.ItemsAllowed[item],
                                highlightCheck: () => _itemsStored[item]))
                            .ToList<IUiElement>()
                    });
                }
            }

            return stackGroup;
        }

        public override IStackUiElement BuildHoverInfoColumn(bool playerOwns, bool playerSees)
        {
            var stackGroup = base.BuildHoverInfoColumn(playerOwns, playerSees);

            if (!UnderConstruction && playerSees)
            {
                stackGroup.Children.Add(new Label($"Providing {ProvidedRecruitmentSlots} {RecruitmentCategory.Name} recruits."));
            }

            return stackGroup;
        }

        public bool CanStore(Item item)
        {
            return _itemsStored[item];
        }
    }
}
