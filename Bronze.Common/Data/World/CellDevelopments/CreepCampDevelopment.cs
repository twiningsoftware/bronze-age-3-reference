﻿using Bronze.Common.Data.Game.CellDevelopments;
using Bronze.Common.InjectableServices;
using Bronze.Common.UI.Modals;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Elements;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.Data.World.CellDevelopments
{
    public class CreepCampDevelopment : AbstractCellDevelopment<CreepCampDevelopmentType>, IRecruitmentSlotProvider, IArmyInteractable, IRecruitableUnitTypeSource
    {
        public override bool CanBeSieged => false;
        public override bool CanBeDestroyed => false;

        public string InteractVerb => "Attacking";
        public bool InteractAdjacent => false;
        public bool InteractWithForeign => true;

        public bool IsBeingPacified => SelectedPacificationOption != null;
        public bool PacificationIncreasing { get; set; }
        public double PacificationLevel { get; set; }
        public double DefenderStrength { get; set; }

        public override int VisionRange => IsPacified ? BaseType.VisionRange : 0;

        public bool IsPacified => PacificationLevel >= 1;
        public double PacificationMonths => BaseType.PacificationMonths;
        public double PacificationPercent => PacificationLevel / Math.Max(1, PacificationMonths);
        public RecruitmentSlotCategory RecruitmentCategory => BaseType.RecruitmentCategory;
        public int ProvidedRecruitmentSlots => IsPacified ?  BaseType.ProvidedRecruitmentSlots : 0;
        public int MaximumProvidedRecruitmentSlots => BaseType.ProvidedRecruitmentSlots;
        public IEnumerable<IUnitType> ProvidedUnitTypes => IsPacified ? BaseType.MercenaryUnitTypes : Enumerable.Empty<IUnitType>();
        public string DialogContext => BaseType.DialogContext;

        public CreepCampDevelopmentType.PacificationOption[] PacificationOptions => BaseType.PacificationOptions;
        private CreepCampDevelopmentType.PacificationOption _selectedPacificationOption;
        
        public double BanditSpawnTimeMin => BaseType.BanditSpawnTimeMin;
        public double BanditSpawnTimeMax => BaseType.BanditSpawnTimeMax;
        public IUnitType BanditUnit => BaseType.BanditUnit;
        public Race BanditRace => BaseType.BanditRace;
        public int BanditCountMin => BaseType.BanditCountMin;
        public int BanditCountMax => BaseType.BanditCountMax;

        private readonly IDialogManager _dialogManager;
        private readonly ICreepBanditManager _creepBanditManager;

        public CreepCampDevelopment(
            IDialogManager dialogManager, 
            IGamedataTracker gamedataTracker,
            ICreepBanditManager creepBanditManager,
            ISimulationEventBus simulationEventBus)
            : base(dialogManager, gamedataTracker, simulationEventBus)
        {
            _dialogManager = dialogManager;
            _creepBanditManager = creepBanditManager;
        }

        public override IEnumerable<ItemRate> NeededInputs
        {
            get
            {
                if(IsBeingPacified)
                {
                    return base.NeededInputs.Concat(SelectedPacificationOption.Inputs);
                }

                return base.NeededInputs;
            }
        }

        public CreepCampDevelopmentType.PacificationOption SelectedPacificationOption
        {
            get => _selectedPacificationOption;
            set
            {
                _selectedPacificationOption = value;

                if(Settlement != null)
                {
                    Settlement.SetCalculationFlag("pacification_option_changed");
                }

                if(_selectedPacificationOption != null)
                {
                    PacificationLevel = Math.Max(PacificationLevel, -.9);
                }
            }
        }

        public override AbstractCellDevelopment<CreepCampDevelopmentType> Initialize(
            Cell cell,
            CreepCampDevelopmentType baseType,
            bool instantBuild)
        {
            base.Initialize(cell, baseType, instantBuild);
            _creepBanditManager.Register(this);
            DefenderStrength = 1;

            return this;
        }

        public override void OnRemoval()
        {
            base.OnRemoval();

            _creepBanditManager.Deregister(this);
        }

        public override void DoUpdate(double deltaMonths)
        {
            base.DoUpdate(deltaMonths);

            DefenderStrength = Math.Min(1, DefenderStrength + deltaMonths / BanditSpawnTimeMin);

            if(IsBeingPacified)
            {
                var workRate = deltaMonths * WorkerPercent;

                var wasPacified = IsPacified;

                var pacificationRate = Util.PullInputs(this, SelectedPacificationOption.Inputs, deltaMonths);
                var pacificationPercent = pacificationRate / deltaMonths;
                var deltaPacification = 0.0;
                if(pacificationPercent < 1)
                {
                    PacificationIncreasing = false;
                    deltaPacification = -deltaMonths * (1 - pacificationPercent);
                }
                else
                {
                    PacificationIncreasing = true;
                    deltaPacification = deltaMonths;
                }
                
                PacificationLevel = Util.Clamp(PacificationLevel + deltaPacification, -1, 1);

                if(!wasPacified && IsPacified)
                {
                    Settlement.SetCalculationFlag("pacification_finished");
                }
                else if (wasPacified && !IsPacified)
                {
                    Settlement.SetCalculationFlag("pacification_failed");
                }

                if(PacificationLevel <= -1)
                {
                    SelectedPacificationOption = null;
                }
            }
        }

        public override IStackUiElement BuildDetailColumn(bool playerOwned, bool playerSees)
        {
            var stackGroup = base.BuildDetailColumn(playerOwned, playerSees);

            stackGroup.Children.Add(
                new StackGroup
                {
                    Orientation = Bronze.UI.Data.Orientation.Vertical,
                    HideCheck = () => !playerSees || IsBeingPacified,
                    Children = new List<IUiElement>
                    {
                        new Label("Hostile", BronzeColor.Red)
                    }
                });

            stackGroup.Children.Add(
                new StackGroup
                {
                    Orientation = Bronze.UI.Data.Orientation.Vertical,
                    HideCheck = () => !playerSees || !IsBeingPacified,
                    Children = new List<IUiElement>
                    {
                        new Label(string.Empty)
                        {
                            ContentUpdater = () => IsPacified ? "Pacified" : $"Being Pacified: {(int)(100 * PacificationPercent)}%"
                        },
                        new Label(string.Empty)
                        {
                            ContentUpdater = () => IsPacified ? $"Providing {ProvidedRecruitmentSlots} {RecruitmentCategory.Name} recruits." : string.Empty
                        }
                    }
                });

            if (playerOwned)
            {
                stackGroup.Children.Add(new Spacer(0, 10));
                stackGroup.Children.Add(new TextButton(
                    "Talk",
                    () => _dialogManager.PushModal<CreepCampTalkModal, CreepCampDevelopment>(this)));
            }

            return stackGroup;
        }

        public override IStackUiElement BuildHoverInfoColumn(bool playerOwns, bool playerSees)
        {
            var stackGroup = base.BuildHoverInfoColumn(playerOwns, playerSees);

            stackGroup.Children.Add(
                new StackGroup
                {
                    Orientation = Bronze.UI.Data.Orientation.Vertical,
                    HideCheck = () => !playerSees || IsBeingPacified,
                    Children = new List<IUiElement>
                    {
                        new Label("Hostile", BronzeColor.Red)
                    }
                });

            stackGroup.Children.Add(
                new StackGroup
                {
                    Orientation = Bronze.UI.Data.Orientation.Vertical,
                    HideCheck = () => !playerSees || !IsBeingPacified,
                    Children = new List<IUiElement>
                    {
                        new Label(string.Empty)
                        {
                            ContentUpdater = () => IsPacified ? "Pacified" : $"Being Pacified: {(int)(100 * PacificationPercent)}%"
                        },
                        new Label(string.Empty)
                        {
                            ContentUpdater = () => IsPacified ? $"Providing {ProvidedRecruitmentSlots} {RecruitmentCategory.Name} recruits." : string.Empty
                        }
                    }
                });

            if(playerOwns)
            {
                stackGroup.Children.Add(new TextButton(
                    "Talk",
                    () => _dialogManager.PushModal<CreepCampTalkModal, CreepCampDevelopment>(this)));
            }
            
            return stackGroup;
        }

        public bool CanPlaceOffshoot(Cell cell)
        {
            return BaseType.CanBePlaced(null, cell);
        }

        public void PlaceOffshoot(Cell cell)
        {
            BaseType.Place(null, cell, true);
        }

        public void DoInteraction(Army interactingArmy)
        {
            _creepBanditManager.HandleArmyAttack(this, interactingArmy);
        }

        public override void SerializeTo(SerializedObject developmentRoot)
        {
            base.SerializeTo(developmentRoot);

            developmentRoot.Set("selected_pacification_option", SelectedPacificationOption?.Id);
            developmentRoot.Set("pacification_level", PacificationLevel);
            developmentRoot.Set("defender_strength", DefenderStrength);
        }

        public override void DeserializeFrom(
            IGamedataTracker gamedataTracker, 
            SerializedObject developmentRoot)
        {
            base.DeserializeFrom(gamedataTracker, developmentRoot);

            SelectedPacificationOption = developmentRoot.GetOptionalObjectReference(
                "selected_pacification_option",
                PacificationOptions,
                x => x.Id);
            PacificationLevel = developmentRoot.GetDouble("pacification_level");
            DefenderStrength = developmentRoot.GetDouble("defender_strength");
        }
    }
}
