﻿using Bronze.Common.Data.Game.CellDevelopments;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.Data.World.CellDevelopments
{
    public class SimpleDevelopment : AbstractCellDevelopment<SimpleDevelopmentType>
    {
        public override bool CanBeSieged => false;

        public SimpleDevelopment(
            IDialogManager dialogManager, 
            IGamedataTracker gamedataTracker,
            ISimulationEventBus simulationEventBus)
            : base(dialogManager, gamedataTracker, simulationEventBus)
        {
        }
    }
}
