﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bronze.Common.UI;
using Bronze.Common.UI.Elements;
using Bronze.Common.UI.Modals;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.Simulation.Events;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;

namespace Bronze.Common.Data.World.CellDevelopments
{
    public abstract class AbstractCellDevelopment<TCellDevelopmentType> : ICellDevelopment where TCellDevelopmentType : class, ICellDevelopmentType
    {
        public string Id { get; private set; }
        public string TypeId => BaseType.Id;
        public virtual string Name => BaseType.Name;
        public virtual string[] AdjacencyGroups => BaseType.AdjacencyGroups;
        public virtual IEnumerable<Trait> Traits => BaseType.Traits;
        public virtual IEnumerable<Trait> NotTraits => BaseType.NotTraits;
        public virtual IEnumerable<CellAnimationLayer> DetailLayers => UnderConstruction ? BaseType.ConstructionDetailLayers : BaseType.DetailLayers;
        public virtual IEnumerable<CellAnimationLayer> SummaryLayers => UnderConstruction ? BaseType.ConstructionSummaryLayers: BaseType.SummaryLayers;
        public virtual bool CanBeDestroyed => true;
        
        public virtual WorkerNeed WorkerNeed => UnderConstruction ? BaseType.ConstructionWorkers : BaseType.OperationWorkers;
        public int WorkerSupply { get; set; }
        public bool UnderConstruction { get; set; }
        public double ConstructionProgress { get; set; }
        public virtual double ConstructionMonths => BaseType.ConstructionMonths;
        public bool IsUpgrading => UpgradingTo != null;
        public double UpgradeProgress { get; set; }
        public double UpgradeMonths => UpgradingTo?.ConstructionMonths ?? 0;
        public ICellDevelopmentType UpgradingTo { get; private set; }
        public double WorkerPercent => WorkerNeed.Need > 0 ? WorkerSupply / (double)WorkerNeed.Need : 1;
        public ItemInventory Inventory { get; private set; }
        public int InventoryCapacity => BaseType.InventoryCapacity;
        public IEnumerable<Recipie> AvailableRecipies => BaseType.Recipies;
        public List<DeliveryRoute> DeliveryRoutes { get; private set; }
        public IEnumerable<HaulerInfo> LogiSourceHaulers => BaseType.LogiSourceHaulers;
        public virtual int VisionRange => BaseType.VisionRange;
        public IEnumerable<ICellDevelopmentType> ManuallyUpgradesTo => BaseType.ManuallyUpgradesTo;
        public bool CanManuallyUpgrade => !UnderConstruction && !IsUpgrading && ManuallyUpgradesTo.Any(u => u.CanBePlaced(Cell.Region.Owner, Cell));
        public bool CanManuallyDowngrade => !UnderConstruction && !IsUpgrading && ManuallyDowngradesTo != null;
        public ICellDevelopmentType ManuallyDowngradesTo => BaseType.ManuallyDowngradesTo;
        public bool IsRemoved { get; set; }
        private FactoryState _factoryState;
        public List<Item> MissingItems { get; }
        public IntIndexableLookup<Item, double> PerItemSatisfaction { get; }
        public Settlement Settlement => Cell?.Region?.Settlement;
        public IEnumerable<ItemRate> UpkeepInputs => BaseType.UpkeepInputs;
        public double UpkeepPercent { get; set; }
        public Cell Cell { get; private set; }
        public IEnumerable<Trait> NetTraits => Cell.Traits;
        public bool IsCellLevel => true;
        public double ProductionCheckWait { get; set; }
        public double ItemTransmissionWait { get; set; }
        public double ProductionProgress { get; set; }
        public double Devastation { get; set; }
        public bool CanBeRaided => BaseType.RaidedBehavior.AbandonmentType != AbandonmentType.NoChange;
        public abstract bool CanBeSieged { get; }
        public IntIndexableLookup<Item, bool> ItemsProduced { get; }
        public IntIndexableLookup<Item, bool> ItemsConsumed { get; }
        public IntIndexableLookup<Item, double> IncomingItems { get; }
        public List<LogiHauler> IncomingHaulers { get; }
        public List<LogiHauler> OutgoingHaulers { get; }
        public IEnumerable<ItemQuantity> ConstructionCost => BaseType.ConstructionCost;
        public IEnumerable<ItemQuantity> UpgradeCost => UpgradingTo?.ConstructionCost ?? Enumerable.Empty<ItemQuantity>();
        public IEnumerable<Tile> DeliveryArea { get; set; }
        public IntIndexableLookup<BonusType, double> CurrentBonuses { get; }

        public List<IServiceProviderActor> ServiceProviders { get; }

        protected TCellDevelopmentType BaseType { get; set; }

        public virtual MinimapColor MinimapColor => BaseType.MinimapColor;

        private Recipie _recipie;

        private readonly IDialogManager _dialogManager;
        private readonly IGamedataTracker _gamedataTracker;
        private readonly ISimulationEventBus _simulationEventBus;

        public AbstractCellDevelopment(
            IDialogManager dialogManager,
            IGamedataTracker gamedataTracker,
            ISimulationEventBus simulationEventBus)
        {
            _dialogManager = dialogManager;
            _gamedataTracker = gamedataTracker;
            _simulationEventBus = simulationEventBus;
            Id = Guid.NewGuid().ToString();
            UpkeepPercent = 1;
            ServiceProviders = new List<IServiceProviderActor>();
            MissingItems = new List<Item>();
            PerItemSatisfaction = new IntIndexableLookup<Item, double>();
            ItemsProduced = new IntIndexableLookup<Item, bool>();
            ItemsConsumed = new IntIndexableLookup<Item, bool>();
            IncomingItems = new IntIndexableLookup<Item, double>();
            IncomingHaulers = new List<LogiHauler>();
            OutgoingHaulers = new List<LogiHauler>();
            DeliveryArea = Enumerable.Empty<Tile>();
            CurrentBonuses = new IntIndexableLookup<BonusType, double>();
        }

        public FactoryState FactoryState
        {
            get => _factoryState;
            set
            {
                if(_factoryState != value)
                {
                    _factoryState = value;
                    Cell.CalculateLayers();
                }
            }
        }

        public Recipie ActiveRecipie
        {
            get => _recipie;
            set
            {
                if (_recipie != value)
                {
                    ProductionCheckWait = 0;
                    ProductionProgress = 0;
                }

                _recipie = value;
                if (Cell.Region?.Settlement != null)
                {
                    Cell.Region.Settlement.SetCalculationFlag("recipie changed");
                    Cell.CalculateLayers();
                }
            }
        }

        public virtual IEnumerable<ItemRate> NeededInputs
        {
            get
            {
                IEnumerable<ItemRate> inputs;

                if(!UnderConstruction)
                {
                    inputs = UpkeepInputs;

                    if(ActiveRecipie != null)
                    {
                        inputs = inputs.Concat(ActiveRecipie.Inputs);
                    }
                }
                else
                {
                    inputs = Enumerable.Empty<ItemRate>();
                }

                return inputs;
            }
        }

        public virtual IEnumerable<ItemRate> ExpectedOutputs
        {
            get
            {
                if (!UnderConstruction && ActiveRecipie != null)
                {
                    return ActiveRecipie.BoostedOutputs(this, Cell.Traits)
                        .Select(ir => new ItemRate(ir.Item, ir.PerMonth * WorkerPercent))
                        .ToArray();
                }

                return Enumerable.Empty<ItemRate>();
            }
        }

        public virtual AbstractCellDevelopment<TCellDevelopmentType> Initialize(
            Cell cell, 
            TCellDevelopmentType baseType,
            bool instantBuild)
        {
            Cell = cell;
            BaseType = baseType;

            UnderConstruction = true;
            Inventory = new ItemInventory();
            DeliveryRoutes = new List<DeliveryRoute>();

            ActiveRecipie = AvailableRecipies
                .Where(r => r.IsValid(this))
                .FirstOrDefault();
            
            if (instantBuild)
            {
                ConstructionProgress = ConstructionMonths;
                UnderConstruction = false;
                OnConstructionFinished();
            }

            Cell.CalculateLayers();

            return this;
        }

        public virtual void OnConstructionFinished()
        {
            if (Cell.Feature != null && Cell.Feature.Traits.ContainsAny(BaseType.TraitsToClear))
            {
                Cell.Feature = null;
            }

            Cell.CalculateLayers();
        }

        public virtual void OnProductionStarted()
        {
            Cell.CalculateLayers();
        }

        public virtual void OnUpgradeFinished(ICellDevelopment newDevelopment)
        {
        }

        public virtual void DoUpdate(double deltaMonths)
        {
            Devastation = Math.Max(0, Devastation - Constants.DEVELOPMENT_DEVASTATION_HEAL * deltaMonths);
        }

        public Tile[] CalculateLogiDoors(IEnumerable<MovementType> movementTypes)
        {
            var nearestDistrict = Cell.Region?.Settlement?.Districts
                ?.Where(d => d.IsGenerated)
                ?.OrderBy(d => d.Cell.Position.DistanceSq(Cell.Position))
                ?.FirstOrDefault();
            
            var tiles = new List<Tile>();

            if (nearestDistrict != null)
            {
                for (var i = 0; i < TilePosition.TILES_PER_CELL; i++)
                {
                    for (var j = 0; j < TilePosition.TILES_PER_CELL; j++)
                    {
                        if (i == 0 || j == 0 || i == TilePosition.TILES_PER_CELL - 1 || j == TilePosition.TILES_PER_CELL - 1)
                        {
                            var tile = nearestDistrict.Tiles[i, j];

                            if (tile != null
                                && movementTypes.Any(mt => tile.Traits.ContainsAll(mt.RequiredTraits))
                                && !movementTypes.Any(mt => tile.Traits.ContainsAny(mt.PreventedBy)))
                            {
                                tiles.Add(tile);
                            }
                        }
                    }
                }
            }

            return tiles.ToArray();
        }

        public virtual IStackUiElement BuildDetailColumn(bool playerOwned, bool playerSees)
        {
            var stack = new StackGroup
            {
                Orientation = Orientation.Vertical,
                Width = Width.Fixed(250),
                Children = new List<IUiElement>
                {
                    new Label(Name),
                    new Spacer(0, 5),
                    new TraitDisplay(() => Traits),
                    new Spacer(0, 10)
                }
            };

            if (playerSees)
            {
                if (UnderConstruction)
                {
                    if (ConstructionMonths > 0)
                    {
                        stack.Children.Add(new Label($"Under Construction: {(int)(ConstructionProgress / ConstructionMonths * 100)}%"));
                    }
                    else
                    {
                        stack.Children.Add(new Label($"Under Construction"));
                    }

                    if (NeededInputs.Any())
                    {
                        stack.Children.Add(new Spacer(0, 5));
                        stack.Children.Add(UiBuilder.BuildDisplayFor(NeededInputs.Select(ir => new ItemRate(ir.Item, ir.PerMonth * -1))));
                    }

                    stack.Children.Add(new Spacer(0, 5));

                    if (playerOwned)
                    {
                        stack.Children.Add(new TextButton(
                            "Cancel",
                            () =>
                            {
                                Cell.Development = null;
                                _dialogManager.PopModal();
                            },
                            KeyCode.C));
                    }
                }

                if (!UnderConstruction && !IsUpgrading && playerOwned)
                {
                    if (ManuallyUpgradesTo.Any())
                    {
                        stack.Children.Add(new TextButton(
                            "Upgrade",
                            () =>
                            {
                                _dialogManager.PushModal<ConstructionInfoModal, ConstructionInfoModalContext>(
                                    new ConstructionInfoModalContext
                                    {
                                        Cell = Cell,
                                        CellDevelopmentTypes = ManuallyUpgradesTo
                                    });
                            },
                            KeyCode.U,
                            disableCheck: () => !CanManuallyUpgrade));
                    }

                    if (ManuallyDowngradesTo != null)
                    {
                        stack.Children.Add(new TextButton(
                            "Downgrade",
                            () =>
                            {
                                _dialogManager.PushModal<ConstructionInfoModal, ConstructionInfoModalContext>(
                                    new ConstructionInfoModalContext
                                    {
                                        Cell = Cell,
                                        CellDevelopmentTypes = ManuallyDowngradesTo.Yield()
                                    });
                            },
                            KeyCode.D,
                            disableCheck: () => !CanManuallyDowngrade));
                    }
                }
                else if (IsUpgrading)
                {
                    if (UpgradingTo.ConstructionMonths > 0)
                    {
                        stack.Children.Add(new Label("")
                        {
                            ContentUpdater = () => $"Upgrading {(int)(100 * UpgradeProgress / UpgradingTo.ConstructionMonths)}%"
                        });
                    }
                    else
                    {
                        stack.Children.Add(new Label("Upgrading"));
                    }

                    if (playerSees)
                    {
                        stack.Children.Add(new Spacer(0, 5));
                        stack.Children.Add(new TextButton(
                            "Cancel",
                            () =>
                            {
                                CancelUpgrading();
                                _dialogManager.PopModal();
                            },
                            KeyCode.C));
                    }
                }

                if (UpkeepInputs.Any())
                {
                    stack.Children.Add(new Spacer(0, 10));
                    stack.Children.Add(UiBuilder.BuildUpkeepRow(UpkeepInputs));
                }

                if (!UnderConstruction && AvailableRecipies.Any())
                {
                    stack.Children.Add(new Spacer(0, 10));
                    stack.Children.Add(new Label($"   Production"));
                    if (ActiveRecipie != null)
                    {
                        stack.Children.Add(new Label($"{ActiveRecipie.Verb}: {ProductionProgress.ToString("P0")}"));
                        stack.Children.Add(UiBuilder.BuildDisplayFor(
                            ActiveRecipie.Inputs.Select(y => new ItemRate(y.Item, -1 * y.PerMonth)).Concat(ActiveRecipie.Outputs)));
                        stack.Children.Add(new Spacer(0, 5));

                        stack.Children.Add(UiBuilder.BuildFactoryStateDisplay(_gamedataTracker, this));
                    }

                    if (playerOwned)
                    {
                        stack.Children.Add(UiBuilder.BuildRecipieButtonRow(AvailableRecipies, this));
                    }
                    else
                    {
                        stack.Children.Add(UiBuilder.BuildRecipieRow(AvailableRecipies));
                    }
                }

                if (WorkerNeed.Need > 0)
                {
                    stack.Children.Add(new Label($"Workers {WorkerSupply} / {WorkerNeed.Need}"));
                }

                if (Inventory.ItemsPresent.Any())
                {
                    stack.Children.Add(new Label($"   Inventory"));
                    stack.Children.Add(new InventoryDisplay(Inventory, 6));
                }
            }

            return stack;
        }

        public virtual void SerializeTo(SerializedObject developmentRoot)
        {
            developmentRoot.Set("id", Id);
            developmentRoot.Set("type_id", BaseType.Id);
            developmentRoot.Set("worker_supply", WorkerSupply);
            developmentRoot.Set("under_construction", UnderConstruction);
            developmentRoot.Set("construction_progress", ConstructionProgress);
            developmentRoot.Set("active_recipie_id", ActiveRecipie?.Id);
            developmentRoot.Set("upgrade_progress", UpgradeProgress);
            developmentRoot.Set("upgrading_to_id", UpgradingTo?.Id);
            developmentRoot.Set("devastation", Devastation);
            Inventory.SerializeTo(developmentRoot.CreateChild("inventory"));
            developmentRoot.Set("production_check_wait", ProductionCheckWait);
            developmentRoot.Set("item_transmission_wait", ItemTransmissionWait);
            developmentRoot.Set("production_progress", ProductionProgress);
        }

        public virtual void DeserializeFrom(
            IGamedataTracker gamedataTracker, 
            SerializedObject developmentRoot)
        {
            Id = developmentRoot.GetString("id");
            WorkerSupply = developmentRoot.GetInt("worker_supply");
            UnderConstruction = developmentRoot.GetBool("under_construction");
            ConstructionProgress = developmentRoot.GetDouble("construction_progress");
            var activeRecipieId = developmentRoot.GetOptionalString("active_recipie_id");
            ActiveRecipie = AvailableRecipies
                .Where(r => r.Id == activeRecipieId)
                .FirstOrDefault();
            Inventory.DeserializeFrom(gamedataTracker, developmentRoot.GetChild("inventory"));
            Devastation = developmentRoot.GetOptionalDouble("devastation");

            UpgradeProgress = developmentRoot.GetDouble("upgrade_progress");
            UpgradingTo = developmentRoot.GetOptionalObjectReference(
                "upgrading_to_id",
                gamedataTracker.CellDevelopments,
                cd => cd.Id);

            ProductionCheckWait = developmentRoot.GetOptionalDouble("production_check_wait");
            ItemTransmissionWait = developmentRoot.GetOptionalDouble("item_transmission_wait");
            ProductionProgress = developmentRoot.GetOptionalDouble("production_progress");
        }

        public override string ToString()
        {
            return Name;
        }

        public virtual IStackUiElement BuildHoverInfoColumn(bool playerOwns, bool playerSees)
        {
            return new StackGroup
            {
                Orientation = Orientation.Vertical
            };
        }

        public virtual void OnRemoval()
        {
            IsRemoved = true;
        }
        
        public void UpgradeTo(ICellDevelopmentType cellDevelopmentType)
        {
            UpgradingTo = cellDevelopmentType;
            UpgradeProgress = 0;
            Settlement.SetCalculationFlag("upgrade start");
        }

        public void FinishUpgrading()
        {
            if(UpgradingTo != null)
            {
                UpgradingTo.Place(Cell.Region.Owner, Cell, instantBuild: false);

                var newDevelopment = Cell.Development;

                newDevelopment.ConstructionProgress = newDevelopment.ConstructionMonths;
                newDevelopment.UnderConstruction = false;
                Inventory.TransferTo(newDevelopment.Inventory);
                
                if (ActiveRecipie != null
                    && newDevelopment.AvailableRecipies.Contains(ActiveRecipie))
                {
                    newDevelopment.ActiveRecipie = ActiveRecipie;
                }

                OnUpgradeFinished(newDevelopment);
                newDevelopment.OnConstructionFinished();
            }

            UpgradingTo = null;
            UpgradeProgress = 0;
        }

        public void CancelUpgrading()
        {
            UpgradingTo = null;
            UpgradeProgress = 0;
            Settlement.SetCalculationFlag("upgrade cancel");
        }

        public virtual IEnumerable<string> GetAnimationsNames()
        {
            var animationNames = Constants.IDLE_ANIMATIONS;

            if (ActiveRecipie != null && ProductionProgress > 0)
            {
                animationNames = ActiveRecipie.AnimationNames;
            }

            return animationNames;
        }

        public virtual void Abandon()
        {
            if (UnderConstruction)
            {
                Cell.Development = null;
            }
            else
            {
                switch (BaseType.AbandonmentBehavior.AbandonmentType)
                {
                    case AbandonmentType.Clear:
                        Cell.Development = null;
                        break;
                    case AbandonmentType.NoChange:
                        break;
                    case AbandonmentType.ToFeature:
                        Cell.Development = null;
                        Cell.Feature = BaseType.AbandonmentBehavior.ReplacementFeature;
                        break;
                    default:
                        throw new Exception("Unknown abandonment type: " + BaseType.AbandonmentBehavior.AbandonmentType);
                }
            }
        }

        public virtual void WasRaided()
        {
            if (UnderConstruction)
            {
                Cell.Development = null;

                var popsToKill = (int)Math.Ceiling(WorkerSupply / 2.0);
                for(var i = 0; i < popsToKill; i++)
                {
                    var pop = Settlement.Population
                        .Where(p => p.Caste == BaseType.ConstructionWorkers.Caste)
                        .FirstOrDefault();

                    if(pop != null)
                    {
                        Settlement.Population.Remove(pop);

                        Settlement.SetCalculationFlag("pop_removed");

                        _simulationEventBus.SendEvent(new PopKilledOnWorldSimulationEvent(pop, Cell));
                    }
                }
            }
            else
            {
                switch (BaseType.RaidedBehavior.AbandonmentType)
                {
                    case AbandonmentType.Clear:
                        Cell.Development = null;
                        break;
                    case AbandonmentType.NoChange:
                        break;
                    case AbandonmentType.ToFeature:
                        Cell.Development = null;
                        Cell.Feature = BaseType.RaidedBehavior.ReplacementFeature;
                        break;
                    default:
                        throw new Exception("Unknown abandonment type: " + BaseType.RaidedBehavior.AbandonmentType);
                }

                var popsToKill = (int)Math.Ceiling(WorkerSupply / 2.0);
                for (var i = 0; i < popsToKill; i++)
                {
                    var pop = Settlement.Population
                        .Where(p => p.Caste == BaseType.OperationWorkers.Caste)
                        .FirstOrDefault();

                    if (pop != null)
                    {
                        Settlement.Population.Remove(pop);
                        Settlement.SetCalculationFlag("pop_removed");
                        _simulationEventBus.SendEvent(new PopKilledOnWorldSimulationEvent(pop, Cell));
                    }
                }
            }
        }

        public virtual bool AllowsAccess(Army army)
        {
            return true;
        }

        public double NeedOf(Item item)
        {
            var supply = Inventory.QuantityOf(item) + IncomingItems[item];

            if (UnderConstruction)
            {
                if (ConstructionProgress <= 0)
                {
                    return BaseType.ConstructionCostLookup[item] - supply;
                }

                return 0;
            }
            else if (IsUpgrading && UpgradeCost.Any(iq => iq.Item == item))
            {
                if (UpgradeProgress <= 0)
                {
                    return UpgradingTo.ConstructionCostLookup[item] - supply;
                }

                return 0;
            }
            else
            {
                var capacity = InventoryCapacity;

                return capacity - supply;
            }
        }
    }
}
