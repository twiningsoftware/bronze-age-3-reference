﻿using System.Collections.Generic;
using Bronze.Common.Data.Game.CellDevelopments;
using Bronze.Common.UI;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;

namespace Bronze.Common.Data.World.CellDevelopments
{
    public class TransportAnchorageDevelopment : AbstractCellDevelopment<TransportAnchorageDevelopmentType>, IArmyInteractable
    {
        public override bool CanBeSieged => false;
        public string InteractVerb => "Boarding";
        public bool InteractAdjacent => true;
        public bool InteractWithForeign => true;
        
        public TransportAnchorageDevelopment(
            IDialogManager dialogManager,
            IGamedataTracker gamedataTracker,
            ISimulationEventBus simulationEventBus)
            : base(dialogManager, gamedataTracker, simulationEventBus)
        {
        }
        
        public override AbstractCellDevelopment<TransportAnchorageDevelopmentType> Initialize(
            Cell cell,
            TransportAnchorageDevelopmentType baseType, 
            bool instantBuild)
        {
            base.Initialize(cell, baseType, instantBuild);
            
            return this;
        }

        public override IStackUiElement BuildDetailColumn(bool playerOwned, bool playerSees)
        {
            var stackGroup = base.BuildDetailColumn(playerOwned, playerSees);

            stackGroup.Children.AddRange(new AbstractUiElement[]
            {
                new Spacer(0, 5),
                new StackGroup
                {
                    Orientation = Orientation.Horizontal,
                    Children = new List<IUiElement>
                    {
                        new Label("Provides Transport: "),
                        UiBuilder.BuildDisplayFor(BaseType.TransportType)
                    }
                }
            });

            return stackGroup;
        }

        public void DoInteraction(Army interactingArmy)
        {
            interactingArmy.MoveTo(Cell);

            interactingArmy.TransportType = BaseType.TransportType;

            Cell.Development = null;
        }

        public override IStackUiElement BuildHoverInfoColumn(bool playerOwns, bool playerSees)
        {
            var stackGroup = base.BuildHoverInfoColumn(playerOwns, playerSees);

            stackGroup.Children.AddRange(new AbstractUiElement[]
            {
                new Spacer(0, 5),
                new StackGroup
                {
                    Orientation = Orientation.Horizontal,
                    Children = new List<IUiElement>
                    {
                        new Label("Provides Transport: "),
                        UiBuilder.BuildDisplayFor(BaseType.TransportType)
                    }
                }
            });

            return stackGroup;
        }
    }
}
