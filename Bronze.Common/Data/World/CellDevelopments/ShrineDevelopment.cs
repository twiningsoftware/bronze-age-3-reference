﻿using Bronze.Common.Data.Game.CellDevelopments;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.Data.World.CellDevelopments
{
    public class ShrineDevelopment : AbstractCellDevelopment<ShrineDevelopmentType>, ICultInfluenceProvider, IAuthorityProvider
    {
        public override bool CanBeSieged => false;

        public Cult Cult => BaseType.Cult;
        public double InfluencePerPop => BaseType.InfluencePerPop;
        public int AuthorityProvided => BaseType.BonusAuthority;

        public ShrineDevelopment(
            IDialogManager dialogManager, 
            IGamedataTracker gamedataTracker,
            ISimulationEventBus simulationEventBus)
            : base(dialogManager, gamedataTracker, simulationEventBus)
        {
        }
    }
}
