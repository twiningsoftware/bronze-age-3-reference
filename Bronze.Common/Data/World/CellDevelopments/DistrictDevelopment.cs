﻿using System.Linq;
using Bronze.Common.Data.Game.CellDevelopments;
using Bronze.Common.UI.Modals;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Elements;

namespace Bronze.Common.Data.World.CellDevelopments
{
    public class DistrictDevelopment : AbstractCellDevelopment<DistrictDevelopmentType>
    {
        private readonly IPlayerDataTracker _playerData;
        private readonly IDialogManager _dialogManager;
        private readonly IDistrictGenerator _districtGenerator;
        
        public override bool CanBeSieged => true;
        public override bool CanBeDestroyed => false;

        public DistrictDevelopment(
            IDialogManager dialogManager,
            IPlayerDataTracker playerData,
            IDistrictGenerator districtGenerator,
            IGamedataTracker gamedataTracker,
            ISimulationEventBus simulationEventBus)
            : base(dialogManager, gamedataTracker, simulationEventBus)
        {
            _dialogManager = dialogManager;
            _playerData = playerData;
            _districtGenerator = districtGenerator;
        }

        public override void OnConstructionFinished()
        {
            base.OnConstructionFinished();
            
            var newDistrict = new District(Cell);
            newDistrict.Settlement = Cell.Region.Settlement;
            _districtGenerator.QueueForGeneration(newDistrict);
            Cell.Region.Settlement.AddDistrict(newDistrict);
        }

        public override IStackUiElement BuildDetailColumn(bool playerOwned, bool playerSees)
        {
            var stack = base.BuildDetailColumn(playerOwned, playerSees) as StackGroup;

            if (!UnderConstruction && playerOwned)
            {
                stack.Children.Add(new TextButton("View Settlement",
                    () =>
                    {
                        _playerData.ActiveSettlement = Cell.Region.Settlement;
                        _playerData.LookAt(new TilePosition(Cell, TilePosition.TILES_PER_CELL / 2, TilePosition.TILES_PER_CELL / 2));
                        _dialogManager.PopModal();
                    }));
            }

            return stack;
        }

        public override IStackUiElement BuildHoverInfoColumn(bool playerOwns, bool playerSees)
        {
            var stack = base.BuildHoverInfoColumn(playerOwns, playerSees);

            if (!UnderConstruction && playerSees)
            {
                stack.Children.Add(new Label("Under Siege", BronzeColor.Red, () => Settlement.IsUnderSiege));
            }
            
            if (!UnderConstruction && playerOwns)
            {
                stack.Children.Add(new TextButton("View Settlement",
                    () =>
                    {
                        _playerData.ActiveSettlement = Cell.Region.Settlement;
                        _playerData.LookAt(new TilePosition(Cell, TilePosition.TILES_PER_CELL / 2, TilePosition.TILES_PER_CELL / 2));

                    }));
            }

            return stack;
        }
        
        public override bool AllowsAccess(Army army)
        {
            return army.Owner == Settlement?.Owner;
        }
    }
}
