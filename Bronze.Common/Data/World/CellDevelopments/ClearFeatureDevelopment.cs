﻿using Bronze.Common.Data.Game.CellDevelopments;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.Data.World.CellDevelopments
{
    public class ClearFeatureDevelopment : AbstractCellDevelopment<ClearFeatureDevelopmentType>
    {
        public override bool CanBeSieged => false;

        public ClearFeatureDevelopment(
            IDialogManager dialogManager, 
            IGamedataTracker gamedataTracker,
            ISimulationEventBus simulationEventBus)
            : base(dialogManager, gamedataTracker, simulationEventBus)
        {
        }

        public override void DoUpdate(double deltaMonths)
        {
            base.DoUpdate(deltaMonths);

            if (!UnderConstruction && Cell.Development == this)
            {
                Cell.Feature = null;
                Cell.Development = null;
            }
        }
    }
}
