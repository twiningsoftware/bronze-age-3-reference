﻿using System.Collections.Generic;
using System.Linq;
using Bronze.Common.Data.Game.CellDevelopments;
using Bronze.Common.UI;
using Bronze.Contracts;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.Trade;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;

namespace Bronze.Common.Data.World.CellDevelopments
{
    public class TraderAndTransportDevelopment : AbstractCellDevelopment<TraderAndTransportDevelopmentType>, ITradeActor, IArmyInteractable
    {
        public MovementType TradeType => BaseType.TradeType;
        public double TradeCapacity => BaseType.TradeCapacity;
        public TradeHaulerInfo TradeHauler => BaseType.TradeHauler;
        public IEnumerable<TradeCellExit> CellExits => _cellExits.Where(x => x != null);
        public TradeRoute TradeRoute { get; set; }
        public override bool CanBeSieged => false;

        public string InteractVerb => "Boarding";
        public bool InteractAdjacent => false;
        public bool InteractWithForeign => false;

        private TradeCellExit[] _cellExits;

        private readonly ITradeManager _tradeManager;

        public TraderAndTransportDevelopment(
            IDialogManager dialogManager,
            ITradeManager tradeManager,
            IGamedataTracker gamedataTracker,
            ISimulationEventBus simulationEventBus)
            : base(dialogManager, gamedataTracker, simulationEventBus)
        {
            _tradeManager = tradeManager;
            _cellExits = new TradeCellExit[0];
        }

        public override IEnumerable<ItemRate> NeededInputs
        {
            get
            {
                if (!UnderConstruction
                    && TradeRoute != null
                    && TradeRoute.FromActor == this)
                {
                    return base.NeededInputs
                        .Concat(TradeRoute.Exports)
                        .ToArray();
                }

                return base.NeededInputs;
            }
        }

        public override IEnumerable<ItemRate> ExpectedOutputs
        {
            get
            {
                if (!UnderConstruction
                    && TradeRoute != null
                    && TradeRoute.ToActor == this)
                {
                    return base.ExpectedOutputs
                        .Concat(TradeRoute.Exports)
                        .ToArray();
                }

                return base.ExpectedOutputs;
            }
        }

        

        public override AbstractCellDevelopment<TraderAndTransportDevelopmentType> Initialize(
            Cell cell,
            TraderAndTransportDevelopmentType baseType, 
            bool instantBuild)
        {
            base.Initialize(cell, baseType, instantBuild);
            
            return this;
        }

        public override void DoUpdate(double deltaMonths)
        {
            base.DoUpdate(deltaMonths);

            if (Cell.Traits.ContainsAll(TradeType.RequiredTraits)
                && !Cell.Traits.ContainsAny(TradeType.PreventedBy))
            {
                _cellExits = new[]
                {
                    new TradeCellExit(Cell, null)
                };
            }
            else
            {
                _cellExits = Cell.OrthoNeighbors
                    .Where(x => x.Traits.ContainsAll(TradeType.RequiredTraits))
                    .Where(x => !x.Traits.ContainsAny(TradeType.PreventedBy))
                    .Select(x => new TradeCellExit(x, Cell))
                    .ToArray();
            }
        }

        public override IStackUiElement BuildDetailColumn(bool playerOwned, bool playerSees)
        {
            var stackGroup = base.BuildDetailColumn(playerOwned, playerSees);

            if (playerSees)
            {
                if (!UnderConstruction)
                {
                    stackGroup.Children.Add(new Spacer(0, 10));

                    if (TradeRoute != null)
                    {
                        if (TradeRoute.FromActor == this)
                        {
                            stackGroup.Children.Add(new Label($"Exporting to {TradeRoute.ToSettlement.Name}"));
                            stackGroup.Children.Add(UiBuilder.BuildDisplayFor(TradeRoute.Exports, 5));
                        }
                        else
                        {
                            stackGroup.Children.Add(new Label($"Importing from {TradeRoute.FromSettlement.Name}"));
                            stackGroup.Children.Add(UiBuilder.BuildDisplayFor(TradeRoute.Exports, 5));
                        }
                    }
                    else
                    {
                        stackGroup.Children.Add(new Label($"Not trading"));
                    }
                }
            }
            
            stackGroup.Children.AddRange(new AbstractUiElement[]
            {
                new Spacer(0, 5),
                new StackGroup
                {
                    Orientation = Orientation.Horizontal,
                    Children = new List<IUiElement>
                    {
                        new Label("Provides Transport: "),
                        UiBuilder.BuildDisplayFor(BaseType.TransportType)
                    }
                }
            });

            return stackGroup;
        }

        public void OnTradePathChanged()
        {
        }

        public override void OnUpgradeFinished(ICellDevelopment newDevelopment)
        {
            base.OnUpgradeFinished(newDevelopment);

            if (TradeRoute != null && newDevelopment is ITradeActor newTradeActor)
            {
                _tradeManager.ReparentTradeRoute(this, newTradeActor, TradeRoute);
            }
        }

        public void DoInteraction(Army interactingArmy)
        {
            var newPosition = Cell.Yield()
                .Concat(Cell.OrthoNeighbors)
                .Where(n => BaseType.TransportType.MovementModes.Any(mm => mm.IsValidFor(n)))
                .FirstOrDefault();

            if(newPosition != null)
            {
                interactingArmy.MoveTo(newPosition);
                interactingArmy.TransportType = BaseType.TransportType;
            }
        }

        public override IStackUiElement BuildHoverInfoColumn(bool playerOwns, bool playerSees)
        {
            var stackGroup = base.BuildHoverInfoColumn(playerOwns, playerSees);

            stackGroup.Children.AddRange(new AbstractUiElement[]
            {
                new Spacer(0, 5),
                new StackGroup
                {
                    Orientation = Orientation.Horizontal,
                    Children = new List<IUiElement>
                    {
                        new Label("Provides Transport: "),
                        UiBuilder.BuildDisplayFor(BaseType.TransportType)
                    }
                }
            });

            return stackGroup;
        }
    }
}
