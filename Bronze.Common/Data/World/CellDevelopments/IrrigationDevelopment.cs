﻿using System.Collections.Generic;
using System.Linq;
using Bronze.Common.Data.Game.CellDevelopments;
using Bronze.Common.InjectableServices;
using Bronze.Contracts;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.Data.World.CellDevelopments
{
    public class IrrigationDevelopment : AbstractCellDevelopment<IrrigationDevelopmentType>
    {
        public Trait SourceTrait => BaseType.SourceTrait;
        public IEnumerable<Trait> CarriedTraits => BaseType.CarriedTraits;
        public override bool CanBeSieged => false;

        private bool _isFull;

        private Trait[] _fullTraits;
        private Trait[] _emptyTraits;

        private readonly IIrrigationCalculator _irrigationCalculator;
        
        public IrrigationDevelopment(
            IDialogManager dialogManager,
            IIrrigationCalculator irrigationCalculator,
            IGamedataTracker gamedataTracker,
            ISimulationEventBus simulationEventBus)
            : base(dialogManager, gamedataTracker, simulationEventBus)
        {

            _irrigationCalculator = irrigationCalculator;
            _fullTraits = new Trait[0];
            _emptyTraits = new Trait[0];
        }

        public bool IsFull
        {
            get => _isFull;
            set
            {
                if (_isFull != value)
                {
                    _isFull = value;
                    Cell.CalculateLayers();
                    Cell.CalculateTraits();
                    Cell.Region?.Settlement?.SetCalculationFlag("irrigation_changed");
                }
            }
        }

        public override IEnumerable<Trait> Traits => base.Traits.Concat(IsFull ? _fullTraits : _emptyTraits);

        public override IEnumerable<string> GetAnimationsNames()
        {
            var names = base.GetAnimationsNames();

            if(IsFull)
            {
                return names.Concat("full".Yield());
            }

            return names.Concat("empty".Yield());
        }

        public override AbstractCellDevelopment<IrrigationDevelopmentType> Initialize(
            Cell cell, 
            IrrigationDevelopmentType baseType, 
            bool instantBuild)
        {
            base.Initialize(cell, baseType, instantBuild);
            _irrigationCalculator.Register(this);
            _fullTraits = BaseType.CarriedTraits.ToArray();
            return this;
        }

        public override void OnRemoval()
        {
            base.OnRemoval();

            _irrigationCalculator.Deregister(this);
        }
        
        public override void SerializeTo(SerializedObject developmentRoot)
        {
            base.SerializeTo(developmentRoot);

            developmentRoot.Set("is_full", IsFull);
        }

        public override void DeserializeFrom(IGamedataTracker gamedataTracker, SerializedObject developmentRoot)
        {
            base.DeserializeFrom(gamedataTracker, developmentRoot);

            IsFull = developmentRoot.GetBool("is_full");
        }
    }
}
