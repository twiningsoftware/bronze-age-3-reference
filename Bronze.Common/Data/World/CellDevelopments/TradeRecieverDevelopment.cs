﻿using Bronze.Common.Data.Game.CellDevelopments;
using Bronze.Common.UI;
using Bronze.Contracts;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.Trade;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Elements;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.Data.World.CellDevelopments
{
    public class TradeRecieverDevelopment : AbstractCellDevelopment<TradeRecieverDevelopmentType>, ITradeReciever
    {
        public IEnumerable<TradeCellExit> CellExits => _cellExits.Where(x => x != null);
        public List<TradeRoute> TradeRoutes { get; }
        public override bool CanBeSieged => false;

        private TradeCellExit[] _cellExits;
        private readonly ITradeManager _tradeManager;

        public TradeRecieverDevelopment(
            IDialogManager dialogManager,
            ITradeManager tradeManager,
            IGamedataTracker gamedataTracker,
            ISimulationEventBus simulationEventBus)
            : base(dialogManager, gamedataTracker, simulationEventBus)
        {
            _tradeManager = tradeManager;
            TradeRoutes = new List<TradeRoute>();
            _cellExits = new TradeCellExit[0];
        }

        public override IEnumerable<ItemRate> ExpectedOutputs
        {
            get
            {
                if (!UnderConstruction)
                {
                    return base.ExpectedOutputs
                        .Concat(TradeRoutes
                            .Where(t => t.RouteValid)
                            .SelectMany(t => t.Exports))
                            .ToArray();
                }

                return base.ExpectedOutputs;
            }
        }

        public override void DoUpdate(double deltaMonths)
        {
            base.DoUpdate(deltaMonths);

            if (!_cellExits.Any())
            {
                _cellExits = new TradeCellExit(Cell, null).Yield()
                    .Concat(Cell.OrthoNeighbors
                        .Select(x => new TradeCellExit(x, Cell)))
                    .ToArray();
            }
        }

        public void OnTradePathChanged()
        {
        }

        public void OnTradeRoutesChanged()
        {
        }

        public override IStackUiElement BuildDetailColumn(bool playerOwned, bool playerSees)
        {
            var stackGroup = base.BuildDetailColumn(playerOwned, playerSees);

            if (!UnderConstruction)
            {
                stackGroup.Children.Add(new Spacer(0, 10));

                if (TradeRoutes.Any())
                {
                    var importSettlements = TradeRoutes
                        .Select(t => t.FromSettlement)
                        .Distinct()
                        .ToArray();

                    if (importSettlements.Length == 1)
                    {
                        stackGroup.Children.Add(new Label($"Importing from {importSettlements.First().Name}"));
                    }
                    else
                    {
                        stackGroup.Children.Add(new Label($"Importing from {importSettlements.Length} settlements"));
                    }

                    stackGroup.Children.Add(UiBuilder.BuildDisplayFor(TradeRoutes.SelectMany(t => t.Exports), 5));
                }
                else
                {
                    stackGroup.Children.Add(new Label($"Not trading"));
                }
            }

            return stackGroup;
        }

        public override void OnUpgradeFinished(ICellDevelopment newDevelopment)
        {
            base.OnUpgradeFinished(newDevelopment);

            if (newDevelopment is ITradeReciever newTradeReciever)
            {
                foreach (var tradeRoute in TradeRoutes.ToArray())
                {
                    _tradeManager.ReparentTradeRoute(this, newTradeReciever, tradeRoute);
                }
            }
        }
    }
}
