﻿using Bronze.Common.Data.Game;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using System;
using System.Linq;

namespace Bronze.Common.Data.World
{
    public class PopNeedState
    {
        public const double CHECK_INTERVAL_BASE = 0.2;

        public IPopNeed Need { get; }
        public bool IsUpgradeNeed { get; }
        public bool IsBonusNeed { get; }

        public double Satisfaction { get; private set; }
        public bool Satisfied { get; private set; }
        public double CheckTimer { get; private set; }
        public double CheckInterval { get; private set; }
        public double BonusProsperity => Need.ProsperityPerPop * Residents * Satisfaction;

        public int Residents { get; set; }

        private Random _random;

        public PopNeedState(IPopNeed popNeed, bool isUpgradeNeed, bool isBonusNeed)
        {
            _random = new Random();

            Satisfaction = 1;
            Satisfied = true;
            Residents = 0;
            Need = popNeed;
            IsUpgradeNeed = isUpgradeNeed;
            IsBonusNeed = isBonusNeed;
            CheckInterval = (_random.NextDouble() + 0.5) * CHECK_INTERVAL_BASE;
            CheckTimer = CheckInterval;
        }

        public void Update(
            IHousingProvider housingProvider, 
            double deltaMonths)
        {
            if(Need is ItemPopNeed itemNeed)
            {
                CheckTimer -= deltaMonths;

                if(!Satisfied)
                {
                    Satisfaction = Math.Max(0, Satisfaction - deltaMonths / CheckInterval);
                }

                if(CheckTimer <= 0)
                {
                    var itemSource = housingProvider.Distributor ?? housingProvider;
                    var toTake = itemNeed.PerMonthPerPop * Residents * CheckInterval;

                    if (itemSource.Inventory.QuantityOf(itemNeed.Item) >= toTake)
                    {
                        itemSource.Inventory.Remove(itemNeed.Item, toTake);
                        itemSource.Settlement.LastMonthProduction[itemNeed.Item] -= toTake;
                        Satisfied = true;
                        Satisfaction = 1;
                    }
                    else
                    {
                        Satisfied = false;
                    }

                    CheckInterval = (_random.NextDouble() + 0.5) * CHECK_INTERVAL_BASE;
                    CheckTimer = CheckInterval;
                }
            }
            else if (Need is ServicePopNeed serviceNeed)
            {
                var serviceProvider = housingProvider.ServiceProviders
                    .Where(sp => sp.ServiceType == serviceNeed.Service)
                    .FirstOrDefault();

                if (serviceProvider != null)
                {
                    Satisfied = true;
                    Satisfaction = serviceProvider.ServicePercent;
                }
                else
                {
                    Satisfied = false;
                    Satisfaction = 0;
                }
            }
            else if (Need is AuthorityPopNeed)
            {
                Satisfaction = housingProvider.Settlement.AuthorityFraction;
            }
        }

        public void SerializeTo(SerializedObject needRoot)
        {
            needRoot.Set("satisfied", Satisfied);
            needRoot.Set("satisfaction", Satisfaction);
        }

        public void DeserializeFrom(SerializedObject needRoot)
        {
            Satisfied = needRoot.GetBool("satisfied");
            Satisfaction = needRoot.GetDouble("satisfaction");
        }
    }
}
