﻿using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace Bronze.Common.Data.World
{
    public class GhulLair : IWorldActor
    {
        public string Id { get; }
        public Facing Facing { get; set; }
        public Vector2 Position { get; set; }
        public Cell Cell { get; set; }

        public void Draw(IDrawingService drawingService, ViewMode viewMode, bool isSelected)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<SoundEffectType> GetSoundEffects()
        {
            throw new NotImplementedException();
        }
    }
}
