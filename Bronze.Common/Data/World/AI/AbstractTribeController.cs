﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bronze.Common.Data.Diplomacy;
using Bronze.Common.Data.Game.AI;
using Bronze.Common.Data.Game.AI.Messaging;
using Bronze.Common.Data.Game.Units;
using Bronze.Common.Data.World.Units;
using Bronze.Contracts;
using Bronze.Contracts.Data.AI;
using Bronze.Contracts.Data.Diplomacy;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.Trade;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.Data.World.AI
{
    public class AbstractTribeController<TControllerType> : ITribeController where TControllerType : AbstractTribeControllerType
    {
        public const string REGION_GOAL_CLEAR = "clear";
        public const string REGION_GOAL_HOLD = "hold";
        public const string REGION_GOAL_ANNEX = "annex";
        public const string REGION_GOAL_SETTLE = "settle";
        public const string REGION_GOAL_BLOCKED = "blocked";

        public TControllerType BaseType { get; private set; }

        protected Dictionary<string, SerializedObject> SettlementStates { get; private set; }
        protected Dictionary<string, SerializedObject> ArmyStates { get; private set; }
        protected Dictionary<int, RegionGoalState> RegionGoals { get; private set; }
        
        public bool IsAggressive { get; private set; }
        public Tribe Tribe { get; private set; }
        public StrategicTarget CurrentStrategicTarget { get; protected set; }
        private double _strategicTargetActDate;
        private bool _strategicTargetAskedFor;
        private double _nextMessageDate;
        private Random _random;

        private readonly IWarHelper _warHelper;
        private readonly ITreatyHelper _treatyHelper;
        private readonly IInjectionProvider _injectionProvider;
        private readonly IWorldManager _worldManager;
        private readonly IPlayerDataTracker _playerData;
        private readonly IDiplomacyHelper _diplomacyHelper;
        private readonly INotificationsSystem _notificationsSystem;
        private readonly IGenericNotificationBuilder _genericNotificationBuilder;
        private readonly IDiplomaticNotificationBuilder _diplomaticNotificationBuilder;
        private readonly IAiHelper _aiHelper;
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IBattleHelper _battleHelper;
        private readonly ILogger _logger;
        private readonly IDialogManager _dialogManager;
        private readonly ITradeManager _tradeManager;
        
        public AbstractTribeController(
            IWarHelper warHelper,
            ITreatyHelper treatyHelper,
            IInjectionProvider injectionProvider,
            IWorldManager worldManager,
            IPlayerDataTracker playerData,
            IDiplomacyHelper diplomacyHelper,
            INotificationsSystem notificationsSystem,
            IGenericNotificationBuilder genericNotificationBuilder,
            IDiplomaticNotificationBuilder diplomaticNotificationBuilder,
            IAiHelper aiHelper,
            IGamedataTracker gamedataTracker,
            IBattleHelper battleHelper,
            ILogger logger,
            IDialogManager dialogManager,
            ITradeManager tradeManager)
        {
            _warHelper = warHelper;
            _treatyHelper = treatyHelper;
            _injectionProvider = injectionProvider;
            _worldManager = worldManager;
            _playerData = playerData;
            _diplomacyHelper = diplomacyHelper;
            _notificationsSystem = notificationsSystem;
            _genericNotificationBuilder = genericNotificationBuilder;
            _diplomaticNotificationBuilder = diplomaticNotificationBuilder;
            _aiHelper = aiHelper;
            _gamedataTracker = gamedataTracker;
            _battleHelper = battleHelper;
            _logger = logger;
            _dialogManager = dialogManager;
            _tradeManager = tradeManager;

            SettlementStates = new Dictionary<string, SerializedObject>();
            RegionGoals = new Dictionary<int, RegionGoalState>();
            _random = new Random();
            ArmyStates = new Dictionary<string, SerializedObject>();
        }

        public virtual void Seed(TControllerType baseType, Tribe tribe, Random random, double worldHostility, double month)
        {
            BaseType = baseType;
            Tribe = tribe;

            var isAgressive = random.NextDouble() < BaseType.Aggression + (worldHostility - 0.5);

            var aggression = BaseType.Aggression + worldHostility + random.NextDouble() / 2 - 0.75;

            if (isAgressive)
            {
                aggression += 0.5;
            }

            Tribe.AddDiplomaticMemory(new DiplomaticMemory(
                "attitude",
                "General attitude",
                month,
                1,
                0,
                0,
                aggression * -5));

            AssignSettlementRoles();
        }
        
        public virtual void DoFastAI()
        {
            AssignSettlementRoles();

            foreach (var settlement in Tribe.Settlements.ToArray())
            {
                if (RegionGoals.ContainsKey(settlement.Region.Id) && RegionGoals[settlement.Region.Id].Goal == REGION_GOAL_CLEAR)
                {
                    Tribe.Abandon(settlement);
                }
            }

            var excessWantRegionMemories = Tribe.DiplomaticMemories
                .Where(m => m.Category.StartsWith("wantregion") && (CurrentStrategicTarget == null || !m.Category.Contains(CurrentStrategicTarget.TargetRegion.Id.ToString())))
                .ToArray();
            foreach (var memory in excessWantRegionMemories)
            {
                Tribe.RemoveDiplomaticMemory(memory.Category);
            }

            AskForRegionOrDeclareWar();

            EndExtraWars();
        }
        
        private void AskForRegionOrDeclareWar()
        {
            if (CurrentStrategicTarget != null
                && CurrentStrategicTarget.TakeByForce
                && !Tribe.IsHostileTo(CurrentStrategicTarget.TargetRegion.Owner)
                && CurrentStrategicTarget.TargetRegion.Owner != null
                && CurrentStrategicTarget.TargetRegion.Owner != Tribe
                && !_treatyHelper.GetTreatiesFor(Tribe, CurrentStrategicTarget.TargetRegion.Owner).Any(t => t.BlocksWar && !t.CanBeCancelled)
                && _strategicTargetActDate <= _worldManager.Month)
            {
                if (CurrentStrategicTarget.TargetRegion.Owner != _playerData.PlayerTribe)
                {
                    var war = _warHelper.StartWar(Tribe, CurrentStrategicTarget.TargetRegion.Owner);

                    war.AddEvent(new WarEvent(
                        $"Want control of {CurrentStrategicTarget.TargetRegion.Settlement.Name}",
                        40,
                        _worldManager.Month,
                        "wargoal",
                        string.Empty,
                        null));
                }
                else
                {
                    var hasWantRegionMemory = Tribe.DiplomaticMemories
                        .Where(m => m.Category == "wantregion_" + CurrentStrategicTarget.TargetRegion.Id)
                        .Any();

                    if (!hasWantRegionMemory)
                    {
                        Tribe.AddDiplomaticMemory(
                            new DiplomaticMemory(
                                "wantregion_" + CurrentStrategicTarget.TargetRegion.Id,
                                $"They desire control of {CurrentStrategicTarget.TargetRegion.Name}.",
                                _worldManager.Month,
                                0,
                                0.04,
                                -100,
                                0));

                        // No sense sending a message if we're just about to attack
                        if (_diplomacyHelper.GetInfluenceFor(Tribe) > 0)
                        {
                            _notificationsSystem.AddNotification(
                                _genericNotificationBuilder.BuildSimpleNotification(
                                "ui_notification_diplomacy",
                                $"Message from {Tribe.Name}",
                                $"{Tribe.Ruler.Name} of the {Tribe.Name} has sent a message.",
                                $"Message from {Tribe.Name}",
                                _diplomacyHelper.GenerateText(
                                    Tribe,
                                    DiploConstants.MESSAGE_WANTREGION,
                                    BuildConversationContext().Concat(new[] { CurrentStrategicTarget.Goal }),
                                    new Dictionary<string, string> { { "target_region", CurrentStrategicTarget.TargetRegion.Name } }),
                                CurrentStrategicTarget.TargetRegion.Center));
                        }
                    }

                    if (_diplomacyHelper.GetInfluenceFor(Tribe) <= 0)
                    {
                        if (!_strategicTargetAskedFor)
                        {
                            _strategicTargetAskedFor = true;
                            _strategicTargetActDate = _worldManager.Month + 2;

                            _notificationsSystem.AddNotification(
                                _diplomaticNotificationBuilder.BuildNegotiationRequestNotification(Tribe));
                        }
                        else
                        {
                            _notificationsSystem.AddNotification(
                                _genericNotificationBuilder.BuildSimpleNotification(
                                "ui_notification_diplomacy",
                                $"Message from {Tribe.Name}",
                                $"{Tribe.Ruler.Name} of the {Tribe.Name} has sent a message.",
                                $"Message from {Tribe.Name}",
                                _diplomacyHelper.GenerateText(
                                    Tribe,
                                    DiploConstants.MESSAGE_STARTWAR,
                                    BuildConversationContext().Concat(new[] { CurrentStrategicTarget.Goal }),
                                    new Dictionary<string, string> { { "target_region", CurrentStrategicTarget.TargetRegion.Name } }),
                                CurrentStrategicTarget.TargetRegion.Center,
                                autoOpen: true));

                            var war = _warHelper.StartWar(Tribe, _playerData.PlayerTribe);

                            war.AddEvent(new WarEvent(
                                $"Want control of {CurrentStrategicTarget.TargetRegion.Settlement.Name}",
                                40,
                                _worldManager.Month,
                                "wargoal",
                                string.Empty,
                                null));
                        }
                    }
                }
            }
        }

        private void EndExtraWars()
        {
            foreach (var war in Tribe.CurrentWars.ToArray())
            {
                var otherSide = war.Aggressor == Tribe ? war.Defender : war.Aggressor;

                var isWanted = CurrentStrategicTarget != null
                    && CurrentStrategicTarget.TargetRegion.Owner != null
                    && otherSide == CurrentStrategicTarget.TargetRegion.Owner;

                var isAggressor = war.Aggressor == Tribe;

                if (!isWanted)
                {
                    if (otherSide != _playerData.PlayerTribe)
                    {
                        otherSide.Controller.HandleMessage(new EndWarInterAiMessage
                        {
                            From = Tribe,
                            To = otherSide,
                            War = war
                        });
                    }
                    else if (_nextMessageDate < _worldManager.Month)
                    {
                        _nextMessageDate =  _worldManager.Month + _random.Next(6, 9);

                        _notificationsSystem.AddNotification(
                            _diplomaticNotificationBuilder.BuildNegotiationRequestNotification(Tribe));
                    }
                }
            }
        }

        public virtual IEnumerable<Action> DoDeepAI()
        {
            var results = new List<Action>();
            
            var adjustedMaxTerritory = (int)((_worldManager.Hostility + 1) * BaseType.MaxTerritory);

            results.AddRange(SetRegionGoalStates(adjustedMaxTerritory));
            
            _aiHelper.CheckForArmyStates(Tribe, ArmyStates);

            _aiHelper.SetSettlementIntruderStates(Tribe, SettlementStates);

            var currentArmies = Tribe.Armies.Select(a => a.Id).ToArray();
            foreach(var armyId in ArmyStates.Keys.ToArray())
            {
                if(!currentArmies.Contains(armyId))
                {
                    ArmyStates.Remove(armyId);
                }
            }

            var currentSettlements = Tribe.Settlements.Select(a => a.Id).ToArray();
            foreach (var settlementId in SettlementStates.Keys.ToArray())
            {
                if (!currentSettlements.Contains(settlementId))
                {
                    SettlementStates.Remove(settlementId);
                }
            }

            foreach (var settlement in Tribe.Settlements)
            {
                if(SettlementStates.ContainsKey(settlement.Id))
                {
                    var recruitmentSlots = _gamedataTracker.RecruitmentSlotCategories
                        .Where(rs => rs.StrengthEstimate > 0)
                        .ToArray();

                    if (SettlementStates[settlement.Id].GetOptionalBool("has_intruder"))
                    {
                        if (recruitmentSlots.Where(rs => rs.StrengthEstimate > 0).Any(rs => settlement.RecruitmentSlots[rs] >= 1))
                        {
                            results.Add(() => _aiHelper.FormLevyDefenseArmies(Tribe, settlement, ArmyStates));
                        }
                    }
                }
            }

            UpdateStrategicTarget();
            
            if (Tribe.CurrentWars.Any())
            {
                results.Add(() => _aiHelper.FormImperialArmies(Tribe, ArmyStates));
            }
            else if (CurrentStrategicTarget != null && !CurrentStrategicTarget.TakeByForce
                && !ArmyStates.Any(a => a.Value.GetOptionalBool("is_colonizer")))
            {
                results.Add(() => _aiHelper.FormColonizerArmy(Tribe, ArmyStates));
            }

            foreach (var army in Tribe.Armies.Where(a => a.Units.Any() && ArmyStates.ContainsKey(a.Id)).ToArray())
            {
                var armyState = ArmyStates[army.Id];

                if (army.CellPathCalculator.PathFinished && !army.CellPathCalculator.PathSuccessful)
                {
                    if (army.CellPathCalculator.Destination.Region == CurrentStrategicTarget?.TargetRegion)
                    {
                        if(RegionGoals.ContainsKey(CurrentStrategicTarget.TargetRegion.Id))
                        {
                            RegionGoals[CurrentStrategicTarget.TargetRegion.Id].Goal = REGION_GOAL_BLOCKED;
                        }
                    }
                }

                if ((army.CurrentOrder.IsIdle || army.CurrentOrder is ArmyCampOrder) && !army.IsLocked)
                {
                    if (armyState != null)
                    {
                        if (armyState.GetString("role") == "levy")
                        {
                            if (Tribe.CurrentWars.Any())
                            {
                                _aiHelper.TryOrderArmyLevyPatrol(Tribe, army, armyState, _random);

                            }
                            else
                            {
                                var wasOrdered = _aiHelper.TryOrderArmyLevyPatrol(Tribe, army, armyState, _random)
                                    || _aiHelper.TryOrderCreepCleanup(Tribe, army)
                                    || _aiHelper.TryOrderArmyDisband(Tribe, army);
                            }
                        }
                        if (armyState.GetString("role") == "grazing")
                        {
                        }
                        else
                        {
                            if (Tribe.CurrentWars.Any())
                            {
                                var isCamping = army.CurrentOrder is ArmyCampOrder;
                                var minSupplyPerc = army.Units.Min(u => u.SuppliesPercent);

                                if (isCamping && minSupplyPerc >= 0.9)
                                {
                                    isCamping = false;
                                }

                                var wasOrdered = _aiHelper.TryOrderArmyPatrol(Tribe, army)
                                    || _aiHelper.TryOrderArmyGatherUnits(Tribe, army)
                                    || _aiHelper.TryOrderArmySiege(Tribe, army, CurrentStrategicTarget?.TargetRegion, CurrentStrategicTarget?.TakeByForce ?? false, isCamping, minSupplyPerc)
                                    || _aiHelper.TryOrderArmyRaiding(Tribe, army, IsAggressive, isCamping, minSupplyPerc)
                                    || _aiHelper.TryOrderArmyCamping(Tribe, army, isCamping);
                            }
                            else
                            {
                                var wasOrdered = _aiHelper.TryOrderArmyColonize(Tribe, army, armyState.GetOptionalBool("is_colonizer"), CurrentStrategicTarget, BaseType.SettlementPlacementGoalScores, BaseType.SettlementPlacementNeighborScores)
                                    || _aiHelper.TryOrderCreepCleanup(Tribe, army)
                                    || _aiHelper.TryOrderArmyPatrol(Tribe, army)
                                    || _aiHelper.TryOrderArmyDisband(Tribe, army);
                            }
                        }
                    }
                }
            }

            foreach (var army in Tribe.Armies)
            {
                if (army.CurrentOrder is ArmyInteractOrder interactOrder
                    && interactOrder.SiegedSettlement != null
                    && interactOrder.SiegedSettlement.Owner != Tribe)
                {
                    results.Add(() => _battleHelper.DoSiegeAssault(interactOrder.SiegedSettlement, Tribe));
                }
            }

            foreach (var settlement in Tribe.Settlements.Where(s => SettlementStates.ContainsKey(s.Id)))
            {
                var role = BaseType.SettlementRoles
                    .Where(r => r.IsFor(SettlementStates[settlement.Id]))
                    .FirstOrDefault();

                if (role != null)
                {
                    results.AddRange(role.RunFor(Tribe, settlement, SettlementStates[settlement.Id], CurrentStrategicTarget));
                }
            }

            var tradeRoutes = _tradeManager.GetTradeRoutesFor(Tribe);

            foreach (var settlement in Tribe.Settlements)
            {
                var neededItems = _gamedataTracker.Items
                    .Where(i => ImportNeedOf(i, settlement) > 0)
                    .OrderByDescending(i => TradeValueOf(i, settlement))
                    .ToArray();

                foreach(var item in neededItems)
                {
                    var sources = Tribe.Settlements
                        .Where(s => s != settlement)
                        .Where(s => ExportAvailabilityOf(item, s) > 0)
                        .OrderByDescending(s => ExportAvailabilityOf(item, s))
                        .Select(s => Tuple.Create(
                            s,
                            tradeRoutes
                                .Where(tr => tr.ToSettlement == settlement || tr.FromSettlement == s)
                                .Where(tr => tr.TotalCapacity - tr.UsedCapacity > 0)
                                .ToArray()))
                        .ToArray();

                    if (sources.Any(t => t.Item2.Any()))
                    {
                        var source = sources
                            .Where(t => t.Item2.Any())
                            .First();

                        var amount = Math.Min(
                            ExportAvailabilityOf(item, source.Item1),
                            source.Item2.First().TotalCapacity - source.Item2.First().UsedCapacity);
                        
                        source.Item2.First().SetExportPerMonth(item, amount);
                    }
                    else
                    {
                        var potentialPairs = sources
                            .SelectMany(t => t.Item1
                                .EconomicActors
                                .OfType<ITradeActor>()
                                .Where(ta => ta.TradeRoute == null)
                                .ToArray())
                            .SelectMany(ta => settlement.EconomicActors
                                    .OfType<ITradeReciever>()
                                    .Select(tr => Tuple.Create(ta, tr)))
                            .ToArray();

                        var tradeRoute = FindTradeRoute(potentialPairs);

                        if(tradeRoute != null)
                        {
                            var amount = Math.Min(
                                ExportAvailabilityOf(item, tradeRoute.FromSettlement),
                                tradeRoute.TotalCapacity);

                            tradeRoute.SetExportPerMonth(item, amount);
                        }
                    }
                }
            }

            return results;
        }

        private TradeRoute FindTradeRoute(Tuple<ITradeActor, ITradeReciever>[] potentialPairs)
        {
            foreach(var pair in potentialPairs)
            {
                var pather = new TradePathCalculator(
                    Tribe,
                    pair.Item1.TradeType,
                    pair.Item1,
                    pair.Item2);

                pather.CalculateToEnd();

                if(pather.PathSuccessful)
                {
                    return _tradeManager.CreateTradeRoute(pair.Item1, pair.Item2);
                }
            }

            return null;
        }

        protected virtual void AssignSettlementRoles()
        {
            foreach (var settlement in Tribe.Settlements)
            {
                if (!SettlementStates.ContainsKey(settlement.Id))
                {
                    var newState = new SerializedObject("settlement_state");
                    SettlementStates.Add(settlement.Id, newState);

                    var newRole = BaseType.SettlementRoles
                        .Where(r => r.CanApplyTo(settlement))
                        .FirstOrDefault();

                    newRole.Assign(settlement, newState);
                }
            }
        }
        
        protected List<Action> SetRegionGoalStates(int maxTerritory)
        {
            var results = new List<Action>();

            var regionsToConsider = Tribe.Settlements
                .SelectMany(s => s.Region.Neighbors)
                .Concat(Tribe.Settlements.Select(s => s.Region))
                .Where(r => r.Owner == Tribe || HasWalkableBorder(r))
                .ToArray();

            foreach (var regionId in RegionGoals.Keys)
            {
                if (!regionsToConsider.Any(n => n.Id == regionId))
                {
                    results.Add(() => { RegionGoals.Remove(regionId); });
                }
            }

            foreach (var neighbor in regionsToConsider)
            {
                var regionGoalState = new RegionGoalState(neighbor);

                if(RegionGoals.ContainsKey(neighbor.Id))
                {
                    regionGoalState = RegionGoals[neighbor.Id];
                }
                else
                {
                    results.Add(() =>
                    {
                        if (!RegionGoals.ContainsKey(regionGoalState.Region.Id))
                        {
                            RegionGoals.Add(regionGoalState.Region.Id, regionGoalState);
                        }
                    });
                }
                
                var neighborPlayerOwned = neighbor.Owner == _playerData.PlayerTribe;
                var playerTrust = _diplomacyHelper.GetTrustFor(Tribe);
                var playerInfluence = _diplomacyHelper.GetInfluenceFor(Tribe);
                var warBlocked = neighbor.Owner != null && _treatyHelper.GetTreatiesFor(Tribe, neighbor.Owner).Any(t => t.BlocksWar && !t.CanBeCancelled);

                switch (regionGoalState.Goal)
                {
                    case REGION_GOAL_ANNEX:
                        if (neighbor.Owner == Tribe)
                        {
                            regionGoalState.Goal = REGION_GOAL_HOLD;
                        }
                        else if (Tribe.Settlements.Count >= maxTerritory)
                        {
                            regionGoalState.Goal = string.Empty;
                        }
                        else if (neighbor.Owner == null)
                        {
                            regionGoalState.Goal = REGION_GOAL_SETTLE;
                        }
                        else if (_diplomacyHelper.GetRelativePower(Tribe, neighbor.Owner) <= 0.3
                            || warBlocked
                            || (neighborPlayerOwned && (playerInfluence + playerTrust) > 30))
                        {
                            regionGoalState.Goal = string.Empty;
                        }
                        else
                        {
                            regionGoalState.Score = ScoreRegion(neighbor);
                        }
                        break;
                    case REGION_GOAL_SETTLE:
                        if (neighbor.Owner == Tribe)
                        {
                            regionGoalState.Goal = REGION_GOAL_HOLD;
                        }
                        else if (Tribe.Settlements.Count >= maxTerritory || !CouldSettle(Tribe, neighbor))
                        {
                            regionGoalState.Goal = string.Empty;
                        }
                        else if (neighbor.Owner != null && IsAggressive)
                        {
                            regionGoalState.Goal = REGION_GOAL_ANNEX;
                        }
                        else
                        {
                            regionGoalState.Score = ScoreRegion(neighbor);
                        }
                        break;
                    case REGION_GOAL_CLEAR:
                        if (neighbor.Owner == null)
                        {
                            regionGoalState.Goal = string.Empty;
                        }
                        else if (_diplomacyHelper.GetRelativePower(Tribe, neighbor.Owner) <= 0.3
                            || warBlocked
                            || (neighborPlayerOwned && (playerInfluence + playerTrust) > 30))
                        {
                            regionGoalState.Goal = string.Empty;
                        }
                        else
                        {
                            regionGoalState.Score = ScoreRegion(neighbor);
                        }
                        break;
                    case REGION_GOAL_HOLD:
                        if (neighbor.Owner == null)
                        {
                            regionGoalState.Goal = REGION_GOAL_SETTLE;
                        }
                        else if (neighbor.Owner != Tribe)
                        {
                            regionGoalState.Goal = REGION_GOAL_ANNEX;
                        }

                        else
                        {
                            regionGoalState.Score = ScoreRegion(neighbor);
                        }
                        break;
                    case REGION_GOAL_BLOCKED:
                        break;
                    default:
                        if (neighbor.Owner == Tribe)
                        {
                            regionGoalState.Goal = REGION_GOAL_HOLD;
                        }
                        else if (neighbor.Owner != null
                            && IsAggressive
                            && _diplomacyHelper.GetRelativePower(Tribe, neighbor.Owner) > 0.3
                            && !warBlocked
                            && (!neighborPlayerOwned || (playerInfluence + playerTrust) < 30))
                        {
                            if (Tribe.Settlements.Count < maxTerritory && Tribe.Race == neighbor.Owner.Race)
                            {
                                regionGoalState.Goal = REGION_GOAL_ANNEX;
                            }
                            else
                            {
                                regionGoalState.Goal = REGION_GOAL_CLEAR;
                            }
                        }
                        else if (neighbor.Owner == null && Tribe.Settlements.Count < maxTerritory && CouldSettle(Tribe, neighbor))
                        {
                            regionGoalState.Goal = REGION_GOAL_SETTLE;
                        }
                        break;
                }
            }

            return results;
        }

        private bool HasWalkableBorder(Region region)
        {
            return region.Cells
                .Where(c => c.IsOnBorder && c.Neighbors.Any(nc => nc.Traits.Contains(BaseType.WalkableTrait) && nc.Owner == Tribe))
                .Where(c => c.Traits.Contains(BaseType.WalkableTrait))
                .Any();
        }
        
        public int ScoreRegion(Region region)
        {
            var score = 0;

            score += region.Cells
                .SelectMany(c => c.Traits)
                .Where(t => BaseType.ExpansionGoalScores.ContainsKey(t))
                .Select(t => BaseType.ExpansionGoalScores[t])
                .Sum();

            score += region.Cells
                .SelectMany(c => c.Neighbors)
                .Where(c => c != null)
                .SelectMany(c => c.Traits)
                .Where(t => BaseType.ExpansionGoalNeighborScores.ContainsKey(t))
                .Select(t => BaseType.ExpansionGoalNeighborScores[t])
                .Sum();

            return score;
        }
        
        protected bool CouldSettle(Tribe tribe, Region region)
        {
            var colonizers = tribe.Race.AvailableUnits
                .SelectMany(u => u.EquipmentSets.SelectMany(e => e.AvailableActions.Where(a => a is ArmySettleAction).Select(a => Tuple.Create(u, e, a))))
                .Where(t => t.Item1 != null && t.Item2 != null && t.Item3 != null)
                .ToArray();

            return region.Cells
                .Any(c => colonizers.Any(u => u.Item2.CanPath(c) && u.Item3.CouldBeValidFor(tribe, c)));
        }
        
        protected void UpdateStrategicTarget()
        {
            if (CurrentStrategicTarget != null && RegionGoals.ContainsKey(CurrentStrategicTarget.TargetRegion.Id))
            {
                if(CurrentStrategicTarget.Goal != RegionGoals[CurrentStrategicTarget.TargetRegion.Id].Goal)
                {
                    CurrentStrategicTarget = null;
                }
            }
            else
            {
                var aggressiveGoals = new[] { REGION_GOAL_ANNEX, REGION_GOAL_CLEAR };
                var expandGoals = new[] { REGION_GOAL_ANNEX, REGION_GOAL_CLEAR, REGION_GOAL_SETTLE };

                var regionGoals = RegionGoals.Values
                    .OrderByDescending(g => g.Score)
                    .ToArray();
                
                var warTargets = Tribe.CurrentWars
                    .Select(w => w.Aggressor == Tribe ? w.Defender : w.Aggressor)
                    .Distinct()
                    .ToArray();

                var newTarget = regionGoals
                    .Where(g => aggressiveGoals.Contains(g.Goal) && warTargets.Contains(g.Region.Owner))
                    .FirstOrDefault();

                if (newTarget == null && IsAggressive)
                {
                    newTarget = regionGoals
                        .Where(g => aggressiveGoals.Contains(g.Goal))
                        .FirstOrDefault();
                }

                if (newTarget == null)
                {
                    newTarget = regionGoals
                        .Where(g => expandGoals.Contains(g.Goal))
                        .FirstOrDefault();
                }

                if (newTarget != null)
                {
                    CurrentStrategicTarget = new StrategicTarget
                    {
                        Goal = newTarget.Goal,
                        TargetRegion = newTarget.Region,
                        TakeByForce = aggressiveGoals.Contains(newTarget.Goal)
                    };

                    _strategicTargetActDate = _worldManager.Month + (1 - _worldManager.Hostility) * 4;
                    _strategicTargetAskedFor = false;
                }
            }
        }
        
        public virtual void HandleMessage(IInterAiMessage message)
        {
            if (message is EndWarInterAiMessage endWarMessage)
            {
                var isWanted = CurrentStrategicTarget != null
                    && CurrentStrategicTarget.TargetRegion.Owner != null
                    && message.From == CurrentStrategicTarget.TargetRegion.Owner;

                // Sometimes the message can get sent twice
                if (!isWanted && endWarMessage.War.IsOngoing)
                {
                    _warHelper.EndWar(endWarMessage.War);
                    _treatyHelper.AddTreaty(
                        _injectionProvider.Build<TruceTreaty>().Initialize(
                            message.From,
                            message.To,
                            _warHelper.GetWarScore(message.From, message.To)));
                }
            }
        }

        public virtual IEnumerable<string> BuildConversationContext()
        {
            var context = new List<string>();

            if (IsAggressive)
            {
                context.Add("aggressive");
            }

            return context;
        }
        
        public virtual DiplomaticResponse GetDiplomaticResponse(string actionType, object actionData)
        {
            if (actionType == DiploConstants.ACTION_TYPE_FIRSTMEET)
            {
                return new DiplomaticResponse
                {
                    ResponseText = _diplomacyHelper.GenerateText(Tribe, DiploConstants.RESPONSE_FIRSTMEET, BuildConversationContext()),
                    Actions = new[]
                    {
                        new DiplomaticAction
                        {
                            ButtonText = _diplomacyHelper.GenerateText(Tribe, DiploConstants.ACTION_TEXT_CHARM),
                            IsEnabled = true,
                            Tooltip = $"Try to sway them with flattery and soft words.\n{(int)(100 * _diplomacyHelper.ScoreConvoCheck(NotablePersonStatType.Charisma, Tribe))}% chance",
                            ActionType = DiploConstants.ACTION_TYPE_CHARM
                        }.Yield(),
                        new DiplomaticAction
                        {
                            ButtonText = _diplomacyHelper.GenerateText(Tribe, DiploConstants.ACTION_TEXT_RATIONALIZE),
                            IsEnabled = true,
                            Tooltip = $"Try to sway them with facts and logic.\n{(int)(100 * _diplomacyHelper.ScoreConvoCheck(NotablePersonStatType.Wit, Tribe))}% chance",
                            ActionType = DiploConstants.ACTION_TYPE_RATIONALIZE
                        }.Yield(),
                        new DiplomaticAction
                        {
                            ButtonText = _diplomacyHelper.GenerateText(Tribe, DiploConstants.ACTION_TEXT_INTIMIDATE),
                            IsEnabled = true,
                            Tooltip = $"Try to sway them with intimidation.\n{(int)(100 * _diplomacyHelper.ScoreConvoCheck(NotablePersonStatType.Valor, Tribe))}% chance",
                            ActionType = DiploConstants.ACTION_TYPE_INTIMIDATE
                        }.Yield(),
                    }
                };
            }
            else if (actionType == DiploConstants.ACTION_TYPE_GREETING)
            {
                return new DiplomaticResponse
                {
                    ResponseText = _diplomacyHelper.GenerateText(Tribe, DiploConstants.RESPONSE_GREETING, BuildConversationContext()),
                    Actions = new[]
                    {
                        BuildStandardActions(Tribe),
                        new List<DiplomaticAction>
                        {
                            new DiplomaticAction
                            {
                                ButtonText = "Nevermind",
                                IsEnabled = true,
                                Tooltip = $"End the conversation.",
                                EndConvo = true
                            }
                        }
                    }
                };
            }
            else if (actionType == DiploConstants.ACTION_TYPE_CHARM)
            {
                var success = _random.NextDouble() < _diplomacyHelper.ScoreConvoCheck(NotablePersonStatType.Charisma, Tribe);

                DiplomaticMemory newMemory;

                if (success)
                {
                    newMemory = new DiplomaticMemory(
                        "first_impression",
                        "Charming first impression.",
                        _worldManager.Month,
                        1,
                        0.001,
                        0,
                        30);
                }
                else
                {
                    newMemory = new DiplomaticMemory(
                        "first_impression",
                        "Poor first impression.",
                        _worldManager.Month,
                        1,
                        0.001,
                        0,
                        -20);
                }
                Tribe.AddDiplomaticMemory(newMemory);

                return new DiplomaticResponse
                {
                    ResponseText = _diplomacyHelper.GenerateText(
                        Tribe,
                        DiploConstants.RESPONSE_CHARM,
                        BuildConversationContext().Concat(new[] { success ? "success" : "failure" })),
                    FormedMemory = newMemory,
                    Actions = new[]
                    {
                        new DiplomaticAction
                        {
                            ButtonText = "Goodbye",
                            IsEnabled = true,
                            Tooltip = $"End the conversation.",
                            EndConvo = true
                        }.Yield()
                    }
                };
            }
            else if (actionType == DiploConstants.ACTION_TYPE_INTIMIDATE)
            {
                var success = _random.NextDouble() < _diplomacyHelper.ScoreConvoCheck(NotablePersonStatType.Valor, Tribe);

                DiplomaticMemory newMemory;
                if (success)
                {
                    newMemory = new DiplomaticMemory(
                        "first_impression",
                        "Indimidating first impression.",
                        _worldManager.Month,
                        1,
                        0.001,
                        30,
                        -30);
                }
                else
                {
                    newMemory = new DiplomaticMemory(
                        "first_impression",
                        "Poor first impression.",
                        _worldManager.Month,
                        1,
                        0.001,
                        -20,
                        -20);
                }
                Tribe.AddDiplomaticMemory(newMemory);

                return new DiplomaticResponse
                {
                    ResponseText = _diplomacyHelper.GenerateText(
                        Tribe,
                        DiploConstants.RESPONSE_INTIMIDATE,
                        BuildConversationContext().Concat(new[] { success ? "success" : "failure" })),
                    FormedMemory = newMemory,
                    Actions = new[]
                    {
                        new DiplomaticAction
                        {
                            ButtonText = "Goodbye",
                            IsEnabled = true,
                            Tooltip = $"End the conversation.",
                            EndConvo = true
                        }.Yield()
                    }
                };
            }
            else if (actionType == DiploConstants.ACTION_TYPE_RATIONALIZE)
            {
                var success = _random.NextDouble() < _diplomacyHelper.ScoreConvoCheck(NotablePersonStatType.Wit, Tribe);

                DiplomaticMemory newMemory;
                if (success)
                {
                    newMemory = new DiplomaticMemory(
                        "first_impression",
                        "Good first impression.",
                        _worldManager.Month,
                        1,
                        0.001,
                        20,
                        0);
                }
                else
                {
                    newMemory = new DiplomaticMemory(
                        "first_impression",
                        "Poor first impression.",
                        _worldManager.Month,
                        1,
                        0.001,
                        -20,
                        -10);
                }
                Tribe.AddDiplomaticMemory(newMemory);

                return new DiplomaticResponse
                {
                    ResponseText = _diplomacyHelper.GenerateText(
                        Tribe,
                        DiploConstants.RESPONSE_RATIONALIZE,
                        BuildConversationContext().Concat(new[] { success ? "success" : "failure" })),
                    FormedMemory = newMemory,
                    Actions = new[]
                    {
                        new DiplomaticAction
                        {
                            ButtonText = "Goodbye",
                            IsEnabled = true,
                            Tooltip = $"End the conversation.",
                            EndConvo = true
                        }.Yield()
                    }
                };
            }
            else if (actionType == DiploConstants.ACTION_TYPE_STARTWAR)
            {
                var newMemory = new DiplomaticMemory(
                    "war",
                    "Declared war.",
                    _worldManager.Month,
                    1,
                    0,
                    -100,
                    -20);
                Tribe.AddDiplomaticMemory(newMemory);

                _warHelper.StartWar(_playerData.PlayerTribe, Tribe);

                return new DiplomaticResponse
                {
                    ResponseText = _diplomacyHelper.GenerateText(
                        Tribe,
                        DiploConstants.RESPONSE_STARTWAR,
                        BuildConversationContext()),
                    FormedMemory = newMemory,
                    Actions = new[]
                    {
                        new DiplomaticAction
                        {
                            ButtonText = "Goodbye",
                            IsEnabled = true,
                            Tooltip = $"End the conversation.",
                            EndConvo = true
                        }.Yield()
                    }
                };
            }
            else if (actionType == DiploConstants.ACTION_TYPE_NEGOTIATE)
            {
                return new DiplomaticResponse
                {
                    ResponseText = _diplomacyHelper.GenerateText(
                        Tribe,
                        DiploConstants.RESPONSE_NEGOTIATE,
                        BuildConversationContext().Concat(new[] { (actionData as bool?) == true ? "success" : "failure" })),
                    Actions = new[]
                    {
                        BuildStandardActions(Tribe),
                        new DiplomaticAction
                        {
                            ButtonText = "Goodbye",
                            IsEnabled = true,
                            Tooltip = $"End the conversation.",
                            EndConvo = true
                        }.Yield()
                    }
                };
            }
            else
            {
                _logger.Error($"Unexpected diplomatic action for {GetType().Name}: {actionType}");

                return new DiplomaticResponse
                {
                    ResponseText = "I don't know.",
                    Actions = new[]
                    {
                        new DiplomaticAction
                        {
                            ButtonText = "Nevermind",
                            IsEnabled = true,
                            Tooltip = $"End the conversation.",
                            EndConvo = true
                        }.Yield()
                    }
                };
            }
        }
        
        public virtual IEnumerable<ITreatyOption> GetNegotiationAiOptions(
            List<ITreaty> proposedTreaties)
        {
            var options = new List<ITreatyOption>();

            if (_warHelper.AreAtWar(_playerData.PlayerTribe, Tribe)
                && !proposedTreaties.Any(t => t is TruceTreaty))
            {
                options.Add(new SimpleTreatyOption<TruceTreaty>(
                    _injectionProvider.Build<TruceTreaty>().Initialize(
                        Tribe,
                        _playerData.PlayerTribe,
                        _warHelper.GetWarScore(Tribe, _playerData.PlayerTribe))));
            }
            else
            {
                if (!proposedTreaties.Any(t => t is MilitaryAccessTreaty && t.From == Tribe))
                {
                    options.Add(new SimpleTreatyOption<MilitaryAccessTreaty>(
                        _injectionProvider.Build<MilitaryAccessTreaty>().Initialize(
                            Tribe,
                            _playerData.PlayerTribe,
                            _worldManager.Month)));
                }

                if (!proposedTreaties.Any(t => t is ExplorationTreaty && t.From == Tribe))
                {
                    options.Add(new SimpleTreatyOption<ExplorationTreaty>(
                        _injectionProvider.Build<ExplorationTreaty>().Initialize(
                            Tribe,
                            _playerData.PlayerTribe,
                            _worldManager.Month)));
                }

                if (proposedTreaties.Any(t => t.IsCoercing(Tribe))
                    && Tribe.Settlements
                        .Where(s => s != Tribe.Capital && !proposedTreaties.Any(t => t is TradeSettlementTreaty tst && tst.Settlement == s))
                        .Count() > 1)
                {
                    options.Add(new TradeSettlementOption(
                        _dialogManager,
                        Tribe,
                        _playerData.PlayerTribe));
                }

                var actorsAvailableToExport = Tribe.Settlements
                    .SelectMany(s => s.EconomicActors.OfType<ITradeActor>())
                    .Where(ta => ta.TradeRoute == null)
                    .Except(proposedTreaties.OfType<TradeTreaty>().Select(tt => tt.FromActor))
                    .ToArray();

                if (actorsAvailableToExport.Any())
                {
                    options.Add(new TradeTreatyOption(_dialogManager, Tribe, _playerData.PlayerTribe, actorsAvailableToExport, _injectionProvider, false));
                }

                if(Tribe.Race.MarriageCompatabilityGroup == _playerData.PlayerTribe.Race.MarriageCompatabilityGroup)
                {
                    options.Add(new MarriageTreatyOption(_dialogManager, Tribe, _playerData.PlayerTribe));
                }
            }

            return options;
        }

        public virtual IEnumerable<ITreaty> GetNegotiationAiWants(
            List<ITreaty> proposedTreaties)
        {
            var proposal = new List<ITreaty>();

            if (_warHelper.AreAtWar(_playerData.PlayerTribe, Tribe))
            {
                var warIsWanted = CurrentStrategicTarget != null
                    && CurrentStrategicTarget.TargetRegion.Owner != null
                    && _playerData.PlayerTribe == CurrentStrategicTarget.TargetRegion.Owner;

                if (!warIsWanted && !proposedTreaties.Any(t => t is TruceTreaty))
                {
                    proposal.Add(_injectionProvider.Build<TruceTreaty>().Initialize(
                        Tribe,
                        _playerData.PlayerTribe,
                        _warHelper.GetWarScore(Tribe, _playerData.PlayerTribe)));
                }

                if (CurrentStrategicTarget != null && CurrentStrategicTarget.TargetRegion.Owner == _playerData.PlayerTribe)
                {
                    var settlement = CurrentStrategicTarget.TargetRegion.Settlement;

                    proposal.Add(_injectionProvider.Build<TruceTreaty>().Initialize(
                        Tribe,
                        _playerData.PlayerTribe,
                        _warHelper.GetWarScore(Tribe, _playerData.PlayerTribe)));

                    proposal.Add(_injectionProvider.Build<TradeSettlementTreaty>().Initialize(
                        _playerData.PlayerTribe,
                        Tribe,
                        settlement,
                        ScoreRegion(settlement.Region)));
                }

                var concessions = new List<ITreaty>()
                {
                    _injectionProvider.Build<ExplorationTreaty>().Initialize(
                            _playerData.PlayerTribe,
                            Tribe,
                            _worldManager.Month),
                    _injectionProvider.Build<MilitaryAccessTreaty>().Initialize(
                            _playerData.PlayerTribe,
                            Tribe,
                            _worldManager.Month)
                };

                foreach (var concession in concessions)
                {
                    if (proposedTreaties
                        .Concat(proposal)
                        .Concat(concession.Yield())
                        .SelectMany(t => t.GetValueDetailFor(Tribe))
                        .Sum(vd => vd.Value) <= 0)
                    {
                        proposal.Add(concession);
                    }
                }
            }
            else
            {
                if (CurrentStrategicTarget != null && CurrentStrategicTarget.TargetRegion.Owner == _playerData.PlayerTribe)
                {
                    var settlement = CurrentStrategicTarget.TargetRegion.Settlement;

                    proposal.Add(_injectionProvider.Build<TradeSettlementTreaty>().Initialize(
                        _playerData.PlayerTribe,
                        Tribe,
                        settlement,
                        ScoreRegion(settlement.Region)));

                    if (CurrentStrategicTarget.TakeByForce && !proposedTreaties.Any(t => t is ThreatenTreaty && t.From == Tribe))
                    {
                        proposal.Add(_injectionProvider.Build<ThreatenTreaty>().Initialize(
                            Tribe,
                            _playerData.PlayerTribe));
                    }
                }
            }

            return proposal;
        }

        public virtual IEnumerable<ITreatyOption> GetNegotiationPlayerOptions(
            List<ITreaty> proposedTreaties)
        {
            var options = new List<ITreatyOption>();

            if (_warHelper.AreAtWar(_playerData.PlayerTribe, Tribe)
                && !proposedTreaties.Any(t => t is TruceTreaty))
            {
                options.Add(new SimpleTreatyOption<TruceTreaty>(
                    _injectionProvider.Build<TruceTreaty>().Initialize(
                        _playerData.PlayerTribe,
                        Tribe,
                        _warHelper.GetWarScore(_playerData.PlayerTribe, Tribe))));
            }
            else
            {
                if (!proposedTreaties.Any(t => t is MilitaryAccessTreaty && t.To == Tribe)
                    && Tribe.Settlements.Any(s => s.Region.Neighbors.Any(r => r.Owner == _playerData.PlayerTribe)))
                {
                    options.Add(new SimpleTreatyOption<MilitaryAccessTreaty>(
                        _injectionProvider.Build<MilitaryAccessTreaty>().Initialize(
                            _playerData.PlayerTribe,
                            Tribe,
                            _worldManager.Month)));
                }

                if (!proposedTreaties.Any(t => t is ExplorationTreaty && t.To == Tribe))
                {
                    options.Add(new SimpleTreatyOption<ExplorationTreaty>(
                        _injectionProvider.Build<ExplorationTreaty>().Initialize(
                            _playerData.PlayerTribe,
                            Tribe,
                            _worldManager.Month)));
                }

                if (!proposedTreaties.Any(t => t is ThreatenTreaty && t.From == _playerData.PlayerTribe)
                    && (!_warHelper.AreAtWar(_playerData.PlayerTribe, Tribe)
                    || proposedTreaties.Any(t => t is TruceTreaty tt && tt.GetValueDetailFor(Tribe).Sum(vd => vd.Value) > 0)))
                {
                    options.Add(new SimpleTreatyOption<ThreatenTreaty>(
                        _injectionProvider.Build<ThreatenTreaty>().Initialize(
                            _playerData.PlayerTribe,
                            Tribe)));
                }


                if (_playerData.PlayerTribe.Settlements
                        .Where(s => s != _playerData.PlayerTribe.Capital && !proposedTreaties.Any(t => t is TradeSettlementTreaty tst && tst.Settlement == s))
                        .Count() > 1)
                {
                    options.Add(new TradeSettlementOption(
                        _dialogManager,
                        _playerData.PlayerTribe,
                        Tribe));
                }
                
                var actorsAvailableToExport = _playerData.PlayerTribe.Settlements
                    .SelectMany(s => s.EconomicActors.OfType<ITradeActor>())
                    .Where(ta => ta.TradeRoute == null)
                    .Except(proposedTreaties.OfType<TradeTreaty>().Select(tt => tt.FromActor))
                    .ToArray();

                if (actorsAvailableToExport.Any())
                {
                    options.Add(new TradeTreatyOption(_dialogManager, _playerData.PlayerTribe, Tribe, actorsAvailableToExport, _injectionProvider, true));
                }

                if (Tribe.Race.MarriageCompatabilityGroup == _playerData.PlayerTribe.Race.MarriageCompatabilityGroup)
                {
                    options.Add(new MarriageTreatyOption(_dialogManager, _playerData.PlayerTribe, Tribe));
                }
            }

            return options;
        }

        protected virtual IEnumerable<DiplomaticAction> BuildStandardActions(Tribe Tribe)
        {
            var actions = new List<DiplomaticAction>();

            var warBlocked = _treatyHelper
                .GetTreatiesFor(_playerData.PlayerTribe, Tribe)
                .Any(t => t.BlocksWar);

            if (!_warHelper.AreAtWar(_playerData.PlayerTribe, Tribe))
            {
                actions.Add(new DiplomaticAction
                {
                    ButtonText = "Declare War",
                    IsEnabled = !warBlocked,
                    Tooltip = warBlocked ? $"Treaties prevent us from declaring war." : $"Declare a war against the {Tribe.Name}.",
                    ActionType = DiploConstants.ACTION_TYPE_STARTWAR
                });
            }

            actions.Add(new DiplomaticAction
            {
                ButtonText = "Negotiate",
                IsEnabled = true,
                Tooltip = $"Negotiate treaties with {Tribe.Name}.",
                StartNegotition = true
            });

            return actions;
        }

        public void SetStrategicTarget(StrategicTarget strategicTarget)
        {
            CurrentStrategicTarget = strategicTarget;

            if(!RegionGoals.ContainsKey(strategicTarget.TargetRegion.Id))
            {
                RegionGoals.Add(strategicTarget.TargetRegion.Id, new RegionGoalState(strategicTarget.TargetRegion)
                {
                    Goal = strategicTarget.Goal,
                    Score = int.MaxValue
                });
            }
            else
            {
                RegionGoals[strategicTarget.TargetRegion.Id].Goal = strategicTarget.Goal;
                RegionGoals[strategicTarget.TargetRegion.Id].Score = int.MaxValue;
            }
        }

        public void SerializeTo(SerializedObject controllerRoot)
        {
            controllerRoot.Set("is_aggressive", IsAggressive);
            controllerRoot.Set("type_id", BaseType.Id);
            controllerRoot.Set("strategic_target_act_date", _strategicTargetActDate);
            controllerRoot.Set("strategic_target_asked_for", _strategicTargetAskedFor);
            controllerRoot.Set("next_message_date", _nextMessageDate);

            if(CurrentStrategicTarget != null)
            {
                var currentTargetRoot = controllerRoot.CreateChild("current_strategic_target");

                currentTargetRoot.Set("goal", CurrentStrategicTarget.Goal);
                currentTargetRoot.Set("take_by_force", CurrentStrategicTarget.TakeByForce);
                currentTargetRoot.Set("target_region_id", CurrentStrategicTarget.TargetRegion.Id);
            }

            foreach(var kvp in SettlementStates)
            {
                var settlementStateRoot = controllerRoot.CreateChild("settlement_state");
                settlementStateRoot.Set("settlement_id", kvp.Key);
                settlementStateRoot.AddChild(kvp.Value);
            }

            foreach (var kvp in ArmyStates)
            {
                var armyStateRoot = controllerRoot.CreateChild("army_state");
                armyStateRoot.Set("army_id", kvp.Key);
                armyStateRoot.AddChild(kvp.Value);
            }

            foreach (var kvp in RegionGoals)
            {
                var regionGoalRoot = controllerRoot.CreateChild("region_goal");
                regionGoalRoot.Set("region_id", kvp.Key.ToString());
                regionGoalRoot.Set("goal", kvp.Value.Goal);
                regionGoalRoot.Set("score", kvp.Value.Score);
            }
        }

        public void DeserializeAndInit(
            TControllerType baseType, 
            Tribe tribe, 
            IEnumerable<Region> regions, 
            SerializedObject controllerRoot)
        {
            BaseType = baseType;
            Tribe = tribe;

            IsAggressive = controllerRoot.GetBool("is_aggressive");
            _strategicTargetActDate = controllerRoot.GetDouble("strategic_target_act_date");
            _strategicTargetAskedFor = controllerRoot.GetBool("strategic_target_asked_for");
            _nextMessageDate = controllerRoot.GetDouble("next_message_date");

            var currentTargetRoot = controllerRoot.GetOptionalChild("current_strategic_target");

            if (currentTargetRoot != null)
            {
                CurrentStrategicTarget = new StrategicTarget
                {
                    Goal = currentTargetRoot.GetString("goal"),
                    TakeByForce = currentTargetRoot.GetBool("take_by_force"),
                    TargetRegion = currentTargetRoot.GetOptionalObjectReferenceOrDefault(
                        "target_region_id",
                        regions,
                        r => r.Id.ToString())
                };

                if(CurrentStrategicTarget.TargetRegion == null)
                {
                    CurrentStrategicTarget = null;
                }
            }

            foreach(var settlementStateRoot in controllerRoot.GetChildren("settlement_state"))
            {
                SettlementStates.Add(
                    settlementStateRoot.GetString("settlement_id"),
                    settlementStateRoot.Children.First());
            }

            foreach (var armyStateRoot in controllerRoot.GetChildren("army_state"))
            {
                SettlementStates.Add(
                    armyStateRoot.GetString("army_id"),
                    armyStateRoot.Children.First());
            }

            foreach (var regionGoalRoot in controllerRoot.GetChildren("region_goal"))
            {
                var region = regionGoalRoot.GetOptionalObjectReferenceOrDefault(
                        "region_id",
                        regions,
                        r => r.Id.ToString());

                if (region != null)
                {
                    RegionGoals.Add(
                        regionGoalRoot.GetInt("region_id"),
                        new RegionGoalState(region)
                        {
                            Goal = regionGoalRoot.GetString("goal"),
                            Score = regionGoalRoot.GetInt("score")
                        });
                }
            }
        }
        
        public int TradeValueOf(Item item, Settlement settlement)
        {
            var value = 0;

            if(SettlementStates.ContainsKey(settlement.Id))
            {
                var role = BaseType.SettlementRoles
                    .Where(r => r.IsFor(SettlementStates[settlement.Id]))
                    .FirstOrDefault();

                if (role != null)
                {
                    var importTargets = role.GetItemImportTargets().Where(iit => iit.Item == item).ToArray();
                    if (importTargets.Any())
                    {
                        value = Math.Max(value, importTargets.Max(iit => iit.Value));
                    }
                }
            }

            return value;
        }

        public int ImportNeedOf(Item item, Settlement settlement)
        {
            var need = 0.0;

            if (SettlementStates.ContainsKey(settlement.Id))
            {
                var role = BaseType.SettlementRoles
                    .Where(r => r.IsFor(SettlementStates[settlement.Id]))
                    .FirstOrDefault();

                if (role != null)
                {
                    var importTargets = role.GetItemImportTargets().Where(iit => iit.Item == item).ToArray();
                    if(importTargets.Any())
                    {
                        var targetSurplus = importTargets.Sum(iit => iit.TargetSurplus);

                        var idealProduction = settlement.EconomicActors
                            .Sum(ea => ea.ExpectedOutputs.Where(ir => ir.Item == item).Sum(ir => ir.PerMonth));
                        var idealConsumption = settlement.EconomicActors
                            .Sum(ea => ea.NeededInputs.Where(ir => ir.Item == item).Sum(ir => ir.PerMonth));

                        var currentSurplus = idealProduction - idealConsumption;

                        need += targetSurplus - currentSurplus;
                    }
                }
            }

            return (int)Math.Ceiling(need);
        }

        public int ExportAvailabilityOf(Item item, Settlement settlement)
        {
            var surplus = 0.0;

            if (SettlementStates.ContainsKey(settlement.Id))
            {
                var role = BaseType.SettlementRoles
                    .Where(r => r.IsFor(SettlementStates[settlement.Id]))
                    .FirstOrDefault();

                if (role != null)
                {
                    var importTargets = role.GetItemImportTargets().Where(iit => iit.Item == item).ToArray();
                    if (importTargets.Any())
                    {
                        var targetSurplus = importTargets.Sum(iit => iit.TargetSurplus);

                        var idealProduction = settlement.EconomicActors
                            .Sum(ea => ea.ExpectedOutputs.Where(ir => ir.Item == item).Sum(ir => ir.PerMonth));
                        var idealConsumption = settlement.EconomicActors
                            .Sum(ea => ea.NeededInputs.Where(ir => ir.Item == item).Sum(ir => ir.PerMonth));

                        var currentSurplus = idealProduction - idealConsumption;

                        surplus += currentSurplus - targetSurplus;
                    }
                }
            }

            return (int)Math.Floor(surplus);
        }
    }
}
