﻿using Bronze.Common.Data.Game.AI;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.Data.World.AI
{
    public class BasicTribeController : AbstractTribeController<BasicTribeControllerType>
    {
        public BasicTribeController(
            IWarHelper warHelper, 
            ITreatyHelper treatyHelper, 
            IInjectionProvider injectionProvider,
            IWorldManager worldManager,
            IPlayerDataTracker playerData,
            IDiplomacyHelper diplomacyHelper,
            INotificationsSystem notificationsSystem,
            IGenericNotificationBuilder genericNotificationBuilder,
            IDiplomaticNotificationBuilder diplomaticNotificationBuilder,
            IAiHelper aiHelper,
            IGamedataTracker gamedataTracker,
            IBattleHelper battleHelper,
            ILogger logger,
            IDialogManager dialogManager,
            ITradeManager tradeManager) 
            : base(warHelper, treatyHelper, injectionProvider, worldManager, 
                  playerData, diplomacyHelper, notificationsSystem, genericNotificationBuilder,
                  diplomaticNotificationBuilder, aiHelper, gamedataTracker, battleHelper, logger,
                  dialogManager, tradeManager)
        {
        }
    }
}
