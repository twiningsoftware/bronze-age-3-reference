﻿using Bronze.Contracts.Data.World;

namespace Bronze.Common.Data.World.AI
{
    public class RegionGoalState
    {
        public Region Region { get; }
        public int Score { get; set; }
        public string Goal { get; set; }

        public RegionGoalState(Region region)
        {
            Region = region;
            Score = 0;
            Goal = string.Empty;
        }
    }
}
