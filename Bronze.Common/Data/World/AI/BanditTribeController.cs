﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bronze.Common.Data.Game.AI;
using Bronze.Common.Data.World.Units;
using Bronze.Contracts.Data.AI;
using Bronze.Contracts.Data.Diplomacy;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.Data.World.AI
{
    public class BanditTribeController : AbstractTribeController<BanditTribeControllerType>
    {
        private readonly IWarHelper _warHelper;
        private readonly IDiplomacyHelper _diplomacyHelper;

        public BanditTribeController(
            IWarHelper warHelper, 
            ITreatyHelper treatyHelper, 
            IInjectionProvider injectionProvider,
            IWorldManager worldManager,
            IPlayerDataTracker playerData,
            IDiplomacyHelper diplomacyHelper,
            INotificationsSystem notificationsSystem,
            IGenericNotificationBuilder genericNotificationBuilder,
            IDiplomaticNotificationBuilder diplomaticNotificationBuilder,
            IAiHelper aiHelper,
            IGamedataTracker gamedataTracker,
            IBattleHelper battleHelper,
            ILogger logger,
            IDialogManager dialogManager,
            ITradeManager tradeManager) 
            : base(warHelper, treatyHelper, injectionProvider, worldManager, 
                  playerData, diplomacyHelper, notificationsSystem, genericNotificationBuilder,
                  diplomaticNotificationBuilder, aiHelper, gamedataTracker, battleHelper, logger, 
                  dialogManager, tradeManager)
        {
            _warHelper = warHelper;
            _diplomacyHelper = diplomacyHelper;
        }

        public override void DoFastAI()
        {
            foreach (var settlement in Tribe.Settlements.ToArray())
            {
                Tribe.Abandon(settlement);
            }
        }

        public override IEnumerable<Action> DoDeepAI()
        {
            var results = new List<Action>();

            for (var i = 0; i < Tribe.Armies.Count; i++)
            {
                var army = Tribe.Armies[i];

                if (army.CurrentOrder.IsIdle)
                {
                    IEnumerable<Cell> raidTargets = Enumerable.Empty<Cell>();

                    if (army.Cell.Region.Owner != null)
                    {
                        raidTargets = army.Cell.Region.Settlement
                            .EconomicActors
                            .OfType<ICellDevelopment>()
                            .Where(cd => cd.CanBeRaided)
                            .Select(cd => cd.Cell)
                            .Where(c => army.CanPath(c))
                            .ToArray();
                    }

                    if (raidTargets.Any())
                    {
                        results.Add(() =>
                        {
                            army.CurrentOrder = new ArmyRaidOrder(
                                army,
                                raidTargets.OrderBy(c => c.Position.DistanceSq(army.Cell.Position)).First(),
                                _warHelper);
                        });
                    }
                    else
                    {
                        var moveTarget = army.Cell.Region.Neighbors
                            .SelectMany(r => r.Cells)
                            .Where(c => army.CanPath(c))
                            .OrderBy(c => c.Position.DistanceSq(army.Cell.Position))
                            .FirstOrDefault();

                        if (moveTarget != null)
                        {
                            army.CurrentOrder = new ArmyMoveOrder(
                                army,
                                moveTarget);
                        }
                    }
                }

            }

            return results;
        }

        public override DiplomaticResponse GetDiplomaticResponse(string actionType, object actionData)
        {
            return new DiplomaticResponse
            {
                ResponseText = _diplomacyHelper.GenerateText(Tribe, "<^_diplo_bandits>"),
                Actions = new[]
                    {
                    new List<DiplomaticAction>
                    {
                        new DiplomaticAction
                        {
                            ButtonText = "Nevermind",
                            IsEnabled = true,
                            Tooltip = $"End the conversation.",
                            EndConvo = true
                        }
                    }
                }
            };
        }

        public override IEnumerable<ITreatyOption> GetNegotiationAiOptions(List<ITreaty> proposedTreaties)
        {
            return Enumerable.Empty<ITreatyOption>();
        }

        public override IEnumerable<ITreaty> GetNegotiationAiWants(List<ITreaty> proposedTreaties)
        {
            return Enumerable.Empty<ITreaty>();
        }

        public override IEnumerable<ITreatyOption> GetNegotiationPlayerOptions(List<ITreaty> proposedTreaties)
        {
            return Enumerable.Empty<ITreatyOption>();
        }

        public override void HandleMessage(IInterAiMessage message)
        {
        }
    }
}
