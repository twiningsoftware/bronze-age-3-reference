﻿using Bronze.Common.UI.Modals;
using Bronze.Contracts.Data.Diplomacy;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.Data.Diplomacy
{
    public class TradeTreaty : ITreaty 
    {
        private readonly IWarHelper _warHelper;
        private readonly IWorldManager _worldManager;
        private readonly ITreatyHelper _treatyHelper;
        private readonly IDiplomacyHelper _diplomacyHelper;
        private readonly IPlayerDataTracker _playerData;
        private readonly INotificationsSystem _notificationsSystem;
        private readonly IGenericNotificationBuilder _genericNotificationBuilder;
        private readonly ITradeManager _tradeManager;
        private readonly IDialogManager _dialogManager;
        private readonly IGamedataTracker _gamedataTracker;

        public Tribe From { get; private set; }
        public Tribe To { get; private set; }
        public ITradeActor FromActor { get; private set; }
        public ITradeReciever ToActor { get; private set; }
        public string Name { get; private set; }
        public string Description { get; private set; }
        public bool CanBeCancelled => true;
        public string SummaryIconKey => "treaty_trade";
        public bool BlocksWar => false;
        public Dictionary<Item, double> ItemsToSend { get; set; }
        public bool IsRatified { get; private set; }
        private double _timeInvalid;

        public TradeTreaty(
            IWarHelper warHelper,
            IWorldManager worldManager,
            ITreatyHelper treatyHelper,
            IDiplomacyHelper diplomacyHelper,
            IPlayerDataTracker playerData,
            INotificationsSystem notificationsSystem,
            IGenericNotificationBuilder genericNotificationBuilder,
            ITradeManager tradeManager,
            IDialogManager dialogManager,
            IGamedataTracker gamedataTracker)
        {
            _warHelper = warHelper;
            _worldManager = worldManager;
            _treatyHelper = treatyHelper;
            _diplomacyHelper = diplomacyHelper;
            _playerData = playerData;
            _notificationsSystem = notificationsSystem;
            _genericNotificationBuilder = genericNotificationBuilder;
            _tradeManager = tradeManager;
            _dialogManager = dialogManager;
            _gamedataTracker = gamedataTracker;
            ItemsToSend = new Dictionary<Item, double>();
        }
        
        public TradeTreaty Initialize(
            ITradeActor fromActor, 
            ITradeReciever toActor, 
            Dictionary<Item, double> itemsToSend)
        {
            From = fromActor.Settlement.Owner;
            FromActor = fromActor;
            To = toActor.Settlement.Owner;
            ToActor = toActor;
            ItemsToSend = itemsToSend;
            IsRatified = false;

            if (From == _playerData.PlayerTribe)
            {
                Name = $"Exports From {FromActor.Settlement.Name}";
                Description = $"Exporting items from {FromActor.Settlement.Name} to {ToActor.Settlement.Name}";
            }
            else
            {
                Name = $"Imports From {FromActor.Settlement.Name}";
                Description = $"Importing items from {FromActor.Settlement.Name} to {ToActor.Settlement.Name}";
            }

            return this;
        }
        
        public void OnRatification()
        {
            IsRatified = true;
            var route = _tradeManager.CreateTradeRoute(FromActor, ToActor);
            route.IsTreatyRoute = true;

            foreach(var item in ItemsToSend.Keys)
            {
                if (ItemsToSend[item] > 0)
                {
                    route.SetExportPerMonth(item, ItemsToSend[item]);
                }
            }
            
            if(From == _playerData.PlayerTribe
                && !To.DiplomaticMemories.Any(dm => dm.Category == "treaty_trade"))
            {
                To.AddDiplomaticMemory(new DiplomaticMemory(
                    "treaty_trade",
                    "Trading partners.",
                    _worldManager.Month,
                    0,
                    0.04,
                    10,
                    20));
            }
        }
        
        public void StepSimulation(double elapsedMonths)
        {
            if(FromActor.Settlement.Owner != From
                || ToActor.Settlement.Owner != To
                || FromActor.TradeRoute == null 
                || FromActor.TradeRoute.ToActor != ToActor)
            {
                _treatyHelper.RemoveTreaty(this);
            }
            else if(FromActor.TradeRoute.PathCalculator.PathFinished && !FromActor.TradeRoute.PathCalculator.PathSuccessful)
            {
                _timeInvalid += elapsedMonths;

                if(_timeInvalid > 1)
                {
                    _treatyHelper.RemoveTreaty(this);
                }
            }
            else
            {
                _timeInvalid = 0;
            }
        }

        public void OnRemoval()
        {
            if(FromActor.TradeRoute != null)
            {
                _tradeManager.RemoveTradeRoute(FromActor.TradeRoute);
            }
            
            if (From == _playerData.PlayerTribe
                && !_tradeManager.GetTradeRoutesFor(To).Any(tr => tr.FromSettlement.Owner == _playerData.PlayerTribe))
            {
                To.RemoveDiplomaticMemory("treaty_trade");
            }
        }

        public void LoadFrom(IWorldManager worldManager, SerializedObject treatyRoot)
        {
            var fromTribe = treatyRoot.GetObjectReference(
                "from_tribe",
                worldManager.Tribes,
                t => t.Id);

            var fromSettlement = treatyRoot.GetObjectReference(
                "from_settlement",
                fromTribe.Settlements,
                s => s.Id);

            var fromActors = fromSettlement.Districts
                .SelectMany(d => d.Structures)
                .OfType<IEconomicActor>()
                .Concat(fromSettlement.Region.Cells.Select(c => c.Development))
                .OfType<ITradeActor>()
                .Where(ea => ea != null)
                .ToArray();

            var fromActor = treatyRoot.GetObjectReference(
                "from_actor",
                fromActors,
                a => a.Id);

            var toTribe = treatyRoot.GetObjectReference(
                "to_tribe",
                worldManager.Tribes,
                t => t.Id);

            var toSettlement = treatyRoot.GetObjectReference(
                "to_settlement",
                toTribe.Settlements,
                s => s.Id);

            var toActors = toSettlement.Districts
                .SelectMany(d => d.Structures)
                .OfType<IEconomicActor>()
                .Concat(toSettlement.Region.Cells.Select(c => c.Development))
                .OfType<ITradeReciever>()
                .Where(ea => ea != null)
                .ToArray();

            var toActor = treatyRoot.GetObjectReference(
                "to_actor",
                toActors,
                a => a.Id);
            
            Initialize(fromActor, toActor, new Dictionary<Item, double>());
            IsRatified = true;
            RevertChanges();
        }

        public void SerializeTo(SerializedObject treatyRoot)
        {
            treatyRoot.Set("from_tribe", From.Id);
            treatyRoot.Set("from_settlement", FromActor.Settlement.Id);
            treatyRoot.Set("from_actor", FromActor.Id);
            treatyRoot.Set("to_tribe", To.Id);
            treatyRoot.Set("to_settlement", ToActor.Settlement.Id);
            treatyRoot.Set("to_actor", ToActor.Id);
        }
        
        public IEnumerable<TreatyValueDetail> GetValueDetailFor(Tribe aiTribe)
        {
            var valueMod = From == aiTribe ? -1 : 1;

            var relativePower = _diplomacyHelper.GetRelativePower(From, To);
            
            var value = new List<TreatyValueDetail>();
            value.Add(new TreatyValueDetail("Base cost", 10 * valueMod));

            var aiSettlement = FromActor.Settlement.Owner == aiTribe ? FromActor.Settlement : ToActor.Settlement;

            var importOrExport = FromActor.Settlement.Owner == aiTribe ? "Import" : "Export";

            foreach (var item in ItemsToSend.Keys)
            {
                if (ItemsToSend[item] > 0)
                {
                    value.Add(new TreatyValueDetail($"{importOrExport} {ItemsToSend[item]} {item.Name}", (int)(ItemsToSend[item] * aiTribe.Controller.TradeValueOf(item, aiSettlement) * valueMod)));
                }
            }

            if (To == _playerData.PlayerTribe)
            {
                var trustMod = _diplomacyHelper.GetTrustMod(From);
                value.Add(new TreatyValueDetail("Trust", (int)(value.Sum(v => v.Value) * trustMod)));
            }

            return value;
        }

        public bool IsCoercing(Tribe tribe)
        {
            return false;
        }

        public void ShowEditor(
            Action finishedCallback, 
            IEnumerable<ITreaty> treatiesInNegotiation)
        {
            _dialogManager.PushModal<TradeTreatyEditorModal, TradeTreatyEditorModalContext>(
                new TradeTreatyEditorModalContext
                {
                    From = From,
                    To = To,
                    Callback = (treaty) => finishedCallback(),
                    TreatiesInNegotiation = treatiesInNegotiation.ToArray(),
                    ExistingTreaty = this
                });
        }

        public void ApplyChanges()
        {
            if (FromActor?.TradeRoute != null)
            {
                foreach (var item in ItemsToSend.Keys)
                {
                    FromActor.TradeRoute.SetExportPerMonth(item, ItemsToSend[item]);
                }
            }
        }

        public void RevertChanges()
        {
            ItemsToSend.Clear();
            foreach(var item in _gamedataTracker.Items)
            {
                ItemsToSend.Add(item, 0);
            }

            if(FromActor?.TradeRoute != null)
            {
                foreach(var itemRate in FromActor.TradeRoute.Exports)
                {
                    ItemsToSend[itemRate.Item] = itemRate.PerMonth;
                }
            }
        }
    }
}
