﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bronze.Common.UI.Modals;
using Bronze.Contracts.Data.Diplomacy;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.Data.Diplomacy
{
    public class TradeTreatyOption : ITreatyOption
    {
        public string Name { get; }
        public string Description { get; }
        public bool HasParameters => true;

        private readonly IDialogManager _dialogManager;
        private readonly IInjectionProvider _injectionProvider;
        private readonly ITradeActor[] _actorsAvailableToExport;
        private readonly Tribe _fromTribe;
        private readonly Tribe _toTribe;
        
        public TradeTreatyOption(
            IDialogManager dialogManager,
            Tribe fromTribe, 
            Tribe toTribe, 
            ITradeActor[] actorsAvailableToExport, 
            IInjectionProvider injectionProvider,
            bool fromPlayer)
        {
            _dialogManager = dialogManager;
            _injectionProvider = injectionProvider;
            _actorsAvailableToExport = actorsAvailableToExport;
            _fromTribe = fromTribe;
            _toTribe = toTribe;

            if(fromPlayer)
            {
                Name = "Export Agreement";
                Description = "Make an agreement to send items to " + toTribe.Name;
            }
            else
            {
                Name = "Import Agreement";
                Description = "Make an agreement to import items from " + toTribe.Name;
            }
        }
        
        public ITreaty BuildTreaty()
        {
            throw new NotImplementedException();
        }

        public void ShowEditor(Action<ITreaty> callback, IEnumerable<ITreaty> treatiesInNegotiation)
        {
            _dialogManager.PushModal<TradeTreatyEditorModal, TradeTreatyEditorModalContext>(
                new TradeTreatyEditorModalContext
                {
                    From = _fromTribe,
                    To = _toTribe,
                    Callback = callback,
                    TreatiesInNegotiation = treatiesInNegotiation.ToArray()
                });
        }
    }
}
