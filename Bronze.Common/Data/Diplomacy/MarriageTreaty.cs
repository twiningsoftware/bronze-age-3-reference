﻿using Bronze.Contracts.Data.Diplomacy;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.Simulation.Events;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.Data.Diplomacy
{
    public class MarriageTreaty : ITreaty 
    {
        private readonly IWorldManager _worldManager;
        private readonly ITreatyHelper _treatyHelper;
        private readonly IDiplomacyHelper _diplomacyHelper;
        private readonly IPlayerDataTracker _playerData;
        private readonly INotificationsSystem _notificationsSystem;
        private readonly IGenericNotificationBuilder _genericNotificationBuilder;
        private readonly ISimulationEventBus _simulationEventBus;

        public Tribe From { get; private set; }
        public Tribe To { get; private set; }
        public NotablePerson FromSpouse { get; private set; }
        public NotablePerson ToSpouse { get; private set; }
        public double FromSpouseScore { get; private set; }

        public string Name { get; private set; }
        public string Description { get; private set; }
        public bool CanBeCancelled => true;
        public string SummaryIconKey => "treaty_marriage";
        public bool BlocksWar => false;

        public MarriageTreaty(
            IWorldManager worldManager,
            ITreatyHelper treatyHelper,
            IDiplomacyHelper diplomacyHelper,
            IPlayerDataTracker playerData,
            INotificationsSystem notificationsSystem,
            IGenericNotificationBuilder genericNotificationBuilder,
            ISimulationEventBus simulationEventBus)
        {
            _worldManager = worldManager;
            _treatyHelper = treatyHelper;
            _diplomacyHelper = diplomacyHelper;
            _playerData = playerData;
            _notificationsSystem = notificationsSystem;
            _genericNotificationBuilder = genericNotificationBuilder;
            _simulationEventBus = simulationEventBus;
        }
        
        public MarriageTreaty Initialize(
            Tribe from, 
            Tribe to, 
            NotablePerson fromSpouse,
            NotablePerson toSpouse,
            double fromSpouseScore)
        {
            if (to == _playerData.PlayerTribe)
            {
                if(fromSpouse.IsMale)
                {
                    Name = "Recieve Groom";
                }
                else
                {
                    Name = "Recieve Bride";
                }
                
                Description = $"They will send {fromSpouse.Name} to marry {toSpouse.Name} and join our court.";
            }
            else
            {
                if (fromSpouse.IsMale)
                {
                    Name = "Send Groom";
                }
                else
                {
                    Name = "Send Bride";
                }

                Description = $"We will send {fromSpouse.Name} to marry {toSpouse.Name} and join their court.";
            }
            From = from;
            To = to;
            FromSpouse = fromSpouse;
            ToSpouse = toSpouse;
            FromSpouseScore = fromSpouseScore;

            return this;
        }
        
        public void OnRatification()
        {
            FromSpouse.Owner = To;
            FromSpouse.Spouse = ToSpouse;
            ToSpouse.Spouse = FromSpouse;
            From.NotablePeople.Remove(FromSpouse);
            To.NotablePeople.Add(FromSpouse);

            FromSpouse.ClearAssignments();

            _simulationEventBus.SendEvent(new NotablePersonMarriageSimulationEvent(ToSpouse, FromSpouse));

            _simulationEventBus.SendEvent(new NotablePersonOwnerChangedSimulationEvent
            {
                OldOwner = From,
                NewOwner = To,
                Person = FromSpouse
            });
            
            if (To == _playerData.PlayerTribe)
            {
                _notificationsSystem.AddNotification(_genericNotificationBuilder
                    .BuildPersonNotification(
                        "ui_notification_wedding",
                        "A Royal Wedding",
                        $"{ToSpouse.Name} has married.",
                        "A Royal Wedding",
                        $"{ToSpouse.Name} has married {FromSpouse.Name}",
                        ToSpouse,
                        null));
            }
            else if(From == _playerData.PlayerTribe)
            {
                _notificationsSystem.AddNotification(_genericNotificationBuilder
                    .BuildPersonNotification(
                        "ui_notification_leftcourt",
                        $"{FromSpouse.Name} Has Left Your Court.",
                        $"{FromSpouse.Name} has left your court to join the {To.Name}.",
                        $"{FromSpouse.Name} Has Left Your Court.",
                        $"{FromSpouse.Name} has left your court to join the {To.Name}.",
                        FromSpouse,
                        null));
            }
            
            if (To == _playerData.PlayerTribe)
            {
                From.AddDiplomaticMemory(
                    new DiplomaticMemory(
                        FromSpouse.Id + "transfer",
                        $"Sent {FromSpouse.Name} for marriage.",
                        _worldManager.Month,
                        1,
                        -0.01,
                        -1 * FromSpouseScore,
                        0));
            }
            if(From == _playerData.PlayerTribe)
            {
                To.AddDiplomaticMemory(
                    new DiplomaticMemory(
                        FromSpouse.Id + "transfer",
                        $"Gave {FromSpouse.Name} for marriage.",
                        _worldManager.Month,
                        1,
                        -0.01,
                        FromSpouseScore,
                        0));
            }
        }

        public void StepSimulation(double elapsedMonths)
        {
            _treatyHelper.RemoveTreaty(this);
        }

        public void OnRemoval()
        {
        }

        public void LoadFrom(IWorldManager worldManager, SerializedObject treatyRoot)
        {
            From = treatyRoot.GetObjectReference(
                    "from",
                    worldManager.Tribes,
                    t => t.Id);
            To = treatyRoot.GetObjectReference(
                    "to",
                    worldManager.Tribes,
                    t => t.Id);
            Name = treatyRoot.GetString("name");
            Description = treatyRoot.GetString("description");
        }

        public void SerializeTo(SerializedObject treatyRoot)
        {
            treatyRoot.Set("name", Name);
            treatyRoot.Set("description", Description);
            treatyRoot.Set("from", From.Id);
            treatyRoot.Set("to", To.Id);
        }
        
        public IEnumerable<TreatyValueDetail> GetValueDetailFor(Tribe aiTribe)
        {
            var valueMod = From == aiTribe ? -1 : 1;

            var relativePower = _diplomacyHelper.GetRelativePower(From, To);

            var value = new List<TreatyValueDetail>();
            value.Add(new TreatyValueDetail("Base cost", 20 * valueMod));
            value.Add(new TreatyValueDetail("Value", (int)(FromSpouseScore * valueMod)));
            if (To == _playerData.PlayerTribe)
            {
                var trustMod = _diplomacyHelper.GetTrustMod(From);
                value.Add(new TreatyValueDetail("Trust", (int)(value.Sum(v => v.Value) * trustMod)));
            }

            return value;
        }

        public bool IsCoercing(Tribe tribe)
        {
            return false;
        }

        public void ShowEditor(Action finishedCallback, IEnumerable<ITreaty> treatiesInNegotiation)
        {
        }

        public void ApplyChanges()
        {
        }

        public void RevertChanges()
        {
        }
    }
}
