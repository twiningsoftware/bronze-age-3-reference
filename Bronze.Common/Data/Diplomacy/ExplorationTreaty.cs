﻿using Bronze.Contracts.Data.Diplomacy;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.Data.Diplomacy
{
    public class ExplorationTreaty : ITreaty
    {
        private readonly IDiplomacyHelper _diplomacyHelper;
        private readonly ILosTracker _losTracker;
        private readonly IPlayerDataTracker _playerData;
        private readonly IWorldManager _worldManager;

        public Tribe From { get; private set; }
        public Tribe To { get; private set; }
        public string Name { get; private set; }
        public string Description { get; private set; }
        public double StartDate { get; private set; }
        public bool CanBeCancelled => true;
        public string SummaryIconKey => "treaty_exploration";
        public bool BlocksWar => false;

        public ExplorationTreaty(
            IDiplomacyHelper diplomacyHelper,
            ILosTracker losTracker,
            IPlayerDataTracker playerData,
            IWorldManager worldManager)
        {
            _diplomacyHelper = diplomacyHelper;
            _losTracker = losTracker;
            _playerData = playerData;
            _worldManager = worldManager;
        }
        
        public IEnumerable<TreatyValueDetail> GetValueDetailFor(Tribe aiTribe)
        {
            var valueMod = From == aiTribe ? -1 : 1;
            
            var relativePower = _diplomacyHelper.GetRelativePower(From, To);

            var value = new List<TreatyValueDetail>();
            value.Add(new TreatyValueDetail("Base cost", 10 * valueMod));
            value.Add(new TreatyValueDetail("Relative power", (int)((relativePower - 0.5) * -10 * valueMod)));

            if(To == _playerData.PlayerTribe)
            {
                var trustMod = _diplomacyHelper.GetTrustMod(From);
                value.Add(new TreatyValueDetail("Trust", (int)(value.Sum(v => v.Value) * trustMod)));
            }

            return value;
        }

        public ExplorationTreaty Initialize(Tribe from, Tribe to, double startDate)
        {
            From = from;
            To = to;
            if (_playerData.PlayerTribe == to)
            {
                Name = "Share Their Maps";
                Description = "They will give us copies of their maps and scouting reports.";
            }
            else
            {
                Name = "Share Our Maps";
                Description = "We will give them copies of our maps and scouting reports.";
            }
            StartDate = startDate;
            return this;
        }

        public void LoadFrom(IWorldManager worldManager, SerializedObject treatyRoot)
        {
            Initialize(
                treatyRoot.GetObjectReference(
                    "from",
                    worldManager.Tribes,
                    t => t.Id),
                treatyRoot.GetObjectReference(
                    "to",
                    worldManager.Tribes,
                    t => t.Id),
                treatyRoot.GetDouble("start_date"));
        }

        public void OnRatification()
        {
            _losTracker.SetLosCopy(From, To);

            if(From == _playerData.PlayerTribe)
            {
                To.AddDiplomaticMemory(new DiplomaticMemory(
                    "treaty_exploration",
                    "Sharing your maps with us.",
                    StartDate,
                    0,
                    0.04,
                    0,
                    10));
            }
        }

        public void OnRemoval()
        {
            _losTracker.RemoveLosCopy(From, To);
            if(From == _playerData.PlayerTribe)
            {
                To.RemoveDiplomaticMemory("treaty_exploration");
            }
        }

        public void SerializeTo(SerializedObject treatyRoot)
        {
            treatyRoot.Set("from", From.Id);
            treatyRoot.Set("to", To.Id);
            treatyRoot.Set("start_date", StartDate);
        }

        public void StepSimulation(double elapsedMonths)
        {
        }

        public bool IsCoercing(Tribe tribe)
        {
            return false;
        }

        public void ShowEditor(Action finishedCallback, IEnumerable<ITreaty> treatiesInNegotiation)
        {
        }

        public void ApplyChanges()
        {
        }

        public void RevertChanges()
        {
        }
    }
}
