﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bronze.Common.UI.Modals;
using Bronze.Contracts.Data.Diplomacy;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.Data.Diplomacy
{
    public class TradeSettlementOption : ITreatyOption
    {
        public string Name { get; }
        public string Description { get; }
        public bool HasParameters => true;

        private readonly IDialogManager _dialogManager;
        private readonly Tribe _fromTribe;
        private readonly Tribe _toTribe;

        public TradeSettlementOption(
            IDialogManager dialogManager,
            Tribe fromTribe, 
            Tribe toTribe)
        {
            _dialogManager = dialogManager;
            _fromTribe = fromTribe;
            _toTribe = toTribe;

            Name = "Give Settlement";
            Description = "Make and agreement to transfer control of a settlement.";
        }
        
        public ITreaty BuildTreaty()
        {
            throw new NotImplementedException();
        }

        public void ShowEditor(Action<ITreaty> callback, IEnumerable<ITreaty> treatiesInNegotiation)
        {
            _dialogManager.PushModal<TradeSettlementTreatyEditorModal, TradeSettlementTreatyEditorModalContext>(
                new TradeSettlementTreatyEditorModalContext
                {
                    From = _fromTribe,
                    To = _toTribe,
                    Callback = callback,
                    TreatiesInNegotiation = treatiesInNegotiation.ToArray()
                });
        }
    }
}
