﻿using Bronze.Contracts;
using Bronze.Contracts.Data.Diplomacy;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.Data.Diplomacy
{
    public class ThreatenTreaty : ITreaty 
    {
        public static readonly int TreatyLength = 12;

        private readonly IWarHelper _warHelper;
        private readonly IWorldManager _worldManager;
        private readonly ITreatyHelper _treatyHelper;
        private readonly IDiplomacyHelper _diplomacyHelper;
        private readonly IPlayerDataTracker _playerData;
        private readonly INotificationsSystem _notificationsSystem;
        private readonly IGenericNotificationBuilder _genericNotificationBuilder;
        
        public Tribe From { get; private set; }
        public Tribe To { get; private set; }
        public string Name { get; private set; }
        public string Description { get; private set; }
        public double EndDate { get; private set; }
        public bool CanBeCancelled => !_isRatified;
        public string SummaryIconKey => "treaty_threaten";
        public bool BlocksWar => true;
        private bool _isRatified;

        public ThreatenTreaty(
            IWarHelper warHelper,
            IWorldManager worldManager,
            ITreatyHelper treatyHelper,
            IDiplomacyHelper diplomacyHelper,
            IPlayerDataTracker playerData,
            INotificationsSystem notificationsSystem,
            IGenericNotificationBuilder genericNotificationBuilder)
        {
            _warHelper = warHelper;
            _worldManager = worldManager;
            _treatyHelper = treatyHelper;
            _diplomacyHelper = diplomacyHelper;
            _playerData = playerData;
            _notificationsSystem = notificationsSystem;
            _genericNotificationBuilder = genericNotificationBuilder;
        }
        
        public ThreatenTreaty Initialize(Tribe from, Tribe to)
        {
            if(from == _playerData.PlayerTribe)
            {
                Name = "We Won't Attack";
                Description = $"Threaten them with war, but promise not to attack until {Util.FriendlyDateDisplay(_worldManager.Month + ThreatenTreaty.TreatyLength)}.";
            }
            else
            {
                Name = "They Won't Attack";
                Description = $"They promise to not attack us until {Util.FriendlyDateDisplay(_worldManager.Month + ThreatenTreaty.TreatyLength)}.";
            }

            From = from;
            To = to;
            EndDate = _worldManager.Month + TreatyLength;
            return this;
        }
        
        public void OnRatification()
        {
            _isRatified = true;

            if(From == _playerData.PlayerTribe)
            {
                To.AddDiplomaticMemory(
                    new DiplomaticMemory(
                        "threatentreaty",
                        "Threatened us.",
                        _worldManager.Month,
                        1,
                        0,
                        0,
                        -20));
            }
        }
        
        public void StepSimulation(double elapsedMonths)
        {
            if (_worldManager.Month >= EndDate)
            {
                _treatyHelper.RemoveTreaty(this);

                if (From == _playerData.PlayerTribe || To == _playerData.PlayerTribe)
                {
                    _notificationsSystem.AddNotification(_genericNotificationBuilder
                        .BuildSimpleNotification(
                            "ui_notification_peace",
                            "Truce Ended",
                            $"The guarantee of peace from {(From == _playerData.PlayerTribe ? To.Name : From.Name)} has expired.",
                            $"Truce With {(From == _playerData.PlayerTribe ? To.Name : From.Name)} Ended",
                            $"The guarantee of peace from {(From == _playerData.PlayerTribe ? To.Name : From.Name)} has expired, either tribe may now declare war.",
                            null));
                }
            }
        }

        public void SerializeTo(SerializedObject treatyRoot)
        {
            treatyRoot.Set("name", Name);
            treatyRoot.Set("description", Description);
            treatyRoot.Set("from", From.Id);
            treatyRoot.Set("to", To.Id);
            treatyRoot.Set("end_date", EndDate);
        }

        public void LoadFrom(IWorldManager worldManager, SerializedObject treatyRoot)
        {
            From = treatyRoot.GetObjectReference(
                    "from",
                    worldManager.Tribes,
                    t => t.Id);
            To = treatyRoot.GetObjectReference(
                    "to",
                    worldManager.Tribes,
                    t => t.Id);
            Name = treatyRoot.GetString("name");
            Description = treatyRoot.GetString("description");
            EndDate = treatyRoot.GetDouble("end_date");
            _isRatified = true;
        }

        public void OnRemoval()
        {
            if (From == _playerData.PlayerTribe)
            {
                To.AddDiplomaticMemory(
                    new DiplomaticMemory(
                        "threatentreaty",
                        "Threatened us.",
                        _worldManager.Month,
                        1,
                        -0.08,
                        0,
                        -20));
            }
        }
        
        public IEnumerable<TreatyValueDetail> GetValueDetailFor(Tribe aiTribe)
        {
            var valueMod = From == aiTribe ? -1 : 1;

            var relativePower = _diplomacyHelper.GetRelativePower(From, To);

            var value = new List<TreatyValueDetail>();
            value.Add(new TreatyValueDetail("Relative power", (int)((relativePower - 0.5) * 40 * valueMod)));

            if (To == _playerData.PlayerTribe)
            {
                var trustMod = _diplomacyHelper.GetTrustMod(From);
                value.Add(new TreatyValueDetail("Trust", (int)(value.Sum(v => v.Value) * trustMod)));
            }

            return value;
        }

        public bool IsCoercing(Tribe tribe)
        {
            return To == tribe;
        }

        public void ShowEditor(Action finishedCallback, IEnumerable<ITreaty> treatiesInNegotiation)
        {
        }

        public void ApplyChanges()
        {
        }

        public void RevertChanges()
        {
        }
    }
}
