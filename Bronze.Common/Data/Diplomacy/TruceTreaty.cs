﻿using Bronze.Contracts;
using Bronze.Contracts.Data.Diplomacy;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.Data.Diplomacy
{
    public class TruceTreaty : ITreaty 
    {
        public static readonly int PeaceTreatyLength = 12;

        private readonly IWarHelper _warHelper;
        private readonly IWorldManager _worldManager;
        private readonly ITreatyHelper _treatyHelper;
        private readonly IDiplomacyHelper _diplomacyHelper;
        private readonly IPlayerDataTracker _playerData;
        private readonly INotificationsSystem _notificationsSystem;
        private readonly IGenericNotificationBuilder _genericNotificationBuilder;
        
        public Tribe From { get; private set; }
        public Tribe To { get; private set; }
        public string Name { get; private set; }
        public string Description { get; private set; }
        public double EndDate { get; private set; }
        public double EndedWarScore { get; private set; }
        public bool CanBeCancelled => false;
        public string SummaryIconKey => "treaty_peace";
        public bool BlocksWar => true;

        public TruceTreaty(
            IWarHelper warHelper,
            IWorldManager worldManager,
            ITreatyHelper treatyHelper,
            IDiplomacyHelper diplomacyHelper,
            IPlayerDataTracker playerData,
            INotificationsSystem notificationsSystem,
            IGenericNotificationBuilder genericNotificationBuilder)
        {
            _warHelper = warHelper;
            _worldManager = worldManager;
            _treatyHelper = treatyHelper;
            _diplomacyHelper = diplomacyHelper;
            _playerData = playerData;
            _notificationsSystem = notificationsSystem;
            _genericNotificationBuilder = genericNotificationBuilder;
        }
        
        public TruceTreaty Initialize(Tribe from, Tribe to, double endedWarScore)
        {
            From = from;
            To = to;
            Name = "Truce";
            Description = $"Truce until {Util.FriendlyDateDisplay(_worldManager.Month + PeaceTreatyLength)}";
            EndDate = _worldManager.Month + PeaceTreatyLength;
            EndedWarScore = endedWarScore;
            return this;
        }

        public void LoadFrom(IWorldManager worldManager, SerializedObject treatyRoot)
        {
            From = treatyRoot.GetObjectReference(
                    "from",
                    worldManager.Tribes,
                    t => t.Id);
            To = treatyRoot.GetObjectReference(
                    "to",
                    worldManager.Tribes,
                    t => t.Id);
            Name = treatyRoot.GetString("name");
            Description = treatyRoot.GetString("description");
            EndDate = treatyRoot.GetDouble("end_date");
            EndedWarScore = treatyRoot.GetDouble("ended_war_score");
        }

        public void OnRatification()
        {
            var wars = From.CurrentWars
                .Where(w => w.Aggressor == To || w.Defender == To)
                .ToArray();

            foreach(var war in wars)
            {
                _warHelper.EndWar(war);
            }

            Tribe aiTribe = null;
            bool playerWon = false;
            bool aiWon = false;
            if(From == _playerData.PlayerTribe)
            {
                aiTribe = To;
                playerWon = EndedWarScore > 0;
                aiWon = EndedWarScore < 0;
            }
            else if(To == _playerData.PlayerTribe)
            {
                aiTribe = From;
                playerWon = EndedWarScore < 0;
                aiWon = EndedWarScore > 0;
            }

            if (aiTribe != null)
            {
                if (playerWon)
                {
                    aiTribe.AddDiplomaticMemory(
                        new DiplomaticMemory(
                            "war",
                            "Defeated us in war.",
                            _worldManager.Month,
                            1,
                            -0.08,
                            -100,
                            -20));
                }
                if(aiWon)
                {
                    aiTribe.AddDiplomaticMemory(
                        new DiplomaticMemory(
                            "war",
                            "Defeated you in war.",
                            _worldManager.Month,
                            1,
                            -0.08,
                            -100,
                            -20));
                }
            }

            if(From == _playerData.PlayerTribe || To == _playerData.PlayerTribe)
            {
                _notificationsSystem.AddNotification(_genericNotificationBuilder
                    .BuildSimpleNotification(
                        "ui_notification_peace",
                        "Truce",
                        $"The war with {(From == _playerData.PlayerTribe ? To.Name : From.Name)} has ended.",
                        $"Truce With {(From == _playerData.PlayerTribe ? To.Name : From.Name)}",
                        $"The war with {(From == _playerData.PlayerTribe ? To.Name : From.Name)} has ended, a truce will be in place until {Util.FriendlyDateDisplay(EndDate)}.",
                        null));
            }
            else if(_playerData.PlayerTribe.HasKnowledgeOf(From) || _playerData.PlayerTribe.HasKnowledgeOf(To))
            {
                _notificationsSystem.AddNotification(_genericNotificationBuilder
                    .BuildTribeNotification(
                        "ui_notification_diplomacy",
                        "News of Peace",
                        $"Peace between {From.Name} and {To.Name}",
                        "News of Peace",
                        $"There are reports that the war between {From.Name} and {To.Name} has ended.",
                        From,
                        To));
            }
        }

        public void SerializeTo(SerializedObject treatyRoot)
        {
            treatyRoot.Set("name", Name);
            treatyRoot.Set("description", Description);
            treatyRoot.Set("from", From.Id);
            treatyRoot.Set("to", To.Id);
            treatyRoot.Set("end_date", EndDate);
            treatyRoot.Set("ended_war_score", EndedWarScore);
        }

        public void StepSimulation(double elapsedMonths)
        {
            if(_worldManager.Month >= EndDate)
            {
                _treatyHelper.RemoveTreaty(this);

                if (From == _playerData.PlayerTribe || To == _playerData.PlayerTribe)
                {
                    _notificationsSystem.AddNotification(_genericNotificationBuilder
                        .BuildSimpleNotification(
                            "ui_notification_peace",
                            "Truce Ended",
                            $"The truce with {(From == _playerData.PlayerTribe ? To.Name : From.Name)} has ended.",
                            $"Truce With {(From == _playerData.PlayerTribe ? To.Name : From.Name)} Ended",
                            $"The truce with {(From == _playerData.PlayerTribe ? To.Name : From.Name)} has ended, either tribe may now declare war.",
                            null));
                }
            }
        }

        public void OnRemoval()
        {
        }
        
        public IEnumerable<TreatyValueDetail> GetValueDetailFor(Tribe aiTribe)
        {
            var valueMod = From == aiTribe ? -1 : 1;

            var relativePower = _diplomacyHelper.GetRelativePower(From, To);

            var value = new List<TreatyValueDetail>();
            value.Add(new TreatyValueDetail("Base cost", -10));
            value.Add(new TreatyValueDetail("Relative power", (int)((relativePower - 0.5) * 20 * valueMod)));
            if (To == _playerData.PlayerTribe)
            {
                var trustMod = _diplomacyHelper.GetTrustMod(From);
                value.Add(new TreatyValueDetail("Trust", (int)(value.Sum(v => v.Value) * trustMod)));
            }

            var adjWarScore = 0;

            if (_worldManager.Month < EndDate)
            {
                adjWarScore = (int)EndedWarScore;
            }
            else
            {
                adjWarScore = Math.Max(0, (int)((1 - (_worldManager.Month - EndDate) / PeaceTreatyLength) * EndedWarScore));
            }

            value.Add(new TreatyValueDetail("War score", adjWarScore * valueMod));
            
            return value;
        }

        public bool IsCoercing(Tribe tribe)
        {
            return (From == tribe && EndedWarScore < 0)
                || (To == tribe && EndedWarScore > 0);
        }

        public void ShowEditor(Action finishedCallback, IEnumerable<ITreaty> treatiesInNegotiation)
        {
        }

        public void ApplyChanges()
        {
        }

        public void RevertChanges()
        {
        }
    }
}
