﻿using Bronze.Contracts.Data.Diplomacy;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.Data.Diplomacy
{
    public class MilitaryAccessTreaty : ITreaty
    {
        private readonly IDiplomacyHelper _diplomacyHelper;
        private readonly IPlayerDataTracker _playerData;

        public Tribe From { get; private set; }
        public Tribe To { get; private set; }
        public string Name { get; private set; }
        public string Description { get; private set; }
        public double StartDate { get; private set; }
        public bool CanBeCancelled => true;
        public string SummaryIconKey => "treaty_military_access";
        public bool BlocksWar => false;

        public MilitaryAccessTreaty(
            IDiplomacyHelper diplomacyHelper,
            IPlayerDataTracker playerData)
        {
            _diplomacyHelper = diplomacyHelper;
            _playerData = playerData;
        }

        public IEnumerable<TreatyValueDetail> GetValueDetailFor(Tribe aiTribe)
        {
            var valueMod = From == aiTribe ? -1 : 1;
            
            var relativePower = _diplomacyHelper.GetRelativePower(From, To);

            var value = new List<TreatyValueDetail>();
            value.Add(new TreatyValueDetail("Base cost", 20 * valueMod));
            value.Add(new TreatyValueDetail("Relative power", (int)((relativePower - 0.5) * -20 * valueMod)));

            if (To == _playerData.PlayerTribe)
            {
                var trustMod = _diplomacyHelper.GetTrustMod(From);
                value.Add(new TreatyValueDetail("Trust", (int)(value.Sum(v => v.Value) * trustMod)));
            }

            return value;
        }
        
        public MilitaryAccessTreaty Initialize(Tribe from, Tribe to, double startDate)
        {
            From = from;
            To = to;
            if (_playerData.PlayerTribe == to)
            {
                Name = "Access to Their Lands";
                Description = "They will give us permission to travel through their territory.";
            }
            else
            {
                Name = "Access to Our Lands";
                Description = "We will give them permission to travel through our territory.";
            }
            
            StartDate = startDate;
            
            return this;
        }

        public void LoadFrom(IWorldManager worldManager, SerializedObject treatyRoot)
        {
            Initialize(
                treatyRoot.GetObjectReference(
                    "from",
                    worldManager.Tribes,
                    t => t.Id),
                treatyRoot.GetObjectReference(
                    "to",
                    worldManager.Tribes,
                    t => t.Id),
                treatyRoot.GetDouble("start_date"));

            From.AllowedAccess.Add(To);
        }

        public void OnRatification()
        {
            From.AllowedAccess.Add(To);

            if (From == _playerData.PlayerTribe)
            {
                To.AddDiplomaticMemory(new DiplomaticMemory(
                    "treaty_military_access",
                    "Giving us access to your lands.",
                    StartDate,
                    0,
                    0.04,
                    0,
                    20));
            }
        }

        public void OnRemoval()
        {
            if (From == _playerData.PlayerTribe)
            {
                To.RemoveDiplomaticMemory("treaty_military_access");
            }

            From.AllowedAccess.Remove(To);
        }

        public void SerializeTo(SerializedObject treatyRoot)
        {
            treatyRoot.Set("name", Name);
            treatyRoot.Set("description", Description);
            treatyRoot.Set("from", From.Id);
            treatyRoot.Set("to", To.Id);
            treatyRoot.Set("start_date", StartDate);
        }

        public void StepSimulation(double elapsedMonths)
        {
        }

        public bool IsCoercing(Tribe tribe)
        {
            return false;
        }

        public void ShowEditor(Action finishedCallback, IEnumerable<ITreaty> treatiesInNegotiation)
        {
        }

        public void ApplyChanges()
        {
        }

        public void RevertChanges()
        {
        }
    }
}
