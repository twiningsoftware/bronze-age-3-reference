﻿using Bronze.Contracts.Data.Diplomacy;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.Simulation.Events;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.Data.Diplomacy
{
    public class TradeSettlementTreaty : ITreaty 
    {
        private readonly IWarHelper _warHelper;
        private readonly IWorldManager _worldManager;
        private readonly ITreatyHelper _treatyHelper;
        private readonly IDiplomacyHelper _diplomacyHelper;
        private readonly IPlayerDataTracker _playerData;
        private readonly INotificationsSystem _notificationsSystem;
        private readonly IGenericNotificationBuilder _genericNotificationBuilder;
        private readonly ISimulationEventBus _simulationEventBus;
        
        public Tribe From { get; private set; }
        public Tribe To { get; private set; }
        public Settlement Settlement { get; private set; }
        public string Name { get; private set; }
        public string Description { get; private set; }
        public bool CanBeCancelled => true;
        public string SummaryIconKey => "treaty_trade_settlement";
        public bool BlocksWar => false;
        public double RegionScore { get; private set; }

        public TradeSettlementTreaty(
            IWarHelper warHelper,
            IWorldManager worldManager,
            ITreatyHelper treatyHelper,
            IDiplomacyHelper diplomacyHelper,
            IPlayerDataTracker playerData,
            INotificationsSystem notificationsSystem,
            IGenericNotificationBuilder genericNotificationBuilder,
            ISimulationEventBus simulationEventBus)
        {
            _warHelper = warHelper;
            _worldManager = worldManager;
            _treatyHelper = treatyHelper;
            _diplomacyHelper = diplomacyHelper;
            _playerData = playerData;
            _notificationsSystem = notificationsSystem;
            _genericNotificationBuilder = genericNotificationBuilder;
            _simulationEventBus = simulationEventBus;
        }
        
        public TradeSettlementTreaty Initialize(
            Tribe from, 
            Tribe to, 
            Settlement settlement,
            double regionScore)
        {
            if (to == _playerData.PlayerTribe)
            {
                Name = "Give " + settlement.Name;
                Description = $"They will give us control over their settlement of {settlement.Name}.";
            }
            else
            {
                Name = "Give " + settlement.Name;
                Description = $"We will give them over out settlement of {settlement.Name}.";
            }
            From = from;
            To = to;
            Settlement = settlement;
            RegionScore = regionScore;

            return this;
        }
        
        public void OnRatification()
        {
            _simulationEventBus.SendEvent(new SettlementOwnerChangedSimulationEvent
            {
                OldOwner = From,
                NewOwner = To,
                Settlement = Settlement
            });

            Settlement.ChangeOwner(To);

            if (To == _playerData.PlayerTribe)
            {
                From.AddDiplomaticMemory(
                    new DiplomaticMemory(
                        Settlement.Id + "transfer",
                        $"Took {Settlement.Name} in a treaty.",
                        _worldManager.Month,
                        1,
                        -0.04,
                        -1 * Settlement.Population.Count,
                        0));
            }
            if(From == _playerData.PlayerTribe)
            {
                To.AddDiplomaticMemory(
                    new DiplomaticMemory(
                        Settlement.Id + "transfer",
                        $"Gave {Settlement.Name} in a treaty.",
                        _worldManager.Month,
                        1,
                        -0.04,
                        Settlement.Population.Count,
                        0));
            }
        }

        public void StepSimulation(double elapsedMonths)
        {
            _treatyHelper.RemoveTreaty(this);
        }

        public void OnRemoval()
        {
        }

        public void LoadFrom(IWorldManager worldManager, SerializedObject treatyRoot)
        {
            From = treatyRoot.GetObjectReference(
                    "from",
                    worldManager.Tribes,
                    t => t.Id);
            To = treatyRoot.GetObjectReference(
                    "to",
                    worldManager.Tribes,
                    t => t.Id);
            Name = treatyRoot.GetString("name");
            Description = treatyRoot.GetString("description");
            Settlement = treatyRoot.GetObjectReference(
                "settlement_id",
                worldManager.Tribes.SelectMany(t => t.Settlements),
                s => s.Id);
            RegionScore = treatyRoot.GetDouble("region_score");
        }

        public void SerializeTo(SerializedObject treatyRoot)
        {
            treatyRoot.Set("name", Name);
            treatyRoot.Set("description", Description);
            treatyRoot.Set("from", From.Id);
            treatyRoot.Set("to", To.Id);
            treatyRoot.Set("settlement_id", Settlement.Id);
            treatyRoot.Set("region_score", RegionScore);
        }
        
        public IEnumerable<TreatyValueDetail> GetValueDetailFor(Tribe aiTribe)
        {
            var valueMod = From == aiTribe ? -1 : 1;

            var relativePower = _diplomacyHelper.GetRelativePower(From, To);

            var value = new List<TreatyValueDetail>();
            value.Add(new TreatyValueDetail("Base cost", 20 * valueMod));
            value.Add(new TreatyValueDetail("Settlement population", Settlement.Population.Count * valueMod * 2));
            value.Add(new TreatyValueDetail("Region features", (int)(RegionScore * valueMod)));
            value.Add(new TreatyValueDetail("Relative power", (int)((relativePower - 0.5) * -20 * valueMod)));
            if (To == _playerData.PlayerTribe)
            {
                var trustMod = _diplomacyHelper.GetTrustMod(From);
                value.Add(new TreatyValueDetail("Trust", (int)(value.Sum(v => v.Value) * trustMod)));
            }

            return value;
        }

        public bool IsCoercing(Tribe tribe)
        {
            return false;
        }

        public void ShowEditor(Action finishedCallback, IEnumerable<ITreaty> treatiesInNegotiation)
        {
        }

        public void ApplyChanges()
        {
        }

        public void RevertChanges()
        {
        }
    }
}
