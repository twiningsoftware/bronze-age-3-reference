﻿using Bronze.Contracts.Data.Diplomacy;
using System;
using System.Collections.Generic;

namespace Bronze.Common.Data.Diplomacy
{
    public class SimpleTreatyOption<TTreaty> : ITreatyOption where TTreaty : ITreaty
    {
        public string Name => Treaty.Name;
        public string Description => Treaty.Description;
        public bool HasParameters => false;

        public ITreaty Treaty { get; }
        
        public SimpleTreatyOption(
            ITreaty treaty)
        {
            Treaty = treaty;
        }

        public ITreaty BuildTreaty()
        {
            return Treaty;
        }
        
        public void ShowEditor(Action<ITreaty> callback, IEnumerable<ITreaty> treatiesInNegotiation)
        {
            throw new NotImplementedException();
        }
    }
}
