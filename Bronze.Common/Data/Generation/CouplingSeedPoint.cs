﻿using Bronze.Common.Data.Game.CellDevelopments;
using Bronze.Common.Data.World.CellDevelopments;
using Bronze.Contracts;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Generation;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Xml.Linq;

namespace Bronze.Common.Data.Generation
{
    public class CouplingSeedPoint : ISeedPoint
    {
        public string Id { get; private set; }
        public int Priority { get; private set; }
        public int X { get; private set; }
        public int Y { get; private set; }
        public int PositionVariance { get; private set; }
        public double InfluenceWeight { get; private set; }
        public double InfluenceRange { get; private set; }
        public Biome Biome { get; private set; }
        public ICellFeature CellFeature { get; private set; }
        public ICellDevelopmentType OutgoingDevelopment { get; private set; }
        public ISeedPoint[] SeedPoints { get; private set; }
        public SeedPointGroup[] SeedPointGroups { get; private set; }
        public string TargetPlane { get; private set; }
        public ISeedPoint TargetSeedPoint { get; private set; }

        public bool ClaimsRegions => false;

        private readonly IGamedataTracker _gamedataTracker;

        public CouplingSeedPoint(IGamedataTracker gamedataTracker)
        {
            _gamedataTracker = gamedataTracker;
        }
        
        public void LoadFrom(
            IXmlReaderUtil xmlReaderUtil,
            XElement element,
            RegionFeature[] regionFeatures,
            IEnumerable<ISeedPoint> childPoints,
            IEnumerable<SeedPointGroup> childGroups)
        {
            Id = xmlReaderUtil.AttributeValue(element, "id");
            Priority = xmlReaderUtil.AttributeAsInt(element, "priority");
            X = xmlReaderUtil.AttributeAsInt(element, "x");
            Y = xmlReaderUtil.AttributeAsInt(element, "y");
            PositionVariance = xmlReaderUtil.AttributeAsInt(element, "pos_variance");
            InfluenceWeight = xmlReaderUtil.AttributeAsDouble(element, "infl_weight");
            InfluenceRange = xmlReaderUtil.AttributeAsDouble(element, "infl_range");
            SeedPoints = childPoints.ToArray();
            SeedPointGroups = childGroups.ToArray();
            Biome = xmlReaderUtil.OptionalObjectReferenceLookup(
                element,
                "biome",
                _gamedataTracker.Biomes,
                b => b.Id);
            CellFeature = xmlReaderUtil.OptionalObjectReferenceLookup(
                element,
                "cell_feature",
                _gamedataTracker.CellFeatures,
                cf => cf.Id);
            TargetPlane = xmlReaderUtil.AttributeValue(element, "target_plane");
            OutgoingDevelopment = xmlReaderUtil.OptionalObjectReferenceLookup(
                element,
                "cell_development",
                _gamedataTracker.CellDevelopments,
                b => b.Id);

            var targetElement = xmlReaderUtil.ElementOrNull(element, "target_point");

            if (targetElement != null)
            {
                TargetSeedPoint = xmlReaderUtil.LoadSeedPoint(targetElement, regionFeatures);
            }
        }

        public void Generate(
            GenerationContext context,
            Contracts.Data.World.Plane plane,
            Vector2 position,
            List<Cell> cells,
            List<Region> regions)
        {
            if(cells.Any())
            {
                var cell = cells.First();

                if(Biome != null)
                {
                    cell.Biome = Biome;
                }

                cell.Feature = CellFeature;

                if (OutgoingDevelopment != null)
                {
                    OutgoingDevelopment.Place(null, cell, true);

                    if (cell.Development is CouplingDevelopment couplingDevelopment)
                    {
                        couplingDevelopment.DestinationPos = new CellPosition(
                            context.PlaneToId[TargetPlane],
                            cell.Position.X,
                            cell.Position.Y);
                    }
                }
            }

            if (TargetSeedPoint != null)
            {
                if (context.PlannedSeedPoints.ContainsKey(TargetPlane))
                {
                    var x = position.X + TargetSeedPoint.X + context.Random.Next(-TargetSeedPoint.PositionVariance / 2, TargetSeedPoint.PositionVariance / 2);
                    var y = position.Y + TargetSeedPoint.Y + context.Random.Next(-TargetSeedPoint.PositionVariance / 2, TargetSeedPoint.PositionVariance / 2);

                    x = Math.Max(0, Math.Min(x, context.Parameters.WorldType.Size));
                    y = Math.Max(0, Math.Min(y, context.Parameters.WorldType.Size));

                    context.PlannedSeedPoints[TargetPlane].Add(Tuple.Create(TargetSeedPoint, new Vector2(x, y)));

                    GenUtils.EvaluateSeedPointsGroups(
                        context,
                        TargetPlane,
                        context.Parameters.WorldType.Size,
                        (int)x,
                        (int)y,
                        TargetSeedPoint.SeedPoints,
                        TargetSeedPoint.SeedPointGroups);
                }
            }
        }
    }
}
