﻿using System;
using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;
using Bronze.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;

namespace Bronze.Common.Gameplay.UiElements
{
    public class ImageButton : UiElementWithTooltip
    {
        private static readonly int PADDING = 5;

        private bool _isHovered;
        private bool _isDisabled;
        private bool _isVisible;

        private string _imageKey;
        private Action _clickAction;
        private Func<bool> _visibleCheck;
        private Func<bool> _disableCheck;
        private bool _needsRelayout;

        public override bool NeedsRelayout => base.NeedsRelayout || _needsRelayout;


        public ImageButton(
            string imageKey,
            string tooltip,
            Action clickAction,
            Func<bool> visibleCheck,
            Func<bool> disableCheck)
        {
            _imageKey = imageKey;
            _clickAction = clickAction;
            _visibleCheck = visibleCheck;
            _disableCheck = disableCheck;
            _needsRelayout = true;

            if (tooltip != null)
            {
                Tooltip = new SlicedBackground(new Text(tooltip)
                {
                    Width = Width.Fixed(200)
                });
            }
        }
        
        public override void DoLayout(
            IDrawingService drawingService, 
            Rectangle parentBounds)
        {
            _needsRelayout = false;

            if(_isVisible)
            {
                var imageBounds = drawingService.GetImageSize(_imageKey);

                Bounds = new Rectangle(
                    parentBounds.X,
                    parentBounds.Y,
                    (int)imageBounds.X + PADDING * 2,
                    (int)imageBounds.Y + PADDING * 2);
            }
            else
            {
                Bounds = new Rectangle(parentBounds.X, parentBounds.Y, 0, 0);
            }

            base.DoLayout(drawingService, parentBounds);
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            base.Draw(drawingService, layer);

            var isVisible = _visibleCheck();
            _isDisabled = _disableCheck() && _isVisible;

            if (isVisible != _isVisible)
            {
                _isVisible = isVisible;
                _needsRelayout = true;
            }

            if (_isVisible)
            {
                if(_isDisabled)
                {
                    drawingService.DrawSlicedImage(UiImages.ButtonInactive, Bounds, layer, true);
                }
                else if (_isHovered)
                {
                    drawingService.DrawSlicedImage(UiImages.ButtonHover, Bounds, layer, true);
                }
                else
                {
                    drawingService.DrawSlicedImage(UiImages.ButtonStandard, Bounds, layer, true);
                }

                drawingService.DrawImage(_imageKey, Bounds.Center.ToVector2(), layer + LAYER_STEP, true);
            }
        }

        public override void Update(
            InputState inputState,
            IAudioService audioService,
            float elapsedSeconds)
        {
            base.Update(inputState, audioService, elapsedSeconds);
            
            _isHovered = Bounds.Contains(inputState.MousePosition) && !_isDisabled;

            if (inputState.MouseReleased && _isHovered)
            {
                audioService.PlaySound(UiSounds.Click);
                _clickAction();
            }
        }
    }
}
