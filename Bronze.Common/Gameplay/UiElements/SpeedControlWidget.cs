﻿using System.Collections.Generic;
using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;

namespace Bronze.Common.Gameplay.UiElements
{
    public class SpeedControlWidget : AbstractUiElement
    {
        private readonly IUserSettings _userSettings;
        private readonly StackGroup _stackGroup;

        public override bool NeedsRelayout => _stackGroup.NeedsRelayout;

        public SpeedControlWidget(IUserSettings userSettings)
        {
            _userSettings = userSettings;

            _stackGroup = new StackGroup
            {
                Orientation = Orientation.Horizontal,
                HorizontalContentAlignment = HorizontalAlignment.Left,
                VerticalContentAlignment = VerticalAlignment.Top,
                Children = new List<IUiElement>
                {
                    new IconButton(KnownImages.IconSpeedPause,
                        "",
                        () =>
                        {
                            _userSettings.PausedByPlayer = !_userSettings.PausedByPlayer;
                        },
                        highlightCheck: () => _userSettings.SimulationPaused,
                        tooltip: "Pause (space)"),
                    new Spacer(10, 0),
                    new IconButton(KnownImages.IconSpeedNormal,
                        "",
                        () =>
                        {
                            _userSettings.SimulationSpeed = 1;
                        },
                        highlightCheck: () => _userSettings.SimulationSpeed == 1,
                        tooltip: "Normal Speed"),
                    new Spacer(10, 0),
                    new IconButton(KnownImages.IconSpeedFast,
                        "",
                        () =>
                        {
                            _userSettings.SimulationSpeed = 2;
                        },
                        highlightCheck: () => _userSettings.SimulationSpeed == 2,
                        tooltip: "Fast Speed"),
                    new Spacer(10, 0),
                    new IconButton(KnownImages.IconSpeedSuperfast,
                        "",
                        () =>
                        {
                            _userSettings.SimulationSpeed = 4;
                        },
                        highlightCheck: () => _userSettings.SimulationSpeed == 4,
                        tooltip: "Fastest Speed")
                }
            };
        }

        public override void Update(InputState inputState, IAudioService audioService, float elapsedSeconds)
        {
            _stackGroup.Update(inputState, audioService, elapsedSeconds);

            if(inputState.JustPressed(KeyCode.Space) && inputState.FocusedElement == null)
            {
                _userSettings.PausedByPlayer = !_userSettings.PausedByPlayer;
            }
        }

        public override void DoLayout(IDrawingService drawingService, Rectangle parentBounds)
        {
            _stackGroup.DoLayout(drawingService, parentBounds);

            Bounds = _stackGroup.Bounds;
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            _stackGroup.Draw(drawingService, layer);
        }
    }
}
