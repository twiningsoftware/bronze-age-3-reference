﻿using System;
using System.Linq;
using System.Numerics;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;

namespace Bronze.Common.Gameplay.UiElements
{
    public class WorldMinimapElement : AbstractUiElement
    {
        private readonly IPlayerDataTracker _playerData;
        private readonly IDialogManager _dialogManager;
        private readonly IWorldManager _worldManager;

        private bool _needsRelayout;

        public Width Width { get; set; }
        public Height Height { get; set; }
        public Vector2 ViewCenter { get; set; }
        public override bool NeedsRelayout => _needsRelayout;
        public Func<bool> HideCondition { get; set; }

        private Vector2 _imageSize;

        public WorldMinimapElement(
            IPlayerDataTracker playerData,
            IDialogManager dialogManager,
            IWorldManager worldManager)
        {
            _playerData = playerData;
            _dialogManager = dialogManager;
            _worldManager = worldManager;
                 
            ViewCenter = Vector2.Zero;
            Width = Width.Relative(1);
            Height = Height.Relative(1);

            _needsRelayout = true;

            HideCondition = () => false;
        }

        public override void DoLayout(IDrawingService drawingService, Rectangle parentBounds)
        {
            _needsRelayout = false;

            if (!HideCondition())
            {
                Bounds = new Rectangle(
                    parentBounds.X,
                    parentBounds.Y,
                    Width.CalculateWidth(parentBounds),
                    Height.CalculateHeight(parentBounds));
            }
            else
            {
                Bounds = new Rectangle(
                    parentBounds.X, 
                    parentBounds.Y, 
                    0, 
                    0);
            }
        }

        public override void Update(InputState inputState, IAudioService audioService, float elapsedSeconds)
        {
            base.Update(inputState, audioService, elapsedSeconds);
            
            if(Bounds.Contains(inputState.MousePosition) 
                && _dialogManager.CurrentModal == null
                && inputState.MouseReleased)
            {
                var pixelsPerCell = Constants.CELL_SIZE_PIXELS;
                if(_playerData.ViewMode == ViewMode.Summary)
                {
                    pixelsPerCell = Constants.CELL_SIZE_SUMMARY_PIXELS;
                }
                
                var relativePos = new Vector2(
                    inputState.MousePosition.X - Bounds.X,
                    inputState.MousePosition.Y - Bounds.Y);

                var scalingFactor = _worldManager.WorldSize / (float)Bounds.Width;

                var cellX = relativePos.X * scalingFactor;
                var cellY = relativePos.Y * scalingFactor;

                cellX = Util.Clamp(cellX, 0, _worldManager.WorldSize);
                cellY = Util.Clamp(cellY, 0, _worldManager.WorldSize);

                _playerData.LookAt(new CellPosition(_playerData.CurrentPlane, (int)cellX, (int)cellY));
                audioService.PlaySound(UiSounds.Click);
            }
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            if(_playerData.ViewMode != ViewMode.Settlement
                && !HideCondition())
            {
                var imageSize = drawingService.GetImageSize(_playerData.CurrentPlane + "minimap");

                if(_imageSize != imageSize)
                {
                    _imageSize = imageSize;
                    _needsRelayout = true;
                }

                drawingService
                    .DrawImage(
                        _playerData.CurrentPlane + "minimap",
                        Bounds,
                        layer,
                        false);

                var pixelsPerCell = Constants.CELL_SIZE_PIXELS;
                if (_playerData.ViewMode == ViewMode.Summary)
                {
                    pixelsPerCell = Constants.CELL_SIZE_SUMMARY_PIXELS;
                }

                var viewCenter = (_playerData.ViewCenter / Constants.CELL_SIZE_PIXELS);

                var scalingFactor = _worldManager.WorldSize / (float)Bounds.Width;

                var pixel = viewCenter / scalingFactor;

                var viewportWidth = drawingService.DisplayWidth / pixelsPerCell / scalingFactor;
                var viewportHeight = drawingService.DisplayHeight / pixelsPerCell / scalingFactor;

                var box = Rectangle.AroundCenter(
                        new Point(
                            Bounds.X + (int)pixel.X,
                            Bounds.Y + (int)pixel.Y),
                        (int)viewportWidth,
                        (int)viewportHeight);

                var left = Math.Max(Bounds.Left, box.Left);
                var right = Math.Min(Bounds.Right, box.Right);
                var top = Math.Max(Bounds.Top, box.Top);
                var bottom = Math.Min(Bounds.Bottom, box.Bottom);

                drawingService.DrawSlicedImage(
                    UiImages.DragSelectBox,
                    new Rectangle(
                        left,
                        top,
                        right - left,
                        bottom - top),
                    layer + 1,
                    false);
            }
        }
    }
}
