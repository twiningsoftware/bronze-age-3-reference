﻿using System;
using System.Linq;
using System.Numerics;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;

namespace Bronze.Common.Gameplay.UiElements
{
    public class SettlementMinimapElement : AbstractUiElement
    {
        private readonly IPlayerDataTracker _playerData;
        private readonly IDialogManager _dialogManager;

        private bool _needsRelayout;

        public Width Width { get; set; }
        public Height Height { get; set; }
        public Vector2 ViewCenter { get; set; }
        public override bool NeedsRelayout => _needsRelayout;
        public Func<bool> HideCondition { get; set; }

        private Vector2 _imageSize;

        public SettlementMinimapElement(
            IPlayerDataTracker playerData,
            IDialogManager dialogManager)
        {
            _playerData = playerData;
            _dialogManager = dialogManager;
            ViewCenter = Vector2.Zero;
            Width = Width.Relative(1);
            Height = Height.Relative(1);

            _needsRelayout = true;

            HideCondition = () => false;
        }

        public override void DoLayout(IDrawingService drawingService, Rectangle parentBounds)
        {
            _needsRelayout = false;

            if (!HideCondition())
            {
                Bounds = new Rectangle(
                    parentBounds.X,
                    parentBounds.Y,
                    Width.CalculateWidth(parentBounds),
                    Height.CalculateHeight(parentBounds));
            }
            else
            {
                Bounds = new Rectangle(
                    parentBounds.X, 
                    parentBounds.Y, 
                    0, 
                    0);
            }
        }

        public override void Update(InputState inputState, IAudioService audioService, float elapsedSeconds)
        {
            base.Update(inputState, audioService, elapsedSeconds);

            if(Bounds.Contains(inputState.MousePosition) 
                && _dialogManager.CurrentModal == null
                && inputState.MouseReleased)
            {
                var relativePos = new Vector2(
                    inputState.MousePosition.X - Bounds.X,
                    inputState.MousePosition.Y - Bounds.Y);

                var cellPositions = _playerData.ActiveSettlement.Districts
                    .Select(x => x.Cell.Position)
                    .ToArray();

                var minCellX = cellPositions.Min(c => c.X);
                var maxCellX = cellPositions.Max(c => c.X);
                var minCellY = cellPositions.Min(c => c.Y);
                var maxCellY = cellPositions.Max(c => c.Y);

                var width = (maxCellX - minCellX + 1) * TilePosition.TILES_PER_CELL;
                var height = (maxCellY - minCellY + 1) * TilePosition.TILES_PER_CELL;
                var dimension = Math.Max(width, height);

                var xPaddingPixels = dimension - width;
                var yPaddingPixels = dimension - height;

                var scalingFactor = dimension / (float)Bounds.Width;
                
                var tileX = (relativePos.X * scalingFactor) - xPaddingPixels / 2;
                var tileY = (relativePos.Y * scalingFactor) - yPaddingPixels / 2;

                tileX = Util.Clamp(tileX, 0, width);
                tileY = Util.Clamp(tileY, 0, height);

                var cellX = (int)tileX / TilePosition.TILES_PER_CELL + minCellX;
                var cellY = (int)tileY / TilePosition.TILES_PER_CELL + minCellY;

                var district = _playerData.ActiveSettlement.Districts
                    .Where(d => d.Cell.Position.X == cellX && d.Cell.Position.Y == cellY)
                    .FirstOrDefault();

                if(district != null)
                {
                    var tileInCellX = (int)tileX % TilePosition.TILES_PER_CELL;
                    var tileInCellY = (int)tileY % TilePosition.TILES_PER_CELL;

                    var tile = district.Tiles[tileInCellX, tileInCellY];

                    _playerData.LookAt(tile.Position);
                    audioService.PlaySound(UiSounds.Click);
                }
            }
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            if(_playerData.ActiveSettlement != null 
                && _playerData.ViewMode == ViewMode.Settlement
                && !HideCondition())
            {
                var imageSize = drawingService.GetImageSize(_playerData.ActiveSettlement.Id + "minimap");

                if(_imageSize != imageSize)
                {
                    _imageSize = imageSize;
                    _needsRelayout = true;
                }

                drawingService
                    .DrawImage(
                        _playerData.ActiveSettlement.Id + "minimap",
                        Bounds,
                        layer,
                        false);

                var cellPositions = _playerData.ActiveSettlement.Districts
                    .Select(x => x.Cell.Position)
                    .ToArray();

                var minCellX = cellPositions.Min(c => c.X);
                var maxCellX = cellPositions.Max(c => c.X);
                var minCellY = cellPositions.Min(c => c.Y);
                var maxCellY = cellPositions.Max(c => c.Y);

                var width = (maxCellX - minCellX + 1) * TilePosition.TILES_PER_CELL;
                var height = (maxCellY - minCellY + 1) * TilePosition.TILES_PER_CELL;
                var dimension = Math.Max(width, height);

                var ulTile = new Vector2(
                    minCellX * TilePosition.TILES_PER_CELL,
                    minCellY * TilePosition.TILES_PER_CELL);

                var viewCenter = (_playerData.ViewCenter / Constants.TILE_SIZE_PIXELS)
                    - ulTile;

                var scalingFactor = dimension / (float)Bounds.Width;

                var xPaddingPixels = dimension - width;
                var yPaddingPixels = dimension - height;

                var pixel = (viewCenter + new Vector2(xPaddingPixels / 2, yPaddingPixels / 2)) / scalingFactor;

                var viewportWidth = drawingService.DisplayWidth / Constants.TILE_SIZE_PIXELS / scalingFactor;
                var viewportHeight = drawingService.DisplayHeight / Constants.TILE_SIZE_PIXELS / scalingFactor;

                var box = Rectangle.AroundCenter(
                        new Point(
                            Bounds.X + (int)pixel.X,
                            Bounds.Y + (int)pixel.Y),
                        (int)viewportWidth,
                        (int)viewportHeight);

                var left = Math.Max(Bounds.Left, box.Left);
                var right = Math.Min(Bounds.Right, box.Right);
                var top = Math.Max(Bounds.Top, box.Top);
                var bottom = Math.Min(Bounds.Bottom, box.Bottom);
                
                drawingService.DrawSlicedImage(
                    UiImages.DragSelectBox,
                    new Rectangle(
                        left,
                        top,
                        right - left,
                        bottom - top),
                    layer + 1,
                    false);
            }
        }
    }
}
