﻿using System;
using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;
using Bronze.UI.Elements;

namespace Bronze.Common.Gameplay.UiElements
{
    public class AmountAndRateLabel : AbstractUiElement
    {
        private const int PADDING = 5;

        private Func<bool> _displayCondition;
        private Func<double> _getAmount;
        private Func<double> _getRate;

        private string _amountContent;
        private string _rateContent;
        private BronzeColor _rateColor;

        private bool _needsRelayout;
        private bool _oldVisible;

        public override bool NeedsRelayout => _needsRelayout;

        public AmountAndRateLabel(Func<double> getAmount, Func<double> getRate, Func<bool> displayCondition = null)
        {
            _getAmount = getAmount;
            _getRate = getRate;
            _displayCondition = displayCondition ?? (() => true);
            _needsRelayout = true;
        }

        public override void DoLayout(
            IDrawingService drawingService,
            Rectangle parentBounds)
        {
            _needsRelayout = false;
            

            var width = 0f;
            var height = 0f;

            if(_amountContent != null)
            {
                var size = drawingService.MeasureText(_amountContent);
                width += size.X;
                height = size.Y;
            }

            if (_rateContent != null)
            {
                if (_amountContent != null)
                {
                    width += PADDING;
                }

                var size = drawingService.MeasureText(_rateContent);
                width += size.X;
                height = Math.Max(height, size.Y);
            }

            Bounds = new Rectangle(
                parentBounds.X,
                parentBounds.Y,
                (int)width,
                (int)height);
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            var visible = _displayCondition();

            if(visible != _oldVisible)
            {
                _oldVisible = visible;
                _needsRelayout = true;
            }

            if (visible)
            {
                if (_getAmount != null)
                {
                    var amount = _getAmount();
                    var amountContent = amount.ToString("0.#");

                    if (amountContent != _amountContent)
                    {
                        _needsRelayout = true;
                        _amountContent = amountContent;
                    }
                }
                else
                {
                    _amountContent = null;
                }

                if (_getRate != null)
                {
                    var rate = _getRate();
                    var rateContent = rate.ToString("0.#");
                    _rateColor = BronzeColor.White;
                    if (rate < 0)
                    {
                        _rateColor = BronzeColor.Red;
                    }
                    if (rate > 0)
                    {
                        rateContent = "+" + _rateContent;
                        _rateColor = BronzeColor.Green;
                    }

                    if (rateContent != _rateContent)
                    {
                        _needsRelayout = true;
                        _rateContent = rateContent;
                    }
                }
                else
                {
                    _rateContent = null;
                }

                var padding = 0f;

                if (_amountContent != null)
                {
                    drawingService
                        .DrawText(
                        _amountContent,
                        new Point(Bounds.X, Bounds.Y),
                        layer,
                        BronzeColor.White,
                        1);

                    padding = drawingService.MeasureText(_amountContent).X + PADDING;
                }

                if (_rateContent != null)
                {
                    drawingService
                        .DrawText(
                        _rateContent,
                        new Point(Bounds.X + (int)padding, Bounds.Y),
                        layer,
                        _rateColor,
                        1);
                }
            }
        }
    }
}
