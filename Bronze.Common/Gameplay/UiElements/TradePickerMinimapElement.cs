﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;

namespace Bronze.Common.Gameplay.UiElements
{
    public class TradePickerMinimapElement : AbstractUiElement
    {
        private readonly IWorldManager _worldManager;

        private bool _needsRelayout;

        public Width Width { get; set; }
        public Height Height { get; set; }
        public Vector2 ViewCenter { get; set; }
        public override bool NeedsRelayout => _needsRelayout;
        public Func<bool> HideCondition { get; set; }
        public int CurrentPlane { get; set; }
        private List<Cell> _pathToShow;

        private Vector2 _imageSize;

        private Cell _focusCell;

        public TradePickerMinimapElement(
            IPlayerDataTracker playerData,
            IWorldManager worldManager)
        {
            _worldManager = worldManager;
                 
            ViewCenter = Vector2.Zero;
            Width = Width.Relative(1);
            Height = Height.Relative(1);
            CurrentPlane = playerData.CurrentPlane;

            _needsRelayout = true;

            HideCondition = () => false;
        }

        public override void DoLayout(IDrawingService drawingService, Rectangle parentBounds)
        {
            _needsRelayout = false;

            if (!HideCondition())
            {
                Bounds = new Rectangle(
                    parentBounds.X,
                    parentBounds.Y,
                    Width.CalculateWidth(parentBounds),
                    Height.CalculateHeight(parentBounds));
            }
            else
            {
                Bounds = new Rectangle(
                    parentBounds.X, 
                    parentBounds.Y, 
                    0, 
                    0);
            }
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            if(!HideCondition())
            {
                var imageSize = drawingService.GetImageSize(CurrentPlane + "minimap");

                if(_imageSize != imageSize)
                {
                    _imageSize = imageSize;
                    _needsRelayout = true;
                }

                drawingService
                    .DrawImage(
                        CurrentPlane + "minimap",
                        Bounds,
                        layer,
                        false);
                
                if (_focusCell != null)
                {
                    DrawIconOnMap(
                        drawingService,
                        layer + 2,
                        _focusCell,
                        "ui_pathdot_small");
                }

                if(_pathToShow != null)
                {
                    foreach(var cell in _pathToShow)
                    {
                        if(cell.Position.Plane == CurrentPlane)
                        {
                            DrawIconOnMap(
                                drawingService,
                                layer + 1,
                                cell,
                                "ui_pathdot_tiny");
                        }
                    }
                }
            }
        }

        private void DrawIconOnMap(
            IDrawingService drawingService, 
            float layer, 
            Cell cell, 
            string iconKey)
        {
            var viewCenter = cell.Position.ToVector() + new Vector2(0.5f, 0.5f);

            var scalingFactor = _worldManager.WorldSize / (float)Bounds.Width;

            var pixel = viewCenter / scalingFactor;

            drawingService.DrawImage(
                iconKey,
                new Vector2(
                    Bounds.X + pixel.X,
                    Bounds.Y + pixel.Y),
                layer,
                false);
        }

        public void Highlight(IEconomicActor economicActor)
        {
            if(economicActor == null)
            {
                _focusCell = null;
            }
            else if(economicActor is IStructure structure)
            {
                _focusCell = structure.Settlement.Districts.First().Cell;
                CurrentPlane = _focusCell.Position.Plane;
            }
            else if(economicActor is ICellDevelopment development)
            {
                _focusCell = development.Cell;
                CurrentPlane = _focusCell.Position.Plane;
            }
        }

        public void ShowPath(List<Cell> path)
        {
            _pathToShow = path;
        }

        public void ClearPath()
        {
            _pathToShow = null;
        }
    }
}
