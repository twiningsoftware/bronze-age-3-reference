﻿using Bronze.Contracts;
using Bronze.Contracts.Data;
using System;
using System.Numerics;

namespace Bronze.Common.Gameplay
{
    public class Viewport
    {
        private Rectangle _bounds;
        private Rectangle _outerBounds;
        public float Scale;

        private float _partialMoveX;
        private float _partialMoveY;

        public Viewport(Rectangle bounds, float scale)
        {
            UpdateBounds(
                bounds.Center,
                bounds.Width,
                bounds.Height);
            _bounds = bounds;
            _outerBounds = Rectangle
                .AroundCenter(
                    bounds.Center,
                    bounds.Width + Constants.LARGE_HEX_WIDTH_PIXELS * 2,
                    bounds.Height + Constants.LARGE_HEX_HEIGHT_PIXELS * 2);
            Scale = scale;
        }
        
        public void UpdateSize(int width, int height)
        {
            UpdateBounds(_bounds.Center, width, height);
        }

        private void UpdateBounds(Point center, int width, int height)
        {
            _bounds = Rectangle
                .AroundCenter(
                    center,
                    width,
                    height);

            _outerBounds = Rectangle
                .AroundCenter(
                    center,
                    width + (int)(2 * Scale),
                    height + (int)(2 * Scale));
        }

        public Point TransformToScreen(Vector2 displayPosition)
        {
            var rawX = displayPosition.X;
            var rawY = displayPosition.Y;

            var screenX = (rawX - _bounds.Center.X) * Scale;
            var screenY = (rawY - _bounds.Center.Y) * Scale;

            return new Point((int)screenX, (int)screenY);
        }
        
        public void Move(float viewDx, float viewDy)
        {
            _partialMoveX += viewDx;
            _partialMoveY += viewDy;

            var moveX = 0;
            var moveY = 0;

            if (Math.Abs(_partialMoveX) > 1)
            {
                moveX = (int)_partialMoveX;
                _partialMoveX -= moveX;
            }
            if (Math.Abs(_partialMoveY) > 1)
            {
                moveY = (int)_partialMoveY;
                _partialMoveY -= moveY;
            }

            if(moveX != 0 || moveY != 0)
            {
                UpdateBounds(
                    _bounds.Center.Relative(moveX, moveY), 
                    _bounds.Width, 
                    _bounds.Height);
            }
        }

        //public bool Contains(HexPosition hexPosition)
        //{
        //    return _outerBounds.Contains(TransformToScreen(hexPosition.DisplayPosition));
        //}

        public bool Contains(Point point)
        {
            return _outerBounds.Contains(point);
        }
    }
}
