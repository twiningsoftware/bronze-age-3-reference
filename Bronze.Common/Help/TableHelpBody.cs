﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bronze.Contracts.Help;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;

namespace Bronze.Common.Help
{
    public class TableHelpBody : IHelpBody
    {
        private int _columns;
        private IHelpBody[] _items;

        public TableHelpBody(int columns, IEnumerable<IHelpBody> items)
        {
            _items = items.ToArray();
            _columns = Math.Min(columns, _items.Length);
        }

        public IUiElement BuildUiElement()
        {
            var columns = Enumerable.Range(0, _columns)
                .Select(i => new StackGroup
                {
                    Orientation = Orientation.Vertical
                })
                .ToArray();
            
            for (var i = 0; i < _items.Length; i++)
            {
                columns[i % _columns].Children.Add(_items[i].BuildUiElement());
            }

            return new StackGroup
            {
                Width = Width.Relative(1),
                Orientation = Orientation.Horizontal,
                Children = columns
                    .SelectMany(c => new IUiElement[] { c, new Spacer(20, 0) })
                    .ToList()
            };
        }
    }
}
