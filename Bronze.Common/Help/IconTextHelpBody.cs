﻿using Bronze.Contracts.Data;
using Bronze.Contracts.Help;
using Bronze.Contracts.UI;
using Bronze.UI.Elements;

namespace Bronze.Common.Help
{
    public class IconTextHelpBody : IHelpBody
    {
        private string _imageKey;
        private string _text;
        private string _tooltip;
        private BronzeColor _textColor;

        public IconTextHelpBody(
            string imageKey, 
            string text, 
            string tooltip, 
            BronzeColor textColor = BronzeColor.White)
        {
            _imageKey = imageKey;
            _text = text;
            _tooltip = tooltip;
            _textColor = textColor;
        }

        public IUiElement BuildUiElement()
        {
            return new IconText(_imageKey, _text, _textColor, _tooltip);
        }
    }
}
