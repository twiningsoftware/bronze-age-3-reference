﻿using Bronze.Contracts.Help;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;

namespace Bronze.Common.Help
{
    public class TextHelpBody : IHelpBody
    {
        private string _text;

        public TextHelpBody(string text)
        {
            _text = text;
        }

        public IUiElement BuildUiElement()
        {
            return new Text(_text)
            {
                Width = Width.Fixed(400)
            };
        }
    }
}
