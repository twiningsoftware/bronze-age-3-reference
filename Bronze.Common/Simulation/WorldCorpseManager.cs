﻿using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.Simulation.Events;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace Bronze.Common.Simulation
{
    public class WorldCorpseManager : ISubSimulator, IWorldStateService
    {
        private readonly IWorldEffectManager _worldEffectManager;
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IWorldManager _worldManager;

        private readonly Random _random;
        private readonly List<WorldCorpse> _corpses;

        public WorldCorpseManager(
            ISimulationEventBus simulationEventBus,
            IWorldEffectManager worldEffectManager,
            IGamedataTracker gamedataTracker,
            IWorldManager worldManager)
        {
            _random = new Random();
            _worldEffectManager = worldEffectManager;
            _worldManager = worldManager;
            _gamedataTracker = gamedataTracker;
            simulationEventBus.OnSimulationEvent += OnSimulationEvent;

            _corpses = new List<WorldCorpse>();
        }

        private void OnSimulationEvent(ISimulationEvent evt)
        {
            if (evt is UnitDiedSimulationEvent deathEvent)
            {
                if(deathEvent.Army.TransportType != null)
                {
                    if (deathEvent.Unit.LastDrawPosition != null)
                    {
                        _worldEffectManager.AddDeathEffect(deathEvent.Army, deathEvent.Unit, deathEvent.LastDrawPosition.Value);
                    }
                }
                else
                {
                    Vector2 corpsePos;

                    if(deathEvent.Unit.LastDrawPosition != null)
                    {
                        corpsePos = deathEvent.Unit.LastDrawPosition.Value / Constants.CELL_SIZE_PIXELS;
                    }
                    else
                    {
                        corpsePos = deathEvent.Army.Position + new Vector2((float)_random.NextDouble() - 0.5f, (float)_random.NextDouble() - 0.5f);
                    }
                        
                    var newCorpse = new WorldCorpse(
                        deathEvent.Army.Cell,
                        corpsePos,
                        deathEvent.Army.Facing,
                        deathEvent.Army.Owner.PrimaryColor,
                        deathEvent.Unit.UnitType,
                        deathEvent.Unit.Equipment)
                    {
                        DecayTime = (float)_random.NextDouble() * -0.1f
                    };

                    deathEvent.Army.Cell.WorldActors.Add(newCorpse);
                    deathEvent.Army.Cell.Region.WorldActors.Add(newCorpse);
                    _corpses.Add(newCorpse);
                }
            }

            if (evt is PopKilledOnWorldSimulationEvent popKilledEvent)
            {
                var corpsePos = popKilledEvent.Cell.Position.ToVector()
                    + new Vector2((float)_random.NextDouble() - 0.5f, (float)_random.NextDouble() - 0.5f);

                var newCorpse = new WorldCorpse(
                    popKilledEvent.Cell,
                    corpsePos,
                    (Facing)_random.Next(0, 8),
                    popKilledEvent.Cell.Owner?.PrimaryColor ?? BronzeColor.None,
                    popKilledEvent.Pop.Race,
                    popKilledEvent.Pop.Caste)
                {
                    DecayTime = (float)_random.NextDouble() * -0.1f
                };

                popKilledEvent.Cell.WorldActors.Add(newCorpse);
                popKilledEvent.Cell.Region.WorldActors.Add(newCorpse);
                _corpses.Add(newCorpse);
            }
        }

        public void Clear()
        {
            _corpses.Clear();
        }

        public void Initialize(WorldState worldState)
        {
        }

        public void LoadCustomData(SerializedObject worldRoot)
        {
            var serviceRoot = worldRoot.GetOptionalChild("world_corpse_manager");

            if(serviceRoot != null)
            {
                foreach(var corpseRoot in serviceRoot.GetChildren("corpse"))
                {
                    var newCorpse = WorldCorpse.DeserializeFrom(
                        _gamedataTracker,
                        _worldManager,
                        corpseRoot);

                    newCorpse.Cell.WorldActors.Add(newCorpse);
                    newCorpse.Cell.Region.WorldActors.Add(newCorpse);
                    _corpses.Add(newCorpse);
                }
            }
        }

        public void SaveCustomData(SerializedObject worldRoot)
        {
            var serviceRoot = worldRoot.CreateChild("world_corpse_manager");

            foreach(var corpse in _corpses)
            {
                corpse.SerializeTo(serviceRoot.CreateChild("corpse"));
            }
            
        }

        public void StepSimulation(double elapsedMonths)
        {
            for(var i = 0; i < _corpses.Count; i++)
            {
                _corpses[i].DecayTime += elapsedMonths / Constants.MONTHS_PER_SECOND;

                if(_corpses[i].IsExpired || _corpses[i].IsRemoved)
                {
                    _corpses[i].Cell.WorldActors.Remove(_corpses[i]);
                    _corpses[i].Cell.Region.WorldActors.Remove(_corpses[i]);
                    _corpses.RemoveAt(i);
                    i -= 1;
                }
            }
        }
    }
}
