﻿using Bronze.Common.Data.World.Units;
using Bronze.Common.UI.Modals;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System.Linq;

namespace Bronze.Common.Simulation
{
    public class HumitteTutorialWatcher : IWorldStateService, ISubSimulator
    {
        public const int STAGE_OVERVIEW = 0;
        public const int STAGE_SETTLEMENT = 1;
        public const int STAGE_ROAD = 2;
        public const int STAGE_BRICKWORKS = 3;
        public const int STAGE_BREAD = 4;
        public const int STAGE_NOBLES = 5;
        public const int STAGE_WAREHOUSES = 6;
        public const int STAGE_GOVERNOR = 7;
        public const int STAGE_UNITS = 8;
        public const int STAGE_SECOND_SETTLEMENT = 9;
        public const int STAGE_TRADE = 10;

        private readonly IUserSettings _userSettings;
        private readonly IPlayerDataTracker _playerData;
        private readonly IDialogManager _dialogManager;
        private readonly ITradeManager _tradeManager;

        private int _tutorialStage;
        private bool _messageShown;

        private bool _tutorialWar;
        private bool _tutorialCombat;
        private bool _tutorialSiege;

        public HumitteTutorialWatcher(
            IUserSettings userSettings,
            IPlayerDataTracker playerDataTracker,
            IDialogManager dialogManager,
            ITradeManager tradeManager)
        {
            _userSettings = userSettings;
            _playerData = playerDataTracker;
            _dialogManager = dialogManager;
            _tradeManager = tradeManager;
        }
        
        public void Clear()
        {
            _tutorialStage = STAGE_OVERVIEW;
            _messageShown = false;
            _tutorialWar = false;
            _tutorialCombat = false;
            _tutorialSiege = false;
        }

        public void Initialize(WorldState worldState)
        {
        }

        public void LoadCustomData(SerializedObject worldRoot)
        {
            var serviceRoot = worldRoot.GetOptionalChild("humitte_tutorial_watcher");
            
            if(serviceRoot != null)
            {
                _tutorialStage = serviceRoot.GetInt("stage");

                _tutorialWar = serviceRoot.GetBool("tutorial_war");
                _tutorialCombat = serviceRoot.GetBool("tutorial_combat");
                _tutorialSiege = serviceRoot.GetBool("tutorial_siege");
            }
        }

        public void SaveCustomData(SerializedObject worldRoot)
        {
            // We don't save message shown so it will show up every time they load as a reminder
            var serviceRoot = worldRoot.CreateChild("humitte_tutorial_watcher");

            serviceRoot.Set("stage", _tutorialStage);
            serviceRoot.Set("tutorial_war", _tutorialWar);
            serviceRoot.Set("tutorial_combat", _tutorialCombat);
            serviceRoot.Set("tutorial_siege", _tutorialSiege);
        }

        public void StepSimulation(double elapsedMonths)
        {
            if(_userSettings.TutorialEnabled && elapsedMonths > 0 && _playerData.PlayerTribe?.Race?.Id == "h_r_humitte")
            {
                switch (_tutorialStage)
                {
                    case STAGE_OVERVIEW:
                        DoOverviewCheck();
                        break;
                    case STAGE_SETTLEMENT:
                        DoSettlementCheck();
                        break;
                    case STAGE_ROAD:
                        DoPathCheck();
                        break;
                    case STAGE_BRICKWORKS:
                        DoBrickworksCheck();
                        break;
                    case STAGE_BREAD:
                        DoBreadCheck();
                        break;
                    case STAGE_NOBLES:
                        DoNoblesCheck();
                        break;
                    case STAGE_WAREHOUSES:
                        DoWarehouseCheck();
                        break;
                    case STAGE_GOVERNOR:
                        DoGovernorCheck();
                        break;
                    case STAGE_UNITS:
                        DoUnitsCheck();
                        break;
                    case STAGE_SECOND_SETTLEMENT:
                        DoSecondSettlementCheck();
                        break;
                    case STAGE_TRADE:
                        DoTradeCheck();
                        break;
                }

                if(_playerData.PlayerTribe != null)
                {
                    if(!_tutorialWar && _playerData.PlayerTribe.CurrentWars.Any())
                    {
                        _tutorialWar = true;

                        _dialogManager.PushModal<GenericNotificationModal, GenericNotificationModalContext>(
                            new GenericNotificationModalContext
                            {
                                Title = "War",
                                AdvisorIcon = "ui_notification_tutorial",
                                Body = new string[]
                                {
                                    "You are now at war. Weither you started it or not, try not to lose the war.",
                                    "Each war has a score, influenced by raiding, battles, and sieges. This score determines how difficult it will be to negotiate peace.",
                                    "You can try to negotiate at any time, but if the war isn't going well the enemy may not accept it.",
                                    "Send your armies into enemy territory, burn their farms and attack their cities!"
                                }
                            });
                    }

                    if (!_tutorialCombat && _playerData.PlayerTribe.Armies.Any(a=>a.SkirmishingWith.Any()))
                    {
                        _tutorialWar = true;

                        _dialogManager.PushModal<GenericNotificationModal, GenericNotificationModalContext>(
                            new GenericNotificationModalContext
                            {
                                Title = "Combat",
                                AdvisorIcon = "ui_notification_tutorial",
                                Body = new string[]
                                {
                                    "One of your armies is engaged in battle.",
                                    "Armies start out skirmishing at long range, but they will slowly move towards eachother. When they meet, they will engage in a pitched battle.",
                                    "You can change this behavior with the army stance buttons along the top of the army panel."
                                }
                            });
                    }

                    if (!_tutorialCombat && _playerData.PlayerTribe.Armies.Any(a => a.SkirmishingWith.Any()))
                    {
                        _tutorialCombat = true;

                        _dialogManager.PushModal<GenericNotificationModal, GenericNotificationModalContext>(
                            new GenericNotificationModalContext
                            {
                                Title = "Combat",
                                AdvisorIcon = "ui_notification_tutorial",
                                Body = new string[]
                                {
                                    "One of your armies is engaged in battle.",
                                    "Armies start out skirmishing at long range, but they will slowly move towards eachother. When they meet, they will engage in a pitched battle.",
                                    "You can change this behavior with the army stance buttons along the top of the army panel."
                                }
                            });
                    }

                    if (!_tutorialSiege && _playerData.PlayerTribe.Armies.Any(a => a.CurrentOrder is ArmyInteractOrder interactOrder && interactOrder.SiegedSettlement != null))
                    {
                        _tutorialSiege = true;

                        _dialogManager.PushModal<GenericNotificationModal, GenericNotificationModalContext>(
                            new GenericNotificationModalContext
                            {
                                Title = "Siege",
                                AdvisorIcon = "ui_notification_tutorial",
                                Body = new string[]
                                {
                                    "One of your armies is sieging an enemy town. This cuts off their access to outlying farms, and the town will slowly begin to starve.",
                                    "The settlement will eventually surrender, but you assault the settlement instead and capture it now.",
                                    "Do do so, open the sieging notification along the top of the screen."
                                }
                            });
                    }
                }
            }
        }

        private void DoOverviewCheck()
        {
            if (!_messageShown)
            {
                _messageShown = true;
                _dialogManager.PushModal<GenericNotificationModal, GenericNotificationModalContext>(
                    new GenericNotificationModalContext
                    {
                        Title = "Welcome to Bronze Age",
                        AdvisorIcon = "ui_notification_tutorial",
                        Body = new string[]
                        {
                            "Welcome to Bronze Age, a game of city building and strategy set in an ancient world. I am Nanash, and I will be your guide.",
                            "You a tribe of Humittes, they are small now but I sense they are destined for great things.",
                            "To feed those ambitions you should build a farm, look in the construction tab of the panel in the bottom right."
                        }
                    });
            }

            if (_playerData.PlayerTribe != null 
                && _playerData.PlayerTribe.Settlements.Any()
                && _playerData.PlayerTribe.Settlements.First().EconomicActors.Where(ea=>ea.Name == "Farms" && !ea.UnderConstruction).Count() > 2)
            {
                _messageShown = false;
                _tutorialStage += 1;
            }
        }
        
        private void DoSettlementCheck()
        {
            if (!_messageShown)
            {
                _messageShown = true;
                _dialogManager.PushModal<GenericNotificationModal, GenericNotificationModalContext>(
                    new GenericNotificationModalContext
                    {
                        Title = "City View",
                        AdvisorIcon = "ui_notification_tutorial",
                        Body = new string[]
                        {
                            "Excellent, farms are essential to feeding your people, make sure to keep adding more farms as your population grows.",
                            "Speaking of which, let's look at their homes. You can reach the city by selecting the cluster of buildings and clicking 'View Settlement' on the lower left panel.",
                        }
                    });
            }

            if (_playerData.PlayerTribe != null
                && _playerData.ViewMode == ViewMode.Settlement)
            {
                _messageShown = false;
                _tutorialStage += 1;
            }
        }

        private void DoPathCheck()
        {
            if (!_messageShown)
            {
                _messageShown = true;
                _dialogManager.PushModal<GenericNotificationModal, GenericNotificationModalContext>(
                    new GenericNotificationModalContext
                    {
                        Title = "Paths",
                        AdvisorIcon = "ui_notification_tutorial",
                        Body = new string[]
                        {
                            "This is a detailed view of the city center.",
                            "You may see some people moving around carrying things, most structures can only send items along roads.",
                            "Just like you can place farms on the region view, you can place structures here on the city view.",
                            "Place some additional paths."
                        }
                    });
            }

            if (_playerData.PlayerTribe != null
                && _playerData.PlayerTribe.Settlements.Any()
                && _playerData.PlayerTribe.Settlements.First().EconomicActors.Where(ea => ea.Name == "Path" && ea.UnderConstruction).Count() > 0)
            {
                _messageShown = false;
                _tutorialStage += 1;
            }
        }

        private void DoBrickworksCheck()
        {
            if (!_messageShown)
            {
                _messageShown = true;
                _dialogManager.PushModal<GenericNotificationModal, GenericNotificationModalContext>(
                    new GenericNotificationModalContext
                    {
                        Title = "Brickworks",
                        AdvisorIcon = "ui_notification_tutorial",
                        Body = new string[]
                        {
                            "Paths are far from the only thing you can build in a city. Most of those structures are built from mudbrick.",
                            "Build a brickworks now to get started on more advanced construction projects."
                        }
                    });
            }

            if (_playerData.PlayerTribe != null
                && _playerData.PlayerTribe.Settlements.Any()
                && _playerData.PlayerTribe.Settlements.First().EconomicActors.Where(ea => ea.Name == "Brickworks" && !ea.UnderConstruction).Count() > 0)
            {
                _messageShown = false;
                _tutorialStage += 1;
            }
        }

        private void DoBreadCheck()
        {
            if (!_messageShown)
            {
                _messageShown = true;
                _dialogManager.PushModal<GenericNotificationModal, GenericNotificationModalContext>(
                    new GenericNotificationModalContext
                    {
                        Title = "Population Needs",
                        AdvisorIcon = "ui_notification_tutorial",
                        Body = new string[]
                        {
                            "Your people have needs, needs that must be met to keep them happy.",
                            "You can see these needs by selecting their houses and clicking 'Details' in the lower left panel. When your people have enough of their needs met they will upgrade their house.",
                            "Upgraded houses will hold more residents, and produce more in taxes.",
                            "Build a Mill and a Bakery now to turn wheat into bread."
                        }
                    });
            }

            if (_playerData.PlayerTribe != null
                && _playerData.PlayerTribe.Settlements.Any()
                && _playerData.PlayerTribe.Settlements.First().EconomicActors.Where(ea => ea.Name == "Mill" && !ea.UnderConstruction).Count() > 0
                && _playerData.PlayerTribe.Settlements.First().EconomicActors.Where(ea => ea.Name == "Bakery" && !ea.UnderConstruction).Count() > 0)
            {
                _messageShown = false;
                _tutorialStage += 1;
            }
        }

        private void DoNoblesCheck()
        {
            if (!_messageShown)
            {
                _messageShown = true;
                _dialogManager.PushModal<GenericNotificationModal, GenericNotificationModalContext>(
                    new GenericNotificationModalContext
                    {
                        Title = "Nobles",
                        AdvisorIcon = "ui_notification_tutorial",
                        Body = new string[]
                        {
                            "Humitte society has more than just Citizens, there is also an upper class of Nobles that rule over and organize them.",
                            "These nobles have different needs than citizens, and their own specialized structures. You can find them on the Noble tab of the contstruction list.",
                            "Build a Manor House to start adding Nobles to the city."
                        }
                    });
            }

            if (_playerData.PlayerTribe != null
                && _playerData.PlayerTribe.Settlements.Any()
                && _playerData.PlayerTribe.Settlements.First().EconomicActors.Where(ea => ea.Name == "Manor House" && !ea.UnderConstruction).Count() > 0)
            {
                _messageShown = false;
                _tutorialStage += 1;
            }
        }

        private void DoWarehouseCheck()
        {
            if (!_messageShown)
            {
                _messageShown = true;
                _dialogManager.PushModal<GenericNotificationModal, GenericNotificationModalContext>(
                    new GenericNotificationModalContext
                    {
                        Title = "Warehouses",
                        AdvisorIcon = "ui_notification_tutorial",
                        Body = new string[]
                        {
                            "Warehouses are essential to managing item movement throughout your cities.",
                            "Not only do they store excess items, but the warehouse workers can carry those items across the entire city.",
                            "Build a warehouse near your brickworks and bakeries."
                        }
                    });
            }

            if (_playerData.PlayerTribe != null
                && _playerData.PlayerTribe.Settlements.Any()
                && _playerData.PlayerTribe.Settlements.First().EconomicActors.Where(ea => ea.Name == "Warehouse" && !ea.UnderConstruction).Count() > 0)
            {
                _messageShown = false;
                _tutorialStage += 1;
            }
        }

        private void DoGovernorCheck()
        {
            if (!_messageShown)
            {
                _messageShown = true;
                _dialogManager.PushModal<GenericNotificationModal, GenericNotificationModalContext>(
                    new GenericNotificationModalContext
                    {
                        Title = "Governors",
                        AdvisorIcon = "ui_notification_tutorial",
                        Body = new string[]
                        {
                            "Your tribe of Humittes is lead by a ruling dynasty, composed of the king and his descendants.",
                            "These characters can be assigned to different roles, such as being the governor of a settlement. Governors help manage a settlement, and provide Authority to keep the population loyal.",
                            "From the Overview tab of the lower right panel you can see the current Governor.",
                            "Assign a governor now."
                        }
                    });
            }

            if (_playerData.PlayerTribe != null
                && _playerData.PlayerTribe.Settlements.Any()
                && _playerData.PlayerTribe.NotablePeople.Any(np => np.GovernorOf != null))
            {
                _messageShown = false;
                _tutorialStage += 1;
            }
        }

        private void DoUnitsCheck()
        {
            if (!_messageShown)
            {
                _messageShown = true;
                _dialogManager.PushModal<GenericNotificationModal, GenericNotificationModalContext>(
                    new GenericNotificationModalContext
                    {
                        Title = "Training Units",
                        AdvisorIcon = "ui_notification_tutorial",
                        Body = new string[]
                        {
                            "The world is a dangerous place, to defend your growing city you'll need soldiers.",
                            "Soldiers come from several sources, starting with the population itself. Your city has a number of Citizen Levy recruits available, you can see them on the military tab of the lower right panel.",
                            "Once you have a source of wealth you can train professional recruits by building a barracks.",
                            "Using the 'Recruit Army' button, raise an army. You can find armies by looking at the world map."
                        }
                    });
            }

            if (_playerData.PlayerTribe != null
                && _playerData.PlayerTribe.Armies.Any())
            {
                _messageShown = false;
                _tutorialStage += 1;
            }
        }

        private void DoSecondSettlementCheck()
        {
            if (!_messageShown)
            {
                _messageShown = true;
                _dialogManager.PushModal<GenericNotificationModal, GenericNotificationModalContext>(
                    new GenericNotificationModalContext
                    {
                        Title = "A Second Settlement",
                        AdvisorIcon = "ui_notification_tutorial",
                        Body = new string[]
                        {
                            "Your tribe isn't limited to just one city, you can expand through settling villages or conquring neighbors.",
                            "Once your Citizen population is above 40, you will gain access to settler recruits. These settlers can settle new villages in unclaimed lands.",
                            "Alternatively, you can attack your neighbors to claim land. To do so, raise an army and declare war through the diplomacy button in the top left panel.",
                            "Whichever path you chose, be on the look out for new resources like copper ore and limestone."
                        }
                    });
            }

            if (_playerData.PlayerTribe != null
                && _playerData.PlayerTribe.Settlements.Count > 1)
            {
                _messageShown = false;
                _tutorialStage += 1;
            }
        }

        private void DoTradeCheck()
        {
            if (!_messageShown)
            {
                _messageShown = true;
                _dialogManager.PushModal<GenericNotificationModal, GenericNotificationModalContext>(
                    new GenericNotificationModalContext
                    {
                        Title = "Trade",
                        AdvisorIcon = "ui_notification_tutorial",
                        Body = new string[]
                        {
                            "Now that you have two settlements, you can link them together with trade routes.",
                            "Trade routes need two structures or developments: a sender and a reciever. A village is automatically a trade reciever, but you can build additional ones as well.",
                            "Trade senders come in different versions for land and sea trade, and can be placed as a development or a structure inside a city.",
                            "To create a trade route, select 'Trade Routes' from the overview tab of the bottom right panel.",
                            "After you create a trade route, you can specify which items to send."
                        }
                    });
            }

            if (_playerData.PlayerTribe != null
                && _tradeManager.GetTradeRoutesFor(_playerData.PlayerTribe).Any())
            {
                _messageShown = false;
                _tutorialStage += 1;
            }
        }
    }
}
