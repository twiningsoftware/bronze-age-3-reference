﻿using Bronze.Common.Services;
using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;
using Bronze.UI;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.Simulation
{
    public class AttritionWatcher : ISubSimulator
    {
        private readonly IPlayerDataTracker _playerData;
        private readonly INotificationsSystem _notificationsSystem;
        private readonly IArmyNotificationBuilder _armyNotificationBuilder;
        private readonly IUnitHelper _unitHelper;

        private double _checkTimer;

        public AttritionWatcher(
            IPlayerDataTracker playerData,
            INotificationsSystem notificationsSystem,
            IArmyNotificationBuilder armyNotificationBuilder,
            IUnitHelper unitHelper)
        {
            _playerData = playerData;
            _notificationsSystem = notificationsSystem;
            _armyNotificationBuilder = armyNotificationBuilder;
            _unitHelper = unitHelper;
        }

        public void StepSimulation(double elapsedMonths)
        {
            _checkTimer -= elapsedMonths;

            if(_checkTimer <= 0)
            {
                _checkTimer = 0.1;

                var attritionNotification = _notificationsSystem.GetActiveNotifications()
                    .Where(n => n.Handler == typeof(HasAttritionNotificationHandler).Name)
                    .FirstOrDefault();

                var hasAttrition = CheckForAttrition();

                if (hasAttrition && attritionNotification == null)
                {
                    _notificationsSystem.AddNotification(
                        new Notification
                        {
                            AutoOpen = false,
                            ExpireDate = -1,
                            Handler = typeof(HasAttritionNotificationHandler).Name,
                            Icon = "ui_notification_attrition",
                            SoundEffect = UiSounds.NotificationMinor,
                            QuickDismissable = false,
                            DismissOnOpen = false,
                            SummaryTitle = "Attrition",
                            SummaryBody = "We are suffering attrition.",
                            Data = new Dictionary<string, string>()
                            {
                                { "type", "has_attrition" }
                            }
                        });
                }
                else if (!hasAttrition && attritionNotification != null)
                {
                    attritionNotification.IsDismissed = true;
                }
            }
        }

        private bool CheckForAttrition()
        {
            for(var i = 0; i < _playerData.PlayerTribe.Armies.Count; i++)
            {
                var army = _playerData.PlayerTribe.Armies[i];
                for(var j = 0; j < army.Units.Count; j++)
                {
                    if(army.Units[j].SufferingAttrition)
                    {
                        return true;
                    }
                }
            }
            
            return false;
        }
    }
}
