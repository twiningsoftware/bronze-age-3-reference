﻿using Bronze.Common.Data.Dungeons;
using Bronze.Common.UI.Modals;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.Simulation.Events;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System.Linq;

namespace Bronze.Common.Simulation
{
    public class DungeonDiscoveryWatcher : IWorldStateService
    {
        private readonly INotificationsSystem _notificationsSystem;
        private readonly IGenericNotificationBuilder _genericNotificationBuilder;
        private readonly ILosTracker _losTracker;

        public DungeonDiscoveryWatcher(
            INotificationsSystem notificationsSystem,
            IGenericNotificationBuilder genericNotificationBuilder,
            ISimulationEventBus simulationEventBus,
            ILosTracker losTracker)
        {
            _notificationsSystem = notificationsSystem;
            _genericNotificationBuilder = genericNotificationBuilder;
            _losTracker = losTracker;
            simulationEventBus.OnSimulationEvent += SimulationEventBus_OnSimulationEvent;
        }

        private void SimulationEventBus_OnSimulationEvent(ISimulationEvent evt)
        {
            if(evt is CellExploredSimulationEvent cellExploredSimulationEvent)
            {
                if(!_losTracker.GodVision
                    && cellExploredSimulationEvent.Cell.Development != null
                    && cellExploredSimulationEvent.Cell.Development is DungeonDevelopment)
                {
                    _notificationsSystem.AddNotification(_genericNotificationBuilder.BuildSimpleNotification(
                        "ui_notification_dungeon",
                        $"{cellExploredSimulationEvent.Cell.Development.Name} Discovered",
                        $"Our scouts have discovered a point of interest.",
                        $"{cellExploredSimulationEvent.Cell.Development.Name} Discovered",
                        $"Our scouts have discovered a point of interest in {cellExploredSimulationEvent.Cell.Region.Name}, we should explore it.",
                        cellExploredSimulationEvent.Cell.Position));
                }
            }
        }

        public void Clear()
        {
        }
        
        public void Initialize(WorldState worldState)
        {
        }

        public void LoadCustomData(SerializedObject worldRoot)
        {
        }

        public void SaveCustomData(SerializedObject worldRoot)
        {
        }

        public void StepSimulation(double elapsedMonths)
        {
        }
    }
}
