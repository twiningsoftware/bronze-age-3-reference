﻿using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Diplomacy;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.Simulation.Events;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.Simulation
{
    public class DiplomacyBonusWatcher : IWorldStateService, ISubSimulator
    {
        private readonly IWorldManager _worldManager;
        private readonly IPlayerDataTracker _playerData;
        private readonly IGamedataTracker _gamedataTracker;
        private readonly INotificationsSystem _notificationsSystem;
        private readonly IGenericNotificationBuilder _genericNotificationBuilder;
        private readonly IDialogManager _dialogManager;
        private readonly IWarHelper _warHelper;
        private readonly List<Action> _results;
        private readonly Random _random;

        private Dictionary<Tribe, Settlement> _lastTribeCapitals;
        private Dictionary<Tribe, double> _nextCultMembershipCheck;

        private int _nextTribe;

        public DiplomacyBonusWatcher(
            IGameInterface gameInterface,
            IWorldManager worldManager, 
            IPlayerDataTracker playerData,
            IGamedataTracker gamedataTracker,
            INotificationsSystem notificationsSystem,
            IGenericNotificationBuilder genericNotificationBuilder,
            IDialogManager dialogManager,
            IWarHelper warHelper,
            ISimulationEventBus simulationEventBus)
        {
            _worldManager = worldManager;
            _playerData = playerData;
            _gamedataTracker = gamedataTracker;
            _notificationsSystem = notificationsSystem;
            _genericNotificationBuilder = genericNotificationBuilder;
            _dialogManager = dialogManager;
            _warHelper = warHelper;
            _results = new List<Action>();
            _random = new Random();

            _lastTribeCapitals = new Dictionary<Tribe, Settlement>();
            _nextCultMembershipCheck = new Dictionary<Tribe, double>();

            simulationEventBus.OnSimulationEvent += SimulationEventBus_OnSimulationEvent;
        }
        
        public void Clear()
        {
            _lastTribeCapitals.Clear();
        }

        public void Initialize(WorldState worldState)
        {
        }

        public void LoadCustomData(SerializedObject worldRoot)
        {
        }

        public void SaveCustomData(SerializedObject worldRoot)
        {
        }

        public void StepSimulation(double elapsedMonths)
        {
            if (_nextTribe > _worldManager.Tribes.Count)
            {
                _nextTribe = 0;
            }
            else
            {
                var tribe = _worldManager.Tribes[_nextTribe];

                if (!_lastTribeCapitals.ContainsKey(tribe))
                {
                    _lastTribeCapitals.Add(tribe, tribe.Capital);

                    RecalculateCapitalDistanceBonus(tribe);
                }
                else if (_lastTribeCapitals[tribe] != tribe.Capital)
                {
                    _lastTribeCapitals[tribe] = tribe.Capital;

                    RecalculateCapitalDistanceBonus(tribe);
                }

                RecalculateCultMembershipBonus(tribe);
            }
        }
        
        private void RecalculateCapitalDistanceBonus(Tribe tribe)
        {
            if(tribe == _playerData.PlayerTribe)
            {
                foreach(var aiTribe in _worldManager.Tribes)
                {
                    if(aiTribe != _playerData.PlayerTribe)
                    {
                        RecalculateCapitalDistanceBonus(aiTribe);
                    }
                }
            }
            else
            {
                if(tribe.Capital != null && _playerData.PlayerTribe != null && _playerData.PlayerTribe.Capital != null)
                {
                    var distanceBetweenCapitalsSq = _playerData.PlayerTribe.Capital.Region.Center.DistanceSq(
                            tribe.Capital.Region.Center);

                    var trustMod = Util.Clamp(Math.Sqrt(distanceBetweenCapitalsSq) / 100, 0, 1) * -100;

                    tribe.AddDiplomaticMemory(new DiplomaticMemory(
                            "distance_trust",
                            "Distance between our capitals.",
                            _worldManager.Month,
                            1,
                            0,
                            0,
                            trustMod));
                }
                else
                {
                    tribe.RemoveDiplomaticMemory("distance_trust");
                }
            }
        }

        private void RecalculateCultMembershipBonus(Tribe tribe)
        {
            if (tribe == _playerData.PlayerTribe)
            {
                foreach (var aiTribe in _worldManager.Tribes)
                {
                    if (aiTribe != _playerData.PlayerTribe)
                    {
                        RecalculateCultMembershipBonus(aiTribe);
                    }
                }
            }
            else
            {
                if(!_nextCultMembershipCheck.ContainsKey(tribe) || _nextCultMembershipCheck[tribe] <= _worldManager.Month)
                {
                    if(!_nextCultMembershipCheck.ContainsKey(tribe))
                    {
                        _nextCultMembershipCheck.Add(tribe, _worldManager.Month + 1);
                    }
                    else
                    {
                        _nextCultMembershipCheck[tribe] = _worldManager.Month + _random.NextDouble() * 6;
                    }

                    var totalOwnCultMembership = new IntIndexableLookup<Cult, int>();
                    var totalPlayerCultMembership = new IntIndexableLookup<Cult, int>();
                    var totalOwnPopulation = tribe.Settlements.Sum(s => s.Population.Count);
                    var totalPlayerPopulation = _playerData.PlayerTribe.Settlements.Sum(s => s.Population.Count);

                    foreach (var cult in _gamedataTracker.Cults)
                    {
                        foreach (var settlement in tribe.Settlements)
                        {
                            totalOwnCultMembership[cult] += settlement.CultMembership[cult]?.Count ?? 0;
                        }

                        foreach (var settlement in _playerData.PlayerTribe.Settlements)
                        {
                            totalPlayerCultMembership[cult] += settlement.CultMembership[cult]?.Count ?? 0;
                        }
                    }
                    

                    var totalSatisfaction = 0.0;
                    foreach (var cult in _gamedataTracker.Cults)
                    {
                        var satisfaction = 0.0;
                        
                        foreach (var otherCult in _gamedataTracker.Cults)
                        {
                            if (cult.CultRelations[otherCult] != 0)
                            {
                                satisfaction += cult.CultRelations[otherCult] * (totalPlayerCultMembership[otherCult] / (double)totalPlayerPopulation);
                            }
                            else if (cult == otherCult)
                            {
                                satisfaction += 1 * (totalPlayerCultMembership[otherCult] / (double)totalPlayerPopulation);
                            }
                        }

                        totalSatisfaction += satisfaction * totalOwnCultMembership[cult];
                    }
                    
                    var trustMod = Util.Clamp(totalSatisfaction / totalOwnPopulation, -1, 1) * 50;
                    var influenceMod = 0.0;
                    var description = "Religous differences.";

                    if(totalSatisfaction > 0)
                    {
                        influenceMod = Util.Clamp(totalSatisfaction / totalOwnPopulation, -1, 1) * 50;
                        description = "We worship the same gods";
                    }

                    tribe.AddDiplomaticMemory(new DiplomaticMemory(
                            "cult_trust",
                            description,
                            _worldManager.Month,
                            1,
                            0,
                            influenceMod,
                            trustMod));
                }
            }
        }

        private void SimulationEventBus_OnSimulationEvent(ISimulationEvent evt)
        {
            if(evt is NotablePersonDiedSimulationEvent
                || evt is NotablePersonMarriageSimulationEvent)
            {
                var dynastyLinkage = new SafeDictionary<Tribe, int>();
                
                foreach(var person in _playerData.PlayerTribe.NotablePeople)
                {
                    if (!person.IsDead && person.BirthTribe != _playerData.PlayerTribe)
                    {
                        dynastyLinkage[person.BirthTribe] = Math.Max(dynastyLinkage[person.BirthTribe], 2);
                    }

                    if (!person.IsDead && person.Mother != null
                        && person.Mother.Owner == _playerData.PlayerTribe
                        && person.Mother.BirthTribe != _playerData.PlayerTribe)
                    {
                        dynastyLinkage[person.BirthTribe] = Math.Max(dynastyLinkage[person.BirthTribe], 1);
                    }

                    if (!person.IsDead && person.Father != null
                        && person.Father.Owner == _playerData.PlayerTribe
                        && person.Father.BirthTribe != _playerData.PlayerTribe)
                    {
                        dynastyLinkage[person.BirthTribe] = Math.Max(dynastyLinkage[person.BirthTribe], 1);
                    }
                }

                foreach(var tribe in _worldManager.Tribes)
                {
                    if(tribe != _playerData.PlayerTribe)
                    {
                        foreach(var person in tribe.NotablePeople)
                        {
                            if (!person.IsDead && person.BirthTribe == _playerData.PlayerTribe)
                            {
                                dynastyLinkage[person.Owner] = Math.Max(dynastyLinkage[person.Owner], 2);
                            }

                            if (!person.IsDead && person.Mother != null
                                && person.Mother.Owner != _playerData.PlayerTribe
                                && person.Mother.BirthTribe == _playerData.PlayerTribe)
                            {
                                dynastyLinkage[person.Owner] = Math.Max(dynastyLinkage[person.Owner], 1);
                            }

                            if (!person.IsDead && person.Father != null
                                && person.Father.Owner != _playerData.PlayerTribe
                                && person.Father.BirthTribe == _playerData.PlayerTribe)
                            {
                                dynastyLinkage[person.Owner] = Math.Max(dynastyLinkage[person.Owner], 1);
                            }
                        }
                    }
                }

                foreach (var otherTribe in dynastyLinkage.Keys)
                {
                    if (dynastyLinkage[otherTribe] > 0)
                    {
                        if (dynastyLinkage[otherTribe] == 1)
                        {
                            otherTribe.AddDiplomaticMemory(new DiplomaticMemory(
                                "dynasty_linkage",
                                "Our dynasties are linked by ties of blood.",
                                _worldManager.Month,
                                1,
                                0,
                                15,
                                15));
                        }
                        else if (dynastyLinkage[otherTribe] == 2)
                        {
                            otherTribe.AddDiplomaticMemory(new DiplomaticMemory(
                                "dynasty_linkage",
                                "Our dynasties are closely linked by ties of blood.",
                                _worldManager.Month,
                                1,
                                0,
                                30,
                                30));
                        }
                    }
                    else
                    {
                        otherTribe.RemoveDiplomaticMemory("dynasty_linkage");
                    }
                }
            }
        }
    }
}
