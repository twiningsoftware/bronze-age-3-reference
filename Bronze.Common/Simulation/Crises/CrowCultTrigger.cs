﻿using Bronze.Common.Data.World.CellDevelopments;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.Simulation.Events;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Bronze.Common.Simulation.Crises
{
    public class CrowCultTrigger : ISubSimulator, IWorldStateService, IDataLoader
    {
        public string ElementName => "crow_cult_trigger";

        private readonly IWorldManager _worldManager;
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IPlayerDataTracker _playerData;
        private readonly INotificationsSystem _notificationsSystem;
        private readonly IGenericNotificationBuilder _genericNotificationBuilder;
        private readonly IUnitHelper _unitHelper;
        private readonly IXmlReaderUtil _xmlReaderUtil;
        private readonly IWarHelper _warHelper;
        private readonly IBattleHelper _battleHelper;
        private readonly IPeopleGenerator _peopleGenerator;
        private readonly ISimulationEventBus _simulationEventBus;
        private readonly IPeopleHelper _peopleHelper;
        private readonly INameSource _nameSource;
        private readonly Random _random;

        private Trait _worldEnableTrait;
        private double _delayMonths;
        private double _growRate;
        private double _maxIntensity;
        private Cult _crowCult;
        private BonusType _authorityBonus;
        private BonusType _strifeBonus;
        private NotablePersonSkill _injurySkill;
        private ITribeControllerType _banditControllerType;
        private Trait _ravenswornTribeTrait;
        private ITribeControllerType _rebelControllerType;

        private bool _playerKnowsAboutCult;
        private int _nextRegion;
        private Region[] _allRegions;
        private Dictionary<Region, double> _eventTimers;

        public CrowCultTrigger(
            IWorldManager worldManager,
            IGamedataTracker gamedataTracker,
            IPlayerDataTracker playerData,
            INotificationsSystem notificationsSystem,
            IGenericNotificationBuilder genericNotificationBuilder,
            IUnitHelper unitHelper,
            IXmlReaderUtil xmlReaderUtil,
            IWarHelper warHelper, 
            IBattleHelper battleHelper,
            IPeopleGenerator peopleGenerator,
            ISimulationEventBus simulationEventBus,
            IPeopleHelper peopleHelper,
            INameSource nameSource)
        {
            _worldManager = worldManager;
            _gamedataTracker = gamedataTracker;
            _playerData = playerData;
            _notificationsSystem = notificationsSystem;
            _genericNotificationBuilder = genericNotificationBuilder;
            _unitHelper = unitHelper;
            _warHelper = warHelper;
            _xmlReaderUtil = xmlReaderUtil;
            _battleHelper = battleHelper;
            _peopleGenerator = peopleGenerator;
            _simulationEventBus = simulationEventBus;
            _peopleHelper = peopleHelper;
            _nameSource = nameSource;
            _random = new Random();
            _eventTimers = new Dictionary<Region, double>();
        }

        public void Clear()
        {
            _allRegions = new Region[0];
            _nextRegion = 0;
            _playerKnowsAboutCult = false;
            _eventTimers.Clear();
        }

        public void Initialize(WorldState worldState)
        {
            _allRegions = worldState.Planes.SelectMany(p => p.Regions).ToArray();
            _nextRegion = 0;
            _playerKnowsAboutCult = false;
        }

        public void Load(XElement element)
        {
            _worldEnableTrait = _xmlReaderUtil.ObjectReferenceLookup(
                element,
                "world_enable_trait",
                _gamedataTracker.Traits,
                t => t.Id);

            _delayMonths = _xmlReaderUtil.AttributeAsDouble(element, "delay_months");

            _growRate = _xmlReaderUtil.AttributeAsDouble(element, "grow_rate");

            _maxIntensity = _xmlReaderUtil.AttributeAsDouble(element, "max_intensity");

            _crowCult = _xmlReaderUtil.ObjectReferenceLookup(
                element,
                "crow_cult",
                _gamedataTracker.Cults,
                c => c.Id);

            _authorityBonus = _xmlReaderUtil.ObjectReferenceLookup(
                element,
                "authority_bonus",
                _gamedataTracker.BonusTypes,
                b => b.Id);

            _strifeBonus = _xmlReaderUtil.ObjectReferenceLookup(
                element,
                "strife_bonus",
                _gamedataTracker.BonusTypes,
                b => b.Id);

            _injurySkill = _xmlReaderUtil.ObjectReferenceLookup(
                element,
                "injury_skill",
                _gamedataTracker.Skills,
                b => b.Id);

            _banditControllerType = _xmlReaderUtil.ObjectReferenceLookup(
                    element,
                    "bandit_controller_type",
                    _gamedataTracker.ControllerTypes,
                    b => b.Id);

            _ravenswornTribeTrait = _xmlReaderUtil.ObjectReferenceLookup(
                element,
                "ravensworn_tribe_trait",
                _gamedataTracker.Traits,
                t => t.Id);

            _rebelControllerType = _xmlReaderUtil.ObjectReferenceLookup(
                    element,
                    "rebel_controller_type",
                    _gamedataTracker.ControllerTypes,
                    b => b.Id);
        }

        public void LoadCustomData(SerializedObject worldRoot)
        {
            var serviceRoot = worldRoot.GetOptionalChild("crow_cult_trigger");

            if (serviceRoot != null)
            {
                _playerKnowsAboutCult = serviceRoot.GetOptionalBool("player_knows");

                foreach (var kvpRoot in serviceRoot.GetChildren("evt_timer"))
                {
                    var region = _worldManager.GetRegion(kvpRoot.GetInt("key"));
                    var val = kvpRoot.GetDouble("val");
                    _eventTimers.Add(region, val);
                }
            }
        }

        public void PostLoad(XElement element)
        {
        }

        public void SaveCustomData(SerializedObject worldRoot)
        {
            var serviceRoot = worldRoot.CreateChild("crow_cult_trigger");

            serviceRoot.Set("player_knows", _playerKnowsAboutCult);

            foreach (var kvp in _eventTimers)
            {
                var kvpRoot = serviceRoot.CreateChild("evt_timer");
                kvpRoot.Set("key", kvp.Key.Id);
                kvpRoot.Set("val", kvp.Value);
            }
        }

        public void StepSimulation(double elapsedMonths)
        {
            if(!_worldManager.WorldTraits.Contains(_worldEnableTrait)
                || _worldManager.Month < Constants.START_MONTH + _delayMonths)
            {
                return;
            }

            var intensity = Util.Clamp(
                (_worldManager.Month - Constants.START_MONTH - _delayMonths) * _growRate,
                0, _maxIntensity);
            
            _nextRegion += 1;
            
            if(_nextRegion < _allRegions.Length)
            {
                var region = _allRegions[_nextRegion];

                if(region.Settlement != null)
                {
                    var buff = region.Settlement.Buffs
                        .Where(b => b.Id == "buff_crow_infiltration")
                        .FirstOrDefault();
                    
                    if (region.Settlement.AverageSatisfaction < intensity)
                    {
                        if(buff == null)
                        {
                            buff = new Buff
                            {
                                Id = "buff_crow_infiltration",
                                IconKey = "buff_crow_infiltration",
                                Name = "Cult Infiltration",
                                Description = string.Empty,
                                ExpireMonth = -1
                            };
                            region.Settlement.AddBuff(buff);

                            buff.AppliedInfluence[_crowCult] = 40;

                            var unhappiestPop = region.Settlement.Population
                                .OrderBy(p => p.Satisfaction)
                                .FirstOrDefault();

                            if(unhappiestPop != null)
                            {
                                unhappiestPop.CultMembership = _crowCult;
                                region.Settlement.SetCalculationFlag("pop cult membership");
                            }
                            
                            if (region.Settlement.Owner == _playerData.PlayerTribe)
                            {
                                if(_playerKnowsAboutCult)
                                {
                                    _notificationsSystem.AddNotification(_genericNotificationBuilder
                                        .BuildSimpleNotification(
                                            "ui_notification_crow_start",
                                            $"{_crowCult.Name} Sighted in {region.Settlement.Name}!",
                                            $"The {_crowCult.Name} has been sighted again.",
                                            $"{_crowCult.Name} Sighted in {region.Settlement.Name}",
                                            $"It seems that the {_crowCult.Name} have made further inroads into your empire. Signs of their worshippers have been found again, this time in {region.Settlement.Name}.",
                                            region.Settlement.EconomicActors.OfType<ICellDevelopment>()
                                                .Where(cd => cd is VillageDevelopment || cd is DistrictDevelopment)
                                                .Select(cd => cd.Cell.Position)
                                                .FirstOrDefault()));
                                }
                                else
                                {
                                    _notificationsSystem.AddNotification(_genericNotificationBuilder
                                        .BuildSimpleNotification(
                                            "ui_notification_crow_start",
                                            $"Strange Cult Sighted in {region.Settlement.Name}!",
                                            $"A strange cult has been sighted in one of your settlements.",
                                            $"Strange Cult Sighted in {region.Settlement.Name}",
                                            $"Signs of a mysterious cult have been sighted in {region.Settlement.Name}. Strange circles marked in blood, adorned with black feathers have been seen in the back alleys. The wise men are calling them the {_crowCult.Name}.",
                                            region.Settlement.EconomicActors.OfType<ICellDevelopment>()
                                                .Where(cd => cd is VillageDevelopment || cd is DistrictDevelopment)
                                                .Select(cd => cd.Cell.Position)
                                                .FirstOrDefault()));
                                }

                                _playerKnowsAboutCult = true;
                            }
                        }
                    }
                    else if(region.Settlement.CultMembership[_crowCult]?.Count < 1
                        || region.Settlement.Owner.Traits.Contains(_ravenswornTribeTrait))
                    {
                        if(buff != null)
                        {
                            region.Settlement.RemoveBuff(buff);

                            if(region.Settlement.Owner == _playerData.PlayerTribe)
                            {
                                _notificationsSystem.AddNotification(_genericNotificationBuilder
                                        .BuildSimpleNotification(
                                            "ui_notification_crow_end",
                                            $"{_crowCult.Name} Sighted in {region.Settlement.Name}!",
                                            $"The {_crowCult.Name} has been sighted again.",
                                            $"{_crowCult.Name} Sighted in {region.Settlement.Name}",
                                            $"It seems that the {_crowCult.Name} have made further inroads into your empire. Signs of their worshippers have been found again, this time in {region.Settlement.Name}.",
                                            region.Settlement.EconomicActors.OfType<ICellDevelopment>()
                                                .Where(cd => cd is VillageDevelopment || cd is DistrictDevelopment)
                                                .Select(cd => cd.Cell.Position)
                                                .FirstOrDefault()));
                            }
                        }
                    }

                    var membershipPercent = region.Settlement.CultMembership[_crowCult]?.Count / (double)region.Settlement.Population.Count;

                    if(!_eventTimers.ContainsKey(region))
                    {
                        _eventTimers.Add(region, 0);
                    }
                    
                    if(_eventTimers[region] <= _worldManager.Month 
                        && region.Settlement.CultMembership[_crowCult] != null
                        && !region.Settlement.Owner.Traits.Contains(_ravenswornTribeTrait))
                    {
                        _eventTimers[region] = _worldManager.Month + _random.NextDouble() * 2 + 5;
                        var percent = region.Settlement.CultMembership[_crowCult].Count / (double)region.Settlement.Population.Count;

                        var roll = _random.NextDouble();

                        var settlementPos = region.Settlement.EconomicActors
                                .OfType<ICellDevelopment>()
                                .Where(cd => cd is DistrictDevelopment || cd is VillageDevelopment)
                                .Select(cd => cd.Cell.Position)
                                .FirstOrDefault();

                        if (percent > 0.05)
                        {
                            if (percent < 0.1)
                            {
                                DoUnrestDebuff(region.Settlement, roll);
                            }
                            else if (percent < 0.25)
                            {
                                if (roll > 0.75 && (region.Settlement == region.Settlement.Owner.Capital || region.Settlement.Governor != null))
                                {
                                    DoCharacterAssault(region.Settlement, settlementPos);
                                }
                                else if (roll > 0.5)
                                {
                                    DoPopAssault(region.Settlement, settlementPos);
                                }
                                else if (roll > 0.25)
                                {
                                    DoUnrestDebuff(region.Settlement, roll);
                                }
                                else
                                {
                                    DoBandits(region.Settlement, settlementPos);
                                }
                            }
                            else if (percent < 0.5)
                            {
                                if (roll > 0.85 && (region.Settlement == region.Settlement.Owner.Capital || region.Settlement.Governor != null))
                                {
                                    DoCharacterAssault(region.Settlement, settlementPos);
                                }
                                else if (roll > 0.7)
                                {
                                    DoPopAssault(region.Settlement, settlementPos);
                                }
                                else if (roll > 0.6)
                                {
                                    DoUnrestDebuff(region.Settlement, roll);
                                }
                                else
                                {
                                    DoBandits(region.Settlement, settlementPos);
                                }
                            }
                            else
                            {
                                DoRevolt(region.Settlement, settlementPos);
                            }
                        }
                    }
                }
            }
            else
            {
                _nextRegion = 0;
            }
        }

        private void DoUnrestDebuff(Settlement settlement, double roll)
        {
            Buff debuff;
            if (roll < 0.5)
            {
                debuff = new Buff
                {
                    Id = "buff_crow_unrest_0",
                    IconKey = "buff_crow_infiltration",
                    Name = "Subversive Activity",
                    Description = "Subversive cult activity is eroding authority.",
                    ExpireMonth = _eventTimers[settlement.Region]
                };
                debuff.AppliedBonuses[_authorityBonus] = -0.5;
                debuff.AppliedInfluence[_crowCult] = 40;
            }
            else
            {
                debuff = new Buff
                {
                    Id = "buff_crow_unrest_1",
                    IconKey = "buff_crow_infiltration",
                    Name = "Religious Unrest",
                    Description = "Subversive cult activity is causing unrest.",
                    ExpireMonth = _eventTimers[settlement.Region]
                };
                debuff.AppliedBonuses[_strifeBonus] = -0.5;
                debuff.AppliedInfluence[_crowCult] = 40;
            }

            settlement.AddBuff(debuff);

            if (settlement.Owner == _playerData.PlayerTribe)
            {
                _notificationsSystem.AddNotification(_genericNotificationBuilder
                        .BuildBuffNotification(
                            "ui_notification_crow_unrest",
                            $"Unrest in {settlement.Name}",
                            $"The {_crowCult.Name} is causing problems.",
                            $"Unrest in {settlement.Name}",
                            $"The {_crowCult.Name} presence in {settlement.Name} is causing problems.",
                            settlement,
                            debuff));
            }
        }

        private void DoPopAssault(Settlement settlement, CellPosition settlementPos)
        {
            var potentialTargets = settlement.Population
                .Where(p => p.CultMembership != _crowCult)
                .ToList();

            if (potentialTargets.Any())
            {
                var count = Math.Max(1, _random.Next(2, 5) / 100.0 * settlement.Population.Count);
                var victims = new List<Pop>();

                for (var i = 0; i < count && settlement.Population.Count > 1 && potentialTargets.Any(); i++)
                {
                    var victim = _random.Choose(potentialTargets);
                    settlement.Population.Remove(victim);
                    potentialTargets.Remove(victim);
                    victims.Add(victim);
                }

                settlement.SetCalculationFlag("pop murders");

                if (_playerData.PlayerTribe == settlement.Owner)
                {
                    var body = $"Evidence has been found in {settlement.Name} of grim human sacrifice, the bodies were found adorned with black feathers. It seems that the {_crowCult.Name} are behind it.";
                    body += $"\nVictims:";

                    foreach (var group in victims.GroupBy(p => p.Caste))
                    {
                        if (group.Count() > 1)
                        {
                            body += $"\n{group.Count()} {group.Key.NamePlural}";
                        }
                        else
                        {
                            body += $"\n1 {group.Key.Name}";
                        }
                    }

                    _notificationsSystem.AddNotification(
                        _genericNotificationBuilder.BuildSimpleNotification(
                            "ui_notification_crow_murders",
                            $"Human Sacrifice in {settlement.Name}",
                            $"There is evidence of human sacrifice in in {settlement.Name}.",
                            $"Human Sacrifice in {settlement.Name}",
                            body,
                            settlementPos));
                }
            }
        }
        
        private void DoCharacterAssault(Settlement settlement, CellPosition settlementPos)
        {
            var potentialTargets = new List<NotablePerson>();

            if (settlement.Governor != null)
            {
                potentialTargets.Add(settlement.Governor);
                if (settlement.Governor.Spouse != null && settlement.Governor.Spouse.IsIdle)
                {
                    potentialTargets.Add(settlement.Governor.Spouse);
                }
                potentialTargets.AddRange(settlement.Governor.Children.Where(c => c.IsIdle || !c.IsMature));
            }

            if (settlement == settlement.Owner.Capital)
            {
                potentialTargets.Add(settlement.Owner.Ruler);
                if (settlement.Owner.Ruler.Spouse != null && settlement.Owner.Ruler.Spouse.IsIdle)
                {
                    potentialTargets.Add(settlement.Owner.Ruler.Spouse);
                }
                potentialTargets.AddRange(settlement.Owner.Ruler.Children.Where(c => c.IsIdle || !c.IsMature));

                potentialTargets.AddRange(settlement.Owner.NotablePeople.Where(c => c.IsIdle || !c.IsMature));
            }
            
            potentialTargets.RemoveAll(np => np == null || np.IsDead);

            if (potentialTargets.Any())
            {
                var target = _random.Choose(potentialTargets);

                var skillPlacement = _peopleHelper.IncrementOrAdd(target, _injurySkill);

                if ((skillPlacement?.CanIncrement() ?? false) && _random.NextDouble() < 0.5)
                {
                    skillPlacement = _peopleHelper.IncrementOrAdd(target, _injurySkill);

                    if ((skillPlacement?.CanIncrement() ?? false) && _random.NextDouble() < 0.5)
                    {
                        skillPlacement = _peopleHelper.IncrementOrAdd(target, _injurySkill);
                    }
                }

                if (skillPlacement != null)
                {
                    var doDeath = _random.NextDouble() <= target.GetMinorStat("Death Chance");

                    if (doDeath)
                    {
                        _peopleHelper.Kill(target);
                    }
                    else if (_playerData.PlayerTribe == settlement.Owner)
                    {
                        _notificationsSystem.AddNotification(_genericNotificationBuilder
                            .BuildPersonSkillNotification(
                                "ui_notification_crow_murders",
                                $"{target.Name} Injured",
                                $"{target.Name} has been injured by an attack in {settlement.Name}.",
                                $"{target.Name} Injured",
                                $"{_crowCult.Name} cultists in {settlement.Name} in have injured {target.Name} in a suprise attack.",
                                target,
                                skillPlacement,
                                settlementPos));
                    }
                }
            }
        }

        private void DoBandits(Settlement settlement, CellPosition settlementPos)
        {
            var recruits = new List<Tuple<IUnitType, UnitEquipmentSet>>();
            foreach(var kvp in _crowCult.RecruitmentSlots)
            {
                var options = settlement.Race.AvailableUnits
                    .Where(ut => ut.RecruitmentCategory == kvp.Key)
                    .SelectMany(ut => ut.EquipmentSets
                        .Where(es => es.RecruitmentCost.All(iq => settlement.ItemAvailability[iq.Item]))
                        .Select(es => Tuple.Create(ut, es)))
                    .ToArray();

                if (options.Any())
                {
                    recruits.AddRange(_random.ChooseCount(options, kvp.Value));
                }
            }

            if(!recruits.Any())
            {
                return;
            }
            
            var potentials = settlement.Region.Cells
                .Where(c => recruits.Any(t=>t.Item2.CanPath(c)))
                .ToArray();

            if (potentials.Any())
            {
                var banditTribe = _worldManager.Tribes
                    .Where(t => t.Name == _crowCult.Adjective + " Bandits")
                    .FirstOrDefault();

                if (banditTribe == null)
                {
                    banditTribe = new Tribe
                    {
                        Race = settlement.Race,
                        PrimaryColor = BronzeColor.Black,
                        SecondaryColor = BronzeColor.Grey,
                        Name = _crowCult.Adjective + " Bandits",
                        IsBandits = true
                    };
                    _worldManager.Tribes.Add(banditTribe);

                    banditTribe.Controller = _banditControllerType.BuildController(
                        banditTribe, 
                        _random, 
                        _worldManager.Hostility, 
                        _worldManager.Month);
                }

                var unitsPlaced = false;

                var army = _unitHelper.CreateArmy(banditTribe, _random.Choose(potentials));

                foreach (var unitToSpawn in recruits)
                {
                    if (army.IsFull)
                    {
                        army = _unitHelper.CreateArmy(banditTribe, _random.Choose(potentials));
                    }

                    if (unitToSpawn.Item2.CanPath(army.Cell))
                    {
                        army.Units.Add(unitToSpawn.Item1.CreateUnit(unitToSpawn.Item2));
                        unitsPlaced = true;
                    }
                }

                if (unitsPlaced && _playerData.PlayerTribe == settlement.Owner)
                {
                    _notificationsSystem.AddNotification(
                        _genericNotificationBuilder.BuildSimpleNotification(
                            "ui_notification_crow_bandits",
                            $"{_crowCult.Adjective} bandits in {settlement.Name}",
                            $"{_crowCult.Adjective} bandits have been spotted near {settlement.Name}.",
                            $"{_crowCult.Adjective} bandits in {settlement.Name}",
                            $"{_crowCult.Name} has grown in strength in {settlement.Name} to the point where cultists are taking to the countryside, raiding and pillaging in the name of their twisted god.",
                            settlementPos));
                }
            }
        }

        private void DoRevolt(Settlement settlement, CellPosition settlementPos)
        {
            var oldOwner = settlement.Owner;

            var neighborsToJoin = settlement.Region.Neighbors
                .Where(n => n.Settlement != null && n.Settlement.Owner.Traits.Contains(_ravenswornTribeTrait))
                .Select(n => n.Owner)
                .Distinct()
                .ToArray();

            Tribe newOwner;
            var joinedExisting = neighborsToJoin.Any();

            if(!neighborsToJoin.Any())
            {
                newOwner = new Tribe
                {
                    Race = settlement.Race,
                    PrimaryColor = BronzeColor.Black,
                    SecondaryColor = BronzeColor.Grey,
                    Name = _nameSource.MakeTribeName(settlement.Race)
                };
                _worldManager.Tribes.Add(newOwner);
                newOwner.Controller = _rebelControllerType.BuildController(
                    newOwner, 
                    _random, 
                    _worldManager.Hostility, 
                    _worldManager.Month);

                newOwner.Race.StartingPeopleScheme
                        .CreateInitialRulers(_random, newOwner);

                newOwner.Ruler = newOwner.NotablePeople.OrderBy(np => np.BirthDate).FirstOrDefault();

                newOwner.Traits.Add(_ravenswornTribeTrait);

                foreach(var person in newOwner.NotablePeople)
                {
                    person.CultMembership = _crowCult;
                }
            }
            else
            {
                newOwner = _random.Choose(neighborsToJoin);
            }

            if(!_warHelper.AreAtWar(_playerData.PlayerTribe, newOwner))
            {
                _warHelper.StartWar(_playerData.PlayerTribe, newOwner);
            }

            var armiesToClaim = settlement.Owner.Armies
                .Where(a => a.Cell.Region == settlement.Region && a.General == null)
                .ToList();

            foreach (var army in armiesToClaim)
            {
                army.ChangeOwner(newOwner);
                army.CurrentOrder = null;
            }

            _simulationEventBus.SendEvent(new SettlementOwnerChangedSimulationEvent
            {
                OldOwner = oldOwner,
                NewOwner = newOwner,
                Settlement = settlement
            });

            settlement.ChangeOwner(newOwner);
            
            if (_playerData.PlayerTribe == settlement.Owner)
            {
                var body = $"The {_crowCult.Name} has grown powerful enough in {settlement.Name} to seize power for themselves. Your representatives have been thrown out of the city";
                if (armiesToClaim.Count > 0)
                {
                    body += $", and {armiesToClaim.Count} of your armies have joined the traitors.";
                }
                else
                {
                    body += ".";
                }

                _notificationsSystem.AddNotification(
                    _genericNotificationBuilder.BuildSimpleNotification(
                        "ui_notification_crow_revolt",
                        $"Rebellion in {settlement.Name}",
                        $"The {_crowCult.Name} in {settlement.Name} have sized power.",
                        $"Rebellion in {settlement.Name}",
                        body,
                        settlementPos,
                        autoOpen: true));
            }
            else if(_playerData.PlayerTribe.HasKnowledgeOf(oldOwner))
            {
                _notificationsSystem.AddNotification(
                    _genericNotificationBuilder.BuildSimpleNotification(
                        "ui_notification_crow_revolt",
                        $"News of Rebellion",
                        $"The {_crowCult.Name} have sized control in {settlement.Name}.",
                        $"News of Rebellion",
                        $"The {_crowCult.Name} has grown powerful enough in {settlement.Name} to seize power from their rightful rulers.",
                        settlementPos,
                        autoOpen: true));
            }
        }
    }
}
