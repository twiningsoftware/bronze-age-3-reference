﻿using Bronze.Common.Data.Game.Units;
using Bronze.Common.Data.World.CellDevelopments;
using Bronze.Common.Data.World.Units;
using Bronze.Contracts;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.Simulation.Events;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Bronze.Common.Simulation.Crises
{
    public class GhulLairTrigger : ISubSimulator, IWorldStateService, IDataLoader
    {
        public string ElementName => "ghul_lair_trigger";

        private readonly IWorldManager _worldManager;
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IPlayerDataTracker _playerData;
        private readonly INotificationsSystem _notificationsSystem;
        private readonly IGenericNotificationBuilder _genericNotificationBuilder;
        private readonly IUnitHelper _unitHelper;
        private readonly IXmlReaderUtil _xmlReaderUtil;
        private readonly IWarHelper _warHelper;
        private readonly IBattleHelper _battleHelper;
        private readonly IPeopleGenerator _peopleGenerator;
        private readonly ISimulationEventBus _simulationEventBus;
        private readonly Random _random;

        private bool _enabled;
        private ICellDevelopmentType _lairDevelopment;
        private Race _ghulRace;
        private IUnitType _ghulUnit;
        private double _spawnTimeMin;
        private double _spawnTimeMax;
        private int _spawnCountMin;
        private int _spawnCountMax;
        private double _progressMod;
        private double _delayMonths;

        private int _nextRegion;
        private Region[] _allRegions;
        private Dictionary<int, GhulRegionState> _statePerRegion;
        private Tribe _ghulTribe;
        private List<string> _knownKings;

        public GhulLairTrigger(
            IWorldManager worldManager,
            IGamedataTracker gamedataTracker,
            IPlayerDataTracker playerData,
            INotificationsSystem notificationsSystem,
            IGenericNotificationBuilder genericNotificationBuilder,
            IUnitHelper unitHelper,
            IXmlReaderUtil xmlReaderUtil,
            IWarHelper warHelper, 
            IBattleHelper battleHelper,
            IPeopleGenerator peopleGenerator,
            ISimulationEventBus simulationEventBus)
        {
            _worldManager = worldManager;
            _gamedataTracker = gamedataTracker;
            _playerData = playerData;
            _notificationsSystem = notificationsSystem;
            _genericNotificationBuilder = genericNotificationBuilder;
            _unitHelper = unitHelper;
            _warHelper = warHelper;
            _xmlReaderUtil = xmlReaderUtil;
            _battleHelper = battleHelper;
            _peopleGenerator = peopleGenerator;
            _simulationEventBus = simulationEventBus;
            _statePerRegion = new Dictionary<int, GhulRegionState>();
            _random = new Random();
            _knownKings = new List<string>();
        }

        public void Clear()
        {
            _allRegions = new Region[0];
            _nextRegion = 0;
            _statePerRegion.Clear();
            _ghulTribe = null;
            _knownKings.Clear();
        }

        public void Initialize(WorldState worldState)
        {
            _allRegions = worldState.Planes.SelectMany(p => p.Regions).ToArray();
            _nextRegion = 0;
        }

        public void Load(XElement element)
        {
            _enabled = _xmlReaderUtil.AttributeAsBool(element, "enabled");

            _progressMod = _xmlReaderUtil.AttributeAsDouble(element, "infestation_progress_mod");

            _ghulRace = _xmlReaderUtil.ObjectReferenceLookup(
                element,
                "race_id",
                _gamedataTracker.Races,
                r => r.Id);

            _lairDevelopment = _xmlReaderUtil.ObjectReferenceLookup(
                element,
                "lair_development_id",
                _ghulRace.AvailableCellDevelopments,
                r => r.Id);

            _ghulUnit = _xmlReaderUtil.ObjectReferenceLookup(
                element,
                "unit_type_id",
                _ghulRace.AvailableUnits,
                r => r.Id);

            _spawnTimeMin = _xmlReaderUtil.AttributeAsDouble(element, "spawn_time_min");
            _spawnTimeMax = _xmlReaderUtil.AttributeAsDouble(element, "spawn_time_max");
            _spawnCountMin = _xmlReaderUtil.AttributeAsInt(element, "spawn_count_min");
            _spawnCountMax = _xmlReaderUtil.AttributeAsInt(element, "spawn_count_max");
            _delayMonths = _xmlReaderUtil.AttributeAsDouble(element, "delay_months");
        }

        public void LoadCustomData(SerializedObject worldRoot)
        {
            var serviceRoot = worldRoot.GetOptionalChild("ghul_lair_trigger");

            if (serviceRoot != null)
            {
                _ghulTribe = serviceRoot.GetOptionalObjectReferenceOrDefault(
                    "ghul_tribe_id",
                    _worldManager.Tribes,
                    t => t.Id);

                foreach (var stateRoot in serviceRoot.GetChildren("region_state"))
                {
                    var regionId = stateRoot.GetInt("id");
                    var state = new GhulRegionState(_worldManager.Month)
                    {
                        HasHiddenLair = stateRoot.GetBool("has_hidden_lair"),
                        HasOvertLair = stateRoot.GetBool("has_overt_lair"),
                        LastEventCheckMonth = stateRoot.GetDouble("last_event_check"),
                        LastUpdateMonth = stateRoot.GetDouble("last_update"),
                        Progress = stateRoot.GetDouble("progress"),
                        SpawnTimer = stateRoot.GetDouble("spawn_timer"),
                        LairKnownToPlayer = stateRoot.GetBool("lair_known_to_player"),
                    };

                    if (stateRoot.GetOptionalChild("lair_location") != null)
                    {
                        state.LairLocation = _worldManager.GetCell(CellPosition.DeserializeFrom(stateRoot.GetChild("lair_location")));

                    }

                    if (_statePerRegion.ContainsKey(regionId))
                    {
                        _statePerRegion.Remove(regionId);
                    }

                    _statePerRegion.Add(regionId, state);
                }

                foreach (var kingRoot in serviceRoot.GetChildren("known_king"))
                {
                    _knownKings.Add(kingRoot.GetString("id"));
                }
            }
        }

        public void PostLoad(XElement element)
        {
        }

        public void SaveCustomData(SerializedObject worldRoot)
        {
            var serviceRoot = worldRoot.CreateChild("ghul_lair_trigger");

            serviceRoot.Set("ghul_tribe_id", _ghulTribe?.Id);

            foreach(var kvp in _statePerRegion)
            {
                var stateRoot = serviceRoot.CreateChild("region_state");
                stateRoot.Set("id", kvp.Key);
                stateRoot.Set("has_hidden_lair", kvp.Value.HasHiddenLair);
                stateRoot.Set("has_overt_lair", kvp.Value.HasOvertLair);
                stateRoot.Set("lair_known_to_player", kvp.Value.LairKnownToPlayer);
                if (kvp.Value.LairLocation != null)
                {
                    kvp.Value.LairLocation.Position.SerializeTo(stateRoot.CreateChild("lair_location"));
                }
                stateRoot.Set("last_event_check", kvp.Value.LastEventCheckMonth);
                stateRoot.Set("last_update", kvp.Value.LastUpdateMonth);
                stateRoot.Set("progress", kvp.Value.Progress);
                stateRoot.Set("spawn_timer", kvp.Value.SpawnTimer);
            }

            foreach(var kingId in _knownKings)
            {
                serviceRoot.CreateChild("known_king").Set("id", kingId);
            }
        }

        public void StepSimulation(double elapsedMonths)
        {
            if (!_enabled || _worldManager.Month < Constants.START_MONTH + _delayMonths)
            {
                return;
            }

            var progressMod = (0.5 + _worldManager.Hostility) * _progressMod;
            
            _nextRegion += 1;

            if(_ghulTribe == null)
            {
                _ghulTribe = new Tribe
                {
                    Race = _ghulRace,
                    PrimaryColor = Contracts.Data.BronzeColor.Black,
                    SecondaryColor = Contracts.Data.BronzeColor.ForestGreen,
                    Name = "Ghuls",
                    IsBandits = true
                };

                _worldManager.Tribes.Add(_ghulTribe);
            }

            if(_nextRegion < _allRegions.Length)
            {
                var region = _allRegions[_nextRegion];
                
                if (!_statePerRegion.ContainsKey(region.Id))
                {
                    _statePerRegion.Add(region.Id, new GhulRegionState(_worldManager.Month));
                }
                var stateForRegion = _statePerRegion[region.Id];
                var deltaMonths = _worldManager.Month - stateForRegion.LastUpdateMonth;
                stateForRegion.LastUpdateMonth = _worldManager.Month;
                var corpsesInRegion = region.WorldActors.OfType<WorldCorpse>().Count();

                ProcessRegion(region, stateForRegion, corpsesInRegion, deltaMonths, progressMod);
            }
            else
            {
                _nextRegion = 0;
            }

            if (_ghulTribe != null)
            {
                foreach(var army in _ghulTribe.Armies.ToArray())
                {
                    if(army.Units.Any())
                    {
                        var potentialMerge = army.Cell.Yield().Concat(army.Cell.Neighbors)
                            .SelectMany(c => c.WorldActors)
                            .OfType<Army>()
                            .Where(a => a.Owner == _ghulTribe && a.General == null)
                            .ToArray();

                        foreach(var otherArmy in potentialMerge)
                        {
                            if(army.StrengthEstimate > otherArmy.StrengthEstimate || army.General != null)
                            {
                                if(army.Units.Count + otherArmy.Units.Count <= Constants.UNITS_PER_ARMY)
                                {
                                    army.Units.AddRange(otherArmy.Units);
                                    otherArmy.Units.Clear();
                                    _unitHelper.RemoveArmy(otherArmy);
                                }
                            }
                        }
                    }
                }

                foreach (var army in _ghulTribe.Armies)
                {
                    if (army.General != null && _playerData.PlayerTribe.IsVisible(army.Cell) && !_knownKings.Contains(army.General.Id))
                    {
                        _knownKings.Add(army.General.Id);

                        if(army.Cell.Owner == _playerData.PlayerTribe)
                        {
                            _notificationsSystem.AddNotification(
                                _genericNotificationBuilder.BuildPersonNotification(
                                    "ui_notification_ghul_king_new",
                                    "Ghul King Sighted!",
                                    $"A Ghul King has been sighted near {army.Cell.Region.Settlement.Name}.",
                                    "Ghul King Sighted!",
                                    $"A foul Ghul King has been sighted near {army.Cell.Region.Settlement.Name}. This foul abomination must be destroyed!",
                                    army.General,
                                    army.Cell.Position));
                        }
                        else
                        {
                            _notificationsSystem.AddNotification(
                                _genericNotificationBuilder.BuildPersonNotification(
                                    "ui_notification_ghul_king_new",
                                    "Ghul King Sighted!",
                                    $"A Ghul King has been sighted in {army.Cell.Region.Name}.",
                                    "Ghul King Sighted!",
                                    $"A foul Ghul King has been sighted in {army.Cell.Region.Name}. This foul abomination must be destroyed!",
                                    army.General,
                                    army.Cell.Position));
                        }
                    }

                    // Issue orders
                    if (army.CurrentOrder.IsIdle)
                    {
                        army.CurrentOrder = GetArmyOrder(army);
                    }

                    // Upgrade to ghul kings
                    if (army.Units.All(u => u.SuppliesPercent > 0.8) && army.General == null && army.IsFull)
                    {
                        var nearbyGhulKings = _ghulTribe.Armies
                            .Where(a => a.Cell.Position.Plane == army.Cell.Position.Plane)
                            .Where(a => a.General != null)
                            .Where(a => (a.Position - army.Position).Length() < 30)
                            .Any();

                        var t = _ghulTribe.Armies
                            .Where(a => a.Cell.Position.Plane == army.Cell.Position.Plane)
                            .Where(a => a.General != null)
                            .Select(a => (a.Position - army.Position).Length())
                            .ToArray();

                        if (!nearbyGhulKings)
                        {
                            var king = _peopleGenerator.CreatePerson(
                                    _random,
                                    _ghulTribe,
                                    _ghulRace,
                                    true,
                                    _worldManager.Month,
                                    null,
                                    null,
                                    true);

                            king.Owner.NotablePeople.Add(king);

                            king.AssignTo(army);
                        }
                    }

                    // Do siege assaults
                    if (army.CurrentOrder is ArmyInteractOrder interactOrder
                        && interactOrder.SiegedSettlement != null
                        && interactOrder.SiegedSettlement.Owner != _ghulTribe)
                    {
                        _battleHelper.DoSiegeAssault(interactOrder.SiegedSettlement, _ghulTribe);
                    }
                }

                foreach(var person in _ghulTribe.NotablePeople)
                {
                    if(!person.IsDead && person.GeneralOf == null)
                    {
                        person.IsDead = true;

                        _simulationEventBus.SendEvent(new NotablePersonDiedSimulationEvent(person));

                        if(_knownKings.Contains(person.Id))
                        {
                            _knownKings.Remove(person.Id);

                            _notificationsSystem.AddNotification(
                                _genericNotificationBuilder.BuildPersonNotification(
                                    "ui_notification_ghul_king_dead",
                                    "Ghul King Dead!",
                                    $"The Ghul King {person.Name} is dead.",
                                    "Ghul King Dead!",
                                    $"The Ghul King {person.Name} is dead. It's foul presence will threaten our world no more.",
                                    person,
                                    null));
                        }
                    }
                }

                foreach(var settlement in _ghulTribe.Settlements.ToArray())
                {
                    var unitCount = Math.Sqrt(settlement.Population.Count) / 2;

                    var settlementPosition = settlement.EconomicActors.OfType<ICellDevelopment>()
                        .Where(cd => cd is VillageDevelopment || cd is DistrictDevelopment)
                        .Select(cd => cd.Cell)
                        .FirstOrDefault();
                    
                    var armiesToFill = _ghulTribe.Armies
                        .Where(a => a.Cell.Region == settlement.Region)
                        .Where(a => !a.IsFull)
                        .ToList();

                    for(var i = 0; i < unitCount; i++)
                    {
                        if(!armiesToFill.Any())
                        {
                            armiesToFill.Add(_unitHelper.CreateArmy(_ghulTribe, settlementPosition));
                        }

                        armiesToFill.First().Units.Add(_ghulUnit.CreateUnit(_random.Choose(_ghulUnit.EquipmentSets)));

                        armiesToFill.RemoveAll(a => a.IsFull);
                    }

                    _ghulTribe.Abandon(settlement);
                }
            }
        }

        private void ProcessRegion(Region region, GhulRegionState stateForRegion, int corpsesInRegion, double deltaMonths, double progressMod)
        {
            if ((stateForRegion.HasOvertLair && stateForRegion.LairLocation.Development?.Name != _lairDevelopment.Name)
                    || ((stateForRegion.HasOvertLair || stateForRegion.HasHiddenLair) && stateForRegion.LairLocation == null)
                    || ((stateForRegion.HasOvertLair || stateForRegion.HasHiddenLair) && stateForRegion.Progress < 3))
            {
                if (stateForRegion.LairLocation != null && stateForRegion.LairLocation?.Development?.Name == _lairDevelopment.Name)
                {
                    stateForRegion.LairLocation.Development = null;
                }

                stateForRegion.HasOvertLair = false;
                stateForRegion.HasHiddenLair = false;
                stateForRegion.LairKnownToPlayer = false;
                stateForRegion.LairLocation = null;
                stateForRegion.Progress = 0;
                stateForRegion.SpawnTimer = 0;
            }

            if (corpsesInRegion > 0)
            {
                stateForRegion.Progress += corpsesInRegion * deltaMonths * progressMod;
            }
            else
            {
                stateForRegion.Progress = Math.Max(0, stateForRegion.Progress * deltaMonths * -1);
            }

            if (!stateForRegion.HasOvertLair && !stateForRegion.HasHiddenLair && stateForRegion.Progress > 10)
            {
                PlaceLair(region, stateForRegion);
            }
            else
            {
                if (_worldManager.Month >= 1 + stateForRegion.LastEventCheckMonth)
                {
                    stateForRegion.LastEventCheckMonth = _worldManager.Month + _random.NextDouble() / 10;

                    if (_random.NextDouble() <= stateForRegion.Progress / 5)
                    {
                        var likelyhoodMurders = region.Settlement != null ? 1 : 0;
                        var likelyhoodAttack = region.WorldActors.OfType<Army>().Any() ? 1 : 0;

                        if (likelyhoodMurders + likelyhoodAttack > 0)
                        {
                            var r = _random.Next(0, likelyhoodMurders + likelyhoodAttack + 1);

                            if (r < likelyhoodMurders)
                            {
                                DoPopMurdersEvent(region, stateForRegion);
                            }
                            else
                            {
                                DoArmyAttackEvent(region, stateForRegion);
                            }
                        }
                    }
                }
            }

            if (stateForRegion.HasHiddenLair && !stateForRegion.HasOvertLair
                && stateForRegion.LairLocation.WorldActors
                    .Concat(stateForRegion.LairLocation.Neighbors.SelectMany(c => c.WorldActors))
                    .OfType<Army>()
                    .Where(a => a.Owner != _ghulTribe)
                    .Any())
            {
                MakeLairOvert(region, stateForRegion);
            }

            if (stateForRegion.HasOvertLair
                && !stateForRegion.LairKnownToPlayer
                && _playerData.PlayerTribe.IsVisible(stateForRegion.LairLocation))
            {
                DiscoverLair(region, stateForRegion);
            }

            if (stateForRegion.HasOvertLair || stateForRegion.HasHiddenLair)
            {
                stateForRegion.SpawnTimer -= deltaMonths;

                if (stateForRegion.SpawnTimer <= 0)
                {
                    var randomFactor = (
                            _random.NextDouble()
                            + (1 - _worldManager.Hostility))
                             / 2;

                    stateForRegion.SpawnTimer = randomFactor * (_spawnTimeMax - _spawnTimeMin) + _spawnTimeMin;

                    var army = _unitHelper.CreateArmy(_ghulTribe, stateForRegion.LairLocation);

                    var unitCount = _random.Next(_spawnCountMin, _spawnCountMax);

                    for (var j = 0; j < unitCount; j++)
                    {
                        army.Units.Add(_ghulUnit.CreateUnit(_random.Choose(_ghulUnit.EquipmentSets)));
                    }
                }
            }

            if (stateForRegion.Progress > 10)
            {
                stateForRegion.Progress = 10;
            }
        }

        private IArmyOrder GetArmyOrder(Army army)
        {
            var eatAction = army.AvailableActions.Where(a => a is ArmyEatCorpseAction).FirstOrDefault();
            if(eatAction != null)
            {
                var eatTarget = army.Cell.Neighbors
                    .Concat(army.Cell.Yield())
                    .OrderBy(c => c.Position.DistanceSq(army.Cell.Position))
                    .Where(c => eatAction.IsValidFor(army, c))
                    .FirstOrDefault();

                if(eatTarget != null)
                {
                    return eatAction.BuildOrderFor(army, eatTarget);
                }
            }

            var destroyTarget = army.Cell.Neighbors
                .Where(c => c.Development != null && c.Development.Name != _lairDevelopment.Name && c.Development is CreepCampDevelopment)
                .Where(c => army.CanPath(c))
                .OrderBy(c => c.Position.DistanceSq(army.Cell.Position))
                .FirstOrDefault();

            if (destroyTarget != null && army.General != null)
            {
                return new ArmyInteractOrder(army, destroyTarget, _battleHelper);
            }

            if (_random.NextDouble() < _worldManager.Hostility)
            {
                var siegeTarget = army.Cell.Neighbors
                    .Where(c => c.Development != null && c.Development.Name != _lairDevelopment.Name)
                    .Where(c => c.Development.CanBeSieged)
                    .FirstOrDefault();

                if(siegeTarget != null && army.General != null)
                {
                    return new ArmyInteractOrder(army, siegeTarget, _battleHelper);
                }

                var raidTarget = army.Cell.Neighbors
                    .Where(c => c.Development != null && c.Development.Name != _lairDevelopment.Name)
                    .Where(c => army.CanPath(c))
                    .Where(c => c.Development.CanBeRaided)
                    .OrderBy(c => c.Position.DistanceSq(army.Cell.Position))
                    .FirstOrDefault();

                if (raidTarget != null)
                {
                    return new ArmyRaidOrder(army, raidTarget, _warHelper);
                }
            }

            if(army.General != null)
            {
                if(army.Cell.Owner == null)
                {
                    var nextRegionCell = army.Cell.Region.Neighbors
                        .Where(r => r.Owner != null)
                        .SelectMany(r => r.Cells)
                        .Where(c => c.Development != null && army.CanPath(c))
                        .OrderBy(c => c.Position.DistanceSq(army.Cell.Position))
                        .FirstOrDefault();

                    if(nextRegionCell != null)
                    {
                        return new ArmyMoveOrder(army, nextRegionCell);
                    }
                }
            }
            
            var wanderCells = army.Cell.OrthoNeighbors
                .Where(c => army.CanPath(c))
                .ToArray();

            if (wanderCells.Any())
            {
                return new ArmyMoveOrder(army, _random.Choose(wanderCells));
            }

            return new IdleOrder();
        }

        private void MakeLairOvert(Region region, GhulRegionState stateForRegion)
        {
            if (_lairDevelopment.CanBePlaced(_ghulTribe, stateForRegion.LairLocation)
                && (stateForRegion.LairLocation.Development == null 
                    || !(stateForRegion.LairLocation.Development is VillageDevelopment || stateForRegion.LairLocation.Development is DistrictDevelopment)))
            {
                stateForRegion.HasHiddenLair = false;
                stateForRegion.HasOvertLair = true;

                _lairDevelopment.Place(_ghulTribe, stateForRegion.LairLocation, instantBuild: true);

                if(_playerData.PlayerTribe.IsVisible(stateForRegion.LairLocation))
                {
                    DiscoverLair(region, stateForRegion);
                }
            }
            else
            {
                stateForRegion.HasHiddenLair = false;
                stateForRegion.HasOvertLair = false;
                stateForRegion.LairKnownToPlayer = false;
                stateForRegion.LairLocation = null;
            }
        }

        private void DoPopMurdersEvent(Region region, GhulRegionState stateForRegion)
        {
            if(region.Settlement == null || region.Settlement.Population.Count < 2)
            {
                return;
            }

            var settlementPos = region.Settlement.EconomicActors
                .OfType<ICellDevelopment>()
                .Where(cd => cd is DistrictDevelopment || cd is VillageDevelopment)
                .Select(cd => cd.Cell.Position)
                .FirstOrDefault();

            var victim = _random.Choose(region.Settlement.Population);
            region.Settlement.Population.Remove(victim);
            region.Settlement.SetCalculationFlag("pop murders");
            stateForRegion.Progress += 1;
            
            if (_playerData.PlayerTribe == region.Settlement.Owner)
            {
                var body = $"There have been a series of brutal murders in {region.Settlement.Name}. Even worse, the bodies have been savagely mauled after death.";
                body += $"\nVictims:";
                body += $"\n1 {victim.Caste.Name}";
                    
                _notificationsSystem.AddNotification(
                    _genericNotificationBuilder.BuildSimpleNotification(
                        "ui_notification_murders",
                        $"Murders in {region.Settlement.Name}",
                        $"There have been a series of murders in {region.Settlement.Name}.",
                        $"Murders in {region.Settlement.Name}",
                        body,
                        settlementPos));
            }
        }

        private void DoArmyAttackEvent(Region region, GhulRegionState stateForRegion)
        {
            var localArmies = region.WorldActors
                .OfType<Army>()
                .Where(a => a.Units.Any())
                .Where(a => a.Owner != _ghulTribe)
                .ToArray();

            if (localArmies.Any())
            {
                var targetArmy = _random.Choose(localArmies);

                if (targetArmy.Units.Any())
                {
                    var targetUnit = _random.Choose(targetArmy.Units);

                    targetUnit.Health = targetUnit.MaxHealth * (targetUnit.HealthPercent - _random.Next(50, 100) / 100.0);

                    if (targetUnit.Health <= 0)
                    {
                        _unitHelper.RemoveUnit(targetArmy, targetUnit, removedViolently: true);
                    }

                    stateForRegion.Progress += 1;

                    if (_playerData.PlayerTribe == targetArmy.Owner)
                    {
                        _notificationsSystem.AddNotification(
                            _genericNotificationBuilder.BuildSimpleNotification(
                                "ui_notification_murders",
                                $"Soldiers Assaulted in {region.Name}",
                                $"Some soldiers in {region.Name} have been assaulted.",
                                $"Soldiers Assaulted in {region.Name}",
                                $"{targetArmy.Name} has been attacked in the night, some of your {targetUnit.Name} in {targetArmy.Name} have been injured.",
                                targetArmy.Cell.Position));
                    }
                }
            }
        }

        private void PlaceLair(Region region, GhulRegionState stateForRegion)
        {
            if(!stateForRegion.HasHiddenLair && !stateForRegion.HasOvertLair)
            {
                var possiblePositions = region.Cells
                    .Where(c => c.Development == null || !(c.Development is VillageDevelopment || c.Development is DistrictDevelopment))
                    .Where(c => _lairDevelopment.CanBePlaced(_ghulTribe, c))
                    .ToArray();

                if(possiblePositions.Any())
                {
                    stateForRegion.LairLocation = _random.Choose(possiblePositions);
                    stateForRegion.HasHiddenLair = true;
                    stateForRegion.LairKnownToPlayer = false;
                    stateForRegion.SpawnTimer = 0;
                }
            }
        }
        
        private void DiscoverLair(Region region, GhulRegionState stateForRegion)
        {
            stateForRegion.LairKnownToPlayer = true;

            if (region.Owner == _playerData.PlayerTribe)
            {
                _notificationsSystem.AddNotification(
                    _genericNotificationBuilder.BuildSimpleNotification(
                        "ui_notification_ghul_lair",
                        $"Ghul Lair Discovered near {region.Settlement.Name}",
                        $"A foul lair of ghuls has been discovered.",
                        $"Ghul Lair Discovered near {region.Settlement.Name}",
                        $"Scouts have discovered a lair of foul Ghuls near {region.Settlement.Name}. We should destroy it immediately, before the creatures can do any more damage.",
                        stateForRegion.LairLocation.Position));
            }
            else
            {
                _notificationsSystem.AddNotification(
                    _genericNotificationBuilder.BuildSimpleNotification(
                        "ui_notification_ghul_lair",
                        $"Ghul Lair Discovered in {region.Name}",
                        $"A foul lair of ghuls has been discovered.",
                        $"Ghul Lair Discovered in {region.Name}",
                        $"Scouts have discovered a lair of foul Ghuls in {region.Name}. We should beware of it, lest our armies fall prey to the creatures.",
                        stateForRegion.LairLocation.Position));
            }
        }

        private class GhulRegionState
        {
            public double LastUpdateMonth { get; set; }
            public double LastEventCheckMonth { get; set; }
            public double Progress { get; set; }
            public bool HasHiddenLair { get; set; }
            public bool HasOvertLair { get; set; }
            public bool LairKnownToPlayer { get; set; }
            public Cell LairLocation { get; set; }
            public double SpawnTimer { get; set; }
            public GhulRegionState(double currentMonth)
            {
                LastUpdateMonth = currentMonth;
                LastEventCheckMonth = currentMonth;
            }
        }
    }
}
