﻿using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Bronze.Common.Simulation
{
    public class SiegeDebuffWatcher : IDataLoader, ISubSimulator
    {
        public string ElementName => "siege_debuff";

        private readonly IXmlReaderUtil _xmlReaderUtil;
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IWorldManager _worldManager;

        private readonly Dictionary<Race, IntIndexableLookup<BonusType, double>> _debuffByRace;
        private int _nextTribe;

        public SiegeDebuffWatcher(
            IXmlReaderUtil xmlReaderUtil,
            IGamedataTracker gamedataTracker,
            IWorldManager worldManager)
        {
            _xmlReaderUtil = xmlReaderUtil;
            _gamedataTracker = gamedataTracker;
            _worldManager = worldManager;
            _debuffByRace = new Dictionary<Race, IntIndexableLookup<BonusType, double>>();
            _nextTribe = 0;
        }

        public void Load(XElement element)
        {
            foreach (var forRaceElement in element.Elements("for_race"))
            {
                var race = _xmlReaderUtil.ObjectReferenceLookup(
                    forRaceElement,
                    "race",
                    _gamedataTracker.Races,
                    b => b.Id);

                var bonuses = new IntIndexableLookup<BonusType, double>();

                if(_debuffByRace.ContainsKey(race))
                {
                    _debuffByRace.Remove(race);
                }
                _debuffByRace.Add(race, bonuses);

                foreach(var bonusElement in forRaceElement.Elements("bonus"))
                {
                    var bonusType = _xmlReaderUtil.ObjectReferenceLookup(
                        bonusElement,
                        "bonus",
                        _gamedataTracker.BonusTypes,
                        bt => bt.Id);

                    var value = _xmlReaderUtil.AttributeAsDouble(bonusElement, "value");

                    bonuses[bonusType] += value;
                }
            }
        }

        public void PostLoad(XElement element)
        {
        }

        public void StepSimulation(double elapsedMonths)
        {
            if(_nextTribe < _worldManager.Tribes.Count)
            {
                foreach(var settlement in _worldManager.Tribes[_nextTribe].Settlements)
                {
                    if (_debuffByRace.ContainsKey(settlement.Race))
                    {
                        var siegeBuff = settlement.Buffs
                            .Where(b => b.Id == "buff_under_siege")
                            .FirstOrDefault();

                        if (settlement.IsUnderSiege && siegeBuff == null)
                        {
                            settlement.AddBuff(new Buff
                            {
                                Id = "buff_under_siege",
                                IconKey = "buff_under_siege",
                                Name = "Under Siege",
                                Description = "The settlement is under siege.",
                                AppliedBonuses = _debuffByRace[settlement.Race],
                                ExpireMonth = -1
                            });
                        }
                        else if (!settlement.IsUnderSiege && siegeBuff != null)
                        {
                            settlement.RemoveBuff(siegeBuff);
                        }
                    }
                }

                _nextTribe += 1;
            }
            else
            {
                _nextTribe = 0;
            }
        }
    }
}
