﻿using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.InjectableServices;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Bronze.Common.Simulation
{
    public class RoadConnectivityChecker : IDataLoader, ISettlementSubcalcuator
    {
        public string ElementName => "road_connectivity";

        private readonly IXmlReaderUtil _xmlReaderUtil;
        private readonly IGamedataTracker _gamedataTracker;
        private readonly Dictionary<Race, Trait> _traitsPerRace;

        public RoadConnectivityChecker(
            IXmlReaderUtil xmlReaderUtil,
            IGamedataTracker gamedataTracker)
        {
            _xmlReaderUtil = xmlReaderUtil;
            _gamedataTracker = gamedataTracker;
            _traitsPerRace = new Dictionary<Race, Trait>(); 
        }

        public void DoCalculations(CalculationResults results)
        {
            for(var i = 0; i < results.Settlement.RoadConnections.Length; i++)
            {
                results.Settlement.RoadConnections[i] = true;
            }

            if (results.Settlement.Districts.Any(d => d.IsGenerated)
                && _traitsPerRace.ContainsKey(results.Settlement.Race)
                && _traitsPerRace[results.Settlement.Race] != null)
            {
                var roadTrait = _traitsPerRace[results.Settlement.Race];

                var allTiles = results.Settlement.Districts.SelectMany(d => d.AllTiles).ToArray();

                foreach(var facing in new [] { Facing.North, Facing.South, Facing.East, Facing.West})
                {
                    results.Settlement.RoadConnections[(int)facing] = allTiles
                        .Where(t => t.GetNeighbor(facing) == null)
                        .Where(t => t.Traits.Contains(roadTrait))
                        .Any();
                }
            }
        }

        public void Load(XElement element)
        {
            foreach(var child in element.Elements("applies_to"))
            {
                var race = _xmlReaderUtil.ObjectReferenceLookup(
                    child,
                    "race",
                    _gamedataTracker.Races,
                    r => r.Id);

                var trait = _xmlReaderUtil.ObjectReferenceLookup(
                    child,
                    "trait",
                    _gamedataTracker.Traits,
                    t => t.Id);

                if(_traitsPerRace.ContainsKey(race))
                {
                    _traitsPerRace[race] = trait;
                }
                else
                {
                    _traitsPerRace.Add(race, trait);
                }
            }
        }

        public void PostLoad(XElement element)
        {
        }
    }
}
