﻿using System.Collections.Generic;
using System.Linq;
using Bronze.Common.Data.World.Structures;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.Trade;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Priority_Queue;

namespace Bronze.Common.Simulation
{
    public class TraderToDistrictBorderTradePathCalculator : ISettlementSubcalcuator
    {
        public TraderToDistrictBorderTradePathCalculator()
        {
        }

        public void DoCalculations(CalculationResults results)
        {
            var borders = new Dictionary<Cell, List<Tile>>();

            var newPaths = new Dictionary<TraderStructure, List<TraderToDistrictBorderPath>>();

            var districts = results.Settlement.Districts.ToArray();
            var orthoFacings = new[] { Facing.North, Facing.South, Facing.East, Facing.West };
            foreach (var district in districts)
            {
                foreach (var tile in district.AllTiles)
                {
                    foreach (var facing in orthoFacings)
                    {
                        var neighborCell = district.Cell.GetNeighbor(facing);

                        if (tile.GetNeighbor(facing) == null
                            && neighborCell != null)
                        {
                            if (!borders.ContainsKey(neighborCell))
                            {
                                borders.Add(neighborCell, new List<Tile>());
                            }

                            borders[neighborCell].Add(tile);
                        }
                    }
                }
            }

            foreach (var trader in results.EconomicActors.OfType<TraderStructure>())
            {
                newPaths.Add(trader, new List<TraderToDistrictBorderPath>());

                foreach (var borderCell in borders.Keys)
                {
                    if (borderCell.Traits.ContainsAll(trader.TradeType.RequiredTraits)
                        && !borderCell.Traits.ContainsAny(trader.TradeType.PreventedBy))
                    {
                        var path = CalculatePath(trader, borders[borderCell]);

                        if (path != null)
                        {
                            newPaths[trader].Add(new TraderToDistrictBorderPath(borderCell, path.Last().District.Cell, path));
                        }
                    }
                }
            }

            results.ApplyResultsFunctions.Add(() =>
            {
                foreach (var tradeStructure in newPaths.Keys)
                {
                    tradeStructure.ToBorderPaths.Clear();
                    tradeStructure.ToBorderPaths.AddRange(newPaths[tradeStructure]);
                    tradeStructure.DetermineActiveToBorderPath();
                }
            });
        }

        private Tile[] CalculatePath(TraderStructure trader, List<Tile> goals)
        {
            var open = new SimplePriorityQueue<Tile, double>();
            var closed = new HashSet<Tile>();
            var cameFrom = new Dictionary<Tile, Tile>();
            var distTo = new Dictionary<Tile, double>();

            var requiredTraits = trader.TradeType.RequiredTraits;
            var forbiddenTraits = trader.TradeType.PreventedBy;

            var starts = trader.CalculateLogiDoors(trader.TradeType.Yield()).ToArray();

            foreach (var start in starts)
            {
                open.Enqueue(start, 0);
                cameFrom.Add(start, null);
                distTo.Add(start, 0);
            }

            while (open.Any())
            {
                var current = open.Dequeue();
                closed.Add(current);

                if (goals.Contains(current))
                {
                    return ConstructPath(current, cameFrom);
                }

                var neighbors = current.OrthoNeighbors
                    .Where(t => (t.Traits.ContainsAll(requiredTraits) && !t.Traits.ContainsAny(forbiddenTraits)) || goals.Contains(t))
                    .Where(n => !closed.Contains(n))
                    .ToArray();

                foreach (var neighbor in neighbors)
                {
                    var dist = distTo[current] + 1 * neighbor.LogiDistance;

                    if (open.Contains(neighbor))
                    {
                        if (dist < distTo[neighbor])
                        {
                            distTo[neighbor] = dist;
                            cameFrom[neighbor] = current;
                            open.UpdatePriority(neighbor, dist);
                        }
                    }
                    else
                    {
                        distTo.Add(neighbor, dist);
                        cameFrom.Add(neighbor, current);
                        open.Enqueue(neighbor, dist);
                    }
                }
            }

            return null;
        }

        private Tile[] ConstructPath(Tile current, Dictionary<Tile, Tile> cameFrom)
        {
            var path = new List<Tile>();
            while (current != null)
            {
                path.Add(current);
                current = cameFrom[current];
            }
            path.Reverse();

            return path.ToArray();
        }
    }
}
