﻿using System.Collections.Generic;
using System.Linq;
using Bronze.Common.Data.World.Structures;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.Trade;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Priority_Queue;

namespace Bronze.Common.Simulation
{
    public class TradeRecieverToDistrictBorderTradePathCalculator : ISettlementSubcalcuator
    {
        private readonly IGamedataTracker _gamedataTracker;

        public TradeRecieverToDistrictBorderTradePathCalculator(
            IGamedataTracker gamedataTracker)
        {
            _gamedataTracker = gamedataTracker;
        }

        public void DoCalculations(CalculationResults results)
        {
            var borders = new Dictionary<Cell, List<Tile>>();

            var newPaths = new Dictionary<TradeRecieverStructure, Dictionary<MovementType, List<TraderToDistrictBorderPath>>>();

            var districts = results.Settlement.Districts.ToArray();
            var orthoFacings = new[] { Facing.North, Facing.South, Facing.East, Facing.West };
            foreach (var district in districts)
            {
                foreach (var tile in district.AllTiles)
                {
                    foreach (var facing in orthoFacings)
                    {
                        var neighborCell = district.Cell.GetNeighbor(facing);

                        if (tile.GetNeighbor(facing) == null
                            && neighborCell != null)
                        {
                            if (!borders.ContainsKey(neighborCell))
                            {
                                borders.Add(neighborCell, new List<Tile>());
                            }

                            borders[neighborCell].Add(tile);
                        }
                    }
                }
            }

            foreach (var trader in results.EconomicActors.OfType<TradeRecieverStructure>())
            {
                newPaths.Add(trader, new Dictionary<MovementType, List<TraderToDistrictBorderPath>>());

                foreach (var movementType in _gamedataTracker.MovementTypes)
                {
                    newPaths[trader].Add(movementType, new List<TraderToDistrictBorderPath>());

                    foreach (var borderCell in borders.Keys)
                    {
                        if (borderCell.Traits.ContainsAll(movementType.RequiredTraits)
                            && !borderCell.Traits.ContainsAny(movementType.PreventedBy))
                        {
                            var path = CalculatePath(trader, movementType, borders[borderCell]);

                            if (path != null)
                            {
                                newPaths[trader][movementType].Add(new TraderToDistrictBorderPath(borderCell, path.Last().District.Cell, path));
                            }
                        }
                    }
                }
            }

            results.ApplyResultsFunctions.Add(() =>
            {
                foreach (var tradeStructure in newPaths.Keys)
                {
                    tradeStructure.ToBorderPaths.Clear();
                    foreach (var movementType in _gamedataTracker.MovementTypes)
                    {
                        tradeStructure.ToBorderPaths.Add(movementType, newPaths[tradeStructure][movementType]);
                    }

                    tradeStructure.DetermineActiveToBorderPaths();
                }
            });
        }

        private Tile[] CalculatePath(TradeRecieverStructure trader, MovementType movementType, List<Tile> goals)
        {
            var open = new SimplePriorityQueue<Tile, double>();
            var closed = new HashSet<Tile>();
            var cameFrom = new Dictionary<Tile, Tile>();
            var distTo = new Dictionary<Tile, double>();

            var requiredTraits = movementType.RequiredTraits;
            var forbiddenTraits = movementType.PreventedBy;

            var starts = trader.CalculateLogiDoors(movementType.Yield()).ToArray();

            foreach (var start in starts)
            {
                open.Enqueue(start, 0);
                cameFrom.Add(start, null);
                distTo.Add(start, 0);
            }

            while (open.Any())
            {
                var current = open.Dequeue();
                closed.Add(current);

                if (goals.Contains(current))
                {
                    return ConstructPath(current, cameFrom);
                }

                var neighbors = current.OrthoNeighbors
                    .Where(t => (t.Traits.ContainsAll(requiredTraits) && !t.Traits.ContainsAny(forbiddenTraits)) || goals.Contains(t))
                    .Where(n => !closed.Contains(n))
                    .ToArray();

                foreach (var neighbor in neighbors)
                {
                    var dist = distTo[current] + 1 * neighbor.LogiDistance;

                    if (open.Contains(neighbor))
                    {
                        if (dist < distTo[neighbor])
                        {
                            distTo[neighbor] = dist;
                            cameFrom[neighbor] = current;
                            open.UpdatePriority(neighbor, dist);
                        }
                    }
                    else
                    {
                        distTo.Add(neighbor, dist);
                        cameFrom.Add(neighbor, current);
                        open.Enqueue(neighbor, dist);
                    }
                }
            }

            return null;
        }

        private Tile[] ConstructPath(Tile current, Dictionary<Tile, Tile> cameFrom)
        {
            var path = new List<Tile>();
            while (current != null)
            {
                path.Add(current);
                current = cameFrom[current];
            }
            path.Reverse();

            return path.ToArray();
        }
    }
}
