﻿using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Bronze.Common.Simulation
{
    public class CultInfluenceSimulator : AbstractBackgroundCalculatorSubSimulator, IDataLoader
    {
        private static readonly object LOCK = new object();

        private readonly IWorldManager _worldManager;
        private readonly IGamedataTracker _gamedataTracker;
        private readonly ITradeManager _tradeManager;
        private readonly IXmlReaderUtil _xmlReaderUtil;
        
        private double _lastMonth;
        private double _nextMonth;
        private List<Action> _results;
        private Random _random;

        private BonusType _strifeBonus;

        public string ElementName => "cult_influence";

        public CultInfluenceSimulator(
            IGameInterface gameInterface,
            IWorldManager worldManager,
            IGamedataTracker gamedataTracker,
            ITradeManager tradeManager,
            IXmlReaderUtil xmlReaderUtil)
            :base(gameInterface)
        {
            _worldManager = worldManager;
            _gamedataTracker = gamedataTracker;
            _tradeManager = tradeManager;
            _xmlReaderUtil = xmlReaderUtil;
            _results = new List<Action>();
            _random = new Random();
        }

        protected override void StartCalculations()
        {
            if(_worldManager.Month >= _nextMonth)
            {
                var deltaMonths = _worldManager.Month - _lastMonth;
                _lastMonth = _worldManager.Month;
                _nextMonth = _worldManager.Month + (_random.NextDouble() / 2) + 0.25;

                for(var i = 0; i < _worldManager.Tribes.Count; i++)
                {
                    var tribe = _worldManager.Tribes[i];

                    for(var j = 0; j < tribe.Settlements.Count; j++)
                    {
                        DoCultInfluence(tribe.Settlements[j], deltaMonths);
                    }
                }
            }
        }

        protected override void ApplyResults()
        {
            Action[] localResults = null;

            lock(LOCK)
            {
                localResults = _results.ToArray();
                _results.Clear();
            }

            foreach(var result in localResults)
            {
                result();
            }
        }

        private void DoCultInfluence(Settlement settlement, double deltaMonths)
        {
            settlement.RiteCooldown = Math.Max(0, settlement.RiteCooldown - deltaMonths);

            var blanketInfluence = new IntIndexableLookup<Cult, double>();
            var influenceSourcesByCult = new IntIndexableLookup<Cult, List<Tuple<string, double>>>();
            var structureInfluenceByCult = new IntIndexableLookup<Cult, double>();
            var tradeInfluenceByCult = new IntIndexableLookup<Cult, double>();

            var occupiedNeighbors = settlement.Region.Neighbors
               .Where(r => r.Settlement != null)
               .Select(r => r.Settlement)
               .ToArray();

            // Influence from cult membership, buffs, and neighbors
            foreach (var cult in _gamedataTracker.Cults)
            {
                influenceSourcesByCult[cult] = new List<Tuple<string, double>>();

                if (settlement.CultMembership[cult] != null)
                {
                    var influence = settlement.CultMembership[cult].Count * cult.OwnPopInfluence;

                    blanketInfluence[cult] += influence;
                    influenceSourcesByCult[cult].Add(Tuple.Create("Membership", influence));
                }

                foreach(var buff in settlement.Buffs)
                {
                    if(buff.AppliedInfluence[cult] != 0)
                    {
                        var influence = buff.AppliedInfluence[cult] * settlement.Population.Count;
                        blanketInfluence[cult] += influence;
                        influenceSourcesByCult[cult].Add(Tuple.Create(buff.Name, influence));
                    }
                }

                var influenceFromNeighbors = occupiedNeighbors
                    .Where(n => n.CultMembership[cult] != null)
                    .Sum(n => n.CultMembership[cult].Count) * cult.OwnPopInfluence / 3;

                if(influenceFromNeighbors != 0)
                {
                    blanketInfluence[cult] += influenceFromNeighbors;
                    influenceSourcesByCult[cult].Add(Tuple.Create("Neighboring Regions", influenceFromNeighbors));
                }

                // Influence from governor
                if (settlement.Governor != null)
                {
                    var influence = settlement.Governor.GetCultInfluence(cult) * settlement.Population.Count;
                    blanketInfluence[cult] += influence;
                    influenceSourcesByCult[cult].Add(Tuple.Create("Governor", influence));
                }

                // Influence from ruler
                if (settlement.Owner.Ruler != null)
                {
                    var influence = settlement.Owner.Ruler.GetCultInfluence(cult) * settlement.Population.Count;
                    blanketInfluence[cult] += influence;
                    influenceSourcesByCult[cult].Add(Tuple.Create("Ruler", influence));
                }
            }

            foreach (var provider in settlement.EconomicActors.OfType<ICultInfluenceProvider>())
            {
                var influence = provider.InfluencePerPop * settlement.Population.Count;
                blanketInfluence[provider.Cult] += influence;
                structureInfluenceByCult[provider.Cult] += influence;
            }
            
            // Influence from trade
            foreach(var tradePartner in _tradeManager.GetTradePartnersFor(settlement))
            {
                var totalItemsMoving = tradePartner.Imports.Sum(ir => ir.PerMonth) + tradePartner.Exports.Sum(ir => ir.PerMonth);

                var influenceTransmission = Util.Clamp(totalItemsMoving / 30.0, 0, 1);

                foreach(var cult in _gamedataTracker.Cults)
                {
                    if (tradePartner.Partner.CultMembership[cult] != null)
                    {
                        var influence = tradePartner.Partner.CultMembership[cult].Count * cult.OwnPopInfluence * influenceTransmission;

                        blanketInfluence[cult] += influence;
                        tradeInfluenceByCult[cult] += influence;
                    }
                }

            }

            var totalInfluence = new IntIndexableLookup<Cult, double>();

            // Influence from service providers and apply to houses
            foreach (var housingProvider in settlement.EconomicActors.OfType<IHousingProvider>())
            {
                housingProvider.InfluencePerPop.Clear();

                foreach (var cultInfluenceProvider in housingProvider.ServiceProviders.OfType<ICultInfluenceServiceProvider>())
                {
                    housingProvider.InfluencePerPop[cultInfluenceProvider.Cult] += cultInfluenceProvider.InfluencePerPop;
                    var influence = cultInfluenceProvider.InfluencePerPop * housingProvider.Residents.Count;
                    structureInfluenceByCult[cultInfluenceProvider.Cult] += influence;
                    totalInfluence[cultInfluenceProvider.Cult] += influence;
                }

                foreach (var cult in _gamedataTracker.Cults)
                {
                    housingProvider.InfluencePerPop[cult] += blanketInfluence[cult] / settlement.Population.Count * housingProvider.Residents.Count;
                }
            }
            
            // Sum Up Influence
            var totalCultInfluence = 0.0;
            foreach (var cult in _gamedataTracker.Cults)
            {
                totalInfluence[cult] += blanketInfluence[cult];
                settlement.InfluenceByCult[cult] = totalInfluence[cult];
                totalCultInfluence += totalInfluence[cult];
                
                if(structureInfluenceByCult[cult] != 0)
                {
                    influenceSourcesByCult[cult].Add(Tuple.Create("Structures", structureInfluenceByCult[cult]));
                }

                if(tradeInfluenceByCult[cult] != 0)
                {
                    influenceSourcesByCult[cult].Add(Tuple.Create("Trade", tradeInfluenceByCult[cult]));
                }

                settlement.InfluenceSourcesByCult[cult] = influenceSourcesByCult[cult];
            }
            
            settlement.PopCultChangeTimer -= deltaMonths * settlement.Population.Count;
            
            // Move Pops Between Cults
            if (settlement.PopCultChangeTimer <= 0 && totalCultInfluence > 0)
            {
                settlement.PopCultChangeTimer = 24;

                var targetPopForCult = new IntIndexableLookup<Cult, int>();

                foreach (var cult in _gamedataTracker.Cults)
                {
                    var targetPop = (int)Math.Floor(totalInfluence[cult] / totalCultInfluence * settlement.Population.Count);

                    targetPopForCult[cult] = targetPop;
                }

                foreach (var cult in _gamedataTracker.Cults)
                {
                    if (targetPopForCult[cult] > 0 
                        && settlement.CultMembership[cult] != null
                        && targetPopForCult[cult] >= settlement.CultMembership[cult].Count + 1)
                    {
                        var cultsToPickFrom = _gamedataTracker.Cults
                            .Where(c => settlement.CultMembership[c].Count > targetPopForCult[c])
                            .OrderByDescending(c => settlement.CultMembership[c].Count - targetPopForCult[c])
                            .ToArray();

                        if (cultsToPickFrom.Any())
                        {
                            var popToPick = cultsToPickFrom
                                .Select(c => settlement.CultMembership[c]
                                    .OrderByDescending(p => p.Home == null ? 1 : p.Home.InfluencePerPop[cult])
                                    .FirstOrDefault())
                                .OrderByDescending(p => p.Home == null ? 1 : p.Home.InfluencePerPop[cult])
                                .FirstOrDefault();

                            if (popToPick != null)
                            {
                                _results.Add(() =>
                                {
                                    popToPick.CultMembership = cult;
                                    settlement.SetCalculationFlag("pop cult membership");
                                });
                            }

                        }
                    }
                }
            }

            // Cult Unrest
            var totalSatisfaction = 0.0;
            foreach (var cult in _gamedataTracker.Cults)
            {
                var satisfaction = 0.0;

                if (settlement.CultMembership[cult]?.Count > 0)
                {
                    foreach (var otherCult in _gamedataTracker.Cults.Where(c => c != cult))
                    {
                        if (cult.CultRelations[otherCult] != 0)
                        {
                            satisfaction += cult.CultRelations[otherCult] * ((settlement.CultMembership[otherCult]?.Count ?? 0) / (double)settlement.Population.Count);
                        }
                    }
                }

                totalSatisfaction += satisfaction * (settlement.CultMembership[cult]?.Count ?? 0);
            }

            var mod = totalSatisfaction / (double)settlement.Population.Count;

            var cultRelationsBuff = settlement.Buffs
                .Where(b => b.Id == "buff_cult_relations")
                .FirstOrDefault();
            if (mod > 0.01 || mod < -0.01)
            {
                if (cultRelationsBuff == null)
                {
                    cultRelationsBuff = new Buff
                    {
                        Id = "buff_cult_relations",
                        IconKey = "buff_cult_relations",
                        Name = "Cult Relations",
                        Description = string.Empty,
                        ExpireMonth = -1
                    };
                    settlement.AddBuff(cultRelationsBuff);
                }

                cultRelationsBuff.AppliedBonuses[_strifeBonus] = mod;
            }
            else if (cultRelationsBuff != null && mod < 0.01 && mod > -0.01)
            {
                settlement.RemoveBuff(cultRelationsBuff);
            }
        }

        public void Load(XElement element)
        {
            _strifeBonus = _xmlReaderUtil.ObjectReferenceLookup(
                element,
                "strife_bonus",
                _gamedataTracker.BonusTypes,
                b => b.Id);
        }

        public void PostLoad(XElement element)
        {
        }
    }
}
