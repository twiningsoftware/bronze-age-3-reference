﻿using Bronze.Contracts;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.Simulation
{
    public class DiplomaticDiscoveryWatcher : AbstractBackgroundCalculatorSubSimulator
    {
        private static readonly object LOCK = new object();

        private readonly IWorldManager _worldManager;
        private readonly IPlayerDataTracker _playerData;
        private readonly INotificationsSystem _notificationsSystem;
        private readonly IDiplomaticNotificationBuilder _diplomaticNotificationBuilder;
        private readonly ILosTracker _losTracker;

        private int _nextTribe;
        private int _nextArmy;
        private List<Action> _results;

        public DiplomaticDiscoveryWatcher(
            IGameInterface gameInterface,
            IWorldManager worldManager,
            IPlayerDataTracker playerData,
            INotificationsSystem notificationsSystem,
            IDiplomaticNotificationBuilder diplomaticNotificationBuilder,
            ILosTracker losTracker)
            :base(gameInterface)
        {
            _worldManager = worldManager;
            _playerData = playerData;
            _notificationsSystem = notificationsSystem;
            _diplomaticNotificationBuilder = diplomaticNotificationBuilder;
            _losTracker = losTracker;
            _results = new List<Action>();
        }

        protected override void StartCalculations()
        {
            if(_worldManager.Tribes.Any())
            {
                _nextTribe = _nextTribe % _worldManager.Tribes.Count;

                var tribe = _worldManager.Tribes[_nextTribe];
 
                var alreadyDiscovered = new List<Tribe>();

                if(_nextArmy < 0)
                {
                    for(var i = 0; i < tribe.Settlements.Count; i++)
                    {
                        var neighbors = tribe.Settlements[i].Region.Neighbors;
                        for(var j = 0; j < neighbors.Count; j++)
                        {
                            if (neighbors[j].Settlement != null
                                && neighbors[j].Settlement.Owner != tribe
                                && !tribe.HasKnowledgeOf(neighbors[j].Settlement.Owner)
                                && !alreadyDiscovered.Contains(neighbors[j].Settlement.Owner)
                                && !neighbors[j].Settlement.Owner.IsBandits
                                && !tribe.IsBandits)
                            {
                                var discoveryLocation = neighbors[j]
                                    .Cells
                                    .Where(c => c.Development != null)
                                    .Select(c => c.Position)
                                    .FirstOrDefault();

                                lock (LOCK)
                                {
                                    var otherTribe = neighbors[j].Settlement.Owner;

                                    _results.Add(() => DoDiscovery(tribe, otherTribe, discoveryLocation));
                                    alreadyDiscovered.Add(otherTribe);
                                }
                            }
                        }
                    }

                    _nextArmy += 1;
                }
                else
                {
                    if (_nextArmy < tribe.Armies.Count)
                    {
                        var army = tribe.Armies[_nextArmy];

                        foreach (var cell in _losTracker.GetVisionFor(army))
                        {
                            foreach (var otherArmy in cell.WorldActors.OfType<Army>())
                            {
                                if (otherArmy != null
                                    && !tribe.HasKnowledgeOf(otherArmy.Owner)
                                    && !alreadyDiscovered.Contains(otherArmy.Owner)
                                    && !otherArmy.Owner.IsBandits
                                    && !tribe.IsBandits)
                                {
                                    lock (LOCK)
                                    {
                                        _results.Add(() => DoDiscovery(tribe, otherArmy.Owner, cell.Position));
                                        alreadyDiscovered.Add(otherArmy.Owner);
                                    }
                                }
                            }

                            if (cell.Region.Settlement != null 
                                && !tribe.HasKnowledgeOf(cell.Region.Settlement.Owner) 
                                && !alreadyDiscovered.Contains(cell.Region.Settlement.Owner)
                                && !cell.Region.Settlement.Owner.IsBandits
                                && !tribe.IsBandits)
                            {
                                lock (LOCK)
                                {
                                    _results.Add(() => DoDiscovery(tribe, cell.Region.Settlement.Owner, cell.Position));
                                    alreadyDiscovered.Add(cell.Region.Settlement.Owner);
                                }
                            }
                        }

                        _nextArmy += 1;
                    }
                    else
                    {
                        _nextTribe += 1;
                        _nextArmy = -1;
                    }
                }
            }
        }

        protected override void ApplyResults()
        {
            Action[] localResults = null;

            lock(LOCK)
            {
                localResults = _results.ToArray();
                _results.Clear();
            }

            foreach(var result in localResults)
            {
                result();
            }
        }

        private void DoDiscovery(Tribe tribe, Tribe otherTribe, CellPosition discoveryLocation)
        {
            tribe.AddKnowledge(otherTribe);
            otherTribe.AddKnowledge(tribe);

            if (_playerData.PlayerTribe == tribe
                || _playerData.PlayerTribe == otherTribe)
            {
                var nonPlayerTribe = tribe == _playerData.PlayerTribe ? otherTribe : tribe;
                
                _notificationsSystem.AddNotification(_diplomaticNotificationBuilder
                    .BuildDiplomaticDiscoveryNotification(nonPlayerTribe, discoveryLocation));
            }
        }
    }
}
