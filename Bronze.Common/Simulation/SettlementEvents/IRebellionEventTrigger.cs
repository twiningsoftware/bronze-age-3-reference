﻿using System.Collections.Generic;
using Bronze.Contracts.Data.World;

namespace Bronze.Common.Simulation.SettlementEvents
{
    public interface IRebellionEventTrigger
    {
        void ToggleFor(Settlement settlement);

        IEnumerable<string> GetInfoFor(Settlement targetSettlement);
    }
}