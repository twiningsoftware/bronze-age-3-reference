﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Bronze.Common.Data.World.CellDevelopments;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.Simulation.Events;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.Simulation.SettlementEvents
{
    public class PeasantRebellionsEvent : ISubSimulator, IWorldStateService, IDataLoader, IRebellionEventTrigger
    {
        public const double THRESHOLD_WARNING = 0.2;
        public const double THRESHOLD_MINOR = 0.35;
        public const double THRESHOLD_MAJOR = 0.5;
        
        public string ElementName => "peasant_rebellions_events";

        private readonly IXmlReaderUtil _xmlReaderUtil;
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IWorldManager _worldManager;
        private readonly IGenericNotificationBuilder _genericNotificationBuilder;
        private readonly INotificationsSystem _notificationsSystem;
        private readonly IPlayerDataTracker _playerData;
        private readonly IPeopleHelper _peopleHelper;
        private readonly IUnitHelper _unitHelper;
        private readonly ISimulationEventBus _simulationEventBus;

        private Random _random;
        private readonly BronzeColor[] _tribeColors;
        private Dictionary<Caste, RebellionAppliesTo> _casteRebellionInfo;
        private Dictionary<string, double> _rebellionProgress;
        private Dictionary<string, double> _eventTimers;
        private Dictionary<string, double> _lastCheckMonth;
        private List<string> _disabledSettlements;
        private int _nextTribe;
        private int _nextSettlement;
        private double _rebellionProgressRate;
        private NotablePersonSkill _injurySkill;

        public PeasantRebellionsEvent(
            IGamedataTracker gamedataTracker,
            IXmlReaderUtil xmlReaderUtil,
            IWorldManager worldManager,
            IGenericNotificationBuilder genericNotificationBuilder,
            INotificationsSystem notificationsSystem,
            IPlayerDataTracker playerData,
            IPeopleHelper peopleHelper,
            IUnitHelper unitHelper,
            ISimulationEventBus simulationEventBus)
        {
            _gamedataTracker = gamedataTracker;
            _xmlReaderUtil = xmlReaderUtil;
            _worldManager = worldManager;
            _genericNotificationBuilder = genericNotificationBuilder;
            _notificationsSystem = notificationsSystem;
            _playerData = playerData;
            _peopleHelper = peopleHelper;
            _unitHelper = unitHelper;
            _simulationEventBus = simulationEventBus;

            _casteRebellionInfo = new Dictionary<Caste, RebellionAppliesTo>();
            _rebellionProgress = new Dictionary<string, double>();
            _eventTimers = new Dictionary<string, double>();
            _lastCheckMonth = new Dictionary<string, double>();
            _random = new Random();
            _disabledSettlements = new List<string>();

            _tribeColors = Enum.GetValues(typeof(BronzeColor))
                .Cast<BronzeColor>()
                .Where(c => c != BronzeColor.None)
                .ToArray();
        }

        public void Clear()
        {
            _rebellionProgress.Clear();
            _eventTimers.Clear();
            _nextTribe = 0;
            _nextSettlement = 0;
            _disabledSettlements.Clear();
        }

        public void Initialize(WorldState worldState)
        {
            _random = new Random(worldState.Seed.GetHashCode());
        }

        public void Load(XElement element)
        {
            _casteRebellionInfo = element.Elements("applies_to")
                .Select(e => LoadRaceAppliesTo(e))
                .GroupBy(rat => rat.Caste)
                .ToDictionary(g => g.Key, g => g.First());

            _rebellionProgressRate = _xmlReaderUtil.AttributeAsDouble(element, "progress_rate");

            _injurySkill = _xmlReaderUtil.ObjectReferenceLookup(
                element,
                "injury_skill",
                _gamedataTracker.Skills,
                s => s.Id);
        }

        private RebellionAppliesTo LoadRaceAppliesTo(XElement element)
        {
            var race = _xmlReaderUtil.ObjectReferenceLookup(
                element,
                "race",
                _gamedataTracker.Races,
                r => r.Id);

            var caste = _xmlReaderUtil.ObjectReferenceLookup(
                element,
                "caste",
                race.Castes,
                c => c.Id);

            var banditTypes = element.Elements("bandit_type")
                .Select(e =>
                {
                    var unitType = _xmlReaderUtil.ObjectReferenceLookup(
                        e,
                        "unit_type",
                        _gamedataTracker.UnitTypes,
                        u => u.Id);

                    var equipmentSet = _xmlReaderUtil.ObjectReferenceLookup(
                        e,
                        "equipment_level",
                        unitType.EquipmentSets,
                        es => es.Level.ToString());

                    return new RebellionBanditType
                    {
                        UnitType = unitType,
                        EquipmentSet = equipmentSet,
                        PopPerUnit = _xmlReaderUtil.AttributeAsDouble(e, "pop_per_unit")
                    };
                })
                .ToArray();

            var unrestDebuff = new IntIndexableLookup<BonusType, double>();
            foreach(var debuffElement in element.Elements("unrest_debuff"))
            {
                var bonusType = _xmlReaderUtil.ObjectReferenceLookup(
                    debuffElement,
                    "bonus",
                    _gamedataTracker.BonusTypes,
                    bt => bt.Id);

                var value = _xmlReaderUtil.AttributeAsDouble(debuffElement, "value");

                unrestDebuff[bonusType] += value;
            }

            return new RebellionAppliesTo
            {
                Caste = caste,
                BanditTypes = banditTypes,
                BanditControllerType = _xmlReaderUtil.ObjectReferenceLookup(
                    element,
                    "bandit_controller_type",
                    _gamedataTracker.ControllerTypes,
                    b => b.Id),
                UnrestDebuff = unrestDebuff
            };
        }

        public void LoadCustomData(SerializedObject worldRoot)
        {
            var serviceRoot = worldRoot.GetOptionalChild("peasant_rebellions_events");

            if (serviceRoot != null)
            {
                _rebellionProgress = serviceRoot.GetChild("rebellion_progress").GetStringDoubleDictionary();
                _eventTimers = serviceRoot.GetChild("event_timers").GetStringDoubleDictionary();
            }
        }

        public void PostLoad(XElement element)
        {
        }

        public void SaveCustomData(SerializedObject worldRoot)
        {
            var serviceRoot = worldRoot.CreateChild("peasant_rebellions_events");

            serviceRoot.CreateChild("rebellion_progress").SetStringDoubleDictionary(_rebellionProgress);
            serviceRoot.CreateChild("event_timers").SetStringDoubleDictionary(_eventTimers);
        }

        public void StepSimulation(double elapsedMonths)
        {
            if (!_worldManager.Tribes.Any())
            {
                return;
            }

            if (_nextTribe < _worldManager.Tribes.Count)
            {
                if (_nextSettlement < _worldManager.Tribes[_nextTribe].Settlements.Count)
                {
                    var settlement = _worldManager.Tribes[_nextTribe].Settlements[_nextSettlement];
                    _nextSettlement += 1;

                    if (!_disabledSettlements.Contains(settlement.Id))
                    {
                        var deltaMonths = 0.0;
                        if (_lastCheckMonth.ContainsKey(settlement.Id))
                        {
                            deltaMonths = _worldManager.Month - _lastCheckMonth[settlement.Id];
                            _lastCheckMonth[settlement.Id] = _worldManager.Month;
                        }
                        else
                        {
                            _lastCheckMonth.Add(settlement.Id, _worldManager.Month);
                        }

                        foreach (var caste in _casteRebellionInfo.Keys)
                        {
                            if (settlement.PopulationByCaste.ContainsKey(caste)
                                && settlement.PopulationByCaste[caste] > 0)
                            {
                                var key = settlement.Id + caste.Id;

                                if (!_rebellionProgress.ContainsKey(key))
                                {
                                    _rebellionProgress.Add(key, 0);
                                    _eventTimers.Add(key, 0);
                                }

                                var targetLevel = 1 - settlement.SatisfactionByCaste[caste];

                                var currentLevel = _rebellionProgress[key];
                                var newLevel = 0.0;
                                var settlementPos = settlement.EconomicActors
                                    .OfType<ICellDevelopment>()
                                    .Where(cd => cd is DistrictDevelopment || cd is VillageDevelopment)
                                    .Select(cd => cd.Cell.Position)
                                    .FirstOrDefault();
                                
                                if (targetLevel < currentLevel)
                                {
                                    newLevel = Math.Max(targetLevel, currentLevel - deltaMonths * _rebellionProgressRate);
                                }
                                else if (targetLevel > currentLevel)
                                {
                                    newLevel = Math.Min(targetLevel, currentLevel + deltaMonths * _rebellionProgressRate);
                                }
                                else
                                {
                                    newLevel = currentLevel;
                                }

                                _rebellionProgress[key] = newLevel;

                                var unrestDebuff = settlement.Buffs
                                    .FirstOrDefault(b => b.Id == "buff_peasant_rebellions");
                                if(newLevel >= THRESHOLD_WARNING && unrestDebuff == null )
                                {
                                    unrestDebuff = new Buff
                                    {
                                        Id = "buff_peasant_rebellions",
                                        IconKey = "buff_peasant_rebellions",
                                        Name = "Unrest",
                                        Description = "The settlement is suffering from unrest.",
                                        AppliedBonuses = new IntIndexableLookup<BonusType, double>(),
                                        ExpireMonth = -1
                                    };

                                    settlement.AddBuff(unrestDebuff);
                                }
                                else if(newLevel < THRESHOLD_WARNING && unrestDebuff != null)
                                {
                                    settlement.RemoveBuff(unrestDebuff);
                                }

                                if(unrestDebuff != null)
                                {
                                    foreach(var bonusType in _casteRebellionInfo[caste].UnrestDebuff.Keys)
                                    {
                                        unrestDebuff.AppliedBonuses[bonusType] = _casteRebellionInfo[caste].UnrestDebuff[bonusType] * newLevel;
                                    }
                                }

                                if (newLevel >= THRESHOLD_WARNING && currentLevel < THRESHOLD_WARNING)
                                {
                                    if (settlement.Owner == _playerData.PlayerTribe)
                                    {
                                        _notificationsSystem.AddNotification(
                                            _genericNotificationBuilder.BuildSimpleNotification(
                                                "ui_notification_unrest",
                                                $"Unrest in {settlement.Name}",
                                                $"{caste.Name} unrest is growing in {settlement.Name}.",
                                                $"Unrest in {settlement.Name}",
                                                $"{caste.Name} unrest is growing in {settlement.Name}. You need to take action quickly, before things get out of hand.",
                                                settlementPos));
                                    }
                                }

                                if (currentLevel > THRESHOLD_MINOR)
                                {
                                    var localArmies = settlement.Owner.Armies
                                        .Where(a => a.Cell.Position == settlementPos)
                                        .ToArray();
                                    
                                    _eventTimers[key] -= deltaMonths;

                                    if (_eventTimers[key] <= 0)
                                    {
                                        _eventTimers[key] = 1.5;

                                        var roll = _random.NextDouble();

                                        if (currentLevel >= THRESHOLD_MAJOR)
                                        {
                                            if (settlement.IsUnderSiege)
                                            {
                                                DoSiegeSurreder(settlement, settlementPos);
                                            }
                                            else
                                            {
                                                if (roll > 0.75 && (settlement == settlement.Owner.Capital || settlement.Governor != null))
                                                {
                                                    DoCharacterAssault(settlement, settlementPos, localArmies);
                                                }
                                                else if (roll > 0.5)
                                                {
                                                    DoPopAssault(settlement, settlementPos);
                                                }
                                                else if (roll > 0.25)
                                                {
                                                    DoArmyAssault(settlement, settlementPos, localArmies);
                                                }
                                                else
                                                {
                                                    DoBandits(settlement, settlementPos, _casteRebellionInfo[caste]);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (roll > 0.75 && (settlement == settlement.Owner.Capital || settlement.Governor != null))
                                            {
                                                DoCharacterAssault(settlement, settlementPos, localArmies);
                                            }
                                            else if (roll > 0.5)
                                            {
                                                DoPopAssault(settlement, settlementPos);
                                            }
                                            else if (roll > 0.25)
                                            {
                                                DoArmyAssault(settlement, settlementPos, localArmies);
                                            }
                                            else
                                            {
                                                DoBandits(settlement, settlementPos, _casteRebellionInfo[caste]);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    _eventTimers[key] = 0;
                                }
                            }
                        }
                    }
                }
                else
                {
                    _nextTribe += 1;
                    _nextSettlement = 0;
                }
            }
            else
            {
                _nextTribe = 0;
                _nextSettlement = 0;
            }
        }

        private void DoSiegeSurreder(Settlement settlement, CellPosition settlementPos)
        {
            var newOwner = settlement.SiegedBy.FirstOrDefault()?.Owner;

            if (newOwner != null)
            {
                if (settlement.Owner == _playerData.PlayerTribe)
                {
                    _notificationsSystem.AddNotification(
                        _genericNotificationBuilder.BuildSimpleNotification(
                            "ui_notification_siege_surrender",
                            $"{settlement.Name} Has Surrendered!",
                            $"{settlement.Name} has surrendered to the besieging armies.",
                            $"{settlement.Name} Has Surrendered!",
                            $"The people of {settlement.Name} have surrendered, opening the city to the besieging armies of {newOwner.Name}. Thanks to their weak wills we will have to recapture the city ourselves.",
                            settlementPos));
                }
                else if (newOwner == _playerData.PlayerTribe)
                {
                    _notificationsSystem.AddNotification(
                        _genericNotificationBuilder.BuildSimpleNotification(
                            "ui_notification_siege_surrended_to",
                            $"{settlement.Name} Captured!",
                            $"{settlement.Name} has surrendered to us.",
                            $"{settlement.Name} Captured!",
                            $"Our siege of {settlement.Name} has succeeded, abandoned by their lords the people have opened the city to our righteous armies.",
                            settlementPos));
                }

                _simulationEventBus.SendEvent(new SettlementOwnerChangedSimulationEvent
                {
                    OldOwner = settlement.Owner,
                    NewOwner = newOwner,
                    Settlement = settlement
                });
                settlement.ChangeOwner(newOwner);
            }
        }

        private void DoPopAssault(Settlement settlement, CellPosition settlementPos)
        {
            if (settlement.Population.Any())
            {
                var count = Math.Max(1, _random.Next(2, 5) / 100.0 * settlement.Population.Count);
                var victims = new List<Pop>();

                for (var i = 0; i < count && settlement.Population.Count > 1; i++)
                {
                    var victim = _random.Choose(settlement.Population);
                    settlement.Population.Remove(victim);
                    victims.Add(victim);
                }

                settlement.SetCalculationFlag("pop murders");

                if (_playerData.PlayerTribe == settlement.Owner)
                {
                    var body = $"The unrest in {settlement.Name} is starting to turn violent. The most recent incident has been several murders.";
                    body += $"\nVictims:";

                    foreach (var group in victims.GroupBy(p => p.Caste))
                    {
                        if (group.Count() > 1)
                        {
                            body += $"\n{group.Count()} {group.Key.NamePlural}";
                        }
                        else
                        {
                            body += $"\n1 {group.Key.Name}";
                        }
                    }

                    _notificationsSystem.AddNotification(
                        _genericNotificationBuilder.BuildSimpleNotification(
                            "ui_notification_murders",
                            $"Murders in {settlement.Name}",
                            $"There have been a series of murders in {settlement.Name}.",
                            $"Murders in {settlement.Name}",
                            body,
                            settlementPos));
                }
            }
        }

        private void DoArmyAssault(Settlement settlement, CellPosition settlementPos, Army[] localArmies)
        {
            if (localArmies.Any())
            {
                var targetArmy = _random.Choose(localArmies);

                if(targetArmy.Units.Any())
                {
                    var targetUnit = _random.Choose(targetArmy.Units);

                    targetUnit.Health = targetUnit.MaxHealth * (targetUnit.HealthPercent - _random.Next(50, 100) / 100.0);

                    if(targetUnit.Health <= 0)
                    {
                        _unitHelper.RemoveUnit(targetArmy, targetUnit, removedViolently: true);
                    }

                    if (_playerData.PlayerTribe == settlement.Owner)
                    {
                        var body = $"The unrest in {settlement.Name} is starting to turn violent. In the most recent incident some of your soldiers have been injured.";
                        body += $"\nSome of your {targetUnit.Name} in {targetArmy.Name} have been injured.";
                        
                        _notificationsSystem.AddNotification(
                            _genericNotificationBuilder.BuildSimpleNotification(
                                "ui_notification_murders",
                                $"Soldiers Assaulted in {settlement.Name}",
                                $"Some soldiers in {settlement.Name} have been assaulted.",
                                $"Soldiers Assaulted in {settlement.Name}",
                                body,
                                settlementPos));
                    }
                }
            }
        }

        private void DoCharacterAssault(Settlement settlement, CellPosition settlementPos, Army[] localArmies)
        {
            var potentialTargets = new List<NotablePerson>();

            if (settlement.Governor != null)
            {
                potentialTargets.Add(settlement.Governor);
                if (settlement.Governor.Spouse != null && settlement.Governor.Spouse.IsIdle)
                {
                    potentialTargets.Add(settlement.Governor.Spouse);
                }
                potentialTargets.AddRange(settlement.Governor.Children.Where(c => c.IsIdle || !c.IsMature));
            }

            if (settlement == settlement.Owner.Capital)
            {
                potentialTargets.Add(settlement.Owner.Ruler);
                if (settlement.Owner.Ruler.Spouse != null && settlement.Owner.Ruler.Spouse.IsIdle)
                {
                    potentialTargets.Add(settlement.Owner.Ruler.Spouse);
                }
                potentialTargets.AddRange(settlement.Owner.Ruler.Children.Where(c => c.IsIdle || !c.IsMature));

                potentialTargets.AddRange(settlement.Owner.NotablePeople.Where(c => c.IsIdle || !c.IsMature));
            }

            potentialTargets.AddRange(localArmies.Select(a => a.General));

            potentialTargets.RemoveAll(np => np == null || np.IsDead);

            if (potentialTargets.Any())
            {
                var target = _random.Choose(potentialTargets);

                var skillPlacement = _peopleHelper.IncrementOrAdd(target, _injurySkill);

                if ((skillPlacement?.CanIncrement() ?? false) && _random.NextDouble() < 0.5)
                {
                    skillPlacement = _peopleHelper.IncrementOrAdd(target, _injurySkill);

                    if ((skillPlacement?.CanIncrement() ?? false) && _random.NextDouble() < 0.5)
                    {
                        skillPlacement = _peopleHelper.IncrementOrAdd(target, _injurySkill);
                    }
                }

                if (skillPlacement != null)
                {
                    var doDeath = _random.NextDouble() <= target.GetMinorStat("Death Chance");

                    if (doDeath)
                    {
                        _peopleHelper.Kill(target);
                    }
                    else if (_playerData.PlayerTribe == settlement.Owner)
                    {
                        _notificationsSystem.AddNotification(_genericNotificationBuilder
                            .BuildPersonSkillNotification(
                                "ui_notification_assault",
                                $"{target.Name} Injured",
                                $"{target.Name} has been injured by an attack in {settlement.Name}.",
                                $"{target.Name} Injured",
                                $"The unrest in {settlement.Name} is starting to turn violent. {target.Name} has been injured in the most recent incident.",
                                target,
                                skillPlacement,
                                settlementPos));
                    }
                }
            }
        }

        private void DoBandits(Settlement settlement, CellPosition settlementPos, RebellionAppliesTo rebellionInfo)
        {
            var potentials = settlement.Region.Cells
                .Where(c => rebellionInfo.BanditTypes.Any(bt => bt.EquipmentSet.CanPath(c)))
                .ToArray();

            if (potentials.Any())
            {
                var unitsToSpawn = new List<RebellionBanditType>();

                foreach (var banditType in rebellionInfo.BanditTypes)
                {
                    var count = banditType.PopPerUnit / (double)settlement.PopulationByCaste[rebellionInfo.Caste];

                    for (var i = 0; i < count; i++)
                    {
                        unitsToSpawn.Add(banditType);
                    }
                }

                _random.Shuffle(unitsToSpawn);

                var banditTribe = _worldManager.Tribes
                    .Where(t => t.Name == rebellionInfo.Caste.Race.Name + " Bandits")
                    .FirstOrDefault();

                if (banditTribe == null)
                {
                    banditTribe = new Tribe
                    {
                        Race = rebellionInfo.Caste.Race,
                        PrimaryColor = _random.Choose(_tribeColors),
                        SecondaryColor = _random.Choose(_tribeColors),
                        Name = rebellionInfo.Caste.Race.Name + " Bandits",
                        IsBandits = true
                    };
                    _worldManager.Tribes.Add(banditTribe);

                    banditTribe.Controller = rebellionInfo.BanditControllerType.BuildController(banditTribe, _random, _worldManager.Hostility, _worldManager.Month);
                }

                var unitsPlaced = false;

                var army = _unitHelper.CreateArmy(banditTribe, _random.Choose(potentials));

                foreach (var unitToSpawn in unitsToSpawn)
                {
                    if (army.IsFull)
                    {
                        army = _unitHelper.CreateArmy(banditTribe, _random.Choose(potentials));
                    }

                    if (unitToSpawn.EquipmentSet.CanPath(army.Cell))
                    {
                        army.Units.Add(unitToSpawn.UnitType.CreateUnit(unitToSpawn.EquipmentSet));
                        unitsPlaced = true;
                    }
                }

                if (unitsPlaced && _playerData.PlayerTribe == settlement.Owner)
                {
                    _notificationsSystem.AddNotification(
                        _genericNotificationBuilder.BuildSimpleNotification(
                            "ui_notification_bandits",
                            $"Bandits in {settlement.Name}",
                            $"Bandits have been spotted near {settlement.Name}.",
                            $"Bandits in {settlement.Name}",
                            $"With the unrest in {settlement.Name} desperate men have taken to banditry, pillaging the countryside.",
                            settlementPos));
                }
            }
        }

        public void ToggleFor(Settlement settlement)
        {
            if (_disabledSettlements.Contains(settlement.Id))
            {
                _disabledSettlements.Remove(settlement.Id);
            }
            else
            {
                _disabledSettlements.Add(settlement.Id);
            }
        }

        public IEnumerable<string> GetInfoFor(Settlement settlement)
        {
            var results = new List<string>();

            if (_disabledSettlements.Contains(settlement.Id))
            {
                results.Add("Peasant rebellions disabled");
            }
            else
            {
                results.Add("Peasant rebellions enabled");

                foreach (var caste in _casteRebellionInfo.Keys)
                {
                    var key = settlement.Id + caste.Id;

                    if (settlement.PopulationByCaste.ContainsKey(caste)
                        && settlement.PopulationByCaste[caste] > 0)
                    {
                        results.Add($"{caste.Name} population {settlement.PopulationByCaste[caste]}");
                        results.Add($"Rebellion progress {_rebellionProgress[key]}");
                        results.Add($"Event timer {_eventTimers[key]}");
                    }
                }
            }

            return results;
        }

        private class RebellionAppliesTo
        {
            public Caste Caste { get; set; }
            public RebellionBanditType[] BanditTypes { get; set; }
            public ITribeControllerType BanditControllerType { get; set; }
            public IntIndexableLookup<BonusType, double> UnrestDebuff { get; set; }
        }

        private class RebellionBanditType
        {
            public IUnitType UnitType { get; set; }
            public UnitEquipmentSet EquipmentSet { get; set; }
            public double PopPerUnit { get; set; }
        }
    }
}
