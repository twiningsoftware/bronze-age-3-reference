﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Bronze.Common.Data.World.CellDevelopments;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Diplomacy;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.Simulation.Events;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.Simulation.SettlementEvents
{
    public class NobleRebellionsEvent : ISubSimulator, IWorldStateService, IDataLoader, IRebellionEventTrigger
    {
        public const double THRESHOLD_WARNING = 0.2;
        public const double THRESHOLD_WARNING2 = 0.35;
        public const double THRESHOLD_REVOLT = 0.5;

        public string ElementName => "noble_rebellions_events";

        private readonly IXmlReaderUtil _xmlReaderUtil;
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IWorldManager _worldManager;
        private readonly IGenericNotificationBuilder _genericNotificationBuilder;
        private readonly INotificationsSystem _notificationsSystem;
        private readonly IPlayerDataTracker _playerData;
        private readonly IPeopleHelper _peopleHelper;
        private readonly IUnitHelper _unitHelper;
        private readonly IAiHelper _aiHelper;
        private readonly INameSource _nameSource;
        private readonly ISimulationEventBus _simulationEventBus;

        private Random _random;
        private readonly BronzeColor[] _tribeColors;
        private Dictionary<Caste, RebellionAppliesTo> _casteRebellionInfo;
        private Dictionary<string, double> _rebellionProgress;
        private Dictionary<string, double> _lastCheckMonth;
        private List<string> _disabledSettlements;
        private int _nextTribe;
        private int _nextSettlement;
        private double _rebellionProgressRate;

        public NobleRebellionsEvent(
            IGamedataTracker gamedataTracker,
            IXmlReaderUtil xmlReaderUtil,
            IWorldManager worldManager,
            IGenericNotificationBuilder genericNotificationBuilder,
            INotificationsSystem notificationsSystem,
            IPlayerDataTracker playerData,
            IPeopleHelper peopleHelper,
            IUnitHelper unitHelper,
            IAiHelper aiHelper,
            INameSource nameSource,
            ISimulationEventBus simulationEventBus)
        {
            _gamedataTracker = gamedataTracker;
            _xmlReaderUtil = xmlReaderUtil;
            _worldManager = worldManager;
            _genericNotificationBuilder = genericNotificationBuilder;
            _notificationsSystem = notificationsSystem;
            _playerData = playerData;
            _peopleHelper = peopleHelper;
            _unitHelper = unitHelper;
            _aiHelper = aiHelper;
            _nameSource = nameSource;
            _simulationEventBus = simulationEventBus;

            _casteRebellionInfo = new Dictionary<Caste, RebellionAppliesTo>();
            _rebellionProgress = new Dictionary<string, double>();
            _lastCheckMonth = new Dictionary<string, double>();
            _random = new Random();
            _disabledSettlements = new List<string>();

            _tribeColors = Enum.GetValues(typeof(BronzeColor))
                .Cast<BronzeColor>()
                .Where(c => c != BronzeColor.None)
                .ToArray();
        }

        public void Clear()
        {
            _rebellionProgress.Clear();
            _nextTribe = 0;
            _nextSettlement = 0;
            _disabledSettlements.Clear();
        }

        public void Initialize(WorldState worldState)
        {
            _random = new Random(worldState.Seed.GetHashCode());
        }

        public void Load(XElement element)
        {
            _casteRebellionInfo = element.Elements("applies_to")
                .Select(e => LoadRaceAppliesTo(e))
                .GroupBy(rat => rat.Caste)
                .ToDictionary(g => g.Key, g => g.First());

            _rebellionProgressRate = _xmlReaderUtil.AttributeAsDouble(element, "progress_rate");
        }

        private RebellionAppliesTo LoadRaceAppliesTo(XElement element)
        {
            var race = _xmlReaderUtil.ObjectReferenceLookup(
                element,
                "race",
                _gamedataTracker.Races,
                r => r.Id);

            var caste = _xmlReaderUtil.ObjectReferenceLookup(
                element,
                "caste",
                race.Castes,
                c => c.Id);

            return new RebellionAppliesTo
            {
                Caste = caste,
                RebelControllerType = _xmlReaderUtil.ObjectReferenceLookup(
                    element,
                    "rebel_controller_type",
                    _gamedataTracker.ControllerTypes,
                    b => b.Id)
            };
        }

        public void LoadCustomData(SerializedObject worldRoot)
        {
            var serviceRoot = worldRoot.GetOptionalChild("noble_rebellions_events");

            if (serviceRoot != null)
            {
                _rebellionProgress = serviceRoot.GetChild("rebellion_progress").GetStringDoubleDictionary();
            }
        }

        public void PostLoad(XElement element)
        {
        }

        public void SaveCustomData(SerializedObject worldRoot)
        {
            var serviceRoot = worldRoot.CreateChild("noble_rebellions_events");

            serviceRoot.CreateChild("rebellion_progress").SetStringDoubleDictionary(_rebellionProgress);
        }

        public void StepSimulation(double elapsedMonths)
        {
            if (!_worldManager.Tribes.Any())
            {
                return;
            }

            if (_nextTribe < _worldManager.Tribes.Count)
            {
                if (_nextSettlement < _worldManager.Tribes[_nextTribe].Settlements.Count)
                {
                    var settlement = _worldManager.Tribes[_nextTribe].Settlements[_nextSettlement];
                    _nextSettlement += 1;

                    if (!_disabledSettlements.Contains(settlement.Id))
                    {
                        var deltaMonths = 0.0;
                        if (_lastCheckMonth.ContainsKey(settlement.Id))
                        {
                            deltaMonths = _worldManager.Month - _lastCheckMonth[settlement.Id];
                            _lastCheckMonth[settlement.Id] = _worldManager.Month;
                        }
                        else
                        {
                            _lastCheckMonth.Add(settlement.Id, _worldManager.Month);
                        }

                        foreach (var caste in _casteRebellionInfo.Keys)
                        {
                            if (settlement.PopulationByCaste.ContainsKey(caste)
                                && settlement.PopulationByCaste[caste] > 0)
                            {
                                var key = settlement.Id + caste.Id;

                                if (!_rebellionProgress.ContainsKey(key))
                                {
                                    _rebellionProgress.Add(key, 0);
                                }

                                var targetLevel = 1 - settlement.SatisfactionByCaste[caste];

                                var currentLevel = _rebellionProgress[key];
                                var newLevel = 0.0;
                                var settlementPos = settlement.EconomicActors
                                    .OfType<ICellDevelopment>()
                                    .Where(cd => cd is DistrictDevelopment || cd is VillageDevelopment)
                                    .Select(cd => cd.Cell.Position)
                                    .FirstOrDefault();

                                if (targetLevel < currentLevel)
                                {
                                    newLevel = Math.Max(targetLevel, currentLevel - deltaMonths * _rebellionProgressRate);
                                }
                                else if (targetLevel > currentLevel)
                                {
                                    newLevel = Math.Min(targetLevel, currentLevel + deltaMonths * _rebellionProgressRate);
                                }
                                else
                                {
                                    newLevel = currentLevel;
                                }

                                _rebellionProgress[key] = newLevel;

                                if (newLevel >= THRESHOLD_WARNING && currentLevel < THRESHOLD_WARNING)
                                {
                                    if (settlement.Owner == _playerData.PlayerTribe)
                                    {
                                        _notificationsSystem.AddNotification(
                                            _genericNotificationBuilder.BuildSimpleNotification(
                                                "ui_notification_noble_unrest",
                                                $"Unrest in {settlement.Name}",
                                                $"{caste.Name} unrest is growing in {settlement.Name}.",
                                                $"Unrest in {settlement.Name}",
                                                $"{caste.Name} unrest is growing in {settlement.Name}. You need to take action quickly, before things get out of hand.",
                                                settlementPos));
                                    }
                                }

                                if (newLevel >= THRESHOLD_WARNING2 && currentLevel < THRESHOLD_WARNING2)
                                {
                                    if (settlement.Owner == _playerData.PlayerTribe)
                                    {
                                        _notificationsSystem.AddNotification(
                                            _genericNotificationBuilder.BuildSimpleNotification(
                                                "ui_notification_noble_unrest",
                                                $"Rebellious Rumors in {settlement.Name}",
                                                $"{caste.Name} unrest is growing in {settlement.Name}.",
                                                $"Rebellious Rumors in {settlement.Name}",
                                                $"The unrest in {settlement.Name} is growing worse. The {caste.NamePlural.ToLower()} are whispering that they would do better without your rule.",
                                                settlementPos));
                                    }
                                }

                                if (currentLevel >= THRESHOLD_REVOLT)
                                {
                                    if (settlement.IsUnderSiege)
                                    {
                                        DoSiegeSurrender(settlement, settlementPos);
                                    }
                                    else
                                    {
                                        DoRevolt(settlement, settlementPos, _casteRebellionInfo[caste]);
                                    }

                                    _rebellionProgress[key] = 0;
                                }
                            }
                        }
                    }
                }
                else
                {
                    _nextTribe += 1;
                    _nextSettlement = 0;
                }
            }
            else
            {
                _nextTribe = 0;
                _nextSettlement = 0;
            }
        }

        private void DoSiegeSurrender(Settlement settlement, CellPosition settlementPos)
        {
            var newOwner = settlement.SiegedBy.FirstOrDefault()?.Owner;

            if (newOwner != null)
            {
                if (settlement.Owner == _playerData.PlayerTribe)
                {
                    _notificationsSystem.AddNotification(
                        _genericNotificationBuilder.BuildSimpleNotification(
                            "ui_notification_siege_surrender",
                            $"{settlement.Name} Has Surrendered!",
                            $"{settlement.Name} has surrendered to the besieging armies.",
                            $"{settlement.Name} Has Surrendered!",
                            $"The people of {settlement.Name} have surrendered, opening the city to the besieging armies of {newOwner.Name}. Thanks to their weak wills we will have to recapture the city ourselves.",
                            settlementPos));
                }
                else if (newOwner == _playerData.PlayerTribe)
                {
                    _notificationsSystem.AddNotification(
                        _genericNotificationBuilder.BuildSimpleNotification(
                            "ui_notification_siege_surrended_to",
                            $"{settlement.Name} Captured!",
                            $"{settlement.Name} has surrendered to us.",
                            $"{settlement.Name} Captured!",
                            $"Our siege of {settlement.Name} has succeeded, abandoned by their lords the people have opened the city to our righteous armies.",
                            settlementPos));
                }

                _simulationEventBus.SendEvent(new SettlementOwnerChangedSimulationEvent
                {
                    OldOwner = settlement.Owner,
                    NewOwner = newOwner,
                    Settlement = settlement
                });
                settlement.ChangeOwner(newOwner);
            }
        }

        private void DoRevolt(Settlement settlement, CellPosition settlementPos, RebellionAppliesTo rebellionInfo)
        {
            var potentialUnits = new List<Tuple<IUnitType, UnitEquipmentSet>>();

            foreach (var category in settlement.RecruitmentSlots.Where(kvp => kvp.Value >= 1).Select(kvp => kvp.Key).ToArray())
            {
                potentialUnits.AddRange(settlement.Race
                    .AvailableUnits
                    .Concat(settlement.EconomicActors.OfType<IRecruitableUnitTypeSource>().SelectMany(s => s.ProvidedUnitTypes))
                    .Where(u => u.RecruitmentCategory == category)
                    .SelectMany(u => u.EquipmentSets.Select(e => Tuple.Create(u, e)))
                    .Where(t => t.Item2.StrengthEstimate > 0));
            }

            var armiesToClaim = settlement.Owner.Armies
                .Where(a => a.Cell.Region == settlement.Region && a.General == null)
                .ToList();

            var traitorArmies = armiesToClaim.Count();

            if (potentialUnits.Any() || armiesToClaim.Any())
            {
                var rebelTribe = new Tribe
                {
                    Race = rebellionInfo.Caste.Race,
                    PrimaryColor = _random.Choose(_tribeColors),
                    SecondaryColor = _random.Choose(_tribeColors),
                    Name = _nameSource.MakeTribeName(rebellionInfo.Caste.Race)
                };
                _worldManager.Tribes.Add(rebelTribe);
                rebelTribe.Controller = rebellionInfo.RebelControllerType.BuildController(rebelTribe, _random, _worldManager.Hostility, _worldManager.Month);
                
                if (potentialUnits.Any())
                {
                    var potentialCells = settlement.EconomicActors
                        .OfType<ICellDevelopment>()
                        .Where(cd => cd is VillageDevelopment || cd is DistrictDevelopment)
                        .Select(cd => cd.Cell)
                        .SelectMany(c => c.OrthoNeighbors)
                        .Where(c => potentialUnits.Any(t => t.Item2.CanPath(c)))
                        .ToArray();

                    if (potentialCells.Any())
                    {
                        var newArmy = _unitHelper.CreateArmy(
                            settlement.Owner,
                            _random.Choose(potentialCells));

                        _aiHelper.RecruitWarriors(settlement, newArmy);

                        armiesToClaim.Add(newArmy);
                    }
                }

                foreach (var army in armiesToClaim)
                {
                    army.ChangeOwner(rebelTribe);
                    army.CurrentOrder = null;
                }

                if (armiesToClaim.Any())
                {
                    rebelTribe.Controller.SetStrategicTarget(new StrategicTarget
                    {
                        Goal = "annex",
                        TakeByForce = true,
                        TargetRegion = settlement.Region
                    });
                    
                    rebelTribe.Race.StartingPeopleScheme
                        .CreateInitialRulers(_random, rebelTribe);

                    rebelTribe.Ruler = rebelTribe.NotablePeople.OrderBy(np => np.BirthDate).FirstOrDefault();

                    var armiesInOrder = rebelTribe.Armies
                        .OrderByDescending(a => a.StrengthEstimate)
                        .ToArray();
                    var generalsInOrder = rebelTribe.NotablePeople
                        .Where(np => np.IsMature)
                        .OrderByDescending(np => np.GetStat(NotablePersonStatType.Valor))
                        .ToArray();

                    for(var i = 0; i < armiesInOrder.Length && i < generalsInOrder.Length; i++)
                    {
                        generalsInOrder[i].AssignTo(armiesInOrder[i]);
                    }

                    if (_playerData.PlayerTribe == settlement.Owner)
                    {
                        var body = $"The ungrateful {rebellionInfo.Caste.NamePlural} in {settlement.Name} have revolted";
                        if (traitorArmies > 0)
                        {
                            body += $", suborning {traitorArmies} of your armies in the process.";
                        }
                        else
                        {
                            body += ".";
                        }

                        _notificationsSystem.AddNotification(
                            _genericNotificationBuilder.BuildSimpleNotification(
                                "ui_notification_rebellion",
                                $"Rebellion in {settlement.Name}",
                                $"The ungrateful {rebellionInfo.Caste.NamePlural} in {settlement.Name} have revolted!",
                                $"Rebellion in {settlement.Name}",
                                body,
                                settlementPos,
                                autoOpen: true));
                    }
                }
            }
        }

        public void ToggleFor(Settlement settlement)
        {
            if (_disabledSettlements.Contains(settlement.Id))
            {
                _disabledSettlements.Remove(settlement.Id);
            }
            else
            {
                _disabledSettlements.Add(settlement.Id);
            }
        }

        public IEnumerable<string> GetInfoFor(Settlement settlement)
        {
            var results = new List<string>();

            if (_disabledSettlements.Contains(settlement.Id))
            {
                results.Add("Noble rebellions disabled");
            }
            else
            {
                results.Add("Noble rebellions enabled");

                foreach (var caste in _casteRebellionInfo.Keys)
                {
                    var key = settlement.Id + caste.Id;

                    if (settlement.PopulationByCaste.ContainsKey(caste)
                        && settlement.PopulationByCaste[caste] > 0)
                    {
                        results.Add($"{caste.Name} population {settlement.PopulationByCaste[caste]}");
                        results.Add($"Rebellion progress {_rebellionProgress[key]}");
                    }
                }
            }

            return results;
        }

        private class RebellionAppliesTo
        {
            public Caste Caste { get; set; }
            public ITribeControllerType RebelControllerType { get; set; }
        }
    }
}
