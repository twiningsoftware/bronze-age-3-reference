﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System.Linq;

namespace Bronze.Common.Simulation
{
    public class SettlementTraitMonitor : ISubSimulator, IWorldStateService
    {
        public const string CAPITAL_TRAIT_ID = "trait_is_capital";

        private readonly IGamedataTracker _gamedataTracker;
        private readonly ILogger _logger;
        private readonly IWorldManager _worldManager;

        private Trait _capitalTrait;

        public SettlementTraitMonitor(
            IGamedataTracker gamedataTracker,
            ILogger logger,
            IWorldManager worldManager)
        {
            _gamedataTracker = gamedataTracker;
            _logger = logger;
            _worldManager = worldManager;
        }

        public void Clear()
        {
        }

        public void Initialize(WorldState worldState)
        {
            _capitalTrait = _gamedataTracker
                .Traits
                .Where(t => t.Id == CAPITAL_TRAIT_ID)
                .FirstOrDefault();

            if(_capitalTrait == null)
            {
                _logger.Error($"Error: could not find capital trait {CAPITAL_TRAIT_ID}");
            }
        }

        public void LoadCustomData(SerializedObject worldRoot)
        {
        }

        public void SaveCustomData(SerializedObject worldRoot)
        {
        }

        public void StepSimulation(double elapsedMonths)
        {
            foreach(var tribe in _worldManager.Tribes)
            {
                foreach(var settlement in tribe.Settlements)
                {
                    if(tribe.Capital == settlement
                        && !settlement.Traits.Contains(_capitalTrait))
                    {
                        settlement.Traits.Add(_capitalTrait);
                        RecalculateTraits(settlement);
                    }

                    if (tribe.Capital != settlement
                        && settlement.Traits.Contains(_capitalTrait))
                    {
                        settlement.Traits.Remove(_capitalTrait);
                        RecalculateTraits(settlement);
                    }
                }
            }
        }

        private void RecalculateTraits(Settlement settlement)
        {
            foreach(var cell in settlement.Region.Cells)
            {
                cell.CalculateTraits();
            }

            foreach(var district in settlement.Districts.Where(d=>d.IsGenerated))
            {
                foreach(var tile in district.Tiles)
                {
                    tile.CalculateTraits();
                }
            }
        }
    }
}
