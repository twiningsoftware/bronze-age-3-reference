﻿using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.Simulation.Events;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Bronze.Common.Simulation.NotablePersonEvents
{
    public class NotablePersonSpousegenEvent : ISubSimulator, IWorldStateService, IDataLoader
    {
        public string ElementName => "notable_person_spousegen_event";

        private readonly IWorldManager _worldManager;
        private readonly IPeopleHelper _peopleHelper;
        private readonly INotificationsSystem _notificationsSystem;
        private readonly IGenericNotificationBuilder _genericNotificationBuilder;
        private readonly IPlayerDataTracker _playerData;
        private readonly IPeopleGenerator _peopleGenerator;
        private readonly IXmlReaderUtil _xmlReaderUtil;
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IMarriageOptionNotificationBuilder _marriageOptionNotificationBuilder;
        private readonly ISimulationEventBus _simulationEventBus;

        private Random _random;
        private Dictionary<Race, PerRaceParameters> _perRaceParameters;
        private Dictionary<Tribe, double> _nextAskForTribe;

        public NotablePersonSpousegenEvent(
            IWorldManager worldManager,
            IPeopleHelper peopleHelper,
            INotificationsSystem notificationsSystem,
            IGenericNotificationBuilder genericNotificationBuilder,
            IPlayerDataTracker playerData,
            IPeopleGenerator peopleGenerator,
            IXmlReaderUtil xmlReaderUtil,
            IGamedataTracker gamedataTracker,
            IMarriageOptionNotificationBuilder marriageOptionNotificationBuilder,
            ISimulationEventBus simulationEventBus)
        {
            _worldManager = worldManager;
            _peopleHelper = peopleHelper;
            _notificationsSystem = notificationsSystem;
            _genericNotificationBuilder = genericNotificationBuilder;
            _playerData = playerData;
            _peopleGenerator = peopleGenerator;
            _xmlReaderUtil = xmlReaderUtil;
            _gamedataTracker = gamedataTracker;
            _marriageOptionNotificationBuilder = marriageOptionNotificationBuilder;
            _simulationEventBus = simulationEventBus;
            _random = new Random();
            _perRaceParameters = new Dictionary<Race, PerRaceParameters>();
            _nextAskForTribe = new Dictionary<Tribe, double>();

            simulationEventBus.OnSimulationEvent += SimulationEventBus_OnSimulationEvent;
        }

        public void Clear()
        {
            _nextAskForTribe.Clear();
        }

        public void Initialize(WorldState worldState)
        {
            _random = new Random(worldState.Seed.GetHashCode());
        }

        public void Load(XElement element)
        {
            foreach (var perRaceElement in element.Elements("for_race"))
            {
                var race = _xmlReaderUtil.ObjectReferenceLookup(
                    perRaceElement,
                    "race",
                    _gamedataTracker.Races,
                    r => r.Id);

                if (_perRaceParameters.ContainsKey(race))
                {
                    _perRaceParameters.Remove(race);
                }

                var warHeroElement = _xmlReaderUtil.Element(perRaceElement, "from_war_hero");

                var raceParams = new PerRaceParameters
                {
                    CanComeFromMinorHouse = perRaceElement.Element("from_minor_house") != null,
                    FromCasteParameters = perRaceElement
                        .Elements("from_caste")
                        .Select(e => new FromCasteParameters
                        {
                            BuffDurationMonths = _xmlReaderUtil.AttributeAsDouble(e, "buff_duration_months"),
                            Caste = _xmlReaderUtil.ObjectReferenceLookup(
                                e,
                                "caste",
                                race.Castes,
                                c => c.Id),
                            SourceSettlementBuff = e
                                .Elements("bonus")
                                .ToDictionary(
                                    be => _xmlReaderUtil.ObjectReferenceLookup(
                                        be,
                                        "bonus",
                                        _gamedataTracker.BonusTypes,
                                        bt => bt.Id),
                                    be => _xmlReaderUtil.AttributeAsDouble(be, "value"))
                        })
                        .ToArray(),
                    CanMarryWarHero = _xmlReaderUtil.AttributeAsBool(warHeroElement, "can_marry"),
                    CanAdoptWarHero = _xmlReaderUtil.AttributeAsBool(warHeroElement, "can_adopt"),
                    WarHeroSkills = _xmlReaderUtil.Element(warHeroElement, "bonus_skills")
                        .Elements("group")
                        .Select(g => new StartingSkillGroup
                        {
                            PickCount = _xmlReaderUtil.AttributeAsInt(g, "picks"),
                            LikelyhoodNone = _xmlReaderUtil.AttributeAsInt(g, "likelyhood_none"),
                            Options = g.Elements("starting_skill")
                                .Select(e => new LikelyhoodFor<NotablePersonSkill>
                                {
                                    Likelyhood = _xmlReaderUtil.AttributeAsInt(e, "likelyhood"),
                                    Value = _xmlReaderUtil.ObjectReferenceLookup(
                                        e,
                                        "skill",
                                        _gamedataTracker.Skills,
                                        s => s.Id)
                                })
                                .ToArray()
                        })
                        .ToArray()
                };

                _perRaceParameters.Add(race, raceParams);
            }
        }

        public void LoadCustomData(SerializedObject worldRoot)
        {
        }

        public void PostLoad(XElement element)
        {
        }

        public void SaveCustomData(SerializedObject worldRoot)
        {
        }

        public void StepSimulation(double elapsedMonths)
        {
            for (var i = 0; i < _worldManager.Tribes.Count; i++)
            {
                var tribe = _worldManager.Tribes[i];

                if (!_nextAskForTribe.ContainsKey(tribe))
                {
                    _nextAskForTribe.Add(tribe, _worldManager.Month + _random.NextDouble() * 6 + 4);
                }
                else if (_nextAskForTribe[tribe] <= _worldManager.Month && _perRaceParameters.ContainsKey(tribe.Race))
                {
                    _nextAskForTribe[tribe] = _worldManager.Month + _random.NextDouble() * 6 + 4;

                    var options = tribe.NotablePeople
                        .Where(np => np.IsMature && !np.IsDead && np.Spouse == null)
                        .ToArray();

                    if (options.Any())
                    {
                        var option = _random.Choose(options);
                        var optionAge = _worldManager.Month - option.BirthDate;

                        var raceParams = _perRaceParameters[tribe.Race];

                        if (raceParams.CanComeFromMinorHouse && (_random.NextDouble() < 0.4 || !raceParams.FromCasteParameters.Any()))
                        {
                            var spouse = GenerateSpouse(
                                tribe,
                                tribe.Race,
                                !option.IsMale,
                                Math.Max(tribe.Race.AgeMaturity, _random.Next(90, 110) / 100.0 * optionAge),
                                Enumerable.Empty<StartingSkillGroup>());

                            if (tribe == _playerData.PlayerTribe)
                            {
                                _notificationsSystem.AddNotification(
                                    _marriageOptionNotificationBuilder.BuildMarriageOptionNotification(
                                        "ui_notification_wedding",
                                        "Marriage Proposal",
                                        $"{option.Name} has recived a marriage proposal.",
                                        "Marriage Proposal",
                                        $"{option.Name} has recived a marriage proposal from {spouse.Name}, a minor noble from your kingdom. {spouse.HeSheCap} proposes marrying {option.Name} and joining your dynasty.",
                                        option,
                                        spouse,
                                        null,
                                        null));
                            }
                            else
                            {
                                tribe.NotablePeople.Add(spouse);
                                spouse.Spouse = option;
                                option.Spouse = spouse;

                                _simulationEventBus.SendEvent(new NotablePersonMarriageSimulationEvent(option, spouse));
                            }
                        }
                        else
                        {
                            var fromSettlementOptions = raceParams.FromCasteParameters
                                .SelectMany(cp => tribe.Settlements.Select(s => Tuple.Create(cp, s)))
                                .Where(t => t.Item2.PopulationByCaste.ContainsKey(t.Item1.Caste) && t.Item2.PopulationByCaste[t.Item1.Caste] > 0)
                                .ToArray();

                            if (fromSettlementOptions.Any())
                            {
                                var fromSettlement = _random.Choose(fromSettlementOptions);

                                var spouse = GenerateSpouse(
                                    tribe,
                                    tribe.Race,
                                    !option.IsMale,
                                    Math.Max(tribe.Race.AgeMaturity, _random.Next(90, 110) / 100.0 * optionAge),
                                    Enumerable.Empty<StartingSkillGroup>());

                                var settlementBuff = new Buff
                                {
                                    Id = "buff_spouse_from_settlement" + _random.Next(),
                                    IconKey = "buff_spouse_from_settlement",
                                    Name = "Unrest",
                                    Description = "The settlement is suffering from unrest.",
                                    AppliedBonuses = new IntIndexableLookup<BonusType, double>(),
                                    ExpireMonth = _worldManager.Month + fromSettlement.Item1.BuffDurationMonths
                                };

                                if (tribe == _playerData.PlayerTribe)
                                {
                                    _notificationsSystem.AddNotification(
                                        _marriageOptionNotificationBuilder.BuildMarriageOptionNotification(
                                            "ui_notification_wedding",
                                            "Marriage Proposal",
                                            $"{option.Name} has recived a marriage proposal.",
                                            "Marriage Proposal",
                                            $"{option.Name} has recived a marriage proposal from {spouse.Name}, a minor noble from your kingdom. {spouse.HeSheCap} proposes marrying {option.Name} and joining your dynasty.",
                                            option,
                                            spouse,
                                            fromSettlement.Item2,
                                            settlementBuff));
                                }
                                else
                                {
                                    tribe.NotablePeople.Add(spouse);
                                    spouse.Spouse = option;
                                    option.Spouse = spouse;

                                    _simulationEventBus.SendEvent(new NotablePersonMarriageSimulationEvent(option, spouse));

                                    fromSettlement.Item2.AddBuff(settlementBuff);
                                }
                            }
                        }
                    }
                }
            }
        }

        private void SimulationEventBus_OnSimulationEvent(ISimulationEvent evt)
        {
            if (evt is BattleOccurredSimulationEvent battleEvent)
            {
                var winners = Enumerable.Empty<Tribe>();
                var fraction = 0.0;

                if (battleEvent.BattleResult.AttackerWon)
                {
                    fraction = battleEvent.BattleResult.AttackerScore / (battleEvent.BattleResult.AttackerScore + battleEvent.BattleResult.DefenderScore);
                    winners = battleEvent.BattleResult.Attackers
                        .Select(a => a.Owner)
                        .Distinct()
                        .ToArray();
                }

                if (battleEvent.BattleResult.DefenderWon)
                {
                    fraction = battleEvent.BattleResult.DefenderScore / (battleEvent.BattleResult.AttackerScore + battleEvent.BattleResult.DefenderScore);

                    winners = battleEvent.BattleResult.Attackers
                        .Select(a => a.Owner)
                        .Distinct()
                        .ToArray();
                }

                foreach (var tribe in winners)
                {
                    if (_perRaceParameters.ContainsKey(tribe.Race)
                        && _random.NextDouble() <= fraction)
                    {
                        var raceParams = _perRaceParameters[tribe.Race];

                        var r = _random.NextDouble();

                        var canAdopt = raceParams.CanAdoptWarHero
                            && tribe.Ruler != null
                            && tribe.Ruler.Children.Where(c => !c.IsDead).Count() < 3;

                        var canMarry = raceParams.CanMarryWarHero
                            && tribe.NotablePeople.Any(p => p.IsMature && !p.IsDead && p.Spouse == null && !p.IsMale);

                        if (canAdopt && (r < 0.5 || !canMarry))
                        {
                            var adoptee = GenerateSpouse(
                                tribe,
                                tribe.Race,
                                _random.NextDouble() < 0.5,
                                Util.Clamp(
                                    _random.Next(100, 150) / 100.0 * tribe.Race.AgeMaturity,
                                    tribe.Race.AgeMaturity,
                                    tribe.Race.AgeOld),
                                raceParams.WarHeroSkills);

                            if (tribe == _playerData.PlayerTribe)
                            {
                                _notificationsSystem.AddNotification(
                                    _marriageOptionNotificationBuilder.BuildAdoptionOptionNotification(
                                        "ui_notification_adoption",
                                        "Adoption Proposal",
                                        $"A war hero asks to join your dynasty.",
                                        "Adoption Proposal",
                                        $"A hero has emerged from the battle of {battleEvent.BattleResult.Name}. As a boon, {adoptee.HeShe} humbly asks to join your dynasty.",
                                        adoptee,
                                        null,
                                        null));
                            }
                            else
                            {
                                tribe.NotablePeople.Add(adoptee);

                                if (tribe.Ruler.IsMale)
                                {
                                    adoptee.Father = tribe.Ruler;
                                    adoptee.Mother = tribe.Ruler.Spouse;
                                    adoptee.Father.Children.Add(adoptee);
                                    if (adoptee.Mother != null)
                                    {
                                        adoptee.Mother.Children.Add(adoptee);
                                    }
                                }
                                adoptee.Mother = tribe.Ruler;
                                adoptee.Father = tribe.Ruler.Spouse;
                                adoptee.Mother.Children.Add(adoptee);
                                if (adoptee.Father != null)
                                {
                                    adoptee.Father.Children.Add(adoptee);
                                }
                            }
                        }
                        else if (canMarry && (r >= 0.5 || !canAdopt))
                        {
                            var option = _random.Choose(
                                tribe.NotablePeople
                                    .Where(p => p.IsMature && !p.IsDead && p.Spouse == null && !p.IsMale)
                                    .ToArray());

                            var optionAge = _worldManager.Month - option.BirthDate;

                            var spouse = GenerateSpouse(
                                tribe,
                                tribe.Race,
                                !option.IsMale,
                                Math.Max(tribe.Race.AgeMaturity, _random.Next(90, 110) / 100.0 * optionAge),
                                raceParams.WarHeroSkills);

                            if (tribe == _playerData.PlayerTribe)
                            {
                                _notificationsSystem.AddNotification(
                                    _marriageOptionNotificationBuilder.BuildMarriageOptionNotification(
                                        "ui_notification_wedding",
                                        "Marriage Proposal",
                                        $"{option.Name} has recived a marriage proposal.",
                                        "Marriage Proposal",
                                        $"A hero has emerged from the battle of {battleEvent.BattleResult.Name}. As a boon, {spouse.HeShe} humbly asks to marry {option.Name} and join your dynasty.",
                                        option,
                                        spouse,
                                        null,
                                        null));
                            }
                            else
                            {
                                tribe.NotablePeople.Add(spouse);
                                spouse.Spouse = option;
                                option.Spouse = spouse;

                                _simulationEventBus.SendEvent(new NotablePersonMarriageSimulationEvent(option, spouse));
                            }
                        }
                    }
                }
            }
        }

        private NotablePerson GenerateSpouse(
            Tribe owner,
            Race race,
            bool isMale,
            double age,
            IEnumerable<StartingSkillGroup> bonusSkills)
        {
            var birthDate = _worldManager.Month - age;


            var spouse = _peopleGenerator.CreatePerson(
                _random,
                owner,
                race,
                isMale,
                birthDate,
                null,
                null,
                false);

            return spouse;
        }

        private class PerRaceParameters
        {
            public FromCasteParameters[] FromCasteParameters { get; set; }
            public bool CanComeFromMinorHouse { get; set; }
            public bool CanMarryWarHero { get; set; }
            public bool CanAdoptWarHero { get; set; }
            public IEnumerable<StartingSkillGroup> WarHeroSkills { get; set; }
        }

        private class FromCasteParameters
        {
            public Caste Caste { get; set; }
            public double BuffDurationMonths { get; set; }
            public Dictionary<BonusType, double> SourceSettlementBuff { get; set; }
        }
    }
}
