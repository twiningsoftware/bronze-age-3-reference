﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Bronze.Common.Simulation.NotablePersonEvents
{
    public class NotablePersonHealingEvent : ISubSimulator, IWorldStateService, IDataLoader
    {
        private readonly IWorldManager _worldManager;
        private readonly IPeopleHelper _peopleHelper;
        private readonly INotificationsSystem _notificationsSystem;
        private readonly IGenericNotificationBuilder _genericNotificationBuilder;
        private readonly IPlayerDataTracker _playerDataTracker;
        private readonly IXmlReaderUtil _xmlReaderUtil;
        private readonly IGamedataTracker _gamedataTracker;

        private int _nextTribe;
        private Dictionary<string, double> _lastCheckDate;

        public string ElementName => "notable_person_healing_event";

        private NotablePersonSkill _injurySkill;
        private NotablePersonSkill _infectionSkill;
        private Random _random;

        public NotablePersonHealingEvent(
            IXmlReaderUtil xmlReaderUtil,
            IWorldManager worldManager,
            IPeopleHelper peopleHelper,
            INotificationsSystem notificationsSystem,
            IGenericNotificationBuilder genericNotificationBuilder,
            IPlayerDataTracker playerDataTracker,
            IGamedataTracker gamedataTracker)
        {
            _worldManager = worldManager;
            _peopleHelper = peopleHelper;
            _notificationsSystem = notificationsSystem;
            _genericNotificationBuilder = genericNotificationBuilder;
            _playerDataTracker = playerDataTracker;
            _xmlReaderUtil = xmlReaderUtil;
            _gamedataTracker = gamedataTracker;
            _lastCheckDate = new Dictionary<string, double>();
            _random = new Random();
        }
        
        public void Clear()
        {
            _nextTribe = 0;
            _lastCheckDate.Clear();
        }

        public void Initialize(WorldState worldState)
        {
            _random = new Random(worldState.Seed.GetHashCode());
        }

        public void Load(XElement element)
        {
            _injurySkill = _xmlReaderUtil.ObjectReferenceLookup(
                element,
                "injury_skill",
                _gamedataTracker.Skills,
                s => s.Id);

            _infectionSkill = _xmlReaderUtil.ObjectReferenceLookup(
                element,
                "infection_skill",
                _gamedataTracker.Skills,
                s => s.Id);
        }

        public void LoadCustomData(SerializedObject worldRoot)
        {
        }

        public void PostLoad(XElement element)
        {
        }

        public void SaveCustomData(SerializedObject worldRoot)
        {
        }

        public void StepSimulation(double elapsedMonths)
        {
            if(_nextTribe >= _worldManager.Tribes.Count)
            {
                _nextTribe = 0;
            }
            else
            {
                var tribe = _worldManager.Tribes[_nextTribe];
                _nextTribe += 1;

                for (var j = 0; j < tribe.NotablePeople.Count; j++)
                {
                    var person = tribe.NotablePeople[j];

                    if (!person.IsDead)
                    {
                        var deltaMonths = 0.0;
                        if (_lastCheckDate.ContainsKey(person.Id))
                        {
                            deltaMonths = _worldManager.Month - _lastCheckDate[person.Id];
                        }
                        else
                        {
                            _lastCheckDate.Add(person.Id, _worldManager.Month);
                        }

                        if (deltaMonths >= 1)
                        {
                            _lastCheckDate[person.Id] = _worldManager.Month;

                            var injurySkillPlacement = person.Skills
                                .Where(sp => sp.Skill == _injurySkill)
                                .FirstOrDefault();

                            var infectionSkillPlacement = person.Skills
                                .Where(sp => sp.Skill == _infectionSkill)
                                .FirstOrDefault();
                            
                            var doHeal = _random.NextDouble() <= 1 + person.GetMinorStat("Heal Chance");
                            var doInfection = _random.NextDouble() <= person.GetMinorStat("Infection Chance");
                            var doDeath = _random.NextDouble() <= person.GetMinorStat("Death Chance");

                            if(doDeath)
                            {
                                _peopleHelper.Kill(person);
                            }
                            else if(doInfection)
                            {
                                if(infectionSkillPlacement == null)
                                {
                                    var newPlacement = new NotablePersonSkillPlacement
                                    {
                                        Skill = _infectionSkill,
                                        Level = _infectionSkill.Levels.First()
                                    };
                                    person.Skills.Add(newPlacement);

                                    if(person.Owner == _playerDataTracker.PlayerTribe)
                                    {
                                        _notificationsSystem.AddNotification(_genericNotificationBuilder
                                            .BuildPersonSkillNotification(
                                                "ui_notification_disease",
                                                $"{person.Name} Infected",
                                                $"{person.Name} is suffering an infection",
                                                $"{person.Name} Infected",
                                                $"{person.Name} is suffering an infection, evil spirits are rampaging through {person.HimHer} body, and {person.HeShe} may perish.",
                                                person,
                                                newPlacement,
                                                null));
                                    }
                                }
                                else if (infectionSkillPlacement.CanIncrement())
                                {
                                    infectionSkillPlacement.Increment();

                                    if (person.Owner == _playerDataTracker.PlayerTribe)
                                    {
                                        _notificationsSystem.AddNotification(_genericNotificationBuilder
                                            .BuildPersonSkillNotification(
                                                "ui_notification_disease",
                                                $"{person.Name} Infection Worsens",
                                                $"{person.Name}'s infection has worsened.",
                                                $"{person.Name} Infection Worsens",
                                                $"The infection plaguing {person.Name} has worsened.",
                                                person,
                                                infectionSkillPlacement,
                                                null));
                                    }
                                }
                            }
                            else if(doHeal)
                            {
                                if(infectionSkillPlacement != null)
                                {
                                    if(infectionSkillPlacement.CanDecrement())
                                    {
                                        infectionSkillPlacement.Decrement();

                                        if (person.Owner == _playerDataTracker.PlayerTribe)
                                        {
                                            _notificationsSystem.AddNotification(_genericNotificationBuilder
                                                .BuildPersonSkillNotification(
                                                    "ui_notification_heal",
                                                    $"{person.Name} Infection Improved",
                                                    $"{person.Name}'s infection has improved.",
                                                    $"{person.Name} Infection Improved",
                                                    $"The infection plaguing {person.Name} has improved, {person.HeShe} may be on the road to recovery.",
                                                    person,
                                                    infectionSkillPlacement,
                                                null));
                                        }
                                    }
                                    else
                                    {
                                        person.Skills.Remove(infectionSkillPlacement);

                                        if (person.Owner == _playerDataTracker.PlayerTribe)
                                        {
                                            _notificationsSystem.AddNotification(_genericNotificationBuilder
                                                .BuildPersonNotification(
                                                    "ui_notification_heal",
                                                    $"{person.Name} Infection Healed",
                                                    $"{person.Name}'s infection has healed.",
                                                    $"{person.Name} Infection Healed",
                                                    $"{person.Name} has recovered from {person.HimHer} infection.",
                                                    person,
                                                    null));
                                        }
                                    }
                                }
                                else if (injurySkillPlacement != null)
                                {
                                    if (injurySkillPlacement.CanDecrement())
                                    {
                                        injurySkillPlacement.Decrement();

                                        if (person.Owner == _playerDataTracker.PlayerTribe)
                                        {
                                            _notificationsSystem.AddNotification(_genericNotificationBuilder
                                                    .BuildPersonSkillNotification(
                                                        "ui_notification_heal",
                                                        $"{person.Name} Injuries Improved",
                                                        $"{person.Name}'s injuries have improved.",
                                                        $"{person.Name} Injuries Improved",
                                                        $"{person.Name}'s injuries have improved. With the gods' favor, {person.HeShe} will be fully recovered soon.",
                                                        person,
                                                        injurySkillPlacement,
                                                        null));
                                        }
                                    }
                                    else
                                    {
                                        person.Skills.Remove(injurySkillPlacement);

                                        if (person.Owner == _playerDataTracker.PlayerTribe)
                                        {
                                            _notificationsSystem.AddNotification(_genericNotificationBuilder
                                                .BuildPersonNotification(
                                                    "ui_notification_heal",
                                                    $"{person.Name} Injuries Healed",
                                                    $"{person.Name}'s injuries have healed.",
                                                    $"{person.Name} Injuries Healed",
                                                    $"{person.Name} has fully recovered from {person.HimHer} injuries.",
                                                    person,
                                                    null));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
