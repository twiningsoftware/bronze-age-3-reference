﻿using Bronze.Contracts;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Bronze.Common.Simulation.NotablePersonEvents
{
    public class NotablePersonAgingEvent : ISubSimulator, IWorldStateService, IDataLoader, INotablePersonGenerationAddon
    {
        private readonly IWorldManager _worldManager;
        private readonly IPeopleHelper _peopleHelper;
        private readonly INotificationsSystem _notificationsSystem;
        private readonly IGenericNotificationBuilder _genericNotificationBuilder;
        private readonly IPlayerDataTracker _playerData;
        private readonly IXmlReaderUtil _xmlReaderUtil;
        private readonly IGamedataTracker _gamedataTracker;

        private int _nextTribe;

        public string ElementName => "notable_person_aging_event";

        private NotablePersonSkill _ageSkill;

        private List<Race> _racesToIgnore;
        
        public NotablePersonAgingEvent(
            IXmlReaderUtil xmlReaderUtil,
            IWorldManager worldManager,
            IPeopleHelper peopleHelper,
            INotificationsSystem notificationsSystem,
            IGenericNotificationBuilder genericNotificationBuilder,
            IPlayerDataTracker playerData,
            IGamedataTracker gamedataTracker)
        {
            _worldManager = worldManager;
            _peopleHelper = peopleHelper;
            _notificationsSystem = notificationsSystem;
            _genericNotificationBuilder = genericNotificationBuilder;
            _playerData = playerData;
            _xmlReaderUtil = xmlReaderUtil;
            _gamedataTracker = gamedataTracker;
            _racesToIgnore = new List<Race>();
        }
        
        public void Clear()
        {
            _nextTribe = 0;
        }

        public void Initialize(WorldState worldState)
        {
        }

        public void Load(XElement element)
        {
            _ageSkill = _xmlReaderUtil.ObjectReferenceLookup(
                element,
                "age_skill",
                _gamedataTracker.Skills,
                s => s.Id);

            _racesToIgnore = element.Elements("not")
                .Select(e => _xmlReaderUtil.ObjectReferenceLookup(
                    e,
                    "race_id",
                    _gamedataTracker.Races,
                    r => r.Id))
                .ToList();
        }

        public void LoadCustomData(SerializedObject worldRoot)
        {
        }

        public void PostLoad(XElement element)
        {
        }

        public void SaveCustomData(SerializedObject worldRoot)
        {
        }

        public void StepSimulation(double elapsedMonths)
        {
            if(_nextTribe >= _worldManager.Tribes.Count)
            {
                _nextTribe = 0;
            }
            else
            {
                var tribe = _worldManager.Tribes[_nextTribe];
                _nextTribe += 1;

                for (var j = 0; j < tribe.NotablePeople.Count; j++)
                {
                    var person = tribe.NotablePeople[j];

                    if (!person.IsDead)
                    {
                        var age = _worldManager.Month - person.BirthDate;

                        if (!_racesToIgnore.Contains(person.Race))
                        {
                            var newBracket = AgeToBracket(age, person);

                            if (newBracket != person.AgeBracket)
                            {
                                person.AgeBracket = newBracket;

                                var ageSkillPlacement = person.Skills
                                    .Where(s => s.Skill == _ageSkill)
                                    .FirstOrDefault();
                                if (ageSkillPlacement == null)
                                {
                                    ageSkillPlacement = new NotablePersonSkillPlacement()
                                    {
                                        Skill = _ageSkill
                                    };
                                    person.Skills.Add(ageSkillPlacement);
                                }
                                
                                ageSkillPlacement.Level = ageSkillPlacement.Skill.Levels.ToArray()[((int)newBracket) - 1];

                                if (person.Owner == _playerData.PlayerTribe
                                    && _worldManager.Month != Constants.START_MONTH)
                                {
                                    _notificationsSystem.AddNotification(_genericNotificationBuilder
                                        .BuildPersonSkillNotification(
                                            "ui_notification_learning",
                                            $"{person.Name} is now {ageSkillPlacement.Level.Name}",
                                            $"With age comes changes.",
                                            $"{person.Name} is now {ageSkillPlacement.Level.Name}",
                                            $"With age comes changes.",
                                            person,
                                            ageSkillPlacement,
                                            null));
                                }
                            }

                            if (!person.IsMature && age >= person.Race.AgeMaturity)
                            {
                                person.IsMature = true;

                                if (person.Owner == _playerData.PlayerTribe
                                    && _worldManager.Month != Constants.START_MONTH)
                                {
                                    _notificationsSystem.AddNotification(_genericNotificationBuilder
                                            .BuildPersonNotification(
                                            "ui_notification_person_mature",
                                            $"{person.Name} has Come of Age",
                                            $"{person.Name} has come of age.",
                                            $"{person.Name} has Come of Age",
                                            $"{person.Name} has come of age, and is ready for for postings.",
                                            person,
                                            null));
                                }
                            }
                        }
                    }
                }
            }
        }
        
        public void Apply(Random random, Tribe owner, Race race, bool inDynasty, NotablePerson person)
        {
            if (!_racesToIgnore.Contains(race))
            {
                var age = Math.Max(_worldManager.Month, Constants.START_MONTH) - person.BirthDate;

                var newBracket = AgeToBracket(age, person);

                person.Skills.Add(
                    new NotablePersonSkillPlacement()
                    {
                        Skill = _ageSkill,
                        Level = _ageSkill.Levels.ToArray()[((int)newBracket) - 1]
                    });

                person.IsMature = age >= person.Race.AgeMaturity;
                person.AgeBracket = newBracket;
            }
        }

        private NotablePersonAgeBracket AgeToBracket(double age, NotablePerson person)
        {
            if (age < person.Race.AgeMaturity / 8)
            {
                return NotablePersonAgeBracket.Baby;
            }
            else if (age < person.Race.AgeMaturity)
            {
                return NotablePersonAgeBracket.Child;
            }
            else if (age < person.Race.AgeOld / 2)
            {
                return NotablePersonAgeBracket.Young;
            }
            else if (age < person.Race.AgeOld)
            {
                return NotablePersonAgeBracket.MiddleAged;
            }
            else
            {
                return NotablePersonAgeBracket.Old;
            }
        }
    }
}
