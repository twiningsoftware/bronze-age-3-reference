﻿using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;

namespace Bronze.Common.Simulation.NotablePersonEvents
{
    public class NotablePersonDeathEvent : ISubSimulator, IWorldStateService
    {
        private readonly IWorldManager _worldManager;
        private readonly IPeopleHelper _peopleHelper;

        private Random _random;

        public NotablePersonDeathEvent(
            IWorldManager worldManager,
            IPeopleHelper peopleHelper)
        {
            _worldManager = worldManager;
            _peopleHelper = peopleHelper;
            _random = new Random();
        }

        public void Clear()
        {
        }

        public void Initialize(WorldState worldState)
        {
            _random = new Random(worldState.Seed.GetHashCode());
        }

        public void LoadCustomData(SerializedObject worldRoot)
        {
        }

        public void SaveCustomData(SerializedObject worldRoot)
        {
        }

        public void StepSimulation(double elapsedMonths)
        {
            for(var i = 0; i < _worldManager.Tribes.Count; i++)
            {
                var tribe = _worldManager.Tribes[i];

                for (var j = 0; j < tribe.NotablePeople.Count; j++)
                {
                    var person = tribe.NotablePeople[j];

                    if (!person.IsDead)
                    {
                        var age = _worldManager.Month - person.BirthDate;

                        var oldAgePerc = (age - person.Race.AgeMaturity) / (person.Race.AgeOld - person.Race.AgeMaturity);

                        var deathChance = (2 * oldAgePerc - 1.9) * elapsedMonths;

                        if(_random.NextDouble() < deathChance)
                        {
                            _peopleHelper.Kill(person);
                        }
                    }
                }
            }
        }
    }
}
