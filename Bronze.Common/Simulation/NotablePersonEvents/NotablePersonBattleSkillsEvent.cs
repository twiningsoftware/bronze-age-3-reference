﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.Simulation.Events;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Bronze.Common.Simulation.NotablePersonEvents
{
    public class NotablePersonBattleSkillsEvent : IWorldStateService, IDataLoader
    {
        private readonly IWorldManager _worldManager;
        private readonly INotificationsSystem _notificationsSystem;
        private readonly IGenericNotificationBuilder _genericNotificationBuilder;
        private readonly IPlayerDataTracker _playerData;
        private readonly IXmlReaderUtil _xmlReaderUtil;
        private readonly IGamedataTracker _gamedataTracker;
        private readonly ISimulationEventBus _simulationEventBus;
        private readonly IPeopleHelper _peopleHelper;

        private Random _random;
        private Dictionary<string, double> _battleXp;
        private NotablePersonSkill _battleSkill;
        private NotablePersonSkill _injurySkill;
        private NotablePersonSkill _crippledSkill;
        private NotablePersonSkill _scarsSkill;
        private double _xpPerLevel;
        private double _variance;

        public string ElementName => "notable_person_battle_skills_event";

        public NotablePersonBattleSkillsEvent(
            IWorldManager worldManager,
            INotificationsSystem notificationsSystem,
            IGenericNotificationBuilder genericNotificationBuilder,
            IPlayerDataTracker playerData,
            IXmlReaderUtil xmlReaderUtil,
            IGamedataTracker gamedataTracker,
            ISimulationEventBus simulationEventBus,
            IPeopleHelper peopleHelper)
        {
            _worldManager = worldManager;
            _notificationsSystem = notificationsSystem;
            _genericNotificationBuilder = genericNotificationBuilder;
            _playerData = playerData;
            _xmlReaderUtil = xmlReaderUtil;
            _gamedataTracker = gamedataTracker;
            _simulationEventBus = simulationEventBus;
            _peopleHelper = peopleHelper;
            _random = new Random();

            _battleXp = new Dictionary<string, double>();
            
            _simulationEventBus.OnSimulationEvent += OnSimulationEvent;
        }

        private void OnSimulationEvent(ISimulationEvent evt)
        {
            if (evt is BattleOccurredSimulationEvent battleEvt)
            {
                var results = battleEvt.BattleResult;
                var battleXp = (results.AttackerBeforeStrength + results.DefenderBeforeStrength) / 2;

                var attackerInjuryRate = 0.0;
                if(results.AttackerBeforeStrength > 0)
                {
                    attackerInjuryRate = 1 - (results.AttackerAfterStrength / results.AttackerBeforeStrength);
                }

                var defenderInjuryRate = 0.0;
                if (results.DefenderBeforeStrength > 0)
                {
                    defenderInjuryRate = 1 - (results.DefenderAfterStrength / results.DefenderBeforeStrength);
                }

                foreach (var leader in results.AttackerLeaders)
                {
                    DoExperienceFor(
                        leader,
                        battleXp,
                        results.AttackerWon,
                        results.DefenderWon,
                        leader.Owner == _playerData.PlayerTribe,
                        attackerInjuryRate,
                        battleEvt.BattleResult.Location.Position);
                }

                foreach (var leader in results.DefenderLeaders)
                {
                    DoExperienceFor(
                        leader,
                        battleXp,
                        results.DefenderWon,
                        results.AttackerWon,
                        leader.Owner == _playerData.PlayerTribe,
                        defenderInjuryRate,
                        battleEvt.BattleResult.Location.Position);
                }
            }
        }

        public void Load(XElement element)
        {
            _battleSkill = _xmlReaderUtil.ObjectReferenceLookup(
                element,
                "battle_skill",
                _gamedataTracker.Skills,
                s => s.Id);
            
            _injurySkill = _xmlReaderUtil.ObjectReferenceLookup(
                element,
                "injury_skill",
                _gamedataTracker.Skills,
                s => s.Id);

            _crippledSkill = _xmlReaderUtil.ObjectReferenceLookup(
                element,
                "crippled_skill",
                _gamedataTracker.Skills,
                s => s.Id);

            _scarsSkill = _xmlReaderUtil.ObjectReferenceLookup(
                element,
                "scars_skill",
                _gamedataTracker.Skills,
                s => s.Id);
            
            _xpPerLevel = _xmlReaderUtil.AttributeAsDouble(element, "xp_per_level");
            _variance = _xmlReaderUtil.AttributeAsDouble(element, "variance");
        }

        public void PostLoad(XElement element)
        {
        }

        public void Clear()
        {
            _battleXp.Clear();
        }

        public void Initialize(WorldState worldState)
        {
            _random = new Random(worldState.Seed.GetHashCode());
        }

        public void LoadCustomData(SerializedObject worldRoot)
        {
            var serviceRoot = worldRoot.GetOptionalChild("notable_person_battle_skills_event");

            if (serviceRoot != null)
            {
                _battleXp = serviceRoot.GetChild("battle_xp").GetStringDoubleDictionary();
            }
        }

        public void SaveCustomData(SerializedObject worldRoot)
        {
            var serviceRoot = worldRoot.CreateChild("notable_person_battle_skills_event");

            serviceRoot.CreateChild("battle_xp").SetStringDoubleDictionary(_battleXp);
        }
        
        private void DoExperienceFor(
            NotablePerson person, 
            double xpGained,
            bool wonBattle,
            bool lostBattle,
            bool doNotification,
            double injuryRate,
            CellPosition battlePosition)
        {
            if (!_battleXp.ContainsKey(person.Id))
            {
                _battleXp.Add(person.Id, 0);
            }
            _battleXp[person.Id] += xpGained;

            var skillPlacement = person.Skills.Where(sp => sp.Skill == _battleSkill).FirstOrDefault();
            var nextLevel = skillPlacement?.Level?.Level ?? -1;

            var timeToNext = Math.Pow(2, nextLevel + 2) * _xpPerLevel;

            var xpToNextMin = timeToNext - _variance;
            var xpToNextMax = timeToNext + _variance;

            var chanceToNext = (_battleXp[person.Id] - xpToNextMin) / (xpToNextMax - xpToNextMin);

            if(_random.NextDouble() < chanceToNext)
            {
                skillPlacement = _peopleHelper.IncrementOrAdd(person, _battleSkill);

                if(skillPlacement != null && doNotification)
                {
                    _notificationsSystem.AddNotification(_genericNotificationBuilder
                        .BuildPersonSkillNotification(
                            "ui_notification_learning",
                            $"{person.Name}'s Skill Improved",
                            $"{person.Name}'s has gained {skillPlacement.Level.Name}",
                            $"{person.Name}'s Skill Improved",
                            $"Through hard work and experience, {person.Name} has grown in knowledge and ability.",
                            person,
                            skillPlacement,
                            battlePosition));
                }
            }

            var injurySkillPlacements = new List<NotablePersonSkillPlacement>();
            
            var injuryRoll = _random.NextDouble();

            if (injuryRoll < injuryRate)
            {
                if(injuryRoll < injuryRate / 2)
                {
                    injurySkillPlacements.Add(_peopleHelper.IncrementOrAdd(person, _injurySkill));
                    
                    if(_random.NextDouble() < 0.5)
                    {
                        injurySkillPlacements.Add(_peopleHelper.IncrementOrAdd(person, _crippledSkill));
                    }
                    else
                    {
                        injurySkillPlacements.Add(_peopleHelper.IncrementOrAdd(person, _scarsSkill));
                    }
                }
            }

            var doDeath = _random.NextDouble() <= person.GetMinorStat("Death Chance");

            if (doDeath)
            {
                _peopleHelper.Kill(person);
            }
            else if (doNotification)
            {
                foreach(var injurySkillPlacement in injurySkillPlacements)
                {
                    _notificationsSystem.AddNotification(_genericNotificationBuilder
                        .BuildPersonSkillNotification(
                            "ui_notification_person_injured",
                            $"{person.Name} Injured",
                            $"{person.Name} has been injured in battle.",
                            $"{person.Name} Injured",
                            $"{person.Name} has been injured in battle.",
                            person,
                            injurySkillPlacement,
                            battlePosition));
                }
            }
        }
    }
}
