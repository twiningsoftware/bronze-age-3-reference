﻿using Bronze.Contracts;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Bronze.Common.Simulation.NotablePersonEvents
{
    public class NotablePersonLearningEvent : ISubSimulator, IWorldStateService, IDataLoader, INotablePersonWorldgenAddon
    {
        private readonly IWorldManager _worldManager;
        private readonly INotificationsSystem _notificationsSystem;
        private readonly IGenericNotificationBuilder _genericNotificationBuilder;
        private readonly IPlayerDataTracker _playerData;
        private readonly IXmlReaderUtil _xmlReaderUtil;
        private readonly IGamedataTracker _gamedataTracker;

        private Random _random;
        private Dictionary<string, double> _timeAsGeneral;
        private Dictionary<string, double> _timeAsRuler;
        private Dictionary<string, double> _timeAsGovernor;
        private NotablePersonSkill _governorSkill;
        private NotablePersonSkill _rulerSkill;
        private NotablePersonSkill _generalSkill;
        private double _monthsPerLevel;
        private double _variance;
        private Dictionary<string, double> _lastCheckDate;
        private int _nextTribe;

        public string ElementName => "notable_person_learning_event";

        public NotablePersonLearningEvent(
            IWorldManager worldManager,
            INotificationsSystem notificationsSystem,
            IGenericNotificationBuilder genericNotificationBuilder,
            IPlayerDataTracker playerData,
            IXmlReaderUtil xmlReaderUtil,
            IGamedataTracker gamedataTracker)
        {
            _worldManager = worldManager;
            _notificationsSystem = notificationsSystem;
            _genericNotificationBuilder = genericNotificationBuilder;
            _playerData = playerData;
            _xmlReaderUtil = xmlReaderUtil;
            _gamedataTracker = gamedataTracker;
            _random = new Random();

            _timeAsGeneral = new Dictionary<string, double>();
            _timeAsGovernor = new Dictionary<string, double>();
            _timeAsRuler = new Dictionary<string, double>();
            _lastCheckDate = new Dictionary<string, double>();
        }

        public void Load(XElement element)
        {
            _rulerSkill = _xmlReaderUtil.ObjectReferenceLookup(
                element,
                "ruler_skill",
                _gamedataTracker.Skills,
                s => s.Id);

            _generalSkill = _xmlReaderUtil.ObjectReferenceLookup(
                element,
                "general_skill",
                _gamedataTracker.Skills,
                s => s.Id);

            _governorSkill = _xmlReaderUtil.ObjectReferenceLookup(
                element,
                "governor_skill",
                _gamedataTracker.Skills,
                s => s.Id);

            _monthsPerLevel = _xmlReaderUtil.AttributeAsDouble(element, "months_per_level");
            _variance = _xmlReaderUtil.AttributeAsDouble(element, "variance");
        }

        public void PostLoad(XElement element)
        {
        }

        public void Clear()
        {
            _timeAsGeneral.Clear();
            _timeAsGovernor.Clear();
            _timeAsRuler.Clear();

            _nextTribe = 0;
            _lastCheckDate.Clear();
        }

        public void Initialize(WorldState worldState)
        {
            _random = new Random(worldState.Seed.GetHashCode());
        }

        public void LoadCustomData(SerializedObject worldRoot)
        {
            var serviceRoot = worldRoot.GetOptionalChild("notable_person_learning_event");

            if (serviceRoot != null)
            {
                _timeAsGeneral = serviceRoot.GetChild("time_as_general").GetStringDoubleDictionary();
                _timeAsGovernor = serviceRoot.GetChild("time_as_governor").GetStringDoubleDictionary();
                _timeAsRuler = serviceRoot.GetChild("time_as_ruler").GetStringDoubleDictionary();
            }
        }

        public void SaveCustomData(SerializedObject worldRoot)
        {
            var serviceRoot = worldRoot.CreateChild("notable_person_learning_event");

            serviceRoot.CreateChild("time_as_general").SetStringDoubleDictionary(_timeAsGeneral);
            serviceRoot.CreateChild("time_as_governor").SetStringDoubleDictionary(_timeAsGovernor);
            serviceRoot.CreateChild("time_as_ruler").SetStringDoubleDictionary(_timeAsRuler);
        }

        public void StepSimulation(double elapsedMonths)
        {
            if (_nextTribe >= _worldManager.Tribes.Count)
            {
                _nextTribe = 0;
            }
            else
            {
                var tribe = _worldManager.Tribes[_nextTribe];
                _nextTribe += 1;

                for (var j = 0; j < tribe.NotablePeople.Count; j++)
                {
                    var person = tribe.NotablePeople[j];

                    if (!person.IsDead)
                    {
                        var deltaMonths = 0.0;
                        if (_lastCheckDate.ContainsKey(person.Id))
                        {
                            deltaMonths = _worldManager.Month - _lastCheckDate[person.Id];
                        }
                        else
                        {
                            _lastCheckDate.Add(person.Id, _worldManager.Month);
                        }

                        if (deltaMonths >= 1)
                        {
                            _lastCheckDate[person.Id] = _worldManager.Month;

                            if(person.GeneralOf != null)
                            {
                                var commandFraction = person.GeneralOf.StrengthEstimate / (Constants.UNITS_PER_ARMY * 8.0);

                                DoExperienceFor(
                                    person,
                                    deltaMonths * commandFraction,
                                    _timeAsGeneral,
                                    _generalSkill,
                                    person.Owner == _playerData.PlayerTribe);
                                
                            }

                            if (person.GovernorOf != null)
                            {
                                DoExperienceFor(
                                    person,
                                    deltaMonths,
                                    _timeAsGovernor,
                                    _governorSkill,
                                    person.Owner == _playerData.PlayerTribe);
                            }

                            if (person == person.Owner.Ruler)
                            {
                                DoExperienceFor(
                                    person,
                                    deltaMonths,
                                    _timeAsRuler,
                                    _rulerSkill,
                                    person.Owner == _playerData.PlayerTribe);

                            }
                        }
                    }
                }
            }
        }

        private void DoExperienceFor(
            NotablePerson person, 
            double deltaMonths, 
            Dictionary<string, double> timeInRole,
            NotablePersonSkill skill,
            bool doNotification)
        {
            if (!timeInRole.ContainsKey(person.Id))
            {
                timeInRole.Add(person.Id, 0);
            }
            timeInRole[person.Id] += deltaMonths;

            var skillPlacement = person.Skills.Where(sp => sp.Skill == skill).FirstOrDefault();
            var nextLevel = skillPlacement?.Level?.Level ?? -1;

            var timeToNext = Math.Pow(2, nextLevel + 2) * _monthsPerLevel;

            var timeToNextMin = timeToNext - _variance;
            var timeToNextMax = timeToNext + _variance;

            var chanceToNext = (timeInRole[person.Id] - timeToNextMin) / (timeToNextMax - timeToNextMin);

            if(_random.NextDouble() < chanceToNext)
            {
                if(skillPlacement == null)
                {
                    skillPlacement = new NotablePersonSkillPlacement
                    {
                        Skill = skill,
                        Level = skill.Levels.First()
                    };

                    person.Skills.Add(skillPlacement);

                    if (doNotification)
                    {
                        _notificationsSystem.AddNotification(_genericNotificationBuilder
                            .BuildPersonSkillNotification(
                                "ui_notification_learning",
                                $"{person.Name}'s Skill Improved",
                                $"{person.Name}'s has gained {skillPlacement.Level.Name}",
                                $"{person.Name}'s Skill Improved",
                                $"Through hard work and experience, {person.Name} has grown in knowledge and ability.",
                                person,
                                skillPlacement,
                                null));
                    }
                }
                else if(skillPlacement.CanIncrement())
                {
                    skillPlacement.Increment();

                    if (doNotification)
                    {
                        _notificationsSystem.AddNotification(_genericNotificationBuilder
                            .BuildPersonSkillNotification(
                                "ui_notification_learning",
                                $"{person.Name}'s Skill Improved",
                                $"{person.Name}'s has gained {skillPlacement.Level.Name}",
                                $"{person.Name}'s Skill Improved",
                                $"Through hard work and experience, {person.Name} has grown in knowledge and ability.",
                                person,
                                skillPlacement,
                                null));
                    }
                }
            }
        }
        
        public void ApplyForWorldgen(
            Random random, 
            Tribe owner, 
            Race race, 
            bool inDynasty, 
            NotablePerson person, 
            bool isRuler)
        {
            var age = Constants.START_MONTH - person.BirthDate;

            if(isRuler)
            {
                var timeInRole = (random.NextDouble() / 2 + 0.5) * (age - race.AgeMaturity);
                for(var i = 0; i < timeInRole; i++)
                {
                    DoExperienceFor(
                        person,
                        1,
                        _timeAsRuler,
                        _rulerSkill,
                        false);
                }
            }
            
            if(random.NextDouble() < 0.5 && age > race.AgeMaturity)
            {
                var timeInRole = random.Next(0, (int)(age - race.AgeMaturity));
                for (var i = 0; i < timeInRole; i++)
                {
                    DoExperienceFor(
                        person,
                        1,
                        _timeAsGeneral,
                        _generalSkill,
                        false);
                }
            }

            if (random.NextDouble() < 0.5 && age > race.AgeMaturity)
            {
                var timeInRole = random.Next(0, (int)(age - race.AgeMaturity));
                for (var i = 0; i < timeInRole; i++)
                {
                    DoExperienceFor(
                        person,
                        1,
                        _timeAsGovernor,
                        _governorSkill,
                        false);
                }
            }
        }
    }
}
