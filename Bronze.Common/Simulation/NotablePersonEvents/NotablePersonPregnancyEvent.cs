﻿using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Bronze.Common.Simulation.NotablePersonEvents
{
    public class NotablePersonPregnancyEvent : ISubSimulator, IWorldStateService, IDataLoader
    {
        private readonly IWorldManager _worldManager;
        private readonly IPeopleHelper _peopleHelper;
        private readonly INotificationsSystem _notificationsSystem;
        private readonly IGenericNotificationBuilder _genericNotificationBuilder;
        private readonly IPlayerDataTracker _playerDataTracker;
        private readonly IXmlReaderUtil _xmlReaderUtil;
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IPeopleGenerator _peopleGenerator;

        private Random _random;
        private Dictionary<string, double> _pregnancyCooldowns;
        private Dictionary<string, double> _pregnancyCounters;
        private NotablePersonSkill _pregnancySkill;
        private List<Race> _racesToIgnore;

        public string ElementName => "notable_person_pregnancy_event";
        
        public NotablePersonPregnancyEvent(
            IWorldManager worldManager,
            IPeopleHelper peopleHelper,
            INotificationsSystem notificationsSystem,
            IGenericNotificationBuilder genericNotificationBuilder,
            IPlayerDataTracker playerDataTracker,
            IXmlReaderUtil xmlReaderUtil,
            IGamedataTracker gamedataTracker,
            IPeopleGenerator peopleGenerator)
        {
            _worldManager = worldManager;
            _peopleHelper = peopleHelper;
            _notificationsSystem = notificationsSystem;
            _genericNotificationBuilder = genericNotificationBuilder;
            _playerDataTracker = playerDataTracker;
            _xmlReaderUtil = xmlReaderUtil;
            _gamedataTracker = gamedataTracker;
            _peopleGenerator = peopleGenerator;
            _random = new Random();

            _pregnancyCooldowns = new Dictionary<string, double>();
            _pregnancyCounters = new Dictionary<string, double>();
            _racesToIgnore = new List<Race>();
        }

        public void Load(XElement element)
        {
            _pregnancySkill = _xmlReaderUtil.ObjectReferenceLookup(
                element,
                "pregnancy_skill",
                _gamedataTracker.Skills,
                s => s.Id);
            
            _racesToIgnore = element.Elements("not")
                .Select(e => _xmlReaderUtil.ObjectReferenceLookup(
                    e,
                    "race_id",
                    _gamedataTracker.Races,
                    r => r.Id))
                .ToList();
        }
        
        public void PostLoad(XElement element)
        {
        }

        public void Clear()
        {
            _pregnancyCooldowns.Clear();
            _pregnancyCounters.Clear();
        }

        public void Initialize(WorldState worldState)
        {
            _random = new Random(worldState.Seed.GetHashCode());
        }

        public void LoadCustomData(SerializedObject worldRoot)
        {
            var serviceRoot = worldRoot.GetOptionalChild("notable_person_pregnancy_event");

            if (serviceRoot != null)
            {
                _pregnancyCooldowns = serviceRoot.GetChild("pregnancy_cooldowns").GetStringDoubleDictionary();
                _pregnancyCounters = serviceRoot.GetChild("pregnancy_counters").GetStringDoubleDictionary();
            }
        }

        public void SaveCustomData(SerializedObject worldRoot)
        {
            var serviceRoot = worldRoot.CreateChild("notable_person_pregnancy_event");

            serviceRoot.CreateChild("pregnancy_cooldowns").SetStringDoubleDictionary(_pregnancyCooldowns);
            serviceRoot.CreateChild("pregnancy_counters").SetStringDoubleDictionary(_pregnancyCounters);
        }

        public void StepSimulation(double elapsedMonths)
        {
            for(var i = 0; i < _worldManager.Tribes.Count; i++)
            {
                var tribe = _worldManager.Tribes[i];

                for (var j = 0; j < tribe.NotablePeople.Count; j++)
                {
                    var person = tribe.NotablePeople[j];

                    if (!person.IsDead
                            && !person.IsMale
                            && person.Spouse != null
                            && !person.Spouse.IsDead
                            && person.Spouse.IsMale
                            && person.Children.Count < 4
                            && person.IsMature
                            && !_racesToIgnore.Contains(person.Race))
                    {
                        var age = _worldManager.Month - person.BirthDate;
                        
                        var generationsToRuler = GenerationsToRuler(person);
                        
                        if (generationsToRuler != null
                            && generationsToRuler <= 2)
                        {
                            if(!_pregnancyCooldowns.ContainsKey(person.Id))
                            {
                                _pregnancyCooldowns.Add(person.Id, 0);
                                _pregnancyCounters.Add(person.Id, -1);
                            }

                            _pregnancyCooldowns[person.Id] -= elapsedMonths;

                            if(_pregnancyCounters[person.Id] < 0 && _pregnancyCooldowns[person.Id] <= 0)
                            {
                                var pregnancyChance = _peopleHelper.GetPregnancyChance(person.Spouse, person, _worldManager.Month);

                                if(_random.NextDouble() < pregnancyChance * elapsedMonths)
                                {
                                    _pregnancyCounters[person.Id] = 0;

                                    person.Skills.Add(
                                        new NotablePersonSkillPlacement()
                                        {
                                            Skill = _pregnancySkill,
                                            Level = _pregnancySkill.Levels.First()
                                        });
                                }
                            }
                            else if(_pregnancyCounters[person.Id] >= 0)
                            {
                                _pregnancyCounters[person.Id] += elapsedMonths;

                                if(_pregnancyCounters[person.Id] >= person.Race.PregnancyTime)
                                {
                                    person.Skills.RemoveAll(sp => sp.Skill == _pregnancySkill);

                                    var newPerson = _peopleGenerator.CreatePerson(
                                        _random,
                                        person.Owner,
                                        person.Race,
                                        _random.NextDouble() < 0.5,
                                        _worldManager.Month,
                                        person.Spouse,
                                        person,
                                        person.InDynasty || person.Spouse.InDynasty);

                                    var boyOrGirl = newPerson.IsMale ? "boy" : "girl";

                                    var maybeHeir = string.Empty;
                                    if(newPerson.InDynasty)
                                    {
                                        maybeHeir = $"\n{newPerson.Owner.Ruler.Name} welcomes a new potential heir, further securing the dynasty.";
                                    }

                                    newPerson.Owner.NotablePeople.Add(newPerson);

                                    if (_playerDataTracker.PlayerTribe == newPerson.Owner)
                                    {
                                        _notificationsSystem.AddNotification(_genericNotificationBuilder
                                            .BuildPersonNotification(
                                            "ui_notification_birth",
                                            "A Child is Born",
                                            $"A baby {boyOrGirl} has been born to {person.Spouse.Name} and {person.Name}",
                                            "A Child is Born",
                                            $"{person.Spouse.Name} and {person.Name} welcome a new baby {boyOrGirl} into the world.{maybeHeir}",
                                            newPerson,
                                            null));
                                    }
                                    
                                    _pregnancyCounters[person.Id] = -1;
                                    _pregnancyCooldowns[person.Id] = person.Race.PregnancyTime * 4;
                                }
                            }
                        }
                    }
                }
            }
        }

        private int? GenerationsToRuler(NotablePerson person)
        {
            if(person == null || person.IsDead)
            {
                return null;
            }

            if(person == person.Owner.Ruler || person.Spouse == person.Owner.Ruler)
            {
                return 0;
            }

            var fatherDist = GenerationsToRuler(person.Father);
            var motherDist = GenerationsToRuler(person.Mother);

            int? minDist = null;

            if (motherDist != null && fatherDist != null)
            {
                minDist = Math.Min(fatherDist.Value, motherDist.Value);
            }
            else
            {
                minDist =  fatherDist ?? motherDist;
            }

            if(minDist != null)
            {
                return minDist + 1;
            }

            return null;
        }
    }
}
