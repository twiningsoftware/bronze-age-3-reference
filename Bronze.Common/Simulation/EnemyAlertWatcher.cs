﻿using Bronze.Common.Data.World.Units;
using Bronze.Common.Services;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.UI;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.Simulation
{
    public class EnemyAlertWatcher : AbstractBackgroundCalculatorSubSimulator
    {
        private static readonly object LOCK = new object();

        private readonly IWorldManager _worldManager;
        private readonly IPlayerDataTracker _playerData;
        private readonly INotificationsSystem _notificationsSystem;
        private readonly IUserSettings _userSettings;
        
        private List<Action> _results;
        
        public EnemyAlertWatcher(
            IGameInterface gameInterface,
            IWorldManager worldManager,
            IPlayerDataTracker playerData,
            INotificationsSystem notificationsSystem,
            IUserSettings userSettings)
            :base(gameInterface)
        {
            _worldManager = worldManager;
            _playerData = playerData;
            _notificationsSystem = notificationsSystem;
            _userSettings = userSettings;
            _results = new List<Action>();
        }

        protected override void StartCalculations()
        {
            if (_playerData.PlayerTribe != null)
            {
                var regionAlertLevels = new Dictionary<Region, AlertLevel>();

                var activeAlertNotifications = _notificationsSystem.GetActiveNotifications()
                    .Where(n => n.Data.ContainsKey("enemy_alert"))
                    .ToArray();

                if ((int)_userSettings.AlertLevel >= (int)AlertLevel.EnemySighted)
                {
                    var alertRegions = _worldManager.Tribes
                        .Where(t => _playerData.PlayerTribe.IsHostileTo(t))
                        .SelectMany(t => t.Armies.Where(a => _playerData.PlayerTribe.IsVisible(a.Cell)))
                        .Select(a => a.Cell.Region)
                        .Distinct()
                        .ToArray();

                    foreach (var region in alertRegions)
                    {
                        UpdateAlertLevels(regionAlertLevels, region, AlertLevel.EnemySighted);
                    }
                }
                if ((int)_userSettings.AlertLevel >= (int)AlertLevel.Invader)
                {
                    var alertRegions = _playerData.PlayerTribe.Settlements
                        .Select(s => s.Region)
                        .Where(r => r.WorldActors.OfType<Army>().Any(a => _playerData.PlayerTribe.IsVisible(a.Cell) && _playerData.PlayerTribe.IsHostileTo(a.Owner)))
                        .ToArray();

                    foreach (var region in alertRegions)
                    {
                        UpdateAlertLevels(regionAlertLevels, region, AlertLevel.Invader);
                    }
                }
                if ((int)_userSettings.AlertLevel >= (int)AlertLevel.SiegeInProgress)
                {
                    var alertRegions = _playerData.PlayerTribe.Settlements
                        .Where(s => s.IsUnderSiege)
                        .Select(s => s.Region)
                        .ToArray();

                    foreach (var region in alertRegions)
                    {
                        UpdateAlertLevels(regionAlertLevels, region, AlertLevel.SiegeInProgress);
                    }
                }

                foreach (var region in regionAlertLevels.Keys)
                {
                    if ((int)_userSettings.AlertLevel >= (int)regionAlertLevels[region])
                    {
                        var level = regionAlertLevels[region].ToString();
                        var title = string.Empty;
                        var icon = string.Empty;
                        var summary = string.Empty;
                        var soundEffect = string.Empty;

                        switch (regionAlertLevels[region])
                        {
                            case AlertLevel.Invader:
                                title = $"Invaders Near {region.Settlement?.Name}";
                                icon = "ui_notification_enemy_invader";
                                soundEffect = UiSounds.NotificationMinor;
                                break;
                            case AlertLevel.SiegeInProgress:
                                // This case is handled in ArmySkirmishInterrupter
                                break;
                            case AlertLevel.EnemySighted:
                            default:
                                title = $"Enemy Sighted in {region.Name}";
                                icon = "ui_notification_enemy_spotted";
                                soundEffect = UiSounds.NotificationMinor;
                                break;
                        }

                        if (!string.IsNullOrWhiteSpace(title))
                        {
                            var notification = activeAlertNotifications
                                .Where(n => n.Data.ContainsKey("enemy_alert") && n.Data["region"] == region.Id.ToString() && n.Data["level"] == level)
                                .FirstOrDefault();

                            if (notification != null)
                            {
                                notification.ExpireDate = _worldManager.Month + 2;
                            }
                            else
                            {
                                lock (LOCK)
                                {
                                    _results.Add(() =>
                                    {
                                        _notificationsSystem.AddNotification(new Notification
                                        {
                                            AutoOpen = false,
                                            ExpireDate = _worldManager.Month + 2,
                                            Handler = typeof(LookAtNotificationHandler).Name,
                                            Icon = icon,
                                            SoundEffect = soundEffect,
                                            QuickDismissable = false,
                                            DismissOnOpen = false,
                                            SummaryTitle = title,
                                            SummaryBody = "Click to zoom to region.",
                                            Data = new Dictionary<string, string>
                                            {
                                            { "enemy_alert", "yes" },
                                            { "level", level },
                                            { "region", region.Id.ToString()}
                                            }
                                        });
                                    });
                                }
                            }
                        }
                    }
                }
            }
        }

        private void UpdateAlertLevels(
            Dictionary<Region, AlertLevel> regionAlertLevels, 
            Region region, 
            AlertLevel alertLevel)
        {
            if (!regionAlertLevels.ContainsKey(region))
            {
                regionAlertLevels.Add(region, alertLevel);
            }
            else if ((int)regionAlertLevels[region] > (int)alertLevel)
            {
                regionAlertLevels[region] = alertLevel;
            }
        }

        protected override void ApplyResults()
        {
            Action[] localResults = null;

            lock(LOCK)
            {
                localResults = _results.ToArray();
                _results.Clear();
            }

            foreach(var result in localResults)
            {
                result();
            }
        }
    }
}
