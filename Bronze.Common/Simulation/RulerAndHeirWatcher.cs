﻿using Bronze.Common.UI.Modals;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System.Linq;

namespace Bronze.Common.Simulation
{
    public class RulerAndHeirWatcher : ISubSimulator, IWorldStateService
    {
        private readonly IWorldManager _worldManager;
        private readonly IPlayerDataTracker _playerData;
        private readonly IGamedataTracker _gamedataTracker;
        private readonly INotificationsSystem _notificationsSystem;
        private readonly IGenericNotificationBuilder _genericNotificationBuilder;
        private readonly IDialogManager _dialogManager;
        private readonly IWarHelper _warHelper;

        public RulerAndHeirWatcher(
            IWorldManager worldManager, 
            IPlayerDataTracker playerData,
            IGamedataTracker gamedataTracker,
            INotificationsSystem notificationsSystem,
            IGenericNotificationBuilder genericNotificationBuilder,
            IDialogManager dialogManager,
            IWarHelper warHelper)
        {
            _worldManager = worldManager;
            _playerData = playerData;
            _gamedataTracker = gamedataTracker;
            _notificationsSystem = notificationsSystem;
            _genericNotificationBuilder = genericNotificationBuilder;
            _dialogManager = dialogManager;
            _warHelper = warHelper;
        }
        
        public void Clear()
        {
        }
        
        public void Initialize(WorldState worldState)
        {
        }

        public void LoadCustomData(SerializedObject worldRoot)
        {
        }

        public void SaveCustomData(SerializedObject worldRoot)
        {
        }

        public void StepSimulation(double elapsedMonths)
        {
            for (var i = 0; i < _worldManager.Tribes.Count; i++)
            {
                var tribe = _worldManager.Tribes[i];

                if (!tribe.IsExtinct)
                {
                    if(tribe.Ruler == null || tribe.Ruler.IsDead)
                    {
                        NotablePerson newRuler;

                        if (tribe.Heir != null && !tribe.Heir.IsDead)
                        {
                            newRuler = tribe.Heir;
                            tribe.Heir = null;
                        }
                        else
                        {
                            newRuler = tribe.NotablePeople
                                .Where(p => p.InDynasty && !p.IsDead && p.IsMature)
                                .OrderBy(p => p.BirthDate)
                                .FirstOrDefault();
                        }

                        if(newRuler != null)
                        {
                            tribe.Ruler = newRuler;

                            if(tribe == _playerData.PlayerTribe)
                            {
                                _notificationsSystem.AddNotification(_genericNotificationBuilder
                                    .BuildPersonNotification(
                                        "ui_notification_newruler",
                                        $"New Ruler of {tribe.Name}",
                                        $"{tribe.Ruler.Name} is the new ruler of {tribe.Name}.",
                                        $"New Ruler of {tribe.Name}",
                                        $"The people of rejoice as {tribe.Ruler.Name} is crowned as the new ruler of the tribe. Long may {(tribe.Ruler.IsMale ? "he" : "she")} reign.",
                                        tribe.Ruler,
                                        null));
                            }
                            else if(_playerData.PlayerTribe.HasKnowledgeOf(tribe))
                            {
                                _notificationsSystem.AddNotification(_genericNotificationBuilder
                                    .BuildTribeNotification(
                                        "ui_notification_newruler",
                                        $"New Ruler of {tribe.Name}",
                                        $"{tribe.Ruler.Name} is the new ruler of {tribe.Name}.",
                                        $"New Ruler of {tribe.Name}",
                                        $"Travellers report that {tribe.Ruler.Name} has been crowned as the new ruler of {tribe.Name}",
                                        tribe));
                            }
                        }
                        else if(!tribe.IsBandits)
                        {
                            tribe.IsExtinct = true;

                            foreach(var settlement in tribe.Settlements.ToArray())
                            {
                                tribe.Abandon(settlement);
                            }
                            foreach(var army in tribe.Armies.ToArray())
                            {
                                // TODO kill the army?
                            }

                            foreach (var war in tribe.CurrentWars.ToArray())
                            {
                                _warHelper.EndWar(war);
                            }

                            if (tribe == _playerData.PlayerTribe && !_worldManager.IsPreview)
                            {
                                _dialogManager.PopAllModals();
                                _dialogManager.PushModal<GameoverModal>();
                            }
                            else if (_playerData.PlayerTribe.HasKnowledgeOf(tribe))
                            {
                                _notificationsSystem.AddNotification(_genericNotificationBuilder
                                    .BuildTribeNotification(
                                        "ui_notification_extinct",
                                        $"Tribe Fallen",
                                        $"The {tribe.Name} are no more.",
                                        $"{tribe.Name} Fallen",
                                        $"The ruling dynasty of the {tribe.Name} have perished, and so the tribe is no more.",
                                        tribe));
                            }
                        }
                    }

                    if (tribe.Heir == null || tribe.Heir.IsDead || tribe.Heir.Owner != tribe)
                    {
                        NotablePerson newHeir = null;

                        if(tribe.Ruler != null)
                        {
                            newHeir = tribe.Ruler.Children
                                .Where(p => p.InDynasty && !p.IsDead && p.Owner == tribe && p.IsMature)
                                .Where(p => _worldManager.Month - p.BirthDate > p.Race.AgeMaturity)
                                .OrderBy(p => p.BirthDate)
                                .FirstOrDefault();
                        }
                        
                        if(newHeir == null)
                        {
                            newHeir = tribe.NotablePeople
                                .Where(p => p != p.Owner.Ruler)
                                .Where(p => p.InDynasty && !p.IsDead && p.Owner == tribe && p.IsMature)
                                .Where(p => _worldManager.Month - p.BirthDate > p.Race.AgeMaturity)
                                .OrderBy(p => p.BirthDate)
                                .FirstOrDefault();

                        }
                        
                        if (newHeir != null)
                        {
                            tribe.Heir = newHeir;

                            if (tribe == _playerData.PlayerTribe)
                            {
                                _notificationsSystem.AddNotification(_genericNotificationBuilder
                                    .BuildPersonNotification(
                                    "ui_notification_newheir",
                                    $"New Heir of {tribe.Name}",
                                    $"{tribe.Heir.Name} is the new heir of {tribe.Name}.",
                                    $"New Heir of {tribe.Name}",
                                    $"The people of rejoice as {tribe.Heir.Name} is chosen as the new heir of the tribe.",
                                    tribe.Heir,
                                    null));
                            }
                        }
                    }
                }
            }
        }
    }
}
