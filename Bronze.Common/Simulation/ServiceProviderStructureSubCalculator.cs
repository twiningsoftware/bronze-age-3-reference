﻿using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.Simulation
{
    public class ServiceProviderStructureSubCalculator : ISettlementSubcalcuator
    {
        public void DoCalculations(CalculationResults results)
        {
            var serviceProviders = results.Structures
                .OfType<IServiceProviderStructure>()
                .Where(s => !s.UnderConstruction)
                .ToArray();

            var serviceProvidersByType = serviceProviders
                .GroupBy(s => s.ServiceType)
                .ToArray();

            var structuresToServe = results.Structures;

            var tilesToStructures = structuresToServe
                .SelectMany(h => h.Footprint.Select(t => Tuple.Create(t, h)))
                .ToDictionary(t => t.Item1, t => t.Item2);

            var tilesServed = new Dictionary<IServiceProviderStructure, IEnumerable<Tile>>();
            var structuresServed = serviceProviders
                .ToDictionary(s => s, s => new List<IStructure>());

            foreach (var group in serviceProvidersByType)
            {
                var structureToProvider = structuresToServe
                    .ToDictionary(h => h, h => (IServiceProviderStructure)null);
                
                foreach (var provider in group)
                {
                    var servedTiles = provider.CalculateServiceArea();

                    tilesServed.Add(provider, servedTiles);

                    var servedStructures = servedTiles
                        .SelectMany(t => t.OrthoNeighbors)
                        .Where(t => tilesToStructures.ContainsKey(t))
                        .Select(t => tilesToStructures[t])
                        .Where(s => provider.CanService(s))
                        .Distinct()
                        .ToArray();

                    foreach (var structure in servedStructures)
                    {
                        if (structureToProvider[structure] == null 
                            || structure.Center.DistanceSq(provider.Center) < structure.Center.DistanceSq(structureToProvider[structure].Center))
                        {
                            structureToProvider[structure] = provider;
                        }
                    }
                }

                foreach (var structure in structureToProvider.Keys)
                {
                    if (structureToProvider[structure] != null)
                    {
                        structuresServed[structureToProvider[structure]].Add(structure);
                    }
                }
            }

            foreach (var provider in serviceProviders)
            {
                results.ServiceProviders.Add(provider, structuresServed[provider].OfType<IEconomicActor>().ToArray());
            }
            
            results.ApplyResultsFunctions.Add(() =>
            {
                foreach (var provider in serviceProviders)
                {
                    provider.ClearServicedActors();
                    provider.TilesServed.Clear();
                    provider.TilesServed.AddRange(tilesServed[provider]);

                    foreach(var structure in structuresServed[provider])
                    {
                        provider.AddServicedActor(structure);
                    }
                }
            });
        }
    }
}
