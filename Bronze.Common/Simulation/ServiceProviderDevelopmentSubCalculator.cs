﻿using System.Collections.Generic;
using System.Linq;
using Bronze.Common.Data.World.CellDevelopments;
using Bronze.Contracts.Data.Simulation;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;

namespace Bronze.Common.Simulation
{
    public class ServiceProviderDevelopmentSubCalculator : ISettlementSubcalcuator
    {
        public void DoCalculations(CalculationResults results)
        {
            var serviceProviders = results.CellDevelopments
                .OfType<IServiceProviderDevelopment>()
                .Where(s => !s.UnderConstruction)
                .ToArray();

            var serviceProvidersByType = serviceProviders
                .GroupBy(s => s.ServiceType)
                .ToArray();

            var developmentsToServe = results.CellDevelopments;

            var cellToStructure = developmentsToServe
                .ToDictionary(d => d.Cell, d => d);

            var cellsServed = new Dictionary<IServiceProviderDevelopment, IEnumerable<Cell>>();
            var structuresServed = serviceProviders
                .ToDictionary(s => s, s => new List<ICellDevelopment>());

            foreach (var group in serviceProvidersByType)
            {
                var developmentToProvider = developmentsToServe
                    .ToDictionary(h => h, h => (IServiceProviderDevelopment)null);
                
                foreach (var provider in group)
                {
                    var servedCells = provider.CalculateServiceArea();

                    cellsServed.Add(provider, servedCells);

                    var servedDevelopments = servedCells
                        .Where(t => cellToStructure.ContainsKey(t))
                        .Select(t => cellToStructure[t])
                        .Where(s => provider.CanService(s))
                        .Distinct()
                        .ToArray();

                    foreach (var development in servedDevelopments)
                    {
                        if (developmentToProvider[development] == null)
                        {
                            developmentToProvider[development] = provider;
                        }
                    }
                }

                foreach (var structure in developmentToProvider.Keys)
                {
                    if (developmentToProvider[structure] != null)
                    {
                        structuresServed[developmentToProvider[structure]].Add(structure);
                    }
                }
            }

            foreach (var provider in serviceProviders)
            {
                results.ServiceProviders.Add(provider, structuresServed[provider].OfType<IEconomicActor>().ToArray());
            }

            results.ApplyResultsFunctions.Add(() =>
            {
                foreach (var provider in serviceProviders)
                {
                    provider.ClearServicedActors();
                    provider.CellsServed.Clear();
                    provider.CellsServed.AddRange(cellsServed[provider]);

                    foreach(var structure in structuresServed[provider])
                    {
                        provider.AddServicedActor(structure);
                    }
                }
            });
        }
    }
}
