﻿using Bronze.Contracts;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.Simulation
{
    public class NewSettlementWatcher : AbstractBackgroundCalculatorSubSimulator, IWorldStateService
    {
        private static readonly object LOCK = new object();
        
        private readonly IPlayerDataTracker _playerData;
        private readonly INotificationsSystem _notificationsSystem;
        private readonly IGenericNotificationBuilder _genericNotificationBuilder;
        private readonly IWorldManager _worldManager;
        
        private List<Action> _results;
        private Dictionary<string, bool> _knownCities;
        
        public NewSettlementWatcher(
            IGameInterface gameInterface,
            IPlayerDataTracker playerData,
            INotificationsSystem notificationsSystem,
            IGenericNotificationBuilder genericNotificationBuilder,
            IWorldManager worldManager)
            :base(gameInterface)
        {
            _playerData = playerData;
            _notificationsSystem = notificationsSystem;
            _genericNotificationBuilder = genericNotificationBuilder;
            _worldManager = worldManager;
            _results = new List<Action>();
            _knownCities = new Dictionary<string, bool>();
        }

        protected override void StartCalculations()
        {
            for(var i = 0; i < _worldManager.Tribes.Count; i++)
            {
                for(var j = 0; j < _worldManager.Tribes[i].Settlements.Count; j++)
                {
                    var settlement = _worldManager.Tribes[i].Settlements[j];

                    if(!_knownCities.ContainsKey(settlement.Id))
                    {
                        _knownCities.Add(settlement.Id, settlement.Districts.Any());

                        if(_worldManager.Tribes[i] == _playerData.PlayerTribe 
                            && _playerData.PlayerTribe.Settlements.Count > 1)
                        {
                            lock(LOCK)
                            {
                                _results.Add(() =>
                                {
                                    _notificationsSystem.AddNotification(_genericNotificationBuilder
                                        .BuildSimpleNotification(
                                            "ui_notification_new_village",
                                            $"{settlement.Name} Founded",
                                            $"Your people have founded a new settlement.",
                                            $"{settlement.Name} Founded",
                                            $"Your people have founded a new settlement in {settlement.Region.Name}, calling it {settlement.Name}.",
                                            settlement.Region.Center));
                                });
                            }
                        }
                    }
                    else if(settlement.Districts.Any() && !_knownCities[settlement.Id])
                    {
                        _knownCities[settlement.Id] = true;

                        if (_worldManager.Tribes[i] == _playerData.PlayerTribe)
                        {
                            lock (LOCK)
                            {
                                _results.Add(() =>
                                {
                                    _notificationsSystem.AddNotification(_genericNotificationBuilder
                                        .BuildSimpleNotification(
                                            "ui_notification_new_city",
                                            $"{settlement.Name} Developed",
                                            $"One of your settlements has developed into a city.",
                                            $"{settlement.Name} Developed",
                                            $"Your settlement of {settlement.Name} has developed into a full city.",
                                            settlement.Region.Center));
                                });
                            }
                        }
                    }
                }
            }
        }
        
        protected override void ApplyResults()
        {
            Action[] localResults = null;

            lock(LOCK)
            {
                localResults = _results.ToArray();
                _results.Clear();
            }

            foreach(var result in localResults)
            {
                result();
            }
        }

        public void Clear()
        {
            _knownCities.Clear();
        }

        public void Initialize(WorldState worldState)
        {
        }

        public void SaveCustomData(SerializedObject worldRoot)
        {
            var serviceRoot = worldRoot.CreateChild("new_settlement_watcher");

            foreach(var kvp in _knownCities)
            {
                var child = serviceRoot.CreateChild("kvp");
                child.Set("settlement_id", kvp.Key);
                child.Set("is_city", kvp.Value);
            }
        }

        public void LoadCustomData(SerializedObject worldRoot)
        {
            var serviceRoot = worldRoot.GetOptionalChild("new_settlement_watcher");
            
            if (serviceRoot != null)
            {
                var allSettlements = _worldManager.Tribes
                    .SelectMany(t => t.Settlements)
                    .ToArray();

                foreach (var child in serviceRoot.GetChildren("kvp"))
                {
                    _knownCities.Add(
                        child.GetString("settlement_id"), 
                        child.GetBool("is_city"));
                }
            }
        }
    }
}
