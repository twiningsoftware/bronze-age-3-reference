﻿using Bronze.Common.UI.Modals;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System.Linq;

namespace Bronze.Common.Simulation
{
    public class TribeExtinctionWatcher : ISubSimulator, IWorldStateService
    {
        private readonly IWorldManager _worldManager;
        private readonly IPlayerDataTracker _playerData;
        private readonly IGamedataTracker _gamedataTracker;
        private readonly INotificationsSystem _notificationsSystem;
        private readonly IGenericNotificationBuilder _genericNotificationBuilder;
        private readonly IDialogManager _dialogManager;
        private readonly IWarHelper _warHelper;

        public TribeExtinctionWatcher(
            IWorldManager worldManager, 
            IPlayerDataTracker playerData,
            IGamedataTracker gamedataTracker,
            INotificationsSystem notificationsSystem,
            IGenericNotificationBuilder genericNotificationBuilder,
            IDialogManager dialogManager,
            IWarHelper warHelper)
        {
            _worldManager = worldManager;
            _playerData = playerData;
            _gamedataTracker = gamedataTracker;
            _notificationsSystem = notificationsSystem;
            _genericNotificationBuilder = genericNotificationBuilder;
            _dialogManager = dialogManager;
            _warHelper = warHelper;
        }
        
        public void Clear()
        {
        }
        
        public void Initialize(WorldState worldState)
        {
        }

        public void LoadCustomData(SerializedObject worldRoot)
        {
        }

        public void SaveCustomData(SerializedObject worldRoot)
        {
        }

        public void StepSimulation(double elapsedMonths)
        {
            for (var i = 0; i < _worldManager.Tribes.Count; i++)
            {
                var tribe = _worldManager.Tribes[i];

                if (!tribe.IsExtinct)
                {
                    if(!tribe.Settlements.Any() && !tribe.Armies.Any() && !tribe.IsBandits)
                    {
                        tribe.IsExtinct = true;

                        foreach(var war in tribe.CurrentWars.ToArray())
                        {
                            _warHelper.EndWar(war);
                        }
                        
                        if(tribe == _playerData.PlayerTribe && !_worldManager.IsPreview)
                        {
                            _dialogManager.PopAllModals();
                            _dialogManager.PushModal<GameoverModal>();
                        }
                        else if (_playerData.PlayerTribe.HasKnowledgeOf(tribe))
                        {
                            _notificationsSystem.AddNotification(_genericNotificationBuilder
                                .BuildTribeNotification(
                                    "ui_notification_extinct",
                                    $"Tribe Fallen",
                                    $"The {tribe.Name} are no more.",
                                    $"{tribe.Name} Fallen",
                                    $"The ruling dynasty of the {tribe.Name} have perished, and so the tribe is no more.",
                                    tribe));
                        }
                    }
                }
            }
        }
    }
}
