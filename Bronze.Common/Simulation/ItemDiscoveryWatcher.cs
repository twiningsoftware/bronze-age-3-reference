﻿using Bronze.Common.Data.World.CellDevelopments;
using Bronze.Common.NotificationBuilders;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.UI;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.Common.Simulation
{
    public class ItemDiscoveryWatcher : ISubSimulator, IWorldStateService
    {
        private readonly IWorldManager _worldManager;
        private readonly IPlayerDataTracker _playerData;
        private readonly IGamedataTracker _gamedataTracker;
        private readonly INotificationsSystem _notificationsSystem;
        private readonly IGenericNotificationBuilder _genericNotificationBuilder;
        private readonly List<string> _playerKnownRecruitmentCategories;

        private int _nextTribeIndex;
        private int _nextSettlementIndex;
        
        public ItemDiscoveryWatcher(
            IWorldManager worldManager, 
            IPlayerDataTracker playerData,
            IGamedataTracker gamedataTracker,
            INotificationsSystem notificationsSystem,
            IGenericNotificationBuilder genericNotificationBuilder)
        {
            _worldManager = worldManager;
            _playerData = playerData;
            _gamedataTracker = gamedataTracker;
            _notificationsSystem = notificationsSystem;
            _genericNotificationBuilder = genericNotificationBuilder;
            _playerKnownRecruitmentCategories = new List<string>();

            _nextTribeIndex = 0;
            _nextSettlementIndex = 0;
        }
        
        // TODO also handle discovering new points of interest

        public void Clear()
        {
            _nextTribeIndex = 0;
            _nextSettlementIndex = 0;
            _playerKnownRecruitmentCategories.Clear();
        }
        
        public void Initialize(WorldState worldState)
        {
        }

        public void LoadCustomData(SerializedObject worldRoot)
        {
            var serviceRoot = worldRoot.GetOptionalChild("discovery_watcher");

            if(serviceRoot != null)
            {
                _playerKnownRecruitmentCategories
                    .AddRange(serviceRoot.GetChildren("recruits")
                        .Select(c => c.GetString("key")));
            }
        }

        public void SaveCustomData(SerializedObject worldRoot)
        {
            var serviceRoot = worldRoot.CreateChild("discovery_watcher");

            foreach(var key in _playerKnownRecruitmentCategories)
            {
                serviceRoot.CreateChild("recruits").Set("key", key);
            }
        }

        public void StepSimulation(double elapsedMonths)
        {
            if(_worldManager.Tribes.Any())
            {
                _nextTribeIndex = _nextTribeIndex % _worldManager.Tribes.Count;
                var tribe = _worldManager.Tribes[_nextTribeIndex];

                foreach(var item in _gamedataTracker.Items)
                {
                    if(!tribe.KnownItems[item] && tribe.Settlements.Any(s=>s.ItemAvailability[item]))
                    {
                        tribe.DiscoverItem(item);

                        if(tribe == _playerData.PlayerTribe)
                        {
                            _notificationsSystem.AddNotification(new Notification
                            {
                                AutoOpen = false,
                                ExpireDate = _worldManager.Month + 2,
                                Handler = typeof(ItemDiscoveryNotificationHandler).Name,
                                Icon = item.IconKeyBig,
                                SoundEffect = UiSounds.NotificationMinor,
                                QuickDismissable = true,
                                DismissOnOpen = true,
                                SummaryTitle = "New Resource Discovered",
                                SummaryBody = $"You have discovered {item.Name}.",
                                Data = new Dictionary<string, string> { { "item", item.Name } }
                            });
                        }
                    }
                }
                
                _nextTribeIndex += 1;
            }

            if(_playerData.PlayerTribe.Settlements.Any())
            {
                _nextSettlementIndex = _nextSettlementIndex % _playerData.PlayerTribe.Settlements.Count;
                var settlement = _playerData.PlayerTribe.Settlements[_nextSettlementIndex];

                if (settlement.RecruitmentSlotsMax.Any())
                {
                    foreach (var category in _gamedataTracker.RecruitmentSlotCategories)
                    {
                        if (settlement.RecruitmentSlotsMax[category] > 0)
                        {
                            var key = settlement.Id + category.Id;
                            if (!_playerKnownRecruitmentCategories.Contains(key))
                            {
                                _playerKnownRecruitmentCategories.Add(key);

                                _notificationsSystem.AddNotification(
                                    _genericNotificationBuilder.BuildSimpleNotification(
                                        "ui_notification_recruits",
                                        $"{category.Name} Recruits Training",
                                        $"{category.Name} recruits training in {settlement.Name}.",
                                        $"{category.Name} Recruits Training",
                                        $"{category.Name} recruits have started training in {settlement.Name} and will soon be available for recruitment.",
                                        settlement.EconomicActors
                                            .OfType<ICellDevelopment>()
                                            .Where(cd => cd is VillageDevelopment || cd is DistrictDevelopment)
                                            .Select(cd => cd.Cell.Position)
                                            .FirstOrDefault()));
                            }
                        }
                    }
                }

                _nextSettlementIndex += 1;
            }
        }
    }
}
