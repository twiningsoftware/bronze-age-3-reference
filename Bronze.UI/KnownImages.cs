﻿namespace Bronze.UI
{
    public static class KnownImages
    {
        public static readonly string MissingImage = "missing_image";
        public static readonly string Title = "bronze_title";

        public static readonly string BigIconUpgrade = "ui_icon_upgrade";
        public static readonly string BigIconAttention = "ui_icon_attention";
        public static readonly string BigIconUpgrading = "ui_icon_upgrading";
        public static readonly string BigIconHappy = "ui_icon_happy";
        public static readonly string BigIconUnhappy = "ui_icon_unhappy";

        public static readonly string IconSpeedPause = "ui_icon_pause";
        public static readonly string IconSpeedNormal = "ui_icon_normal";
        public static readonly string IconSpeedFast = "ui_icon_fast";
        public static readonly string IconSpeedSuperfast = "ui_icon_superfast";
        
        public static readonly string IconHealth = "ui_icon_health";
        public static readonly string IconStrength = "ui_icon_strength";
        public static readonly string IconDefense = "ui_icon_defense";
        public static readonly string IconAttackMelee = "ui_icon_attack_melee";
        public static readonly string IconAttackRanged = "ui_icon_attack_ranged";
        public static readonly string IconTransportCapacity = "ui_icon_transport_capacity";
        public static readonly string IconVision = "ui_icon_vision";
        public static readonly string IconRangedAttackRange = "ui_icon_ranged_range";
        public static readonly string IconTime = "ui_icon_time";

        public static readonly string IconCanTrade = "ui_icon_can_trade";
        public static readonly string IconCanCapture = "ui_icon_can_capture";
        public static readonly string IconCanBeTransported = "ui_icon_can_be_transported";
        public static readonly string IconCanSettle = "ui_icon_can_settle";

        public static readonly string IconView = "ui_icon_view";
        public static readonly string IconWarning = "ui_icon_warning";

        public static readonly string ModeBuild = "ui_mode_build";
        public static readonly string ModePops = "ui_mode_pops";
        public static readonly string ModeTrade = "ui_mode_trade";
        public static readonly string ModeMilitary = "ui_mode_military";
        public static readonly string ModeSettlements = "ui_mode_settlement";

        public static readonly string IconHostilityWar = "diplo_state_war";
        public static readonly string IconHostilityHostile = "diplo_state_hostile";
        public static readonly string IconHostilityNeutral = "diplo_state_neutral";
        public static readonly string IconHostilityAlly = "diplo_state_ally";
        public static readonly string IconIsUpgrading = "ui_is_upgrading";
        public static readonly string IconUpgrade = "ui_upgrade";
        public static readonly string IconDowngrade = "ui_downgrade";

        public static readonly string IconFactoryNoRecipie = "ui_icon_factory_no_recipie";
        public static readonly string IconFactoryNeedsInputs = "ui_icon_factory_no_inputs";
        public static readonly string IconFactoryOutputsFull = "ui_icon_factory_outputs_full";
        public static readonly string IconFactoryNoInputPaths = "ui_icon_factory_no_input_paths";

        public static readonly string IconNotEnoughWorkers = "ui_icon_not_enough_workers";

        public static readonly string IconHouseSatisfactionMed = "ui_icon_house_satisfaction_med";
        public static readonly string IconHouseSatisfactionLow = "ui_icon_house_satisfaction_low";
        public static readonly string IconHouseEmpty = "ui_icon_empty_house";
    }
}
