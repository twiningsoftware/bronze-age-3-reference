﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;

namespace Bronze.UI.Modals
{
    public class ConfirmModal : AbstractModal, IModal<ConfirmModalContext>
    {
        protected override int ModalWidth => 250;
        protected override int ModalHeight => Elements.Max(e => e.Bounds.Height);

        public override bool PausesGame => true;

        private readonly IDialogManager _dialogManager;

        private ConfirmModalContext _context;
        
        public ConfirmModal(IDialogManager dialogManager)
            : base(dialogManager)
        {
            _dialogManager = dialogManager;
        }
        
        public void Initialize(ConfirmModalContext initValue)
        {
            _context = initValue;
        }

        public override void OnLoad()
        {
            base.OnLoad();

            Elements.Add(new StackGroup
            {
                HorizontalContentAlignment = HorizontalAlignment.Middle,
                VerticalContentAlignment = VerticalAlignment.Middle,
                Width = Width.Relative(1f),
                Orientation = Orientation.Vertical,
                Children = new List<IUiElement>
                {
                    new Spacer(0,20),
                    new Text(_context.Title),
                    new Spacer(10,10),
                    new Text(_context.Body)
                    {
                        Width = Width.Fixed(ModalWidth - 20)
                    },
                    new Spacer(10,10),
                    new StackGroup
                    {
                        Orientation = Orientation.Horizontal,
                        Children = new List<IUiElement>
                        {
                            new TextButton("Yes",
                            () =>
                            {
                                _dialogManager.PopModal();
                                _context.YesAction();
                            },
                            KeyCode.Y),
                            new Spacer(30,10),
                            new TextButton("No",
                            () =>
                            {
                                _dialogManager.PopModal();
                                _context.NoAction();
                            },
                            KeyCode.N),
                        }
                    },
                    new Spacer(0,20),
                }
            });
        }

        public override void Update(
            float elapsedSeconds, 
            InputState inputState, 
            IAudioService audioService)
        {
            base.Update(elapsedSeconds, inputState, audioService);

            if(inputState.JustPressed(KeyCode.Escape))
            {
                _dialogManager.PopModal();
                _context.NoAction();
                audioService.PlaySound(UiSounds.Click);
            }
        }
    }

    public class ConfirmModalContext
    {
        public string Title { get; set; }
        public string Body { get; set; }
        public Action YesAction { get; set; }
        public Action NoAction { get; set; }
    }
}
