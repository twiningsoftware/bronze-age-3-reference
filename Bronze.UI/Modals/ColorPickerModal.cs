﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;
using Bronze.UI.Widgets;

namespace Bronze.UI.Modals
{
    public class ColorPickerModal : AbstractModal, IModal<Action<BronzeColor>>
    {
        protected override int ModalWidth => Elements.Max(e => e.Bounds.Width);
        protected override int ModalHeight => Elements.Max(e => e.Bounds.Height);
        public override bool PausesGame => true;

        private readonly IDialogManager _dialogManager;
        private readonly BronzeColor[] _colorOptions;
        private Action<BronzeColor> _callback;

        public ColorPickerModal(
            IDialogManager dialogManager)
            : base(dialogManager)
        {
            _dialogManager = dialogManager;

            _colorOptions = Enum.GetValues(typeof(BronzeColor))
                .Cast<BronzeColor>()
                .Where(c => c != BronzeColor.None)
                .ToArray();
        }

        public void Initialize(Action<BronzeColor> initValue)
        {
            _callback = initValue;
        }

        public override void OnLoad()
        {
            base.OnLoad();

            var perRow = 4;

            var column = new StackGroup
            {
                Orientation = Orientation.Vertical
            };

            for (var i = 0; i < _colorOptions.Length; i += perRow)
            {
                column.Children.Add(new StackGroup
                {
                    Orientation = Orientation.Horizontal,
                    Children = _colorOptions.Skip(i).Take(perRow)
                        .Select(c => new IconButton(
                                "ui_colorization_square",
                                string.Empty,
                                () =>
                                {
                                    _dialogManager.PopModal();
                                    _callback(c);
                                })
                        {
                            ColorizationUpdater = () => c
                        })
                        .ToList<IUiElement>()
                });
            }

            Elements.Add(new StackGroup
            {
                Orientation = Orientation.Horizontal,
                Children = new List<IUiElement>
                {
                    new Spacer(5, 0),
                    new StackGroup
                    {
                        Orientation = Orientation.Vertical,
                        HorizontalContentAlignment = HorizontalAlignment.Middle,
                        Children = new List<IUiElement>
                        {
                            new Spacer(0, 5),
                            column,
                            new Spacer(0, 10),
                            new TextButton(
                                "Cancel (Esc)",
                                () => _dialogManager.PopModal(),
                                KeyCode.Escape),
                            new Spacer(0, 5),
                        },
                    },
                    new Spacer(5, 0)
                }
            });
        }
    }
}
