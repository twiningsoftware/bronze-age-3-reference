﻿using System.Collections.Generic;
using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;

namespace Bronze.UI.Modals
{
    public abstract class AbstractModal : IModal
    {
        private readonly IDialogManager _dialogManager;
        protected List<IUiElement> Elements { get; }

        protected abstract int ModalWidth { get; }
        protected abstract int ModalHeight { get; }
        public abstract bool PausesGame { get; }

        public AbstractModal(IDialogManager dialogManager)
        {
            _dialogManager = dialogManager;
            Elements = new List<IUiElement>();
        }

        public virtual void OnLoad()
        {
            Elements.Clear();
        }

        public virtual void OnUnload()
        {
            Elements.Clear();
        }

        public virtual void Update(
            float elapsedSeconds,
            InputState inputState,
            IAudioService audioService)
        {
            if (inputState.MousePressed
                && inputState.FocusedElement != null
                && !inputState.FocusedElement.Bounds.Contains(inputState.MousePosition))
            {
                inputState.FocusedElement = null;
            }

            foreach (var element in Elements.ToArray())
            {
                element.Update(inputState, audioService, elapsedSeconds);
            }

            if (inputState.JustPressed(KeyCode.Escape) && inputState.FocusedElement != null)
            {
                inputState.FocusedElement = null;
            }
        }

        public void Layout(IDrawingService drawingService)
        {
            var xOffset = (drawingService.DisplayWidth - ModalWidth) / 2;
            var yOffset = (drawingService.DisplayHeight - ModalHeight) / 2;

            var bounds = new Rectangle(
                xOffset,
                yOffset,
                ModalWidth,
                ModalHeight);

            foreach (var element in Elements.ToArray())
            {
                element.DoLayout(drawingService, bounds);
            }

            xOffset = (drawingService.DisplayWidth - ModalWidth) / 2;
            yOffset = (drawingService.DisplayHeight - ModalHeight) / 2;
            bounds = new Rectangle(
                xOffset,
                yOffset,
                ModalWidth,
                ModalHeight);

            foreach (var element in Elements.ToArray())
            {
                element.DoLayout(drawingService, bounds);
            }
        }

        public virtual void Draw(
            float elapsedSeconds,
            IDrawingService drawingService)
        {
            var xOffset = (drawingService.DisplayWidth - ModalWidth) / 2;
            var yOffset = (drawingService.DisplayHeight - ModalHeight) / 2;

            var bounds = new Rectangle(
                xOffset,
                yOffset,
                ModalWidth,
                ModalHeight);

            drawingService.DrawSlicedImage(UiImages.ModalBackground, bounds, 1000, false);

            foreach (var element in Elements.ToArray())
            {
                element.Draw(drawingService, 1001);
            }
        }
    }
}
