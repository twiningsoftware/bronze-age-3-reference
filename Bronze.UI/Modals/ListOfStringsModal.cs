﻿using System.Collections.Generic;
using System.Linq;
using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;

namespace Bronze.UI.Modals
{
    public class ListOfStringsModal : AbstractModal, IModal<IEnumerable<string>>
    {
        private const int CONTENT_WIDTH = 400;

        protected override int ModalWidth => Elements.Max(e=>e.Bounds.Width);
        protected override int ModalHeight => Elements.Max(e => e.Bounds.Height);

        public override bool PausesGame => false;

        private readonly IDialogManager _dialogManager;
        private IEnumerable<string> _strings;

        public ListOfStringsModal(IDialogManager dialogManager)
            : base(dialogManager)
        {
            _dialogManager = dialogManager;
        }

        public void Initialize(IEnumerable<string> initValue)
        {
            _strings = initValue;
        }

        public override void OnLoad()
        {
            base.OnLoad();

            Elements.Add(new StackGroup
            {
                HorizontalContentAlignment = HorizontalAlignment.Middle,
                VerticalContentAlignment = VerticalAlignment.Middle,
                Orientation = Orientation.Vertical,
                Children = new List<IUiElement>
                {
                    new Spacer(CONTENT_WIDTH + 40, 20),
                    new ScrollableList(5,
                        _strings
                        .Select(s=>new Text(s)
                        {
                            Width = Width.Fixed(CONTENT_WIDTH)
                        })
                        .ToArray()),
                    new Spacer(CONTENT_WIDTH + 40, 10),
                    new TextButton("Close (esc)",
                        () =>
                        {
                            _dialogManager.PopModal();
                        },
                        KeyCode.Escape),
                    new Spacer(CONTENT_WIDTH + 40, 20)
                }
            });
        }
    }
}
