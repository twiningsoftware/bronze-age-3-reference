﻿using Bronze.Contracts.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bronze.UI
{
    public static class UiUtil
    {
        private static readonly Dictionary<KeyCode, Tuple<string, string>> KeyToCharMappings =
            new Dictionary<KeyCode, Tuple<string, string>>
            {
                {KeyCode.D1, Tuple.Create("1", "!")},
                {KeyCode.D2, Tuple.Create("2", "@")},
                {KeyCode.D3, Tuple.Create("3", "#")},
                {KeyCode.D4, Tuple.Create("4", "$")},
                {KeyCode.D5, Tuple.Create("5", "%")},
                {KeyCode.D6, Tuple.Create("6", "^")},
                {KeyCode.D7, Tuple.Create("7", "&")},
                {KeyCode.D8, Tuple.Create("8", "*")},
                {KeyCode.D9, Tuple.Create("9", "(")},
                {KeyCode.D0, Tuple.Create("0", ")")},
                {KeyCode.NumPad1, Tuple.Create("1", "1")},
                {KeyCode.NumPad2, Tuple.Create("2", "2")},
                {KeyCode.NumPad3, Tuple.Create("3", "3")},
                {KeyCode.NumPad4, Tuple.Create("4", "4")},
                {KeyCode.NumPad5, Tuple.Create("5", "5")},
                {KeyCode.NumPad6, Tuple.Create("6", "6")},
                {KeyCode.NumPad7, Tuple.Create("7", "7")},
                {KeyCode.NumPad8, Tuple.Create("8", "8")},
                {KeyCode.NumPad9, Tuple.Create("9", "9")},
                {KeyCode.NumPad0, Tuple.Create("0", "0")},
                {KeyCode.OemMinus, Tuple.Create("-", "_")},
                {KeyCode.OemPlus, Tuple.Create("=", "+")},
                {KeyCode.OemOpenBrackets, Tuple.Create("[", "{")},
                {KeyCode.OemCloseBrackets, Tuple.Create("]", "}")},
                {KeyCode.OemPipe, Tuple.Create("\\", "|")},
                {KeyCode.OemSemicolon, Tuple.Create(";", ":")},
                {KeyCode.OemQuotes, Tuple.Create("\'", "\"")},
                {KeyCode.A, Tuple.Create("a", "A") },
                {KeyCode.B, Tuple.Create("b", "B") },
                {KeyCode.C, Tuple.Create("c", "C") },
                {KeyCode.D, Tuple.Create("d", "D") },
                {KeyCode.E, Tuple.Create("e", "E") },
                {KeyCode.F, Tuple.Create("f", "F") },
                {KeyCode.G, Tuple.Create("g", "G") },
                {KeyCode.H, Tuple.Create("h", "H") },
                {KeyCode.I, Tuple.Create("i", "I") },
                {KeyCode.J, Tuple.Create("j", "J") },
                {KeyCode.K, Tuple.Create("k", "K") },
                {KeyCode.L, Tuple.Create("l", "L") },
                {KeyCode.M, Tuple.Create("m", "M") },
                {KeyCode.N, Tuple.Create("n", "N") },
                {KeyCode.O, Tuple.Create("o", "O") },
                {KeyCode.P, Tuple.Create("p", "P") },
                {KeyCode.Q, Tuple.Create("q", "Q") },
                {KeyCode.R, Tuple.Create("r", "R") },
                {KeyCode.S, Tuple.Create("s", "S") },
                {KeyCode.T, Tuple.Create("t", "T") },
                {KeyCode.U, Tuple.Create("u", "U") },
                {KeyCode.V, Tuple.Create("v", "V") },
                {KeyCode.W, Tuple.Create("w", "W") },
                {KeyCode.X, Tuple.Create("x", "X") },
                {KeyCode.Y, Tuple.Create("y", "Y") },
                {KeyCode.Z, Tuple.Create("z", "Z") },
                {KeyCode.OemComma, Tuple.Create(",", "<") },
                {KeyCode.OemPeriod, Tuple.Create(".", ">") },
                {KeyCode.OemQuestion, Tuple.Create("/", "?") },
                {KeyCode.Space, Tuple.Create(" ", " ") },
            };

        public static IEnumerable<KeyCode> TextInputKeys => Enum.GetValues(typeof(KeyCode)).Cast<KeyCode>();

        public static string InputKeyToText(KeyCode keyCode, bool shiftDown)
        {
            if (KeyToCharMappings.ContainsKey(keyCode))
            {
                if(shiftDown)
                {
                    return KeyToCharMappings[keyCode].Item2;
                }
                else
                {
                    return KeyToCharMappings[keyCode].Item1;
                }   
            }

            return string.Empty;
        }

        public static IEnumerable<string> StringsMatchingHotkey(KeyCode hotkey)
        {
            switch (hotkey)
            {
                case KeyCode.A:
                    return new[] { "a" };
                case KeyCode.B:
                    return new[] { "b" };
                case KeyCode.C:
                    return new[] { "c" };
                case KeyCode.D:
                    return new[] { "d" };
                case KeyCode.E:
                    return new[] { "e" };
                case KeyCode.F:
                    return new[] { "f" };
                case KeyCode.G:
                    return new[] { "g" };
                case KeyCode.H:
                    return new[] { "h" };
                case KeyCode.I:
                    return new[] { "i" };
                case KeyCode.J:
                    return new[] { "j" };
                case KeyCode.K:
                    return new[] { "k" };
                case KeyCode.L:
                    return new[] { "l" };
                case KeyCode.M:
                    return new[] { "m" };
                case KeyCode.N:
                    return new[] { "n" };
                case KeyCode.O:
                    return new[] { "o" };
                case KeyCode.P:
                    return new[] { "p" };
                case KeyCode.Q:
                    return new[] { "q" };
                case KeyCode.R:
                    return new[] { "r" };
                case KeyCode.S:
                    return new[] { "s" };
                case KeyCode.T:
                    return new[] { "t" };
                case KeyCode.U:
                    return new[] { "u" };
                case KeyCode.V:
                    return new[] { "v" };
                case KeyCode.W:
                    return new[] { "w" };
                case KeyCode.X:
                    return new[] { "x" };
                case KeyCode.Y:
                    return new[] { "y" };
                case KeyCode.Z:
                    return new[] { "z" };
                case KeyCode.Escape:
                    return new[] { "escape", "esc" };
            }

            return Enumerable.Empty<string>();
        }
    }
}
