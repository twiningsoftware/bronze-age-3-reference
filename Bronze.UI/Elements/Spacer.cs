﻿using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;

namespace Bronze.UI.Elements
{
    public class Spacer : AbstractUiElement
    {
        private int _width;
        private int _height;
        private bool _needsRelayout;

        public override bool NeedsRelayout => _needsRelayout;

        public Spacer(int width, int height)
        {
            _width = width;
            _height = height;
            _needsRelayout = true;
        }

        public override void DoLayout(
            IDrawingService drawingService, 
            Rectangle parentBounds)
        {
            _needsRelayout = false;
            Bounds = new Rectangle(parentBounds.X, parentBounds.Y, _width, _height);
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
        }
    }
}
