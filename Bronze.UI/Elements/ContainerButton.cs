﻿using System;
using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;

namespace Bronze.UI.Elements
{
    public class ContainerButton : UiElementWithTooltip
    {
        private static readonly int PADDING = 5;

        private IUiElement _content;
        private Action _clickAction;
        private Func<bool> _disableCheck;
        private Func<bool> _highlightCheck;
        private bool _isHovered;
        private bool _needsRelayout;

        public bool Highlighted { get; set; }
        public override bool NeedsRelayout => base.NeedsRelayout || _needsRelayout || _content.NeedsRelayout;

        public ContainerButton(
            IUiElement content,
            Action clickAction,
            Func<bool> disableCheck = null,
            Func<bool> highlightCheck = null,
            string tooltip = null)
        {
            _content = content;
            _clickAction = clickAction;
            _disableCheck = disableCheck ?? (() => { return false; });
            _highlightCheck = highlightCheck ?? (() => { return false; });
            _needsRelayout = false;
            
            if (tooltip != null)
            {
                Tooltip = new SlicedBackground(new Text(tooltip)
                {
                    Width = Width.Fixed(200)
                });
            }
        }
        
        public override void DoLayout(
            IDrawingService drawingService,
            Rectangle parentBounds)
        {
            _needsRelayout = false;

            Bounds = new Rectangle(
                parentBounds.X,
                parentBounds.Y,
                _content.Bounds.Width + 2 * PADDING,
                _content.Bounds.Height + 2 * PADDING);

            base.DoLayout(drawingService, parentBounds);

            _content.DoLayout(
                drawingService,
                new Rectangle(
                    Bounds.X + PADDING,
                    Bounds.Y + PADDING,
                    Bounds.Width,
                    Bounds.Height));
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            base.Draw(drawingService, layer);
            
            if (_disableCheck() && !_highlightCheck())
            {
                drawingService.DrawSlicedImage(UiImages.ButtonInactive, Bounds, layer, false);
            }
            else if (_isHovered || Highlighted || _highlightCheck())
            {
                drawingService.DrawSlicedImage(UiImages.ButtonHover, Bounds, layer, false);
            }
            else
            {
                drawingService.DrawSlicedImage(UiImages.ButtonStandard, Bounds, layer, false);
            }

            _content.Draw(drawingService, layer + 1);
        }

        public override void Update(
            InputState inputState,
            IAudioService audioService,
            float elapsedSeconds)
        {
            base.Update(inputState, audioService, elapsedSeconds);
            _content.Update(inputState, audioService, elapsedSeconds);

            if (!_disableCheck())
            {
                _isHovered = Bounds.Contains(inputState.MousePosition);

                if (inputState.MouseReleased && _isHovered)
                {
                    _clickAction();
                    audioService.PlaySound(UiSounds.Click);
                }
            }
            else
            {
                _isHovered = false;
            }
        }
    }
}