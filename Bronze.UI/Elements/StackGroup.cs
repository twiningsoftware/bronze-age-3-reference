﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;

namespace Bronze.UI.Elements
{
    public class StackGroup : UiElementWithTooltip, IStackUiElement
    {
        private bool _needsRelayout;
        private int _oldChildCount;
        private bool _hideState;

        public HorizontalAlignment HorizontalContentAlignment { get; set; }
        public VerticalAlignment VerticalContentAlignment { get; set; }
        public Orientation Orientation { get; set; }
        public Width Width { get; set; }
        public Height Height { get; set; }
        public List<IUiElement> Children { get; set; }
        public Func<bool> HideCheck { get; set; }
        public int Margin { get; set; }

        public override bool NeedsRelayout => _needsRelayout || _oldChildCount != Children.Count || Children.Any(c => c.NeedsRelayout);

        public StackGroup()
        {
            HorizontalContentAlignment = HorizontalAlignment.Left;
            VerticalContentAlignment = VerticalAlignment.Top;
            Orientation = Orientation.Vertical;
            Children = new List<IUiElement>();
            Width = new AutoStackgroupWidth(this);
            Height = new AutoStackgroupHeight(this);
            HideCheck = () => false;
            _needsRelayout = true;
        }
        
        public override void Update(
            InputState inputState,
            IAudioService audioService,
            float elapsedSeconds)
        {
            base.Update(inputState, audioService, elapsedSeconds);

            foreach (var child in Children.ToArray())
            {
                child.Update(inputState, audioService, elapsedSeconds);
            }

            var newHideState = HideCheck();

            if(newHideState != _hideState)
            {
                _hideState = newHideState;
                _needsRelayout = true;
            }
        }
        public override void DoLayout(
            IDrawingService drawingService,
            Rectangle parentBounds)
        {
            base.DoLayout(drawingService, parentBounds);

            _oldChildCount = Children.Count;
            _needsRelayout = false;
            InternalLayout(drawingService, parentBounds);
            InternalLayout(drawingService, parentBounds);
        }

        private void InternalLayout(IDrawingService drawingService, Rectangle parentBounds)
        {
            if(_hideState)
            {
                Bounds = new Rectangle(
                    parentBounds.X,
                    parentBounds.Y,
                    0,
                    0);

                return;
            }

            Bounds = new Rectangle(
                parentBounds.X,
                parentBounds.Y,
                Width.CalculateWidth(parentBounds),
                Height.CalculateHeight(parentBounds));

            if (Children.Any())
            {
                int contentHeight;
                int contentWidth;

                var baseContentX = Bounds.X + Margin;
                var baseContentY = Bounds.Y + Margin;

                if (Orientation == Orientation.Horizontal)
                {
                    contentWidth = Children.Sum(c => c.Bounds.Width);
                    contentHeight = Children.Max(c => c.Bounds.Height);

                    if (HorizontalContentAlignment == HorizontalAlignment.Middle)
                    {
                        baseContentX += (Bounds.Width - contentWidth) / 2;
                    }
                    if (HorizontalContentAlignment == HorizontalAlignment.Right)
                    {
                        baseContentX += Bounds.Width - contentWidth;
                    }
                }
                else
                {
                    contentWidth = Children.Max(c => c.Bounds.Width);
                    contentHeight = Children.Sum(c => c.Bounds.Height);

                    if (VerticalContentAlignment == VerticalAlignment.Middle)
                    {
                        baseContentY += (Bounds.Height - contentHeight) / 2;
                    }
                    if (VerticalContentAlignment == VerticalAlignment.Bottom)
                    {
                        baseContentY += Bounds.Height - contentHeight;
                    }
                }

                foreach (var child in Children.ToArray())
                {
                    var childX = baseContentX;
                    var childY = baseContentY;

                    if (Orientation == Orientation.Horizontal)
                    {
                        if (VerticalContentAlignment == VerticalAlignment.Middle)
                        {
                            childY += (Bounds.Height - child.Bounds.Height) / 2;
                        }
                        if (VerticalContentAlignment == VerticalAlignment.Bottom)
                        {
                            childY += Bounds.Height - child.Bounds.Height;
                        }
                    }
                    else
                    {
                        if (HorizontalContentAlignment == HorizontalAlignment.Middle)
                        {
                            childX += (Bounds.Width - child.Bounds.Width) / 2;
                        }
                        if (HorizontalContentAlignment == HorizontalAlignment.Right)
                        {
                            childX += Bounds.Width - child.Bounds.Width;
                        }
                    }

                    child.DoLayout(
                        drawingService,
                        new Rectangle(
                        childX,
                        childY,
                        Bounds.Width,
                        Bounds.Height));

                    if (Orientation == Orientation.Vertical)
                    {
                        baseContentY += child.Bounds.Height;
                    }
                    else
                    {
                        baseContentX += child.Bounds.Width;
                    }
                }
            }

            Bounds = new Rectangle(
                parentBounds.X,
                parentBounds.Y,
                Width.CalculateWidth(parentBounds),
                Height.CalculateHeight(parentBounds));
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            if(HideCheck())
            {
                return;
            }

            base.Draw(drawingService, layer);

            foreach (var child in Children.ToArray())
            {
                child.Draw(drawingService, layer + LAYER_STEP);
            }
        }

        private class AutoStackgroupWidth : Width
        {
            private StackGroup _stackGroup;

            public AutoStackgroupWidth(StackGroup stackGroup)
            {
                _stackGroup = stackGroup;
            }

            public override int CalculateWidth(Rectangle parentBounds)
            {
                if(_stackGroup.Children.Any())
                {
                    if(_stackGroup.Orientation == Orientation.Horizontal)
                    {
                        return _stackGroup.Children.Sum(c => c.Bounds.Width) + _stackGroup.Margin * 2;
                    }
                    else
                    {
                        return _stackGroup.Children.Max(c => c.Bounds.Width) + _stackGroup.Margin * 2;
                    }
                }

                return 0;
            }
        }

        private class AutoStackgroupHeight : Height
        {
            private StackGroup _stackGroup;

            public AutoStackgroupHeight(StackGroup stackGroup)
            {
                _stackGroup = stackGroup;
            }

            public override int CalculateHeight(Rectangle parentBounds)
            {
                if (_stackGroup.Children.Any())
                {
                    if (_stackGroup.Orientation == Orientation.Horizontal)
                    {
                        return _stackGroup.Children.Max(c => c.Bounds.Height) + _stackGroup.Margin * 2;
                    }
                    else
                    {
                        return _stackGroup.Children.Sum(c => c.Bounds.Height) + _stackGroup.Margin * 2;
                    }
                }

                return 0;
            }
        }
    }
}
