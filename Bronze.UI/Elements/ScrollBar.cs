﻿using System;
using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;
using Bronze.UI.Data;

namespace Bronze.UI.Elements
{
    public class ScrollBar : AbstractUiElement
    {
        private float _minValue;
        private float _maxValue;
        private float ValueRange => _maxValue - _minValue;
        private Func<float> _getValue;
        private Action<float> _setValue;
        private bool _needsRelayout;

        public Orientation Orientation { get; set; }
        public Width Width { get; set; }
        public Height Height { get; set; }
        public bool Highlighted { get; set; }
        public override bool NeedsRelayout => _needsRelayout;

        public ScrollBar(
            Orientation orientation,
            float minValue, 
            float maxValue, 
            Func<float> getValue, 
            Action<float> setValue)
        {
            Orientation = orientation;
            _minValue = minValue;
            _maxValue = maxValue;
            _getValue = getValue;
            _setValue = setValue;
            _needsRelayout = false;

            if (orientation == Orientation.Horizontal)
            {
                Width = Width.Fixed(200);
                Height = Height.Fixed(10);
            }
            else
            {
                Width = Width.Fixed(10);
                Height = Height.Fixed(200);
            }
        }
        
        public override void DoLayout(
            IDrawingService drawingService, 
            Rectangle parentBounds)
        {
            _needsRelayout = false;

            Bounds = new Rectangle(
                parentBounds.X,
                parentBounds.Y,
                Width.CalculateWidth(parentBounds),
                Height.CalculateHeight(parentBounds));
        }
        
        public override void Draw(IDrawingService drawingService, float layer)
        {
            drawingService.DrawSlicedImage(UiImages.ScrollBackground, Bounds, layer, false);

            var fillWidth = Bounds.Width;
            var fillHeight = Bounds.Height;
            
            var valPercent = Math.Max(0,
                Math.Min(1,
                    (_getValue() - _minValue) / ValueRange));

            if(Orientation == Orientation.Horizontal)
            {
                fillWidth = (int)(fillWidth * valPercent);
            }
            else
            {
                fillHeight = (int)(fillHeight * valPercent);
            }

            drawingService.DrawSlicedImage(
                UiImages.ScrollFill, 
                new Rectangle(
                    Bounds.X,
                    Bounds.Y,
                    fillWidth,
                    fillHeight),
                layer + LAYER_STEP,
                false);
        }

        public override void Update(
            InputState inputState,
            IAudioService audioService,
            float elapsedSeconds)
        {
            base.Update(inputState, audioService, elapsedSeconds);

            if(inputState.MouseDown && Bounds.Contains(inputState.MousePosition))
            {
                if(Orientation == Orientation.Horizontal)
                {
                    var xPercentIn = (inputState.MousePosition.X - Bounds.X) / (float)Bounds.Width;
                    var newValue = xPercentIn * ValueRange + _minValue;
                    _setValue(newValue);
                }
                else
                {
                    var yPercentIn = (inputState.MousePosition.Y - Bounds.Y) / (float)Bounds.Height;
                    var newValue = yPercentIn * ValueRange + _minValue;
                    _setValue(newValue);
                }
            }
        }
    }
}