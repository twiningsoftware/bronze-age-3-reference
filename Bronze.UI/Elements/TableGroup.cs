﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;

namespace Bronze.UI.Elements
{
    public class TableGroup<TRowItem> : AbstractUiElement
    {
        private const int PADDING = 10;

        public HorizontalAlignment HorizontalContentAlignment { get; set; }
        public VerticalAlignment VerticalContentAlignment { get; set; }
        public Orientation Orientation { get; set; }
        public Width Width { get; set; }
        public Height Height { get; set; }
        public override bool NeedsRelayout => _needsRelayout || _rows.Any(r => r.Any(x => x.NeedsRelayout));

        private IUiElement[] _paddedHeader;
        private List<IUiElement[]> _rows;
        private TRowItem[] _rowItems;
        private Func<TRowItem, IEnumerable<IUiElement>> _rowBuilder;
        private bool _contentsBuilt;
        private IUiElement[] _headers;
        private bool _needsRelayout;
        private int _firstItem;
        private int _itemsToShow;
        private float _accumulatedScroll;
        private ScrollBar _scrollbar;

        public Func<bool> HideCheck { get; set; }

        public TableGroup(string[] headers, IEnumerable<TRowItem> rowItems, Func<TRowItem, IEnumerable<IUiElement>> rowBuilder, int rowCount)
        {
            _rowItems = rowItems.ToArray();
            _rowBuilder = rowBuilder;
            _headers = headers.Select(h=>new Label(h)).ToArray();
            HorizontalContentAlignment = HorizontalAlignment.Left;
            VerticalContentAlignment = VerticalAlignment.Top;
            Orientation = Orientation.Vertical;
            HideCheck = () => false;
            _contentsBuilt = false;
            _needsRelayout = true;
            _firstItem = 0;
            _itemsToShow = rowCount;
            _accumulatedScroll = 0;

            if (_rowItems.Length > _itemsToShow)
            {
                _scrollbar = new ScrollBar(
                    Orientation.Vertical,
                    0,
                    _rowItems.Length - _itemsToShow,
                    () => _firstItem,
                    i =>
                    {
                        _needsRelayout = true;
                        _firstItem = (int)(i + 0.5);
                    })
                {
                    Height = Height.Relative(1f),
                    Width = Width.Fixed(10)
                };
            }
        }

        public TableGroup(IUiElement[] headers, IEnumerable<TRowItem> rowItems, Func<TRowItem, IEnumerable<IUiElement>> rowBuilder, int rowCount)
        {
            _rowItems = rowItems.ToArray();
            _rowBuilder = rowBuilder;
            _headers = headers;
            HorizontalContentAlignment = HorizontalAlignment.Left;
            VerticalContentAlignment = VerticalAlignment.Top;
            Orientation = Orientation.Vertical;
            HideCheck = () => false;
            _contentsBuilt = false;
            _needsRelayout = true;
            _firstItem = 0;
            _itemsToShow = rowCount;
            _accumulatedScroll = 0;

            if (_rowItems.Length > _itemsToShow)
            {
                _scrollbar = new ScrollBar(
                    Orientation.Vertical,
                    0,
                    _rowItems.Length - _itemsToShow,
                    () => _firstItem,
                    i =>
                    {
                        _needsRelayout = true;
                        _firstItem = (int)(i + 0.5);
                    })
                {
                    Height = Height.Relative(1f),
                    Width = Width.Fixed(10)
                };
            }
        }

        public override void Update(
            InputState inputState,
            IAudioService audioService,
            float elapsedSeconds)
        {
            base.Update(inputState, audioService, elapsedSeconds);

            if(!_contentsBuilt)
            {
                BuildContents();
            }

            foreach (var child in _rows.SelectMany(l => l).Concat(_paddedHeader))
            {
                child.Update(inputState, audioService, elapsedSeconds);
            }

            if (Bounds.Contains(inputState.MousePosition))
            {
                _accumulatedScroll += inputState.MouseScrollDelta;

                if (_accumulatedScroll < -1)
                {
                    _accumulatedScroll = 0;
                    _firstItem = Math.Min(_firstItem + 1, _rowItems.Length - _itemsToShow);
                    _needsRelayout = true;
                }
                if (_accumulatedScroll > 1)
                {
                    _accumulatedScroll = 0;
                    _firstItem = Math.Max(0, _firstItem - 1);
                    _needsRelayout = true;
                }
            }
        }

        private void BuildContents()
        {
            _contentsBuilt = true;

            _paddedHeader = AddPadding(_headers);
            _rows = new List<IUiElement[]>();
            foreach(var item in _rowItems)
            {
                _rows.Add(AddPadding(_rowBuilder(item).ToArray()));
            }
        }

        private IUiElement[] AddPadding(IEnumerable<IUiElement> elements)
        {
            var output = new List<IUiElement>();
            output.Add(new Spacer(PADDING, 0));
            foreach(var item in elements)
            {
                output.Add(item);
                output.Add(new Spacer(PADDING, 0));
            }

            return output.ToArray();
        }

        public override void DoLayout(
            IDrawingService drawingService,
            Rectangle parentBounds)
        {
            if (_contentsBuilt)
            {
                _needsRelayout = false;
                InternalDoLayout(drawingService, parentBounds);
            }
        }

        private void InternalDoLayout(
            IDrawingService drawingService,
            Rectangle parentBounds)
        {
            if (HideCheck())
            {
                Bounds = new Rectangle(
                    parentBounds.X,
                    parentBounds.Y,
                    0,
                    0);

                return;
            }
            
            if (_rows.Any())
            {
                var allCellsTogether = _rows.SelectMany(c => c).Concat(_paddedHeader).ToArray();
                
                foreach (var cell in allCellsTogether)
                {
                    cell.DoLayout(drawingService, parentBounds);
                }

                var columnCount = _rows.Select(c => c.Length).Max();

                var cellWidth = allCellsTogether.Max(c => c.Bounds.Width);
                var cellHeight = allCellsTogether.Max(c => c.Bounds.Height);

                var columnWidths = new int[columnCount];
                for (var c = 0; c < _paddedHeader.Length; c++)
                {
                    columnWidths[c] = Math.Max(columnWidths[c], _paddedHeader[c].Bounds.Width);
                }
                foreach (var row in _rows)
                {
                    for (var c = 0; c < row.Length; c++)
                    {
                        columnWidths[c] = Math.Max(columnWidths[c], row[c].Bounds.Width);
                    }
                }

                _scrollbar?.DoLayout(
                    drawingService,
                    new Rectangle(
                        Bounds.X + columnWidths.Sum(),
                        Bounds.Y,
                        Bounds.Width,
                        Bounds.Height));

                var rowsForLayout = _paddedHeader.Yield()
                    .Concat(_rows.Skip(_firstItem).Take(_itemsToShow))
                    .ToArray();
                
                var yStart = 0;
                foreach(var row in rowsForLayout)
                {
                    var rowHeight = row.Max(i => i.Bounds.Height);

                    var xStart = 0;
                    for(var c = 0; c < row.Length; c++)
                    {
                        var cell = row[c];

                        var yOffset = (rowHeight - cell.Bounds.Height) / 2;

                        cell.DoLayout(drawingService, 
                            new Rectangle(
                                parentBounds.X + xStart, 
                                parentBounds.Y + yStart + yOffset,
                                cellWidth, 
                                cellHeight));

                        xStart += columnWidths[c];
                    }

                    yStart += rowHeight;
                }

                var totalWidth = columnWidths.Sum();
                if(_scrollbar != null)
                {
                    totalWidth += _scrollbar.Bounds.Width;
                }

                Bounds = new Rectangle(parentBounds.X, parentBounds.Y, totalWidth, yStart);
            }
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            if(HideCheck())
            {
                return;
            }

            _scrollbar?.Draw(drawingService, layer);

            if (_contentsBuilt)
            {
                foreach (var cell in _paddedHeader)
                {
                    cell.Draw(drawingService, layer + LAYER_STEP);
                }

                foreach (var cell in _rows.Skip(_firstItem).Take(_itemsToShow).SelectMany(r => r))
                {
                    cell.Draw(drawingService, layer + LAYER_STEP);
                }
            }
        }
    }
}
