﻿using System;
using System.Linq;
using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;
using Bronze.UI.Data;

namespace Bronze.UI.Elements
{
    public class TextButton : UiElementWithTooltip
    {
        private static readonly int PADDING = 5;
        
        private Action _clickAction;
        private Func<bool> _disableCheck;
        private Func<bool> _highlightCheck;
        private KeyCode? _hotkey;
        private int _hotkeyHighlightStart;
        private int _hotkeyHighlightLength;
        private bool _isHovered;
        private string _content;
        private bool _needsRelayout;


        public bool Highlighted { get; set; }
        public override bool NeedsRelayout => base.NeedsRelayout || _needsRelayout;
        public BronzeColor TextColor { get; set; }
        public Action HoverAction { get; set; }

        public TextButton(
            string content,
            Action clickAction,
            KeyCode? hotkey = null,
            Func<bool> disableCheck = null,
            Func<bool> highlightCheck = null,
            string tooltip = null)
        {
            _content = content;
            _clickAction = clickAction;
            _hotkey = hotkey;
            _disableCheck = disableCheck ?? (() => { return false; });
            _highlightCheck = highlightCheck ?? (() => { return false; });
            _needsRelayout = true;
            TextColor = BronzeColor.White;
            
            if (tooltip != null)
            {
                Tooltip = new SlicedBackground(new Text(tooltip)
                {
                    Width = Width.Fixed(200)
                });
            }

            RecalculateHotkeyHighlight();
        }

        private void RecalculateHotkeyHighlight()
        {
            _hotkeyHighlightStart = -1;
            _hotkeyHighlightLength = 0;

            if (_hotkey == null)
            {
                return;
            }
            
            var matchStrings = UiUtil.StringsMatchingHotkey(_hotkey.Value);

            if (matchStrings.Any())
            {
                string highlightString = null;
                var highlightStart = int.MaxValue;

                foreach (var str in matchStrings)
                {
                    var index = _content.IndexOf(str, StringComparison.OrdinalIgnoreCase);

                    if (index >= 0 && index < highlightStart)
                    {
                        highlightString = str;
                        highlightStart = index;
                    }
                }

                if(highlightString != null)
                {
                    _hotkeyHighlightStart = highlightStart;
                    _hotkeyHighlightLength = highlightString.Length;
                }
            }
        }

        public override void DoLayout(
            IDrawingService drawingService, 
            Rectangle parentBounds)
        {
            _needsRelayout = false;

            var textSize = drawingService.MeasureText(_content);

            Bounds = new Rectangle(
                parentBounds.X,
                parentBounds.Y,
                (int)(textSize.X + 2 * PADDING),
                (int)(textSize.Y + 2 * PADDING));

            base.DoLayout(drawingService, parentBounds);
        }
        
        public override void Draw(IDrawingService drawingService, float layer)
        {
            base.Draw(drawingService, layer);

            if (_disableCheck())
            {
                drawingService.DrawSlicedImage(UiImages.ButtonInactive, Bounds, layer, false);
            }
            else if (_isHovered || Highlighted || _highlightCheck())
            {
                drawingService.DrawSlicedImage(UiImages.ButtonHover, Bounds, layer, false);
            }
            else
            {
                drawingService.DrawSlicedImage(UiImages.ButtonStandard, Bounds, layer, false);
            }
            
            drawingService
                .DrawText(
                    _content,
                    new Point(
                        Bounds.X + PADDING,
                        Bounds.Y + PADDING),
                    layer + LAYER_STEP,
                    TextColor);

            if(_hotkeyHighlightStart >= 0)
            {
                var xOffset = 0;

                if(_hotkeyHighlightStart > 0)
                {
                    xOffset = (int)drawingService.MeasureText(_content.Substring(0, _hotkeyHighlightStart)).X;
                }

                drawingService
                .DrawText(
                    _content.Substring(_hotkeyHighlightStart, _hotkeyHighlightLength),
                    new Point(
                        Bounds.X + PADDING + xOffset,
                        Bounds.Y + PADDING),
                    layer + 2 * LAYER_STEP,
                    BronzeColor.Yellow);
            }
        }

        public override void Update(
            InputState inputState,
            IAudioService audioService,
            float elapsedSeconds)
        {
            base.Update(inputState, audioService, elapsedSeconds);

            if (!_disableCheck())
            {
                var wasHovered = _isHovered;
                _isHovered = Bounds.Contains(inputState.MousePosition);

                if(_isHovered && !wasHovered)
                {
                    HoverAction?.Invoke();
                }

                if ((_hotkey != null && inputState.JustPressed(_hotkey.Value) && inputState.FocusedElement == null)
                    || (inputState.MouseReleased && _isHovered))
                {
                    _clickAction();
                    audioService.PlaySound(UiSounds.Click);
                }
            }
            else
            {
                _isHovered = false;
            }
        }
    }
}