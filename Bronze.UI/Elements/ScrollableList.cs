﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;

namespace Bronze.UI.Elements
{
    public class ScrollableList : AbstractUiElement
    {
        public Width Width { get; set; }
        public override bool NeedsRelayout => _needsRelayout || _items.Any(i => i.NeedsRelayout) || (_scrollbar?.NeedsRelayout ?? false);

        private IUiElement[] _items;
        private int _firstItem;
        private int _itemsToShow;
        private float _accumulatedScroll;
        private ScrollBar _scrollbar;
        private IEnumerable<IUiElement> CurrentItems => _items.Skip(_firstItem).Take(_itemsToShow).ToArray();
        private bool _needsRelayout;
        
        public ScrollableList(int itemsToShow, IEnumerable<IUiElement> items)
        {
            _items = items.ToArray();
            _firstItem = 0;
            _itemsToShow = itemsToShow;
            _needsRelayout = true;

            if (_items.Length > _itemsToShow)
            {
                _scrollbar = new ScrollBar(
                    Orientation.Vertical,
                    0,
                    _items.Length - _itemsToShow,
                    () => _firstItem,
                    i =>
                    {
                        _needsRelayout = true;
                        _firstItem = (int)(i + 0.5);
                    })
                {
                    Height = Height.Relative(1f),
                    Width = Width.Fixed(10)
                };
            }

            Width = new AutoScrollableViewWidth(this);
        }

        public override void Update(
            InputState inputState,
            IAudioService audioService,
            float elapsedSeconds)
        {
            base.Update(inputState, audioService, elapsedSeconds);

            _scrollbar?.Update(inputState, audioService, elapsedSeconds);

            foreach (var item in CurrentItems)
            {
                item.Update(inputState, audioService, elapsedSeconds);
            }

            if(Bounds.Contains(inputState.MousePosition))
            {
                _accumulatedScroll += inputState.MouseScrollDelta;

                if(_accumulatedScroll < -1)
                {
                    _accumulatedScroll = 0;
                    _firstItem = Math.Min(_firstItem + 1, _items.Length - _itemsToShow);
                    _needsRelayout = true;
                }
                if(_accumulatedScroll > 1)
                {
                    _accumulatedScroll = 0;
                    _firstItem = Math.Max(0, _firstItem - 1);
                    _needsRelayout = true;
                }
            }
        }

        public override void DoLayout(
            IDrawingService drawingService,
            Rectangle parentBounds)
        {
            _needsRelayout = false;
            InternalLayout(drawingService, parentBounds);
            InternalLayout(drawingService, parentBounds);
        }

        private void InternalLayout(
            IDrawingService drawingService, 
            Rectangle parentBounds)
        {
            var contentWidth = Width.CalculateWidth(parentBounds);
            
            Bounds = new Rectangle(
                parentBounds.X,
                parentBounds.Y,
                contentWidth + (_scrollbar?.Bounds.Width ?? 0),
                CurrentItems.Sum(e => e.Bounds.Height));

            _scrollbar?.DoLayout(
                drawingService,
                new Rectangle(
                    Bounds.X + contentWidth,
                    Bounds.Y,
                    Bounds.Width,
                    Bounds.Height));

            var childY = parentBounds.Y;
            foreach (var item in CurrentItems)
            {
                item.DoLayout(drawingService,
                    new Rectangle(
                        parentBounds.X,
                        childY,
                        Bounds.Width,
                        Bounds.Height));
                childY += item.Bounds.Height;
            }
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            foreach (var item in CurrentItems)
            {
                item.Draw(drawingService, layer + LAYER_STEP);
            }

            _scrollbar?.Draw(drawingService, layer);
        }

        private class AutoScrollableViewWidth : Width
        {
            private ScrollableList _scrollableList;

            public AutoScrollableViewWidth(ScrollableList scrollableList)
            {
                _scrollableList = scrollableList;
            }

            public override int CalculateWidth(Rectangle parentBounds)
            {
                if (_scrollableList.CurrentItems.Any())
                {
                    return _scrollableList.CurrentItems.Max(i => i.Bounds.Width) + 10;
                }

                return 0;
            }
        }
    }
}
