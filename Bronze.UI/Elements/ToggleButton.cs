﻿using System;
using System.Linq;
using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;
using Bronze.UI.Data;

namespace Bronze.UI.Elements
{
    public class ToggleButton : UiElementWithTooltip
    {
        private static readonly int PADDING = 5;

        private Action<bool> _setValue;
        private Func<bool> _getValue;
        private Func<bool> _disableCheck;
        private Func<bool> _highlightCheck;
        private KeyCode? _hotkey;

        private int _hotkeyHighlightStartOn;
        private int _hotkeyHighlightLengthOn;
        private int _hotkeyHighlightStartOff;
        private int _hotkeyHighlightLengthOff;

        private bool _isHovered;
        private bool _needsRelayout;

        private string _contentOn;
        private string _contentOff;

        public override bool NeedsRelayout => _needsRelayout;

        public ToggleButton(
            string contentOn,
            string contentOff,
            Action<bool> setValue,
            Func<bool> getValue,
            KeyCode? hotkey = null,
            Func<bool> disableCheck = null,
            Func<bool> highlightCheck = null,
            string tooltip = null)
        {
            _contentOn = contentOn;
            _contentOff = contentOff;
            _setValue = setValue;
            _getValue = getValue;
            _hotkey = hotkey;
            _disableCheck = disableCheck ?? (() => { return false; });
            _highlightCheck = highlightCheck ?? (() => { return false; });
            _needsRelayout = true;

            RecalculateHotkeyHighlight();

            if (tooltip != null)
            {
                Tooltip = new SlicedBackground(new Text(tooltip)
                {
                    Width = Width.Fixed(200)
                });
            }
        }

        public string Content
        {
            get => _getValue() ? _contentOn : _contentOff;
        }

        public int HotkeyHighlightStart
        {
            get => _getValue() ? _hotkeyHighlightStartOn : _hotkeyHighlightStartOff;
        }

        public int HotkeyHighlightLength
        {
            get => _getValue() ? _hotkeyHighlightLengthOn : _hotkeyHighlightLengthOff;
        }

        private void RecalculateHotkeyHighlight()
        {
            _hotkeyHighlightStartOn = -1;
            _hotkeyHighlightLengthOn = 0;
            _hotkeyHighlightStartOff = -1;
            _hotkeyHighlightLengthOff = 0;

            if (_hotkey == null)
            {
                return;
            }
            
            var matchStrings = UiUtil.StringsMatchingHotkey(_hotkey.Value);

            if (matchStrings.Any())
            {
                string highlightString = null;
                var highlightStart = int.MaxValue;

                foreach (var str in matchStrings)
                {
                    var index = _contentOn.IndexOf(str, StringComparison.OrdinalIgnoreCase);

                    if (index >= 0 && index < highlightStart)
                    {
                        highlightString = str;
                        highlightStart = index;
                    }
                }

                if(highlightString != null)
                {
                    _hotkeyHighlightStartOn = highlightStart;
                    _hotkeyHighlightLengthOn = highlightString.Length;
                }
                
                highlightString = null;
                highlightStart = int.MaxValue;

                foreach (var str in matchStrings)
                {
                    var index = _contentOff.IndexOf(str, StringComparison.OrdinalIgnoreCase);

                    if (index >= 0 && index < highlightStart)
                    {
                        highlightString = str;
                        highlightStart = index;
                    }
                }

                if (highlightString != null)
                {
                    _hotkeyHighlightStartOff = highlightStart;
                    _hotkeyHighlightLengthOff = highlightString.Length;
                }
            }
        }

        public override void DoLayout(
            IDrawingService drawingService, 
            Rectangle parentBounds)
        {
            base.DoLayout(drawingService, parentBounds);

            var textSize = drawingService.MeasureText(Content);
            _needsRelayout = false;

            Bounds = new Rectangle(
                parentBounds.X,
                parentBounds.Y,
                (int)(textSize.X + 2 * PADDING),
                (int)(textSize.Y + 2 * PADDING));
        }
        
        public override void Draw(IDrawingService drawingService, float layer)
        {
            base.Draw(drawingService, layer);

            if(_disableCheck())
            {
                drawingService.DrawSlicedImage(UiImages.ButtonInactive, Bounds, layer, false);
            }
            else if (_isHovered || _highlightCheck())
            {
                drawingService.DrawSlicedImage(UiImages.ButtonHover, Bounds, layer, false);
            }
            else
            {
                drawingService.DrawSlicedImage(UiImages.ButtonStandard, Bounds, layer, false);
            }
            
            drawingService
                .DrawText(
                    Content,
                    new Point(
                        Bounds.X + PADDING,
                        Bounds.Y + PADDING),
                    layer + 1,
                    BronzeColor.White);

            if(HotkeyHighlightStart >= 0)
            {
                var xOffset = 0;

                if(HotkeyHighlightStart > 0)
                {
                    xOffset = (int)drawingService.MeasureText(Content.Substring(0, HotkeyHighlightStart)).X;
                }

                drawingService
                .DrawText(
                    Content.Substring(HotkeyHighlightStart, HotkeyHighlightLength),
                    new Point(
                        Bounds.X + PADDING + xOffset,
                        Bounds.Y + PADDING),
                    layer + 2,
                    BronzeColor.Yellow);
            }
        }

        public override void Update(
            InputState inputState,
            IAudioService audioService,
            float elapsedSeconds)
        {
            base.Update(inputState, audioService, elapsedSeconds);

            if (!_disableCheck())
            {
                _isHovered = Bounds.Contains(inputState.MousePosition);

                if ((_hotkey != null && inputState.JustPressed(_hotkey.Value) && inputState.FocusedElement == null)
                    || (inputState.MouseReleased && _isHovered))
                {
                    _setValue(!_getValue());
                    _needsRelayout = true;
                    audioService.PlaySound(UiSounds.Click);
                }
            }
            else
            {
                _isHovered = false;
            }
        }
    }
}