﻿using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;
using System;

namespace Bronze.UI.Elements
{
    public delegate bool KeyTypedEventHandler(KeyCode keyCode);

    public class TextInput : AbstractUiElement
    {
        private static readonly int PADDING = 3;

        private Func<string> _getValue;
        private Action<string> _setValue;
        private int _width;
        private bool _isFocused;
        private bool _isHovered;
        private bool _needsRelayout;

        public override bool NeedsRelayout => _needsRelayout;

        public Func<string> GetHintText { get; set; }
        
        private float _backHoldTimer;

        public event KeyTypedEventHandler OnKeyTyped;

        public TextInput(Func<string> getValue, Action<string> setValue, int width)
        {
            _getValue = getValue;
            _setValue = setValue;
            _width = width;
            _needsRelayout = true;
        }

        public override void Update(
            InputState inputState,
            IAudioService audioService,
            float elapsedSeconds)
        {
            base.Update(inputState, audioService, elapsedSeconds);

            _isHovered = Bounds.Contains(inputState.MousePosition);

            if (inputState.MouseReleased && _isHovered)
            {
                inputState.FocusedElement = this;
            }

            _isFocused = inputState.FocusedElement == this;

            if (_isFocused)
            {
                var currentVal = _getValue();

                if(inputState.KeyDown(KeyCode.Back))
                {
                    if(_backHoldTimer == 0 && currentVal.Length > 0)
                    {
                        _setValue(currentVal.Substring(0, currentVal.Length - 1));
                    }

                    _backHoldTimer += elapsedSeconds;

                    if(_backHoldTimer > 0.1f)
                    {
                        _backHoldTimer = 0f;

                        if(currentVal.Length > 0)
                        {
                            _setValue(currentVal.Substring(0, currentVal.Length - 1));
                        }
                    }
                }
                else
                {
                    _backHoldTimer = 0f;

                    var shouldUpdate = false;

                    foreach (var keyCode in UiUtil.TextInputKeys)
                    {
                        if (inputState.JustPressed(keyCode))
                        {
                            var supress = OnKeyTyped?.Invoke(keyCode) ?? false;

                            if (!supress)
                            {
                                shouldUpdate = true;
                                currentVal += UiUtil.InputKeyToText(
                                    keyCode,
                                    inputState.KeyDown(KeyCode.LeftShift) || inputState.KeyDown(KeyCode.RightShift));
                            }
                        }
                    }

                    if (shouldUpdate)
                    {
                        _setValue(currentVal);
                    }
                }
            }
        }

        public override void DoLayout(
            IDrawingService drawingService,
            Rectangle parentBounds)
        {
            _needsRelayout = false;
            var value = _getValue();
            var textSize = drawingService.MeasureText(value);

            if(textSize.Y < 1)
            {
                textSize = drawingService.MeasureText(" ");
            }

            while(textSize.X > _width && value.Length > 0)
            {
                value = value.Substring(0, value.Length - 1);
                textSize = drawingService.MeasureText(value);
            }

            _setValue(value);

            Bounds = new Rectangle(
                parentBounds.X,
                parentBounds.Y,
                (int)(_width + 2 * PADDING),
                (int)(textSize.Y + 2 * PADDING));
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            if (_isFocused)
            {
                drawingService.DrawSlicedImage(UiImages.TextInputFocused, Bounds, layer, false);
            }
            else if (_isHovered)
            {
                drawingService.DrawSlicedImage(UiImages.TextInputHover, Bounds, layer, false);
            }
            else
            {
                drawingService.DrawSlicedImage(UiImages.TextInputStandard, Bounds, layer, false);
            }

            var text = _getValue();
            var hintText = GetHintText?.Invoke() ?? string.Empty;

            var textSize = drawingService.MeasureText(text);
            
            drawingService
                .DrawText(
                    text,
                    new Point(
                        Bounds.X + PADDING,
                        Bounds.Y + PADDING),
                    layer + LAYER_STEP);

            drawingService
                .DrawText(
                    hintText,
                    new Point(
                        Bounds.X + PADDING + (int)textSize.X,
                        Bounds.Y + PADDING),
                    layer + LAYER_STEP,
                    BronzeColor.Grey);
        }
    }
}
