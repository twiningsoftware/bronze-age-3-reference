﻿using System;
using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;

namespace Bronze.UI.Elements
{
    public class SizePadder : AbstractUiElement
    {
        private IUiElement _child;
        private int _minWidth;
        private int _minHeight;
        private bool _needsRelayout;

        public override bool NeedsRelayout => _needsRelayout || _child.NeedsRelayout;

        public HorizontalAlignment HorizontalAlignment { get; set; }
        public VerticalAlignment VerticalAlignment { get; set; }

        public SizePadder(IUiElement child, int? minWidth = null, int? minHeight = null)
        {
            _child = child;
            _minWidth = minWidth ?? 0;
            _minHeight = minHeight ?? 0;
            _needsRelayout = true;
            HorizontalAlignment = HorizontalAlignment.Middle;
            VerticalAlignment = VerticalAlignment.Middle;
        }

        public override void Update(
            InputState inputState,
            IAudioService audioService,
            float elapsedSeconds)
        {
            base.Update(inputState,audioService, elapsedSeconds);

            _child.Update(inputState, audioService, elapsedSeconds);
        }

        public override void DoLayout(IDrawingService drawingService, Rectangle parentBounds)
        {
            _needsRelayout = false;
            var xOffset = 0;
            var yOffset = 0;

            if(HorizontalAlignment == HorizontalAlignment.Left)
            {
                xOffset = 0;
            }
            else if (HorizontalAlignment == HorizontalAlignment.Middle)
            {
                xOffset = Math.Max(0, _minWidth - _child.Bounds.Width) / 2;
            }
            else
            {
                xOffset = Math.Max(0, _minWidth - _child.Bounds.Width);
            }

            if (VerticalAlignment == VerticalAlignment.Top)
            {
                yOffset = 0;
            }
            else if (VerticalAlignment == VerticalAlignment.Middle)
            {
                yOffset = Math.Max(0, _minHeight - _child.Bounds.Height) / 2;
            }
            else
            {
                yOffset = Math.Max(0, _minHeight - _child.Bounds.Height);
            }
            
            Bounds = new Rectangle(
                parentBounds.X,
                parentBounds.Y,
                Math.Max(_minWidth, _child.Bounds.Width),
                Math.Max(_minHeight, _child.Bounds.Height));

            _child.DoLayout(
                drawingService,
                new Rectangle(
                    parentBounds.X + xOffset,
                    parentBounds.Y + yOffset,
                    parentBounds.Width,
                    parentBounds.Height));
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            _child.Draw(drawingService, layer);
        }
    }
}
