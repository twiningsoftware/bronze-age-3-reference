﻿using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;

namespace Bronze.UI.Elements
{
    public class ColorizedImage : AbstractUiElement
    {
        private string _imageKey;
        private bool _needsRelayout;
        public BronzeColor Colorization { get; set; }
        public override bool NeedsRelayout => _needsRelayout;

        public ColorizedImage(string imageKey, BronzeColor colorization)
        {
            _imageKey = imageKey;
            Colorization = colorization;
            _needsRelayout = true;
        }
        
        public override void DoLayout(
            IDrawingService drawingService,
            Rectangle parentBounds)
        {
            _needsRelayout = false;
            var imageBounds = drawingService.GetImageSize(_imageKey);

            Bounds = new Rectangle(
                parentBounds.X,
                parentBounds.Y,
                (int)imageBounds.X,
                (int)imageBounds.Y);
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            drawingService.DrawScaledImage(
                _imageKey, 
                Colorization,
                Bounds.Center.ToVector2(),
                1,
                1,
                layer,
                false);
        }
    }
}
