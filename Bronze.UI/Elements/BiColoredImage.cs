﻿using System;
using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;
using Bronze.UI.Data;

namespace Bronze.UI.Elements
{
    public class BiColoredImage : UiElementWithTooltip
    {
        private string _primaryImageKey;
        private string _secondaryImageKey;

        public Func<string> PrimaryImageUpdater { get; set; }
        public Func<BronzeColor> PrimaryColorizationUpdater { get; set; }
        public Func<string> SecondaryImageUpdater { get; set; }
        public Func<BronzeColor> SecondaryColorizationUpdater { get; set; }

        private bool _needsRelayout;
        private bool _oldVisiblity;
        public override bool NeedsRelayout => base.NeedsRelayout || _needsRelayout;

        public Func<bool> VisibilityCheck { get; set; }

        protected override bool SuppressTooltip => !(VisibilityCheck == null || VisibilityCheck());

        public BiColoredImage(string primaryImageKey, string secondaryImageKey, string tooltip = null)
        {
            _primaryImageKey = primaryImageKey;
            _secondaryImageKey = secondaryImageKey;
            _needsRelayout = true;

            if (tooltip != null)
            {
                Tooltip = new SlicedBackground(new Text(tooltip)
                {
                    Width = Width.Fixed(200)
                });
            }
        }
        
        public override void DoLayout(
            IDrawingService drawingService,
            Rectangle parentBounds)
        {
            base.DoLayout(drawingService, parentBounds);

            _needsRelayout = false;

            if(PrimaryImageUpdater != null)
            {
                _primaryImageKey = PrimaryImageUpdater();
            }
            if(SecondaryImageUpdater != null)
            {
                _secondaryImageKey = SecondaryImageUpdater();
            }


            var primaryImageBounds = drawingService.GetImageSize(_primaryImageKey);
            var secondaryImageBounds = drawingService.GetImageSize(_secondaryImageKey);

            Bounds = new Rectangle(
                parentBounds.X,
                parentBounds.Y,
                (int)Math.Max(primaryImageBounds.X, secondaryImageBounds.X),
                (int)Math.Max(primaryImageBounds.Y, secondaryImageBounds.Y));
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            base.Draw(drawingService, layer);

            var visibility = VisibilityCheck == null || VisibilityCheck();

            if(visibility != _oldVisiblity)
            {
                _needsRelayout = true;
            }

            var primaryColor = PrimaryColorizationUpdater?.Invoke() ?? BronzeColor.None;
            var secondaryColor = SecondaryColorizationUpdater?.Invoke() ?? BronzeColor.None;

            _oldVisiblity = visibility;
            if (visibility)
            {
                drawingService.DrawImage(
                    _primaryImageKey,
                    primaryColor,
                    Bounds.Center.ToVector2(),
                    layer,
                    false);

                drawingService.DrawImage(
                    _secondaryImageKey,
                    secondaryColor,
                    Bounds.Center.ToVector2(),
                    layer + 0.1f,
                    false);
            }
        }
    }
}
