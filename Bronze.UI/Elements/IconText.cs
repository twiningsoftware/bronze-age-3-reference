﻿using System;
using System.Numerics;
using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;
using Bronze.UI.Data;

namespace Bronze.UI.Elements
{
    public class IconText : UiElementWithTooltip
    {
        public string Content { get; set; }
        private string _imageKey;
        private bool _needsRelayout;
        private int _padding;
        private bool _willShow;

        public BronzeColor Color { get; set; }
        public override bool NeedsRelayout => base.NeedsRelayout || _needsRelayout;
        public Func<string> ContentUpdater { get; set; }
        public Func<string> IconUpdater { get; set; }
        public Func<BronzeColor> ColorUpdater { get; set; }
        public BronzeColor IconColorization { get; set; }
        public Func<bool> DisplayCondition { get; set; }

        public IconText(
            string imageKey,
            string content,
            BronzeColor color = BronzeColor.White,
            string tooltip = null)
        {
            _imageKey = imageKey;
            Content = content;
            Color = color;
            _needsRelayout = false;
            DisplayCondition = () => true;

            _padding = 5;
            if(string.IsNullOrWhiteSpace(_imageKey))
            {
                _padding = 0;
            }

            if(tooltip != null)
            {
                Tooltip = new SlicedBackground(new Text(tooltip)
                {
                    Width = Width.Fixed(200)
                });
            }
        }

        public override void Update(InputState inputState, IAudioService audioService, float elapsedSeconds)
        {
            base.Update(inputState, audioService, elapsedSeconds);

            var wasShowing = DisplayCondition?.Invoke() ?? false;
            if(wasShowing != _willShow)
            {
                _willShow = wasShowing;
                _needsRelayout = true;
            }
        }

        public override void DoLayout(
            IDrawingService drawingService,
            Rectangle parentBounds)
        {
            base.DoLayout(drawingService, parentBounds);

            _needsRelayout = false;

            if (_willShow)
            {
                var textSize = drawingService.MeasureText(Content);
                var imageSize = Vector2.Zero;

                if (!string.IsNullOrWhiteSpace(_imageKey))
                {
                    imageSize = drawingService.GetImageSize(_imageKey);
                }

                Bounds = new Rectangle(
                    parentBounds.X,
                    parentBounds.Y,
                    (int)(textSize.X + _padding + imageSize.X),
                    (int)Math.Max(textSize.Y, imageSize.Y));
            }
            else
            {
                Bounds = new Rectangle(
                    parentBounds.X,
                    parentBounds.Y,
                    0,
                    0);
            }
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            base.Draw(drawingService, layer);

            if (_willShow)
            {
                if (ContentUpdater != null)
                {
                    var newContent = ContentUpdater();

                    if (newContent != Content)
                    {
                        _needsRelayout = true;
                        Content = newContent;
                    }
                }

                if (IconUpdater != null)
                {
                    _imageKey = IconUpdater();
                }

                if (ColorUpdater != null)
                {
                    Color = ColorUpdater();
                }

                var imageSize = Vector2.Zero;

                if (!string.IsNullOrWhiteSpace(_imageKey))
                {
                    imageSize = drawingService.GetImageSize(_imageKey);

                    drawingService
                        .DrawImage(_imageKey,
                        IconColorization,
                        new Vector2(
                            Bounds.X + imageSize.X / 2,
                            Bounds.Y + imageSize.Y / 2),
                        layer,
                        false);
                }

                drawingService
                    .DrawText(
                        Content,
                        new Point(
                            (int)(Bounds.X + imageSize.X + _padding),
                            Bounds.Y),
                        layer,
                        Color);
            }
        }
    }
}
