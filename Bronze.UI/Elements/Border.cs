﻿using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;

namespace Bronze.UI.Elements
{
    public class Border : AbstractUiElement
    {
        private int _margin;
        private AbstractUiElement _child;
        private bool _needsRelayout;

        public override bool NeedsRelayout => _needsRelayout || _child.NeedsRelayout;

        public Border(AbstractUiElement child, int margin = 10)
        {
            _child = child;
            _margin = margin;
            _needsRelayout = true;
        }
        
        public override void Update(
            InputState inputState,
            IAudioService audioService,
            float elapsedSeconds)
        {
            base.Update(inputState, audioService, elapsedSeconds);

            _child.Update(inputState, audioService, elapsedSeconds);
        }
        public override void DoLayout(
            IDrawingService drawingService,
            Rectangle parentBounds)
        {
            _needsRelayout = false;

            _child.DoLayout(
                drawingService, 
                new Rectangle(
                    parentBounds.X + _margin,
                    parentBounds.Y + _margin,
                    parentBounds.Width,
                    parentBounds.Height));

            Bounds = new Rectangle(
                parentBounds.X,
                parentBounds.Y,
                _child.Bounds.Width + _margin * 2,
                _child.Bounds.Height + _margin * 2);
        }
        
        public override void Draw(IDrawingService drawingService, float layer)
        {
            drawingService.DrawSlicedImage(
                UiImages.ModalBackground,
                Bounds,
                layer,
                false);

            _child.Draw(drawingService, layer + 1);
        }
    }
}
