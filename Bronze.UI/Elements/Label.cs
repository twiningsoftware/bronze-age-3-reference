﻿using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;
using System;

namespace Bronze.UI.Elements
{
    public class Label : AbstractUiElement
    {
        public string Content { get; set; }
        public float Scale { get; set; }
        public BronzeColor Color { get; set; }
        private Func<bool> _displayCondition;
        private bool _needsRelayout;

        public Func<string> ContentUpdater { get; set; }
        public Func<BronzeColor> ColorUpdater { get; set; }
        public override bool NeedsRelayout => _needsRelayout;

        public Label(
            string content,
            BronzeColor color = BronzeColor.White,
            Func<bool> displayCondition = null)
        {
            Content = content;
            Color = color;
            Scale = 1;
            _displayCondition = displayCondition ?? (() => true);
            _needsRelayout = true;
        }

        public override void Update(InputState inputState, IAudioService audioService, float elapsedSeconds)
        {
            base.Update(inputState, audioService, elapsedSeconds);

            //Here as well as in layout to prevent jitters on label creation, before content updater has run
            if (ContentUpdater != null)
            {
                var newContent = ContentUpdater();

                if(newContent != Content)
                {
                    _needsRelayout = true;
                    Content = newContent;
                }
            }

            if (ColorUpdater != null)
            {
                Color = ColorUpdater();
            }
        }

        public override void DoLayout(
            IDrawingService drawingService,
            Rectangle parentBounds)
        {
            _needsRelayout = false;

            var size = drawingService.MeasureText(Content) * Scale;
            
            Bounds = new Rectangle(
                parentBounds.X,
                parentBounds.Y,
                (int)size.X,
                (int)size.Y);
        }
        
        public override void Draw(IDrawingService drawingService, float layer)
        {
            if (ContentUpdater != null)
            {
                var newContent = ContentUpdater();

                if (newContent != Content)
                {
                    _needsRelayout = true;
                    Content = newContent;
                }
            }

            if (ColorUpdater != null)
            {
                Color = ColorUpdater();
            }

            if (_displayCondition())
            {
                drawingService
                    .DrawText(
                        Content,
                        new Point(Bounds.X, Bounds.Y),
                        layer,
                        Color,
                        Scale);
            }
        }
    }
}
