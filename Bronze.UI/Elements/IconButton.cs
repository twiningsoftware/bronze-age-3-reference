﻿using System;
using System.Linq;
using System.Numerics;
using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;
using Bronze.UI.Data;

namespace Bronze.UI.Elements
{
    public class IconButton : UiElementWithTooltip
    {
        private static readonly int PADDING = 5;

        private Action _clickAction;
        private Func<bool> _disableCheck;
        private Func<bool> _highlightCheck;
        private KeyCode? _hotkey;
        private int _hotkeyHighlightStart;
        private int _hotkeyHighlightLength;
        private bool _isHovered;
        private string _content;
        private string _imageKey;
        private bool _needsRelayout;
        private bool _hideState;

        public bool Highlighted { get; set; }
        public BronzeColor Colorization { get; set; }
        public Func<BronzeColor> ColorizationUpdater { get; set; }
        public override bool NeedsRelayout => base.NeedsRelayout || _needsRelayout;
        public Func<string> ImageUpdater { get; set; }
        public Func<bool> HideCheck { get; set; }

        public IconButton(
            string imageKey,
            string content,
            Action clickAction,
            KeyCode? hotkey = null,
            Func<bool> disableCheck = null,
            Func<bool> highlightCheck = null,
            string tooltip = null)
        {
            _imageKey = imageKey;
            _content = content;
            _clickAction = clickAction;
            _hotkey = hotkey;
            _disableCheck = disableCheck ?? (() => { return false; });
            _highlightCheck = highlightCheck ?? (() => { return false; });
            _needsRelayout = false;

            HideCheck = () => false;
            ImageUpdater = () => imageKey;

            RecalculateHotkeyHighlight();

            if (tooltip != null)
            {
                Tooltip = new SlicedBackground(new Text(tooltip)
                {
                    Width = Width.Fixed(200)
                });
            }
        }

        private void RecalculateHotkeyHighlight()
        {
            _hotkeyHighlightStart = -1;
            _hotkeyHighlightLength = 0;

            if (_hotkey == null)
            {
                return;
            }

            var matchStrings = UiUtil.StringsMatchingHotkey(_hotkey.Value);

            if (matchStrings.Any())
            {
                string highlightString = null;
                var highlightStart = int.MaxValue;

                foreach (var str in matchStrings)
                {
                    var index = _content.IndexOf(str, StringComparison.OrdinalIgnoreCase);

                    if (index >= 0 && index < highlightStart)
                    {
                        highlightString = str;
                        highlightStart = index;
                    }
                }

                if (highlightString != null)
                {
                    _hotkeyHighlightStart = highlightStart;
                    _hotkeyHighlightLength = highlightString.Length;
                }
            }
        }

        public override void DoLayout(
            IDrawingService drawingService,
            Rectangle parentBounds)
        {
            if (_hideState)
            {
                Bounds = new Rectangle(
                    parentBounds.X,
                    parentBounds.Y,
                    0,
                    0);

                return;
            }

            _needsRelayout = false;
            var imageSize = drawingService.GetImageSize(_imageKey);
            var textSize = drawingService.MeasureText(_content);

            var extraPadding = 0;
            if(_content.Any())
            {
                extraPadding = 2 * PADDING;
            }

            Bounds = new Rectangle(
                parentBounds.X,
                parentBounds.Y,
                (int)(textSize.X + imageSize.X + 2 * PADDING + extraPadding),
                (int)(Math.Max(textSize.Y, imageSize.Y) + 2 * PADDING));

            base.DoLayout(drawingService, parentBounds);
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            if (HideCheck())
            {
                return;
            }

            base.Draw(drawingService, layer);

            if(ColorizationUpdater != null)
            {
                Colorization = ColorizationUpdater();
            }

            if (_disableCheck() && !_highlightCheck())
            {
                drawingService.DrawSlicedImage(UiImages.ButtonInactive, Bounds, layer, false);
            }
            else if (_isHovered || Highlighted || _highlightCheck())
            {
                drawingService.DrawSlicedImage(UiImages.ButtonHover, Bounds, layer, false);
            }
            else
            {
                drawingService.DrawSlicedImage(UiImages.ButtonStandard, Bounds, layer, false);
            }
            
            var imageSize = drawingService.GetImageSize(_imageKey);
            var textSize = drawingService.MeasureText(_content);

            drawingService.DrawImage(
                _imageKey,
                Colorization,
                new Vector2(
                    Bounds.X + imageSize.X / 2 + PADDING,
                    Bounds.Y + imageSize.Y / 2 + PADDING),
                layer + 1,
                false);


            var textYOffset = (imageSize.Y - textSize.Y) / 2;

            drawingService
                .DrawText(
                    _content,
                    new Point(
                        (int)(Bounds.X + 2 * PADDING + imageSize.X),
                        (int)(Bounds.Y + PADDING + textYOffset)),
                    layer + 1,
                    BronzeColor.White);

            if (_hotkeyHighlightStart >= 0)
            {
                var xOffset = 0;

                if (_hotkeyHighlightStart > 0)
                {
                    xOffset = (int)drawingService.MeasureText(_content.Substring(0, _hotkeyHighlightStart)).X;
                }

                drawingService
                .DrawText(
                    _content.Substring(_hotkeyHighlightStart, _hotkeyHighlightLength),
                    new Point(
                        Bounds.X + PADDING + xOffset,
                        Bounds.Y + PADDING),
                    layer + 2,
                    BronzeColor.Yellow);
            }
        }

        public override void Update(
            InputState inputState,
            IAudioService audioService,
            float elapsedSeconds)
        {
            base.Update(inputState, audioService, elapsedSeconds);

            if (!_disableCheck())
            {
                _isHovered = Bounds.Contains(inputState.MousePosition);

                if ((_hotkey != null && inputState.JustPressed(_hotkey.Value) && inputState.FocusedElement == null)
                    || (inputState.MouseReleased && _isHovered))
                {
                    _clickAction();
                    audioService.PlaySound(UiSounds.Click);
                }
            }
            else
            {
                _isHovered = false;
            }

            _imageKey = ImageUpdater();

            var newHideState = HideCheck();

            if (newHideState != _hideState)
            {
                _hideState = newHideState;
                _needsRelayout = true;
            }
        }
    }
}