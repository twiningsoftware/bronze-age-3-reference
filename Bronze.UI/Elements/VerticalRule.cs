﻿using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;
using Bronze.UI.Data;

namespace Bronze.UI.Elements
{
    public class VerticalRule : UiElementWithTooltip
    {
        public Height Height { get; set; }
        public override bool NeedsRelayout => _needsRelayout;

        private string _imageKey;
        private bool _needsRelayout;

        public VerticalRule()
        {
            Height = Height.Relative(1);
            _imageKey = UiImages.RuleHorizontal;
            _needsRelayout = true;
        }

        public override void DoLayout(
            IDrawingService drawingService,
            Rectangle parentBounds)
        {
            base.DoLayout(drawingService, parentBounds);

            _needsRelayout = false;

            var imageBounds = drawingService.GetImageSize(_imageKey);

            Bounds = new Rectangle(
                parentBounds.X,
                parentBounds.Y,
                (int)imageBounds.X,
                Height.CalculateHeight(parentBounds));
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            base.Draw(drawingService, layer);

            drawingService.DrawImage(_imageKey, Bounds, layer, false);
        }
    }
}
