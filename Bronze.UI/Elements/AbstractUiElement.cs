﻿using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;

namespace Bronze.UI.Elements
{
    public abstract class AbstractUiElement : IUiElement
    {
        public static readonly float LAYER_STEP = 100;

        public Rectangle Bounds { get; protected set; }
        
        public abstract bool NeedsRelayout { get; }

        public AbstractUiElement()
        {
            Bounds = new Rectangle(0, 0, 0, 0);
        }

        public abstract void DoLayout(
            IDrawingService drawingService,
            Rectangle parentBounds);

        public abstract void Draw(IDrawingService drawingService, float layer);

        public virtual void Update(
            InputState inputState, 
            IAudioService audioService, 
            float elapsedSeconds)
        {
        }
    }
}
