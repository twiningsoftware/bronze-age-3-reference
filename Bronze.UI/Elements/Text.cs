﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;
using Bronze.UI.Data;

namespace Bronze.UI.Elements
{
    public class Text : AbstractUiElement
    {
        public string Content { get; set; }
        public BronzeColor Color { get; set; }
        public Width Width { get; set; }
        private Func<bool> _displayCondition;
        private bool _needsRelayout;
        private bool _oldDisplay;
        private string _preTrimOldContent;
        public Func<string> ContentUpdater { get; set; }
        public Func<BronzeColor> ColorUpdater { get; set; }
        public override bool NeedsRelayout => _needsRelayout;
        public bool WordWrap { get; set; }

        public Text(
            string content,
            BronzeColor color = BronzeColor.White,
            Func<bool> displayCondition = null)
        {
            Content = content;
            _preTrimOldContent = content;
            Color = color;
            Width = Width.Relative(1f);
            _needsRelayout = true;
            _displayCondition = displayCondition ?? (() => true);
            WordWrap = true;
        }

        public override void DoLayout(
            IDrawingService drawingService,
            Rectangle parentBounds)
        {
            _needsRelayout = false;
            
            if (ColorUpdater != null)
            {
                Color = ColorUpdater();
            }

            var maxWidth = Width.CalculateWidth(parentBounds);
            
            var size = drawingService.MeasureText(Content);

            if(size.X > maxWidth)
            {
                TrimText(drawingService, maxWidth);
                size = drawingService.MeasureText(Content);
                _needsRelayout = true;
            }

            Bounds = new Rectangle(
                parentBounds.X,
                parentBounds.Y,
                (int)size.X,
                (int)size.Y);
        }
        
        public override void Draw(IDrawingService drawingService, float layer)
        {
            var display = _displayCondition();

            if(display != _oldDisplay)
            {
                _oldDisplay = display;
                _needsRelayout = true;
            }

            if (display)
            {
                drawingService
                    .DrawText(
                    Content,
                    new Point(Bounds.X, Bounds.Y),
                    layer,
                    Color);
            }

            // Do this after draw, so the new content has a chance to be trimmed
            if (ContentUpdater != null)
            {
                var newContent = ContentUpdater();
                if (newContent != _preTrimOldContent)
                {
                    _preTrimOldContent = newContent;
                    Content = newContent;
                    _needsRelayout = true;
                }
            }
        }

        private void TrimText(IDrawingService drawingService, int maxWidth)
        {
            var result = new StringBuilder();

            var lines = Content.Split('\n');

            foreach(var line in lines)
            {
                var wordQueue = new Queue<string>(line.Split(' '));

                var lineResult = new StringBuilder();
                var currentLength = 0;

                while (wordQueue.Any())
                {
                    var nextWord = wordQueue.Dequeue();
                    if(currentLength > 0)
                    {
                        nextWord = " " + nextWord;
                    }

                    var nextWordLength = (int)drawingService.MeasureText(nextWord).X;

                    if(nextWordLength + currentLength <= maxWidth || currentLength < 1)
                    {
                        currentLength += nextWordLength;
                        lineResult.Append(nextWord);

                    }
                    else
                    {
                        result.AppendLine(lineResult.ToString());
                        lineResult.Clear();
                        lineResult.Append(nextWord.Trim());
                        currentLength = (int)drawingService.MeasureText(lineResult.ToString()).X;

                        if(!WordWrap)
                        {
                            Content = result.ToString().Trim();
                            return;
                        }
                    }
                }

                result.AppendLine(lineResult.ToString());
            }

            Content = result.ToString().Trim();
        }
    }
}
