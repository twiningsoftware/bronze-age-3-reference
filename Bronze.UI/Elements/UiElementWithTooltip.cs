﻿using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using System;

namespace Bronze.UI.Elements
{
    public abstract class UiElementWithTooltip : AbstractUiElement
    {
        public IUiElement Tooltip { get; set; }
        public TooltipPosition TooltipPosition { get; set; }
        private bool _showTooltip;

        protected virtual bool SuppressTooltip => false;
        public override bool NeedsRelayout => _needsRelayout || (Tooltip?.NeedsRelayout ?? false);
        private bool _needsRelayout;

        public UiElementWithTooltip()
        {
            TooltipPosition = TooltipPosition.UpperRight;
        }
        
        public override void DoLayout(
            IDrawingService drawingService, 
            Rectangle parentBounds)
        {
            _needsRelayout = false;

            if(Tooltip != null)
            {
                int tooltipX;
                int tooltipY;

                switch(TooltipPosition)
                {
                    case TooltipPosition.UpperRight:
                        tooltipX = Bounds.X + Bounds.Width;
                        tooltipY = Bounds.Y;
                        break;
                    case TooltipPosition.Above:
                        tooltipX = Bounds.X;
                        tooltipY = Bounds.Y - Tooltip.Bounds.Height;
                        break;
                    default:
                        throw new Exception("Unexpected TooltipPosition: " + TooltipPosition);
                }
                
                if(tooltipX + Tooltip.Bounds.Width > drawingService.DisplayWidth)
                {
                    tooltipX = Bounds.X - Tooltip.Bounds.Width;
                }

                if(tooltipY + Tooltip.Bounds.Height > drawingService.DisplayHeight)
                {
                    tooltipY -= (tooltipY + Tooltip.Bounds.Height - drawingService.DisplayHeight);
                }
                
                Tooltip.DoLayout(
                    drawingService,
                    new Rectangle(
                        tooltipX,
                        tooltipY,
                        parentBounds.Width,
                        parentBounds.Height));
            }
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            if(_showTooltip)
            {
                Tooltip.Draw(drawingService, layer + 600);
            }
        }
        
        public override void Update(
            InputState inputState, 
            IAudioService audioService, 
            float elapsedSeconds)
        {
            base.Update(inputState, audioService, elapsedSeconds);

            var oldShowTooltip = _showTooltip;

            _showTooltip = Tooltip != null
                && !SuppressTooltip
                && Bounds.Contains(inputState.MousePosition);

            if(_showTooltip != oldShowTooltip)
            {
                _needsRelayout = true;
            }

            Tooltip?.Update(inputState, audioService, elapsedSeconds);
        }
    }
}
