﻿using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;

namespace Bronze.UI.Elements
{
    public class SlicedBackground : AbstractUiElement
    {
        private const int PADDING = 10;

        private bool _needsRelayout;

        public IUiElement Child { get; }
        public override bool NeedsRelayout => _needsRelayout || Child.NeedsRelayout;

        public SlicedBackground(IUiElement child)
        {
            Child = child;
            _needsRelayout = true;
        }

        public override void DoLayout(
            IDrawingService drawingService, 
            Rectangle parentBounds)
        {
            _needsRelayout = false;

            Child.DoLayout(drawingService,
                new Rectangle(
                    parentBounds.X + PADDING,
                    parentBounds.Y + PADDING,
                    parentBounds.Width,
                    parentBounds.Height));

            Bounds = new Rectangle(
                parentBounds.X,
                parentBounds.Y,
                Child.Bounds.Width + PADDING * 2,
                Child.Bounds.Height + PADDING * 2);
        }

        public override void Update(InputState inputState, IAudioService audioService, float elapsedSeconds)
        {
            base.Update(inputState, audioService, elapsedSeconds);

            Child.Update(inputState, audioService, elapsedSeconds);
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            if (Child.Bounds.Width > 0 || Child.Bounds.Height > 0)
            {
                Child.Draw(drawingService, layer + 1);

                drawingService.DrawSlicedImage(
                    UiImages.ModalBackground,
                    Bounds,
                    layer,
                    false);
            }
        }
    }
}
