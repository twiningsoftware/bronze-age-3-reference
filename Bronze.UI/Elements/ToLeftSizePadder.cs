﻿using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;
using System;

namespace Bronze.UI.Elements
{
    public class ToLeftSizePadder : AbstractUiElement
    {
        private AbstractUiElement _child;
        private int _minWidth;
        private int _minHeight;
        private bool _needsRelayout;

        public override bool NeedsRelayout => _needsRelayout;

        public ToLeftSizePadder(AbstractUiElement child, int? minWidth = null, int? minHeight = null)
        {
            _child = child;
            _minWidth = minWidth ?? 0;
            _minHeight = minHeight ?? 0;
            _needsRelayout = true;
        }

        public override void Update(
            InputState inputState,
            IAudioService audioService,
            float elapsedSeconds)
        {
            base.Update(inputState,audioService, elapsedSeconds);

            _child.Update(inputState, audioService, elapsedSeconds);
        }

        public override void DoLayout(IDrawingService drawingService, Rectangle parentBounds)
        {
            _needsRelayout = false;
            
            Bounds = new Rectangle(
                parentBounds.X,
                parentBounds.Y,
                Math.Max(_minWidth, _child.Bounds.Width),
                Math.Max(_minHeight, _child.Bounds.Height));

            _child.DoLayout(
                drawingService,
                new Rectangle(
                    parentBounds.X,
                    parentBounds.Y,
                    parentBounds.Width,
                    parentBounds.Height));
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            _child.Draw(drawingService, layer);
        }
    }
}
