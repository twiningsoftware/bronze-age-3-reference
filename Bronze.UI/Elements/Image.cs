﻿using System;
using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;
using Bronze.UI.Data;

namespace Bronze.UI.Elements
{
    public class Image : UiElementWithTooltip
    {
        private string _imageKey;
        private bool _needsRelayout;
        private bool _oldVisiblity;
        public override bool NeedsRelayout => base.NeedsRelayout || _needsRelayout;

        public Func<string> ImageUpdater { get; set; }
        public Func<bool> VisibilityCheck { get; set; }
        public BronzeColor Colorization { get; set; }

        protected override bool SuppressTooltip => !(VisibilityCheck == null || VisibilityCheck());

        public Image(string imageKey, string tooltip = null)
        {
            _imageKey = imageKey;
            _needsRelayout = true;
            Colorization = BronzeColor.None;

            if (tooltip != null)
            {
                Tooltip = new SlicedBackground(new Text(tooltip)
                {
                    Width = Width.Fixed(200)
                });
            }
        }
        
        public override void DoLayout(
            IDrawingService drawingService,
            Rectangle parentBounds)
        {
            _needsRelayout = false;
            
            if (!string.IsNullOrWhiteSpace(_imageKey))
            {
                var imageBounds = drawingService.GetImageSize(_imageKey);

                Bounds = new Rectangle(
                    parentBounds.X,
                    parentBounds.Y,
                    (int)imageBounds.X,
                    (int)imageBounds.Y);
            }
            else
            {
                Bounds = new Rectangle(
                    parentBounds.X,
                    parentBounds.Y,
                    0,
                    0);
            }

            base.DoLayout(drawingService, parentBounds);
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            base.Draw(drawingService, layer);

            var visibility = VisibilityCheck == null || VisibilityCheck();

            if(visibility != _oldVisiblity)
            {
                _needsRelayout = true;
            }

            _oldVisiblity = visibility;
            if (visibility && !string.IsNullOrWhiteSpace(_imageKey))
            {
                drawingService.DrawImage(
                    _imageKey,
                    Bounds,
                    layer, 
                    Colorization,
                    false);
            }

            if (ImageUpdater != null)
            {
                var newImage = ImageUpdater();

                if (newImage != _imageKey)
                {
                    _needsRelayout = true;
                    _imageKey = newImage;
                }
            }
        }
    }
}
