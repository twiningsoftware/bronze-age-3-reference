﻿namespace Bronze.UI.Data
{
    public enum TooltipPosition
    {
        UpperRight,
        Above
    }
}
