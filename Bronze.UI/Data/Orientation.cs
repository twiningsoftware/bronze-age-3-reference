﻿namespace Bronze.UI.Data
{
    public enum Orientation
    {
        Vertical,
        Horizontal
    }
}
