﻿namespace Bronze.UI.Data
{
    public enum HorizontalAlignment
    {
        Right,
        Middle,
        Left
    }
}
