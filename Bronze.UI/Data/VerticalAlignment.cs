﻿namespace Bronze.UI.Data
{
    public enum VerticalAlignment
    {
        Top,
        Middle,
        Bottom
    }
}
