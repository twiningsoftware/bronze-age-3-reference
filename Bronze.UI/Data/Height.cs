﻿using Bronze.Contracts.Data;

namespace Bronze.UI.Data
{
    public abstract class Height
    {
        public abstract int CalculateHeight(Rectangle parentBounds);

        public static Height Relative(float percentOfParent)
        {
            return new RelativeHeight(percentOfParent);
        }

        public static Height Fixed(int pixelsHigh)
        {
            return new FixedHeight(pixelsHigh);
        }
        
        private class RelativeHeight : Height
        {
            private float _percentOfParent;

            public RelativeHeight(float percentOfParent)
            {
                _percentOfParent = percentOfParent;
            }

            public override int CalculateHeight(Rectangle parentBounds)
            {
                return (int)(_percentOfParent * parentBounds.Height);
            }
        }

        private class FixedHeight : Height
        {
            private int _pixelsHigh;

            public FixedHeight(int pixelsHigh)
            {
                _pixelsHigh = pixelsHigh;
            }

            public override int CalculateHeight(Rectangle parentBounds)
            {
                return _pixelsHigh;
            }
        }
    }
}
