﻿using Bronze.Contracts.Data;

namespace Bronze.UI.Data
{
    public abstract class Width
    {
        public abstract int CalculateWidth(Rectangle parentBounds);

        public static Width Relative(float percentOfParent)
        {
            return new RelativeWidth(percentOfParent);
        }

        public static Width Fixed(int pixelsWide)
        {
            return new FixedWidth(pixelsWide);
        }
        
        private class RelativeWidth : Width
        {
            private float _percentOfParent;

            public RelativeWidth(float percentOfParent)
            {
                _percentOfParent = percentOfParent;
            }

            public override int CalculateWidth(Rectangle parentBounds)
            {
                return (int)(_percentOfParent * parentBounds.Width);
            }
        }

        private class FixedWidth : Width
        {
            private int _pixelsWide;

            public FixedWidth(int pixelsWide)
            {
                _pixelsWide = pixelsWide;
            }

            public override int CalculateWidth(Rectangle parentBounds)
            {
                return _pixelsWide;
            }
        }
    }
}
