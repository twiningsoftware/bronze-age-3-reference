﻿namespace Bronze.UI
{
    public static class UiSounds
    {
        public static readonly string Click = "ui_click";
        public static readonly string NotificationMinor = "ui_notification_minor";
        public static readonly string NotificationMajor = "ui_notification_major";
        public static readonly string Dig = "ambient_digging";
        public static readonly string Demolish = "Collapse";
    }
}
