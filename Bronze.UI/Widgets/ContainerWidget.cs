﻿using System.Collections.Generic;
using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;

namespace Bronze.UI.Widgets
{
    public class ContainerWidget : UiWindowWidget
    {
        protected override HorizontalAlignment HorizontalAlignment { get; }
        protected override VerticalAlignment VerticalAlignment { get; }

        protected override int WidgetWidth => _internalGroup.Bounds.Width;
        protected override int WidgetHeight => _internalGroup.Bounds.Height;

        private readonly bool _showBackground;
        private StackGroup _internalGroup;

        public override bool NeedsRelayout => _internalGroup.NeedsRelayout;
        public override bool IsActive => true;

        public ContainerWidget(
            HorizontalAlignment horizontalAlignment,
            VerticalAlignment verticalAlignment,
            bool showBackground)
        {
            HorizontalAlignment = horizontalAlignment;
            VerticalAlignment = verticalAlignment;
            _showBackground = showBackground;
            _internalGroup = new StackGroup();
        }

        public Orientation Orientation
        {
            get => _internalGroup.Orientation;
            set => _internalGroup.Orientation = value;
        }

        public HorizontalAlignment HorizontalContentAlignment
        {
            get => _internalGroup.HorizontalContentAlignment;
            set => _internalGroup.HorizontalContentAlignment = value;
        }

        public VerticalAlignment VerticalContentAlignment
        {
            get => _internalGroup.VerticalContentAlignment;
            set => _internalGroup.VerticalContentAlignment = value;
        }

        public List<IUiElement> Children
        {
            get => _internalGroup.Children;
            set => _internalGroup.Children= value;
        }
        
        public override void Update(InputState inputState, IAudioService audioService, float elapsedSeconds)
        {
            base.Update(inputState, audioService, elapsedSeconds);

            _internalGroup.Update(inputState, audioService, elapsedSeconds);
        }

        public override void DoLayout(IDrawingService drawingService, Rectangle parentBounds)
        {
            base.DoLayout(drawingService, parentBounds);

            _internalGroup.DoLayout(drawingService, Bounds);
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            if (_showBackground)
            {
                base.Draw(drawingService, layer);
            }

            _internalGroup.Draw(drawingService, layer);
        }
    }
}
