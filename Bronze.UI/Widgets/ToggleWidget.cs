﻿using System;
using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;
using Bronze.UI.Data;
using Bronze.UI.Elements;

namespace Bronze.UI.Widgets
{
    public class ToggleWidget : AbstractUiElement
    {
        private StackGroup _row;

        private Text _textDisplay;
        private Func<bool> _getValue;
        private string _onText;
        private string _offText;

        public override bool NeedsRelayout => _row.NeedsRelayout;

        public ToggleWidget(
            string buttonText,
            KeyCode? hotkey,
            Func<bool> getValue,
            Action<bool> setValue,
            string onText,
            string offText)
        {
            _textDisplay = new Text("");
            _getValue = getValue;
            _onText = onText;
            _offText = offText;

            _row = new StackGroup
            {
                HorizontalContentAlignment = HorizontalAlignment.Left,
                VerticalContentAlignment = VerticalAlignment.Top,
                Orientation = Orientation.Horizontal
            };
            
            _row.Children.Add(new TextButton(buttonText,
                () =>
                {
                    setValue(!getValue());
                },
                hotkey));
            _row.Children.Add(new Spacer(10, 10));
            _row.Children.Add(new SizePadder(_textDisplay, minHeight: 35));
        }
        
        public override void DoLayout(IDrawingService drawingService, Rectangle parentBounds)
        {
            _row.DoLayout(drawingService, parentBounds);
            Bounds = _row.Bounds;
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            _row.Draw(drawingService, layer);
        }

        public override void Update(
            InputState inputState,
            IAudioService audioService,
            float elapsedSeconds)
        {
            base.Update(inputState, audioService, elapsedSeconds);

            if (_getValue())
            {
                _textDisplay.Content = _onText;
            }
            else
            {
                _textDisplay.Content = _offText;
            }
            
            _row.Update(inputState, audioService, elapsedSeconds);
        }
    }
}
