﻿using System;
using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;
using Bronze.UI.Data;
using Bronze.UI.Elements;

namespace Bronze.UI.Widgets
{
    public class SetNumberWidget : AbstractUiElement
    {
        private StackGroup _row;

        private Text _valueDisplay;
        private Func<string> _getDisplayValue;

        public override bool NeedsRelayout => _row.NeedsRelayout;

        public SetNumberWidget(
            Func<int> getValue, 
            Action<int> setValue,
            Func<string> getDisplayValue,
            int deltaAmount,
            int displayWidth)
        {
            _valueDisplay = new Text(string.Empty.PadLeft(displayWidth));
            _getDisplayValue = getDisplayValue;

            _row = new StackGroup
            {
                HorizontalContentAlignment = HorizontalAlignment.Left,
                VerticalContentAlignment = VerticalAlignment.Top,
                Orientation = Orientation.Horizontal
            };
            
            _row.Children.Add(new TextButton(" - ",
                () =>
                {
                    setValue(getValue() - deltaAmount);
                }));
            _row.Children.Add(new Spacer(10, 10));
            _row.Children.Add(new SizePadder(_valueDisplay, displayWidth, 35));
            _row.Children.Add(new Spacer(10, 10));
            _row.Children.Add(new TextButton(" + ",
                () =>
                {
                    setValue(getValue() + deltaAmount);
                }));
        }
        
        public override void DoLayout(IDrawingService drawingService, Rectangle parentBounds)
        {
            _row.DoLayout(drawingService, parentBounds);
            Bounds = _row.Bounds;
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            _row.Draw(drawingService, layer);
        }

        public override void Update(
            InputState inputState,
            IAudioService audioService,
            float elapsedSeconds)
        {
            base.Update(inputState, audioService, elapsedSeconds);

            _valueDisplay.Content = _getDisplayValue();
            
            _row.Update(inputState, audioService, elapsedSeconds);
        }
    }
}
