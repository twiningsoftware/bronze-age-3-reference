﻿using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;
using Bronze.Contracts.UI;
using Bronze.UI.Data;
using Bronze.UI.Elements;

namespace Bronze.UI.Widgets
{
    public abstract class UiWindowWidget : AbstractUiElement, IUiWidget
    {
        protected abstract HorizontalAlignment HorizontalAlignment { get; }
        protected abstract VerticalAlignment VerticalAlignment { get; }
        protected abstract int WidgetWidth { get; }
        protected abstract int WidgetHeight { get; }

        public abstract bool IsActive { get; }

        public override void DoLayout(IDrawingService drawingService, Rectangle parentBounds)
        {
            var x = parentBounds.X;
            var y = parentBounds.Y;

            if(HorizontalAlignment == HorizontalAlignment.Middle)
            {
                x += (parentBounds.Width - WidgetWidth) / 2;
            }
            else if (HorizontalAlignment == HorizontalAlignment.Right)
            {
                x += parentBounds.Width - WidgetWidth;
            }

            if (VerticalAlignment == VerticalAlignment.Middle)
            {
                y += (parentBounds.Height - WidgetHeight) / 2;
            }
            else if (VerticalAlignment == VerticalAlignment.Bottom)
            {
                y += parentBounds.Height - WidgetHeight;
            }

            Bounds = new Rectangle(
                x,
                y,
                WidgetWidth,
                WidgetHeight);
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            drawingService.DrawSlicedImage(UiImages.ModalBackground, Bounds, layer, false);
        }
    }
}
