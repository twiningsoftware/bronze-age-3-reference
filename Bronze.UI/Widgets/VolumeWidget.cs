﻿using System;
using Bronze.Contracts.Data;
using Bronze.Contracts.InjectableServices;
using Bronze.UI.Data;
using Bronze.UI.Elements;

namespace Bronze.UI.Widgets
{
    public class VolumeWidget : AbstractUiElement
    {
        private StackGroup _row;

        public override bool NeedsRelayout => _row.NeedsRelayout;

        public VolumeWidget(Func<float> getValue, Action<float> setValue)
        {
            _row = new StackGroup
            {
                HorizontalContentAlignment = HorizontalAlignment.Left,
                VerticalContentAlignment = VerticalAlignment.Top,
                Orientation = Orientation.Horizontal
            };

            _row.Children.Add(new TextButton(" - ",
                () =>
                {
                    setValue(getValue() - 0.1f);
                }));
            _row.Children.Add(new Spacer(10, 10));
            _row.Children.Add(new SizePadder(new ScrollBar(
                    Orientation.Horizontal,
                    0,
                    1,
                    getValue,
                    setValue)
                {
                    Height = Height.Fixed(20)
                },
                minHeight: 35));
            _row.Children.Add(new Spacer(10, 10));
            _row.Children.Add(new TextButton(" + ",
                () =>
                {
                    setValue(getValue() + 0.1f);
                }));
        }
        
        public override void DoLayout(
            IDrawingService drawingService, 
            Rectangle parentBounds)
        {
            _row.DoLayout(drawingService, parentBounds);
            Bounds = _row.Bounds;
        }

        public override void Draw(IDrawingService drawingService, float layer)
        {
            _row.Draw(drawingService, layer);
        }

        public override void Update(
            InputState inputState,
            IAudioService audioService,
            float elapsedSeconds)
        {
            base.Update(inputState, audioService, elapsedSeconds);

            _row.Update(inputState, audioService, elapsedSeconds);
        }
    }
}
