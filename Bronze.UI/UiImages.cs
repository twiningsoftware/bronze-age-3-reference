﻿namespace Bronze.UI
{
    public static class UiImages
    {
        public static readonly string ButtonStandard = "ui_buttonStandard";
        public static readonly string ButtonHover = "ui_buttonHover";
        public static readonly string ButtonInactive = "ui_buttonInactive";
        public static readonly string ScrollBackground = "ui_scrollBackground";
        public static readonly string ScrollFill = "ui_scrollFill";
        public static readonly string TextInputStandard = "ui_textInputStandard";
        public static readonly string TextInputFocused = "ui_textInputFocused";
        public static readonly string TextInputHover = "ui_textInputHover";
        public static readonly string ModalBackground = "ui_modalBackground";
        public static readonly string HexHighlight = "ui_hex_higlight";
        public static readonly string HexBorder = "ui_hex_border";
        public static readonly string Pathdot = "ui_pathdot";
        public static readonly string Bigdot = "ui_hex_bigdot";
        public static readonly string PathdotSmall = "ui_pathdot_small";

        public static readonly string RedX = "ui_hex_red_x";
        public static readonly string PopEmpty = "ui_pop_empty";
        public static readonly string MapBackground = "ui_mapBackground";
        public static readonly string DragSelectBox = "ui_dragselect";
        public static readonly string UnitSummaryBackground = "ui_unitsummary_background";
        public static readonly string RuleHorizontal = "ui_rule_horizontal";
        public static readonly string TileInvalid = "ui_tile_invalid";
        public static readonly string TileMask = "ui_tile_mask";
        public static readonly string CellInvalid = "ui_cell_invalid";
        public static readonly string TileSelected = "ui_tile_selected";

        public static readonly string CellExploredMask = "cell_explored_mask";

        public static readonly string TINY_POP_HOUSING = "ui_pop_housing";
        public static readonly string TINY_POP_GROWTH = "ui_pop_growth";
        public static readonly string TINY_POP_ASCENDING = "ui_pop_ascending";
        public static readonly string TINY_POP_HAPPINESS = "ui_icon_happiness";
        public static readonly string SMALL_PROSPERITY = "ui_icon_prosperity";
        public static readonly string TINY_TIME = "ui_icon_time";

        public static readonly string BUTTON_RECRUIT = "ui_button_recruit";
        public static readonly string BUTTON_MOVE = "ui_button_move";
        public static readonly string BUTTON_ACTION = "ui_button_action";
        public static readonly string BUTTON_SPLIT_ARMY = "ui_button_form_army";

        public static readonly string TradeRouteInvalid = "ui_trade_invalid";

        public static readonly string IconView = "ui_icon_view";

        public static readonly string STAT_VALOR = "ui_stat_valor";
        public static readonly string STAT_WIT = "ui_stat_wit";
        public static readonly string STAT_CHARISMA = "ui_stat_charisma";
        public static readonly string STAT_VALOR_HEADER = "ui_stat_valor_header";
        public static readonly string STAT_WIT_HEADER = "ui_stat_wit_header";
        public static readonly string STAT_CHARISMA_HEADER = "ui_stat_charisma_header";

        public static readonly string ICON_EMPIRE_AUTHORITY = "ui_icon_authority";
        public static readonly string ICON_EMPIRE_SETTLEMENTS = "ui_icon_settlements";

        public static readonly string PORTRAIT_NULL = "portrait_null";
        public static readonly string PORTRAIT_BORDER = "portrait_border";
        public static readonly string STANCE_AGRESSIVE = "ui_stance_agressive";
        public static readonly string STANCE_DEFENSIVE = "ui_stance_defensive";
        public static readonly string STANCE_EVASIVE = "ui_stance_evasive";
        public static readonly string STANCE_HARASSING = "ui_stance_harassing";

        public static readonly string ARMY_FLAG_ATTRITION = "army_flag_attrition";
    }
}
